/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.compounds.impl;

import java.util.LinkedList;
import java.util.List;

import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableEdge;
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableModelElement;
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableNode;
import mgl.Edge;
import mgl.ModelElement;

public class ExecutableEdgeImpl implements ExecutableEdge{
	private List<ExecutableNode> sources;
	private List<ExecutableNode> targets;
	
	private mgl.Edge modelElement;
	private ExecutableEdge parent;
	
	public ExecutableEdgeImpl() {
		this.sources = new LinkedList<ExecutableNode>();
		this.targets = new LinkedList<ExecutableNode>();
	}
	
	public mgl.Edge getModelElement() {
		return (Edge) this.modelElement;
	}
	
	public ExecutableEdge getParent() {
		return (ExecutableEdge) this.parent;
	}
	
	public List<ExecutableNode> getSources() {
		return sources;
	}
	
	public void setSources(List<ExecutableNode> sources) {
		this.sources = sources;
	}
	
	public List<ExecutableNode> getTargets() {
		return targets;
	}
	
	public void setTargets(List<ExecutableNode> targets) {
		this.targets = targets;
	}

	@Override
	public void setModelElement(ModelElement modelElement) {
		this.modelElement = (Edge) modelElement;
	}

	@Override
	public void setParent(ExecutableModelElement parent) {
		this.parent = (ExecutableEdge) parent;
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.generator.tracer

import de.jabc.cinco.meta.plugin.executer.generator.tracer.MainTemplate
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableGraphmodel

class ThreadViewTemplate extends MainTemplate {
	
	new(ExecutableGraphmodel graphmodel) {
		super(graphmodel)
	}
	
	override fileName() {
		return "ThreadView.java"
	}
	
	override create(ExecutableGraphmodel graphmodel)
	'''
	package «graphmodel.tracerPackage».views;
	
	import org.eclipse.jface.viewers.TableViewer;
	
	/**
	 * Combines a table view of one thread
	 * and the current mode: History, Level or Current
	 * @author zweihoff
	 *
	 */
	public final class ThreadView {
		private TableViewer viewer;
		private int currentMode;
		
		
		public final TableViewer getViewer() {
			return viewer;
		}
		public final void setViewer(TableViewer viewer) {
			this.viewer = viewer;
		}
		public final int getCurrentMode() {
			return currentMode;
		}
		public final void setCurrentMode(int currentMode) {
			this.currentMode = currentMode;
		}
		
	}
	
	'''
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.compounds;

import java.util.List;

public interface ExecutableGraphmodel extends ExecutableNodeContainer{
	
	public List<ExecutableModelElement> getModelElements();

	public void setModelElements(List<ExecutableModelElement> modelElements);
	
	public List<ExecutableContainer> getContainers();
	
	public List<ExecutableNode> getNodes();
	
	public List<ExecutableEdge> getEdges();
	
	public List<ExecutableNode> getExclusivelyNodes();

	public mgl.GraphModel getGraphModel();

	public void setGraphModel(mgl.GraphModel graphModel);
	
	public List<ExecutableNode> getContainableElements();
	
	public void setContainableElements(List<ExecutableNode> executableNodes);
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.compounds.impl;

import java.util.LinkedList;
import java.util.List;

import mgl.ModelElement;
import mgl.Node;
import mgl.NodeContainer;
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableContainer;
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableEdge;
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableModelElement;
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableNode;

public class ExecutableContainerImpl implements ExecutableContainer {
	private List<ExecutableEdge> outgoing;
	private List<ExecutableEdge> incoming;
	
	private mgl.NodeContainer modelElement;
	private ExecutableContainer parent;
	
	private List<ExecutableNode> containableNodes;
	
	public ExecutableContainerImpl() {
		this.outgoing = new LinkedList<ExecutableEdge>();
		this.incoming = new LinkedList<ExecutableEdge>();
		this.containableNodes = new LinkedList<ExecutableNode>();
	}
	
	@Override
	public List<ExecutableNode> getContainableNodes() {
		return this.containableNodes;
	}

	@Override
	public void setContainableNodes(List<ExecutableNode> containableNodes) {
		this.containableNodes = containableNodes;
	}

	@Override
	public void setModelElement(ModelElement modelElement) {
		this.modelElement = (NodeContainer) modelElement;
	}

	@Override
	public void setParent(ExecutableModelElement parent) {
		this.parent = (ExecutableContainer) parent;
	}

	@Override
	public Node getModelElement() {
		return this.modelElement;
	}

	@Override
	public void setModelElement(Node modelElement) {
		this.modelElement = (NodeContainer) modelElement;
	}

	@Override
	public ExecutableNode getParent() {
		return parent;
	}

	@Override
	public void setParent(ExecutableNode parent) {
		this.parent = (ExecutableContainer) parent;
	}

	@Override
	public List<ExecutableEdge> getOutgoing() {
		return this.outgoing;
	}

	@Override
	public void setOutgoing(List<ExecutableEdge> outgoing) {
		this.outgoing = outgoing;
	}

	@Override
	public List<ExecutableEdge> getIncoming() {
		return incoming;
	}

	@Override
	public void setIncoming(List<ExecutableEdge> incoming) {
		this.incoming = incoming;
	}

}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.generator.tracer

import de.jabc.cinco.meta.plugin.executer.generator.tracer.MainTemplate
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableGraphmodel

class CardinalityCheckerTemplate extends MainTemplate {
	
	new(ExecutableGraphmodel graphmodel) {
		super(graphmodel)
	}
	
	override fileName() {
		return "CardinalityChecker.java"
	}
	
	override create(ExecutableGraphmodel graphmodel)
	'''
	package «graphmodel.tracerPackage».match.simulation;
	
	import «graphmodel.apiPackage».ExecutableEdge;
	
	public class CardinalityChecker {
		
		public static boolean checkCardinality(ExecutableEdge transition,int size)
		{
			if(transition.getCardinality() < 0)return true;
			
			switch(transition.getCompare())
			{
				case EQ: return size == transition.getCardinality();
				case G: return size > transition.getCardinality();
				case GEQ: return size >= transition.getCardinality();
				case L: return size < transition.getCardinality();
				case LEQ: return size <= transition.getCardinality();
			}
			return false;
		}
	}
	
	'''
	
}

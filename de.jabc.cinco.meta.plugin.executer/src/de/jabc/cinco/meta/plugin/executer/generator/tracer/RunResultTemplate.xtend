/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.generator.tracer

import de.jabc.cinco.meta.plugin.executer.generator.tracer.MainTemplate
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableGraphmodel

class RunResultTemplate extends MainTemplate {
	
	new(ExecutableGraphmodel graphmodel) {
		super(graphmodel)
	}
	
	override fileName() {
		return "RunResult.java"
	}
	
	override create(ExecutableGraphmodel graphmodel)
	'''
	package «graphmodel.tracerPackage».runner.model;
	
	import java.util.LinkedList;
	import java.util.List;
	
	/**
	 * Compound class for the active and inactive runs
	 * present for the current runner.
	 * 
	 * The run result will be created after the termination of a runner
	 * to display the results.
	 * @author zweihoff
	 *
	 */
	public final class RunResult {
		private List<RunStepper> activeRunSteppers;
		private List<RunStepper> inactiveRunSteppers;
	
		public RunResult()
		{
			this.activeRunSteppers = new LinkedList<RunStepper>();
			this.inactiveRunSteppers = new LinkedList<RunStepper>();
		}
	
		public RunResult(List<RunStepper> activeRunSteppers, List<RunStepper> inactiveRunSteppers) {
			this.activeRunSteppers = activeRunSteppers;
			this.inactiveRunSteppers = inactiveRunSteppers;
		}
	
		public final List<RunStepper> getActiveRunSteppers() {
			return activeRunSteppers;
		}
		public final void setActiveRunSteppers(List<RunStepper> activeRunSteppers) {
			this.activeRunSteppers = activeRunSteppers;
		}
		public final List<RunStepper> getInactiveRunSteppers() {
			return inactiveRunSteppers;
		}
		public final void setInactiveRunSteppers(List<RunStepper> inactiveRunSteppers) {
			this.inactiveRunSteppers = inactiveRunSteppers;
		}
	
	}
	
	'''
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.generator.tracer

import de.jabc.cinco.meta.plugin.executer.generator.tracer.MainTemplate
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableGraphmodel
import mgl.MGLModel

class PluginTemplate extends MainTemplate {
	
	new(ExecutableGraphmodel graphmodel) {
		super(graphmodel)
	}
	
	override fileName() {
		return "plugin.xml"
	}
	
	override create(ExecutableGraphmodel graphmodel)
	'''
	<?xml version="1.0" encoding="UTF-8"?>
	<?eclipse version="3.4"?>
	<plugin>
	   <extension-point id="«graphmodel.tracerPackage»" name="Execution" schema="schema/«graphmodel.tracerPackage».exsd"/>
	   <extension-point id="«graphmodel.runnerPackage»" name="runner" schema="schema/«graphmodel.runnerPackage».exsd"/>
	
	   <extension
	         point="org.eclipse.ui.views">
	      <category
	            name="Execution Semantic"
	            id="«graphmodel.tracerPackage»">
	      </category>
	      <view
	            name="Tracer View"
	            category="«graphmodel.tracerPackage»"
	            class="«graphmodel.tracerPackage».views.View"
	            id="«graphmodel.tracerPackage».views.SampleView">
	      </view>
	   </extension>
	   <extension
	         point="org.eclipse.ui.perspectiveExtensions">
	      <perspectiveExtension
	            targetID="«(graphmodel.graphModel.eContainer as MGLModel).package».«graphmodel.graphModel.name.toLowerCase»toolperspective">
	         <view
	               ratio="0.5"
	               relative="de.jabc.cinco.meta.core.ui.propertyview"
	               relationship="right"
	               id="«graphmodel.tracerPackage».views.SampleView">
	         </view>
	      </perspectiveExtension>
	   </extension>
	   <extension
	         point="org.eclipse.help.contexts">
	      <contexts
	            file="contexts.xml">
	      </contexts>
	   </extension>
	
	</plugin>
	
	'''
	
}

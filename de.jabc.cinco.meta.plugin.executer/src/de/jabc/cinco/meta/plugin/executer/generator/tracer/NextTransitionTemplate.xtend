/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.executer.generator.tracer

import de.jabc.cinco.meta.plugin.executer.generator.tracer.MainTemplate
import de.jabc.cinco.meta.plugin.executer.compounds.ExecutableGraphmodel

class NextTransitionTemplate extends MainTemplate {
	
	new(ExecutableGraphmodel graphmodel) {
		super(graphmodel)
	}
	
	override fileName() {
		return "NextTransition.java"
	}
	
	override create(ExecutableGraphmodel graphmodel)
	'''
	package «graphmodel.tracerPackage».stepper.model;
	
	import java.util.LinkedList;
	import java.util.List;
	
	import «graphmodel.tracerPackage».match.model.TransitionMatch;
	/**
	 * A container class for the next edge to be the current element
	 * and a list of edges which will spawn new threads to hold the new execution control flows
	 * @author zweihoff
	 *
	 */
	public final class NextTransition {
		private TransitionMatch nextEdge;
		private List<TransitionMatch> forkEdges;
		
		public NextTransition()
		{
			this.forkEdges = new LinkedList<TransitionMatch>();
		}
		
		public final TransitionMatch getNextEdge() {
			return nextEdge;
		}
		public final void setNextEdge(TransitionMatch nextEdge) {
			this.nextEdge = nextEdge;
		}
		public final List<TransitionMatch> getForkEdges() {
			return forkEdges;
		}
		public final void setForkEdges(List<TransitionMatch> forkEdges) {
			this.forkEdges = forkEdges;
		}
		
		
	}
	
	'''
	
}

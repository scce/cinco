# How to build a Windows icon

- Windows requires application icons in the `.ico` format

- To build a `.ico` file, you'll need [Gimp](https://www.gimp.org)



## Instructions

1. Import all icon sizes into your canvas
	- Each icon size must have its own layer

2. Import all icon sizes into your canvas again
	- Each icon size must have its own layer
	- You'll have each icon size twice in your workspace

3. Go to "File" > "Export"

4. Choose `icon_windows.ico` as the file name and click "Export"

5. You'll see a screen with options for each layer

6. Select "32 bpp, 8-bit alpha, no palette" for the first half of the layers

7. Select "8 bpp, 1-bit alpha, 256-slot palette" for the second half of the layers

8. Uncheck "Compressed (PNG)" for every layer

9. Click "Export"

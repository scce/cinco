# How to build a macOS icon

- macOS requires application icons in the `.icns` format

- To build a `.icns` file, you'll need a macOS machine



## Instructions

1. Create a `.png` file for each icon size:

	| File name             | Actual size |
	|:----------------------|:-----------:|
	| `icon_16x16.png`      |   16 x 16   |
	| `icon_16x16@2x.png`   |   32 x 32   |
	| `icon_32x32.png`      |   32 x 32   |
	| `icon_32x32@2x.png`   |   64 x 64   |
	| `icon_128x128.png`    |  128 x 128  |
	| `icon_128x128@2x.png` |  256 x 256  |
	| `icon_256x256.png`    |  256 x 256  |
	| `icon_256x256@2x.png` |  512 x 512  |
	| `icon_512x512.png`    |  512 x 512  |
	| `icon_512x512@2x.png` | 1024 x 1024 |

2. Put all `.png`s into a folder named `icon_macos.iconset`

3. On a macOS machine run `iconutil -c icns icon_macos.iconset`

4. This produces a `icon_macos.icns` file

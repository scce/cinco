package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.plugin.template.FileTemplate
import mgl.ComplexAttribute
import mgl.Edge
import mgl.ModelElement
import mgl.Type
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class ScopeProviderTmpl extends FileTemplate {
	
	GeneratorUtils gu = GeneratorUtils.instance
	
	override getTargetFileName() '''«gu.getFileName(model)»GratextScopeProvider.xtend'''
	
	def scopeMethodTmpl(ModelElement me) {
		val modelElementRefs = me.allAttributes
			.filter(ComplexAttribute)
			.filter[model.containsChildModelElement(type.name)]
			.filter[!model.containsUserDefinedType(type.name)]
		if (me instanceof Edge || !modelElementRefs.isEmpty) '''
			dispatch def IScope getScope(^GratextInternal«me.name» element, String refName) {
				switch refName {
					«if (me instanceof Edge) '''
					case "_targetElement": element.scopeForContents(
						«FOR node : gu.getUsableNodes(graphModel, true).filter[MGLUtil.canNodeBeReferencedByEdge(it, me as Edge)] SEPARATOR ","»
							«node.fqInternalBeanName»
						«ENDFOR»
						 )
					'''»
					«modelElementRefs.map['''
						case "«name»": element.scopeForContents(
							«FOR t : (#[type] + type.nonAbstractSubTypes) SEPARATOR ","»
								«t.fqInternalBeanName»
							«ENDFOR»
						)
					'''].join("\n")»
				}
			}
		'''
	}
	
	def getNonAbstractSubTypes(Type it) {
		switch it {
			ModelElement: subTypes.drop[isAbstract]
			default: #[]
		}
	}
	
	override template() '''	
		package «package»
		
		import graphmodel.internal.InternalIdentifiableElement
		import «basePackage».*
		
		import org.eclipse.emf.ecore.EObject
		import org.eclipse.emf.ecore.EReference
		import org.eclipse.xtext.naming.QualifiedName
		import org.eclipse.xtext.scoping.IScope
		import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider
		
		import static extension org.eclipse.xtext.EcoreUtil2.getRootContainer
		import static extension org.eclipse.xtext.scoping.Scopes.scopeFor
		
		/**
		 * This class contains custom scoping description.
		 */
		class «gu.getFileName(model)»GratextScopeProvider extends AbstractDeclarativeScopeProvider {
			
			override getScope(EObject context, EReference reference) {
				getScope(context, reference.name) ?: super.getScope(context, reference)
			}
			
			dispatch def IScope getScope(EObject element, String refName) {
				null
			}
			«graphModel.scopeMethodTmpl ?: ""»
			
			«(gu.getUsableNodes(graphModel, true) + gu.getUsableEdges(graphModel) + gu.getUsableUserDefinedTypes(graphModel))//+ model.userDefinedTypes)
				.drop[isAbstract].map[scopeMethodTmpl].filterNull.join('\n')»
			def scopeForContents(EObject obj, Class<?>... types) {
				obj.rootContainer.contents
					.filter(anyTypeOf(types))
					.filter(InternalIdentifiableElement)
					.toScope
			}
			
			def getContents(EObject obj) {
				val Iterable<EObject> iterable = [obj.eAllContents]
				return iterable
			}
			
			def anyTypeOf(Class<?>... types) {
				[Object obj | types.stream.anyMatch[isInstance(obj)]]
			}
			
			def IScope toScope(Iterable<InternalIdentifiableElement> elements) {
				scopeFor(elements, [QualifiedName::create(id)], IScope.NULLSCOPE)
			}
		}
	'''
	
}
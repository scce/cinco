package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.template.FileTemplate
import java.util.Map
import java.util.Set
import mgl.Attribute
import mgl.ComplexAttribute
import mgl.Edge
import mgl.Enumeration
import mgl.GraphModel
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.PrimitiveAttribute
import mgl.ReferencedModelElement
import mgl.ReferencedType
import mgl.Type
import mgl.UserDefinedType
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier

import static de.jabc.cinco.meta.core.utils.MGLUtil.getAllSubTypes
import static de.jabc.cinco.meta.core.utils.MGLUtil.getComplexAttributeTypesDeeply
import static de.jabc.cinco.meta.core.utils.MGLUtil.allSuperTypes

class GrammarTmpl extends FileTemplate {
	protected extension MGLUtil = new MGLUtil
	protected extension GeneratorUtils = GeneratorUtils.instance
	
	val Map<String, String> importMappings = newHashMap
	
	var Map<MGLModel, Set<Type>> typesWithOrigins = newHashMap
	
	override getTargetFileName() '''«model.fileName»Gratext.xtext'''
	
	override init() {
		model.primeReferences.map[pr | switch pr.type {
			ModelElement: if(!(pr as ReferencedModelElement).local)
				MGLUtil.mglModel(pr.imprt).alias -> MGLUtil.nsURI(MGLUtil.mglModel(pr.imprt))
				else
					model.alias -> MGLUtil.nsURI(model)
			EClassifier: (pr.type as EClassifier).EPackage.name -> (pr.type as EClassifier).EPackage.nsURI
		}].filter[key !== null]
			.filter[value != MGLUtil.nsURI(model) && key != "ecore"]
			.forEach[importMappings.add(it)]
		typesWithOrigins = MGLUtil.typesWithOrigins(graphModel)
	}
	
	def modelRule(GraphModel it) {
		val containables = getUsableNodes(it).filter[!isAbstract]
		'''
			«graphModel.name» returns GratextInternal«graphModel.name»:{GratextInternal«graphModel.name»}
			'«graphModel.name»' (id = _ID)?
			('{'
				«attributes(it)»
				«FOR containable : containables BEFORE "( modelElements += " SEPARATOR "| modelElements += " AFTER ")*"»
					«containable.name»
				«ENDFOR»
			'}')?
			;
		'''
	}

	def containerRule(NodeContainer node) {
		val outEdges = node.outgoingEdges.flatMap[#[it] + MGLUtil.getAllSubclasses(it)].toSet
		outEdges.removeIf[isAbstract]
		val containables = node.containables.flatMap[#[it] + MGLUtil.getAllSubclasses(it)].drop[isAbstract].toSet
		'''
			«node.name» returns GratextInternal«node.name»:{GratextInternal«node.name»}
			'«node.name»' (id = _ID)? ( ('at' x=_EInt ',' y=_EInt)?
					& ('size' width=_EInt ',' height=_EInt)?
					& ('index' index=_EInt)? 
					)
			('{'
				«attributes(node)»
				«IF !containables.empty»
					( modelElements += «containables.get(0).name»
					«IF (containables.size > 1)»
						«FOR i:1..(containables.size-1)»
							| modelElements += «containables.get(i).name»
						«ENDFOR»
					«ENDIF»
					)*
				«ENDIF»
				«IF !outEdges.empty»
					( outgoingEdges += «outEdges.get(0).name»
					«IF (outEdges.size > 1)»
						«FOR i:1..(outEdges.size-1)»
							| outgoingEdges += «outEdges.get(i).name»
						«ENDFOR»
					«ENDIF»
					)*
				«ENDIF»
			'}')?
			;
		'''
	}

	def nodeRule(Node node) {
		val outEdges = node.outgoingEdges.flatMap[#[it] + MGLUtil.getAllSubclasses(it)].toSet
		outEdges.removeIf[isAbstract]
		'''
			«node.name» returns GratextInternal«node.name»:{GratextInternal«node.name»}
			'«node.name»' (id = _ID)? ( ('at' x=_EInt ',' y=_EInt)?
				& ('size' width=_EInt ',' height=_EInt)?
				& ('index' index=_EInt)? 
				)
			('{'
				«attributes(node)»
				«IF !outEdges.empty»
					( outgoingEdges += «outEdges.get(0).name»
					«IF (outEdges.size > 1)»
						«FOR i:1..(outEdges.size-1)»
							| outgoingEdges += «outEdges.get(i).name»
						«ENDFOR»
					«ENDIF»
					)*
				«ENDIF»
			'}')?
			;
		'''
	}

	def edgeRule(Edge edge) '''
		«edge.name» returns GratextInternal«edge.name»:{GratextInternal«edge.name»}
		'-«edge.name»->' _targetElement = [_graphmodel::InternalNode|_ID]
		('via' (bendpoints += _Point)+)?
		(decorators += _Decoration)*
		('{'
			('id' id = _ID)?
			«attributes(edge)»
		'}')?
		;
	'''

	def typeRule(UserDefinedType type) {
		val subTypes = getAllSubTypes(type)
		'''
			«type.name» returns GratextInternal«type.name»:
				__«type.name»«subTypes.join('|','|','')['''__«it.name»''']»
			;
			__«type.name» returns GratextInternal«type.name»:{GratextInternal«type.name»}
				'«type.name»' (id = _ID)? '{'
					«attributes(type)»
				'}'
			;
		'''
	}

	def enumRule(Enumeration type) '''
		enum «type.name» returns «MGLUtil.mglModel(type).alias»::«type.name»:
			«type.literals.map(literal | '''^«literal»''').join(' | ')»
		;
	'''

	def typeReference(MGLModel it, Attribute attr) {
		var MGLModel containingMGLModel
		switch attr {
			PrimitiveAttribute: '''_«attr.type.literal»'''
			ComplexAttribute:
				if (containsEnumeration(attr.type.name) || containsUserDefinedType(attr.type.name)) {
					attr.type.name
				} else if (containsModelElement(attr.type.name)) {
					'''[«model.alias»::«attr.type.name»|_ID]'''
				} else if ((containingMGLModel = getContainingImportedMglModel(attr.type.name)) !== null) {
					'''[«containingMGLModel.alias»::«attr.type.name»|_ID]'''	
				} else {
					attr.type.name
				}
		}
	}

	def typeReference(ReferencedType ref) {
		val type = ref.type
		if (type !== null) {
			val entry = switch it:type {
				ModelElement: if(!(ref as ReferencedModelElement).isLocal)
					MGLUtil.mglModel(ref.imprt).alias -> name
					else
						model.alias -> name
				EClass:{
					if (EPackage.name == "ecore")
						"_ecore" -> name
					else EPackage.name -> name
				}
			}
			'''[«entry.key»::«entry.value»|_ID]'''
		}
	}

	def gratextName(Attribute attr) {
		switch attr {
			ComplexAttribute:
				switch attr.type {
					UserDefinedType: '''gratext_«attr.name»'''
					default:
						attr.name
				}
			default:
				attr.name
		}
	}

	def attributes(ModelElement elm) {
		val sorted = cpd?.annotations?.exists[name == "sortGratext"]
		val attrs = if (sorted) elm.allAttributes.sortBy[name] else elm.allAttributes
		val attrSeparator = if (sorted) ' \n' else ' &\n'
		val attrsStr = attrs.map [
			switch it {
				case (upperBound < 0) || (upperBound > 1): '''( '«it.name»' '[' ( ^«gratextName» += «typeReference(model,it)» ( ',' ^«gratextName» += «typeReference(model,it)» )* )? ']' )?'''
				default: '''( '«name»' ^«gratextName» = «typeReference(model,it)» )?'''
			}
		].join(attrSeparator)
		val primeStr = switch elm {
			Node: elm.prime()
		}
		if (attrs.empty)
			primeStr
		else if (primeStr !== null)
			"( " + primeStr + attrSeparator + attrsStr + " )"
		else "( " + attrsStr + " )"
	}

	def prime(Node node) {
		val ref = node.anyPrimeReference
		if (ref !== null)
			'''( '«ref.name»' prime = «ref.typeReference» | 'libraryComponentUID' libraryComponentUID = _STRING )'''
	}

	def imports() {
		val typeOrigins = typesWithOrigins.keySet.filter[m|!MGLUtil.equalMGLModels(m,model)]
		'''
			import "«MGLUtil.nsURI(graphModel)»/«graphModel.name.toLowerCase»/gratext"
			import "«MGLUtil.nsURI(model)»" as «model.alias»
			«importMappings.entrySet.map['''import "«it.value»" as «it.key»'''].join('\n')»
			«FOR typeOrigin : typeOrigins»
				«IF !importMappings.values.contains(MGLUtil.nsURI(typeOrigin))»
					import "«MGLUtil.nsURI(typeOrigin)»" as «typeOrigin.alias»
				«ENDIF»
			«ENDFOR»
			import "http://www.jabc.de/cinco/gdl/graphmodel/internal" as _graphmodel
			import "http://www.eclipse.org/emf/2002/Ecore" as _ecore
		'''
	}
	
	def String alias(MGLModel mglModel) {
		'''_mgl_«mglModel.fileName.toLowerCase»'''
	}
	
	def escaped(String pkg) {
		pkg.split('\\.').map['^' + it].join('.')
	}
	
	static def allUserDefinedTypes(GraphModel graphModel) {
		GeneratorUtils.instance
			.getUsableUserDefinedTypes(graphModel)
			.flatMap[#[it] + getAllSubTypes(it) + allSuperTypes(it)]
			.flatMap[#[it] + getComplexAttributeTypesDeeply(it)]
			.filter(UserDefinedType)
			.toSet
	}

	override template() {
		val usableNodes = graphModel.getUsableNodes(true).filter[!isAbstract]
		val usableEdges = graphModel.getUsableEdges.filter[!isAbstract]
		val usableUserDefinedTypes = allUserDefinedTypes(graphModel) 
		
		'''
			grammar «package.escaped».«model.fileName»Gratext hidden(_WS, _ML_COMMENT, _SL_COMMENT)
			
			«imports»
			«modelRule(graphModel)»
			«FOR node : usableNodes.filter(NodeContainer)»«containerRule(node)»«ENDFOR»
			
			«FOR node : usableNodes.filter[!(it instanceof NodeContainer)]»«nodeRule(node)»«ENDFOR»
			
			«FOR edge : usableEdges»«edgeRule(edge)»«ENDFOR»
			
			«FOR type : usableUserDefinedTypes»«typeRule(type)»«ENDFOR»
			
			«FOR type : graphModel.enumerations»«enumRule(type)»«ENDFOR»
			
			_Decoration returns _graphmodel::_Decoration:{_graphmodel::_Decoration}
				'decorate' (nameHint = _EString)? 'at' locationShift = _Point
			;
			
			_Point returns _graphmodel::_Point:{_graphmodel::_Point}
				'(' x = _EInt ',' y = _EInt ')'
			;
			
			_EString returns _ecore::EString:
				_STRING
			;
			
			_EInt returns _ecore::EInt:
				_SIGN? _INT
			;
			
			_ELong returns _ecore::ELong:
				_SIGN? _INT
			;
			
			_EDouble returns _ecore::EDouble:
				_SIGN? _INT? '.' _INT
			;
			
			_EFloat returns _ecore::EFloat:
				_SIGN? _INT? '.' _INT
			;
			
			_EBoolean returns _ecore::EBoolean:
				'true' | 'false'
			;
			
			_EDate returns _ecore::EDate:
				_STRING
			;
			
			terminal _ID : '^'?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'-'|'0'..'9')* ;
			
			terminal _SIGN : '+' | '-' ;
			
			terminal _INT returns _ecore::EInt: ('0'..'9')+ ;
			
			terminal _STRING	: 
						'"' ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|'"') )* '"' |
						"'" ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|"'") )* "'"
					; 
			terminal _ML_COMMENT	: '/*' -> '*/' ;
			terminal _SL_COMMENT 	: '//' !('\n'|'\r')* ('\r'? '\n')? ;
			
			terminal _WS			: (' '|'\t'|'\r'|'\n')+ ;
			
			terminal _ANY_OTHER: . ;
		'''
	}
}
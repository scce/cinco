package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.template.FileTemplate
import mgl.ComplexAttribute
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import mgl.UserDefinedType

import static de.jabc.cinco.meta.plugin.gratext.tmpl.file.GrammarTmpl.allUserDefinedTypes

class EcoreTmpl extends FileTemplate {
	
	val generatorUtils = GeneratorUtils.instance
	override getTargetFileName() '''«graphModel.name»Gratext.ecore'''
	
	def graphModelURI() '''«MGLUtil.nsURI(graphModel)»/«graphModel.name.toLowerCase»/internal'''
	
	def mglModelEcorePlatformResourceURI() '''platform:/resource/«model.projectName»/src-gen/model/«cpd.name».ecore#//«generatorUtils.getFileName(model).toLowerCase»'''
	
	def mglModelEcorePlatformResourceURI(MGLModel mgl) {
		val cincoProduct = MGLUtil.mglModelCpdMap.entrySet.findFirst[MGLUtil.equalMGLModels(key, mgl)]?.value
		if(cincoProduct === null) {
			'''platform:/resource/«mgl.projectName»/src-gen/model/«cpd.name».ecore#//«generatorUtils.getFileName(mgl).toLowerCase»'''
		} else {
			'''platform:/resource/«mgl.projectName»/src-gen/model/«cincoProduct.name».ecore#//«generatorUtils.getFileName(mgl).toLowerCase»'''
		}
	}
	
	
	def Iterable<E_Class> classes() {#[
			new E_Class("_Placed")
				.add(new E_Attribute("index", E_Type.EInt).defaultValue("-1")),
			new E_Interface("_EdgeSource")
				.add(new E_Reference("outgoingEdges", "#//_Edge").containment(true).upper(-1)),
			new E_Interface("_Edge"),
			new E_Interface("_Prime")
				.add(new E_Reference("prime", "ecore:EClass http://www.eclipse.org/emf/2002/Ecore#//EObject"))
	]}

	def getInterfaces(Node node) {
		var str = '''<eSuperTypes href="#//_Placed"/>'''
		if (node.isEdgeSource)
			str += '''<eSuperTypes href="#//_EdgeSource"/>'''
		if (node.hasPrimeReference)
			str += '''<eSuperTypes href="#//_Prime"/>'''
		return str
	}

	def getTypeAttributes(ModelElement it) {
		allAttributes.filter(ComplexAttribute)
			.filter[type instanceof UserDefinedType].map['''
				<eStructuralFeatures xsi:type="ecore:EReference"
				«IF ((upperBound < 0) || (upperBound > 1))»
					upperBound="-1"
				«ENDIF»
				name="gratext_«name»" eType="#//GratextInternal«type.name»" containment="true"/>
			'''].join("\n") ?: ""
	}

	override template() '''	
		<?xml version="1.0" encoding="ASCII"?>
		<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore"
		    name="gratext"
		    nsURI="«MGLUtil.nsURI(graphModel)»/«graphModel.name.toLowerCase»/gratext"
		    nsPrefix="gratext">
			<eAnnotations source="http://www.eclipse.org/emf/2002/Ecore">
			</eAnnotations>
			<eClassifiers xsi:type="ecore:EClass" name="GratextInternal«graphModel.name»">
				<eSuperTypes href="«mglModelEcorePlatformResourceURI»/internal/Internal«graphModel.name»"/>
				«graphModel.typeAttributes»
			</eClassifiers>
			«FOR gmEntry : MGLUtil.nodesWithOrigins(graphModel, true).entrySet»
				«FOR node : gmEntry.value»
					<eClassifiers xsi:type="ecore:EClass" name="GratextInternal«node.name»" abstract="«node.isIsAbstract»">
						<eSuperTypes href="«mglModelEcorePlatformResourceURI(gmEntry.key)»/internal/Internal«node.name»"/>
						«node.interfaces»
						«node.typeAttributes»
					</eClassifiers>
				«ENDFOR»
			«ENDFOR»
			«FOR gmEntry : MGLUtil.edgesWithOrigins(graphModel).entrySet»
				«FOR edge : gmEntry.value»
					<eClassifiers xsi:type="ecore:EClass" name="GratextInternal«edge.name»">
						<eSuperTypes href="«mglModelEcorePlatformResourceURI(gmEntry.key)»/internal/Internal«edge.name»"/>
						<eSuperTypes href="#//_Edge"/>
						«edge.typeAttributes»
					</eClassifiers>
				«ENDFOR»
			«ENDFOR»
			«FOR gmEntry : allUserDefinedTypes(graphModel).groupBy[generatorUtils.getMGLModel(it)].entrySet»
				«FOR userDefinedType : gmEntry.value»
					<eClassifiers xsi:type="ecore:EClass" name="GratextInternal«userDefinedType.name»" abstract="«userDefinedType.isIsAbstract»">
						<eSuperTypes href="«mglModelEcorePlatformResourceURI(gmEntry.key)»/internal/Internal«userDefinedType.name»"/>
						«IF userDefinedType.superType !== null»
							<eSuperTypes href="#//GratextInternal«userDefinedType.superType.name»"/>
						«ENDIF»
						«userDefinedType.typeAttributes»
					</eClassifiers>
				«ENDFOR»
			«ENDFOR»
			«FOR cls:classes»
				«cls.toXMI»
			«ENDFOR»
		</ecore:EPackage>
	'''

	static class E_Class {
		protected String name
		protected String supertypes
		protected Boolean isInterface = false
		val attributes = <E_Attribute>newArrayList
		val references = <E_Reference>newArrayList
		
		new(String name) { this.name = name }
		
		def add(E_Attribute attr) { attributes.add(attr); attr.classname = name; this }
		def add(E_Reference ref) { references.add(ref); ref.classname = name; this }
		def supertypes(String types) { supertypes = types; this }
		
		def toXMI() '''
			<eClassifiers xsi:type="ecore:EClass" name="«name»" abstract="«isInterface»" interface="«isInterface»" eSuperTypes="«supertypes»">
			«FOR attr:attributes»«attr.toXMI»«ENDFOR»
			«FOR ref:references»«ref.toXMI»«ENDFOR»
			</eClassifiers>
		'''
		
		def toGenmodelXMI(String ecoreClass) '''
			<genClasses ecoreClass="«ecoreClass»#//«name»">
				«FOR attr:attributes»«attr.toGenmodelXMI(ecoreClass)»«ENDFOR»
				«FOR ref:references»«ref.toGenmodelXMI(ecoreClass)»«ENDFOR»
			</genClasses>
		'''
	}	
	
	static class E_Interface extends E_Class {
		new(String name) { super(name); isInterface = false }
	}
	
	static class E_Attribute {
		protected String name
		protected String classname
		protected String type
		protected String defaultValue
		
		new(String name, String type) { this.name = name; this.type = type }
		
		def defaultValue(String value) { defaultValue = value; this }
		
		def toXMI()
		'''<eStructuralFeatures xsi:type="ecore:EAttribute" name="«name»" eType="«type»" defaultValueLiteral="«defaultValue»"/>'''
		
		def toGenmodelXMI(String ecoreClass)
		'''<genFeatures createChild="false" ecoreFeature="ecore:EAttribute «ecoreClass»#//«classname»/«name»"/>'''
	}
	
	static class E_Reference {
		protected String name
		protected String classname
		protected String type
		protected boolean isContainment = false
		protected int lower = 0
		protected int upper = 1
		
		new(String name, String type) {
			this.name = name
			this.type = type
		}
		
		def containment(boolean flag) { isContainment = flag; this }
		def lower(int num) { lower = num; this }
		def upper(int num) { upper = num; this }
		
		def toXMI() '''
			<eStructuralFeatures xsi:type="ecore:EReference" name="«name»" eType="«type»" containment="«isContainment»" lowerBound="«lower»" upperBound="«upper»"/>
		'''
		
		def toGenmodelXMI(String ecoreClass) '''
			<genFeatures property="None" children="true" createChild="true" ecoreFeature="ecore:EReference «ecoreClass»#//«classname»/«name»"/>
		'''
	}
	
	static class E_Type {
		static val EInt = '''ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EInt'''
	}

}
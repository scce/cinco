package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class TransformerTmpl extends FileTemplate {
	protected extension MGLUtil = new MGLUtil
	extension GeneratorUtils = GeneratorUtils.instance
	
	override getTargetFileName() '''«model.fileName»GratextTransformer.xtend'''
	
	override template() { 
		val dependendMGLModels = MGLUtil.getAllExtendedGraphModels(graphModel).map[MGLUtil.mglModel(it)].toSet
		dependendMGLModels.addAll(MGLUtil.nodesWithOrigins(graphModel).keySet)
		dependendMGLModels.addAll(MGLUtil.edgesWithOrigins(graphModel).keySet)
		'''	
			package «package»
			
			import graphmodel.internal.InternalModelElementContainer
			import de.jabc.cinco.meta.plugin.gratext.runtime.resource.Transformer
			import «model.beanPackage».«model.fileName.toLowerCase».«model.fileName.toLowerCase.toFirstUpper»Package
			import «basePackage»._Placed
			import org.eclipse.emf.ecore.EPackage
			
			class «model.fileName»GratextTransformer extends Transformer {
			
				new() { super(
					#[EPackage.Registry.INSTANCE.getEFactory("«MGLUtil.nsURI(model)»")
					«FOR mgl : dependendMGLModels BEFORE "," SEPARATOR ","»
						EPackage.Registry.INSTANCE.getEFactory("«MGLUtil.nsURI(mgl)»")
					«ENDFOR»
					].toSet,
					#[EPackage.Registry.INSTANCE.getEPackage("«MGLUtil.nsURI(model)»/internal")
					«FOR mgl : dependendMGLModels BEFORE "," SEPARATOR ","»
						EPackage.Registry.INSTANCE.getEPackage("«MGLUtil.nsURI(mgl)»/internal")
					«ENDFOR»
					].toSet,
					«model.fileName.toLowerCase.toFirstUpper»Package.eINSTANCE.get«graphModel.name»
				)}
			
				dispatch def getIndex(_Placed it) {
					if (index >= 0) index
					else (eContainer as InternalModelElementContainer).modelElements.indexOf(it)
				}
			}
		'''
	}
	
}
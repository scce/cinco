package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class ResourceTmpl extends FileTemplate {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	override getTargetFileName() '''«model.fileName»GratextResource.xtend'''
	
	override template() '''
		package «package»

		import graphmodel.internal.InternalGraphModel
		
		import de.jabc.cinco.meta.core.utils.registry.NonEmptyIdentityRegistry
		import de.jabc.cinco.meta.plugin.gratext.runtime.resource.GratextResource
		
		import «package».generator.«model.fileName»GratextTransformer
		
		class «model.fileName»GratextResource extends GratextResource {
			
			public static val transformers = new NonEmptyIdentityRegistry<InternalGraphModel,«model.fileName»GratextTransformer> [
				new «model.fileName»GratextTransformer
			]
			
			val lastTransformers = new NonEmptyIdentityRegistry<InternalGraphModel,«model.fileName»GratextTransformer> [
				new «model.fileName»GratextTransformer
			]
		
			override getTransformer(InternalGraphModel model) {
				transformers.get(model)
			}
		
			override getLastTransformer(InternalGraphModel model) {
				lastTransformers.get(model)
			}
		
			override removeTransformer(InternalGraphModel model) {
				lastTransformers.put(model, transformers.remove(model))
			}
		
			override isSortGratext() {
				«cpd?.annotations?.exists[name == "sortGratext"]»
			}
		}
	'''
	
}
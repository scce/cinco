package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import org.eclipse.core.runtime.IProgressMonitor
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension

class EditorTmpl extends FileTemplate {
	
	val generatorUtils = GeneratorUtils.instance
	
	override getTargetFileName() '''«generatorUtils.getFileName(model)»GratextEditor.java'''
	
	override template() '''
		package «package»;
		
		import de.jabc.cinco.meta.plugin.gratext.runtime.editor.MultiPageGratextEditor;
		import org.eclipse.xtext.ui.editor.XtextEditor;
		
		public class «generatorUtils.getFileName(model)»GratextEditor extends MultiPageGratextEditor {
		
			@Override
			protected XtextEditor getSourceEditor() {
				«generatorUtils.getFileName(model)»GratextExecutableExtensionFactory fac = new «generatorUtils.getFileName(model)»GratextExecutableExtensionFactory();
				try {
					Class<?> clazz = fac.getBundle().loadClass("org.eclipse.xtext.ui.editor.XtextEditor");
					Object editor = fac.getInjector().getInstance(clazz);
					return (XtextEditor) editor;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					return null;
				}
			}
		
			@Override
			public void doSave(«IProgressMonitor.name» monitor) {
				org.eclipse.emf.ecore.EObject obj = new «WorkbenchExtension.name»().getActiveGraphModel().getInternalElement_();
				«IF model.annotations.exists[name == "preSave"]»
					if (obj instanceof «graphModel.getFqInternalBeanName()») {
						«generatorUtils.fqBeanName(model)» graph = («graphModel.fqBeanName») ((«graphModel.getFqInternalBeanName()») obj).getElement();
						new «model.annotations.filter[name == "preSave"]?.head?.value?.head»().preSave(graph);
					}
				«ENDIF»
				super.doSave(monitor);
			}
		}
	'''
	
}
package de.jabc.cinco.meta.plugin.gratext.tmpl.file

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class ProposalProviderTmpl extends FileTemplate {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	override getTargetFileName() '''«model.fileName»GratextProposalProvider.xtend'''
	
	override template() '''
		package «package»
		
		import org.eclipse.emf.ecore.EObject
		
		class «model.fileName»GratextProposalProvider extends Abstract«model.fileName»GratextProposalProvider {
			
			override getDisplayString(EObject element, String qualifiedNameAsString, String shortName) {
				element.eClass.name + " " + (
					labelProvider.getText(element) ?: super.getDisplayString(element, qualifiedNameAsString, shortName)
				)
			}
			
		}
	'''
}
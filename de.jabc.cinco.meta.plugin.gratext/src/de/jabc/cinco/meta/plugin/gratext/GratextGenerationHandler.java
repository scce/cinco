/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.gratext;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import productDefinition.CincoProduct;
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.core.utils.MGLUtil;
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import de.jabc.cinco.meta.runtime.xapi.FileExtension;
import mgl.GraphModel;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class GratextGenerationHandler extends AbstractHandler {

	private static FileExtension fileHelper = new FileExtension();
	
	@SuppressWarnings("restriction")
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IFile mglFile = MGLSelectionListener.INSTANCE.getCurrentMGLFile();
		if (mglFile == null) 
			return null;
		MGLModel model = fileHelper.getContent(mglFile, MGLModel.class, 0);
		
		for (MGLModel mgl: GeneratorUtils.getInstance().allMGLs) {
			if (MGLUtil.equalMGLModels(model, mgl)) {
				model = mgl;
			}
		}
		
		CincoProduct cpd = MGLSelectionListener.INSTANCE.getSelectedCPD();
		for (GraphModel graphModel: model.getGraphModels()) {
			if (!graphModel.isIsAbstract()) {
				new GratextPlugin().createGratextProjects(cpd, model, graphModel);
			}
		}
		
		return null;
		
	}
	
}

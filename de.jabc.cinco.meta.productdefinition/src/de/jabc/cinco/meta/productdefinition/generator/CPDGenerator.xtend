/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.generator

import de.jabc.cinco.meta.core.ui.templates.DefaultPerspectiveContent
import de.jabc.cinco.meta.core.utils.BuildProperties
import de.jabc.cinco.meta.core.utils.BundleRegistry
import de.jabc.cinco.meta.core.utils.projects.ContentWriter
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.productdefinition.configuration.CincoPluginConfiguration
import de.jabc.cinco.meta.productdefinition.templates.ExtensionTemplate
import de.jabc.cinco.meta.productdefinition.templates.ParentPOMTemplate
import de.jabc.cinco.meta.productdefinition.templates.ProductProjectPOMTemplate
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.charset.StandardCharsets
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set
import org.apache.commons.io.FileUtils
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.pde.internal.core.iproduct.IArgumentsInfo
import org.eclipse.pde.internal.core.iproduct.IProduct
import org.eclipse.pde.internal.core.iproduct.IProductFeature
import org.eclipse.pde.internal.core.product.AboutInfo
import org.eclipse.pde.internal.core.product.ArgumentsInfo
import org.eclipse.pde.internal.core.product.LauncherInfo
import org.eclipse.pde.internal.core.product.ProductFeature
import org.eclipse.pde.internal.core.product.SplashInfo
import org.eclipse.pde.internal.core.product.WindowImages
import org.eclipse.pde.internal.core.product.WorkspaceProductModel
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.util.StringInputStream
import productDefinition.CincoProduct
import productDefinition.Color
import static extension de.jabc.cinco.meta.core.utils.projects.ContentWriter.stripOffQuotes
/**
 * Generates code from your model files on save.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#TutorialCodeGeneration
 */
class CPDGenerator implements IGenerator {
	
	extension WorkspaceExtension = new WorkspaceExtension
	var CincoProduct cpd
	var IProgressMonitor monitor
	var IProject mglProject
	var String id
	
	var IProject productProject
	var IProduct product
	
	/** BuildProperties of the MGL project */
	var BuildProperties bp
	
	var Set<String> binIncludes

	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
		
		cpd = resource.contents.filter(CincoProduct).head
		if (cpd === null) {
			return
		}

		monitor = new NullProgressMonitor
		mglProject = ProjectCreator.getProject(resource)
		
		id = if (cpd.id.nullOrEmpty) {
			'''«mglProject.name».product'''
		}
		else {
			cpd.id
		}
		
		generateProductProject
		generateProductFile
		generateBuildProperties
		
		setWindowImages
		setLinuxIcon
		setMacOSIcon
		setWindowsIcon
		generateSplashScreen
		generateAbout
		setLauncherArguments
		setBinIncludes
		product.save
		
		generatePluginXML
		generateDefaultPerspective
		mavenizeProduct
		refreshProjects
		
	}
	
	
	
	// SUBROUTINES
	
	
	
	private def void generateProductProject() {
		productProject = ProjectCreator.createProject(
			id,                                // projectName
			new ArrayList<String>,             // srcFolders
			new ArrayList<IProject>,           // referencedProjects
			new HashSet<String>,               // requiredBundles
			new ArrayList<String>,             // exportedPackages
			new ArrayList<String>(#[           // additionalNatures
				"org.eclipse.pde.PluginNature"
			]),
			monitor,                           // progressMonitor
			false                              // askIfDel
		)		
		productProject.open(monitor)
	}
	
	private def void generateProductFile() {
		
		// Create product file 
		val productFile = ProjectCreator.createFile('''«cpd.name».product''', productProject, "", monitor)
		val productModel = new WorkspaceProductModel(productFile, true)
		productModel.load

		// Set name etc.
		product = productModel.product
		product.name = cpd.name
		product.id = '''«id».id'''
		product.productId = '''«id».product'''
		product.application = "org.eclipse.ui.ide.workbench"
		product.version = if (cpd.version.nullOrEmpty) "1.0.0.qualifier" else cpd.version
		product.launcherInfo = new LauncherInfo(productModel)
		product.launcherInfo.launcherName = cpd.name.toLowerCase
		
		// Add features
		val features = new ArrayList<IProductFeature>
		var feature = new ProductFeature(productModel)
		feature.id = '''«ProjectCreator.getProjectSymbolicName(mglProject)».feature'''
		features.add(feature)
		BundleRegistry.INSTANCE.addBundle(id, false, true)
		for (featureId: usedFeatures) {
			feature = new ProductFeature(productModel)
			feature.id = featureId
			features.add(feature)
		}
		product.addFeatures(features)
		product.useFeatures = true
		
		// Add plugin configurations
		val customPluginConfigurations = #[
			newPluginConfiguration("org.apache.felix.scr",                   2),
			newPluginConfiguration("org.eclipse.core.runtime",               0),
			newPluginConfiguration("org.eclipse.equinox.common",             2),
			newPluginConfiguration("org.eclipse.equinox.event",              2),
			newPluginConfiguration("org.eclipse.equinox.simpleconfigurator", 1)
		]
		product.addPluginConfigurations(product.pluginConfigurations + customPluginConfigurations)
		
	}
	
	private def void generateBuildProperties() {
		
		// Build properties of the MGL project
		val bpFile = mglProject.getFile("build.properties")
		bp = BuildProperties.loadBuildProperties(bpFile)
		val srcFolder = mglProject.getFolder("src")
		if (srcFolder !== null && srcFolder.exists) {
			bp.appendSource("src/")
		}
		bp.save
		binIncludes = newHashSet
		
		// Build properties of the product project
		val productBPFile = productProject.getFile("build.properties")
		val productBP = BuildProperties.loadBuildProperties(productBPFile)
		productBP.appendBinIncludes("plugin.xml")
		productBP.deleteEntry("source..")
		
		// Plugin customization
		val iniFileName = "plugin_customization.ini"
		val iniFileContent = PluginCustomization.customizeProject("org.eclipse.ui.resourcePerspective")
		ProjectCreator.createFile(iniFileName, productProject, iniFileContent, monitor)
		productBP.appendBinIncludes(iniFileName)
		productBP.save
		
	}
		
	private def void generateDefaultPerspective() {
		val cpdFile = cpd.file
		val perspectiveContent = DefaultPerspectiveContent.generateDefaultPerspective(cpd, cpdFile)
		val packageName = '''«cpdFile.project.name».perspective'''
		val fileName = '''«cpd.name»Perspective.java'''
		ContentWriter.writeJavaFileInSrcGen(cpd.project, packageName, fileName, perspectiveContent)
		ProjectCreator.exportPackage(cpd.project, packageName)
	}
	
	private def void setWindowImages() {
		setWindowImage(0,  16, cpd.image16)
		setWindowImage(1,  32, cpd.image32)
		setWindowImage(2,  48, cpd.image48)
		setWindowImage(3,  64, cpd.image64)
		setWindowImage(4, 128, cpd.image128)
		setWindowImage(5, 256, cpd.image256)
	}
	
	private def void setWindowImage(int index, int size, String imagePath) {
		if (imagePath.nullOrEmpty) {
			return
		}
		if (product.windowImages === null) {
			product.windowImages = new WindowImages(product.model)
		}
		val filePath = addFileToMGLProject(imagePath, "icons/branding", '''icon_«size».png''', false)
		if (!filePath.nullOrEmpty) {
			product.windowImages.setImagePath(filePath, index)
		}
	}
	
	private def void setLinuxIcon() {
		if (cpd.linuxIcon.nullOrEmpty) {
			return
		}
		val filePath = addFileToMGLProject(cpd.linuxIcon, "icons/branding", "icon_linux.xpm", false)
		if (!filePath.nullOrEmpty) {
			product.launcherInfo.setIconPath(LauncherInfo.LINUX_ICON, filePath)
		}
	}
	
	private def void setMacOSIcon() {
		if (cpd.macOSIcon.nullOrEmpty) {
			return
		}
		val filePath = addFileToMGLProject(cpd.macOSIcon, "icons/branding", "icon_macos.icns", false)
		if (!filePath.nullOrEmpty) {
			product.launcherInfo.setIconPath(LauncherInfo.MACOSX_ICON, filePath)
		}
	}
	
	private def void setWindowsIcon() {
		if (cpd.windowsIcon.nullOrEmpty) {
			return
		}
		val filePath = addFileToMGLProject(cpd.windowsIcon, "icons/branding", "icon_windows.ico", false)
		if (!filePath.nullOrEmpty) {
			product.launcherInfo.useWinIcoFile = true
			product.launcherInfo.setIconPath(LauncherInfo.P_ICO_PATH, filePath)
		}
	}
	
	private def void generateSplashScreen() {
		
		val splashScreen = cpd.splashScreen
		if (splashScreen?.path.nullOrEmpty) {
			return
		}
		
		val filePath = addFileToMGLProject(splashScreen.path, null, "splash.bmp", true)
		if (filePath.nullOrEmpty) {
			return
		}
		
		val splashInfo = new SplashInfo(product.model)
		product.splashInfo = splashInfo
		splashInfo.setLocation(mglProject.name, true)
		
		if (splashScreen.addProgressBar) {
			splashInfo.addProgressBar(true, true)
			val geo = #[
				splashScreen.pbXOffset,
				splashScreen.pbYOffset,
				splashScreen.pbWidth,
				splashScreen.pbHeight
			]
			splashInfo.setProgressGeometry(geo, true)
		}
		
		if (splashScreen.addProgressMessage) {
			splashInfo.addProgressMessage(true, true)
			val geo = #[
				splashScreen.pmXOffset,
				splashScreen.pmYOffset,
				splashScreen.pmWidth,
				splashScreen.pmHeight
			]
			splashInfo.setMessageGeometry(geo, true)
			if (splashScreen.textColor !== null) {
				val color = splashScreen.textColor.colorString
				splashInfo.setForegroundColor(color, true)
			}
		}
		
	}
	
	private def void generateAbout() {
		
		if (cpd.about === null) {
			return
		}
		
		val aboutInfo = new AboutInfo(product.model)
		product.aboutInfo = aboutInfo
		aboutInfo.text = cpd.about.aboutText
		
		if (cpd.about.imagePath.nullOrEmpty) {
			return
		}
		
		val imagePath = addFileToMGLProject(cpd.about.imagePath, "icons/branding", "about.png", false)
		
		if (!imagePath.nullOrEmpty) {
			aboutInfo.setImagePath(imagePath)
		}
		
	}
	
	private def void setLauncherArguments() {
		
		val arguments = new ArgumentsInfo(product.model)
		product.launcherArguments = arguments
		
		// Add default perspective
		val defaultPerspective = if (cpd.defaultPerspective.nullOrEmpty) {
			'''-perspective «mglProject.name».«cpd.name.toLowerCase»perspective'''
		}
		else {
			'''-perspective «cpd.defaultPerspective»'''
		}
		arguments.setProgramArguments(defaultPerspective, 0)
		
		
		// Add VM arguments
		val vmArguments = "-Xms40m -Xmx3072m -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -Dfile.encoding=UTF-8"
		val programArgumentsLinux = "--launcher.GTK_version 3"
		val linux = IArgumentsInfo.L_ARGS_LINUX
		arguments.setVMArguments(arguments.getVMArguments(0) + vmArguments, 0)
		arguments.setProgramArguments(arguments.getProgramArguments(linux) + programArgumentsLinux, linux)
		
	}
	
	private def void setBinIncludes() {
		binIncludes.forEach [ binInclude |
			bp.appendBinIncludes(binInclude)
		]
		bp.save
	}
	
	private def void generatePluginXML() {
		val windowImages = (0..5)
			.map [ i | product.windowImages?.getImagePath(i) ]
			.reject[nullOrEmpty]
			.map [ wi | '''platform:/plugin«wi»''' ]
			.join(",")
		val content = '''
			<?xml version="1.0" encoding="UTF-8"?>
			<?eclipse version="3.4"?>
			<plugin>
				<extension
					id="product"
					point="org.eclipse.core.runtime.products">
					<product
						application="org.eclipse.ui.ide.workbench"
						name="«cpd.name»">
						<property
							name="appName"
							value="«cpd.name»">
						</property>
						<property
							name="preferenceCustomization"
							value="plugin_customization.ini">
						</property>
						«IF !windowImages.nullOrEmpty»
							<property
								name="windowImages"
								value="«windowImages»">
							</property>
						«ENDIF»
						«IF !product.aboutInfo?.imagePath.nullOrEmpty»
							<property
								name="aboutImage"
								value="platform:/plugin«product.aboutInfo.imagePath»">
							</property>
						«ENDIF»
						«IF !product.aboutInfo?.text.nullOrEmpty»
							<property
								name="aboutText"
								value="«product.aboutInfo.text»">
							</property>
						«ENDIF»
						«IF !product.splashInfo?.foregroundColor.nullOrEmpty»
							<property
								name="startupForegroundColor"
								value="«product.splashInfo.foregroundColor»">
							</property>
						«ENDIF»
						«IF !product.splashInfo?.messageGeometry.nullOrEmpty»
							<property
								name="startupMessageRect"
								value="«product.splashInfo.messageGeometry.join(",")»">
							</property>
						«ENDIF»
						«IF !product.splashInfo?.progressGeometry.nullOrEmpty»
							<property
								name="startupProgressRect"
								value="«product.splashInfo.progressGeometry.join(",")»">
							</property>
						«ENDIF»
					</product>
				</extension>
			</plugin>
		'''
		ProjectCreator.createFile("plugin.xml", productProject, content.toString, monitor)
	}
	

	
	private def void mavenizeProduct() {
		
		// Create pom.xml
		val productPOMContent = ProductProjectPOMTemplate.content(cpd, mglProject.name).toString
		val productPOMContentStream = new StringInputStream(productPOMContent)
		val productPOMFile = productProject.getFile("pom.xml")
		if (productPOMFile.exists) {
			productPOMFile.setContents(productPOMContentStream, false, false, monitor)
		}
		else {
			productPOMFile.create(productPOMContentStream, false, monitor)
		}
		
		// Prepare workspace
		val workspaceDirectory = productProject.workspace.root.location.toFile
		val pathSeparator = System.getProperty("file.separator")
		
		// Create extensions.xml
		val mvnPath = '''«workspaceDirectory.absolutePath»«pathSeparator».mvn«pathSeparator»'''.toString
		try {
			new File(mvnPath).mkdirs
		}
		catch(Exception e) {
			
		}
		val extensionsPath = '''«mvnPath»extensions.xml'''.toString
		val extensionsFile = new File(extensionsPath)
		if (extensionsFile.createNewFile) {
			val extensionsContent = ExtensionTemplate.content
			FileUtils.write(extensionsFile, extensionsContent, StandardCharsets.UTF_8)	
		}
		
		// Create parent pom.xml
		val parentPOMPath = '''«workspaceDirectory.absolutePath»«pathSeparator»pom.xml'''.toString
		val parentPOMFile = new File(parentPOMPath)
		parentPOMFile.createNewFile
		val parentPOMContent = ParentPOMTemplate.content(cpd, mglProject.name)
		FileUtils.write(parentPOMFile, parentPOMContent, StandardCharsets.UTF_8)
		
	}
	
	private def void refreshProjects() {
		mglProject.refreshLocal(IProject.DEPTH_INFINITE, monitor)
		productProject.refreshLocal(IProject.DEPTH_INFINITE, monitor)
	}
	
	
	
	// HELPER METHODS
	

	
	/**
	 * Writes the model of the given {@link IProduct} to its file. 
	 */
	private def void save(IProduct iProduct) {
		(iProduct.model as WorkspaceProductModel).save
	}
	
	/**
	 * Writes the given {@link BuildProperties} to its file. 
	 */
	private def void save(BuildProperties buildProperties) {
		buildProperties.store(buildProperties.file, monitor)
	}
	
	/**
	 * Returns a string with the hex representation of this color.
	 * <p>
	 * Each primary color is represented with exactly 2 hexadecimal digits
	 * (upper case).<br>
	 * {@code Color(r = 127, g = 0, b = 255)} --> {@code "7F00FF"}
	 */
	private def colorString(Color color) {
		String.format("%02X%02X%02X", color.r, color.g, color.b).toUpperCase
	}
	
	/**
	 * Adds the file from {@code sourceFilePath} to the {@linkplain #mglProject
	 * MGL project}.
	 * <p>
	 * If the {@code sourceFilePath} is outside of the {@linkplain #mglProject
	 * MGL project}, the file will be copied to {@code targetFolderPath}.
	 * <p>
	 * If the {@code sourceFilePath} is in the {@linkplain #mglProject MGL
	 * project}, the file will only be copied if {@code forceCopy} is {@code
	 * true}.
	 * <p>
	 * In both cases the file will be in the {@linkplain #bp build properties}'
	 * "bin includes" list. Preferably the file's parent folder will be added.
	 * If the parent folder is the project folder, the file will be added
	 * directly to the list instead.
	 * @param sourceFilePath_   The (absolute or project-relative) path to the
	 *                          file, that will be added. Single or double quotes
	 *                          will be stripped.
	 * @param targetFolderPath_ The path to the folder, where the file will be
	 *                          copied to. {@code null} and the empty String
	 *                          represent the base folder of the MGL project.
	 * @param targetFileName_   The new name for copied file. If the new name
	 *                          {@code null} or empty, the original name of the
	 *                          source file will be used.
	 * @param forceCopy         Force copying the source file to the target folder,
	 *                          even if the source file is already located in the
	 *                          MGL project.
	 * @return The workspace-relative path to the added file (e. g. `{@code
	 *         /project-name/target-folder-path/target-file-name.png}`). May be
	 *         {@code null} if the source file could not be found or copied.
	 */
	private def String addFileToMGLProject(String sourceFilePath_, String targetFolderPath_, String targetFileName_, boolean forceCopy) {
		
		// Strip off quotes
		val sourceFilePathString   = sourceFilePath_.stripOffQuotes
		var targetFolderPathString = targetFolderPath_.stripOffQuotes?: ""
		var targetFileName         = targetFileName_.stripOffQuotes
		
		// Check source file
		if (sourceFilePathString.nullOrEmpty) {
			System.err.println('''«class.simpleName».addFileToMGLProject(...): sourceFilePathString is null or empty''')
			return null
		}
		val externalSourceFile = new File(sourceFilePathString)
		val internalSourceFile = try mglProject.getFile(sourceFilePathString)
			catch (IllegalArgumentException e) {
				System.err.println('''«class.simpleName».addFileToMGLProject(...): Cannot check file «sourceFilePathString»''')
				e.printStackTrace
				return null
			}
		
		// Discern whether the source file is internal or external
		var File sourceFile
		var copy = false
		if (internalSourceFile.exists) {
			// File is inside of MGL project -> Only copy if forced 
			sourceFile = internalSourceFile.location.toFile
			copy = forceCopy
		}
		else if (externalSourceFile.exists) {
			// File is outside of MGL project -> Always copy
			sourceFile = externalSourceFile
			copy = true
		}
		else {
			System.err.println('''«class.simpleName».addFileToMGLProject(...): Cannot find file «sourceFilePathString»''')
			return null
		}
		
		// Copy
		var IPath targetFilePath
		if (copy) {
			val targetFolderPath = mglProject.location.append(targetFolderPathString)
			val targetFolder = targetFolderPath.toFile
			if (!targetFolder.exists && !targetFolder.mkdirs) {
				System.err.println('''«class.simpleName».copyFile(...): Cannot create target folder «targetFolderPathString»''')
				return null
			}
			targetFileName = if (targetFileName.nullOrEmpty) sourceFile.name else targetFileName
			targetFilePath = targetFolderPath.append(targetFileName)
			val targetFile = targetFilePath.toFile
			try {
				val sourceChannel = new FileInputStream(sourceFile).channel
				val targetChannel = new FileOutputStream(targetFile).channel
				targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size)
			}
			catch (Exception e) {
				System.err.println('''«class.simpleName».copyFile(...): Cannot copy file «sourceFilePathString» to «targetFolderPathString»''')
				e.printStackTrace
				return null
			}
			targetFilePath = targetFilePath.makeRelativeTo(mglProject.workspace.root.location).makeAbsolute
		}
		else {
			targetFilePath = internalSourceFile.fullPath
		}
			
		// Add to build properties
		var String targetFilePathString
		if (targetFolderPathString.nullOrEmpty) {
			// Target folder is the project folder
			// -> Add file directly to "bin includes"
			targetFilePathString = targetFileName
		}
		else {
			// Add parent folder to "bin includes"
			targetFilePathString = targetFilePath
				.removeFirstSegments(1) // Remove project prefix
				.removeLastSegments(1)  // Remove file
				.addTrailingSeparator
				.toPortableString
		}
		binIncludes.add(targetFilePathString)
			
		// Return path to the target file
		return targetFilePath.toPortableString
				
	}
	

	
	/**
	 * Creates a new {@link CincoPluginConfiguration} with the the given
	 * {@code id} and {@code startLevel}. Auto-start will be enabled.
	 */
	private def newPluginConfiguration(String id, int startLevel) {
		val config = new CincoPluginConfiguration(product.model)
		config.id         = id
		config.autoStart  = true
		config.startLevel = startLevel
		return config
	}
	
	/**
	 * Returns a list of all internally needed features, as well as features
	 * defined in the CPD.
	 */
	private def List<String> getUsedFeatures() {
		val defaultFeatures = #[
			"org.eclipse.e4.rcp",
			"org.eclipse.e4.rcp.source",
			"org.eclipse.ecf.core.feature",
			"org.eclipse.ecf.core.feature.source",
			"org.eclipse.ecf.core.ssl.feature",
			"org.eclipse.ecf.core.ssl.feature.source",
			"org.eclipse.ecf.filetransfer.feature",
			"org.eclipse.ecf.filetransfer.feature.source",
			"org.eclipse.ecf.filetransfer.httpclient45.feature",
			"org.eclipse.ecf.filetransfer.httpclient45.feature.source",
			"org.eclipse.ecf.filetransfer.ssl.feature",
			"org.eclipse.ecf.filetransfer.ssl.feature.source",
			"org.eclipse.egit",
			"org.eclipse.emf",
			"org.eclipse.emf.codegen",
			"org.eclipse.emf.codegen.ecore",
			"org.eclipse.emf.codegen.ecore.source",
			"org.eclipse.emf.codegen.ecore.ui",
			"org.eclipse.emf.codegen.ecore.ui.source",
			"org.eclipse.emf.codegen.source",
			"org.eclipse.emf.codegen.ui",
			"org.eclipse.emf.codegen.ui.source",
			"org.eclipse.emf.common",
			"org.eclipse.emf.common.source",
			"org.eclipse.emf.common.ui",
			"org.eclipse.emf.common.ui.source",
			"org.eclipse.emf.converter",
			"org.eclipse.emf.converter.source",
			"org.eclipse.emf.databinding",
			"org.eclipse.emf.databinding.edit",
			"org.eclipse.emf.databinding.edit.source",
			"org.eclipse.emf.databinding.source",
			"org.eclipse.emf.doc",
			"org.eclipse.emf.ecore",
			"org.eclipse.emf.ecore.edit",
			"org.eclipse.emf.ecore.edit.source",
			"org.eclipse.emf.ecore.editor",
			"org.eclipse.emf.ecore.editor.source",
			"org.eclipse.emf.ecore.source",
			"org.eclipse.emf.edit",
			"org.eclipse.emf.edit.source",
			"org.eclipse.emf.edit.ui",
			"org.eclipse.emf.edit.ui.source",
			"org.eclipse.emf.mapping",
			"org.eclipse.emf.mapping.ecore",
			"org.eclipse.emf.mapping.ecore.editor",
			"org.eclipse.emf.mapping.ecore.editor.source",
			"org.eclipse.emf.mapping.ecore.source",
			"org.eclipse.emf.mapping.source",
			"org.eclipse.emf.mapping.ui",
			"org.eclipse.emf.mapping.ui.source",
			"org.eclipse.emf.mwe2.language.sdk",
			"org.eclipse.emf.mwe2.launcher",
			"org.eclipse.emf.mwe2.launcher.source",
			"org.eclipse.emf.mwe2.runtime.sdk",
			"org.eclipse.emf.sdk",
			"org.eclipse.emf.source",
			"org.eclipse.equinox.p2.core.feature",
			"org.eclipse.equinox.p2.core.feature.source",
			"org.eclipse.equinox.p2.extras.feature",
			"org.eclipse.equinox.p2.extras.feature.source",
			"org.eclipse.equinox.p2.rcp.feature",
			"org.eclipse.equinox.p2.rcp.feature.source",
			"org.eclipse.equinox.p2.user.ui",
			"org.eclipse.equinox.p2.user.ui.source",
			"org.eclipse.graphiti.feature",
			"org.eclipse.graphiti.feature.examples",
			"org.eclipse.graphiti.feature.examples.source",
			"org.eclipse.graphiti.feature.tools",
			"org.eclipse.graphiti.feature.tools.source",
			"org.eclipse.graphiti.sdk.feature",
			"org.eclipse.help",
			"org.eclipse.help.source",
			"org.eclipse.jdt",
			"org.eclipse.jdt.source",
			"org.eclipse.jgit",
			"org.eclipse.jgit.gpg.bc",
			"org.eclipse.jgit.http.apache",
			"org.eclipse.jgit.ssh.apache",
			"org.eclipse.jgit.ssh.jsch",
			"org.eclipse.pde",
			"org.eclipse.pde.source",
			"org.eclipse.platform",
			"org.eclipse.platform.source",
			"org.eclipse.rcp",
			"org.eclipse.rcp.source",
			"org.eclipse.sdk",
			"org.eclipse.xtend.sdk",
			"org.eclipse.xtext.docs",
			"org.eclipse.xtext.examples",
			"org.eclipse.xtext.redist",
			"org.eclipse.xtext.runtime",
			"org.eclipse.xtext.sdk",
			"org.eclipse.xtext.ui",
			"org.eclipse.xtext.xbase",
			"org.eclipse.xtext.xbase.lib",
			"org.eclipse.xtext.xtext.ui",
			"org.eclipse.xtext.xtext.ui.graph"
		]
		val productFeatures = cpd.features.map[stripOffQuotes]
		return (defaultFeatures + productFeatures).toList
	}
	
}

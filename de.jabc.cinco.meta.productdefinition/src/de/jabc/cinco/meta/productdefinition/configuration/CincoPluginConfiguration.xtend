/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.configuration

import java.io.PrintWriter
import org.eclipse.pde.internal.core.iproduct.IPluginConfiguration
import org.eclipse.pde.internal.core.iproduct.IProductModel
import org.eclipse.pde.internal.core.product.ProductObject
import org.w3c.dom.Element
import org.w3c.dom.Node

class CincoPluginConfiguration extends ProductObject implements IPluginConfiguration {
	
	IProductModel cModel
	
	String cID
	
	int cStartLevel
	
	boolean cAutoStart
	
	new(IProductModel model) {
		super(model)
		cModel = model
		
	}
	
	override getId() {
		cID
	}
	
	override getStartLevel() {
		cStartLevel
	}
	
	override isAutoStart() {
		cAutoStart
	}
	
	override setAutoStart(boolean autostart) {
		cAutoStart= autostart
	}
	
	override setId(String id) {
		cID = id
	}
	
	override setStartLevel(int startLevel) {
		cStartLevel = startLevel
	}
	
	override getModel() {
		cModel
	}
	
	override getProduct() {
		cModel.product
	}
	
	override setModel(IProductModel model) {
		cModel = model
	}
	
	override parse(Node it) {
		if (nodeType == Node.ELEMENT_NODE) {
			val element = it as Element;
			cID = element.getAttribute("id");
			cAutoStart = Boolean.parseBoolean(element.getAttribute(P_AUTO_START));
			cStartLevel = Integer.parseInt(element.getAttribute(P_START_LEVEL));
		}

	}

	override write(String indent, PrintWriter writer) {
		writer.println(xmlFragment(indent))
	}
	
	def xmlFragment(String indent)'''
	«indent»<plugin id="«cID»" autoStart="«cAutoStart»" startLevel="«cStartLevel»" /> 
	'''
	
	
}
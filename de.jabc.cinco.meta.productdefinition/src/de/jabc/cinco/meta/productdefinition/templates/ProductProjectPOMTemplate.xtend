/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.templates

import productDefinition.CincoProduct
class ProductProjectPOMTemplate {
	def static content(CincoProduct cpd,String cincoProductID)'''
	<!-- GENERATED POM NO CHANGES REQUIRED BELOW THIS LINE -->
	<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	  <modelVersion>4.0.0</modelVersion>
	  <artifactId>«cincoProductID».product</artifactId>
	  <parent>
	    <groupId>«cincoProductID»</groupId>
	    <artifactId>«cincoProductID».parent</artifactId>
	    <version>1.0.0-SNAPSHOT</version>
	    <relativePath>../pom.xml</relativePath>
	  </parent>
	  <packaging>eclipse-repository</packaging>
	  <properties>
	    <build.version>${unqualifiedVersion}.${buildQualifier}</build.version>
	    <cincoproduct.name>«cpd.name»</cincoproduct.name>
	    <cincoproduct.id>«cincoProductID».product.id</cincoproduct.id>
	    <cincoproduct.regexp>^(«cincoProductID»-)(.*)$$</cincoproduct.regexp>
	  </properties>
	
	  <build>
	    <plugins>
	        <plugin>
			  <groupId>org.codehaus.mojo</groupId>
			  <artifactId>build-helper-maven-plugin</artifactId>
			  <version>1.9</version>
			  <executions>
				 <execution>
					<id>regex-property</id>
					<goals>
					  <goal>regex-property</goal>
					</goals>
					<configuration>
					  <name>build.version.clean</name>
					  <value>${build.version}</value>
					  <regex>\.0</regex>
					  <replacement></replacement>
					  <failIfNoMatch>false</failIfNoMatch>
					</configuration>
				 </execution>
			  </executions>
			</plugin>
	
	      <plugin>
	        <groupId>org.eclipse.tycho</groupId>
	        <artifactId>tycho-p2-repository-plugin</artifactId>
	        <version>${tycho.version}</version>
	        <configuration>
	          <includeAllDependencies>true</includeAllDependencies>
	        </configuration>
	      </plugin>
	      <plugin>
	        <groupId>org.eclipse.tycho</groupId>
	        <artifactId>tycho-p2-director-plugin</artifactId>
	        <version>${tycho.version}</version>
	        <executions>
	          <execution>
	            <id>materialize-products</id>
	            <goals>
	              <goal>materialize-products</goal>
	            </goals>
	          </execution>
	          <execution>
	            <id>archive-products</id>
	            <goals>
	              <goal>archive-products</goal>
	            </goals>
	          </execution>
	        </executions>
	        <configuration>
	          <products>
	            <product>
	              <id>${cincoproduct.id}</id>
	              <rootFolder>${cincoproduct.name}-${build.version.clean}</rootFolder>
	            </product>
	          </products>
	        </configuration>
	      </plugin>
	      <!-- workaround for naming the zip file. there seems to be no easier way... see http://stackoverflow.com/a/8584420 -->
	      <plugin>
	        <artifactId>maven-antrun-plugin</artifactId>
	        <version>${antrun.version}</version>
	        <executions>
	          <!-- Rename the ZIP files -->
	          <execution>
	            <id>update-zip-files</id>
	            <phase>package</phase>
	            <configuration>
	              <target>
	                <!-- Rename the products -->
	                <move verbose="true" todir="${project.build.directory}/products">
	                  <mapper type="regexp" from="${cincoproduct.regexp}" to="${cincoproduct.name}-${build.version.clean}-\2" />
	                  <fileset dir="${project.build.directory}/products">
	                    <include name="*.zip" />
	                  </fileset>
	                </move>
	              </target>
	            </configuration>
	            <goals>
	              <goal>run</goal>
	            </goals>
	          </execution>
	        </executions>
	      </plugin>
	    </plugins>
	  </build>
	</project> 
	
	'''
}

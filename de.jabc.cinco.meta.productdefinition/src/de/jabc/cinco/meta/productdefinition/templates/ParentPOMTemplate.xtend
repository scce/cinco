/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.templates

import productDefinition.CincoProduct

import static de.jabc.cinco.meta.productdefinition.templates.TemplateExtensions.*

class ParentPOMTemplate {
	def static content(CincoProduct cpd,String cincoProductID)'''
		<?xml version="1.0" encoding="UTF-8"?>
		<!-- GENERATED POM NO CHANGES REQUIRED BELOW THIS LINE -->
		<project>
		  <name>«cpd.name»</name> <!-- EDIT THIS LINE -->
		  <description>«cpd.about»</description> <!-- EDIT THIS LINE -->
		  <modelVersion>4.0.0</modelVersion>
		  <groupId>«cincoProductID»</groupId> <!-- EDIT THIS LINE -->
		  <artifactId>«cincoProductID».parent</artifactId> <!-- EDIT THIS LINE -->
		  <version>1.0.0-SNAPSHOT</version>
		  <packaging>pom</packaging>
		  <modules> <!-- EDIT THESE LINES AND ADD MODULES IF NECESSARY -->
		    «modules»
		  </modules>
		
		  <!-- NO CHANGES REQUIRED BELOW THIS LINE -->

		<properties>
		    <tycho.version>2.1.0</tycho.version>
		    <xtext.version>2.22.0</xtext.version>
		    <build-helper.version>1.8</build-helper.version>
		    <antrun.version>1.8</antrun.version>
		    <clean-plugin.version>2.5</clean-plugin.version>
		    <wagon.version>1.0</wagon.version>
		    <cinco.snapshot-version>2.1.0-SNAPSHOT</cinco.snapshot-version>
		    <mwe2.launch.version>2.12.0.M2</mwe2.launch.version>
		    <photon.url>https://download.eclipse.org/releases/2020-06/</photon.url>
		  </properties>
		 <repositories>
		   <repository>
		    <id>photon</id>
		    <url>${photon.url}</url>
		    <layout>p2</layout>
			</repository>
			<repository>
				<id>cinco-meta-snapshot</id>
				<url>https://ls5download.cs.tu-dortmund.de/repository/cinco-meta-snapshot</url>
				<layout>default</layout>
				<snapshots>
					<enabled>true</enabled>
				</snapshots>
			</repository>
	 	 </repositories>
	
		<dependencies>
		
		    <dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.branding</artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.branding</artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.branding</artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.event </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.event </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.event </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.fragment </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.fragment </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.fragment </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.generator.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.generator.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.generator.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ge.style.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.jabcprojectgenerator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.jabcprojectgenerator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.jabcprojectgenerator </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.mgl.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.pluginregistry </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.pluginregistry </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.pluginregistry </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.referenceregistry </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.referenceregistry </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.referenceregistry </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.utils </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.utils </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.utils </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.wizards </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.wizards </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.core.wizards </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.feature </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.feature </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.feature </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.feature </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>root.gtk.linux.x86_64</classifier>
			 <type>zip</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.feature </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>root</classifier>
			 <type>zip</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.headless </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.headless </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.headless </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.libraries  </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.libraries  </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.libraries  </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.behavior </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.behavior </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.behavior </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.behavior.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.behavior.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.behavior.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.event.api </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.event.api </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.event.api </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.event.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.event.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.event.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.executer </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.executer </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.executer </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.generator </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.generator.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.generator.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.generator.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext.build </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext.build </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext.build </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.gratext.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.mcam </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.mcam </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.mcam </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.mcam.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.mcam.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.mcam.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.modelchecking </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.modelchecking </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.modelchecking </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.modelchecking.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.modelchecking.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.modelchecking.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.primeviewer </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.primeviewer </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.primeviewer </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.startup </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.startup </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.plugin.startup </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.model </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.sdk </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.productdefinition.ui </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.runtime </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.util </artifactId>
		    <version>${cinco.snapshot-version}</version>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.util </artifactId>
		    <version>${cinco.snapshot-version}</version>
		    <classifier>p2artifacts</classifier>
		    <type>xml</type>
		</dependency>
		<dependency>
		    <groupId>de.jabc.cinco</groupId>
		    <artifactId>de.jabc.cinco.meta.util </artifactId>
		    <version>${cinco.snapshot-version}</version>
			 <classifier>p2metadata</classifier>
			 <type>xml</type>
		</dependency>
		</dependencies>
	    <build>
	      <plugins>
	       <plugin>
	        <groupId>org.eclipse.tycho</groupId>
	        <artifactId>tycho-maven-plugin</artifactId>
	        <version>${tycho.version}</version>
	        <extensions>true</extensions>
	       </plugin>
	      <plugin>
	       <groupId>org.eclipse.tycho</groupId>
	       <artifactId>tycho-versions-plugin</artifactId>
	       <version>${tycho.version}</version>
	      </plugin>
	      <plugin>
	       <groupId>org.eclipse.tycho</groupId>
	       <artifactId>target-platform-configuration</artifactId>
		<version>${tycho.version}</version>
	       <configuration>
	        <pomDependencies>consider</pomDependencies>
	        <environments>
		<!--
	           <environment>
	             <os>linux</os>
	             <ws>gtk</ws>
	             <arch>x86</arch>
	           </environment>
		-->
	          <environment>
	            <os>linux</os>
	            <ws>gtk</ws>
	            <arch>x86_64</arch>
	          </environment>
		<!--
	           <environment>
	             <os>win32</os>
	             <ws>win32</ws>
	             <arch>x86</arch>
	           </environment>
		-->
	          <environment>
	            <os>win32</os>
	            <ws>win32</ws>
	            <arch>x86_64</arch>
	          </environment>
	          <environment>
	            <os>macosx</os>
	            <ws>cocoa</ws>
	            <arch>x86_64</arch>
	          </environment>
	        </environments>
	       </configuration>
	     </plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>build-helper-maven-plugin</artifactId>
				<version>${build-helper.version}</version>
				<executions>
					<execution>
						<id>add-xtend</id>
						<phase>generate-sources</phase>
						<goals>
							<goal>add-source</goal>
						</goals>
						<configuration>
							<sources>
								<source>${project.basedir}/xtend-gen</source>
							</sources>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.eclipse.xtend</groupId>
				<artifactId>xtend-maven-plugin</artifactId>
				<version>${xtext.version}</version>
				<executions>
					<execution>
						<id>default-compile</id>
						<goals>
							<goal>compile</goal>
							<goal>testCompile</goal>
						</goals>
					</execution>
				</executions>
				<dependencies>
					<dependency>
						<groupId>org.eclipse.platform</groupId>
						<artifactId>org.eclipse.equinox.common</artifactId>
						<version>3.10.0</version>
					</dependency>
				</dependencies>
				<configuration>
					<outputDirectory>${basedir}/xtend-gen</outputDirectory>
					<testOutputDirectory>${basedir}/xtend-gen</testOutputDirectory>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-clean-plugin</artifactId>
				<version>${clean-plugin.version}</version>
				<configuration>
					<filesets combine.children="append">
						<fileset>
							<directory>${project.basedir}/xtend-gen/</directory>
							<includes>
								<include>**/*</include>
							</includes>
						</fileset>
						<fileset>
							<directory>${project.basedir}/workspace-emf-tmp</directory>
						</fileset>
						<fileset>
							<directory>${project.basedir}/workspace</directory>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
	        </plugins>
		<extensions>
			<extension>
	        		<groupId>org.apache.maven.wagon</groupId>
	        		<artifactId>wagon-ssh-external</artifactId>
	        		<version>${wagon.version}</version>
		      </extension>
		</extensions>
	      </build>
	      <profiles>
		<profile>
			<id>linux64</id>
			<build>
				    <plugins>
				      <plugin>
				       <groupId>org.eclipse.tycho</groupId>
				       <artifactId>target-platform-configuration</artifactId>
				       <configuration>
					 <environments>
					   <environment>
					     <os>linux</os>
					     <ws>gtk</ws>
					     <arch>x86_64</arch>
					   </environment>
					 </environments>
				       </configuration>
				     </plugin>
				   </plugins>
			</build>
		</profile>
		<profile>
			<id>linux</id>
			<build>
				    <plugins>
				      <plugin>
				       <groupId>org.eclipse.tycho</groupId>
				       <artifactId>target-platform-configuration</artifactId>
				       <configuration>
					 <environments>
					   <environment>
					     <os>linux</os>
					     <ws>gtk</ws>
					     <arch>x86</arch>
					   </environment>
					 </environments>
				       </configuration>
				     </plugin>
				   </plugins>
			</build>
		</profile>
		<profile>
			<id>macosx</id>
			<build>
				    <plugins>
				      <plugin>
				       <groupId>org.eclipse.tycho</groupId>
				       <artifactId>target-platform-configuration</artifactId>
				       <configuration>
					 <environments>
					   <environment>
					     <os>macosx</os>
					     <ws>cocoa</ws>
					     <arch>x86_64</arch>
					   </environment>
					 </environments>
				       </configuration>
				     </plugin>
				   </plugins>
			</build>
		</profile>
		<profile>
			<id>win64</id>
			<build>
				    <plugins>
				      <plugin>
				       <groupId>org.eclipse.tycho</groupId>
				       <artifactId>target-platform-configuration</artifactId>
				       <configuration>
					 <environments>
					   <environment>
					     <os>win32</os>
					     <ws>win32</ws>
					     <arch>x86_64</arch>
					   </environment>
					 </environments>
				       </configuration>
				     </plugin>
				   </plugins>
			</build>
		</profile>
	</profiles>
	
	  <scm>
	    <connection>https://projekte.itmc.tu-dortmund.de/svn/cinco/trunk/</connection>
	    <developerConnection>https://projekte.itmc.tu-dortmund.de/svn/cinco/trunk/</developerConnection>
	    <url>https://projekte.itmc.tu-dortmund.de/projects/cinco</url>
	  </scm>
	
	</project>
	'''
	
}

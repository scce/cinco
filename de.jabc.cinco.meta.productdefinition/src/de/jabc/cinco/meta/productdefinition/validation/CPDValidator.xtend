/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.validation

import de.jabc.cinco.meta.util.xapi.FileExtension
import java.io.File
import mgl.MGLModel
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtend.lib.annotations.Data
import org.eclipse.xtext.validation.Check
import productDefinition.About
import productDefinition.CincoProduct
import productDefinition.Color
import productDefinition.MGLDescriptor
import productDefinition.SplashScreen

import static productDefinition.ProductDefinitionPackage.Literals.*

import static extension de.jabc.cinco.meta.core.utils.PathValidator.getRelativePath
import static extension de.jabc.cinco.meta.core.utils.projects.ProjectCreator.getProject
import static extension de.jabc.cinco.meta.core.utils.projects.ContentWriter.stripOffQuotes
import static extension java.util.Collections.frequency

/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class CPDValidator extends AbstractCPDValidator {
	
	extension FileExtension = new FileExtension
	
	@Check
	def void checkColor(Color it) {
		if (r < 0 || r > 255) error("Value for red must be in the range from 0 to 255.",   COLOR__R)
		if (g < 0 || g > 255) error("Value for green must be in the range from 0 to 255.", COLOR__G)
		if (b < 0 || b > 255) error("Value for blue must be in the range from 0 to 255.",  COLOR__B)
	}
	
	@Check
	def void checkMGLs(MGLDescriptor mglDesc) {
		checkIfPathExists(mglDesc.mglPath, mglDesc, MGL_DESCRIPTOR__MGL_PATH, "an MGL file")
		checkFileExtension(mglDesc.mglPath, mglDesc, MGL_DESCRIPTOR__MGL_PATH, "mgl")
	}
	
	@Check
	def void checkWindowImages(CincoProduct cpd) {
		checkIfPathExists(cpd.image16,  cpd, CINCO_PRODUCT__IMAGE16,  "the 16x16 icon")
		checkIfPathExists(cpd.image32,  cpd, CINCO_PRODUCT__IMAGE32,  "the 32x32 icon")
		checkIfPathExists(cpd.image48,  cpd, CINCO_PRODUCT__IMAGE48,  "the 48x48 icon")
		checkIfPathExists(cpd.image64,  cpd, CINCO_PRODUCT__IMAGE64,  "the 64x64 icon")
		checkIfPathExists(cpd.image128, cpd, CINCO_PRODUCT__IMAGE128, "the 128x128 icon")
		checkIfPathExists(cpd.image256, cpd, CINCO_PRODUCT__IMAGE256, "the 256x256 icon")
	}
	
	@Check
	def void checkAboutImage(About about) {
		checkIfPathExists(about.imagePath, about, ABOUT__IMAGE_PATH, "the about screen image")
	}
	
	@Check
	def void checkSplashImage(SplashScreen splashScreen) {
		checkIfPathExists(splashScreen.path, splashScreen, SPLASH_SCREEN__PATH, "the splash screen image")
		checkFileExtension(splashScreen.path, splashScreen, SPLASH_SCREEN__PATH, "bmp")
	}
	
	@Check
	def void checkLinuxIcon(CincoProduct cpd) {
		checkIfPathExists(cpd.linuxIcon, cpd, CINCO_PRODUCT__LINUX_ICON, "the Linux icon")
		checkFileExtension(cpd.linuxIcon, cpd, CINCO_PRODUCT__LINUX_ICON, "xpm")
	}
	
	@Check
	def void checkMacOSIcon(CincoProduct cpd) {
		checkIfPathExists(cpd.macOSIcon, cpd, CINCO_PRODUCT__MAC_OS_ICON, "the macOS icon")
		checkFileExtension(cpd.macOSIcon, cpd, CINCO_PRODUCT__MAC_OS_ICON, "icns")
	}
	
	@Check
	def void checkWindowsIcon(CincoProduct cpd) {
		checkIfPathExists(cpd.windowsIcon, cpd, CINCO_PRODUCT__WINDOWS_ICON, "the Windows icon")
		checkFileExtension(cpd.windowsIcon, cpd, CINCO_PRODUCT__WINDOWS_ICON, "ico")
	}
	
	@Check
	def void checkMGLUniquenesses(CincoProduct cpd) {
		
		// Get MGL models
		val cpdProject = cpd.eResource.project
		val mgls = cpd.mgls.map [ mglDesc |
			val path = mglDesc.mglPath.getRelativePath(cpdProject)
			val file = cpdProject.getFile(path)
			return new MGLPair(mglDesc, file.getContent(MGLModel))
		]
		
		// Check for duplicate MGL IDs
		val mglIDs = mgls.map[model.package]
		for (mgl: mgls) {
			if (mglIDs.frequency(mgl.model.package) > 1) {
				error('''MGL ID "«mgl.model.package»" is not unique.''', mgl.desc, MGL_DESCRIPTOR__MGL_PATH)
			}
		}
		
		// Collect all graph models and check for duplicate meta data
		val allGraphModels = mgls.flatMap[ mgl | mgl.model.graphModels].toList
		val allGraphModelNames = allGraphModels.map[name]
		val allGraphModelFileExtensions = allGraphModels.map[fileExtension]
		for (mgl: mgls) {
			for (gm: mgl.model.graphModels) {
				// Check for duplicate graph model names
				if (allGraphModelNames.frequency(gm.name) > 1) {
					error('''Graph model name "«gm.name»" is not unique.''', mgl.desc, MGL_DESCRIPTOR__MGL_PATH)
				}
				// Check for duplicate graph model file extensions
				if (allGraphModelFileExtensions.frequency(gm.fileExtension) > 1) {
					error('''File extension "«gm.fileExtension»" for graph model "«gm.name»" is not unique.''', mgl.desc, MGL_DESCRIPTOR__MGL_PATH)
				}
			}
		}
		
	}
	
	private def void checkIfPathExists(String path, EObject eObj, EStructuralFeature eStructuralFeature, String fileDescription) {
		if (path.nullOrEmpty) {
			return
		}
		val strippedPath = path.stripOffQuotes
		if (strippedPath.nullOrEmpty) {
			val fileDesc = if (fileDescription.nullOrEmpty) "the file" else fileDescription.toFirstLower
			error('''Please enter the path to «fileDesc».''', eObj, eStructuralFeature)
		}
		else {
			val project = eObj.eResource.project
			if (!project.getFile(strippedPath).exists && !new File(strippedPath).exists) {
				error('''The file "«strippedPath»" does not exist.''', eObj, eStructuralFeature)
			}
		}
	}
	
	private def void checkFileExtension(String path, EObject eObj, EStructuralFeature eStructuralFeature, String fileExtension) {
		if (path.nullOrEmpty) {
			return
		}
		val strippedPath = path.toLowerCase?.stripOffQuotes
		val suffix = '''.«fileExtension.toLowerCase»'''
		if (strippedPath.nullOrEmpty || !strippedPath.endsWith(suffix)) {
			error('''The file must be a «fileExtension.toUpperCase» file.''', eObj, eStructuralFeature)
		}
	}
	
	@Data
	private static class MGLPair {
		val MGLDescriptor desc
		val MGLModel model
	}
	
}

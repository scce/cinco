/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.ui.contentassist

import de.jabc.cinco.meta.core.pluginregistry.PluginRegistry
import de.jabc.cinco.meta.core.utils.xtext.PickColorApplier
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.Assignment
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import productDefinition.Annotation
import productDefinition.CincoProduct

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class CPDProposalProvider extends AbstractCPDProposalProvider {
	
	override complete_Color(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		var proposal = createCompletionProposal("Pick color...", context);
		if (proposal instanceof ConfigurableCompletionProposal) {
			var configProp = proposal as ConfigurableCompletionProposal
			configProp.setTextApplier(new PickColorApplier("("))
		}
		
		acceptor.accept(proposal)
		super.complete_Color(model, ruleCall, context, acceptor)
	}
	
	override completeColor_R(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		var proposal = createCompletionProposal("Pick color...", context);
		if (proposal instanceof ConfigurableCompletionProposal) {
			var configProp = proposal as ConfigurableCompletionProposal
			configProp.setTextApplier(new PickColorApplier(""))
		}
		
		acceptor.accept(proposal)
		
		super.completeColor_R(model, assignment, context, acceptor)
		
	}
	
	override complete_Annotation(EObject model, RuleCall ruleCall, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		switch context.currentNode.semanticElement {
			CincoProduct: acceptor.accept(createCompletionProposal("@disableGratext", context))
		}
	}
	
	override completeAnnotation_Value(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (model instanceof Annotation) {
			var annot = model as Annotation
			var pluginReg = PluginRegistry::getInstance();
			var metaPlugins = pluginReg.getSuitableCPDMetaPlugins(annot.name);
			
			if(!metaPlugins.nullOrEmpty){
			
				for(mp: metaPlugins){
					var myAcceptor = mp.acceptor
					if(myAcceptor!==null){
						for(acc: myAcceptor.getAcceptedStrings(annot)){
							
							var proposal = createCompletionProposal(acc, context);
							if (proposal instanceof ConfigurableCompletionProposal) {
								var configProp = proposal as ConfigurableCompletionProposal
								configProp.setTextApplier(myAcceptor.getTextApplier(annot))
							}			
							acceptor.accept(proposal)
						}
					}
				}
			}
		}
	}
	
	override completeCincoProduct_Features(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		for(bgp:Platform.bundleGroupProviders){
		  			for(feature: bgp.bundleGroups){
		  				acceptor.accept(createCompletionProposal(feature.identifier,context))	
		  			}
		  		
		}
		var workspace = ResourcesPlugin.getWorkspace();
		for (project: workspace.root.projects){
			if(project.description.hasNature("org.eclipse.pde.FeatureNature"))
				acceptor.accept(createCompletionProposal(project.description.name,context))
		}
		
		
	}
	
	override completeCincoProduct_Plugins(EObject model, Assignment assignment, ContentAssistContext context, ICompletionProposalAcceptor acceptor){
		  var pluginModels = org.eclipse.pde.core.plugin.PluginRegistry.allModels
		  
		  for(pluginModel: pluginModels){
		  		
		  	val bundleDesription = pluginModel.bundleDescription
		  		acceptor.accept(createCompletionProposal(bundleDesription.symbolicName,context))
		  	}
		  		
	}
}

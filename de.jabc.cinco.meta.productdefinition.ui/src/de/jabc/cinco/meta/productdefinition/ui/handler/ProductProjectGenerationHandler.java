/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.productdefinition.ui.handler;

import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.OutputConfiguration;

import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.productdefinition.generator.CPDGenerator;


public class ProductProjectGenerationHandler extends AbstractHandler {
	
    private IGenerator generator;
    
    //@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {	
		 try {
			deleteGeneratedProductProject();
			callGenerator();
		} catch (Exception e) {
			throw new RuntimeException("Exception while Generating Product Project", e);
		}
		return null;
	}
	
	
	private void callGenerator() {
		IFile selectedFile = MGLSelectionListener.INSTANCE.getSelectedCPDFile();
		if(selectedFile.getFileExtension().equals("cpd")){
			IProject project = selectedFile.getProject();
			URI createPlatformResourceURI = URI.createPlatformResourceURI(selectedFile.getFullPath().toOSString(), true);
			Resource res = Resource.Factory.Registry.INSTANCE.getFactory(createPlatformResourceURI, "cpd").createResource(createPlatformResourceURI);
			try {
				res.load(null);
				EclipseResourceFileSystemAccess2 access =  new EclipseResourceFileSystemAccess2();
				access.setProject(project);
				access.setMonitor(null);
				OutputConfiguration defaultOutput = new OutputConfiguration("DEFAULT_OUTPUT");
			    defaultOutput.setOutputDirectory("./src-gen");
			    defaultOutput.setCreateOutputDirectory(true);
			    defaultOutput.setOverrideExistingResources(true);
			    defaultOutput.setCleanUpDerivedResources(true);
			    defaultOutput.setSetDerivedProperty(true);
			    defaultOutput.setCanClearOutputDirectory(true);
			    
				access.getOutputConfigurations().put("DEFAULT_OUTPUT", defaultOutput);
				generator = new CPDGenerator();
				generator.doGenerate(res, access);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			
		}
	}
	
	private void deleteGeneratedProductProject() {
		IFile selectedFile = MGLSelectionListener.INSTANCE.getSelectedCPDFile();
		if(selectedFile.getFileExtension().equals("cpd")){
			IProject project = selectedFile.getProject();
			IFolder packageFolder = project.getFolder("src-gen/" + project.getName().replace('.', '/') + "/perspective");
			if (packageFolder.exists()) {
				try {
					packageFolder.delete(org.eclipse.core.resources.IResource.FORCE, null);
				} catch (CoreException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

}

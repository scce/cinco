/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.behavior;

import mgl.MGLModel;
import mgl.Node;

public class Constants {
	public static final String PROJECT_ANNOTATION = "behavior";
	public static final String PROJECT_SUFFIX = "behave";
	public static final String HOOK_PACKAGE_SUFFIX = ".hooks";
	
	public static String getPostMoveHookClassName(Node node){
		return node.getName() + "PostMoveHook";
	}
	public static String getPostCreateHookClassName(Node node){
		return node.getName() + "PostCreateHook";
	}
	public static String projectPackage(Node node){
		return getPackage(node) + "." + Constants.PROJECT_SUFFIX + HOOK_PACKAGE_SUFFIX;
	}
	private static String getPackage(Node node) {
		((MGLModel)node.eContainer()).getPackage();
		return null;
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
// Unit tests: de.jabc.cinco.meta.plugin.event.generator.test.ImportManagerTest

package de.jabc.cinco.meta.plugin.event.generator.util

import de.jabc.cinco.meta.plugin.event.api.util.Fqn
import java.lang.reflect.Type
import java.util.List
import java.util.Map

/**
 * An extension class for managing imports in code generating classes.
 * <p>
 * Use the {@link ImportManager#render(ImportManager.JavaDialect, CharSequence,
 * CharSequence, CharSequence[],
 * org.eclipse.xtext.xbase.lib.Functions.Function0) render} method to generate
 * the typical class layout. (See in the example below.)
 * <p>
 * While generating your code, use the {@link ImportManager#fqn(CharSequence)
 * fqn} methods to add FQNs to the ImportManager. If the FQN can be imported,
 * the shortened class name will be returned. If the FQN cannot be imported
 * (e. g. because of a name conflict), the full FQN will be returned.
 * <p>
 * {@code java.lang} FQNs and FQNs from the {@linkplain
 * ImportManager#packageName local package} will be shortened but not be
 * included in the import block (except if there is a name conflict).
 * <p>
 * <b>Example:</b>
 * <pre>
 * extension ImportManager = new ImportManager</br>
 * def generate() {
 *     render(
 *         JavaDialect.JAVA,
 *         'This is example code',
 *         'bar.bar.Bar',
 *         #[
 *             'not.foo.Foo'
 *             'java.util.Map',
 *             'java.util.Set',
 *         ],
 *         ['''
 *             public class Bar {
 *                 «'java.util.List&lt;java.lang.String&gt;'.fqn» list;
 *                 «'foo.foo.Foo'.fqn» foo;
 *                 «'java.lang.Integer'.foo» count;
 *                 «'bar.bar.Controller'.fqn» controller;
 *                 // ...
 *             }
 *         ''']
 *     )
 * }
 * </pre>
 * <p>
 * <b>Result:</b>
 * <pre>
 * // This is example code<br>
 * package bar.bar;<br>
 * import java.util.List;
 * import java.util.Map;
 * import java.util.Set;
 * import not.foo.Foo;<br>
 * public class Bar {
 *     List&lt;String&gt; list;
 *     foo.foo.Foo foo;
 *     Integer count;
 *     Controller controller;
 *     // ...
 * }
 * </pre>
 * 
 * @author Fabian Storek
 */
class ImportManager {
	
	
	
	/*** Attributes ***/
	
	/**
	 * The FQN of the class to be generated.
	 */
	var Fqn classFqn
		
	/**
	 * The map containing all class names and their corresponding FQNs, that
	 * will be imported.
	 */
	var Map<String, Fqn> importMap
	
	/**
	 * Whether or not the extension should shorten FQNs. If {@code false}, the
	 * {@link ImportManager#fqn(CharSequence) fqn} methods will not shorten
	 * FQNs and the will not be added to the import map.
	 */
	var boolean enabled
	
	
	
	/*** Constructors ***/
	
	/**
	 * Default constructor.
	 */
	new () {
		this.classFqn  = null
		this.importMap = null
		this.enabled   = false
	}
		

	
	/*** Internal methods ***/
	
	/**
	 * Returns a of all FQNs, that will be imported.
	 */
	def List<Fqn> getImportList() {
		importMap
			.values
			.reject[isJavaLangPackage || hasSamePackageName]
			.toList
	}
		
	/**
	 * Checks if the {@link Fqn#packageName packageName} of the FQN is the same
	 * as {@linkplain ImportManager#classFqn the class to be generated}.
	 */
	def private boolean hasSamePackageName(Fqn fqn) {
		fqn.packageName == classFqn.packageName
	}
	
	/**
	 * Checks if the {@link Fqn#className className} of the FQN is the same as
	 * {@linkplain ImportManager#classFqn the class to be generated}.
	 */
	def private boolean hasSameClassName(Fqn fqn) {
		fqn.className == classFqn.className
	}
	
	/**
	 * Checks if this (or an equivalent) FQN is in the {@link
	 * ImportManager#importMap importMap}.
	 */
	def private boolean isInImports(Fqn fqn) {
		if (importMap.containsKey(fqn.className)) {
			val fqnFromImports = importMap.get(fqn.className)
			return fqnFromImports.fullyQualifiedName == fqn.fullyQualifiedName
		}	
		return false
	}
	
	/**
	 * Checks if the FQN can be added to the {@link ImportManager#importMap
	 * importMap}. If the FQN is already in the map, {@code false} will be
	 * returned.
	 */
	def private boolean canAddToImports(Fqn fqn) {
		fqn.hasPackageName    &&
		!fqn.isPrimitive      &&
		!fqn.hasSameClassName &&
		!importMap.containsKey(fqn.className)
	}
	
	/**
	 * Adds the provided FQN to the {@link ImportManager#importMap importMap}
	 * and returns either the {@link Fqn#className className} (if the FQN was
	 * successfully added) or the {@link Fqn#fullyQualifiedName
	 * fullyQualifiedName} (if the FQN could not be added).
	 */
	def private String addToImports(Fqn fqn) {
		if (fqn.canAddToImports) {
			importMap.put(fqn.className, fqn)
			return fqn.className
		}
		else if (fqn.fullyQualifiedName == classFqn.fullyQualifiedName) {
			return fqn.className
		}
		else {
			return fqn.fullyQualifiedName
		}
	}
	
	
	
	/*** Main methods ***/
	
	/**
	 * Adds an FQN to the imports and returns the corresponding class name.
	 * <p>
	 * If there already exists an import with the same class name, the FQN will
	 * be returned instead.
	 * <p>
	 * If the FQN contains generics, all sub-FQNs will be dealt with, too.
	 * 
	 * @see ImportManager#fqn(CharSequence)
	 * @see ImportManager#fqn(Type)
	 */
	def String fqn(Fqn it) {
		if (enabled) {
			'''«if (isInImports) className else addToImports»«IF hasGenerics»<«generics.join(', ') [ fqn ]»>«ELSEIF hasExtendsConstraint» extends «extendsConstraint.fqn»«ELSEIF hasSuperConstraint» super «superConstraint.fqn»«ENDIF»'''	
		}
		else {
			fullyQualifiedNameWithSuffix
		}
	}
	
	/**
	 * Adds an FQN to the imports and returns the corresponding class name.
	 * <p>
	 * If there already exists an import with the same class name, the FQN will
	 * be returned instead.
	 * <p>
	 * If the FQN contains generics, all sub-FQNs will be dealt with, too.
	 * <p>
	 * This is an convenience method for {@link ImportManager#fqn(Fqn)
	 * fqn(Fqn)}.
	 * 
	 * @throws Fqn.FqnFormatException The FQN is not formatted correctly or is
	 *                                not an FQN.
	 * 
	 * @see ImportManager#fqn(Fqn)
	 * @see ImportManager#fqn(Type)
	 */
	def String fqn(CharSequence fqn) {
		fqn(new Fqn(fqn))
	}
	
	/**
	 * Adds an FQN to the imports and returns the corresponding class name.
	 * <p>
	 * If there already exists an import with the same class name, the FQN will
	 * be returned instead.
	 * <p>
	 * If the FQN contains generics, all sub-FQNs will be dealt with, too.
	 * <p>
	 * This is an convenience method for {@link ImportManager#fqn(Fqn)
	 * fqn(Fqn)}.
	 * 
	 * @throws Fqn.FqnFormatException The FQN is not formatted correctly or is
	 *                                not an FQN.
	 * 
	 * @see ImportManager#fqn(Fqn)
	 * @see ImportManager#fqn(CharSequence)
	 */
	def String fqn(Type type) {
		fqn(new Fqn(type))
	}
	
	/**
	 * Returns the {@code comments}, the class header (package declaration and
	 * import block) followed by the class code generated by {@code
	 * classGenerator}.
	 * <p>
	 * If the package name in {@code classFqn} is {@code null} or empty, the
	 * package declaration will be omitted.
	 * <p>
	 * <b>Note:</b>
	 * <p>
	 * Be aware of the order of execution! The code in the {@code
	 * classGenerator} function must be executed <i>after</i> adding all FQNs
	 * from {@code importFqns}. Thus, do not generate your code (using the
	 * {@link ImportManager#fqn(CharSequence) fqn} methods) beforehand and
	 * invoke this method afterwards. Instead, use a {@link Runnable} or {@link
	 * org.eclipse.xtext.xbase.lib.Functions.Function0 Function0<CharSequence>}
	 * ( {@code () => CharSequence} ).
	 * <p>
	 * <b>Example:</b>
	 * <pre>
	 * extension ImportManager = new ImportManager</br>
	 * def generate() {
	 *     render(
	 *         JavaDialect.JAVA,
	 *         'This is example code',
	 *         'bar.bar.Bar',
	 *         #[
	 *             'not.foo.Foo'
	 *             'java.util.Map',
	 *             'java.util.Set',
	 *         ],
	 *         ['''
	 *             public class Bar {
	 *                 «'java.util.List&lt;java.lang.String&gt;'.fqn» list;
	 *                 «'foo.foo.Foo'.fqn» foo;
	 *                 «'java.lang.Integer'.foo» count;
	 *                 «'bar.bar.Controller'.fqn» controller;
	 *                 // ...
	 *             }
	 *         ''']
	 *     )
	 * }
	 * </pre>
	 * <p>
	 * <b>Result:</b>
	 * <pre>
	 * // This is example code<br>
	 * package bar.bar;<br>
	 * import java.util.List;
	 * import java.util.Map;
	 * import java.util.Set;
	 * import not.foo.Foo;<br>
	 * public class Bar {
	 *     List&lt;String&gt; list;
	 *     foo.foo.Foo foo;
	 *     Integer count;
	 *     Controller controller;
	 *     // ...
	 * }
	 * </pre>
	 * 
	 * @param dialect        The target Java dialect ({@code JAVA} or {@code
	 *                       XTEND}).
	 * @param comment        A comment prefix for the whole file. Each line
	 *                       will be prefixed by double slashes ({@code //}).
	 *                       (Optional, may be {@code null} or empty.)
	 * @param classFqn       The fully qualified name of the class to be
	 *                       generated.
	 * @param importFqns     An array of FQNs that should always be imported.
	 *                       (Optional, may be {@code null} or empty.)
	 * @param classGenerator A function, that generates the declaration and
	 *                       body of the class. Must not generate the package
	 *                       or import declarations.
	 * 
	 * @return A fully rendered class file, including the {@code comments} (if
	 *         available), the package declaration (if available), any
	 *         resulting imports (either from the {@code importFqns} or the
	 *         usage of the {@link ImportManager#fqn(CharSequence) fqn}
	 *         methods) and the class code from the {@code classGenerator}
	 *         function.
	 * 
	 * @throws Fqn.FqnFormatException Either the {@code classFqn} or elements
	 *                                from {@code importFqns} are not formatted
	 *                                correctly or are not FQNs.
	 */
	def synchronized render(JavaDialect dialect, CharSequence comment, CharSequence classFqn, CharSequence[] importFqns, () => CharSequence classGenerator) {
		val lineTerminator = switch dialect {
			case JAVA:  ';'
			case XTEND: ''
		}
		val cleanComment = comment?.toString?.trim
		val commentLines = if (cleanComment.nullOrEmpty) {
			newArrayOfSize(0)
		}
		else {
			cleanComment.split('''\n''')
		}
		this.classFqn = new Fqn(classFqn)
		this.importMap = newHashMap
		importFqns?.forEach[fqn | new Fqn(fqn).addToImports]
		this.enabled = true
		val classCode = classGenerator.apply
		val result = '''
			«FOR line: commentLines AFTER '\n'»
				// «line»
			«ENDFOR»
			«IF this.classFqn.hasPackageName»
				package «this.classFqn.packageName»«lineTerminator»
				
			«ENDIF»
			«FOR fqn: importList.map[fullyQualifiedName].sort AFTER '\n'»
				import «fqn»«lineTerminator»
			«ENDFOR»
			«classCode»
		'''
		this.enabled = false
		this.classFqn = null
		this.importMap = null
		return result
	}
	
	
	
	/*** Additional methods ***/
	
	/**
	 * Check whether the FQN will be imported or not.
	 */
	def boolean isImported(Fqn fqn) {
		if (fqn.hasPackageName) {
			importList.exists[fullyQualifiedName == fqn.fullyQualifiedName]
		}
		else {
			importList.exists[className == fqn.className]
		}
	}
	
	
	
	/*** Inner classes ***/
	
	/**
	 * Enum of Java dialects:
	 * <ul>
	 * <li>{@code JAVA}</li>
	 * <li>{@code XTEND}</li>
	 * </ul>
	 */
	static enum JavaDialect {
		JAVA,
		XTEND
	}
	
}

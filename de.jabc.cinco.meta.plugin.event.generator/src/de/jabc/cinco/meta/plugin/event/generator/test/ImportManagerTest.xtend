/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.test

import de.jabc.cinco.meta.plugin.event.api.util.Fqn
import de.jabc.cinco.meta.plugin.event.api.util.Fqn.FqnFormatException
import de.jabc.cinco.meta.plugin.event.generator.util.ImportManager
import java.lang.reflect.Type
import java.util.List
import java.util.Map
import java.util.Set
import org.junit.jupiter.api.Test

import static de.jabc.cinco.meta.plugin.event.generator.util.ImportManager.JavaDialect.*
import static org.junit.jupiter.api.Assertions.*

/**
 * Unit test (JUnit 5) for {@link ImportManager}.
 * @author Fabian Storek
 */
class ImportManagerTest {
	
	extension ImportManager = new ImportManager
	
	
		
	/*** Tests ***/

	@Test
	def void testEdgeCases() {
		render(JAVA, null, 'this.is.a.DummyFqn', null) [
			assertThrows(NullPointerException) [fqn(null as Fqn)]
			assertThrows(NullPointerException) [fqn(null as String)]
			assertThrows(NullPointerException) [fqn(null as Class<?>)]
			assertThrows(FqnFormatException)   [fqn('')]
			assertThrows(FqnFormatException)   [fqn('? extends ')]
			assertThrows(FqnFormatException)   [fqn('missing.class.name.')]
			assertThrows(FqnFormatException)   [fqn('unbalanced.angle.Brackets<Foo')]
			assertThrows(FqnFormatException)   [fqn('unbalanced.angle.Brackets<Foo>>')]
			return null
		]
	}
	
	@Test
	def void testWhitespace() {
		check(
			' Map '                     -> 'Map',
			' Map < > '                 -> 'Map<>',
			' Map < String , String > ' -> 'Map<String, String>'
		)
	}
	
	@Test
	def void testJavaLang() {
		check(#[
			'java.lang.Boolean'   -> 'Boolean',
			'java.lang.Byte'      -> 'Byte',
			'java.lang.Character' -> 'Character',
			'java.lang.Double'    -> 'Double',
			'java.lang.Float'     -> 'Float',
			'java.lang.Integer'   -> 'Integer',
			'java.lang.Long'      -> 'Long',
			'java.lang.Object'    -> 'Object',
			'java.lang.Short'     -> 'Short',
			'java.lang.String'    -> 'String',
			Boolean               -> 'Boolean',
			Byte                  -> 'Byte',
			Character             -> 'Character',
			Double                -> 'Double',
			Float                 -> 'Float',
			Integer               -> 'Integer',
			Long                  -> 'Long',
			Object                -> 'Object',
			Short                 -> 'Short',
			String                -> 'String'
		], #[
			// Empty
		])
	}
	
	@Test
	def void testJavaPrimitives() {
		check(#[
			'byte'    -> 'byte',
			'short'   -> 'short',
			'int'     -> 'int',
			'long'    -> 'long',
			'float'   -> 'float',
			'double'  -> 'double',
			'char'    -> 'char',
			'boolean' -> 'boolean',
			byte      -> 'byte',
			short     -> 'short',
			int       -> 'int',
			long      -> 'long',
			float     -> 'float',
			double    -> 'double',
			char      -> 'char',
			boolean   -> 'boolean'
		], #[
			// Empty
		])
	}
	
	@Test
	def void testNormal() {
		check(#[
			'Map'             -> 'Map',
			'java.util.Map'   -> 'Map',
			'java.util.x.Map' -> 'java.util.x.Map',
			'java.util.Map'   -> 'Map',
			'Map'             -> 'Map'
		], #[
			'java.util.Map'
		])
	}
		
	@Test
	def void testSimpleGenerics() {
		check(#[
			'List<>'                                   -> 'List<>',
			'List<String>'                             -> 'List<String>',
			'List<java.lang.String>'                   -> 'List<String>',
			'java.util.List<String>'                   -> 'List<String>',
			'java.util.List<java.lang.String>'         -> 'List<String>',
			'java.util.List<? extends java.util.List>' -> 'List<? extends List>'
		], #[
			'java.util.List'
		])
	}
	
	@Test
	def void testMultiGenerics() {
		check(#[
			'Map<String, Integer>'                     -> 'Map<String, Integer>',
			'Map<java.lang.String, java.lang.Integer>' -> 'Map<String, Integer>',
			'java.util.Map<java.lang.String, Integer>' -> 'Map<String, Integer>'
		], #[
			'java.util.Map'
		])
	}
	
	@Test
	def void testNestedGenerics() {
		check(#[
			'List<ArrayList<String>>'                               -> 'List<ArrayList<String>>',
			'List<java.util.ArrayList<String>>'                     -> 'List<ArrayList<String>>',
			'java.util.List<java.util.ArrayList<java.lang.String>>' -> 'List<ArrayList<String>>'
		], #[
			'java.util.List',
			'java.util.ArrayList'
		])
	}
	
	@Test
	def void testNestedMultiGenerics() {
		check(#[
			'List<Map<String, Integer>>'                     -> 'List<Map<String, Integer>>',
			'List<java.util.Map<java.lang.String, Integer>>' -> 'List<Map<String, Integer>>',
			'java.util.List<java.util.Map<String, Integer>>' -> 'List<Map<String, Integer>>'
		], #[
			'java.util.List',
			'java.util.Map'
		])
	}
	
	@Test
	def void testClasses() {
		check(#[
			List                  -> 'List',
			'java.util.List'      -> 'List',
			'foo.bar.List'        -> 'foo.bar.List',
			'java.util.Map'       -> 'Map',
			Map                   -> 'Map',
			Map.Entry             -> 'Entry',
			'java.util.Map.Entry' -> 'Entry',
			'foo.bar.Entry'       -> 'foo.bar.Entry'
		], #[
			'java.util.List',
			'java.util.Map',
			'java.util.Map.Entry'
		])
	}
	
	@Test
	def void testLocalPackage() {
		check('test.pkg.Foo', #[
			'test.pkg.Foo' -> 'Foo',
			'test.pkg.Bar' -> 'Bar'
		], #[
			// Empty
		])
	}
	
	@Test
	def void testRender() {
		val actualResult = render(
			JAVA,
			'''
				This is a prefix.
				1 + 2 = «1 + 2»
			''',
			'foo.foo.Foo',
			#[
				'java.util.Date',
				'foo.foo.Set'
			],
			['''
				public class Foo extends «'bar.bar.Bar'.fqn» {
					
					«'java.lang.String'.fqn» name;
					«'java.util.List<bar.bar.Bar>'.fqn» barList;
					«Map.fqn»<«String.fqn», «'foo.foo.Foo'.fqn»> fooMap;
					«'java.util.Set<foo.foo.Foo>'.fqn» fooSet;
					«'foo.foo.Set<bar.bar.Bar>'.fqn» barSet;
					
					public Foo(String name, «'bar.bar.List'.fqn»<«'bar.bar.Bar'.fqn»> barList, «'java.util.Map<java.lang.String, foo.foo.Foo>'.fqn» fooMap) {
						this.name = name;
						this.barList = barList.toList();
						this.fooMap = fooMap;
						this.fooSet = new «Set.fqn»<>();
						this.barSet = new «'foo.foo.Set<>'.fqn»();
					}
					
				}
			'''])
		val expectedResult =
			'''
			// This is a prefix.
			// 1 + 2 = 3
			
			package foo.foo;
			
			import bar.bar.Bar;
			import java.util.Date;
			import java.util.List;
			import java.util.Map;
			
			public class Foo extends Bar {
				
				String name;
				List<Bar> barList;
				Map<String, Foo> fooMap;
				java.util.Set<Foo> fooSet;
				Set<Bar> barSet;
				
				public Foo(String name, bar.bar.List<Bar> barList, Map<String, Foo> fooMap) {
					this.name = name;
					this.barList = barList.toList();
					this.fooMap = fooMap;
					this.fooSet = new java.util.Set<>();
					this.barSet = new Set<>();
				}
				
			}
		'''
		checkCode(expectedResult, actualResult)
	}
	
	
	
	/*** Setup ***/
	
	def private void check(String classFqn, Pair<? extends Object, String>[] fqns, String[] expectedImports) {
		render(JAVA, null, classFqn?: 'this.is.a.DummyFqn', null) [
			for (pair: fqns) {
				val fqn = pair.key
				val expectedResult = pair.value
				val actualResult = switch fqn {
					case null: {	
						assertThrows(NullPointerException) [fqn(fqn as Fqn)]
						assertThrows(NullPointerException) [fqn(fqn as Class<?>)]
						assertThrows(NullPointerException) [fqn(fqn as String)]
					}
					Fqn:          fqn.fqn
					Type:         fqn.fqn
					CharSequence: fqn.fqn
					default:      throw new IllegalArgumentException('The check method must only be used with Map<null, String> or Map<Fqn, String> or Map<Type, String> or Map<CharSequence, String>.')
				}
				assertEquals(expectedResult, actualResult, '''fqn("«fqn?: 'null'»") should return "«expectedResult?: 'null'»", but it returns "«actualResult?: 'null'»"."''')
			}
			if (expectedImports !== null) {
				assertIterableEquals(expectedImports.sort, importList.map[fullyQualifiedName].sort)
			}
			return null
		]
	}
	
	def private void check(Pair<? extends Object, String>[] fqns, String[] includedFqns) {
		check(null, fqns, includedFqns)
	}
	
	
	def private void check(Pair<? extends Object, String> ... fqns) {
		check(null, fqns, null)
	}
	
	
	def private void checkCode(CharSequence expected, CharSequence actual) {
		val expectedClean = expected.toString.replaceAll('''\s+''', ' ')
		val actualClean = actual.toString.replaceAll('''\s+''', ' ')
		assertEquals(expectedClean, actualClean)
	}
		
}

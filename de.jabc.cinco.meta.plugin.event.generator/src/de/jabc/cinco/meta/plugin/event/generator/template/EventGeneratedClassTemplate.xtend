/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.template

import de.jabc.cinco.meta.core.event.hub.Subscriber
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import de.jabc.cinco.meta.plugin.event.generator.util.XtendFileTemplate
import java.util.List
import mgl.ModelElement

import static de.jabc.cinco.meta.plugin.event.api.util.EventEnum.*

class EventGeneratedClassTemplate extends XtendFileTemplate {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	
	val ModelElement element
	val ModelElement superElement
	val boolean      hasSuperElement
	
	new (ModelElement element) {
		this.element         = element
		this.superElement    = element.superElement
		this.hasSuperElement = superElement !== null && superElement.isEventEnabled
	}
	
	override getClassName() {
		element.eventGeneratedClassName
	}
	
	override getClassTemplate() '''
		«IF element.hasEventAnnotation»abstract«ELSE»final«ENDIF» class «className» implements «element.eventApiFqn.fqn»<«element.fqn»> {
			
			protected extension «EventApiExtension.fqn» = new «EventApiExtension.fqn»
			
			var static «className» eventInstance
			
			var «List.fqn»<«Subscriber.fqn»> subscribers
			
			protected new () {
				// Intentionally left blank
			}
			
			def final static «className» getInstance() {
				if (eventInstance === null) {
					eventInstance = new «if (element.hasEventAnnotation) element.eventUserFqn.fqn else element.eventGeneratedFqn.fqn»
				}
				return eventInstance
			}
			
			override final subscribe() {
				subscribers = #[
					«FOR event: element.events SEPARATOR ','»
						subscribePayloadSubscriber('«event.getContextIdentifier(element)»') [
							«event.payloadClass.fqn»<«element.fqn»> payload |
								instance.«event.methodName»(payload)
						]
					«ENDFOR»
				]
			}
			
			override final unsubscribe() {
				subscribers?.forEach [ unsubscribe ]
				subscribers = null
			}
			«FOR event: element.events»
				
				override«IF !event.isImplemented» final«ENDIF» «event.methodName»(«event.getMethodParameterDeclarations(element.elementFqn) [ fqn() ]») {
					«IF hasSuperElement»
						«superElement.eventGeneratedFqn.fqn».instance.«event.methodName»(«event.methodParameterNames»)
					«ELSE»
						«IF event === POST_DELETE»
							[ /* Intentionally left blank */ ]
						«ELSEIF event === CAN_CREATE_GRAPH_MODEL»
							null
						«ELSEIF event === CAN_ATTRIBUTE_CHANGE»
							null
						«ELSEIF event.prefix == CAN»
							true
						«ELSE»
							// Intentionally left blank
						«ENDIF»
					«ENDIF»
				}
				
				override final «event.methodName»(«event.payloadClass.fqn»<«element.fqn»> it) {
					«event.methodName»(«event.methodParameterNames»)
				}
			«ENDFOR»
			
		}
	'''
	
}

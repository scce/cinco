/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.template

import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import de.jabc.cinco.meta.plugin.event.generator.util.XtendFileTemplate
import mgl.ModelElement

import static de.jabc.cinco.meta.plugin.event.api.util.EventEnum.*

class EventUserClassTemplate extends XtendFileTemplate {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	
	var boolean generateExampleLogs = false
	
	val ModelElement        element
	val ModelElement        superElement
	val boolean             hasSuperElement
	val Iterable<EventEnum> events
		
	new (ModelElement element) {
		this.element         = element
		this.superElement    = element.superElement
		this.hasSuperElement = superElement !== null && superElement.isEventEnabled
		this.events          = element.events.filter[ isImplemented ]
	}
	
	override getClassName() {
		element.eventUserClassName
	}
	
	override getClassTemplate() '''
		/* 
		 * About this class:
		 * - This is a default implementation for «element.eventGeneratedFqn.fqn».
		 * - This class was generated, because you added an "@«eventAnnotationName»" annotation to
		 *   «element.graphmodelClass.simpleName» "«element.elementClassName»" in "«element.model.modelFileName».mgl".
		 * - This file will not be overwritten on future generation processes.
		«IF hasSuperElement»
			«null /* Fixes indentation */»
			 * 
			 * Edit this class:
			 * - If you wish «element.elementClassName» to react the same way as its super class «superElement.elementClassName»,
			 *   you may delete the method or leave it as is (with only the super call).
			 * - If you wish to only add functionality, leave the super call in the
			 *   corresponding method and add your code to it.
			 * - If you wish to break the inheritance chain, remove the super call, but do
			 *   not delete the corresponding method. You may leave it empty or write new
			 *   code.
		«ENDIF»
		 * 
		 * Available event methods:
		«FOR event: events»
			«null /* Fixes indentation */»
			 * - «event.methodName»(«event.getMethodParameterDeclarations(element.elementFqn) [ fqn() ]»)
		«ENDFOR»
		 */
		final class «className» extends «element.eventGeneratedFqn.fqn» {
			«FOR event: events»
				
				override «event.methodName»(«event.getMethodParameterDeclarations(element.elementFqn) [ fqn() ]») {
					// TODO: Auto-generated method stub
					«IF event === POST_DELETE»
						// Set up your post delete Runnable here.
						// This will be executed pre delete.
						«IF hasSuperElement»
							val super«event.methodName.toFirstUpper» = super.«event.methodName»(«event.methodParameterNames»)
						«ENDIF»
						return [
							// This is your post delete Runnable.
							// This will be executed post delete.
							«event.logCall»
							«IF hasSuperElement»
								super«event.methodName.toFirstUpper».run
							«ENDIF»
						]
					«ELSEIF event === CAN_CREATE_GRAPH_MODEL»
						// A wizard is used to create a new «element.elementClassName». Instead of just returning
						// a boolean whether a «element.elementClassName» can be created, you must return a String.
						// The String must contain a suitable error message, if the «element.elementClassName» cannot be created.
						// The message will be displayed in the wizard page.
						// To allow the creation of a new «element.elementClassName», return `null`.
						«event.logCall»
						«IF hasSuperElement»
							super.«event.methodName»(«event.methodParameterNames»)
						«ELSE»
							null
						«ENDIF»
					«ELSEIF event === CAN_ATTRIBUTE_CHANGE»
						// Instead of just returning a boolean whether an attribute can be changed, you must return a String.
						// The String must contain a suitable error message, if an attribute cannot be changed.
						// The message will be displayed in the Cinco property view.
						// To allow the change of an attribute, return `null`.
						«event.logCall»
						«IF hasSuperElement»
							super.«event.methodName»(«event.methodParameterNames»)
						«ELSE»
							null
						«ENDIF»
					«ELSEIF event.prefix == CAN»
						«event.logCall»
						«IF hasSuperElement»
							super.«event.methodName»(«event.methodParameterNames»)
						«ELSE»
							true
						«ENDIF»
					«ELSE»
						«event.logCall»
						«IF hasSuperElement»
							super.«event.methodName»(«event.methodParameterNames»)
						«ENDIF»
					«ENDIF»
				}
			«ENDFOR»
			
		}
	'''
	
	def private getLogCall(EventEnum event) {
		if (generateExampleLogs) {
			'''
				log(«"'''"»
					«className».«event.methodName»(
						«FOR parameter: event.payloadClass.declaredFields SEPARATOR ','»
							«parameter.name» = «"«"»«parameter.name».description«"»"»
						«ENDFOR»
					)
				«"'''"»)
			'''
		}
	}
	
}

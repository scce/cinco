/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.template

import de.jabc.cinco.meta.plugin.dsl.ProjectDescription
import de.jabc.cinco.meta.plugin.event.generator.util.ClassLoaderExtension
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import java.io.FileOutputStream
import java.util.Iterator
import java.util.NoSuchElementException
import java.util.jar.Manifest
import mgl.MGLModel
import mgl.ModelElement
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.NullProgressMonitor
import org.w3c.dom.Node
import org.w3c.dom.NodeList

import static de.jabc.cinco.meta.plugin.dsl.ProjectType.JAVA

import static extension de.jabc.cinco.meta.core.utils.PluginXMLEditor.addAttribute
import static extension de.jabc.cinco.meta.core.utils.PluginXMLEditor.addExtension
import static extension de.jabc.cinco.meta.core.utils.PluginXMLEditor.addSubElement
import static extension de.jabc.cinco.meta.core.utils.PluginXMLEditor.openPluginXMLDocument
import static extension de.jabc.cinco.meta.core.utils.PluginXMLEditor.save

class MainProjectTemplate extends ProjectTemplate {
	
	extension ClassLoaderExtension = new ClassLoaderExtension
	extension EventGeneratorExtension = new EventGeneratorExtension
	
	override _projectName() {
		mainProject.name
	}
	
	override _projectDescription() {
		
		new ProjectDescription(this) {
			
			override create() {
				val project = super.create
				project.addRequiredBundles
				for (mgl: allMGLs) {
					project.addStartupExtension(mgl)
				}
				return project
			}
			
		} => [
			
			type = JAVA
			deleteIfExistent = false
			
			val elements = allMGLs.toInvertedMap[eventEnabledElements]
			
			folder('src') [
				
				deleteIfExistent = false
				isSourceFolder = true
								
				forEachOf(elements) [ mgl, mglElements |
					forEachOf(mglElements) [ element |
						if (element.hasEventAnnotation && !element.hasEventUserClassImplementation) {
							pkg(element.eventUserPackageName) [
								deleteIfExistent = false
								file(new EventUserClassTemplate(element), false)
							]
						}
					]
				]
				
			]
			
			folder('src-gen') [
				
				deleteIfExistent = false
				isSourceFolder = true
				
				forEachOf(elements) [ mgl, mglElements |
					pkg(mgl.eventGeneratedPackageName) [
						deleteIfExistent = true
							file(new EventStartupClassTemplate(mgl))
							forEachOf(mglElements) [ element |
							file(new EventGeneratedClassTemplate(element))
						]
					]
				]
				
			]
			
		]

	}
	
	def private boolean hasEventUserClassImplementation(ModelElement element) {
		element.eventUserFqn.fullyQualifiedName.classExists
	}
	
	def private void addRequiredBundles(IProject project) {
		
		val manifestFile = project.getFolder('META-INF').getFile('MANIFEST.MF')
		if (!manifestFile.exists) {
			err('Missing MANIFEST.MF')
			return
		}
		val manifest = new Manifest(manifestFile.contents)
		
		val requiredBundles = manifest
			.mainAttributes
			.getValue('Require-Bundle')
			?.split(',')
			?.toSet
			?: #{}
		
		val eventRequiredBundles = #[
			// Xtend
			'com.google.guava',
			'org.eclipse.core.runtime',
			'org.eclipse.xtext.xbase.lib',
			'org.eclipse.xtend.lib',
			'org.eclipse.xtend.lib.macro',
			// Cinco runtime
			'de.jabc.cinco.meta.runtime',
			// Event
			eventCorePluginID,
			eventApiPluginID
		]
		
		requiredBundles.addAll(eventRequiredBundles)
		manifest.mainAttributes.putValue('Require-Bundle', requiredBundles.join(','))
		manifest.write(new FileOutputStream(manifestFile.location.toFile))
		
	}
	
	def private void addStartupExtension(IProject project, MGLModel mgl) {
		
		val cincoProductId = mgl.projectSymbolicName
		val startupClass   = mgl.eventStartupFqn.fullyQualifiedName
		val pluginXml      = project.openPluginXMLDocument
		
		// Check if startup extension for this MGL already exists
		for (extensionPoint: pluginXml.documentElement.getElementsByTagName('extension').iterable) {
			val extensionPointId = extensionPoint.attributes?.getNamedItem('point')?.nodeValue
			if (extensionPointId == 'de.jabc.cinco.meta.runtime.startup') {
				for (childNode: extensionPoint.childNodes.iterable) {
					val childName           = childNode.nodeName
					val childAttributes     = childNode.attributes
					val childCincoProductId = childAttributes?.getNamedItem('cincoProductID')?.nodeValue
					val childStartupClass   = childAttributes?.getNamedItem('class')?.nodeValue
					if (childName           == 'startupclass' &&
						childCincoProductId == cincoProductId &&
						childStartupClass   == startupClass     ) {
						return // Do not add startup extension
					}
				}
			}
		}
		
		/*
		 * Add startup extension
		 * 
		 * <extension point="de.jabc.cinco.meta.runtime.startup">
		 *     <startupclass
		 *         cincoProductID="«cincoProductId»"
		 *         class="«startupClass»">
		 *     </startupclass>
		 * </extension>
		*/
		val extensionPoint = pluginXml.addExtension('de.jabc.cinco.meta.runtime.startup')
		val startupclass = extensionPoint.addSubElement('startupclass')
		startupclass.addAttribute('cincoProductID', cincoProductId)
		startupclass.addAttribute('class', startupClass)
		pluginXml.save(project, new NullProgressMonitor)
		
	}
	
	def private NodeListIterable getIterable(NodeList nodeList) {
		new NodeListIterable(nodeList)
	}
	
	private static class NodeListIterable implements Iterable<Node>, Iterator<Node> {
		
		val NodeList nodeList
		var int nextIndex
		
		new (NodeList nodeList) {
			this.nodeList = nodeList
			this.nextIndex = 0
		}
		
		override iterator() {
			this
		}
		
		override hasNext() {
			nextIndex < nodeList.length
		}
		
		override next() {
			if (!hasNext) {
				throw new NoSuchElementException
			}
			val item = nodeList.item(nextIndex)
			nextIndex += 1
			return item
		}
		
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.validation

import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator
import de.jabc.cinco.meta.plugin.event.generator.util.ClassLoaderExtension
import de.jabc.cinco.meta.plugin.event.generator.util.EventGeneratorExtension
import java.util.regex.Pattern
import mgl.Annotation
import mgl.Edge
import mgl.GraphModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import org.eclipse.emf.ecore.EObject

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.*

class EventValidator implements IMetaPluginValidator {
	
	extension EventGeneratorExtension = new EventGeneratorExtension
	extension ClassLoaderExtension = new ClassLoaderExtension
	
	val static FQN_PATTERN = Pattern.compile('''^([a-zA-Z_$][a-zA-Z0-9_$]*\.)*[a-zA-Z_$][a-zA-Z0-9_$]*$''')
	val static CLASS_LOADER_REFRESH_INTERVAL = 10000000000L // 10000000000 ns = 10 s
	
	var static lastClassLoaderRefresh = System.nanoTime 
	
	def boolean shouldCheck(EObject obj) {
		
		// Check annotation
		if (obj === null) {
			return false
		}
		if (!(obj instanceof Annotation)) {
			return false
		}
		val annotation = obj as Annotation
		if (!annotation.isEventAnnotation) {
			return false
		}
		if (annotation.parent === null) {
			return false
		}
		if (!(annotation.parent instanceof ModelElement)) {
			return false
		}
		return true
		
	}
	
	override checkOnEdit(EObject obj) {
		
		if (!shouldCheck(obj)) {
			return null
		}
		
		// Check parent type
		val annotation = obj as Annotation
		val parent = annotation.parent as ModelElement
		switch parent {
			GraphModel,
			NodeContainer,
			Node,
			Edge: {
				// No Error
			}
			default: {
				return newError('''The "@«eventAnnotationName»" annotation is not compatible with "«parent.class.simpleName»".''', annotation.nameFeature)
			}
		}
		
		// Check number of arguments
		if (annotation.value === null || annotation.value.size != 1) {
			return newError('Provide exactly one FQN.', annotation.nameFeature)
		}
		
		// Check FQN pattern
		val fqn = annotation.value.head
		if (!FQN_PATTERN.matcher(fqn).matches) {
			return newError('''"«fqn»" is not a valid Java FQN.''', annotation.valueFeature)
		}
		
		// Check for duplicate event FQNs
		val model = parent.model
		val allElements = model.graphModels + model.nodes + model.edges
		val allEventAnnotations = allElements.map[annotations].flatten.filter[isEventAnnotation]
		if (allEventAnnotations.exists[ anno | fqn == anno.value.head && annotation !== anno ]) {
			return newError('''"«fqn»" is already used by another element.''', annotation.valueFeature)
		}
		
		// Check if FQN matches a generated class from src-gen
		val lowerCaseFqn = fqn.toLowerCase
		if (allElements.exists[ element | lowerCaseFqn == element.eventGeneratedFqn.fullyQualifiedName.toLowerCase ]) {
			return newError('''"«fqn»" cannot be used, because it is used internally.''', annotation.valueFeature)
		}
		
		return null
		
	}
	
	override checkOnSave(EObject obj) {
				
		if (!shouldCheck(obj)) {
			return null
		}
		
		// Refresh class loaders at most every 10 seconds.
		// Refreshing for every model element would be too expensive.
		if (System.nanoTime - lastClassLoaderRefresh > CLASS_LOADER_REFRESH_INTERVAL) {
			refreshClassLoaders
			lastClassLoaderRefresh = System.nanoTime
		}
		
		// Check if Java class exists
		val annotation = obj as Annotation
		val fqn = annotation.value.head
		if (fqn.nullOrEmpty) {
			return null
		}
		val eventUserClass = try fqn.loadClass
			catch (ClassNotFoundException e) {
				return newInfo('''Java class "«fqn»" does not exist. A template will be generated.''', annotation.valueFeature)
			}
		
		// Check if Java class implements correct event interface
		val parent = annotation.parent as ModelElement
		val eventGeneratedClass = parent.eventGeneratedFqn.fullyQualifiedName
		if (!eventUserClass.implementsOrExtends(eventGeneratedClass)) {
			return newWarning('''"«fqn»" must extend the class "«eventGeneratedClass»".''', annotation.valueFeature)
		}
		
		return null
		
	}
	
	def private getNameFeature(Annotation it) {
		eClass.getEStructuralFeature('name')
	}
	
	def private getValueFeature(Annotation it) {
		eClass.getEStructuralFeature('value')
	}
		
}

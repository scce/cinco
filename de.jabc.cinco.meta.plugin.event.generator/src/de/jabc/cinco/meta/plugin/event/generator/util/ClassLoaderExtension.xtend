/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.util

import java.net.URLClassLoader
import java.util.List
import org.eclipse.core.runtime.Path
import org.eclipse.jdt.core.JavaCore

import static org.eclipse.core.resources.ResourcesPlugin.getWorkspace

import static extension org.eclipse.jdt.launching.JavaRuntime.computeDefaultRuntimeClassPath

/**
 * This extension provides a way to load classes from Java projects in the
 * runtime workspace.
 * <p>
 * Use {@link ClassLoaderExtension#refreshClassLoaders() refreshClassLoaders()}
 * to refresh the list of class loaders, if any Java projects were added or
 * removed.
 * <p>
 * Use {@link ClassLoaderExtension#loadClass(String) loadClass(String)} to load
 * the class with the provided fully qualified name.
 * 
 * @author Fabian Storek
 */
class ClassLoaderExtension {
	
	var static List<URLClassLoader> classLoaders
	
	new() {
		refreshClassLoaders
	}
	
	/**
	 * Refreshes the list of class loaders. Use this before calling
	 * {@link ClassLoaderExtension#loadClass(String) loadClass(String)},
	 * if any Java projects were added or removed.
	 */
	def void refreshClassLoaders() {
		classLoaders =
			workspace
			.root
			.projects
			.filter[isOpen && hasNature(JavaCore.NATURE_ID)]
			.map[project |
				project.open(null)
				val javaProject = JavaCore.create(project)
				val loader      = javaProject.class.classLoader
				val urls        = javaProject.computeDefaultRuntimeClassPath
				                             .map[new Path(it).toFile.toURI.toURL]
				new URLClassLoader(urls, loader)
			]
			.toList
		if (classLoaders === null) {
			classLoaders = #[]
		}
	}
	
	/**
	 * Loads any primitive class ({@code byte}, {@code short}, {@code int},
	 * {@code long}, {@code float}, {@code double}, {@code char}, {@code boolean}).
	 * 
	 * @param fqn Fully qualified name of the primitive class.
	 * 
	 * @returns Class of the {@code fqn}.
	 * 
	 * @throws ClassNotFoundException No primitive class can be found for the
	 *         {@code fqn}.
	 */
	def Class<?> loadPrimitiveClass(String fqn) throws ClassNotFoundException {
		switch (fqn) {
			case 'byte':    byte
			case 'short':   short
			case 'int':     int
			case 'long':    long
			case 'float':   float
			case 'double':  double
			case 'char':    char
			case 'boolean': boolean
			default:        throw new ClassNotFoundException('''Could not load "«fqn»".''')
		}
	}
	
	/**
	 * Loads a class (in the runtime workspace) from its fully qualified name.
	 * If any Java projects were added or removed, call {@link #refreshClassLoaders()
	 * refreshClassLoaders()} beforehand, or else new classes will not be found
	 * and deleted classes will be.
	 * 
	 * @param fqn Fully qualified name of the class.
	 * 
	 * @returns Class of the {@code fqn}.
	 * 
	 * @throws ClassNotFoundException No class can be found for the {@code fqn}.
	 * 
	 * @see #loadClass(String, boolean) loadClass(String, boolean)
	 */
	def Class<?> loadClass(String fqn) throws ClassNotFoundException {
		try return fqn.loadPrimitiveClass
		catch (ClassNotFoundException e) { }
		try return Class.forName(fqn)
		catch (ClassNotFoundException e) { }
		for (loader : classLoaders) {
			try return loader.loadClass(fqn)
			catch (ClassNotFoundException e) { }
		}
		throw new ClassNotFoundException('''Could not load "«fqn»".''')
	}
	
	/**
	 * Loads a class (in the runtime workspace) from its fully qualified name.
	 * If any Java projects were added or removed, set the {@code refreshBeforehand}
	 * flag to {@code true}, or else new classes will not be found and deleted
	 * classes will be.
	 * 
	 * @param fqn               Fully qualified name of the class.
	 * @param refreshBeforehand Whether the class loaders should be refreshed before
	 *                          loading the class.
	 * 
	 * @returns Class of the {@code fqn}.
	 * 
	 * @throws ClassNotFoundException No class can be found for the {@code fqn}.
	 * 
	 * @see #loadClass(String) loadClass(String)
	 */
	def Class<?> loadClass(String fqn, boolean refreshBeforehand) throws ClassNotFoundException {
		if (refreshBeforehand) refreshClassLoaders
		return fqn.loadClass
	}
	
	/**
	 * Checks whether a class exists (in the runtime workspace). If any Java
	 * projects were added or removed, call {@link #refreshClassLoaders()
	 * refreshClassLoaders()} beforehand, or else new classes will not be found
	 * and deleted classes will be.
	 * 
	 * @param fqn Fully qualified name of the class.
	 * 
	 * @returns {@code true}, if the class exists.
	 * 
	 * @see #classExists(String, boolean) classExists(String, boolean)
	 */
	def boolean classExists(String fqn) {
		try {
			fqn.loadClass
			return true
		}
		catch (ClassNotFoundException e) {
			return false
		}
	}
	
	/**
	 * Checks whether a class exists (in the runtime workspace). If any Java
	 * projects were added or removed, set the {@code refreshBeforehand}
	 * flag to {@code true}, or else new classes will not be found and deleted
	 * classes will be.
	 * 
	 * @param fqn Fully qualified name of the class.
	 * @param refreshBeforehand Whether the class loaders should be refreshed before
	 *                          loading the class.
	 * 
	 * @returns {@code true}, if the class exists.
	 * 
	 * @see #classExists(String) classExists(String)
	 */
	def boolean classExists(String fqn, boolean refreshBeforehand) {
		if (refreshBeforehand) refreshClassLoaders
		fqn.classExists
	}
	
}

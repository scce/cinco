/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.generator.util

import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.Fqn
import de.jabc.cinco.meta.plugin.event.generator.EventPlugin
import mgl.MGLModel
import mgl.ModelElement

/**
 * @author Fabian Storek
 */
class EventGeneratorExtension extends EventApiExtension {
	
	val public static String EVENT_GENERATED_PACKAGE_SUFFIX    = 'event'
	val public static String EVENT_GENERATED_CLASS_NAME_PREFIX = 'Internal'
	val public static String EVENT_GENERATED_CLASS_NAME_SUFFIX = 'Event'
	val public static String EVENT_STARTUP_CLASS_NAME          = 'EventStartup'
	
	
	
	/*** Plugin IDs ***/
	
	def String getEventPluginPluginID() {
		EventPlugin.PLUGIN_ID
	}
	
	
	
	/*** Event startup ***/
	
	def String getEventStartupClassName() {
		EVENT_STARTUP_CLASS_NAME
	}
	
	def String getEventStartupPackageName(MGLModel model) {
		model.eventGeneratedPackageName
	}
	
	def Fqn getEventStartupFqn(MGLModel model) {
		'''«model.eventStartupPackageName».«eventStartupClassName»'''.toFqn
	}
	
	
	
	/*** Event generated ***/
	
	def String getEventGeneratedPackageSuffix() {
		EVENT_GENERATED_PACKAGE_SUFFIX
	}
	
	def String getEventGeneratedPackageName(MGLModel model) {
		'''«model.modelPackageName».«EVENT_GENERATED_PACKAGE_SUFFIX»'''
	}
	
	def String getEventGeneratedPackageName(ModelElement element) {
		element.model.eventGeneratedPackageName
	}
			
	def String getEventGeneratedClassName(ModelElement element) {
		'''«EVENT_GENERATED_CLASS_NAME_PREFIX»«element.elementFqn.className»«EVENT_GENERATED_CLASS_NAME_SUFFIX»'''
	}
	
	def Fqn getEventGeneratedFqn(ModelElement element) {
		'''«element.eventGeneratedPackageName».«element.eventGeneratedClassName»'''.toFqn
	}
	
	
	
	/*** Event user ***/
	
	def String getEventUserPackageName(ModelElement element) {
		element.eventUserFqn?.packageName
	}
	
	def String getEventUserClassName(ModelElement element) {
		element.eventUserFqn?.className
	}
	
	def Fqn getEventUserFqn(ModelElement element) {
		element.eventAnnotation?.value?.head?.toFqn
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.referenceregistry.listener;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;

import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry;

public class RegistryResourceChangeListener implements IResourceChangeListener {
	
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		if (delta != null) {
			processAffectedFiles(delta);
		}
	}

	private void processAffectedFiles(IResourceDelta delta) {
		IPath from = null;
		for (IResourceDelta child: delta.getAffectedChildren()) {
			IResource res = child.getResource();
			
			if (res instanceof IFile) {
				IFile file = (IFile) res;
				
				if (added(child)){
					ReferenceRegistry.getInstance().handleAdd(getUri(file.getFullPath()));
				}
				if (deleted(child)){
					ReferenceRegistry.getInstance().handleDelete(getUri(file.getFullPath()));
				}
				if (changed(child)){
					ReferenceRegistry.getInstance().handleChange(getUri(file.getFullPath()));
				} 
				if (movedFrom(child)) {
					from = child.getMovedFromPath();
					ReferenceRegistry.getInstance().handleRename(getUri(from), getUri(file.getFullPath()));
				}
			}
			processAffectedFiles(child);
		}
	}

	private boolean movedFrom(IResourceDelta child) {
		return child.getKind() == IResourceDelta.ADDED && ((child.getFlags() & IResourceDelta.MOVED_FROM) != 0);
	}
	
	private boolean changed(IResourceDelta child) {
		return child.getKind() == IResourceDelta.CHANGED && ((child.getFlags() & IResourceDelta.CONTENT) != 0);
	}
	
	private boolean deleted(IResourceDelta child) {
		return child.getKind() == IResourceDelta.REMOVED && child.getFlags() == IResourceDelta.NO_CHANGE;
	}
	
	private boolean added(IResourceDelta child) {
		return child.getKind() == IResourceDelta.ADDED && child.getFlags() == IResourceDelta.NO_CHANGE;
	}
	
	private URI getUri(IPath path) {
		URI uri = URI.createPlatformResourceURI(path.toPortableString(), true);
		return uri;
	}
}

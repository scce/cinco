/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.pluginregistry.validation;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public interface IMetaPluginValidator {
	
	/**
	 * <b>Deprecated:</b>
	 * Implement {@link #checkOnEdit(EObject)}, {@link #checkOnSave(EObject)}
	 * or {@link #checkOnRequest(EObject)} instead. This method remains only
	 * for the sake of backwards compatibility.
	 * <p>
	 * Checks every CPD/MGL model element after editing the CPD/MGL source file.
	 * @param eObject The current CPD/MGL model element that will be checked.
	 * @return An {@link de.jabc.cinco.meta.core.pluginregistry.validation.Info Info},
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Warning Warning} or
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Error Error} for
	 *         this CPD/MGL model element.<br>
	 *         Use {@link ValidationResult#newInfo(MESSAGE, FEATURE)},
	 *         {@link ValidationResult#newWarning(MESSAGE, FEATURE)}, or
	 *         {@link ValidationResult#newError(MESSAGE, FEATURE)} for convenience.<br>
	 *         Returning {@code null} will result in no validation message.
	 * @see #checkOnEdit(EObject)
	 * @see #checkOnSave(EObject)
	 * @see #checkOnRequest(EObject)
	 */
	public default ValidationResult<String,EStructuralFeature> checkAll(final EObject eObject) {
		return null;
	}
	
	/**
	 * Checks every CPD/MGL model element after editing the CPD/MGL source file.
	 * @param eObject The current CPD/MGL model element that will be checked.
	 * @return An {@link de.jabc.cinco.meta.core.pluginregistry.validation.Info Info},
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Warning Warning} or
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Error Error} for
	 *         this CPD/MGL model element.<br>
	 *         Use {@link ValidationResult#newInfo(MESSAGE, FEATURE)},
	 *         {@link ValidationResult#newWarning(MESSAGE, FEATURE)}, or
	 *         {@link ValidationResult#newError(MESSAGE, FEATURE)} for convenience.<br>
	 *         Returning {@code null} will result in no validation message.
	 * @see #checkOnSave(EObject)
	 * @see #checkOnRequest(EObject)
	 */
	public default ValidationResult<String,EStructuralFeature> checkOnEdit(final EObject eObject) {
		return null;
	}
	
	/**
	 * Checks every CPD/MGL model element after saving the CPD/MGL source file.
	 * @param eObject The current CPD/MGL model element that will be checked.
	 * @return An {@link de.jabc.cinco.meta.core.pluginregistry.validation.Info Info},
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Warning Warning} or
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Error Error} for
	 *         this CPD/MGL model element.<br>
	 *         Use {@link ValidationResult#newInfo(MESSAGE, FEATURE)},
	 *         {@link ValidationResult#newWarning(MESSAGE, FEATURE)}, or
	 *         {@link ValidationResult#newError(MESSAGE, FEATURE)} for convenience.<br>
	 *         Returning {@code null} will result in no validation message.
	 * @see #checkOnEdit(EObject)
	 * @see #checkOnRequest(EObject)
	 */
	public default ValidationResult<String,EStructuralFeature> checkOnSave(final EObject eObject) {
		return null;
	}
	
	/**
	 * Checks every CPD/MGL model element after the user explicitly requests it
	 * by right-clicking the CPD/MGL editor and selecting "Validate".
	 * @param eObject The current CPD/MGL model element that will be checked.
	 * @return An {@link de.jabc.cinco.meta.core.pluginregistry.validation.Info Info},
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Warning Warning} or
	 *         {@link de.jabc.cinco.meta.core.pluginregistry.validation.Error Error} for
	 *         this CPD/MGL model element.<br>
	 *         Use {@link ValidationResult#newInfo(MESSAGE, FEATURE)},
	 *         {@link ValidationResult#newWarning(MESSAGE, FEATURE)}, or
	 *         {@link ValidationResult#newError(MESSAGE, FEATURE)} for convenience.<br>
	 *         Returning {@code null} will result in no validation message.
	 * @see #checkOnEdit(EObject)
	 * @see #checkOnSave(EObject)
	 */
	public default ValidationResult<String,EStructuralFeature> checkOnRequest(final EObject eObject) {
		return null;
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.pluginregistry.validation

import static de.jabc.cinco.meta.core.pluginregistry.impl.PluginRegistryEntryImpl.*
import de.jabc.cinco.meta.core.pluginregistry.PluginRegistry

import mgl.Annotation
import mgl.Attribute
import mgl.Edge
import mgl.GraphModel
import mgl.MglPackage
import mgl.Node
import mgl.NodeContainer
import mgl.ReferencedType
import mgl.UserDefinedType

import java.util.ArrayList
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check

class MetaPluginValidator extends AbstractMetaPluginValidator {
	
	static MetaPluginValidator instance
	
	ArrayList<IMetaPluginValidator> validators = new ArrayList;
	
	static def getInstance() {
		if (instance === null)
			instance = new MetaPluginValidator();

		return instance;
	}

	def putValidator(IMetaPluginValidator validator) {
		validators += validator;
	}
	
	@Check
	def checkMetaPluginRegisteredForAnnotation(Annotation annotation) {
		if (!annotation.metaPluginExists) {
			error(
				'''Annotation "@«annotation.name»" is unknown. Please Register a suitable MetaPlugin or remove annotation.''',
            	MglPackage.Literals::ANNOTATION__NAME
        	)
       	}
	}
	
	@Check(FAST) // Check on edit
	def checkValidators(EObject eObj) {
		validators
			.map[checkAll(eObj)]
			.forEach[applyResult]
	}
	
	@Check(FAST) // Check on edit
	def checkOnEditValidators(EObject eObj) {
		validators
			.map[checkOnEdit(eObj)]
			.forEach[applyResult]
	}
	
	@Check(NORMAL) // Check on save
	def checkOnSaveValidators(EObject eObj) {
		validators
			.map[checkOnSave(eObj)]
			.forEach[applyResult]
	}
	
	@Check(EXPENSIVE) // Check on request
	def checkOnRequestValidators(EObject eObj) {
		validators
			.map[checkOnRequest(eObj)]
			.forEach[applyResult]
	}
	
	private def metaPluginExists(Annotation annotation) {
		val typeID = annotation.typeId
		val pr = PluginRegistry.instance
		return (typeID !== null && !pr.getSuitableMetaPlugins(annotation.name, typeID).nullOrEmpty)
			|| !pr.getSuitableMetaPlugins(annotation.name).nullOrEmpty
	}
	
	private def getTypeId(Annotation annotation) {
		switch annotation.parent {
			NodeContainer:   NODE_CONTAINER_ANNOTATION
			Node:            NODE_ANNOTATION
			Edge:            EDGE_ANNOTATION
			GraphModel:      GRAPH_MODEL_ANNOTATION
			UserDefinedType: TYPE_ANNOTATION
			ReferencedType:  PRIME_ANNOTATION
			Attribute:       ATTRIBUTE_ANNOTATION
			default:         null
		}
	}
}

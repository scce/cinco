/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.pluginregistry.validation

import de.jabc.cinco.meta.core.pluginregistry.PluginRegistry
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check
import productDefinition.Annotation
import productDefinition.ProductDefinitionPackage

class CPDMetaPluginValidator extends AbstractCPDMetaPluginValidator {
	
	static CPDMetaPluginValidator instance
		
	static def getInstance() {
		if (instance === null) {
			instance = new CPDMetaPluginValidator()
		}
		return instance
	}
	
	@Check
	def checkMetaPluginRegisteredForAnnotation(Annotation annotation) {
		if (!annotation.metaPluginExists) {
			error(
				'''Annotation "@«annotation.name»" is unknown. Please register a suitable CPD MetaPlugin or remove annotation.''',
				annotation,
				ProductDefinitionPackage.Literals::ANNOTATION__NAME
			)
		}
	}
	
	@Check(FAST) // Check on edit
	def checkValidators(EObject eObj) {
		PluginRegistry::instance
			.pluginCPDGenerators
			.map[validator]
			.map[checkAll(eObj)]
			.forEach[applyResult]
	}
	
	@Check(FAST) // Check on edit
	def checkOnEditValidators(EObject eObj) {
		PluginRegistry::instance
			.pluginCPDGenerators
			.map[validator]
			.map[checkOnEdit(eObj)]
			.forEach[applyResult]
	}
	
	@Check(NORMAL) // Check on save
	def checkOnSaveValidators(EObject eObj) {
		PluginRegistry::instance
			.pluginCPDGenerators
			.map[validator]
			.map[checkOnSave(eObj)]
			.forEach[applyResult]
	}
	
	@Check(EXPENSIVE) // Check on request
	def checkOnRequestValidators(EObject eObj) {
		PluginRegistry::instance
			.pluginCPDGenerators
			.map[validator]
			.map[checkOnRequest(eObj)]
			.forEach[applyResult]
	}
	
	private def boolean metaPluginExists(Annotation annotation) {
		return !PluginRegistry::instance
			.getSuitableCPDMetaPlugins(annotation.name)
			.nullOrEmpty
	}

}

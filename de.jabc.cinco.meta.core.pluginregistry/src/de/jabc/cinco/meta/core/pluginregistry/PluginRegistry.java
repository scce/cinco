/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.pluginregistry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtend.typesystem.emf.EcoreUtil2;

import de.jabc.cinco.meta.core.pluginregistry.impl.PluginRegistryEntryImpl;

public class PluginRegistry {
	private static PluginRegistry instance;
	HashMap<EPackage,String> genModelMap;
	private HashMap<String, EPackage> ecoreMap;
	private HashMap<String,IMGLMetaPlugin> pluginGenerators;
	private HashSet<PluginRegistryEntryImpl> metaPlugins;
	private HashMap<String, Set<String>> mglDependentPlugins;
	private HashMap<String, Set<String>> usedPlugins;
	private HashMap<String, Set<String>> mglDependentFragments;
	private HashMap<String, Set<String>> usedFragments;
	
	private Set<CPDAnnotation> cpdPluginGenerators;
	
	public HashMap<String, IMGLMetaPlugin> getPluginGenerators() {
		return pluginGenerators;
	}
	
	public void setPluginGenerators(
			HashMap<String, IMGLMetaPlugin> pluginGenerators) {
		this.pluginGenerators = pluginGenerators;
	}
	
	public Set<CPDAnnotation> getPluginCPDGenerators() {
		return cpdPluginGenerators;
	}
	
	public void setCPDPluginGenerators(
			Set<CPDAnnotation> cpdPluginGenerators) {
		this.cpdPluginGenerators = cpdPluginGenerators;
	}
	
	private PluginRegistry(){
		this.genModelMap = new HashMap<EPackage,String>();
		this.ecoreMap = new HashMap<String,EPackage>();
		this.metaPlugins = new HashSet<PluginRegistryEntryImpl>();
		
		this.pluginGenerators = new HashMap<String,IMGLMetaPlugin>();
		
		this.cpdPluginGenerators = new HashSet<CPDAnnotation>();
		
		EPackage abstractGraphModel = EcoreUtil2.getEPackage("platform:/plugin/de.jabc.cinco.meta.core.mgl.model/model/GraphModel.ecore");
		ecoreMap.put("abstractGraphModel", abstractGraphModel);
		genModelMap.put(abstractGraphModel,"platform:/plugin/de.jabc.cinco.meta.core.mgl.model/model/GraphModel.genmodel");
		
		this.mglDependentPlugins = new HashMap<>();
		this.usedPlugins = new HashMap<>();
		this.mglDependentFragments = new HashMap<>();
		this.usedFragments = new HashMap<>();
	}
	
	public static PluginRegistry getInstance() {
		if(instance==null){
			instance = new PluginRegistry();
		}
		return instance;
	}
	
	public HashMap<String, EPackage> getRegisteredEcoreModels() {
	  return this.ecoreMap;
	}
	
	public HashMap<EPackage, String> getGenModelMap() {
	  return this.genModelMap;
	}
	
	public void registerMetaPlugin(PluginRegistryEntryImpl pEntry) {
		String firstRecognizedAnnotation = (String) pEntry.getAllRecognizedAnnotations().toArray()[0];
		this.metaPlugins.add(pEntry);
		this.genModelMap.put(pEntry.getUsedEPackage(), pEntry.getGenModelPath());
		this.mglDependentPlugins.put(firstRecognizedAnnotation, pEntry.getMGLDependentPlugins());
		this.usedPlugins.put(firstRecognizedAnnotation, pEntry.getUsedPlugins());
		this.mglDependentFragments.put(firstRecognizedAnnotation, pEntry.getMGLDependentFragments());
		this.usedFragments.put(firstRecognizedAnnotation, pEntry.getUsedFragments());
		for (String annotation: pEntry.getAllRecognizedAnnotations()) {
			this.ecoreMap.put(annotation, pEntry.getUsedEPackage());
			this.pluginGenerators.put(annotation, pEntry.getMetaPluginService());
		}
	}
	
	public void registerCPDMetaPlugin(CPDAnnotation annotation)	{
		cpdPluginGenerators.add(annotation);
	}
	
	public Set<PluginRegistryEntry> getSuitableMetaPlugins(String annotation){
		HashSet<PluginRegistryEntry> set = new HashSet<PluginRegistryEntry>();
		for(PluginRegistryEntry p: metaPlugins){
			if(p.doesRecognizeAnnotation(annotation))
				set.add(p);
		}
		return set;
	}
	
	public Set<CPDAnnotation> getSuitableCPDMetaPlugins(String annotation){
		return this.cpdPluginGenerators.stream().filter(n->n.getAnnotationName().equals(annotation)).collect(Collectors.toSet());
	}
	
	public Set<String> getAnnotations(int annotationType){
		HashSet<String> annotations = new HashSet<String>();
		for(PluginRegistryEntryImpl entry: metaPlugins){
			annotations.addAll(entry.getRecognizedAnnotations(annotationType));
		}
		return annotations;
	}
	
	public Set<PluginRegistryEntry> getSuitableMetaPlugins(String annotation,int annotationType){
		HashSet<PluginRegistryEntry> set = new HashSet<PluginRegistryEntry>();
		for(PluginRegistryEntryImpl p: metaPlugins){
			if(p.doesRecognizeAnnotation(annotation,annotationType))
				set.add(p);
		}
		return set;
	}
	
	public HashMap<String,Set<String>> getMGLDependentPlugins() {
		return this.mglDependentPlugins;
	}
	
	public HashMap<String,Set<String>> getUsedPlugins() {
		return this.usedPlugins;
	}
	
	public HashMap<String,Set<String>> getMGLDependentFragments() {
		return this.mglDependentFragments;
	}
	
	public HashMap<String,Set<String>> getUsedFragments() {
		return this.usedFragments;
	}

}

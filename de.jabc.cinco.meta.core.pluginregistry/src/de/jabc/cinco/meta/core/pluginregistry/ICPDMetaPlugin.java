/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.pluginregistry;

import java.util.List;

import org.eclipse.core.resources.IProject;

import mgl.MGLModel;
import productDefinition.Annotation;
import productDefinition.CincoProduct;

public interface ICPDMetaPlugin {
	
	/**
	 * Executes the CPD meta plugin. This method will only be called, if at
	 * least one model element in any MGL has a corresponding annotation. This
	 * method will be called only once for all annotations.
	 * 
	 * @param cpdAnnotations A list of all {@link Annotation CPD-Annotations}
	 *                       that correspond to this CPD meta plugin.
	 * @param generatedMGLs  A list of {@link MGLModel MGLModels} that were newly
	 *                       generated. (Usually only MGLs with changes are
	 *                       generated. To access all MGLModels, whether or not
	 *                       they were generated, use {@code allMgls}.)
	 * @param allMGLs        A list of all {@link MGLModel MGLModels} listed in
	 *                       the CPD file including all of their imported MGLs.
	 * @param cpd            The {@link CincoProduct} object representing the
	 *                       definitions in the CPD file.
	 * @param mainProject    The {@link IProject} of the CPD file.
	 */
	public void executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject);
	
	/**
	 * Returns a priority for execution order of CPD meta plugins.
	 * Meta plugins with a higher priority get executed first.
	 * Negative priorities are allowed. The default priority is 0.
	 */
	public default int getCPDMetaPluginPriority() {
		return 0;
	}
	
	/**
	 * Compares this CPD meta plugin with the {@code other} one for
	 * {@linkplain #getMGLMetaPluginPriority() priority} order. Returns a
	 * negative integer, zero, or a positive integer as this plugin has a
	 * lower, equal, or higher priority than the {@code other} one.
	 * (Thus, lowest priority first.)
	 */
	public default int comparePriorityTo(ICPDMetaPlugin other) {
		// Lowest priority first
		return this.getCPDMetaPluginPriority() - other.getCPDMetaPluginPriority();
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.pluginregistry.validation

import org.eclipse.xtend.lib.annotations.Data

@Data abstract class ValidationResult<MESSAGE, FEATURE> {
	
	MESSAGE message
	FEATURE feature
	
	static class Error<MESSAGE, FEATURE> extends ValidationResult<MESSAGE, FEATURE> {
		new(MESSAGE message, FEATURE feature) {
			super(message, feature)
		}
	}
	
	static class Warning<MESSAGE, FEATURE> extends ValidationResult<MESSAGE, FEATURE> {
		new(MESSAGE message, FEATURE feature) {
			super(message, feature)
		}
	}
	
	static class Info<MESSAGE, FEATURE> extends ValidationResult<MESSAGE, FEATURE> {
		new(MESSAGE message, FEATURE feature) {
			super(message, feature)
		}
	}
	
	static def <MESSAGE, FEATURE> newError(MESSAGE message, FEATURE feature) {
		new Error(message, feature)
	}
	
	static def <MESSAGE, FEATURE> newWarning(MESSAGE message, FEATURE feature) {
		new Warning(message, feature)
	}
	
	static def <MESSAGE, FEATURE> newInfo(MESSAGE message, FEATURE feature) {
		new Info(message, feature)
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.forceGenerate

import de.jabc.cinco.meta.core.ui.handlers.CincoProductGenerationHandler
import org.eclipse.jface.action.IAction
import org.eclipse.jface.viewers.ISelection
import org.eclipse.ui.IActionDelegate
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.handlers.IHandlerService

class ForceGenerateAction implements IActionDelegate {
	
	new() {
		// Intentionally left blank
	}

	override void run(IAction action) {
		CincoProductGenerationHandler.forceNextGeneration();
		val handlerService = PlatformUI.getWorkbench().getService(IHandlerService)
		handlerService.executeCommand("de.jabc.cinco.meta.core.ui.cincoproductgenerationcommand", null)
	}
	
	override selectionChanged(IAction action, ISelection selection) {
		// Intentionally left blank
	}
	
}

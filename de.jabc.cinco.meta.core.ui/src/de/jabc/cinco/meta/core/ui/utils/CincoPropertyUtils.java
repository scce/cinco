/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.IEMFListProperty;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class CincoPropertyUtils {

	/**
	 * This method iterates over the {@link bo}'s class and super classes and
	 * retrieves all EAttributes.
	 * 
	 * @param bo
	 *            The EObject for which the EAttributes should be retrieved
	 * @param attributesMap
	 * @return All EAttributes of EObject {@link bo}
	 */
	public static List<EStructuralFeature> getAllEStructuralFeatures(Class<? extends Object> clazz,
			Map<Class<? extends EObject>, List<EStructuralFeature>> attributesMap) {
		Class<? extends Object> currentClass = clazz;
		List<EStructuralFeature> attributes = new ArrayList<EStructuralFeature>();
		while (currentClass != null) {
			List<EStructuralFeature> list = attributesMap.get(currentClass);
			if (list != null)
				attributes.addAll(list);
			currentClass = (Class<? extends Object>) currentClass.getSuperclass();
		}
		return attributes;
	}

	public static IEMFListProperty getAllListProperties(Class<? extends Object> clazz,
			Map<Class<? extends EObject>, IEMFListProperty> emfListPropertesMap) {
		Class<? extends Object> currentClass = clazz;
		List<IEMFListProperty> properties = new ArrayList<IEMFListProperty>();
		while (currentClass != null) {
			IEMFListProperty emfProperty = emfListPropertesMap.get(currentClass);
			if (emfProperty != null) {
				properties.add(emfProperty);
			}
			currentClass = (Class<? extends Object>) currentClass.getSuperclass();
		}

		IEMFListProperty retVal = EMFProperties.multiList(properties.toArray(new IEMFListProperty[properties.size()]));

		return retVal;
	}

	public static EStructuralFeature getLabeledFeature(Class<? extends Object> clazz, Map<Class<? extends EObject>, EStructuralFeature> typeLabeles) {
		Class<? extends Object> currentClass = clazz;
		while (currentClass != null) {
			EStructuralFeature feature = typeLabeles.get(currentClass);
			if (feature  != null)
				return feature;
			currentClass = (Class<? extends Object>) currentClass.getSuperclass();
		}
		return null;
	}
	
}

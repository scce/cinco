/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;

import de.jabc.cinco.meta.core.ui.CincoNature;

public class ToggleCincoNatureHandler extends AbstractHandler{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try{
			StructuredSelection selection = (StructuredSelection)HandlerUtil.getActiveMenuSelection(event);
			IProject project = (IProject)selection.getFirstElement();
			CincoNature nature = new CincoNature();
			nature.setProject(project);
			
			if(!project.hasNature("de.jabc.cinco.meta.core.CincoNature")){
				nature.configure();
			}else{
				nature.deconfigure();
			}
		}catch(ClassCastException ce){} catch (CoreException e) {
			throw new ExecutionException("Could not toggle Cinco Nature",e);
		}
		return null;
	}

}

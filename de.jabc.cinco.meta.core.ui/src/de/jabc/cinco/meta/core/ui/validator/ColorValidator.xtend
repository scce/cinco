/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.validator

import java.util.regex.Pattern
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.swt.widgets.Text

class ColorValidator extends ControlValidator<Text, String> {
	
	val public static HEX_PATTERN  = "^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$"
	val public static RGB_PATTERN  = "^([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3})$"
	val public static RGBA_PATTERN = "^([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3})$"
	
	val public static HEX  = "hex"
	val public static RGB  = "rgb"
	val public static RGBA = "rgba"
	
	val String colorFormat
	
	new (EObject bo, EStructuralFeature attribute, Text control, String colorFormat) {
		super (bo, attribute, control)
		if (!colorFormat.isColorFormat) {
			throw new IllegalArgumentException('''Unknown color format "«colorFormat»". Must be "«HEX»", "«RGB»", or "«RGBA»"."''')
		}
		this.colorFormat = colorFormat
	}
	
	override hasValidInput(Text control) {
		val colorString = control.value
		val isValid = switch colorFormat {
			case HEX:  colorString.isHex
			case RGB:  colorString.isRGB
			case RGBA: colorString.isRGBA
			default:   false
		}
		if (!isValid) {
			validationErrorMessage = "Color String is malformed."
		}
		return isValid
	}
	
	override getValue(Text control) {
		control.text
	}
	
	def static boolean isColorFormat(String colorFormat) {
		#[HEX, RGB, RGBA].contains(colorFormat)
	}
	
	def static boolean isHex(String colorString) {
		if (colorString.nullOrEmpty) return false
		val p = Pattern.compile(HEX_PATTERN)
		val m = p.matcher(colorString)
		return m.matches
	}
	
	def static boolean isRGB(String colorString) {
		if (colorString.nullOrEmpty) return false
		val p = Pattern.compile(RGB_PATTERN)
		val m = p.matcher(colorString)
		if (!m.matches) return false
		try {
			val r = Integer.parseInt(m.group(1))
			val g = Integer.parseInt(m.group(2))
			val b = Integer.parseInt(m.group(3))
			return #[r, g, b].forall[ v | (0..255).contains(v) ]
		}
		catch (NumberFormatException e) {
			return false
		}
	}
	
	def static boolean isRGBA(String colorString) {
		if (colorString.nullOrEmpty) return false
		val p = Pattern.compile(RGBA_PATTERN)
		val m = p.matcher(colorString)
		if (!m.matches) return false
		try {
			val r = Integer.parseInt(m.group(1))
			val g = Integer.parseInt(m.group(2))
			val b = Integer.parseInt(m.group(3))
			val a = Integer.parseInt(m.group(4))
			return #[r, g, b, a].forall[ v | (0..255).contains(v) ]
		}
		catch (NumberFormatException e) {
			return false
		}
	}
	
}

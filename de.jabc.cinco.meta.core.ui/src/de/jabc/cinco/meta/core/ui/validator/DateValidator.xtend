/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.validator

import java.util.Calendar
import java.util.Date
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.swt.widgets.DateTime

import static java.util.Calendar.DAY_OF_MONTH
import static java.util.Calendar.HOUR_OF_DAY
import static java.util.Calendar.MINUTE
import static java.util.Calendar.MONTH
import static java.util.Calendar.SECOND
import static java.util.Calendar.YEAR

class DateValidator extends ControlValidator<DateTime, Date> {
	
	val DateTime dateControl
	val DateTime timeControl
	
	new (EObject bo, EStructuralFeature attribute, DateTime dateControl, DateTime timeControl) {
		super (bo, attribute, dateControl)
		this.dateControl = dateControl
		this.timeControl = timeControl
	}
		
	override hasValidInput(DateTime control) {
		true
	}
	
	override getValue(DateTime control) {
		val calendar = Calendar.instance
		calendar.clear
		calendar.set(YEAR,         dateControl.year   )
		calendar.set(MONTH,        dateControl.month  )
		calendar.set(DAY_OF_MONTH, dateControl.day    )
		calendar.set(HOUR_OF_DAY,  timeControl.hours  )
		calendar.set(MINUTE,       timeControl.minutes)
		calendar.set(SECOND,       timeControl.seconds)
		return calendar.time
	}
	
}

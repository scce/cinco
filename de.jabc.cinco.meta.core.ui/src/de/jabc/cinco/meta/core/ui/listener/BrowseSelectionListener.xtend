/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.listener

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import java.util.List
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.swt.SWT
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.widgets.FileDialog
import org.eclipse.swt.widgets.Shell
import org.eclipse.swt.widgets.Text

class BrowseSelectionListener implements SelectionListener {
	
	extension WorkbenchExtension = new WorkbenchExtension
	extension WorkspaceExtension = new WorkspaceExtension
		
	val Shell shell
	val Text text
	val List<String> fileExtensions

	new (Shell shell, Text text, List<String> fileExtensions) {
		this.shell = shell
		this.text = text
		this.fileExtensions = fileExtensions
	}

	override widgetSelected(SelectionEvent e) {
		val dialog = new FileDialog(shell, SWT.OPEN)
		val project =  activeEditor.project
		if (project !== null) {
			val location = project.location
			println('''Setting browse location via project: «location»''')
			dialog.filterPath = location.toString
		}
		else {
			val location = ResourcesPlugin.workspace.root.location
			println('''Setting browse location via workspace: «location»''')
			dialog.filterPath = location.toString
		}
		val extensions = fileExtensions
			.reject[nullOrEmpty]
			.join("")[ ext | '''*.«ext»;''' ]
		if (!extensions.empty) {
			dialog.filterExtensions = #[extensions]
		}
		val path = dialog.open
		if (path !== null) {
			val files = workspaceRoot.getFiles[ f | f.location.toOSString == path ]
			var iFile = if (files.empty) null else files.head
			text.text = iFile?.projectRelativePath?.toString?: path
		}
	}

	override widgetDefaultSelected(SelectionEvent e) {
		// Do mothing
	}
	
}

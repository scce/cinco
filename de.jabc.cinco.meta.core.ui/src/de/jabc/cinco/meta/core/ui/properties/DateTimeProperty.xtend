/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties

import java.util.Calendar
import java.util.Date
import org.eclipse.core.databinding.observable.list.IObservableList
import org.eclipse.core.databinding.observable.map.IObservableMap
import org.eclipse.core.databinding.observable.set.IObservableSet
import org.eclipse.core.databinding.observable.value.IObservableValue
import org.eclipse.jface.databinding.swt.WidgetValueProperty
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.DateTime

import static java.util.Calendar.DAY_OF_MONTH
import static java.util.Calendar.HOUR_OF_DAY
import static java.util.Calendar.MINUTE
import static java.util.Calendar.MONTH
import static java.util.Calendar.SECOND
import static java.util.Calendar.YEAR

class DateTimeProperty extends WidgetValueProperty<DateTime, Date> {
	
	val DateTime dateControl
	val DateTime timeControl
	
	new (DateTime dateControl, DateTime timeControl) {
		super(#[SWT.FocusOut])
		this.dateControl = dateControl
		this.timeControl = timeControl
	}
	
	override getValueType() {
		Date
	}
	
	override doGetValue(DateTime source) {
		val calendar = Calendar.instance
		calendar.clear
		if (source === dateControl || source == timeControl) {
			calendar.set(YEAR,         dateControl.year)
			calendar.set(MONTH,        dateControl.month)
			calendar.set(DAY_OF_MONTH, dateControl.day)
			calendar.set(HOUR_OF_DAY,  timeControl.hours)
			calendar.set(MINUTE,       timeControl.minutes)
			calendar.set(SECOND,       timeControl.seconds)
		}
		else if (source.style.bitwiseAnd(SWT.TIME) != 0) {
			calendar.set(HOUR_OF_DAY,  source.hours)
			calendar.set(MINUTE,       source.minutes)
			calendar.set(SECOND,       source.seconds)
		}
		else {
			calendar.set(YEAR,         source.year)
			calendar.set(MONTH,        source.month)
			calendar.set(DAY_OF_MONTH, source.day)
		}
		return calendar.time
	}
	
	override doSetValue(DateTime source, Date value) {
		if (value === null) {
			return
		}
		val calendar = Calendar.instance
		calendar.time = value
		if (source === dateControl || source == timeControl) {
			dateControl.year    = calendar.get(YEAR)
			dateControl.month   = calendar.get(MONTH)
			dateControl.day     = calendar.get(DAY_OF_MONTH)
			timeControl.hours   = calendar.get(HOUR_OF_DAY)
			timeControl.minutes = calendar.get(MINUTE)
			timeControl.seconds = calendar.get(SECOND)
		}
		else if (source.style.bitwiseAnd(SWT.TIME) != 0) {
			source.hours   = calendar.get(HOUR_OF_DAY)
			source.minutes = calendar.get(MINUTE)
			source.seconds = calendar.get(SECOND)
		}
		else {
			source.year    = calendar.get(YEAR)
			source.month   = calendar.get(MONTH)
			source.day     = calendar.get(DAY_OF_MONTH)
		}
	}
	
	override <M extends DateTime> observeDetail(IObservableValue<M> master) {
		super.observeDetail(master)
	}
	
	override <M extends DateTime> observeDetail(IObservableList<M> master) {
		super.observeDetail(master)
	}
	
	override <M extends DateTime> observeDetail(IObservableSet<M> master) {
		super.observeDetail(master)
	}
	
	override <K,V extends DateTime> observeDetail(IObservableMap<K, V> master) {
		super.observeDetail(master)
	}
	
}

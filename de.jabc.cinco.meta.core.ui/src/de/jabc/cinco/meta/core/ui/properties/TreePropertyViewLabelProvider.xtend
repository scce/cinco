/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties

import graphmodel.ModelElement
import graphmodel.Type
import graphmodel.internal.InternalIdentifiableElement
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.jface.viewers.LabelProvider

import static extension de.jabc.cinco.meta.core.ui.utils.CincoPropertyUtils.getLabeledFeature

class TreePropertyViewLabelProvider extends LabelProvider {
	
	val Map<Class<? extends EObject>, EStructuralFeature> typeLabelMap
	
	new (Map<Class<? extends EObject>, EStructuralFeature> typeLabelMap) {
		super()
		this.typeLabelMap = typeLabelMap
	}
	
	override getText(Object object) {
		switch object {
			ModelElement:                object.internalElement_.element.eClass.name
			InternalIdentifiableElement: object.element.eClass.name
			Type:                        object.typeLabel
			EObject:                     object.referenceName
			default:                     super.getText(object)
		}
	}
	
	def private String getTypeLabel(Type object) {
		val internalType = object.internalElement_
		val attribute = internalType.class.getLabeledFeature(typeLabelMap)
		if (attribute === null) {
			return object.referenceName
		}
		else {
			switch it: internalType.eGet(attribute) {
				String case !empty: return it
				Enum<?>:            return toString
				default:            return '''<<«attribute.name»>>'''
			}
		}
	}
	
	def private String getReferenceName(EObject object) {
		var eContainer = object.eContainer
		for (ref: eContainer.eClass.getEAllReferences) {
			switch it: eContainer.eGet(ref, true) {
				List<?> case contains(object): return ref.name
				case object:                   return ref.name
			}
		}
	}
	
}

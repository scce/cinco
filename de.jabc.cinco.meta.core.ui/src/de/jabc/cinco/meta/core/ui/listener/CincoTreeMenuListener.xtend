/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.listener

import graphmodel.GraphModel
import graphmodel.ModelElement
import graphmodel.Type
import graphmodel.internal.InternalGraphModel
import graphmodel.internal.InternalIdentifiableElement
import graphmodel.internal.InternalModelElement
import java.util.List
import java.util.Map
import java.util.Set
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.transaction.RecordingCommand
import org.eclipse.emf.transaction.TransactionalEditingDomain
import org.eclipse.jface.action.Action
import org.eclipse.jface.action.IMenuListener2
import org.eclipse.jface.action.IMenuManager
import org.eclipse.jface.action.MenuManager
import org.eclipse.jface.action.Separator
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.viewers.StructuredViewer

import static extension de.jabc.cinco.meta.core.ui.utils.CincoPropertyUtils.getAllEStructuralFeatures
import static extension org.eclipse.emf.ecore.util.EcoreUtil.create
import static extension org.eclipse.emf.ecore.util.EcoreUtil.delete
import static extension org.eclipse.emf.transaction.util.TransactionUtil.getEditingDomain

class CincoTreeMenuListener implements IMenuListener2 {
	
	extension TransactionalEditingDomain.Factory = TransactionalEditingDomain.Factory.INSTANCE
	
	StructuredViewer viewer
	Map<Class<? extends EObject>, List<EStructuralFeature>> referencesMap
	Set<EClass> disableCreate
	Set<EClass> disableDelete
	
	new (StructuredViewer viewer, Map<Class<? extends EObject>, List<EStructuralFeature>> referencesMap, Set<EClass> disableCreate, Set<EClass> disableDelete) {
		this.viewer = viewer
		this.referencesMap = referencesMap
		this.disableCreate = disableCreate
		this.disableDelete = disableDelete
	}
	
	// IMenuListener2
	override menuAboutToShow(IMenuManager mainMenu) {
		// Get the selected item of the tree view
		var selection = viewer.selection
		if (selection instanceof IStructuredSelection) {
			var object = selection.firstElement
			if (object instanceof Type) {
				object = object.internalElement_
			}
			if (object instanceof EObject) {
				// Get all references to UserDefinedTypes
				val features = object.class.getAllEStructuralFeatures(referencesMap)?.sortBy[name]
				if (features !== null) {
					for (feature: features) {
						// Find all non-abstract sub-types of feature's return type
						val eType = feature.EType as EClass
						val ePackage = eType.EPackage
						val subPackages = ePackage.ESubpackages + #[ePackage]
						val subTypes = subPackages
							.flatMap[EClassifiers]
							.filter(EClass)
							.reject[isAbstract]
							.filter[ subType | eType.isSuperTypeOf(subType) ]
							.reject[ subType | disableCreate.contains(subType) ]
							.sortBy[name]
						val subTypesCount = subTypes.size
						val label = '''Add «feature.name»'''
						if (subTypesCount == 0) {
							// All sub-types are abstract:
							// Do nothing
						}
						else if (subTypesCount == 1) {
							// Exactly 1 sub-type found:
							// Add a simple "add" action to the menu
							mainMenu.addAddAction(object, feature, subTypes.head, label)
						}
						else if (object.canAdd(feature)) {
							// More than one sub-type found and we can add an instance to the feature:
							// Create a sub-menu with "add" actions for each sub-type
							val subMenu = new MenuManager(label)
							for (subType: subTypes) {
								subMenu.addAddAction(object, feature, subType, subType.name)
							}
							mainMenu.add(subMenu)
						}
						else {
							// More than one sub-type found, but we can not add any more instances to the feature:
							// Add an always disabled "add" action
							mainMenu.addDisabledAction(label)
						}
					}
				}
				// Add a "remove" action to remove the selected item itself
				mainMenu.addSeparator
				mainMenu.addRemoveAction(object)
			}
		}
	}
	
	// IMenuListener2
	override menuAboutToHide(IMenuManager manager) {
		// Do nothing
	}
	
	def private void addAddAction(IMenuManager menu, EObject eObject, EStructuralFeature feature, EClass type, String label) {
		
		val action = new Action {
			
			override getText() {
				label
			}

			override isEnabled() {
				eObject.canAdd(feature)
			}

			override run() {
				val newValue = type.create
				eObject.transaction [
					if (feature.upperBound == 1) {
						eObject.eSet(feature, newValue)
					}
					else {
						val result = eObject.eGet(feature) as List<EObject>
						result.add(newValue)
					}
				]
				viewer.refresh
			}
			
		}
		
		menu.add(action)
		
	}
	
	def private void addRemoveAction(IMenuManager menu, EObject eObject) {
		
		val action = new Action {
			
			override getText() {
				"Remove"
			}

			override isEnabled() {
				switch eObject {
					ModelElement:         false
					GraphModel:           false
					InternalModelElement: false
					InternalGraphModel:   false
					default:              !disableDelete.contains(eObject.eClass)
				}
			}

			override run() {
				eObject.transaction [
					if (eObject instanceof InternalIdentifiableElement) {
						eObject.element.delete
					}
					eObject.delete
				]
				viewer.refresh
			}
			
		}
		
		menu.add(action)
		
	}
	
	def private void addDisabledAction(IMenuManager menu, String label) {
		
		val action = new Action {
			
			override getText() {
				label
			}

			override isEnabled() {
				false
			}

			override run() {
				// Do nothing
			}
			
		}
		
		menu.add(action)
		
	}
	
	def private void addSeparator(IMenuManager menu) {
		menu.add(new Separator)
	}
	
	def private boolean canAdd(EObject eObject, EStructuralFeature feature) {
		val upperBound = feature.upperBound
		if (upperBound == 1) {
			return eObject.eGet(feature) === null
		}
		else {
			val list = eObject.eGet(feature) as EList<EObject>
			return list.size < upperBound || upperBound == -1
		}
	}
	
	def private void transaction(EObject eObject, () => void transaction) {
		val domain = eObject.editingDomain?: eObject.eResource.resourceSet.createEditingDomain
		val command = new RecordingCommand(domain) {
			override protected doExecute() {
				transaction.apply
			}
		}
		domain.commandStack.execute(command)
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.converter

import java.util.regex.Pattern
import org.eclipse.core.databinding.conversion.IConverter
import org.eclipse.swt.graphics.Color
import org.eclipse.swt.widgets.Display

import static extension de.jabc.cinco.meta.core.ui.validator.ColorValidator.*

class StringToColorConverter implements IConverter<String, Color> {
	
	override getFromType() {
		String
	}

	override getToType() {
		Color
	}

	override convert(String it) {
		try {
			switch it {
				case isHex: {
					val p = Pattern.compile(HEX_PATTERN)
					val m = p.matcher(it)
					m.matches
					val r = Integer.parseInt(m.group(1), 16)
					val g = Integer.parseInt(m.group(2), 16)
					val b = Integer.parseInt(m.group(3), 16)
					return new Color(Display.current, r, g, b)
				}
				case isRGB: {
					val p = Pattern.compile(RGB_PATTERN)
					val m = p.matcher(it)
					m.matches
					val r = Integer.parseInt(m.group(1))
					val g = Integer.parseInt(m.group(2))
					val b = Integer.parseInt(m.group(3))
					return new Color(Display.current, r, g, b)
				}
				case isRGBA: {
					val p = Pattern.compile(RGBA_PATTERN)
					val m = p.matcher(it)
					m.matches
					val r = Integer.parseInt(m.group(1))
					val g = Integer.parseInt(m.group(2))
					val b = Integer.parseInt(m.group(3))
					val a = Integer.parseInt(m.group(4))
					return new Color(Display.current, r, g, b, a)
				}
				default: {
					return null
				}
			}
		}
		catch (IllegalArgumentException e) { // Color constructor
			e.printStackTrace
			return null
		}
	}
	
}

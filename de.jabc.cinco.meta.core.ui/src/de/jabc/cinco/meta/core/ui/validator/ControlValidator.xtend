/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.validator

import graphmodel.IdentifiableElement
import graphmodel.internal.InternalIdentifiableElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.jface.fieldassist.ControlDecoration
import org.eclipse.jface.fieldassist.FieldDecorationRegistry
import org.eclipse.swt.events.ModifyEvent
import org.eclipse.swt.events.ModifyListener
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.widgets.Control

import static de.jabc.cinco.meta.core.ui.properties.CincoPropertyView.canAttributeChange
import static org.eclipse.jface.fieldassist.FieldDecorationRegistry.DEC_ERROR
import static org.eclipse.swt.SWT.LEFT
import static org.eclipse.swt.SWT.TOP

/**
 * A abstract class for validation of SWT's {@link Control Controls}.
 * <p>
 * Implement {@link #hasValidInput(C) hasValidInput(C)} to validate the control
 * and use {@link #setValidationErrorMessage(String) setValidationErrorMessage(String)}
 * to set an error message.
 * <p>
 * Implement {@link #getValue(C) getValue(C)}, so that it returns the current
 * value of the control.  
 * 
 * @param C the type of the {@link Control}, that will be validated.
 * @param V the tvpe of the controls's value.
 */
abstract class ControlValidator<C extends Control, V> implements SelectionListener, ModifyListener {
	
	val protected static DEFAULT_VALIDATION_ERROR_MESSAGE = "This input is invalid."
	
	val protected IdentifiableElement element
	val protected String attribute
	val protected C control
	var protected ControlDecoration decoration
	var protected String validationErrorMessage
	
	new (EObject bo, EStructuralFeature attribute, C control) {
		this.element = switch bo {
			InternalIdentifiableElement: bo.element
			IdentifiableElement:         bo
		}
		this.attribute = attribute.name
		this.control = control
	}
	
	// SelectionListener
	override widgetSelected(SelectionEvent event) {
		validate
	}
	
	// SelectionListener
	override widgetDefaultSelected(SelectionEvent e) {
		// Do nothing
	}
	
	// ModifyListener
	override modifyText(ModifyEvent event) {
		validate
	}
	
	/**
	 * Performs validation using {@link #hasValidInput(C) hasValidInput(C)}.
	 * If that returns {@code true} (meaning input is valid), {@link
	 * de.jabc.cinco.meta.core.ui.properties.CincoPropertyView#canAttributeChange
	 * CincoPropertyView.canAttributeChange} will also be checked.
	 * <p>
	 * If both checks succeed, the error {@link #decoration decoration} will be
	 * hidden. Otherwise, if one of the checks fails, the decoration will be shown. 
	 */
	def protected void validate() {
		if (control.hasValidInput) {
			validationErrorMessage = canAttributeChange.apply(element, attribute, control.value)
			if (validationErrorMessage === null) {
				hideDecoration
				return
			}
		}
		showDecoration
	}
	
	/**
	 * Returns {@code true} if the {@code control} has a valid input.
	 */
	def protected boolean hasValidInput(C control)
	
	/**
	 * Returns the current value of the {@code control}.
	 */
	def protected V getValue(C control)
	
	/**
	 * Sets an error message, that will be displayed at the control if the
	 * control has an invalid input. If {@code message} is {@code null} or
	 * empty, a default error message will be displayed.
	 */
	def protected void setValidationErrorMessage(String message) {
		validationErrorMessage = message
	}
	
	/**
	 * The current error message for displaying at the control if the control
	 * has an invalid input. If the error message was set to {@code null} or is
	 * empty, a default error message will be displayed.
	 */
	def protected String getValidationErrorMessage() {
		if (validationErrorMessage.nullOrEmpty) {
			DEFAULT_VALIDATION_ERROR_MESSAGE
		}
		else {
			validationErrorMessage
		}
	}

	/** 
	 * Shows a decoration with an error message ({@link #validationErrorMessage})
	 * at the {@link #control}.
	 */
	def protected void showDecoration() {
		if (decoration === null) {
			decoration = new ControlDecoration(control, TOP.bitwiseOr(LEFT))
			decoration.image = FieldDecorationRegistry.getDefault.getFieldDecoration(DEC_ERROR).image
		}
		decoration.descriptionText = getValidationErrorMessage
		decoration.show
		control.redraw
	}

	/** 
	 * Hides the decoration at the {@link #control}.
	 */
	def protected void hideDecoration() {
		decoration?.hide
		control.redraw
	}
	
}

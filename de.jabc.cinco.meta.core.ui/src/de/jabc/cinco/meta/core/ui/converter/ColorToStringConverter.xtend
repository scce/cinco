/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.converter

import org.eclipse.core.databinding.conversion.IConverter
import org.eclipse.swt.graphics.Color

import static extension de.jabc.cinco.meta.core.ui.validator.ColorValidator.*

class ColorToStringConverter implements IConverter<Color, String> {
	
	val String colorFormat
	
	new (String colorFormat) {
		if (!colorFormat.isColorFormat) {
			throw new IllegalArgumentException('''Unknown color format "«colorFormat»". Must be "«HEX»", "«RGB»", or "«RGBA»"."''')
		}
		this.colorFormat = colorFormat
	}
	
	override getFromType() {
		Color
	}

	override getToType() {
		String
	}

	override convert(Color it) {
		if (it === null) {
			switch colorFormat {
				case RGBA: "0,0,0,0"
				case RGB:  "0,0,0"
				case HEX:  "#000000"
			}
		}
		else {
			switch colorFormat {
				case RGBA: String.format("%d,%d,%d,%d",   red, green, blue, alpha)
				case RGB:  String.format("%d,%d,%d",      red, green, blue)
				case HEX:  String.format("#%02X%02X%02X", red, green, blue)
			}
		}
	}
	
}

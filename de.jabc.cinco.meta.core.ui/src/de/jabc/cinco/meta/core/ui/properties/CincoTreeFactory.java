/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties;

import java.util.Map;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.masterdetail.IObservableFactory;
import org.eclipse.emf.databinding.IEMFListProperty;
import org.eclipse.emf.ecore.EObject;

import de.jabc.cinco.meta.core.ui.utils.CincoPropertyUtils;
import graphmodel.IdentifiableElement;

public class CincoTreeFactory implements IObservableFactory<Object, IObservable> {

	Map<Class<? extends EObject>, IEMFListProperty> emfListPropertiesMap;
	EObject bo;

	public CincoTreeFactory(EObject bo, Map<Class<? extends EObject>, IEMFListProperty> emfListPropertiesMap) {
		this.bo = bo;
		this.emfListPropertiesMap = emfListPropertiesMap;
	}

	@Override
	public IObservable createObservable(Object target) {
		
		if (target instanceof IObservableList) {
			WritableList<EObject> list = new WritableList<>();
			list.add(bo);
			return list;
		}

		if (target instanceof EObject) {
			if (target instanceof IdentifiableElement) {
				target = ((IdentifiableElement) target).getInternalElement_();
			}
			IEMFListProperty iEmfListProperty = CincoPropertyUtils.getAllListProperties(((EObject)target).getClass(), emfListPropertiesMap);
			return iEmfListProperty.listFactory().createObservable(target);
		}
			
		return null;
		
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.converter;

import org.eclipse.core.databinding.conversion.IConverter;

public class CharStringConverter implements IConverter {
	@Override
	public Object getToType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getFromType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object convert(Object fromObject) {
		if (fromObject instanceof Character) {
			if ((Character) fromObject == '\0')
				return new String("a");
			return String.valueOf(fromObject);
		}
		if (fromObject instanceof String) {
			if ("0".equals(fromObject))
				return new Character('\0');
			return ((String) fromObject).charAt(0);
		}
		
		return fromObject;
	}
}

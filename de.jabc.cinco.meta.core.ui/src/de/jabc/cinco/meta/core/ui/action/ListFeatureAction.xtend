/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.action

import de.jabc.cinco.meta.core.ui.properties.AttributeCreator
import de.jabc.cinco.meta.core.ui.properties.CincoPropertyView
import de.jabc.cinco.meta.core.ui.properties.dialogs.InputComboBoxDialog
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import java.util.List
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.transaction.RecordingCommand
import org.eclipse.emf.transaction.TransactionalEditingDomain
import org.eclipse.jface.action.Action
import org.eclipse.jface.dialogs.Dialog
import org.eclipse.jface.viewers.DoubleClickEvent
import org.eclipse.jface.viewers.IDoubleClickListener
import org.eclipse.jface.viewers.ISelectionChangedListener
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.viewers.SelectionChangedEvent
import org.eclipse.jface.viewers.TableViewer
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Display

abstract class ListFeatureAction extends Action implements SelectionListener, ISelectionChangedListener, IDoubleClickListener {
	
	val TableViewer tableViewer
	val EObject bo
	val EStructuralFeature feature
	
	var List<Button> buttons
	var TransactionalEditingDomain _domain
	
	new (String text, TableViewer tableViewer, EObject bo, EStructuralFeature feature) {
		super(text)
		if (feature.upperBound == 1) {
			throw new IllegalArgumentException('''The feature "«feature.name»" is not a list (upper bound is «feature.upperBound»)''')
		}
		this.tableViewer = tableViewer
		this.bo = bo
		this.feature = feature
		this.buttons = newArrayList()
		tableViewer.addSelectionChangedListener(this)
		update()
	}
	
	def boolean canExecuteAction()
	
	def void executeAction()
	
	def Button createButton(Composite parent, int style) {
		var button = new Button(parent, style)
		button.text = text
		button.addSelectionListener(this)
		buttons.add(button)
		update()
		return button
	}
	
	def protected List<Object> getList() {
		return bo.eGet(feature) as List<Object>
	}
	
	def protected int getUpperBound() {
		feature.upperBound
	}
	
	def protected Object getSelection() {
		(tableViewer.selection as IStructuredSelection).firstElement
	}
	
	def protected Object showDialog() {
		showDialog(null)
	}
	
	def protected Object showDialog(Object defaultValue) {
		switch feature {
			EAttribute: {
				val stringValue = if (defaultValue === null) {
					AttributeCreator.createAttribute(feature)
				}
				else {
					AttributeCreator.createAttribute(feature, defaultValue)
				}
				if (stringValue !== null) {
					return EcoreUtil.createFromString(feature.EAttributeType, stringValue)
				}
			}
			EReference: {
				val shell = Display.current.activeShell
				val possibleValuesLabelProvider = CincoPropertyView.getPossibleValuesLabelProvider(bo, feature)
				val possibleValues = CincoPropertyView.getPossibleValues(feature)
				val input = if (possibleValues === null) {
					CincoPropertyView.getInput(bo, feature.EReferenceType.instanceClass)
				}
				else {
					possibleValues.keySet.toList
				}
				val dialog = if (defaultValue === null) {
					new InputComboBoxDialog(shell, null, possibleValuesLabelProvider, input)
				}
				else {
					new InputComboBoxDialog(shell, null, possibleValuesLabelProvider, input, defaultValue)
				}
				if (dialog.open === Dialog.OK) {
					return dialog.value
				}
			}
		}
	}
	
	def private void update() {
		enabled = canExecuteAction
		for (button: buttons) {
			button.enabled = enabled
		}
		tableViewer.refresh()
	}
	
	def private TransactionalEditingDomain getDomain() {
		if (_domain === null) {
			_domain = new ResourceExtension().getEditingDomain(bo.eResource)
		}
		return _domain
	}
	
	// Action
	override run() {
		val command = new RecordingCommand(domain, '''«text» («feature.name»)''') {
			override protected doExecute() {
				executeAction()
			}
		}
		domain.commandStack.execute(command)
		update()
	}
	
	// SelectionListener
	override widgetSelected(SelectionEvent e) {
		update()
		if (enabled) {
			run()
		}
	}
	
	// SelectionListener
	override widgetDefaultSelected(SelectionEvent e) {
		// Do nothing
	}
	
	// ISelectionChangedListener
	override selectionChanged(SelectionChangedEvent e) {
		update()
	}
	
	// IDoubleClickListener
	override doubleClick(DoubleClickEvent e) {
		update()
		if (enabled) {
			run()
		}
	}
	
}

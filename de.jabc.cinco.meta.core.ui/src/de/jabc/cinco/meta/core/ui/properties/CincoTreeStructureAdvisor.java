/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.databinding.viewers.TreeStructureAdvisor;

import graphmodel.IdentifiableElement;

public class CincoTreeStructureAdvisor extends TreeStructureAdvisor {

	private Map<Class<? extends EObject>, List<EStructuralFeature>> map;

	public CincoTreeStructureAdvisor(Map<Class<? extends EObject>, List<EStructuralFeature>> referencesMap) {
		this.map = referencesMap;
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof EReference) {
			if (((EReference) element).isContainment())
				return ((EReference) element).eContainer();
		}
		return super.getParent(element);
	}

	@Override
	public Boolean hasChildren(Object element) {
		if (!(element instanceof EObject))
			return super.hasChildren(element);
		
		EObject obj = (EObject) element;
		if (obj instanceof IdentifiableElement)
			obj = ((IdentifiableElement) obj).getInternalElement_();
		
		//Check for null reference, if a type is deleted. The non-internal element exists
		//but it's internal element is null.
		if (obj == null) return false;
		
		List<EStructuralFeature> refsList = map.get(obj.getClass());
		if (refsList != null) { 
			for (EStructuralFeature f : refsList) {
				Object featureValue = obj.eGet(f, true);
				if (featureValue instanceof List)
					if (!((List<?>) featureValue).isEmpty())
						return true;
				if (featureValue != null)
					return true;
			}
		}

		return super.hasChildren(element);
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.property.list.IListProperty;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.databinding.EMFDataBindingContext;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.IEMFListProperty;
import org.eclipse.emf.databinding.edit.EMFEditObservables;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.databinding.edit.IEMFEditListProperty;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.platform.IDiagramBehavior;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.platform.GraphitiConnectionEditPart;
import org.eclipse.graphiti.ui.platform.GraphitiShapeEditPart;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.databinding.swt.ISWTObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableListTreeContentProvider;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorFactory;
import org.eclipse.xtext.xbase.lib.Functions.Function3;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure3;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import de.jabc.cinco.meta.core.ui.action.AddAttributeAction;
import de.jabc.cinco.meta.core.ui.action.AddReferenceAction;
import de.jabc.cinco.meta.core.ui.action.EditAttributeAction;
import de.jabc.cinco.meta.core.ui.action.EditReferenceAction;
import de.jabc.cinco.meta.core.ui.action.RemoveAttributeAction;
import de.jabc.cinco.meta.core.ui.action.RemoveReferenceAction;
import de.jabc.cinco.meta.core.ui.converter.ByteToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.CharacterToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.ColorToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.DoubleToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.FloatToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.IntegerToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.LongToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.ShortToStringConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToByteConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToCharacterConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToColorConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToDoubleConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToFloatConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToIntegerConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToLongConverter;
import de.jabc.cinco.meta.core.ui.converter.StringToShortConverter;
import de.jabc.cinco.meta.core.ui.listener.BrowseSelectionListener;
import de.jabc.cinco.meta.core.ui.listener.CincoTreeMenuListener;
import de.jabc.cinco.meta.core.ui.utils.CincoPropertyUtils;
import de.jabc.cinco.meta.core.ui.validator.BooleanValidator;
import de.jabc.cinco.meta.core.ui.validator.ByteValidator;
import de.jabc.cinco.meta.core.ui.validator.CharacterValidator;
import de.jabc.cinco.meta.core.ui.validator.ColorValidator;
import de.jabc.cinco.meta.core.ui.validator.DateValidator;
import de.jabc.cinco.meta.core.ui.validator.DoubleValidator;
import de.jabc.cinco.meta.core.ui.validator.FloatValidator;
import de.jabc.cinco.meta.core.ui.validator.GrammarValidator;
import de.jabc.cinco.meta.core.ui.validator.IntegerValidator;
import de.jabc.cinco.meta.core.ui.validator.LongValidator;
import de.jabc.cinco.meta.core.ui.validator.ObjectValidator;
import de.jabc.cinco.meta.core.ui.validator.PossibleValuesValidator;
import de.jabc.cinco.meta.core.ui.validator.ShortValidator;
import de.jabc.cinco.meta.core.ui.validator.StringValidator;
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension;
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension;
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension;
import graphmodel.Container;
import graphmodel.GraphModel;
import graphmodel.ModelElement;
import graphmodel.ModelElementContainer;
import graphmodel.Type;
import graphmodel.internal.InternalModelElement;
import graphmodel.internal.InternalType;

/**
 * @author kopetzki
 */
@SuppressWarnings("restriction")
public class CincoPropertyView extends ViewPart implements ISelectionListener {
	
	// Constants
	private static final int MIN_MULTILINE_HEIGHT = 100;
	
	// Extensions
	private static WorkbenchExtension workbench = new WorkbenchExtension();
	
	// Static attributes
	private static Map<Class<? extends EObject>, IEMFListProperty>         emfListPropertiesMap = new HashMap<Class<? extends EObject>, IEMFListProperty>();
	private static Map<Class<? extends EObject>, List<EStructuralFeature>> attributesMap        = new HashMap<Class<? extends EObject>, List<EStructuralFeature>>();
	private static Map<Class<? extends EObject>, List<EStructuralFeature>> referencesMap        = new HashMap<Class<? extends EObject>, List<EStructuralFeature>>();
	private static Map<Class<? extends EObject>, EStructuralFeature>       typeLabels           = new HashMap<Class<? extends EObject>, EStructuralFeature>();
	
	private static Map<EStructuralFeature, Map<? extends Object, String>> possibleValuesMap    = new HashMap<EStructuralFeature, Map<? extends Object, String>>();
	private static Map<EStructuralFeature, Injector>                      grammarAttributes    = new HashMap<EStructuralFeature, Injector>();
	private static Map<EStructuralFeature, List<String>>                  fileExtensionFilters = new HashMap<EStructuralFeature, List<String>>();
	private static Map<EStructuralFeature, String>                        colorParameters      = new HashMap<EStructuralFeature, String>();
	
	private static Set<EStructuralFeature> multiLineAttributes  = new HashSet<EStructuralFeature>();
	private static Set<EStructuralFeature> readOnlyAttributes   = new HashSet<EStructuralFeature>();
	private static Set<EStructuralFeature> fileAttributes       = new HashSet<EStructuralFeature>();
	private static Set<EStructuralFeature> colorAttributes      = new HashSet<EStructuralFeature>();
	
	private static Set<EClass> disableCreate = new HashSet<EClass>();
	private static Set<EClass> disableDelete = new HashSet<EClass>();
	
	public static Set<Consumer<EObject>>                     postSelects           = new HashSet<>();
	public static Set<Consumer<EObject>>                     possibleValueRefreshs = new HashSet<>();
	public static Function3<EObject, String, Object, String> canAttributeChange    = (bo, attrName, value) -> null;
	public static Procedure3<EObject, String, Object>        preAttributeChange    = (bo, attrName, value) -> {};
	
	// Singleton pattern
	private static CincoPropertyView instance;
	
	// Views
	private Composite parent;
	private ScrolledComposite simpleViewComposite;
	private Composite treeViewComposite;
	
	// Attributes
	private TransactionalEditingDomain domain;
	private EMFDataBindingContext context;
	private Object lastSelectedObject;
	private PictogramElement lastSelectedPictogramElement;
	
	// Constructor
	public CincoPropertyView() {
		context = new EMFDataBindingContext();
		if (instance != null) {
			removeSelectionListener(instance);
		}
		instance = this;
		assertSelectionListener();
	}

	@Override // ISelectionListener
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}
		Object element = ((IStructuredSelection) selection).getFirstElement();
		PictogramElement pe = null;
		if (element instanceof GraphitiShapeEditPart) {
			pe = ((GraphitiShapeEditPart) element).getPictogramElement();
		}
		else if (element instanceof GraphitiConnectionEditPart) {
			pe = ((GraphitiConnectionEditPart) element).getPictogramElement();
		}
		EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
		if (bo != null) {
			for (Consumer<EObject> possibleValueRefresh: possibleValueRefreshs) {
				possibleValueRefresh.accept(bo);
			}
			init_PropertyView(bo);
		}
		else {
			clearPage();
		}
		IDiagramBehavior diagramBehavior = null;
		DiagramEditor editor = workbench.getActiveDiagramEditor();
		if (editor != null) {
			diagramBehavior = editor.getDiagramBehavior();
		}
		if (diagramBehavior instanceof DiagramBehavior) {
			DiagramBehavior db = (DiagramBehavior) diagramBehavior;
			if (pe instanceof ConnectionDecorator && !pe.equals(lastSelectedPictogramElement)) {
				Connection connection = ((ConnectionDecorator) pe).getConnection();
				lastSelectedPictogramElement = pe;
				db.getDiagramContainer().selectPictogramElements(new PictogramElement[] {pe, connection});
			}
			lastSelectedPictogramElement = pe;
		}
		if (bo != null) {
			for (Consumer<EObject> postSelect: postSelects) {
				postSelect.accept(bo);
			}
		}
	}
	
	private void clearPage() {
		if (parent != null) {
			disposeChildren(parent);
		}
	}
	
	public static void assertSelectionListener() {
		if (instance == null) {
			instance = new CincoPropertyView();
		}
		addSelectionListener(instance);
	}
	
	public static void removeSelectionListener(ISelectionListener listener) {
		try {
			IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			if (activeWorkbenchWindow == null) return;
			IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
			if (activePage == null) return;
			activePage.removeSelectionListener(listener);
		}
		catch (Exception e) {
			System.out.println("Not In Graphical Model.");
			return;
		}
	}
	
	public static void addSelectionListener(ISelectionListener listener) {
		try {
			IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			if (activeWorkbenchWindow == null) return;
			IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
			if (activePage == null) return;
			activePage.addSelectionListener(listener);
		}
		catch (Exception e) {
			System.out.println("Not In Graphical Model.");
			return;
		}
	}
	
	public static void init_EStructuralFeatures(Class<? extends EObject> clazz,	EStructuralFeature... features) {
		List<EStructuralFeature> featureList = Arrays.asList(features);
		List<EStructuralFeature> attributeList = featureList.stream()
				.filter(f -> (f instanceof EAttribute) || (f instanceof EReference && !((EReference) f).isContainment()) )
				.collect(Collectors.toList());
		List<EStructuralFeature> referenceList = featureList.stream()
				.filter(f -> f instanceof EReference && ((EReference) f).isContainment())
				.collect(Collectors.toList());

		init_ListPorperties(clazz, referenceList.toArray(new EStructuralFeature[] {}));

		if (referencesMap.get(clazz) == null) {
			referencesMap.put(clazz, referenceList);
		}

		if (attributesMap.get(clazz) == null) {
			attributesMap.put(clazz, attributeList);
		}
	}

	public static void init_ListPorperties(Class<? extends EObject> clazz,	EStructuralFeature... features) {
		if (emfListPropertiesMap.get(clazz) == null) {
			emfListPropertiesMap.put(clazz, EMFProperties.multiList(features));
		}
	}

	public static void init_MultiLineAttributes(EStructuralFeature... features) {
		for (EStructuralFeature f: features) {
			multiLineAttributes.add(f);
		}
	}
	
	public static void init_ReadOnlyAttributes(EStructuralFeature... features) {
		for (EStructuralFeature f: features) {
			readOnlyAttributes.add(f);
		}
	}
	
	public static void init_FileAttributes(EStructuralFeature... features) {
		for (EStructuralFeature f: features) {
			fileAttributes.add(f);
		}
	}
	
	public static void init_FileAttributesExtensionFilters(EStructuralFeature feature, String... extensions) {
		fileExtensionFilters.put(feature, Arrays.asList(extensions));
	}

	public static void init_ColorAttributes(EStructuralFeature... features) {
		for (EStructuralFeature f: features) {
			colorAttributes.add(f);
		}
	}
	
	public static void init_ColorAttributesParameter(EStructuralFeature feature, String parameter){
		colorParameters.put(feature, parameter);
	}

	public static void init_GrammarEditor(EStructuralFeature feature, Injector injector) {
		grammarAttributes.put(feature, injector);
	}

	public static void init_DisableCreate(EClass... eClasses) {
		for (var eClass: eClasses) {
			disableCreate.add(eClass);
		}
	}

	public static void init_DisableDelete(EClass... eClasses) {
		for (var eClass: eClasses) {
			disableDelete.add(eClass);
		}
	}
	
	public static void init_TypeLabel(Class<? extends EObject> clazz, EStructuralFeature feature) {
		typeLabels.put(clazz, feature);
	}
	
	public void init_PropertyView(EObject bo) {
		if (parent == null) {
			// No parent means the PropertyView is not visible
			return;
		}
		if (bo instanceof graphmodel.IdentifiableElement) {
			bo = ((graphmodel.IdentifiableElement)bo).getInternalElement_();
		}
		if (bo == null || bo.equals(lastSelectedObject)) {
			return;
		}
		
		disposeChildren(parent);
		context = new EMFDataBindingContext();
		parent.setLayout(new GridLayout(2, false));
		
		treeViewComposite = new Composite(parent, SWT.BORDER);
		treeViewComposite.setLayout(new GridLayout(1, false));
		treeViewComposite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, true));
		
		simpleViewComposite = new ScrolledComposite(parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		simpleViewComposite.setLayout(new GridLayout(1, false));
		simpleViewComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		createTreePropertyView(bo);
		createSimplePropertyView(bo);
		
		treeViewComposite.pack();
		simpleViewComposite.pack();
		treeViewComposite.layout(true);
		simpleViewComposite.layout(true);
		
		parent.layout(true);
		
		lastSelectedObject = bo;
	}

	public static void refreshPossibleValues(EStructuralFeature feature, Map<? extends Object, String> values) {
		possibleValuesMap.put(feature, values);
	}
	
	public static Map<? extends Object, String> getPossibleValues(EStructuralFeature feature) {
		return possibleValuesMap.get(feature);
	}

	public void createTreePropertyView(EObject bo) {
		Tree tree = new Tree(treeViewComposite, SWT.SINGLE);
		GridData data = new GridData(SWT.LEFT, SWT.FILL, false, true);
		data.widthHint = 255;
		tree.setLayoutData(data);
		TreeViewer viewer = new TreeViewer(tree);
		
		@SuppressWarnings({ "unchecked", "rawtypes" })
		ObservableListTreeContentProvider cp = new ObservableListTreeContentProvider(
			// FIXME Fix the CincoTreeFactory. Removing single valued attributes from the tree ends in AssertionFailedException 
			new CincoTreeFactory(bo, emfListPropertiesMap),
			new CincoTreeStructureAdvisor(referencesMap)
		);

		viewer.setContentProvider(cp);
		viewer.setLabelProvider(new TreePropertyViewLabelProvider(typeLabels));

		List<EStructuralFeature> inputList = CincoPropertyUtils.getAllEStructuralFeatures(bo.getClass(), referencesMap);
		IEMFEditListProperty input = EMFEditProperties.multiList(domain, inputList.toArray(new EStructuralFeature[] {}));
		viewer.setInput(input.observe(bo));

		viewer.addSelectionChangedListener(event -> {
			ISelection selection = event.getSelection();
			if (selection instanceof IStructuredSelection) {
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				if (obj instanceof Type) {
					InternalType internalElement = ((Type) obj).getInternalElement_();
					for (Consumer<EObject> possibleValueRefresh: possibleValueRefreshs) {
						possibleValueRefresh.accept((EObject) obj);
					}
					createSimplePropertyView(internalElement);
				}
				else if (obj instanceof EObject) {
					createSimplePropertyView((EObject) obj);
				}
			}
		});

		MenuManager menuManager = new MenuManager();
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(new CincoTreeMenuListener(viewer, referencesMap, disableCreate, disableDelete));

		viewer.getControl().setMenu(menuManager.createContextMenu(tree));
		viewer.getTree().setSelection(viewer.getTree().getItem(0));
		viewer.expandAll();
	}
	
	private void createUIAndBindings(EObject bo, Composite comp) {
		Resource resource = bo.eResource();
		if (resource == null) {
			if (bo instanceof InternalType) {
				resource = ((InternalType) bo).getElement().eResource();
			}
			else if (bo instanceof Type) {
				resource = ((Type) bo).getInternalElement_().eResource();
			}
		}
		domain = new ResourceExtension().getEditingDomain(resource);
		
		List<EStructuralFeature> attributes = CincoPropertyUtils.getAllEStructuralFeatures(bo.getClass(), attributesMap);
		if (attributes.isEmpty()) {
			Label label = new Label(comp, SWT.NONE);
			label.setText("No properties available ...");
		}
		else {
			for (EStructuralFeature attr: attributes) {
				if (attr.getUpperBound() == 1) {
					createSingleAttributeProperty(bo, comp,  attr);
				}
				else if (attr.getUpperBound() == -1 || attr.getUpperBound() > 1) {
					createMultiAttributeProperty(bo, comp, attr);
				}
				else {
					System.err.println("[" + this.getClass().getSimpleName() + "] Upper bound of attribute '" + attr.getName() + "' is " + attr.getUpperBound());
				}
			}
		}
	}
	
	private void setLayoutData(Control control,
	                           boolean fillHorizontal,       boolean centerVertical,
	                           boolean grabExcessHorizontal, boolean grabExcessVertical,
	                           int spanHorizontal,           int verticalSpan) {
		GridData gridData = new GridData(
			fillHorizontal ? SWT.FILL   : SWT.LEFT,
			centerVertical ? SWT.CENTER : SWT.TOP,
			grabExcessHorizontal,
			grabExcessVertical,
			spanHorizontal,
			verticalSpan
		);
		control.setLayoutData(gridData);
	}
	
	private void setLayoutData(Label label) {
		setLayoutData(label, false);
	}
	
	private void setLayoutData(Label label, boolean multiline) {
		GridData gridData = new GridData(SWT.TOP, multiline ? SWT.TOP : SWT.CENTER, false, false, 1, 1);
		label.setLayoutData(gridData);
	}
	
	private void setLayoutData(Text text) {
		GridData gridData;
		if ((text.getStyle() & SWT.MULTI) == 0) {
			gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		}
		else {
			gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
			gridData.minimumHeight = MIN_MULTILINE_HEIGHT;
		}
		text.setLayoutData(gridData);
	}
	
	private void setLayoutData(Combo combo) {
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		combo.setLayoutData(gridData);
	}
	
	private void setLayoutData(Button button) {
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		button.setLayoutData(gridData);
	}
	
	private void setLayoutData(EmbeddedEditor editor) {
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1);
		gridData.minimumHeight = MIN_MULTILINE_HEIGHT;
		editor.getViewer().getControl().setLayoutData(gridData);
	}
	
	private void setLayoutData(Table table) {
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 3);
		gridData.minimumHeight = MIN_MULTILINE_HEIGHT;
		table.setLayoutData(gridData);
	}
	
	protected void createSimplePropertyView(EObject bo) {
		if (simpleViewComposite == null) {
			throw new RuntimeException("NPE: Composite for the simple property view is null");
		}
		disposeChildren(simpleViewComposite);
		Composite comp = new Composite(simpleViewComposite, SWT.NONE);
		comp.setLayout(new GridLayout(4, false));
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		createUIAndBindings(bo, comp);
		simpleViewComposite.setContent(comp);
		simpleViewComposite.setExpandHorizontal(true);
		simpleViewComposite.setExpandVertical(true);
		simpleViewComposite.setAlwaysShowScrollBars(true);
		simpleViewComposite.layout(true, true);
		simpleViewComposite.setMinSize(comp.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	private void createSingleAttributeProperty(EObject bo, Composite comp, EStructuralFeature feature) {
		if (feature instanceof EAttribute) {
			createSingleAttributeProperty(bo, comp, (EAttribute) feature);
		}
		else if (feature instanceof EReference) {
			createSingleAttributeProperty(bo, comp, (EReference) feature);
		}
	}
	
	private void createSingleAttributeProperty(EObject bo, Composite comp, EReference ref) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(ref.getName() + ": ");
		// Combo
		Combo combo = new Combo(comp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
		setLayoutData(combo);
		PossibleValues<ModelElement> modelElementPossibleValues = PossibleValues.fromReference(bo, ref, possibleValuesMap.get(ref));
		combo.setItems(modelElementPossibleValues.getLabels());
		// Validation
		combo.addSelectionListener(new PossibleValuesValidator<>(bo, ref, combo, modelElementPossibleValues));
		// Observer
		ISWTObservableValue<ModelElement> targetProperty = new ComboProperty<>(modelElementPossibleValues).observe(combo);
		@SuppressWarnings("unchecked")
		IObservableValue<ModelElement> modelProperty = EMFEditObservables.observeValue(domain, bo, ref);
		AttributeChangeEventStrategy<ModelElement, ModelElement> updateStrategy = new AttributeChangeEventStrategy<>(bo, ref);
		context.bindValue(targetProperty, modelProperty, updateStrategy, null);
		// Enable
		combo.setEnabled(!readOnlyAttributes.contains(ref));
	}
	
	public static List<Object> getInput(EObject bo, Class<?> searchFor) {
		List<Object> result = new ArrayList<Object>();
		if (bo instanceof ModelElement)
			bo = ((ModelElement)bo).getInternalElement_();
		if (bo instanceof InternalModelElement) {
			GraphModel gm = ((InternalModelElement) bo).getRootElement().getElement();
			getAllModelElements(gm, result, searchFor);
		}
		if (bo instanceof InternalType) {
			GraphModel gm = new GraphModelExtension().getRootElement(((InternalType) bo).getElement());
			getAllModelElements(gm, result, searchFor);
		}
		return result;
	}

	private static void getAllModelElements(ModelElementContainer container, List<Object> result, Class<?> searchFor) {
		@SuppressWarnings("unchecked")
		Class<? extends ModelElement> modelElementClass = (Class<? extends ModelElement>) searchFor;
		EList<? extends ModelElement> modelElements = container.getModelElements(modelElementClass);
		result.addAll(modelElements);
		for (Container c: container.getAllContainers()) {
			getAllModelElements(c, result, searchFor);
		}
	}

	private void createSingleAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		if (attr.getEAttributeType() instanceof EEnum) {
			createEnumAttributeProperty(bo, comp, attr);
		}
		else if (fileAttributes.contains(attr)) {
			createFileAttributeProperty(bo, comp, attr);
		}
		else if (colorAttributes.contains(attr)) {
			createColorAttributeProperty(bo, comp, attr);
		}
		else if (possibleValuesMap.containsKey(attr)) {
			createPossibleValuesAttributeProperty(bo, comp, attr);
		}
		else if (grammarAttributes.containsKey(attr)) {
			createGrammarAttributeProperty(bo, comp, attr);
		}
		else {
			switch (attr.getEAttributeType().getName()) {
				case "EDate":    createDateAttributeProperty(bo, comp, attr);    break;
				case "EBoolean": createBooleanAttributeProperty(bo, comp, attr); break;
				case "EByte":    createByteAttributeProperty(bo, comp, attr);    break;
				case "EShort":   createShortAttributeProperty(bo, comp, attr);   break;
				case "EInt":     createIntAttributeProperty(bo, comp, attr);     break;
				case "ELong":    createLongAttributeProperty(bo, comp, attr);    break;
				case "EFloat":   createFloatAttributeProperty(bo, comp, attr);   break;
				case "EDouble":  createDoubleAttributeProperty(bo, comp, attr);  break;
				case "EChar":    createCharAttributeProperty(bo, comp, attr);    break;
				case "EString":  createStringAttributeProperty(bo, comp, attr);  break;
				default:         createDefaultAttributeProperty(bo, comp, attr); break;
			}
		}
	}
	
	private void createEnumAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Combo
		Combo combo = new Combo(comp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
		setLayoutData(combo);
		EEnum eEnum = (EEnum) attr.getEAttributeType();
		PossibleValues<Enumerator> enumPossibleValues = PossibleValues.fromEnum(eEnum);
		combo.setItems(enumPossibleValues.getLabels());
		// Validation
		combo.addSelectionListener(new PossibleValuesValidator<>(bo, attr, combo, enumPossibleValues));
		// Observer
		ISWTObservableValue<Enumerator> targetProperty = new ComboProperty<>(enumPossibleValues).observe(combo);
		@SuppressWarnings("unchecked")
		IObservableValue<Enumerator> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<Enumerator, Enumerator> updateStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty, modelProperty, updateStrategy, null);
		// Enable
		combo.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createFileAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text, true, true, true, false, 2, 1);
		text.setEnabled(false);
		// Button
		Button button = new Button(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(button);
		button.setText("Browse ...");
		button.addSelectionListener(new BrowseSelectionListener(comp.getShell(), text, fileExtensionFilters.get(attr)));
		// Validation
		text.addModifyListener(new StringValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut, SWT.Modify).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<String> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, String> updateStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty,  modelProperty, updateStrategy, null);
		// Enable
		button.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createColorAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text, true, true, true, false, 2, 1);
		text.setEnabled(false);
		// Button
		Button button = new Button(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(button);
		try {
			Bundle bundle = Platform.getBundle("de.jabc.cinco.meta.core.ui");
			URL url = FileLocator.find(bundle, new Path("icons/color_picker.png"), null);
			if (url == null) {
				throw new NullPointerException("Cannot find color picker icon.");
			}
			Image image = ImageDescriptor.createFromURL(url).createImage();
			if (image == null) {
				throw new NullPointerException("Cannot create color picker icon.");
			}
			button.setImage(image);
			button.setText("Pick ...");
		}
		catch (Exception e) {
			e.printStackTrace();
			button.setText("Pick Color ...");
		}
		// Button: Selection listener
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				Color color = new StringToColorConverter().convert(text.getText());
				ColorDialog dialog = new ColorDialog(comp.getShell());
				if (color != null) {
					dialog.setRGB(color.getRGB());
				}
				dialog.setText("Pick a color");
				RGB rgb = dialog.open();
				if (rgb == null) {
					return;
				}
				if (color != null) {
					color.dispose();
				}
				color = new Color(comp.getShell().getDisplay(), rgb);
				String stringValue = new ColorToStringConverter(colorParameters.get(attr)).convert(color);
				text.setText(stringValue);
			}
		});
		// Text: Modify listener
		text.addModifyListener(event -> {
			Color oldBackground = button.getBackground();
			Color newBackground = new StringToColorConverter().convert(text.getText());
			button.setBackground(newBackground);
			if (oldBackground != null) {
				oldBackground.dispose();
			}
			Color oldForeground = button.getForeground();
			Color newForeground = getContrastingColor(newBackground);
			button.setForeground(newForeground);
			if (oldForeground != null) {
				oldForeground.dispose();
			}
		});
		// Validation
		text.addModifyListener(new ColorValidator(bo, attr, text, colorParameters.get(attr)));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut, SWT.Modify).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<String> modelProperty = EMFEditProperties.value(domain,attr).observe(bo);
		AttributeChangeEventStrategy<String, String> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty,  modelProperty, targetToModelStrategy, null);
		// Enable
		button.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private Color getContrastingColor(Color color) {
		if (color == null) {
			return null;
		}
		double r = getRelativeLuminanceValue(color.getRed());
		double g = getRelativeLuminanceValue(color.getGreen());
		double b = getRelativeLuminanceValue(color.getBlue());
		double relativeLuminance = 0.2126 * r + 0.7152 * g + 0.0722 * b;
		if (relativeLuminance < 0.5) {
			return new Color(Display.getCurrent(), 255, 255, 255); // White
		}
		else {
			return new Color(Display.getCurrent(), 0, 0, 0); // Black
		}
	}
	
	private double getRelativeLuminanceValue(int value) {
		if (value <= 10) {
			return value / 3294.6;
		}
		else {
			return Math.pow((value + 14.025) / 269.025, 2.4);
		}
	}
	
	private void createPossibleValuesAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Combo
		Combo combo = new Combo(comp, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
		setLayoutData(combo);
		PossibleValues<Object> possibleValues = PossibleValues.fromAttribute(bo, attr, possibleValuesMap.get(attr));
		combo.setItems(possibleValues.getLabels());
		// Validation
		combo.addSelectionListener(new PossibleValuesValidator<>(bo, attr, combo, possibleValues));
		// Observer
		ISWTObservableValue<Object> targetProperty = new ComboProperty<>(possibleValues).observe(combo);
		@SuppressWarnings("unchecked")
		IObservableValue<Object> modelProperty = EMFEditObservables.observeValue(domain,bo,attr);
		AttributeChangeEventStrategy<Object, Object> updateStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty, modelProperty, updateStrategy, null);
		// Enable
		combo.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createGrammarAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label, false, false, false, false, 1, 1);
		label.setText(attr.getName() + ": ");
		// Editor
		Injector injector = grammarAttributes.get(attr);
		CincoResourceProvider provider = injector.getInstance(CincoResourceProvider.class);
		EmbeddedEditorFactory factory = injector.getInstance(EmbeddedEditorFactory.class);
		EmbeddedEditor editor = factory
			.newEditor(provider)
			.showErrorAndWarningAnnotations()
			.withParent(comp);
		editor.createPartialEditor();
		setLayoutData(editor);
		// Text
		StyledText text = editor.getViewer().getTextWidget();
		text.setFont(JFaceResources.getTextFont());
		// Validation
		text.addModifyListener(new GrammarValidator(bo, attr, editor));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<String> modelPorperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, String> updateStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty, modelPorperty, updateStrategy, null);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createDateAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// DateTime
		DateTime date = new DateTime(comp, SWT.DATE);
		setLayoutData(date, false, true, false, false, 1, 1);
		DateTime time = new DateTime(comp, SWT.TIME);
		setLayoutData(time, false, true, true, false, 2, 1);
		// Validation
		DateValidator validator = new DateValidator(bo, attr, date, time);
		date.addSelectionListener(validator);
		time.addSelectionListener(validator);
		// Observer
		DateTimeProperty targetProperty = new DateTimeProperty(date, time);
		ISWTObservableValue<Date> targetPropertyDate = targetProperty.observe(date);
		ISWTObservableValue<Date> targetPropertyTime = targetProperty.observe(time);
		@SuppressWarnings("unchecked")
		IObservableValue<Date> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<Date, Date> updateStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetPropertyDate, modelProperty, updateStrategy, null);
		context.bindValue(targetPropertyTime, modelProperty, updateStrategy, null);
		// Enable
		date.setEnabled(!readOnlyAttributes.contains(attr));
		time.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createBooleanAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// CheckBox
		Button checkbox = new Button(comp, SWT.CHECK);
		setLayoutData(checkbox, false, true, false, false, 3, 1);
		// Validation
		checkbox.addSelectionListener(new BooleanValidator(bo, attr, checkbox));
		// Observer
		ISWTObservableValue<Boolean> targetProperty = WidgetProperties.buttonSelection().observe(checkbox);
		@SuppressWarnings("unchecked")
		IObservableValue<Boolean> modelProperty = EMFEditProperties.value(domain,attr).observe(bo);
		AttributeChangeEventStrategy<Boolean, Boolean> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, null);
		// Enable
		checkbox.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createByteAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new ByteValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Byte> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Byte> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToByteConverter());
		UpdateValueStrategy<Byte, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new ByteToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createShortAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new ShortValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Short> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Short> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToShortConverter());
		UpdateValueStrategy<Short, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new ShortToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createIntAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new IntegerValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Integer> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Integer> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToIntegerConverter());
		UpdateValueStrategy<Integer, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new IntegerToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createLongAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new LongValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Long> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Long> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToLongConverter());
		UpdateValueStrategy<Long, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new LongToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createFloatAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new FloatValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Float> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Float> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToFloatConverter());
		UpdateValueStrategy<Float, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new FloatToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createDoubleAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new DoubleValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Double> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Double> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToDoubleConverter());
		UpdateValueStrategy<Double, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new DoubleToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createCharAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validation
		text.addModifyListener(new CharacterValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Character> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Character> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		targetToModelStrategy.setConverter(new StringToCharacterConverter());
		UpdateValueStrategy<Character, String> modelToTargetStrategy = new UpdateValueStrategy<>();
		modelToTargetStrategy.setConverter(new CharacterToStringConverter());
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, modelToTargetStrategy);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}

	private void createStringAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = null;
		if (multiLineAttributes.contains(attr)) {
			setLayoutData(label, true);
			text = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		}
		else {
			setLayoutData(label, false);
			text = new Text(comp, SWT.BORDER);
		}
		setLayoutData(text);
		// Validation
		text.addModifyListener(new StringValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<String> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, String> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, null);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createDefaultAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label);
		label.setText(attr.getName() + ": ");
		// Text
		Text text = new Text(comp, SWT.BORDER);
		setLayoutData(text);
		// Validator
		text.addModifyListener(new ObjectValidator(bo, attr, text));
		// Observer
		ISWTObservableValue<String> targetProperty = WidgetProperties.text(SWT.DefaultSelection, SWT.FocusOut).observe(text);
		@SuppressWarnings("unchecked")
		IObservableValue<Object> modelProperty = EMFEditProperties.value(domain, attr).observe(bo);
		AttributeChangeEventStrategy<String, Object> targetToModelStrategy = new AttributeChangeEventStrategy<>(bo, attr);
		context.bindValue(targetProperty, modelProperty, targetToModelStrategy, null);
		// Enable
		text.setEnabled(!readOnlyAttributes.contains(attr));
	}
	
	private void createMultiAttributeProperty(EObject bo, Composite comp, EStructuralFeature attr) {
		if (attr instanceof EAttribute) {
			createMultiAttributeProperty(bo, comp, (EAttribute) attr);
		}
		else if (attr instanceof EReference) {
			createMultiAttributeProperty(bo, comp, (EReference) attr);
		}
	}

	private void createMultiAttributeProperty(EObject bo, Composite comp, EAttribute attr) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label, false, false, false, false, 1, 3);
		label.setText(attr.getName() + ": ");
		// Table
		Table table = new Table(comp, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
		setLayoutData(table);
		// TableViewer
		TableViewer tableViewer = new TableViewer(table);
		TextCellEditor editor = new TextCellEditor(table);
		tableViewer.setCellEditors(new CellEditor[] {editor});
		tableViewer.getTable().setEnabled(true);
		tableViewer.setContentProvider(new ObservableListContentProvider<>());
		tableViewer.setLabelProvider(getPossibleValuesLabelProvider(bo, attr));
		@SuppressWarnings("unchecked")
		IListProperty<EObject, ?> input = EMFEditProperties.list(domain, attr);
		tableViewer.setInput(input.observe(bo));
		// Add button
		AddAttributeAction addAction = new AddAttributeAction(tableViewer, bo, attr);
		Button addButton = addAction.createButton(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(addButton);
		// Edit button
		EditAttributeAction editAction = new EditAttributeAction(tableViewer, bo, attr);
		Button editButton = editAction.createButton(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(editButton);
		tableViewer.addDoubleClickListener(editAction);
		// Remove button
		RemoveAttributeAction removeAction = new RemoveAttributeAction(tableViewer, bo, attr);
		Button removeButton = removeAction.createButton(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(removeButton);
		// Menu
		MenuManager menuManager = new MenuManager();
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(manager -> {
			manager.add(addAction);
			manager.add(editAction);
			manager.add(new Separator());
			manager.add(removeAction);
		});
		Menu menu = menuManager.createContextMenu(table);
		table.setMenu(menu);
	}

	private void createMultiAttributeProperty(EObject bo, Composite comp, final EReference ref) {
		// Label
		Label label = new Label(comp, SWT.NONE);
		setLayoutData(label, false, false, false, false, 1, 3);
		label.setText(ref.getName() + ": ");
		// Table
		Table table = new Table(comp, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
		setLayoutData(table);
		// TableViewer
		TableViewer tableViewer = new TableViewer(table);
		TextCellEditor editor = new TextCellEditor(table);
		tableViewer.setCellEditors(new CellEditor[] {editor});
		tableViewer.getTable().setEnabled(true);
		tableViewer.setContentProvider(new ObservableListContentProvider<>());
		tableViewer.setLabelProvider(getPossibleValuesLabelProvider(bo, ref));
		@SuppressWarnings("unchecked")
		IListProperty<EObject, ?> input = EMFEditProperties.list(domain, ref);
		tableViewer.setInput(input.observe(bo));
		// Add button
		AddReferenceAction addAction = new AddReferenceAction(tableViewer, bo, ref);
		Button addButton = addAction.createButton(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(addButton);
		// Edit button
		EditReferenceAction editAction = new EditReferenceAction(tableViewer, bo, ref);
		Button editButton = editAction.createButton(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(editButton);
		tableViewer.addDoubleClickListener(editAction);
		// Remove button
		RemoveReferenceAction removeAction = new RemoveReferenceAction(tableViewer, bo, ref);
		Button removeButton = removeAction.createButton(comp, SWT.PUSH | SWT.BORDER);
		setLayoutData(removeButton);
		// Menu
		MenuManager menuManager = new MenuManager();
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(manager -> {
			manager.add(addAction);
			manager.add(editAction);
			manager.add(new Separator());
			manager.add(removeAction);
		});
		Menu menu = menuManager.createContextMenu(table);
		table.setMenu(menu);
	}
	
	private void disposeChildren(Composite composite) {
		for (Control c: composite.getChildren()) {
			c.dispose();
		}
	}

	public static PossibleValuesLabelProvider getPossibleValuesLabelProvider(EObject bo, EStructuralFeature feature) {
		if (feature instanceof EAttribute) {
			EAttribute attribute = (EAttribute) feature;
			PossibleValues<Object> possibleValues = PossibleValues.fromAttribute(bo, attribute, possibleValuesMap.get(attribute));
			return new PossibleValuesLabelProvider(possibleValues);
		}
		else if (feature instanceof EReference) {
			EReference reference = (EReference) feature;
			PossibleValues<ModelElement> possibleValues = PossibleValues.fromReference(bo, reference, possibleValuesMap.get(reference));
			return new PossibleValuesLabelProvider(possibleValues);
		}
		else {
			throw new IllegalArgumentException("feature is neither EAttribute nor EReference.");
		}
	}
	
	@Override // ViewPart
	public void createPartControl(Composite parent) {
		this.parent = parent;
	}

	@Override // ViewPart
	public void setFocus() {
		// Do nothing
	}

	@Override // ViewPart
	public void init(IViewSite site) throws PartInitException {
		PlatformUI
			.getWorkbench()
			.getActiveWorkbenchWindow()
			.getActivePage()
			.addSelectionListener(this);
		super.init(site);
	}
	
	@Override // ViewPart
	public void dispose() {
		super.dispose();
		PlatformUI
			.getWorkbench()
			.getActiveWorkbenchWindow()
			.getActivePage()
			.removeSelectionListener(this);
	}
	
}

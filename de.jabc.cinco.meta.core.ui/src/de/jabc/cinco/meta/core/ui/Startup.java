/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui;

import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		final MGLSelectionListener selectionListener = MGLSelectionListener.INSTANCE;
		IWorkbench workbench = PlatformUI.getWorkbench();
		workbench.addWindowListener(new IWindowListener() {
			@Override
			public void windowOpened(IWorkbenchWindow window) {
				window.getSelectionService().addSelectionListener(selectionListener);
			}
			@Override
			public void windowDeactivated(IWorkbenchWindow window) { }
			@Override
			public void windowClosed(IWorkbenchWindow window) { }
			@Override
			public void windowActivated(IWorkbenchWindow window) { }
		});
		for (IWorkbenchWindow window : workbench.getWorkbenchWindows()) {
			window.getSelectionService().addSelectionListener(selectionListener);
		}
	}
}

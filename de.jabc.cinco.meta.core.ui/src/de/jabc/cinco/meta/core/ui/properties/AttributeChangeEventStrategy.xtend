/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties

import graphmodel.IdentifiableElement
import graphmodel.internal.InternalIdentifiableElement
import org.eclipse.core.databinding.UpdateValueStrategy
import org.eclipse.core.databinding.observable.value.IObservableValue
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

import static de.jabc.cinco.meta.core.ui.properties.CincoPropertyView.canAttributeChange
import static de.jabc.cinco.meta.core.ui.properties.CincoPropertyView.preAttributeChange
import static org.eclipse.core.databinding.validation.ValidationStatus.error

class AttributeChangeEventStrategy<S, D> extends UpdateValueStrategy<S, D> {

	IdentifiableElement element
	String attribute
	
	new (EObject bo, EStructuralFeature attribute) {
		this.element = switch bo {
			InternalIdentifiableElement: bo.element
			IdentifiableElement:         bo
		}
		this.attribute = attribute.name
	}
	
	// UpdateValueStrategy
	override validateBeforeSet(D newValue) {
		val message = canAttributeChange.apply(element, attribute, newValue)
		if (message === null) {
			super.validateBeforeSet(newValue)
		}
		else {
			error(message)
		}
	}
	
	// UpdateValueStrategy
	override doSet(IObservableValue<? super D> observableValue, D value) {
		preAttributeChange.apply(element, attribute, value)
		super.doSet(observableValue, value)
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties

import org.eclipse.core.databinding.observable.list.IObservableList
import org.eclipse.core.databinding.observable.map.IObservableMap
import org.eclipse.core.databinding.observable.set.IObservableSet
import org.eclipse.core.databinding.observable.value.IObservableValue
import org.eclipse.jface.databinding.swt.WidgetValueProperty
import org.eclipse.swt.SWT
import org.eclipse.swt.widgets.Combo

class ComboProperty<T> extends WidgetValueProperty<Combo, T> {

	val PossibleValues<T> possibleValues

	new (PossibleValues<T> possibleValues) {
		super(SWT.Modify)
		this.possibleValues = possibleValues
	}
	
	override getValueType() {
		Object
	}
	
	override doGetValue(Combo source) {
		return possibleValues.getValue(source.text)
	}
	
	override doSetValue(Combo source, T value) {
		val label = possibleValues.getLabel(value)
		val items = source.items?: newArrayOfSize(0)
		val index = items.indexOf(label)
		if (index >= 0 && index < items.length) {
			source.select(index)
		}
		else {
			System.err.println('''
				[«this.class.name»] Cannot set value "«value»" in combo box. Possible values:
				«possibleValues»
			''')
			source.text = "[ERROR]"
		}
	}
	
	override <M extends Combo> observeDetail(IObservableValue<M> master) {
		super.observeDetail(master)
	}
	
	override <M extends Combo> observeDetail(IObservableList<M> master) {
		super.observeDetail(master)
	}
	
	override <M extends Combo> observeDetail(IObservableSet<M> master) {
		super.observeDetail(master)
	}
	
	override <K,V extends Combo> observeDetail(IObservableMap<K, V> master) {
		super.observeDetail(master)
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.validator

import de.jabc.cinco.meta.core.ui.properties.PossibleValues
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.swt.widgets.Combo

class PossibleValuesValidator<T> extends ControlValidator<Combo, T> {
	
	val PossibleValues<T> possibleValues
	
	new (EObject bo, EStructuralFeature attribute, Combo control, PossibleValues<T> possibleValues) {
		super (bo, attribute, control)
		this.possibleValues = possibleValues
	}
	
	override hasValidInput(Combo control) {
		possibleValues.existsLabel(control.text)
	}
	
	override getValue(Combo control) {
		possibleValues.getValue(control.text)
	}
	
}

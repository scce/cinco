/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties

import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.GraphModel
import graphmodel.IdentifiableElement
import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import graphmodel.Type
import graphmodel.internal.InternalGraphModel
import graphmodel.internal.InternalModelElement
import graphmodel.internal.InternalType
import java.util.List
import java.util.Map
import org.eclipse.emf.common.util.Enumerator
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference

/**
 * Represents a set of values of type {@code T}, that each have a unique label.
 */
class PossibleValues<T> {
	
	/**
	 * The special label for values that are {@code null}.  
	 */
	val public static NULL_LABEL = ""
	
	/**
	 * The name of the String attribute, whose value will be used for labels.
	 * This will only be used, if the element is of type {@link ModelElement}
	 * ({@code T == ModelElement}) and the element has a String attribute with
	 * his name.
	 */
	val public static NAME_ATTRIBUTE = "name"
	
	/**
	 * Maps each value of type {@code T} to its unique label. 
	 */
	val Map<T, String> map
	
	/**
	 * Creates a new {@link PossibleValues} instance from a map.
	 * @see #fromMap(Map, Class) fromMap(Map, Class)
	 * @see #fromEnum(EEnum) fromEnum(EEnum)
	 * @see #fromReference(EObject, EReference) fromReference(EObject, EReference)
	 */
	new (Map<T, String> possibleValuesMap) {
		map = possibleValuesMap.fixLabels
	}
	
	/**
	 * Returns the value for the label {@code searchLabel}.
	 */
	def getValue(String searchLabel) {
		map.filter[ value, valueLabel | valueLabel == searchLabel ].keySet.head
	}
	
	/**
	 * Returns the label for the value {@code searchValue}.
	 */
	def getLabel(T searchValue) {
		map.get(searchValue)
	}
	
	/**
	 * Returns an array of all values.
	 */
	def T[] getValues() {
		map.keySet
	}
	
	/**
	 * Returns an array of all labels.
	 */
	def String[] getLabels() {
		map.values
	}
	
	/**
	 * Check, if the value {@code searchValue} exists.
	 */
	def boolean existsValue(T searchValue) {
		map.containsKey(searchValue)
	}
	
	/**
	 * Check, if the label {@code searchLabel} exists.
	 */
	def boolean existsLabel(String searchLabel) {
		map.containsValue(searchLabel)
	}
	
	/**
	 * Adds an entry, that allows the {@link org.eclipse.swt.widgets.Combo
	 * Combo} box to set the value {@code null}. {@link PossibleValues#NULL_LABEL
	 * NULL_LABEL} will be used for its label.
	 */
	def void addNull() {
		map.put(null, NULL_LABEL)
	}
	
	/**
	 * Creates a new {@link PossibleValues} instance from an attribute field of
	 * {@code bo}. All keys from {@code map}, that do not fit the attribute's
	 * type will be ignored. This is a convenience method for the constructor
	 * {@link PossibleValues#PossibleValues(Map) PossibleValues(Map)}.
	 */
	def static <F> PossibleValues<Object> fromAttribute(EObject bo, EAttribute attribute, Map<F, String> map) {
		val attributeClass = attribute.EAttributeType.instanceClass
		val possibleValuesMap = map
			?.entrySet
			?.filter[key === null || key.class.canCastTo(attributeClass)]
			?.toMap([key.castTo(attributeClass)], [value])
		return new PossibleValues<Object>(possibleValuesMap)
	}
	
	def private static Class<?> wrapped(Class<?> primitiveType) {
		switch primitiveType {
			case byte:    Byte
			case short:   Short
			case int:     Integer
			case long:    Long
			case float:   Float
			case double:  Double
			case char:    Character
			case boolean: Boolean
			case void:    Void
			case null:    throw new NullPointerException
			default:      primitiveType
		}
	}
	
	def private static <F, T> boolean canCastTo(Class<F> fromClass, Class<T> toClass) {
		toClass.wrapped.isAssignableFrom(fromClass.wrapped)
	}
	
	def private static <F, T> T castTo(F object, Class<T> type) {
		if (type === null) {
			throw new NullPointerException
		}
		if (object === null) {
			return switch type {
				case byte:    0     as byte
				case short:   0     as short
				case int:     0     as int
				case long:    0     as long
				case float:   0.0   as float
				case double:  0.0   as double
				case char:    0     as char
				case boolean: false as boolean
				default:      null
			} as T
		}
		if (object.class.canCastTo(type)) {
			return object as T
		}
		throw new ClassCastException
	}
	
	/**
	 * Creates a new {@link PossibleValues} instance for an {@link EEnum}. It
	 * will contain all of {@code eEnum}'s instances. The labels will be their
	 * corresponding literals.
	 */
	def static PossibleValues<Enumerator> fromEnum(EEnum eEnum) {
		val possibleValuesMap = eEnum.ELiterals.toMap([instance], [literal])
		return new PossibleValues<Enumerator>(possibleValuesMap)
	}
	
	/**
	 * Creates a new {@link PossibleValues} instance for a reference field of
	 * {@code bo}. It will contain all model elements with a fitting type, that
	 * are in the same {@link GraphModel} as {@code bo}. The labels will be
	 * either the value of the element's {@code name} attribute (if available),
	 * or its class name and {@link IdentifiableElement#getId() id}.
	 */
	def static <F> PossibleValues<ModelElement> fromReference(EObject bo, EReference reference, Map<F, String> map) {
		var Map<ModelElement, String> possibleValuesMap = null
		if (map === null) {
			val referenceClass = reference.EReferenceType.instanceClass as Class<? extends ModelElement>
			val values = bo.getAllModelElements(referenceClass)
			possibleValuesMap = values.sortBy[name].toMap([it], [name])
		}
		else {
			possibleValuesMap = map
				.entrySet
				.filter[key instanceof ModelElement]
				.toMap([key as ModelElement], [value])
		}
		val possibleValues = new PossibleValues<ModelElement>(possibleValuesMap)
		possibleValues.addNull
		return possibleValues
	}
	
	/**
	 * Retrieves all model elements of type {@code E}, that are in the same
	 * {@link GraphModel} as {@code bo}.
	 */
	def static <E extends ModelElement> List<E> getAllModelElements(EObject bo, Class<E> searchClass) {
		val graphModel = switch bo {
			GraphModel:           bo
			InternalGraphModel:   bo.element
			ModelElement:         bo.rootElement
			InternalModelElement: bo.rootElement.element
			Type:                 new GraphModelExtension().getRootElement(bo)
			InternalType:         new GraphModelExtension().getRootElement(bo.element)
			default:              throw new IllegalArgumentException('''Parameter "bo" has unknown type "«bo.class.name»".''')
		}
		return graphModel.getAllModelElementsRecursive(searchClass).toList
	}
	
	/**
	 * Retrieves all descendants (containment-wise) of {@code container} that
	 * are of type {@code E}.
	 */
	def private static <E extends ModelElement> Iterable<E> getAllModelElementsRecursive(ModelElementContainer container, Class<E> searchClass) {
		val modelElements = container.getModelElements(searchClass)
		val childModelElements = container.allContainers.flatMap[ con |
			con.getAllModelElementsRecursive(searchClass)
		]
		return modelElements + childModelElements
	}
	
	/**
	 * Returns a name for this {@code object}. If {@code object} is ...
	 * <ul>
	 * <li>a {@link ModelElement} and has a String attribute {@code name} and it is not {@code null} or empty, the {@code name} will be returned.</li>
	 * <li>a {@link ModelElement} and has no such attribute or it is {@code null} or empty, {@code object}'s class name and id will be returned.</li>
	 * <li>a {@link IdentifiableElement}, {@code object}'s class name and {@link IdentifiableElement#getId() id} will be returned.</li>
	 * <li>any other {@link Object}, its {@link Object#toString() toString()} will be returned.</li>
	 * <li>{@code null}, the {@link PossibleValues#NULL_LABEL NULL_LABEL} will be returned.</li>
	 * </ul>
	 */
	def private static String getName(Object object) {
		switch it: object {
			case null: {
				return NULL_LABEL
			}
			ModelElement: {
				val nameFeature = internalElement_.eClass.getEStructuralFeature(NAME_ATTRIBUTE)
				if (nameFeature !== null) {
					val nameFeatureValue = internalElement_.eGet(nameFeature)
					if (nameFeatureValue instanceof String) {
						if (!nameFeatureValue.nullOrEmpty) {
							return nameFeatureValue
						}
					}
				}
				return '''«eClass.name» («id»)'''
			}
			IdentifiableElement: {
				return '''«eClass.name» («id»)'''
			}
			default: {
				return toString
			}
		}
	}
	
	/**
	 * Checks if labels are not null or empty. If multiple values map to the
	 * same label, their labels are suffixed with their indices.
	 */
	def private Map<T, String> fixLabels(Map<T, String> possibleValues) {
		if (possibleValues === null || possibleValues.empty) {
			return newHashMap
		}
		val Map<T, String> valueToLabel = newLinkedHashMap
		val Map<String, Boolean> labelToDuplicate = newLinkedHashMap
		val Map<String, Integer> labelToCount = newLinkedHashMap
		// Replace invalid labels and check for duplicate labels
		possibleValues.forEach [ value, label |
			val checkedLabel = switch label {
				case label.nullOrEmpty: value.name
				case NULL_LABEL:        '''"«label»"'''
				default:                label
			}
			valueToLabel.put(value, checkedLabel)
			if (labelToDuplicate.containsKey(checkedLabel)) {
				labelToDuplicate.put(checkedLabel, true)
			}
			else {
				labelToDuplicate.put(checkedLabel, false)
			}
		]
		// Suffix duplicate labels with an index
		valueToLabel.forEach [ value, label |
			val duplicate = labelToDuplicate.get(label)
			if (duplicate) {
				val count = labelToCount.getOrDefault(label, 0) + 1
				labelToCount.put(label, count)
				valueToLabel.put(value, '''«label» («count»)''')
			}
		]
		return valueToLabel
	}
	
	override toString() {
		map.entrySet.join('{\n\t', ',\n\t', '\n}') [
			'''«key» <-> "«value»"'''
		]
	}
	
}

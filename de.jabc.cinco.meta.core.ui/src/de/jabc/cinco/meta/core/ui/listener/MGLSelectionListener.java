/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.listener;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import productDefinition.CincoProduct;

public class MGLSelectionListener implements ISelectionListener{

	ISelection selection = null;
	IWorkbenchPart part = null;
	IFile selectedFile = null;
	private IFile selectedMGLFile = null;
	private CincoProduct selectedCPD;
	
	public static MGLSelectionListener INSTANCE = new MGLSelectionListener(); 
		
	private MGLSelectionListener(){}
	
	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if(selection instanceof IStructuredSelection && ((IStructuredSelection)selection).getFirstElement() instanceof IFile){
			if(((IFile)((IStructuredSelection)selection).getFirstElement()).getFileExtension().equals("cpd")){
				this.part = part;
				this.selection = (IStructuredSelection)selection;
				selectCPDFile((IFile)((IStructuredSelection)selection).getFirstElement());
			}
		}
		
		if(selection==null){
			this.part = null;
			this.selection = null;
			this.selectedFile = null;
		}
		
	}
	
	public IFile getCurrentMGLFile(){
		return this.selectedMGLFile;
	}
	
	public void putMGLFile(IFile iFile){
		if(iFile.getFileExtension().equals("mgl"))
			this.selectedMGLFile = iFile;
	}
	
	public IFile getSelectedCPDFile(){
		return this.selectedFile;
	}
	
	public void selectCPDFile(IFile cpd) {
		this.selectedFile = cpd;
	}
	
	public CincoProduct getSelectedCPD(){
		return this.selectedCPD;
	}

	public void setCurrentCPD(CincoProduct cpd) {
		this.selectedCPD = cpd;
		
	}
	
}

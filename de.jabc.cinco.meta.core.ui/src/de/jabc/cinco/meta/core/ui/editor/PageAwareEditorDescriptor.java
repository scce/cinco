/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.editor;

import java.util.function.Function;

import org.eclipse.ui.IEditorInput;

public class PageAwareEditorDescriptor {

	private String name;
	private Class<? extends PageAwareEditor> editorClass;
	private Function<IEditorInput,PageAwareEditorInput> inputMapper;
	private PageAwareEditor editor;
	
	public PageAwareEditorDescriptor(PageAwareEditor editor) {
		this.editor = editor;
		this.editorClass = editor.getClass();
	}
	
	public PageAwareEditorDescriptor(Class<? extends PageAwareEditor> editorClass) {
		this.editorClass = editorClass;
	}
	
	public PageAwareEditorDescriptor(String name, Class<? extends PageAwareEditor> editorClass, Function<IEditorInput,PageAwareEditorInput> inputMapper) { //, Function<Resource, String> sourceCodeGenerator) {
		super();
		this.editorClass = editorClass;
		this.inputMapper = inputMapper;
		this.name = name;
	}
	
	public PageAwareEditor getEditor() {
		if (editor == null) {
			editor = newEditor();
		}
		return editor;
	}

	public String getPageName() {
		if (name == null) {
			name = getEditor().getPageName();
		}
		return name;
	}

	public Class<? extends PageAwareEditor> getEditorClass() {
		return editorClass;
	}

	public Function<IEditorInput, PageAwareEditorInput> getInputMapper() {
		if (inputMapper == null) {
			inputMapper = input -> getEditor().mapEditorInput(input);
		}
		return inputMapper;
	}
	
	public PageAwareEditor newEditor() {
		try {
			return editorClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
}

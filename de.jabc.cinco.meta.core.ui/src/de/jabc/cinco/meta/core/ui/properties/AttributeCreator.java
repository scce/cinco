/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ui.properties;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Display;

import de.jabc.cinco.meta.core.ui.properties.dialogs.InputComboBoxDialog;

public class AttributeCreator {

	public static String createAttribute(EAttribute attr) {
		String retval = null;
		Dialog dialog = getDialog(attr);
		if (dialog.open() == Dialog.OK)
			return getValue(dialog);
		return retval;
	}

	public static String createAttribute(EAttribute attr, Object selectedObject) {
		String retval = null;
		Dialog dialog = getDialog(attr, selectedObject);
		if (dialog.open() == Dialog.OK)
			return getValue(dialog);
		return retval;
	}
	
	private static Dialog getDialog(EAttribute attr) {
		if (attr.getEType() instanceof EEnum) {
			 EList<EEnumLiteral> input = ((EEnum) attr.getEType()).getELiterals();
			 InputComboBoxDialog d = new InputComboBoxDialog(Display.getCurrent().getActiveShell(), input);
			 return d;
		}
		else {
			return new InputDialog(
				Display.getCurrent().getActiveShell(), 
				"Add new item to " + attr.getName(), 
				"Add a new item to " + attr.getName() + ":", 
				getInitialValue(attr), 
				getValidator(attr)
			);
		}
	}
	
	private static Dialog getDialog(EAttribute attr, Object selectedObject) {
		if (attr.getEType() instanceof EEnum) {
			EList<EEnumLiteral> input = ((EEnum) attr.getEType()).getELiterals();
			InputComboBoxDialog d = new InputComboBoxDialog(Display.getCurrent().getActiveShell(),input);
			return d;
		}
		else {
			return new InputDialog(
				Display.getCurrent().getActiveShell(), 
				"Edit item from " + attr.getName(), 
				"Edit an item from " + attr.getName() + ":", 
				selectedObject.toString(),
				getValidator(attr)
			);
		}
	}

	private static IInputValidator getValidator(final EAttribute attr) {
		return new IInputValidator() {
			@Override
			public String isValid(String newText) {
				try {
					EcoreUtil.createFromString(attr.getEAttributeType(), newText);
				} catch (Exception e) {
					return "Input: \"" + newText + "\" is not a valid " + attr.getEAttributeType().getName();
				}
				return null;
			}
		};
	}

	private static String getValue(Dialog dialog) {
		if (dialog instanceof InputDialog)
			return ((InputDialog) dialog).getValue();
		if (dialog instanceof InputComboBoxDialog)
			return ((InputComboBoxDialog) dialog).getValue().toString();
		else
			return null;
	}
	
	private static String getInitialValue(EAttribute attr) {
		EDataType eAttributeType = attr.getEAttributeType();
		switch (eAttributeType.getName()) {
			case "EInt":
				return "0";
			case "EEnum":
				return "";
			default:
				break;
		}
		return null;
	}
	
}

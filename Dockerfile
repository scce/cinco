FROM debian:10.9
ENV CINCO_FILE cinco-*-linux.gtk.x86_64.zip
RUN apt-get update -qq && \
      apt-get upgrade -qq && \
      apt-get install -qq unzip
ADD de.jabc.cinco.meta.product/target/products/$CINCO_FILE .
RUN unzip -qq $CINCO_FILE && \
      rm $CINCO_FILE && \
      mv cinco-* cinco

FROM maven:3.8.1-openjdk-11-slim
COPY --from=0 cinco cinco
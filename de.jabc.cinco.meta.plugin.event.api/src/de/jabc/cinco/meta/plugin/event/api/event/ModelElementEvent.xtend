/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.event

import de.jabc.cinco.meta.plugin.event.api.payload.CanDeletePayload
import de.jabc.cinco.meta.plugin.event.api.payload.CanSelectPayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostDeletePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostSelectPayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreDeletePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreSelectPayload
import graphmodel.ModelElement

interface ModelElementEvent<Element extends ModelElement> extends IdentifiableElementEvent<Element> {
	
	/*** Delete ***/
	
	def boolean canDelete(Element element)
	def boolean canDelete(CanDeletePayload<Element> payload)
	
	def void preDelete(Element element)
	def void preDelete(PreDeletePayload<Element> payload)
	
	def Runnable postDelete(Element element)
	def Runnable postDelete(PostDeletePayload<Element> payload)
	
	
	
	/*** Select ***/
	
	def boolean canSelect(Element element)
	def boolean canSelect(CanSelectPayload<Element> payload)
	
	def void preSelect(Element element)
	def void preSelect(PreSelectPayload<Element> payload)
	
	def void postSelect(Element element)
	def void postSelect(PostSelectPayload<Element> payload)
	
}

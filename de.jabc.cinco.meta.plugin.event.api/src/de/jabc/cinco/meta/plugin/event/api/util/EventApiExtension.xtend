/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.util

import de.jabc.cinco.meta.core.event.util.EventCoreExtension
import de.jabc.cinco.meta.plugin.event.api.Activator
import de.jabc.cinco.meta.plugin.event.api.event.ContainerEvent
import de.jabc.cinco.meta.plugin.event.api.event.EdgeEvent
import de.jabc.cinco.meta.plugin.event.api.event.Event
import de.jabc.cinco.meta.plugin.event.api.event.GraphModelEvent
import de.jabc.cinco.meta.plugin.event.api.event.NodeEvent
import de.jabc.cinco.meta.plugin.event.api.payload.Payload
import graphmodel.Direction
import graphmodel.IdentifiableElement
import java.io.ByteArrayInputStream
import java.lang.reflect.Type
import java.util.ArrayList
import java.util.Calendar
import java.util.Date
import java.util.LinkedList
import java.util.regex.Pattern
import mgl.Annotatable
import mgl.Annotation
import mgl.Edge
import mgl.GraphModel
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.UserDefinedType
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.Resource.Diagnostic
import org.eclipse.graphiti.features.context.IResizeShapeContext
import org.eclipse.xtext.ISetup
import org.eclipse.xtext.resource.XtextResourceSet

import static de.jabc.cinco.meta.plugin.event.api.util.Color.*
import static de.jabc.cinco.meta.plugin.event.api.util.EventEnum.*
import static graphmodel.Direction.*
import static java.util.Calendar.DAY_OF_MONTH
import static java.util.Calendar.DAY_OF_WEEK
import static java.util.Calendar.HOUR_OF_DAY
import static java.util.Calendar.MINUTE
import static java.util.Calendar.MONTH
import static java.util.Calendar.SECOND
import static java.util.Calendar.YEAR

/**
 * @author Fabian Storek
 */
class EventApiExtension extends EventCoreExtension {
	
	val public static String EVENT_ANNOTATION_NAME      = 'event'
	val public static String EVENT_API_PACKAGE_SUFFIX   = 'event'
	val public static String PAYLOAD_API_PACKAGE_SUFFIX = 'payload'
	val static Pattern MGL_FILE_NAME_PATTERN            = Pattern.compile('''^.*?(?<fileName>[\w\-. ]+)\.mgl$''')
	
	
	
	/*** Plugin IDs ***/
	
	def String getEventApiPluginID() {
		Activator.PLUGIN_ID
	}
	
	
	
	/*** Annotation ***/
	
	def String getEventAnnotationName() {
		EVENT_ANNOTATION_NAME
	}
	
	def boolean isEventAnnotation(Annotation annotation) {
		annotation.name == EVENT_ANNOTATION_NAME
	}
	
	def boolean hasEventAnnotation(Annotatable element) {
		element.annotations.exists[isEventAnnotation]
	}
	
	def Annotation getEventAnnotation(Annotatable element) {
		element.annotations.findFirst[isEventAnnotation]
	}
	
	def boolean isEventEnabled(ModelElement element) {
		if (element.hasEventAnnotation) {
			return true
		}
		val superElement = element.superElement
		if (superElement === null) {
			return false
		}
		return superElement.isEventEnabled
	}
	
	
	
	/*** Payload API ***/
	
	def String getPayloadApiProjectName() {
		eventApiPluginID
	}
		
	def Class<?> getPayloadApiClass() {
		Payload
	}
	
	def Fqn getPayloadApiFqn() {
		payloadApiClass.toFqn
	}
	
	
	
	/*** Event API ***/
	
	def String getEventApiProjectName() {
		eventApiPluginID
	}
	
	def Class<?> getEventApiClass() {
		Event
	}
	
	def Class<?> getEventApiClass(IdentifiableElement element) {
		switch element {
			graphmodel.GraphModel: GraphModelEvent
			graphmodel.Container:  ContainerEvent
			graphmodel.Node:       NodeEvent
			graphmodel.Edge:       EdgeEvent
			default:               throw new IllegalArgumentException
		}
	}
	
	def Class<?> getEventApiClass(ModelElement element) {
		switch element {
			GraphModel:    GraphModelEvent
			NodeContainer: ContainerEvent
			Node:          NodeEvent
			Edge:          EdgeEvent
			default:       throw new IllegalArgumentException
		}
	}
	
	def Fqn getEventApiFqn() {
		eventApiClass.toFqn
	}
	
	def Fqn getEventApiFqn(IdentifiableElement element) {
		element.eventApiClass.toFqn
	}
	
	def Fqn getEventApiFqn(ModelElement element) {
		element.eventApiClass.toFqn
	}
	
	def boolean hasCreateEvent(ModelElement element) {
		switch element {
			GraphModel, Node, Edge: true
			default:                false
		}
	}
	
	def EventEnum getCanCreateEvent(ModelElement element) {
		switch element {
			GraphModel: CAN_CREATE_GRAPH_MODEL
			Node:       CAN_CREATE_NODE
			Edge:       CAN_CREATE_EDGE
			default:    throw new IllegalArgumentException
		}
	}
	
	def EventEnum getPreCreateEvent(ModelElement element) {
		switch element {
			GraphModel: PRE_CREATE_GRAPH_MODEL
			Node:       PRE_CREATE_NODE
			Edge:       PRE_CREATE_EDGE
			default:    throw new IllegalArgumentException
		}
	}
	
	def EventEnum getPostCreateEvent(ModelElement element) {
		switch element {
			GraphModel: POST_CREATE_GRAPH_MODEL
			Node:       POST_CREATE_NODE
			Edge:       POST_CREATE_EDGE
			default:    throw new IllegalArgumentException
		}
	}
	
	
	
	/*** Model element ***/
	
	def ModelElement getSuperElement(ModelElement element) {
		switch it: element {
			GraphModel:   	 extends
			NodeContainer: 	 extends
			Node:          	 extends
			Edge:          	 extends
			UserDefinedType: extends
			default:         throw new IllegalArgumentException
		}
	}
	
	
	
	/*** EventEnum ***/
	
	def Iterable<EventEnum> getEvents() {
		EventEnum.EVENTS
	}
	
	def Iterable<EventEnum> getEvents(ModelElement element) {
		events.filter [ accepts(element) ]
	}
	
	def Iterable<EventEnum> getEvents(IdentifiableElement element) {
		events.filter [ accepts(element) ]
	}
	
	
	
	/*** General ***/
	
	def Fqn toFqn(CharSequence fqn) {
		new Fqn(fqn)
	}
	
	def Fqn toFqn(Type type) {
		new Fqn(type)
	}
	
	def Class<?> getGraphmodelClass(Class<?> mglClass) {
		switch it: mglClass {
			case implementsOrExtends(GraphModel):            graphmodel.GraphModel
			case implementsOrExtends(NodeContainer):         graphmodel.Container
			case implementsOrExtends(Node):                  graphmodel.Node
			case implementsOrExtends(Edge):                  graphmodel.Edge
			case implementsOrExtends(graphmodel.GraphModel): graphmodel.GraphModel
			case implementsOrExtends(graphmodel.Container):  graphmodel.Container
			case implementsOrExtends(graphmodel.Node):       graphmodel.Node
			case implementsOrExtends(graphmodel.Edge):       graphmodel.Edge
			default: throw new IllegalArgumentException
		}
	}
	
	def Class<?> getGraphmodelClass(IdentifiableElement element) {
		element.class.graphmodelClass
	}
	
	def Class<?> getGraphmodelClass(ModelElement element) {
		element.class.graphmodelClass
	}
	
	def Class<?> getMglClass(Class<?> graphmodelClass) {
		switch it: graphmodelClass {
			case implementsOrExtends(graphmodel.GraphModel): GraphModel
			case implementsOrExtends(graphmodel.Container):  NodeContainer
			case implementsOrExtends(graphmodel.Node):       Node
			case implementsOrExtends(graphmodel.Edge):       Edge
			case implementsOrExtends(GraphModel):            GraphModel
			case implementsOrExtends(NodeContainer):         NodeContainer
			case implementsOrExtends(Node):                  Node
			case implementsOrExtends(Edge):                  Edge
			default: throw new IllegalArgumentException
		}
	}
	
	def Class<?> getMglClass(IdentifiableElement element) {
		element.class.mglClass
	}
	
	def Class<?> getMglClass(ModelElement element) {
		element.class.mglClass
	}
	
	def boolean implementsOrExtends(Class<?> subClass, Type superClass) {
		if (subClass == superClass) {
			return true
		}
		val interfaces = subClass.interfaces
		if (!interfaces.nullOrEmpty && interfaces.exists[implementsOrExtends(superClass)]) {
			return true
		}
		val superclass = subClass.superclass
		if (superclass !== null && superclass.implementsOrExtends(superClass)) {
			return true
		}
		return false
	}
	
	def boolean implementsOrExtends(Class<?> subClass, CharSequence superClass) {
		if (subClass.name == superClass) {
			return true
		}
		val interfaces = subClass.interfaces
		if (!interfaces.nullOrEmpty && interfaces.exists[implementsOrExtends(superClass)]) {
			return true
		}
		val superclass = subClass.superclass
		if (superclass !== null && superclass.implementsOrExtends(superClass)) {
			return true
		}
		return false
	}
	
	
	
	/*** MGL model ***/
	
	def String getModelPackageName(MGLModel model) {
		'''«model.package».«model.modelFileName.toLowerCase»'''
	}
	
	def String getModelFileName(MGLModel model) {
		val modelResourceUri = model.eResource.URI
		val platformString = modelResourceUri.toPlatformString(true)
		val uriString = platformString?: modelResourceUri.toFileString
		val matcher = MGL_FILE_NAME_PATTERN.matcher(uriString)
		if (matcher.matches) {
			return matcher.group('fileName')
		}
		else {
			throw new IllegalStateException('''The name of the MGL model "«model.package»" could not be resolved properly.''')
		}
	}
	
	def MGLModel getModel(ModelElement element) {
		switch it: element {
			GraphModel:      eContainer as MGLModel
			NodeContainer:   eContainer as MGLModel
			Node:            eContainer as MGLModel
			Edge:            eContainer as MGLModel
			UserDefinedType: eContainer as MGLModel
			default:         throw new RuntimeException('''Can not determine MGLModel for «it»''')
		}
	}
	
	
	
	/*** Model element ***/
	
	def String getElementPackageName(ModelElement element) {
		element.model.modelPackageName
	}
	
	def String getElementClassName(ModelElement element) {
		element.name.toFirstUpper
	}
	
	def Fqn getElementFqn(ModelElement element) {
		'''«element.elementPackageName».«element.elementClassName»'''.toFqn
	}
	
	def Iterable<ModelElement> getEventEnabledElements(MGLModel model) {
		(
			model.nodes +
			model.edges +
			model.graphModels
		)
		.filter [ isEventEnabled ]
	}
	
	def Iterable<ModelElement> getEventEnabledElements(GraphModel graphModel) {
		graphModel.model.eventEnabledElements
	}
	
	def Iterable<ModelElement> getEventEnabledElements(MGLModel model, EventEnum event) {
		model.eventEnabledElements.filter [ element | event.accepts(element) ]
	}
	
	def Iterable<ModelElement> getEventEnabledElements(GraphModel graphModel, EventEnum event) {
		graphModel.eventEnabledElements.filter [ element | event.accepts(element) ]
	}
	
	def getAncestorElements(ModelElement element) {
		val resultSet = newHashSet
		var currentElement = element
		while (currentElement !== null) {
			resultSet += currentElement
			currentElement = currentElement.superElement
		}
		return resultSet
	}
	
	def getAncestorElements(Iterable<ModelElement> elements) {
		val resultSet = newHashSet
		for (element: elements) {
			resultSet.addAll(element.ancestorElements)
		}
		return resultSet
	}
	
	/**
	 * Sorts a set of {@link ModelElement}s from first root elements (elements
	 * without super-elements) to last leaf elements (elements without
	 * sub-elements). The order of independent groups of classes is not
	 * determined.
	 */
	def sortByInheritance(Iterable<? extends ModelElement> elements) {
		val sortedList = new ArrayList<ModelElement>(elements.size)
		val remainingQueue = new LinkedList<ModelElement>
		remainingQueue.addAll(elements)
		while (!remainingQueue.empty) {
			val current = remainingQueue.pop // pops first
			val superElement = current.superElement
			if (superElement === null || !remainingQueue.contains(superElement)) {
				sortedList.add(current)
			}
			else {
				remainingQueue.add(current) // appends to end
			}
		}
		return sortedList
	}
	
	def <T> CharSequence ifElseCascade(Iterable<T> it, (T) => CharSequence condition, (T) => CharSequence elseIfBody, () => CharSequence elseBody) {
		if (nullOrEmpty) {
			return ''
		}
		val elseBodyResult = elseBody.apply?.toString
		val elseBlock = if (elseBodyResult.nullOrEmpty) {
			null
		}
		else {
			'''
				else {
					«elseBodyResult»
				}
			'''
		}
		return join(
			'if ',
			'else if ',
			elseBlock,
			[ obj |
				'''
					(«condition.apply(obj)») {
						«elseIfBody.apply(obj)»
					}
				'''
			]
		)
	}
	
	def <T> CharSequence ifElseCascade(Iterable<T> it, (T) => CharSequence condition, (T) => CharSequence elseIfBody) {
		ifElseCascade(condition, elseIfBody, [])
	}
	
	def ResizeDirectionSet getResizeDirectionSet(int oldWidth, int oldHeight, int oldX, int oldY,
	                                             int newWidth, int newHeight, int newX, int newY) {
		new ResizeDirectionSet(oldWidth, oldHeight, oldX, oldY, newWidth, newHeight, newX, newY)
	}
	
	def Direction getResizeDirection(int oldWidth, int oldHeight, int oldX, int oldY,
	                                 int newWidth, int newHeight, int newX, int newY) {
		getResizeDirectionSet(oldWidth, oldHeight, oldX, oldY, newWidth, newHeight, newX, newY).direction
	}
	
	def Direction getResizeDirection(IResizeShapeContext context) {
		switch context.direction {
			case IResizeShapeContext.DIRECTION_NORTH:      NORTH
			case IResizeShapeContext.DIRECTION_NORTH_EAST: NORTH_EAST
			case IResizeShapeContext.DIRECTION_EAST:       EAST
			case IResizeShapeContext.DIRECTION_SOUTH_EAST: SOUTH_EAST
			case IResizeShapeContext.DIRECTION_SOUTH:      SOUTH
			case IResizeShapeContext.DIRECTION_SOUTH_WEST: SOUTH_WEST
			case IResizeShapeContext.DIRECTION_WEST:       WEST
			case IResizeShapeContext.DIRECTION_NORTH_WEST: NORTH_WEST
			default:                                       UNSPECIFIED
		}
	}
	
	
	
	/*** Attribute parsing ***/
	
	def Color toColor(String colorString) throws IllegalArgumentException {
		new Color(colorString)
	}
	
	def style.Color toStyleColor(String colorString) throws IllegalArgumentException {
		colorString.toColor.toStyleColor
	}
	
	def boolean isHex(String colorString) {
		if (colorString.nullOrEmpty) return false
		val m = HEX_PATTERN.matcher(colorString)
		return m.matches
	}
	
	def boolean isRGB(String colorString) {
		if (colorString.nullOrEmpty) return false
		val m = RGB_PATTERN.matcher(colorString)
		if (!m.matches) return false
		try {
			val r = Integer.parseInt(m.group(1))
			val g = Integer.parseInt(m.group(2))
			val b = Integer.parseInt(m.group(3))
			return #[r, g, b].forall[ v | (0..255).contains(v) ]
		}
		catch (NumberFormatException e) {
			return false
		}
	}
	
	def boolean isRGBA(String colorString) {
		if (colorString.nullOrEmpty) return false
		val m = RGBA_PATTERN.matcher(colorString)
		if (!m.matches) return false
		try {
			val r = Integer.parseInt(m.group(1))
			val g = Integer.parseInt(m.group(2))
			val b = Integer.parseInt(m.group(3))
			val a = Integer.parseInt(m.group(4))
			return #[r, g, b, a].forall[ v | (0..255).contains(v) ]
		}
		catch (NumberFormatException e) {
			return false
		}
	}
	
	def boolean isCSSColor(String colorName) {
		val simpleName = colorName?.toLowerCase?.replaceAll("[^a-z]", "")
		if (simpleName.nullOrEmpty) return false
		return CSS_COLORS.containsKey(simpleName)
	}
	
	def <S extends ISetup> Resource getResource(String code, String fileExtension, Class<S> standaloneSetupClass) {
		if (code === null || fileExtension.nullOrEmpty || standaloneSetupClass === null) {
			return null
		}
		val stream = new ByteArrayInputStream(code.bytes)
		val standaloneSetup = standaloneSetupClass.constructors.findFirst[parameterCount == 0].newInstance as S
		val injector = standaloneSetup.createInjectorAndDoEMFRegistration
		val resourceSet = injector.getInstance(XtextResourceSet)
		val dummyResourceURI = URI.createURI('''dummy:/dummy.«fileExtension»''')
		val resource = resourceSet.createResource(dummyResourceURI)
		resource.load(stream, resourceSet.loadOptions)
		return resource
	}
	
	def <M extends EObject, S extends ISetup> M getModel(String code, Class<M> modelClass, String fileExtension, Class<S> standaloneSetupClass) {
		code.getResource(fileExtension, standaloneSetupClass).contents.head as M
	}
	
	def <S extends ISetup> EList<Diagnostic> getErrors(String code, String fileExtension, Class<S> standaloneSetupClass) {
		code.getResource(fileExtension, standaloneSetupClass).errors
	}
	
	def <S extends ISetup> EList<Diagnostic> getWarnings(String code, String fileExtension, Class<S> standaloneSetupClass) {
		code.getResource(fileExtension, standaloneSetupClass).warnings
	}
	
	def boolean hasErrors(Resource resource) {
		!resource.errors.empty
	}
	
	def boolean hasWarnings(Resource resource) {
		!resource.warnings.empty
	}
	
	def <S extends ISetup> boolean hasErrors(String code, String fileExtension, Class<S> standaloneSetupClass) {
		!code.getErrors(fileExtension, standaloneSetupClass).empty
	}
	
	def <S extends ISetup> boolean hasWarnings(String code, String fileExtension, Class<S> standaloneSetupClass) {
		!code.getWarnings(fileExtension, standaloneSetupClass).empty
	}
	
	def int getYear(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		return calendar.get(YEAR)
	}
	
	def int getMonth(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		// Calendar associates January == 0, February == 1, ...
		// Thus (+ 1) to get the typical January == 1, Feburary == 2, ...
		return calendar.get(MONTH) + 1
	}
	
	def Month getMonthLiteral(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		switch calendar.get(MONTH) {
			case Calendar.JANUARY:   Month.JANUARY
			case Calendar.FEBRUARY:  Month.FEBRUARY
			case Calendar.MARCH:     Month.MARCH
			case Calendar.APRIL:     Month.APRIL
			case Calendar.MAY:       Month.MAY
			case Calendar.JUNE:      Month.JUNE
			case Calendar.JULY:      Month.JULY
			case Calendar.AUGUST:    Month.AUGUST
			case Calendar.SEPTEMBER: Month.SEPTEMBER
			case Calendar.OCTOBER:   Month.OCTOBER
			case Calendar.NOVEMBER:  Month.NOVEMBER
			case Calendar.DECEMBER:  Month.DECEMBER
		}
	}
	
	def int getDay(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		return calendar.get(DAY_OF_MONTH)
	}
	
	def int getHour(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		return calendar.get(HOUR_OF_DAY)
	}
	
	def int getMinute(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		return calendar.get(MINUTE)
	}
	
	def int getSecond(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		return calendar.get(SECOND)
	}
	
	def DayOfWeek getDayOfWeek(Date date) {
		val calendar = Calendar.instance
		calendar.time = date
		return switch calendar.get(DAY_OF_WEEK) {
			case Calendar.MONDAY:    DayOfWeek.MONDAY
			case Calendar.TUESDAY:   DayOfWeek.TUESDAY
			case Calendar.WEDNESDAY: DayOfWeek.WEDNESDAY
			case Calendar.THURSDAY:  DayOfWeek.THURSDAY
			case Calendar.FRIDAY:    DayOfWeek.FRIDAY
			case Calendar.SATURDAY:  DayOfWeek.SATURDAY
			case Calendar.SUNDAY:    DayOfWeek.SUNDAY
		}
	}
	
	def boolean isMonday(Date date) {
		date.dayOfWeek === DayOfWeek.MONDAY
	}
	
	def boolean isTuesday(Date date) {
		date.dayOfWeek === DayOfWeek.TUESDAY
	}
	
	def boolean isWednesday(Date date) {
		date.dayOfWeek === DayOfWeek.WEDNESDAY
	}
	
	def boolean isThursday(Date date) {
		date.dayOfWeek === DayOfWeek.THURSDAY
	}
	
	def boolean isFriday(Date date) {
		date.dayOfWeek === DayOfWeek.FRIDAY
	}
	
	def boolean isSaturday(Date date) {
		date.dayOfWeek === DayOfWeek.SATURDAY
	}
	
	def boolean isSunday(Date date) {
		date.dayOfWeek === DayOfWeek.SUNDAY
	}
	
	def boolean isWeekday(Date date) {
		#[
			DayOfWeek.MONDAY,
			DayOfWeek.TUESDAY,
			DayOfWeek.WEDNESDAY,
			DayOfWeek.THURSDAY,
			DayOfWeek.FRIDAY
		].contains(date.dayOfWeek)
	}
	
	def boolean isWeekend(Date date) {
		#[
			DayOfWeek.SATURDAY,
			DayOfWeek.SUNDAY
		].contains(date.dayOfWeek)
	}
	
}

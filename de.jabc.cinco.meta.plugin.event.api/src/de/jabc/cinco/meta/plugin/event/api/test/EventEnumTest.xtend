/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.test

import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.*

/**
 * Unit test (JUnit 5) for {@link EventEnum}.
 * @author Fabian Storek
 */
class EventEnumTest {
	
	extension EventApiExtension = new EventApiExtension
	
	@Test
	def void testEventClasses() {
		
		// EventEnum: Event list
		assertNotNull(EventEnum.EVENTS)
		assertFalse(EventEnum.EVENTS.empty)
		
		
		for (event: EventEnum.EVENTS) {
			
			// EventEnum
			assertNotNull(event)
			
			// EventEnum: Method name
			assertNotNull(event.methodName)
			assertFalse(event.methodName.empty)
			
			// EventEnum: Event class
			assertNotNull(event.eventClass)
			assertFalse(event.eventClass.declaredMethods.empty)
			
			// EventEnum: Payload class
			assertNotNull(event.payloadClass)
			assertFalse(event.payloadClass.declaredFields.empty)
			
			// Event class: Methods
			val eventMethods = event.eventClass.declaredMethods.filter [ name == event.methodName ]
			assertNotNull(eventMethods)
			assertEquals(eventMethods.size, 2)
			
			// Event class: Payload method
			val payloadMethod = eventMethods.findFirst [
				parameters.size == 1 &&
				parameters.head.type.implementsOrExtends(event.payloadClass)
			]
			assertNotNull(payloadMethod)
			
			// Event class: Direct method
			val directMethod = eventMethods.findFirst [ it !== payloadMethod ]
			assertNotNull(directMethod)
			
			// Event class: Return types
			assertEquals(payloadMethod.returnType, directMethod.returnType)
			
			// Event class: Direct method: Parameters == Payload class: Fields
			assertEquals(directMethod.parameters.size, event.payloadClass.declaredFields.size)
			for (it: directMethod.parameters <> event.payloadClass.declaredFields) {
				assertEquals(
					left.parameterizedType.toFqn.unwrapped.fullyQualifiedNameWithSuffix,
					right.genericType.toFqn.unwrapped.fullyQualifiedNameWithSuffix
				)
			}
			
		}
		
	}
	
}

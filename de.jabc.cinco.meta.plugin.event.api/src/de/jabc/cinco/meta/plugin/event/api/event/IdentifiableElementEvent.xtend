/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.event

import de.jabc.cinco.meta.plugin.event.api.payload.CanAttributeChangePayload
import de.jabc.cinco.meta.plugin.event.api.payload.CanDoubleClickPayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostAttributeChangePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostDoubleClickPayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreAttributeChangePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreDoubleClickPayload
import graphmodel.IdentifiableElement

interface IdentifiableElementEvent<Element extends IdentifiableElement> extends Event<Element> {
	
	/*** AttributeChange ***/
	
	def String canAttributeChange(Element element, String attribute, Object newValue)
	def String canAttributeChange(CanAttributeChangePayload<Element> payload)
	
	def void preAttributeChange(Element element, String attribute, Object newValue)
	def void preAttributeChange(PreAttributeChangePayload<Element> payload)

	def void postAttributeChange(Element element, String attribute, Object oldValue)
	def void postAttributeChange(PostAttributeChangePayload<Element> payload)
	
	
	
	/*** DoubleClick ***/
	
	def boolean canDoubleClick(Element element)
	def boolean canDoubleClick(CanDoubleClickPayload<Element> payload)
	
	def void preDoubleClick(Element element)
	def void preDoubleClick(PreDoubleClickPayload<Element> payload)
	
	def void postDoubleClick(Element element)
	def void postDoubleClick(PostDoubleClickPayload<Element> payload)
	
}

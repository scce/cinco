/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.util

import graphmodel.Direction
import org.eclipse.xtend.lib.annotations.Data

import static graphmodel.Direction.*

/**
 * This class helps with determining the directions in which a 2D object has
 * been resized.
 * <p>
 * The calculated values {@link #north north}, {@link #east east}, {@link
 * #south south} and {@link #west west} describe the movement of the respective
 * edge away form the center. Thus, negative values describe movement towards
 * the center.
 * <p>
 * The attribute {@link #direction direction} gives a simplified view and can
 * only convey the resizing of sides and corners. It shows which side or corner
 * the user dragged to resize the object. Resizing opposite sides (e. g. north
 * and south) will result in {@link Direction#UNSPECIFIED UNSPECIFIED}.
 * <p>
 * You can use the {@link #toString() toString()} method for visualization.
 * <p>
 * <b>Example 1:</b><br>
 * The south west / bottom right corner was dragged 10 pixels to the left
 * and 10 pixels downwards.
 * <pre>
 * new ResizeDirectionSet(100, 100, 100, 100,
 *                         90, 110, 100, 100)
 * 
 *        N: ±0
 *       ┌───────┐
 *       │       │
 * W: ±0 │       ← E: -10
 *       │       │
 *       └── ↓ ──┘
 *        S: +10
 * 
 * Direction: SOUTH_EAST
 * </pre>
 * <p>
 * <b>Example 2:</b><br>
 * The object was moved and resized programmatically.
 * <pre>
 * new ResizeDirectionSet(100, 100, 100, 100,
 *                         90, 120, 100,  90)
 * 
 *        N: +10
 *       ┌── ↑ ──┐
 *       │       │
 * W: ±0 │       ← E: -10
 *       │       │
 *       └── ↓ ──┘
 *        S: +10
 * 
 * Direction: UNSPECIFIED
 * </pre>
 */
@Data
class ResizeDirectionSet {
	
	val int oldWidth
	val int oldHeight
	val int oldX
	val int oldY
	
	val int newWidth
	val int newHeight
	val int newX
	val int newY
	
	/**
	 * This value describes the movement of the north / top edge.<br>
	 * A positive value means the edge moved this many pixels away from the center / upwards.<br>
	 * A negative value means the edge moved towards the center / downwards.
	 */
	val int north
	
	/**
	 * This value describes the movement of the east / right edge.<br>
	 * A positive value means the edge moved this many pixels away from the center / to the right.<br>
	 * A negative value means the edge moved towards the center / to the left.
	 */
	val int east
	
	/**
	 * This value describes the movement of the south / bottom edge.<br>
	 * A positive value means the edge moved this many pixels away from the center / downwards.<br>
	 * A negative value means the edge moved towards the center / upwards.
	 */
	val int south
	
	/**
	 * This value describes the movement of the west / left edge.<br>
	 * A positive value means the edge moved this many pixels away from the center / to the left.<br>
	 * A negative value means the edge moved towards the center / to the right.
	 */
	val int west
	
	/**
	 * This attribute gives a simplified view and can only convey the resizing of sides and corners.<br>
	 * It shows which side or corner the user dragged to resize the object.<br>
	 * Resizing opposite sides (e. g. north and south) will result in {@link Direction#UNSPECIFIED UNSPECIFIED}.
	 */
	val Direction direction
	
	new (int oldWidth, int oldHeight, int oldX, int oldY,
	     int newWidth, int newHeight, int newX, int newY) {
		this.oldWidth  = oldWidth
		this.oldHeight = oldHeight
		this.oldX      = oldX
		this.oldY      = oldY
		this.newWidth  = newWidth
		this.newHeight = newHeight
		this.newX      = newX
		this.newY      = newY
		this.north     = oldY - newY
		this.west      = oldX - newX
		this.south     = (newHeight - oldHeight) - north
		this.east      = (newWidth  - oldWidth ) - west
		this.direction =
			     if (north != 0 && east == 0 && south == 0 && west == 0) NORTH
			else if (north != 0 && east != 0 && south == 0 && west == 0) NORTH_EAST
			else if (north == 0 && east != 0 && south == 0 && west == 0) EAST
			else if (north == 0 && east != 0 && south != 0 && west == 0) SOUTH_EAST
			else if (north == 0 && east == 0 && south != 0 && west == 0) SOUTH
			else if (north == 0 && east == 0 && south != 0 && west != 0) SOUTH_WEST
			else if (north == 0 && east == 0 && south == 0 && west != 0) WEST
			else if (north != 0 && east == 0 && south == 0 && west != 0) NORTH_WEST
			else UNSPECIFIED
	}
	
	/**
	 * Returns an arrow pointing in the {@link #direction direction}.
	 * Returns a question mark ({@code ?}), if the direction is
	 * {@link Direction#UNSPECIFIED UNSPECIFIED}.
	 */
	def String getArrow(Direction direction) {
		switch (direction) {
			case NORTH:      '↑'
			case NORTH_EAST: '↗︎'
			case EAST:       '→'
			case SOUTH_EAST: '↘︎'
			case SOUTH:      '↓'
			case SOUTH_WEST: '↙︎'
			case WEST:       '←'
			case NORTH_WEST: '↖︎'
			default:         '?'
		}
	}
	
	/**
	 * This method visualizes the resizing.
	 * <p>
	 * <b>Example 1:</b><br>
	 * The south west / bottom right corner was dragged 10 pixels to the left
	 * and 10 pixels downwards.
	 * <pre>
	 * new ResizeDirectionSet(100, 100, 100, 100,
	 *                         90, 110, 100, 100)
	 * 
	 *        N: ±0
	 *       ┌───────┐
	 *       │       │
	 * W: ±0 │       ← E: -10
	 *       │       │
	 *       └── ↓ ──┘
	 *        S: +10
	 * 
	 * Direction: SOUTH_EAST
	 * </pre>
	 * <p>
	 * <b>Example 2:</b><br>
	 * The object was moved and resized programmatically.
	 * <pre>
	 * new ResizeDirectionSet(100, 100, 100, 100,
	 *                         90, 120, 100,  90)
	 * 
	 *        N: +10
	 *       ┌── ↑ ──┐
	 *       │       │
	 * W: ±0 │       ← E: -10
	 *       │       │
	 *       └── ↓ ──┘
	 *        S: +10
	 * 
	 * Direction: UNSPECIFIED
	 * </pre>
	 */
	override String toString() {
		'''
		«spaces»  N: «north.format»
		«spaces» ┌──«arrowN»──┐
		«spaces» │       │
		W: «west.format» «arrowW»       «arrowE» E: «east.format»
		«spaces» │       │
		«spaces» └──«arrowS»──┘
		«spaces»  S: «south.format»
		
		Direction: «direction»
		'''
	}
	
	def private String format(int x) {
		'''«if (x > 0) '+' else if (x == 0) '±'»«x»'''
	}
	
	def private spaces() {
		' '.repeat(west.format.length + 3)
	}
	
	def private arrowN() {
		if (north > 0) ' ↑ '
		else if (north == 0) '───'
		else ' ↓ '
	}
	
	def private arrowE() {
		if (east > 0) '→'
		else if (east == 0) '│'
		else '←'
	}
	
	def private arrowS() {
		if (south > 0) ' ↓ '
		else if (south == 0) '───'
		else ' ↑ '
	}
	
	def private arrowW() {
		if (west > 0) '←'
		else if (west == 0) '│'
		else '→'
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.payload

import graphmodel.ModelElement
import org.eclipse.xtend.lib.annotations.Data

interface DeletePayload<Element extends ModelElement> extends Payload<Element> {
	
	// Intentionally left blank
	
}

@Data
class CanDeletePayload<Element extends ModelElement> implements DeletePayload<Element> {
	
	val Element element
	
}

@Data
class PreDeletePayload<Element extends ModelElement> implements DeletePayload<Element> {
	
	val Element element
	
}

@Data
class PostDeletePayload<Element extends ModelElement> implements DeletePayload<Element> {
	
	val Element element
	
}

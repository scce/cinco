/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.event

import de.jabc.cinco.meta.plugin.event.api.payload.CanCreateNodePayload
import de.jabc.cinco.meta.plugin.event.api.payload.CanMovePayload
import de.jabc.cinco.meta.plugin.event.api.payload.CanResizePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostCreateNodePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostMovePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostResizePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreCreateNodePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreMovePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreResizePayload
import graphmodel.Direction
import graphmodel.ModelElementContainer
import graphmodel.Node

interface NodeEvent<Element extends Node> extends ModelElementEvent<Element> {
	
	/*** Create ***/
	
	def boolean canCreate(Class<? extends Element> elementClass, ModelElementContainer container, int x, int y, int width, int height)
	def boolean canCreate(CanCreateNodePayload<Element> payload)
	
	def void preCreate(Class<? extends Element> elementClass, ModelElementContainer container, int x, int y, int width, int height)
	def void preCreate(PreCreateNodePayload<Element> payload)
	
	def void postCreate(Element element)
	def void postCreate(PostCreateNodePayload<Element> payload)
	
	
	
	/*** Move ***/
	
	def boolean canMove(Element element, ModelElementContainer newContainer, int newX, int newY)
	def boolean canMove(CanMovePayload<Element> payload)
	
	def void preMove(Element element, ModelElementContainer newContainer, int newX, int newY)
	def void preMove(PreMovePayload<Element> payload)
	
	def void postMove(Element element, ModelElementContainer oldContainer, int oldX, int oldY)
	def void postMove(PostMovePayload<Element> payload)
	
	
	
	/*** Resize ***/
	
	def boolean canResize(Element element, int newWidth, int newHeight, int newX, int newY, Direction direction)
	def boolean canResize(CanResizePayload<Element> payload)
	
	def void preResize(Element element, int newWidth, int newHeight, int newX, int newY, Direction direction)
	def void preResize(PreResizePayload<Element> payload)
	
	def void postResize(Element element, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction)
	def void postResize(PostResizePayload<Element> payload)
	
}

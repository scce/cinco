/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.event

import de.jabc.cinco.meta.plugin.event.api.payload.CanCreateGraphModelPayload
import de.jabc.cinco.meta.plugin.event.api.payload.CanSavePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostCreateGraphModelPayload
import de.jabc.cinco.meta.plugin.event.api.payload.PostSavePayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreCreateGraphModelPayload
import de.jabc.cinco.meta.plugin.event.api.payload.PreSavePayload
import graphmodel.GraphModel
import org.eclipse.core.runtime.IPath

interface GraphModelEvent<Element extends GraphModel> extends ModelElementContainerEvent<Element> {
	
	/*** Create ***/
	
	def String canCreate(Class<? extends Element> elementClass, String name, IPath path)
	def String canCreate(CanCreateGraphModelPayload<Element> payload)
	
	def void preCreate(Class<? extends Element> elementClass, String name, IPath path)
	def void preCreate(PreCreateGraphModelPayload<Element> payload)
	
	def void postCreate(Element element)
	def void postCreate(PostCreateGraphModelPayload<Element> payload)
	
	
	
	/*** Save ***/
	
	def boolean canSave(Element element)
	def boolean canSave(CanSavePayload<Element> payload)
	
	def void preSave(Element element)
	def void preSave(PreSavePayload<Element> payload)
	
	def void postSave(Element element)
	def void postSave(PostSavePayload<Element> payload)
	
}

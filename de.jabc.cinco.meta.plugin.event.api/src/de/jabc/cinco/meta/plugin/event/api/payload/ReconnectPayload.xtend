/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.event.api.payload

import graphmodel.Edge
import graphmodel.Node
import org.eclipse.xtend.lib.annotations.Data

interface ReconnectPayload<Element extends Edge> extends Payload<Element> {
	
	// Intentionally left blank
	
}

@Data
class CanReconnectPayload<Element extends Edge> implements ReconnectPayload<Element> {
	
	val Element element
	val Node newSourceNode
	val Node newTargetNode
	
}

@Data
class PreReconnectPayload<Element extends Edge> implements ReconnectPayload<Element> {
	
	val Element element
	val Node newSourceNode
	val Node newTargetNode
	
}

@Data
class PostReconnectPayload<Element extends Edge> implements ReconnectPayload<Element> {
	
	val Element element
	val Node oldSourceNode
	val Node oldTargetNode
	
}

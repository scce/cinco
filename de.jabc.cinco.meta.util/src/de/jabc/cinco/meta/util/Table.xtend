/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.util

import java.util.LinkedList

/**
 * Creates ASCII tables like this one:
 * <pre>
 * ┌───┬──────────┬─────┐
 * │ ID│Item      │Price│
 * ├───┼──────────┼─────┤
 * │ 42│Towel     │ 25 €│
 * │178│Headphones│130 €│
 * │ 55│Clock     │ 70 €│
 * ├───┼──────────┼─────┤
 * │   │Total     │225 €│
 * └───┴──────────┴─────┘
 * </pre>
 * To build a table write code like this:
 * <pre>
 * new Table()
 *     .setHeader("ID", "Item", "Price")
 *     .setAlignment(RIGHT, LEFT, RIGHT)
 *     .addRow(42, "Towel", "25 €")
 *     .addRow(178, "Headphones", "130 €")
 *     .addRow(55, "Clock", "70 €")
 *     .addSeparator()
 *     .addRow("", "Total", "225 €")
 *     .print();
 * </pre>
 */
class Table {
	
	var boolean              useSafeCharacters
	var String[]             header
	var Alignment[]          alignment
	var LinkedList<String[]> rows
	var int[]                columnWidths
	var int                  columnCount
	
	/**
	 * Creates a new empty table.
	 */
	new () {
		useSafeCharacters = false
		header            = #[]
		alignment         = #[]
		rows              = newLinkedList
		columnWidths      = #[]
		columnCount       = 0
	}
	
	/**
	 * Creates a new empty table with a header.
	 */
	new (String ... header) {
		useSafeCharacters = false
		setHeader(header)
		alignment         = #[]
		rows              = newLinkedList
		columnWidths      = #[]
		columnCount       = 0
	}
	
	/**
	 * Creates a new empty table with a header.
	 */
	new (Object ... header) {
		useSafeCharacters = false
		setHeader(header)
		alignment         = #[]
		rows              = newLinkedList
		columnWidths      = #[]
		columnCount       = 0
	}
	
	/**
	 * Sets the header for each column.
	 */
	def setHeader(String ... header) {
		this.header = header.map[ value | value?: "(null)" ] as String[]
		return this
	}
	
	/**
	 * Sets the header for each column.
	 */
	def setHeader(Object ... header) {
		this.header = header.map[ value | value?.toString?: "(null)" ] as String[]
		return this
	}
	
	/**
	 * Sets the alignment for each column.
	 */
	def setAlignment(Alignment ... alignment) {
		this.alignment = alignment
		return this
	}
	
	/**
	 * Adds a new data row.
	 */
	def addRow(String ... tuple) {
		rows.add(tuple.map[ value | value?: "(null)" ])
		return this
	}
	
	/**
	 * Adds a new data row.
	 */
	def addRow(Object ... tuple) {
		rows.add(tuple.map[ value | value?.toString?: "(null)" ])
		return this
	}
	
	/**
	 * Adds a horizontal separator.
	 */
	def addSeparator() {
		rows.add(null)
		return this
	}
	
	/**
	 * Enable or disable the use of safe characters ({@code | - +}) instead
	 * of Unicode table characters ({@code ┌ ┬ ┐ ├ ┼ ┤ └ ┴ ┘}).
	 */
	def useSafeCharacters(boolean enable) {
		useSafeCharacters = enable
		return this
	}
	
	/**
	 * Parses the table as a Sting.
	 */
	override toString() {
		columnCount = Integer.max(
			header.length,
			rows.filterNull.map[length].max
		)
		columnWidths = (0 ..< columnCount).map[ i |
			Integer.max(
				header.safeGet(i).safeLength,
				rows.filterNull.map[safeGet(i).safeLength].max
			)
		]
		return '''
			«formatTop»
			«IF !header.nullOrEmpty»
				«header.formatRow»
				«formatSeparator»
			«ENDIF»
			«FOR row: rows»
				«row.formatRow»
			«ENDFOR»
			«formatBottom»
		'''
	}
	
	/**
	 * Prints the table to the console.
	 */
	def print() {
		print(toString)
		return this
	}
	
	def private String formatValue(String[] row, int column) {
		val columnWidth = columnWidths.safeGet(column)
		if (row === null) {
			if (useSafeCharacters) {
				return "-".repeat(columnWidth)
			}
			else {
				return "─".repeat(columnWidth)
			}
		}
		val value = row.safeGet(column)?: ""
		if (value === null) {
			return " ".repeat(columnWidth)
		}
		val spaces = " ".repeat(columnWidth - value.length)
		val align = alignment.safeGet(column)?: Alignment.LEFT
		return switch align {
			case LEFT:  value + spaces
			case RIGHT: spaces + value
		}
	}
	
	def private String formatRow(String[] row) {
		if (row === null) {
			formatSeparator
		}
		else if (useSafeCharacters) {
			(0 ..< columnCount).join("|", "|", "|") [ i | row.formatValue(i) ]
		}
		else {
			(0 ..< columnCount).join("│", "│", "│") [ i | row.formatValue(i) ]
		}
	}
	
	def private String formatTop() {
		if (!useSafeCharacters) {
			(0 ..< columnCount).join("┌", "┬", "┐") [ i | null.formatValue(i) ]
		}
	}
	
	def private String formatSeparator() {
		if (useSafeCharacters) {
			(0 ..< columnCount).join("|", "+", "|") [ i | null.formatValue(i) ]
		}
		else {
			(0 ..< columnCount).join("├", "┼", "┤") [ i | null.formatValue(i) ]
		}
	}
	
	def private String formatBottom() {
		if (!useSafeCharacters) {
			(0 ..< columnCount).join("└", "┴", "┘") [ i | null.formatValue(i) ]
		}
		
	}
	
	def private <T> T safeGet(Iterable<T> iterable, int index) {
		if (iterable === null) {
			return null
		}
		val iterator = iterable.iterator
		if (iterator === null) {
			return null
		}
		var currentIndex = -1
		while (iterator.hasNext) {
			currentIndex += 1
			val currentObject = iterator.next
			if (currentIndex == index) {
				return currentObject
			}
		}
		return null
	}
	
	def private int safeLength(String str) {
		if (str !== null) {
			return str.length
		}
		else {
			return 0
		}
	}
	
	enum Alignment {
		LEFT,
		RIGHT
	}
	
}

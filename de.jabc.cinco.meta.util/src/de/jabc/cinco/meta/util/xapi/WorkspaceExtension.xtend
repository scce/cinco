/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.util.xapi

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset
import java.util.HashSet
import java.util.List
import java.util.function.Predicate
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.IncrementalProjectBuilder
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.IConfigurationElement
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.core.runtime.OperationCanceledException
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.osgi.framework.Bundle
import org.osgi.framework.FrameworkUtil

import static extension org.eclipse.emf.ecore.util.EcoreUtil.getURI

/**
 * Workspace-specific extension methods.
 * 
 * @author Steve Bosselmann
 */
class WorkspaceExtension {
	
	/**
	 * Retrieves the current workspace.
	 */
	def getWorkspace() {
		ResourcesPlugin.workspace
	}

	/**
	 * Retrieve the root container of the current workspace that holds all
	 * projects and files.
	 */
	def getWorkspaceRoot() {
		workspace.root
	}
	
	/**
	 * Retrieves the resource for the specified URI from the workspace,
	 * if existent. Returns {@code null} if the resource does not exist.
	 */
	def getIResource(URI uri) {
		if (uri !== null) {
			workspace.root.findMember(
				if (uri.isPlatformResource)
					uri.toPlatformString(true)
				else uri.path)
		}
	}
	
	/**
	 * Retrieves the resource for the specified object from the workspace,
	 * if existent. Returns {@code null} if the resource does not exist.
	 */
	def getIResource(EObject eobj) {
		eobj.getURI.IResource
	}
	
	/**
	 * Retrieves the project that contains the resource of the specified
	 * object from the workspace, if existent. Returns {@code null} if the
	 * resource or the project does not exist.
	 */
	def getProject(EObject eobj) {
		eobj.getURI?.IResource?.project
	}

	/**
	 * Retrieves the file for the specified URI from the workspace,
	 * if existent. Returns {@code null} if the resource does not exist
	 * or it is not a file.
	 */
	def getFile(URI uri) {
		val res = uri.IResource
		if (res instanceof IFile)
			res as IFile
		else null
	}
	
	/**
	 * Retrieves the file for the specified EObject from the workspace,
	 * if existent. Returns {@code null} if the resource does not exist
	 * or it is not a file.
	 */
	def getFile(EObject eobj) {
		eobj.getURI.getFile
	}
	
	/**
	 * Creates the specified resource.
	 * Recursively creates its parents as well, if not existent yet.
	 */
	def <T extends IResource> T create(T resource) throws CoreException {
		if (resource === null || resource.exists)
			return resource
		val monitor = new NullProgressMonitor
		if (!resource.parent.exists)
			resource.parent.create
		switch resource.type {
			case IResource.FILE:
				(resource as IFile).create(newEmptyInputStream, true, monitor)
			case IResource.FOLDER:
				(resource as IFolder).create(true, true, monitor)
			case IResource.PROJECT: {
				(resource as IProject) => [
					create(monitor)
					open(monitor)
				]
			}
		}
		return resource
	}
	
	/**
	 * Creates a file with the specified name and content.
	 * Only replaces its content if the file already exists.
	 */
	def createFile(IContainer container, String name, CharSequence content) {
		container.createFile(name, content?.toString, true)
	}
	
	/**
	 * Creates a file with the specified name and content.
	 * Only replaces its content if the file already exists.
	 */
	def createFile(IContainer container, String name, String content) {
		container.createFile(name, content, true)
	}
	
	/**
	 * Creates a file with the specified name and content.
	 * @param createFolders decides whether all folders in the given name are created
	 */
	def createFile(IContainer container, String name, String content, boolean createFolders) {
		val file = container.getFile(new Path(name))
		val stream = content.toInputStream(file.charsetObject)
		try {
			container.createFile(name, stream, createFolders)
		} catch (Exception e) {
			e.printStackTrace()
		}
		return file
	}
	
	/**
	 * Creates a file with the specified name and content provided
	 * by the specified input stream.
	 * Only replaces its content if the file already exists.
	 */
	def createFile(IContainer container, String name, InputStream content) {
		container.createFile(name, content, true)
	}
	
	/**
	 * Creates a file with the specified name and content provided
	 * by the specified input stream.
	 * @param createFolders decides whether all folders in the given name are created
	 */
	def createFile(IContainer container, String name, InputStream content, boolean createFolders) {
		if (createFolders) {
			val index = name.lastIndexOf("/")
			if (index > -1) {
				val folderPath = name.substring(0, index)
				container.createFolder(folderPath)
			}
		}
		val file = container.getFile(new Path(name))
		val stream = content?: newEmptyInputStream
		try {
			if (file.exists)
				file.setContents(stream, true, true, null)
			else
				file.create(stream, true, null)
		} catch (Exception e) {
			e.printStackTrace
		} finally {
			stream.close
		}
		return file
	}
	
	/**
	 * Creates a folder with the specified name.
	 */
	def createFolder(IContainer container, String name) {
		container.createFolder(new Path(name))
	}
	
	/**
	 * Creates a folder with the specified path.
	 */
	def createFolder(IContainer container, IPath path) {
		val folder = container.getFolder(path)
		if (!folder.exists) try {
			folder.create()
		} catch (CoreException e) {
			e.printStackTrace
		}
		return folder
	}
	
	/**
	 * Creates a Path of Folders 
	 */
	def createFolders(IContainer container, IPath path){
		var currentContainer = container
		for(segment: path.segments){
			currentContainer = createFolder(currentContainer, new Path(segment))	
		}
		currentContainer 
	}
	
	/**
	 * Deletes a resource
	 */
	def void delete(IResource resource) {
		if (resource === null || !resource.exists) {
			return
		}
		val monitor = new NullProgressMonitor
		switch resource.type {
			case IResource.FILE:
				(resource as IFile).delete(true, false, monitor)
			case IResource.FOLDER:
				(resource as IFolder).delete(true, false, monitor)
			case IResource.PROJECT: {
				(resource as IProject).delete(true, true, monitor)
			}
		}
	}
	
	/**
	 * Retrieve all files in the container.
	 * Only recurses through accessible sub-containers (e.g. open projects).
	 * <p>Convenience method equal to <code>getResource(IFile.class)</code></p>
	 */
	def getFiles(IContainer container) {
		container.getFiles(null)
	}
	
	/**
	 * Retrieve all files in the container that fulfill the specified
	 * file-related constraint.
	 * Only recurses through accessible sub-containers (e.g. open projects).
	 * <p>Convenience method equal to <code>getResource(IFile.class, ...)</code></p>
	 */
	def getFiles(IContainer container, Predicate<IFile> fileConstraint) {
		container.getResources(IFile, fileConstraint, null)
	}
	
	/**
	 * Retrieve all files in the container that fulfill the specified
	 * file-related constraint.
	 * Only recurses through accessible sub-containers (e.g. open projects)
	 * that fulfill the specified container-related constraint.
	 * <p>Convenience method equal to <code>getResource(IFile.class, ...)</code></p>
	 */
	def getFiles(IContainer container, Predicate<IFile> fileConstraint, Predicate<IContainer> contConstraint) {
		container.getResources(IFile, fileConstraint, contConstraint)
	}
	
	/**
	 * Retrieves the folder identified by the path of this container that
	 * is interpreted relative to this container's project.
	 * Returns {@code null} if path or folder does not exist.
	 */
	def getFolder(IContainer container) {
		val path = container.projectRelativePath
		if (path === null)
			return null
		container.getFolder(path)
	}
	
	/**
	 * Retrieve all resources of the specified type.
	 * Only recurses through accessible sub-containers.
	 */
	def <T extends IResource> getResources(IContainer container, Class<T> clazz) {
		container.getResources(clazz, null, null)
	}
	
	/**
	 * Retrieve all resources of the specified type that fulfill the specified
	 * resource-related constraint.
	 * Only recurses through accessible sub-containers.
	 */
	def <T extends IResource> getResources(IContainer container, Class<T> clazz, Predicate<T> resConstraint) {
		container.getResources(clazz, resConstraint, null)
	}
	
	/**
	 * Retrieve all resources of the specified type that fulfill the specified
	 * resource-related constraint.
	 * Only recurses through accessible sub-containers that fulfill the
	 * specified container-related constraint.
	 * <p>
	 * Example: Recursively retrieve derived files from folders:
	 * <pre><code>
	 * getResource(IFile.class,
	 *   file -> file.isDerived(),
	 *   cont -> cont instanceof IFolder
	 * )
	 * </code></pre>
	 * </p>
	 */
	def <T extends IResource> List<T> getResources(IContainer container, Class<T> clazz, Predicate<T> resConstraint, Predicate<IContainer> contConstraint) {
		val List<T> list = newArrayList
		if (container === null
				|| !container.exists
				|| !container.isAccessible
				|| contConstraint !== null && !contConstraint.test(container)) {
			return list
		}
		var IResource[] members = null
		try {
			members = container.members
		} catch (CoreException e) {
			e.printStackTrace();
		}
		if (members !== null) {
			for (member : members) {
				if (clazz.isAssignableFrom(member.class)) {
					val T resource = member as T
					if (resConstraint === null || resConstraint.test(resource)) {
						list.add(resource)
					}
				}
				if (member instanceof IContainer) {
					val cont = member as IContainer
					list.addAll(getResources(cont, clazz, resConstraint, contConstraint))
				}
			}
		}
		return list
	}
	
	/**
	 * Creates a {@code Resource} object for the specified URI.
	 * 
	 * @param uri The URI. Must not be {@code null}.
	 * @return the {@code Resource} object, or {@code null} if its creation failed.
	 */
	def createResource(URI uri) {
		extension val ext = new CodingExtension;
		[ (new ResourceSetImpl).getResource(uri, true) ]
			.onException[warn("Failed to create Resource object for URI: " + uri)]
	}
	
	/**
	 * Retrieves the OSGi context of the caller, i.e. the bundle context of its class.
	 */
	def getOSGiContext(Object caller) {
		FrameworkUtil.getBundle(caller.class)?.bundleContext
	}
	
	/**
	 * Retrieves a list of bundle files with specific file extensions.
	 * 
	 * @param bundle  The bundle to be searched through.
	 * @param fileExtension  Must not be {@code null} or empty.
	 * @return  the URIs of the files that match the specified criteria.
	 * @throws IllegalArgumentException  if {@code fileExtension} is {@code null} or empty.
	 */
	def findFiles(Bundle bundle, String fileExtension) {
		extension val ext = new CollectionExtension
		if (fileExtension.nullOrEmpty)
			throw new IllegalArgumentException("fileExtension must not be empty")
   		(bundle.findEntries("/", '''*.«fileExtension»''',true)?.toList ?: newArrayList)
   			.map[toString]
   			.drop[endsWith("/")] // findEntries also lists hidden directories, like .data/
   			.map[URI.createURI(it)]
	}
	
	/**
	 * Retrieves a list file URIs with specific file extensions present in the current workspace.
	 * 
	 * @param fileExtension  Must not be {@code null} or empty.
	 * @return  the URIs of the files that match the specified criteria.
	 * @throws IllegalArgumentException  if {@code fileExtension} is {@code null} or empty.
	 */
	def findFilesInWorkspace(String fileExtension) {
		findFilesInContainer(workspaceRoot, fileExtension)
	}
	
	private def Iterable<IFile> findFilesInContainer(IContainer container, String fileExtension) {
		val result = new HashSet<IFile>()
		for(member : container.members) {
			if(member instanceof IContainer) {
				result.addAll(findFilesInContainer(member, fileExtension))
			} else if(member instanceof IFile && (member as IFile).fileExtension == fileExtension) {
				result.add(member as IFile)
			}
		}
		return result
	}
	
	def cleanAndBuild(IProject it) {
		build(IncrementalProjectBuilder.CLEAN_BUILD, null);
	}
	
	def buildFull(IProject it) {
		build(IncrementalProjectBuilder.FULL_BUILD, null);
	}
	
	def buildIncremental(IProject it) {
		build(IncrementalProjectBuilder.INCREMENTAL_BUILD, null);
	}
	
	/**
	 * Retrieves all extensions for the specified extension point from
	 * the platform's extension registry.
	 * 
	 * @return list of newly created instances for all found extensions
	 */
	def getExtensions(String extensionPointID) {
		getExtensions(extensionPointID, Object)
	}
	
	/**
	 * Retrieves all extensions for the specified extension point from
	 * the platform's extension registry.
	 * 
	 * @return list of newly created instances for all found extensions
	 *   that match the specified type.
	 */
	def <T> getExtensions(String extensionPointID, Class<T> expectedType) {
		Platform.extensionRegistry
			?.getConfigurationElementsFor(extensionPointID)
			?.map[createExecutableExtension]
			?.filter(expectedType)
	}
	
	/**
	 * Creates a new instance of the extension type given by the 'class'
	 * attribute of the specified configuration element.
	 * 
	 * @return a newly created instance of the extension
	 */
	def createExecutableExtension(IConfigurationElement elem) {
		try {
			elem.createExecutableExtension("class")
		} catch (CoreException e) {
			e.printStackTrace
			return null
		}
	}
	
	/**
	 * Constructs a new path from the given string path. The string path must
	 * represent a valid file system path on the local file system. The path is
	 * canonicalized and double slashes are removed except at the beginning (to
	 * handle UNC paths). All forward slashes ('/') are treated as segment
	 * delimiters, and any segment and device delimiters for the local file
	 * system are also respected (such as colon (':') and backslash ('\') on
	 * some file systems). This constructor should be used if the string path
	 * if for the local file system.
	 * 
	 * @return A new path from the given string path
	 */
	def toPath(String stringPath) {
		new Path(stringPath)
	}
	
	/**
	 * Creates or overwrites the specified file and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param file    The file handle that will be created and written to
	 * @param content An InputStream that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The original file handle
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IFile file, InputStream content) throws CoreException, OperationCanceledException {
		if (!file.parent.exists) {
			file.parent.create
		}
		val stream = content?: newEmptyInputStream
		try {
			if (file.exists) {
				file.setContents(stream, true, true, null)
			}
			else {
				file.create(stream, true, null)
			}
		}
		finally {
			stream.close
		}
		return file
	}
	
	/**
	 * Creates or overwrites the specified file and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param file    The file handle that will be created and written to
	 * @param content A String that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The original file handle
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IFile file, String content) {
		val stream = content.toInputStream(file.charsetObject)
		return file.createFile(stream)
	}
	
	/**
	 * Creates or overwrites the specified file and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param file    The file handle that will be created and written to
	 * @param content A CharSequence that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The original file handle
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IFile file, CharSequence content) {
		file.createFile(content?.toString)
	}
	
	/**
	 * Creates or overwrites the specified file and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param file    The file handle that will be created and written to
	 * @param content A String that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The original file handle
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IFile file, StringBuilder content) {
		file.createFile(content?.toString)
	}
	
	/**
	 * Creates or overwrites the specified file. Its contents will be empty.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param file The file handle that will be created and written to
	 * 
	 * @return The original file handle
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IFile file) {
		file.createFile(null as InputStream)
	}

	/**
	 * Returns a path equivalent to {@code path}, but relative to the workspace
	 * root if possible.
	 * <p>
	 * The path is only made relative if the workspace root and {@code path}
	 * have the same device and a non-zero length common prefix. If they have
	 * different devices, or no common prefix, then {@code path} is simply
	 * returned. If the path is successfully made relative, then appending the
	 * returned path to the workspace root's {@linkplain
	 * org.eclipse.core.resources.IWorkspaceRoot#getLocation() location} will
	 * always produce a path equal to {@code path}.
	 * 
	 * @return A path relative to the workspace root, or {@code path} if it
	 *         could not be made relative
	 */
	def makeWorkspaceRelative(IPath path) {
		if (path.isAbsolute && workspaceRoot.location.isPrefixOf(path)) {
			path.makeRelativeTo(workspaceRoot.location)
		}
		else {
			path
		}
	}
	
	/**
	 * Retrieves the file for the specified path from the workspace.
	 * The {@code path} can be relative to the workspace root or absolute with
	 * a prefix equal to the workspace root's {@linkplain
	 * org.eclipse.core.resources.IWorkspaceRoot#getLocation() location}.
	 */
	def getFile(IPath path) {
		workspaceRoot.getFile(path.makeWorkspaceRelative)
	}
	
	/**
	 * Returns whether this resource exists in the workspace.
	 */
	def exists(IPath path) {
		path.file.exists
	}
	
	/**
	 * Creates or overwrites a file at the specified path and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param path    The location where the file will be created
	 * @param content An InputStream that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The file handle of the file
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IPath path, InputStream content) {
		path.file.createFile(content)
	}
	
	/**
	 * Creates or overwrites a file at the specified path and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param path    The location where the file will be created
	 * @param content A String that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The file handle of the file
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IPath path, String content) {
		path.file.createFile(content)
	}
	
	/**
	 * Creates or overwrites a file at the specified path and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param path    The location where the file will be created
	 * @param content A CharSequence that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The file handle of the file
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IPath path, CharSequence content) {
		path.file.createFile(content)
	}
	
	/**
	 * Creates or overwrites a file at the specified path and sets its contents.
	 * Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param path    The location where the file will be created
	 * @param content A String that will be written to the file;
	 *                may be empty or {@code null}
	 * 
	 * @return The file handle of the file
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IPath path, StringBuilder content) {
		path.file.createFile(content)
	}
	
	/**
	 * Creates or overwrites a file at the specified path. Its contents will be
	 * empty. Recursively creates its parents as well, if not existent yet.
	 * 
	 * @param path    The location where the file will be created
	 * 
	 * @return The file handle of the file
	 * 
	 * @throws CoreException if the file (or any of its parents) cannot be
	 *                       created or written to
	 * @throws OperationCanceledException if the operation is canceled
	 */
	def createFile(IPath path) {
		path.file.createFile()
	}
	
	/**
	 * Encodes the given {@code content} String using the provided {@code
	 * charset} and creates an InputStream.
	 * 
	 * @param content If the {@code content} is {@code null}, an empty stream
	 *                will be returned.
	 * @param charset If invalid or {@code null}, the {@link
	 *                Charset#defaultCharset() defaultCharset()} is used.
	 * 
	 * @return An InputStream from {@code content}, encoded with {@code
	 *         charset}. Never {@code null}.
	 */
	def InputStream toInputStream(String content, Charset charset) {
		if (content.nullOrEmpty) {
			return newEmptyInputStream
		}
		val nullsafeCharset = charset?: Charset.defaultCharset
		val bytes = content.getBytes(nullsafeCharset)
		return new ByteArrayInputStream(bytes)
	}
	
	/**
	 * Encodes the given {@code content} String using the provided {@code
	 * charsetName} and creates an InputStream.
	 * 
	 * @param content     If the {@code content} is {@code null}, an empty
	 *                    stream will be returned.
	 * @param charsetName If invalid or {@code null}, the {@link
	 *                    Charset#defaultCharset() defaultCharset()} is used.
	 * 
	 * @return An InputStream from {@code content}, encoded with {@code
	 *         charsetName}. Never {@code null}.
	 */
	def InputStream toInputStream(String content, String charsetName) {
		val charset = try Charset.forName(charsetName) catch (Exception e) null
		content.toInputStream(charset)
	}
	
	/**
	 * Encodes the given {@code content} String using the {@link
	 * Charset#defaultCharset() defaultCharset()} and creates an InputStream.
	 * 
	 * @param content If the {@code content} is {@code null}, an empty stream
	 *                will be returned.
	 * 
	 * @return An InputStream from {@code content}. Never {@code null}.
	 */
	def InputStream toInputStream(String content) {
		content.toInputStream(null as Charset)
	}
	
	/**
	 * Returns a new empty InputStream.
	 */
	def InputStream newEmptyInputStream() {
		new ByteArrayInputStream(newByteArrayOfSize(0))
	}
	
	/**
	 * Returns a charset object to be used when decoding the contents of this
	 * file into characters. Returns the the {@link Charset#defaultCharset()
	 * defaultCharset()} if the charset cannot be determined (e.g. if the file
	 * does not exist). Never {@code null}. 
	 */
	def Charset getCharsetObject(IFile file) {
		try {
			return Charset.forName(file.charset)
		}
		catch (Exception e) {
			e.printStackTrace
			return Charset.defaultCharset
		}
	}
	
}

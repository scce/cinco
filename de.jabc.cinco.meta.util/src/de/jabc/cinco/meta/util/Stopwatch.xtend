/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.util

import java.util.LinkedList
import java.util.NoSuchElementException
import org.eclipse.xtend.lib.annotations.Accessors

import static de.jabc.cinco.meta.util.Stopwatch.TimeUnit.*
import static de.jabc.cinco.meta.util.Table.Alignment.*
import static java.lang.String.format
import static java.util.Locale.ENGLISH

/**
 * A small utility to measure the runtime of procedures.
 * To use the stopwatch write code like this:
 * <pre>
 * var sw = new Stopwatch(MINUTE);<br>
 * sw.start("Task A");
 * doTaskA();<br>
 * sw.start("Sleep for 5 min");
 * Thread.sleep(5 * 60 * 1000);<br>
 * sw.start("Task B with a short break");
 * doTaskB_1();
 * sw.stop();
 * Thread.sleep(1000);
 * sw.resume();
 * doTaskB_2();<br>
 * sw.stop();
 * sw.printTable();
 * </pre>
 * The printed table would look like this:
 * <pre>
 * ┌───┬─────────────────────────┬──────────┬────────────┐
 * │No.│Label                    │Time [min]│Relative [%]│
 * ├───┼─────────────────────────┼──────────┼────────────┤
 * │  0│Task A                   │         5│       25.01│
 * │  1│Sleep for 5 min          │         5│       25.03│
 * │  2│Task B with a short break│        10│       49.96│
 * ├───┼─────────────────────────┼──────────┼────────────┤
 * │   │Total                    │        20│      100.00│
 * └───┴─────────────────────────┴──────────┴────────────┘
 * </pre>
 */
class Stopwatch {
	
	/**
	 * The displayed unit of time.
	 * (The precision of the stopwatch is not affected.
	 * The stopwatch always measures durations in nanoseconds.)
	 */
	@Accessors(#[PUBLIC_GETTER, PUBLIC_SETTER])
	var TimeUnit unit
	
	/**
	 * Whether the stopwatch is currently running or not.
	 */
	@Accessors(PUBLIC_GETTER)
	var boolean running
	
	/**
	 * The point in time when the current measurement was started
	 * (in nanoseconds since midnight January 1, 1970).
	 */
	var long startTime
	
	/**
	 * The duration of the completed measurements in nanoseconds.
	 */
	val LinkedList<Long> times
	
	/**
	 * The lables for each measurement.
	 */
	val LinkedList<String> labels
	
	/**
	 * Whether or not to print the labels and times as the stopwatch is used.
	 */
	val boolean debug
	
	/**
	 * Creates a new stopwatch. Initially the stopwatch is stopped
	 * and uses milliseconds for displaying results.
	 * @see #Stopwatch(TimeUnit, boolean) new Stopwatch(TimeUnit, boolean)
	 */
	new () {
		this(MILLISECOND, false)
	}
	
	/**
	 * Creates a new stopwatch. Initially the stopwatch is stopped
	 * and uses milliseconds for displaying results.
	 * @param debug Whether or not to print the labels and times as the
	 *              stopwatch is used.
	 * @see #Stopwatch(TimeUnit, boolean) new Stopwatch(TimeUnit, boolean)
	 */
	new (boolean debug) {
		this(MILLISECOND, debug)
	}
	
	/**
	 * Creates a new stopwatch. Initially the stopwatch is stopped.
	 * @param unit The displayed unit of time.
	 *             (The precision of the stopwatch is not affected.
	 *             The stopwatch always measures durations in nanoseconds.)
	 * @see #Stopwatch(TimeUnit, boolean) new Stopwatch(TimeUnit, boolean)
	 */
	new (TimeUnit unit) {
		this(unit, false)
	}
	
	/**
	 * Creates a new stopwatch. Initially the stopwatch is stopped.
	 * @param unit  The displayed unit of time.
	 *              (The precision of the stopwatch is not affected.
	 *              The stopwatch always measures durations in nanoseconds.)
	 * @param debug Whether or not to print the labels and times as the
	 *              stopwatch is used.
	 */
	new (TimeUnit unit, boolean debug) {
		this.unit      = unit
		this.running   = false
		this.startTime = 0
		this.times     = newLinkedList
		this.labels    = newLinkedList
		this.debug     = debug
	}
	
	/**
	 * Start the stopwatch for measuring a duration.
	 * <p>
	 * If the stopwatch is already running, the measurement for the current
	 * duration will be stopped before starting this new one.
	 * <p>
	 * You can only run one timer at a time. For multiple concurrent timers,
	 * use separate stopwatch instances.
	 */
	def void start(String label) {
		if (running) {
			val endTime = System.nanoTime
			times.add(endTime - startTime)
			startTime = endTime
			if (debug) {
				println('''Stop: «labels.last»: «times.last.convert» «unitSymbol»''')
			}
		}
		else {
			running = true
			startTime = System.nanoTime
		}
		labels.add(label)
		if (debug) {
			println('''Start: «labels.last»''')
		}
	}
	
	/**
	 * Stop the stopwatch and the current measurement.
	 * Afterwards, you can either {@linkplain #resume() resume} the last
	 * measurement, or {@linkplain #start() start} a new one.  
	 */
	def void stop() {
		if (running) {
			val endTime = System.nanoTime
			times.add(endTime - startTime)
			running = false
			if (debug) {
				println('''Stop: «labels.last»: «times.last.convert» «unitSymbol»''')
			}
		}
	}
	
	/**
	 * Resumes the last stopped measurement. Does nothing, if the stopwatch is
	 * already running, or there is no measurement to resume.
	 */
	def void resume() {
		if (!running) {
			try {
				val lastDuration = times.removeLast
				running = true
				val currentTime = System.nanoTime
				startTime = currentTime - lastDuration
				if (debug) {
					println('''Resume: «labels.last»''')
				}
			}
			catch (NoSuchElementException e) {
				System.err.println("Stopwatch: No measurement to resume")
			}
		}
	}
	
	/**
	 * Stops the stopwatch and removes all measurements.
	 */
	def void reset() {
		running = false
		times.clear
		labels.clear
	}
	
	/**
	 * Returns the sum of all completed measurements in the current
	 * {@linkplain #setUnit(TimeUnit) unit of time}.
	 */
	def getTotal() {
		times.fold(0L) [ a, b | a + b ].convert
	}
	
	/**
	 * Returns the measurement at index {@code i} in the current
	 * {@linkplain #setUnit(TimeUnit) unit of time}.
	 */
	def getTime(int i) {
		times.get(i).convert
	}
	
	/**
	 * Returns a copy of all measurements in the current
	 * {@linkplain #setUnit(TimeUnit) unit of time}.
	 */
	def getTimes() {
		stop()
		(0 ..< times.size).map[ i | getLabel(i) -> getTime(i) ].toList
	}
	
	/**
	 * Returns the label of the measurement at index {@code i}.
	 */
	def getLabel(int i) {
		labels.get(i)
	}
	
	/**
	 * Returns a copy of all labels.
	 */
	def getLabels() {
		val list = newArrayList
		list.addAll(labels)
		return list
	}
	
	/**
	 * Returns the SI symbol for the current
	 * {@linkplain #setUnit(TimeUnit) unit of time}.
	 */
	def getUnitSymbol() {
		unit.unitSymbol
	}
	
	/**
	 * {@linkplain #stop() Stops} the stopwatch and returns a table of the results
	 * in the current {@linkplain #setUnit(TimeUnit) unit of time} as a String.
	 * <p>
	 * <b>Example:</b>
	 * <pre>
	 * ┌───┬─────────────────────────┬──────────┬────────────┐
	 * │No.│Label                    │Time [min]│Relative [%]│
	 * ├───┼─────────────────────────┼──────────┼────────────┤
	 * │  0│Task A                   │         5│       25.01│
	 * │  1│Sleep for 5 min          │         5│       25.03│
	 * │  2│Task B with a short break│        10│       49.96│
	 * ├───┼─────────────────────────┼──────────┼────────────┤
	 * │   │Total                    │        20│      100.00│
	 * └───┴─────────────────────────┴──────────┴────────────┘
	 * </pre>
	 * @see #printTable() printTable()
	 * @see Table
	 */
	def getTable() {
		stop
		val totalNanoSeconds = times.fold(0L) [ a, b | a + b ]
		val table = new Table("No.", "Label", '''Time [«unitSymbol»]''', "Relative [%]")
			.setAlignment(RIGHT, LEFT, RIGHT, RIGHT)
		for (i: 0 ..< times.size) {
			val relative = Math.round(10000D * times.get(i) / totalNanoSeconds) / 100D
			table.addRow(i, getLabel(i), getTime(i), format(ENGLISH, "%.2f", relative))
		}
		table
			.addSeparator
			.addRow("", "Total", totalNanoSeconds.convert, format(ENGLISH, "%.2f", 100D))
			.toString
	}
	
	/**
	 * {@linkplain #stop() Stops} the stopwatch and prints a table with the results
	 * in the current {@linkplain #setUnit(TimeUnit) unit of time}.
	 * <p>
	 * <b>Example:</b>
	 * <pre>
	 * ┌───┬─────────────────────────┬──────────┬────────────┐
	 * │No.│Label                    │Time [min]│Relative [%]│
	 * ├───┼─────────────────────────┼──────────┼────────────┤
	 * │  0│Task A                   │         5│       25.01│
	 * │  1│Sleep for 5 min          │         5│       25.03│
	 * │  2│Task B with a short break│        10│       49.96│
	 * ├───┼─────────────────────────┼──────────┼────────────┤
	 * │   │Total                    │        20│      100.00│
	 * └───┴─────────────────────────┴──────────┴────────────┘
	 * </pre>
	 * @see #getTable() getTable()
	 * @see Table
	 */
	def printTable() {
		println(table)
	}
	
	/**
	 * Converts the given duration {@code time} from nanoseconds to the
	 * stopwatch's {@linkplain #unit time unit}. 
	 */
	def private convert(long time) {
		time.convert(NANOSECOND, unit)
	}
	
	/**
	 * Converts the given duration {@code time} from the {@code source} time
	 * unit to the {@code target} time unit.
	 */
	def static convert(long time, TimeUnit source, TimeUnit target) {
		if (source === target) {
			return time
		}
		val toNanoSeconds = switch source {
			case NANOSECOND:        1D
			case MICROSECOND:    1000D
			case MILLISECOND: 1000000D
			case SECOND:   1000000000D
			case MINUTE:  60000000000D
			case HOUR:  3600000000000D
			case DAY:  86400000000000D
		}
		val toTarget = switch target {
			case NANOSECOND:        1D
			case MICROSECOND:    1000D
			case MILLISECOND: 1000000D
			case SECOND:   1000000000D
			case MINUTE:  60000000000D
			case HOUR:  3600000000000D
			case DAY:  86400000000000D
		}
		return Math.round(time * toNanoSeconds / toTarget)
	}
	
	/**
	 * Returns the SI symbol of the provided {@link TimeUnit}.
	 */
	def static getUnitSymbol(TimeUnit unit) {
		switch unit {
			case NANOSECOND:  "ns"
			case MICROSECOND: "µs"
			case MILLISECOND: "ms"
			case SECOND:      "s"
			case MINUTE:      "min"
			case HOUR:        "h"
			case DAY:         "d"
		}
	}
	
	/**
	 * Enum of time units from nanoseconds to days.
	 */
	enum TimeUnit {
		NANOSECOND,
		MICROSECOND,
		MILLISECOND,
		SECOND,
		MINUTE,
		HOUR,
		DAY
	}
	
}

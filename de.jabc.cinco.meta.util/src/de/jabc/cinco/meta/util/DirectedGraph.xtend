/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.util

import java.util.EmptyStackException
import java.util.HashSet
import java.util.LinkedList
import java.util.Set
import java.util.Stack
import org.eclipse.xtend.lib.annotations.Accessors

/**
 * A directed graph, where each {@link DirectedGraph.Node Node} has references
 * to both its parents and its children. Graphs may be disconnected, empty or
 * cyclic.
 * <p>
 * Nodes will be compared using their {@link DirectedGraph.Node#getContent()
 * contents}. You cannot add more than one node with the same content. Instead,
 * the node with the same content will be modified.
 * <p>
 * To create a graph could use code like this:
 * <pre>
 * var graph = new DirectedGraph&ltInteger&gt();
 * graph
 *     .addNode(0)
 *     .addNode(1)
 *         .addParents(0)
 *         .addChildren(2, 3)
 *     .addNode(4)
 *         .addChildren(5, 6, 7)
 *     .addNode(8)
 *         .addParents(2, 7)
 *     .getGraph()
 *         .print()
 *     .getNode(8)
 *         .getAncestors()
 *         .stream()
 *         .forEach(node -> node.print());
 * </pre>
 * Result:
 * <pre>
 * DirectedGraph {
 *     0 -> {1},
 *     1 -> {2, 3},
 *     2 -> {8},
 *     3 -> {},
 *     4 -> {5, 6, 7},
 *     5 -> {},
 *     6 -> {},
 *     7 -> {8},
 *     8 -> {}
 * }
 * 7 -> {8}
 * 2 -> {8}
 * 4 -> {5, 6, 7}
 * 1 -> {2, 3}
 * 0 -> {1}
 * </pre>
 */
@Accessors(PUBLIC_GETTER)
class DirectedGraph<T> {
	
	/**
	 * A set of all nodes contained by this graph.
	 */
	val Set<Node<T>> nodes
	
	/**
	 * A comparator for identifying if the {@link Node#content content} of two
	 * {@link Node}s are equal. The default is based on Xtend's {@link
	 * org.eclipse.xtext.xbase.lib.ObjectExtensions#operator_equals(Object, Object)
	 * ==} operator.
	 */
	val (T, T) => boolean contentComparator
	
	/**
	 * Creates a new empty directed graph.
	 */
	new () {
		this[ l, r | l == r ]
	}
	
	/**
	 * Creates a new empty directed graph.
	 * @param contentComparator A comparator for identifying if the {@link Node#content
	 *                          content} of two {@link Node}s are equal.
	 */
	new ((T, T) => boolean contentComparator) {
		this.nodes = new HashSet
		this.contentComparator = contentComparator
	}
	
	/**
	 * Returns the node, that contains {@code content}.
	 * Returns {@code null}, if there is no such node.
	 */
	def getNode(T content) {
		nodes.findFirst[ node | contentComparator.apply(node.content, content) ]
	}
	
	/**
	 * Returns an existing node, that contains {@code content}.
	 * If there is no such node, a new node will be created and added to the
	 * graph.
	 */
	def addNode(T content) {
		var node = getNode(content)
		if (node === null) {
			node = new Node(content, this)
			nodes.add(node)
		}
		return node
	}
	
	/**
	 * Returns an {@link Iterable} of existing and newly created nodes, that
	 * contain {@code contents}. New node will only be created, if there is no
	 * existing node for that content.
	 */
	def addNodes(Iterable<T> contents) {
		contents.map[addNode]
	}
	
	/**
	 * Returns an {@link Iterable} of existing and newly created nodes, that
	 * contain {@code contents}. New node will only be created, if there is no
	 * existing node for that content.
	 */
	def addNodes(T ... contents) {
		contents.map[addNode]
	}
	
	/**
	 * Whether or not this graph contains a node with this {@code content}.
	 */
	def contains(T content) {
		nodes.exists[ node | contentComparator.apply(node.content, content) ]
	}
	
	/**
	 * Whether or not this graph contains nodes with all {@code contents}.
	 */
	def containsAll(T ... contents) {
		contents.forall[contains]
	}
	
	/**
	 * Returns a list of all of this graph's nodes' content.
	 */
	def getContents() {
		nodes.map[content].toList
	}
	
	/**
	 * Returns a list of all roots of this graph.
	 */
	def getRoots() {
		nodes.filter[isRoot].toList
	}
	
	/**
	 * Returns a list of all leaves of this graph.
	 */
	def getLeaves() {
		nodes.filter[isLeaf].toList
	}
	
	/**
	 * Returns a list of independent subgraphs of this graph.
	 */
	def getSubgraphs() {
		val queue = new LinkedList(nodes)
		val subgraphs = new LinkedList<DirectedGraph<T>>
		while (!queue.empty) {
			val node = queue.removeFirst
			val subgraph = node.subgraph
			subgraphs.add(subgraph)
			queue.removeIf[subgraph.contains(content)]
		}
		return subgraphs
	}
	
	/**
	 * Returns a list of all nodes contained by this graph, sorted
	 * topologically (roots to leaves).
	 * @throws GraphContainsCyclesException The elements cannot be sorted
	 *                                      topologically, because the graph
	 *                                      contains cycles.
	 */
	def getTopSortedNodes() throws GraphContainsCyclesException {
		if (nodes.empty) {
			return #[]
		}
		if (isCyclic) {
			throw new GraphContainsCyclesException
		}
		val sorted = roots
		while (sorted.size < size) {
			nodes
				.reject [ node | sorted.contains(node) ]
				.filter [ node | node.parents.forall [ parent | sorted.contains(parent) ] ]
				.forEach [ node | sorted.add(node) ]
		}
		return sorted
	}
	
	/**
	 * Returns a list of the contents of all nodes contained by this graph,
	 * sorted topologically (roots to leaves).
	 * @throws GraphContainsCyclesException The elements cannot be sorted
	 *                                      topologically, because the graph
	 *                                      contains cycles.
	 */
	def getTopSortedContents() throws GraphContainsCyclesException {
		topSortedNodes.map[content]
	}
	
	/**
	 * Returns the number of nodes in this graph.
	 */
	def size() {
		nodes.size
	}
	
	/**
	 * Whether or not this graph contains any nodes.
	 */
	def isEmpty() {
		nodes.empty
	}
	
	/**
	 * Whether or not this graph's nodes are all connected to another, such
	 * that there are no independent subtrees.
	 */
	def isConnected() {
		subgraphs.size <= 1
	}
	
	/**
	 * Whether or not this graph contains cycles.
	 */
	def isCyclic() {
		val visited = new HashSet<Node<T>>
		val path = new Stack<Node<T>>
		for (node: nodes) {
			if (node.isCyclic(visited, path)) {
				return true
			}
		}
		return false
	}
	
	def private boolean isCyclic(Node<T> node, Set<Node<T>> visited, Stack<Node<T>> path) {
		if (path.contains(node)) {
			// The current node is already part of the path. Cycle detected!
			return true
		}
		if (visited.add(node)) {
			// Node was never visited.
			path.push(node)
			for (child: node.children) {
				if (child.isCyclic(visited, path)) {
					return true
				}
			}
			path.pop
			return false
		}
		else {
			// Node was already visited. No need to check again.
			return false
		}
	}
	
	/**
	 * Whether or not this graph is a tree.
	 * <p>
	 * Trees are connected graphs, that have exactly one root node. Furthermore
	 * there each node (except the root) must have exactly one parent.
	 */
	def isTree() {
		val roots = roots
		val root = roots.head
		return roots.size == 1 && 
			nodes.forall[ node | node.parents.size == 1 || node === root ] &&
			isConnected
	}
	
	/**
	 * Whether or not this graph is a forest.
	 * <p>
	 * A forest's subgraphs are all trees.
	 * 
	 */
	def isForest() {
		subgraphs.forall[isTree]
	}
	
	/**
	 * Returns a String representation of this graph.
	 */
	override toString() {
		toString[ content | content.toString ]
	}
	
	/**
	 * Returns a String representation of this graph.
	 * @param nameMap A function, that returns a name for an element.
	 */
	def String toString((T) => CharSequence nameMap) {
		val nodeStrings = nodes.map[ node | node.toString(nameMap) ].sort
		return '''«class.simpleName» {«nodeStrings.join("\n\t", ",\n\t", "\n") [it]»}'''
	}
	
	/**
	 * Prints a representation of this graph to the console and returns the
	 * graph itself. 
	 */
	def print() {
		println(toString)
		return this
	}
	
	/**
	 * Prints a representation of this graph to the console and returns the
	 * graph itself. 
	 */
	def print((T) => CharSequence nameMap) {
		println(toString(nameMap))
		return this
	}
	
	/**
	 * Prints a tree-representation of this graph to the console and returns
	 * the graph itself.
	 * @throws GraphIsNotForestException The graph is not a tree or a forest.
	 */
	def printTree() {
		printTree[ content | content.toString ]
	}
	
	/**
	 * Prints a tree-representation of this graph to the console and returns
	 * the graph itself.
	 * @param nameMap A function, that returns a name for an element.
	 * @throws GraphIsNotForestException The graph is not a tree or a forest.
	 */
	def printTree((T) => CharSequence nameMap) {
		if (!isForest) {
			throw new GraphIsNotForestException
		}
		for (root: roots) {
			root.printTreeRecursive(new Stack<Boolean>, nameMap)
		}
		return this
	}
	
	def private void printTreeRecursive(Node<T> node, Stack<Boolean> stack, (T) => CharSequence nameMap) {
		val isLast = try stack.pop catch (EmptyStackException e) { null }
		val isRoot = isLast === null
		if (isRoot) {
			println(nameMap.apply(node.content))
		}
		else {
			println('''«stack.map[ if (it) "    " else "│   " ].join»«if (isLast) "└" else "├"»─► «nameMap.apply(node.content)»''')
			stack.push(isLast)
		}
		val lastIndex = node.children.size - 1
		for (it: node.children.indexed) {
			val index = key
			val child = value
			stack.push(index == lastIndex)
			child.printTreeRecursive(stack, nameMap)
			stack.pop
		}
	}
	
	
	
	/**
	 * The element class for {@link DirectedGraph}s.
	 */
	static class Node<T> {
		
		/**
		 * The content of this node.
		 */
		@Accessors(PUBLIC_GETTER)
		val T content
		
		/**
		 * The graph that contains this node.
		 */
		@Accessors(PUBLIC_GETTER)
		val DirectedGraph<T> graph
		
		/**
		 * The set of parent nodes of this node.
		 */
		val Set<Node<T>> parents
		
		/**
		 * The set of child nodes of this node.
		 */
		val Set<Node<T>> children
		
		private new (T content, DirectedGraph<T> graph) {
			this.content = content
			this.graph = graph
			this.parents = new HashSet
			this.children = new HashSet
		}
		
		/**
		 * This is a convenience method for {@link DirectedGraph#getNode(T)
		 * DirectedGraph.getNode(T)}.
		 * <p>
		 * Returns an existing node of this node's graph, that contains
		 * {@code content}. If there is no such node, {@code null} is returned.
		 */
		def getNode(T content) {
			graph.getNode(content)
		}
		
		/**
		 * This is a convenience method for {@link DirectedGraph#addNode(T)
		 * DirectedGraph.addNode(T)}.
		 * <p>
		 * Returns an existing node of this node's graph, that contains
		 * {@code content}. If there is no such node, a new node will be
		 * created and added to the graph.
		 */
		def addNode(T content) {
			graph.addNode(content)
		}
		
		/**
		 * This is a convenience method for {@link DirectedGraph#addNodes(Iterable)
		 * DirectedGraph.addNodes(Iterable&lt;T&gt;)}.
		 * <p>
		 * Returns an {@link Iterable} of existing and newly created nodes, that
		 * contain {@code contents}. New node will only be created, if there is no
		 * existing node for that content.
		 */
		def addNodes(Iterable<T> contents) {
			graph.addNodes(contents)
		}
		
		/**
		 * This is a convenience method for {@link DirectedGraph#addNodes(T[])
		 * DirectedGraph.addNodes(T ...)}.
		 * <p>
		 * Returns an {@link Iterable} of existing and newly created nodes, that
		 * contain {@code contents}. New node will only be created, if there is no
		 * existing node for that content.
		 */
		def addNodes(T ... contents) {
			graph.addNodes(contents)
		}
		
		/**
		 * Returns a list of all of this node's parents.
		 */
		def getParents() {
			parents.toList
		}
		
		/**
		 * Adds a parent to this node. If there already is a node with equal
		 * contents in the graph, that node will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addParent(T parent) {
			val parentNode = graph.addNode(parent)
			this.parents.add(parentNode)
			parentNode.children.add(this)
			return this
		}
		
		/**
		 * Adds parents to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addParents(Iterable<T> parents) {
			for (parent: parents) {
				addParent(parent)
			}
			return this
		}
		
		/**
		 * Adds parents to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addParents(T ... parents) {
			for (parent: parents) {
				addParent(parent)
			}
			return this
		}
		
		/**
		 * Returns a list of all of this node's children.
		 */
		def getChildren() {
			children.toList
		}
		
		/**
		 * Adds a child to this node. If there already is a node with equal
		 * contents in the graph, that node will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addChild(T child) {
			val childNode = graph.addNode(child)
			this.children.add(childNode)
			childNode.parents.add(this)
			return this
		}
		
		/**
		 * Adds children to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addChildren(Iterable<T> children) {
			for (child: children) {
				addChild(child)
			}
			return this
		}
		
		/**
		 * Adds children to this node. If there already are nodes with equal
		 * contents in the graph, those nodes will be linked. Missing nodes
		 * will be created. Returns this node itself.
		 */
		def addChildren(T ... children) {
			for (child: children) {
				addChild(child)
			}
			return this
		}
		
		/**
		 * Returns a list of all of this node's ancestors including itself.
		 */
		def getAncestorsAndSelf() {
			val ancestorNodes = ancestors
			if (!ancestorNodes.contains(this)) {
				ancestorNodes.add(0, this)
			}
			return ancestorNodes
		}
		
		/**
		 * Returns a list of all of this node's ancestors (excluding itself).
		 */
		def getAncestors() {
			val ancestorNodes = new HashSet<Node<T>>
			getAncestorsRecursive(ancestorNodes)
			return ancestorNodes.toList
		}
		
		private def void getAncestorsRecursive(Set<Node<T>> ancestorNodes) {
			for (parent: parents) {
				if (ancestorNodes.add(parent)) {
					parent.getAncestorsRecursive(ancestorNodes)
				}
			}
		}
		
		/**
		 * Returns a list of all of this node's descendants including itself.
		 */
		def getDescendantsAndSelf() {
			val descendantNodes = descendants
			if (!descendantNodes.contains(this)) {
				descendantNodes.add(0, this)
			}
			return descendantNodes
		}
		
		/**
		 * Returns a list of all of this node's descendants (excluding itself).
		 */
		def getDescendants() {
			val descendantNodes = new HashSet<Node<T>>
			getDescendantsRecursive(descendantNodes)
			return descendantNodes.toList
		}
		
		private def void getDescendantsRecursive(Set<Node<T>> descendantNodes) {
			for (child: children) {
				if (descendantNodes.add(child)) {
					child.getDescendantsRecursive(descendantNodes)
				}
			}
		}
		
		/**
		 * Returns a subgraph of this node's graph, that contains this node.
		 */
		def getSubgraph() {
			val nodes = new HashSet<Node<T>>
			getSubgraphRecursive(nodes)
			val subgraph = new DirectedGraph<T>
			for (node: nodes) {
				subgraph
					.addNode(node.content)
					.addParents(node.parents.map[content])
					.addChildren(node.children.map[content])
			}
			return subgraph
		}
		
		private def void getSubgraphRecursive(Set<Node<T>> nodes) {
			if (nodes.add(this)) {
				for (parent: parents) {
					parent.getSubgraphRecursive(nodes)
				}
				for (child: children) {
					child.getSubgraphRecursive(nodes)
				}
			}
		}
		
		/**
		 * Whether this node is a root node.
		 */
		def isRoot() {
			parents.empty
		}
		
		/**
		 * Whether this node is a leaf node.
		 */
		def isLeaf() {
			children.empty
		}
		
		override equals(Object other) {
			switch other {
				Node<T>: this.content.equals(other.content)
				default: false
			}
		}
		
		/**
		 * Returns a String representation of this node.
		 */
		override toString() {
			toString[ content | content.toString ]
		}
		
		/**
		 * Returns a String representation of this node.
		 * @param nameMap A function, that returns a name for an element.
		 */
		def String toString((T) => CharSequence nameMap) {
			val childrenStrings = children.map[ child | nameMap.apply(child.content).toString ].sort
			return '''«nameMap.apply(content)» -> {«childrenStrings.join(", ")»}'''
		}
		
		/**
		 * Prints a representation of this node to the console and returns the
		 * node itself. 
		 */
		def print() {
			println(toString)
			return this
		}
		
		/**
		 * Prints a representation of this node to the console and returns the
		 * node itself. 
		 */
		def print((T) => CharSequence nameMap) {
			println(toString(nameMap))
			return this
		}
		
	}
	
	
	
	/**
	 * Indicates that a method has encountered an error, because the graph
	 * contains one or more cycles.
	 */
	static class GraphContainsCyclesException extends RuntimeException {
		
		new () {
			super()
		}
		
		new (String message) {
			super(message)
		}
		
	}
	
	/**
	 * Indicates that a method has encountered an error, because the graph
	 * is not a forest.
	 */
	static class GraphIsNotForestException extends RuntimeException {
		
		new () {
			super()
		}
		
		new (String message) {
			super(message)
		}
		
	}
	
}

package de.jabc.cinco.meta.plugin.spreadsheet;

import java.util.List;

import org.eclipse.core.resources.IProject;

import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin;
import mgl.Annotation;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class SpreadSheetMetaPLugin implements IMGLMetaPlugin {
	
	@Override
	public void executeMGLMetaPlugin(List<Annotation> mglAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		System.out.println("RUNNING SPREADSHEET METAPLUGIN");
		for (var mglModel: allMGLs) {
			for (var graphModel: mglModel.getGraphModels()) {
				new CreateSpreadSheetPlugin().execute(mglModel, graphModel);
			}
		}
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.gratext.build;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.mwe2.language.Mwe2StandaloneSetup;
import org.eclipse.emf.mwe2.launch.runtime.Mwe2Launcher;
import org.eclipse.emf.mwe2.launch.runtime.Mwe2Runner;
import org.eclipse.osgi.internal.loader.EquinoxClassLoader;
import com.google.common.collect.Lists;
import com.google.inject.Injector;

import de.jabc.cinco.meta.core.utils.CincoProperties;

public class HeadlessGratextMwe2Job extends GratextMwe2Job {




	

	public HeadlessGratextMwe2Job(IProject project, IFile mwe2File) {
		super(project, mwe2File);
		// TODO Auto-generated constructor stub
	}
	boolean success;
	private Object jobStatus;
	
	@Override
	public void onTerminated(IProcess process) {
		try {
			success = (process.getExitValue() == 0);
		} catch (DebugException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onQuit() {
		jobStatus = success
			? Status.OK_STATUS
			: new Status(Status.ERROR, getName(), "Mwe2 workflow failed.");
	}
	
    @Override
    protected void afterwork() {}
    
    @Override
    protected void cleanup() {}
    
    @Override
    protected void prepare() {
    	if (mwe2File == null) try {
    		mwe2File = getProjectFiles(project, "mwe2").stream()
				.filter(file -> file.getName().endsWith("Gratext.mwe2"))
				.collect(Collectors.toList()).get(0);
    	} catch(Exception e) {
    		e.printStackTrace();
    		quit();
    		return;
    	}
		launches = new ArrayList<>();
		
		unseen = new ArrayList<>();
    	processes = new ArrayList<>();
		registerLaunchListener();
		launch(project, mwe2File);
    }
    @Override
    protected void launch(ILaunchConfigurationType type, IProject project, IFile file) throws CoreException {
    	buildMWE2(file,project);
    	
    }
    
    protected void launch(IProject project, IFile file) {
		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType type = manager.getLaunchConfigurationType("org.eclipse.emf.mwe2.launch.Mwe2LaunchConfigurationType");
		try {
			Arrays.stream(manager.getLaunchConfigurations(type))
				.filter(cfg -> cfg.getName().equals(file.getName()))
				.forEach(cfg -> { try { 
					cfg.delete();
				} catch(CoreException e) { e.printStackTrace(); }});
			launch(type, project, file);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
    
	public void buildMWE2(IFile mwe, IProject project) {
		try {
			//Mwe2StandaloneSetup.doSetup();
			IPath workspacePath = project.getWorkspace().getRoot().getRawLocation();
			Injector i = new Mwe2StandaloneSetup().createInjectorAndDoEMFRegistration();
			Mwe2Runner runner = i.getInstance(Mwe2Runner.class);
//			runner.
			System.out.println(CincoProperties.getVmArgs());
			System.out.println(project.getRawLocation());
			System.out.println(project.getLocation());
			IPath projectPath = project.getLocation().makeAbsolute();
			System.out.println(projectPath.makeAbsolute());
			Mwe2Launcher launcher = i.getInstance(Mwe2Launcher.class);
			java.net.URL url = getClass().getResource(workspacePath.append(mwe.getFullPath().makeAbsolute()).toOSString());
			
			String str = workspacePath.append(mwe.getFullPath().makeAbsolute()).toOSString();
			URI uri = URI.createFileURI(str);
			System.out.println(url);
			System.out.println("URI: "+uri);
			
			String[] args = {//mwe.getFullPath().makeAbsolute().toOSString(,
			"-p",
			String.format("runtimeProject=%s", projectPath.makeAbsolute().toOSString())};
			HashMap<String,String> params = new HashMap<>();
			params.put("runtimeProject", projectPath.makeAbsolute().toOSString());
			//launcher.run(args);
			runner.run(uri, params);
			
			
			System.out.println("NOTHING HERE");
		}catch(Exception e) {
			e.printStackTrace();
			CincoProperties.shutdown(true);
		}
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.gratext.build;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.util.GenModelUtil;
import org.eclipse.emf.codegen.util.CodeGenUtil;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.jabc.cinco.meta.core.utils.job.ReiteratingJob;

public class GratextModelBuild extends ReiteratingJob {
	private IProject project;
	private IFile genmodel;
	private IProgressMonitor monitor;
	private IStatus jobStatus;
	private boolean failed;
	
	public GratextModelBuild(IProject project) {
		super("Building Gratext: " + project.getName());
		this.project = project;
	}

	@Override
	protected void prepare() {
		monitor = getMonitor();
		findGenmodel();
		runGenmodel();
		jobStatus = Status.OK_STATUS;
	}
	
	@Override
	protected void repeat() {
		if (jobStatus != null)
			quit(jobStatus);
	}
	
	@Override
	protected void afterwork() {
		if (jobStatus.isOK())  {
			triggerRefresh();
		} else triggerRefresh();
	}
	
	private void findGenmodel() {
		if (!failed) try {
			monitor.setTaskName("Retrieve .genmodel: " + project.getName());
			genmodel = getProjectFiles("genmodel").stream()
				.filter(file -> file.getName().endsWith("Gratext.genmodel"))
				.collect(Collectors.toList())
				.get(0);
		} catch(Exception e) {
			fail("Failed to retrieve .genmodel file.", e);
			return;
		}
	}
	
	private void runGenmodel() {
		if (!failed) try {
			monitor.setTaskName("Genmodel job: " + project.getName());
			ResourceSetImpl resSet = new ResourceSetImpl();
			
			/*
			 * The URI used to load the GenModel determines how relative URIs within
			 * it are resolved. If platform:/resource/{...} is used computing the
			 * PlatformURIMap is necessary to resolve the locations correctly, e.g.
			 * by mapping on platform:/plugin/{...} URIs.
			 */
			resSet.getURIConverter().getURIMap().putAll(
					EcorePlugin.computePlatformURIMap(true));
			
			Resource res = resSet.getResource(
					URI.createPlatformResourceURI(genmodel.getFullPath().toString(), true),true);
			res.load(null);
			res.getContents().stream()
				.filter(GenModel.class::isInstance)
				.map(GenModel.class::cast)
				.forEach(genModel -> {
					genModel.reconcile();
					
					// !!! Very important lines, do not delete !!!
					genModel.getUsedGenPackages().stream()
						.filter(pkg -> !pkg.getGenModel().equals(genModel))
						.forEach(genModel.getUsedGenPackages()::add);
					
					genModel.setCanGenerate(true);
					GenModelUtil.createGenerator(genModel).generate(genModel,
						GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE,
						CodeGenUtil.EclipseUtil.createMonitor(new NullProgressMonitor(), 100));
				});
		} catch(Exception e) {
			fail("Model code generation failed.", e);
			return;
		}
	}
	
	private void triggerRefresh() {
		if (!failed) try {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			getProject(project.getName() + ".ui").refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	public void buildProject() {
		if (!failed) try {
			monitor.setTaskName("Building " + project.getName());
			project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	public void buildUIProject() {
		if (!failed) try {
			String projectName = this.project.getName() + ".ui";
			IProject project = getProject(projectName);
			if (project != null) {
				monitor.setTaskName("Building " + projectName);
				project.build(IncrementalProjectBuilder.INCREMENTAL_BUILD, null);
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private IProject getProject(String name) {
		return ResourcesPlugin.getWorkspace().getRoot().getProject(name);
	}
	
	private List<IFile> getProjectFiles(String fileExtension) {
		return getFiles(project, fileExtension, true);
	}
	
	private List<IFile> getFiles(IContainer container, String fileExtension, boolean recurse) {
	    List<IFile> files = new ArrayList<>();
	    IResource[] members = null;
	    try {
	    	members = container.members();
	    } catch(CoreException e) {
	    	e.printStackTrace();
	    }
	    if (members != null)
			Arrays.stream(members).forEach(mbr -> {
			   if (recurse && mbr instanceof IContainer)
				   files.addAll(getFiles((IContainer) mbr, fileExtension, recurse));
			   else if (mbr instanceof IFile && !mbr.isDerived()) {
				   IFile file = (IFile) mbr;
				   if (fileExtension == null || fileExtension.equals(file.getFileExtension()))
				       files.add(file);
			   }
		   });
		return files;
	}
	
	@Override
	protected void fail(String msg, Exception e) {
		failed = true;
		super.fail(msg, e);
	}
}

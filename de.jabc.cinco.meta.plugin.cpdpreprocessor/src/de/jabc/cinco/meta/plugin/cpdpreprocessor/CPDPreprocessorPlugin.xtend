package de.jabc.cinco.meta.plugin.cpdpreprocessor

import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin
import java.util.List
import mgl.MGLModel
import org.eclipse.core.resources.IProject
import productDefinition.Annotation
import productDefinition.CincoProduct

class CPDPreprocessorPlugin implements ICPDMetaPlugin {
	
	new() {
		// Intentionally left blank
	}

	override executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		// See de.jabc.cinco.meta.core.ui project
	}
	
}

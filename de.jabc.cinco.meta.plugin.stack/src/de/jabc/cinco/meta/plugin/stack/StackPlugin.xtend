package de.jabc.cinco.meta.plugin.stack

import de.jabc.cinco.meta.plugin.stack.template.project.StackProjectTemplate
import de.jabc.cinco.meta.plugin.CincoMetaPlugin

class StackPlugin extends CincoMetaPlugin{
		
	override executeCincoMetaPlugin() {
		for (mgl: allMGLs) {
			for (gm: mgl.graphModels) {
				new StackProjectTemplate => [
					model = mgl
					graphModel = gm
					create
				]
			}
		}
	}
	
}

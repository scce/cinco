Starting Requirements:

- Open JDK 11 (Oracle JDK may work, too)
- Maven 3 installed (>= 3.54)
- Eclipse "2020_06" in the "Modeling Tools" variant 
  (Newer Versions do not work at the moment): 
  https://www.eclipse.org/downloads/packages/release/2020-06/r


Preparing Eclipse Modeling:
- Linux users may want to add the lines:

    --launcher.GTK_version
    2

to 'eclipse.ini' before the line '--launcher.appendVmargs' when experiencing graphical issues
- Start Eclipse with a fresh workspace
- Help / Install Modeling Components
	- mark Xtend, Graphiti and Xtext for installation (not Xpand)
	- finish -> next -> next -> accept -> finish
	- yes, restart

Import Cinco Projects:
- Import / General / Existing Projects into Workspace 
	- select repository root (where this file is)
	- mark all found projects for import
	- finish
- Do as build.txt says


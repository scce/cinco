package de.jabc.cinco.meta.plugin.modelchecking.tmpl.provider

import de.jabc.cinco.meta.plugin.template.FileTemplate

class ModelCheckingProviderTmpl extends FileTemplate {

	override getTargetFileName() '''«graphModel.name»ModelCheckingProvider.java'''

	override template() '''
		package «package»;
		
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightDescription;
		
		«FOR node : model.nodes»
			import «node.fqBeanName»;
		«ENDFOR»
		«FOR edge : model.edges»
			import «edge.fqBeanName»;
		«ENDFOR»
		import «graphModel.fqBeanName»;
		import java.util.HashSet;
		import java.util.Set;
		import java.util.List;
		
		public abstract class «graphModel.name»ModelCheckingProvider {
			
			public Set<String> getAtomicPropositions(graphmodel.Node node){
				return null;
			}
			
			public Set<String> getEdgeLabels(graphmodel.Edge edge){
				return null;
			}
			
			«FOR node : model.nodes SEPARATOR '\n'»
				public Set<String> getAtomicPropositions(«node.name» node){
					return null;
				}
			«ENDFOR»
			
			«FOR edge : model.edges SEPARATOR '\n'»
				public Set<String> getEdgeLabels(«edge.name» edge){
					return null;
				}
			«ENDFOR»
		
			
			public boolean isSupportNode(graphmodel.Node node, boolean isSupportNodeIntended){
				return isSupportNodeIntended;
			}
			
			public Set<de.jabc.cinco.meta.plugin.modelchecking.runtime.model.ReplacementEdge> getReplacementEdges(«graphModel.name» model){
				return new HashSet<de.jabc.cinco.meta.plugin.modelchecking.runtime.model.ReplacementEdge>();
			}

			public Set<String> getReplacementEdgeLabels(List<graphmodel.Node> path, Set<String> intendedEdgeLabels) {
				return intendedEdgeLabels;
			}
			
			public boolean fulfills(«graphModel.name» model, Set<graphmodel.Node> satisfyingNodes, boolean intendedCheckResult){
				return intendedCheckResult;
			}
			
			public Set<HighlightDescription> getHighlightDescriptions(«graphModel.name» model, Set<HighlightDescription> intendedHighlightDescriptions){
				return intendedHighlightDescriptions;
			}
		}
	'''

}

package de.jabc.cinco.meta.plugin.modelchecking.tmpl.provider

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.plugin.modelchecking.util.ModelCheckingExtension
import mgl.Node
import mgl.Edge

class ProviderHandlerTmpl extends FileTemplate {

	extension ModelCheckingExtension = new ModelCheckingExtension

	String providerPath
	String providerClassName

	override init() {
		providerPath = graphModel.providerPath
		providerClassName = providerPath?.substring(providerPath.lastIndexOf(".") + 1)
	}

	override template() '''
			package «package»;
			
			«IF graphModel.providerExists»
			import java.util.List;
			import java.util.HashSet;
			«ENDIF»
			import java.util.Set;
			
			import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightDescription;
			import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FulfillmentConstraint;
			import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel;
			
			import «graphModel.fqBeanName»;	
			«IF graphModel.providerExists»
			import «graphModel.beanPackage».util.«model.fileName.toOnlyFirstUpper»Switch;
			«FOR node:model.nodes.filter[!isIsAbstract]»
				import «node.fqBeanName»;
			«ENDFOR»
			«FOR edge:model.edges.filter[!isIsAbstract]»
				import «edge.fqBeanName»;
			«ENDFOR»
			
			import «providerPath»;
			
			
			
			public class «graphModel.name.toFirstUpper»ProviderHandler extends «model.fileName.toOnlyFirstUpper»Switch<Set<String>>{
				
				«providerClassName» provider = new «providerClassName»();
				
				«FOR node:model.nodes SEPARATOR '\n'»
					«node.caseMethod»
				«ENDFOR»
				«FOR edge:model.edges SEPARATOR '\n'»
					«edge.caseMethod»
				«ENDFOR»
				
				public Set<de.jabc.cinco.meta.plugin.modelchecking.runtime.model.ReplacementEdge> getReplacementEdges(«graphModel.name» model){
					return provider.getReplacementEdges(model);
				}
				
				public Set<String> getReplacementEdgeLabels(List<graphmodel.Node> path, Set<String> intendedEdgeLabels){
					return provider.getReplacementEdgeLabels(path, intendedEdgeLabels);
				}

		«ELSE»
			public class «graphModel.name.toFirstUpper»ProviderHandler{
		«ENDIF»
		
			public boolean isSupportNode(graphmodel.Node node){
				boolean intended = false;
				
				«FOR node:model.nodes.filter[isSupportNode]»
				if (node instanceof «node.name») intended = true;
				«ENDFOR»
				
				«IF graphModel.providerExists»
				return provider.isSupportNode(node, intended);
				«ELSE»
				return intended;
				«ENDIF»
			}
		
			public boolean fulfills(«graphModel.name» model, CheckableModel<?,?> checkableModel, Set<graphmodel.Node> satisfyingNodes){
				boolean intendedCheckResult = FulfillmentConstraint.«graphModel.fulfillmentConstraint».fulfills(checkableModel, satisfyingNodes);
				«IF graphModel.providerExists»
					return provider.fulfills(model, satisfyingNodes, intendedCheckResult);
				«ELSE»
					return intendedCheckResult;
				«ENDIF»
			}
			
			public Set<HighlightDescription> getHighlightDescriptions(«graphModel.name» model, Set<HighlightDescription> intendedHighlightDescriptions){
				«IF graphModel.providerExists»
					return provider.getHighlightDescriptions(model, intendedHighlightDescriptions);
				«ELSE»
					return intendedHighlightDescriptions;
				«ENDIF»
			}
		}	
	'''
	
	def getCaseMethod(Node node){
		'''
		@Override
		public Set<String> case«node.name»(«node.name» node) {
			Set<String> atomicPropositions = new HashSet<String>();
			
			Set<String> nodeAPs = provider.getAtomicPropositions(node);
			
			if (nodeAPs != null){
				atomicPropositions.addAll(nodeAPs);
			}
			
			«IF node.superType !== null»
			Set<String> inherited = case«node.superType.name»(node);
			«ELSE»
			Set<String> inherited = provider.getAtomicPropositions((graphmodel.Node) node);
			«ENDIF»
			if (inherited != null) {
				atomicPropositions.addAll(inherited);
			}
			
			return atomicPropositions;
		}
		'''
	}
	
	def getCaseMethod(Edge edge){
		'''
		@Override
		public Set<String> case«edge.name»(«edge.name» edge) {
			Set<String> edgeLabels = new HashSet<String>();
			
			Set<String> labels = provider.getEdgeLabels(edge);
			
			if (labels != null){
				edgeLabels.addAll(labels);
			}
			
			«IF edge.superType !== null»
			Set<String> inherited = case«edge.superType.name»(edge);
			«ELSE»
			Set<String> inherited = provider.getEdgeLabels((graphmodel.Edge) edge);
			«ENDIF»
			if (inherited != null) {
				edgeLabels.addAll(inherited);
			}
			
			return edgeLabels;
		}
		'''
	}
	

	override getTargetFileName() '''«graphModel.name.toFirstUpper»ProviderHandler.java'''
}

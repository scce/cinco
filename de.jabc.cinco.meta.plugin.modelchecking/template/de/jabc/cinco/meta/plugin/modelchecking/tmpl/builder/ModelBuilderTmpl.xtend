package de.jabc.cinco.meta.plugin.modelchecking.tmpl.builder

import de.jabc.cinco.meta.plugin.template.FileTemplate
import mgl.Attribute
import mgl.ModelElement
import mgl.UserDefinedType
import de.jabc.cinco.meta.plugin.modelchecking.util.ModelCheckingExtension

class ModelBuilderTmpl extends FileTemplate {

	override getTargetFileName() '''«graphModel.name»ModelBuilder.java'''

	extension ModelCheckingExtension = new ModelCheckingExtension

	var boolean providerExists

	override init() {
		providerExists = graphModel.providerExists
	}

	override template() '''
		package «package»;
		
		import java.util.ArrayList;
		import java.util.Arrays;
		import java.util.HashSet;
		import java.util.List;
		import java.util.Set;
		import java.util.stream.Collectors;
		
		import org.eclipse.emf.common.util.EList;
		
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.*;	
		import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension;
		
		import «model.package».modelchecking.«graphModel.name.toFirstUpper»ProviderHandler;
		
		import graphmodel.Edge;
		import graphmodel.Node;
		import graphmodel.ModelElement;
		
		public class «graphModel.name»ModelBuilder implements ModelBuilder<«graphModel.fqBeanName»> {
			
			private ModelCheckingRuntimeExtension xapi = new ModelCheckingRuntimeExtension();
			
			private ArrayList<String> selectedIds;
			private boolean withSelection;
			«graphModel.name.toFirstUpper»ProviderHandler providerHandler;
			
			public void init(boolean withSelection){
				selectedIds = xapi.getSelectedElementIds();
				this.withSelection = withSelection && selectedIds.size() != 0;
				providerHandler = new «graphModel.name.toFirstUpper»ProviderHandler();
			}
			
			@Override
			public void buildModel(«graphModel.fqBeanName» model, CheckableModel<?,?> checkableModel, boolean withSelection){
				init(withSelection);
				
				Set<Node> supportNodes = new HashSet<>();
				Set<Node> includeNodesWithSupportNodeSuccs = new HashSet<>();
				Set<Node> includeNodesWithSupportNodePreds = new HashSet<>();
				Set<Node> includeNodes = new HashSet<>();
				
				xapi.getAllNodesAndContainers(model).stream().filter(node -> toInclude(node))
				.forEach(node -> {
					if (providerHandler.isSupportNode(node)) {
						supportNodes.add(node);
					}else{
						includeNodes.add(node);
					}
				});
				
				includeNodes.stream().filter(node -> node.getSuccessors().stream()
					.anyMatch(succ -> supportNodes.contains(succ))
				)
				.forEach(node -> includeNodesWithSupportNodeSuccs.add(node));
				
				includeNodes.stream().filter(node -> node.getPredecessors().stream()
					.anyMatch(pred -> supportNodes.contains(pred))
				)
				.forEach(node -> includeNodesWithSupportNodePreds.add(node));
				
				for (Node node : includeNodes) {
					checkableModel.addNewNode(node.getId(), isStartNode(node), getAtomicPropositions(node));
				}			
				
				for (Edge edge : model.getAllEdges()) {
					if (isIncludedEdgeType(edge)) {
						addEdge(checkableModel, edge.getSourceElement().getId(), edge.getTargetElement().getId(), getEdgeLabels(edge), includeNodes);
					}
				}
		
				«IF providerExists»
					for(ReplacementEdge edge: providerHandler.getReplacementEdges(model)){
						if (edge.isValidEdge()) {
							addEdge(checkableModel, edge.getSource().getId(), edge.getTarget().getId(), edge.getLabels(), includeNodes);
						}
					}
				«ENDIF»	
				
				List<List<Node>> replacementPaths = new ArrayList<List<Node>>();
				for (Node node : includeNodesWithSupportNodeSuccs) {
					List<List<Node>> paths = xapi.findPathsToFirst(node, Node.class, n-> includeNodesWithSupportNodePreds.contains(n))
							.stream()
							.filter(path -> path.size() > 1)
							.filter(path -> path.subList(0, path.size()-2)
										.stream()
										.allMatch(supportNode -> supportNodes.contains(supportNode)))
							.collect(Collectors.toList());
					paths.forEach(path -> path.add(0, node));
					replacementPaths.addAll(paths);
				}
				
				for (List<Node>  path : replacementPaths) {
					«IF providerExists»
						Set<String> labels = providerHandler.getReplacementEdgeLabels(path, getEdgeLabelsOfPath(path));
						if (labels == null){
							labels = new HashSet<String>();
						}
					«ELSE»
						Set<String> labels = getEdgeLabelsOfPath(path);
					«ENDIF»
					addEdge(checkableModel, path.get(0).getId(), path.get(path.size() - 1).getId(), labels, includeNodes);
				}
		
			}
			
			private <N,E> void addEdge(CheckableModel<N,E> checkableModel, String sourceId, String targetId, Set<String> labels, Set<Node> includeNodes){
				if (isAddableEdge(sourceId, targetId, includeNodes))
				checkableModel.addNewEdge(getNodeById(checkableModel, sourceId), getNodeById(checkableModel, targetId), labels);
			}
			
			«FOR node : nodes SEPARATOR '\n'»
				«templateGetAtomicPropositions(node, providerExists, false)»
			«ENDFOR»
			
			«FOR type : model.userDefinedTypes SEPARATOR '\n'»
				«templateGetAtomicPropositions(type, false, true)»
			«ENDFOR»
			
			private Set<String> getEdgeLabels(Edge edge){
				Set<String> edgeLabels = new HashSet<>();
				
				«FOR e : edges SEPARATOR '\n'»
					if (edge instanceof «e.fqBeanName»){
						edgeLabels = getEdgeLabels((«e.fqBeanName») edge);
					}
				«ENDFOR»
				
				edgeLabels.remove("");
				edgeLabels.remove(null);
				return edgeLabels;				
			}
			
			private Set<String> getAtomicPropositions(Node node){
				Set<String> atomicPropositions = new HashSet<>();
				
				«FOR n : nodes SEPARATOR '\n'»
					if (node instanceof «n.fqBeanName»){
						atomicPropositions = getAtomicPropositions((«n.fqBeanName») node);
					}
				«ENDFOR»
				
				atomicPropositions.remove("");
				atomicPropositions.remove(null);
				return atomicPropositions;
			}
			
			private Set<String> getEdgeLabelsOfPath(List<Node> path){
				Set<String> labels = new HashSet<>();
				for(int i = 0; i<path.size()-1;i++) {
					final int index = i;
					Set<Edge> edges = path.get(index).getOutgoing().stream()
							.filter(edge -> edge.getTargetElement().equals(path.get(index+1)))
							.collect(Collectors.toSet());
					edges.stream().forEach(edge -> labels.addAll(getEdgeLabels(edge)));
				}
				for (int i=1; i<path.size()-1;i++) {
					labels.addAll(getAtomicPropositions(path.get(i)));
				}
				
				return labels;
			}
			
			«FOR edge : edges SEPARATOR '\n'»
				«templateGetEdgeLabels(edge, providerExists, false)»
			«ENDFOR»
			
			«FOR type : model.userDefinedTypes SEPARATOR '\n'»
				«templateGetEdgeLabels(type, false, true)»
			«ENDFOR»			
			
			private boolean isIncludedEdgeType(Edge edge){
				String edgeName = edge.getClass().getSimpleName().substring(1);
				
				«FOR e : edges»
					if (edgeName.equals(«e.fqBeanName».class.getSimpleName())) return true;
				«ENDFOR»
				
				return false;
			}
			
			private boolean isIncludedNodeType(Node node){
				String nodeName = node.getClass().getSimpleName().substring(1);
				
				«FOR n : nodes»
					if (nodeName.equals(«n.fqBeanName».class.getSimpleName())) return true;
				«ENDFOR»
				
				return false;
			}
		
			private boolean isStartNode(Node node) {
				«FOR node : nodes.filter[startNode] SEPARATOR '\n'»
					if (node instanceof «node.fqBeanName»){
						return true;
					}
				«ENDFOR»				
				return false;
			}
			
			private boolean isRelevantNode(Node node) {
				EList<Edge> incomingEdges = node.getIncoming();
				if(incomingEdges.size() > 0) {
					for (Edge edge : node.getIncoming()) {
						if (isIncludedEdgeType(edge)) {
							return true;
						}
					}
					return false;
				}
				return true;
			}
			
			private boolean isSelected(ModelElement element){
				return !withSelection || selectedIds.contains(element.getId());
			}
			
			private boolean toInclude(Node node){
				return isSelected(node) && isRelevantNode(node) && isIncludedNodeType(node);
			}
			
			private boolean isAddableEdge(String sourceId, String targetId, Set<Node> includeNodes){
				return includeNodes.stream().map(node -> node.getId())
					.collect(Collectors.toSet())
					.containsAll(Arrays.asList(sourceId, targetId));
			}
			
			private <N,E> N getNodeById(CheckableModel<N,E> checkableModel, String id) {
				return checkableModel.getNodes().stream()
					.filter(node -> checkableModel.getId(node).equals(id))
					.findFirst().orElse(null);
			}
			
		}
	'''

	def templateGetAtomicPropositions(ModelElement element, boolean provider, boolean forType) '''
		private Set<String> getAtomicPropositions(«element.fqBeanName» element«IF forType», Set<String> visitedIds«ENDIF»){
			Set<String> atomicPropositions = new HashSet<>();			
			«IF provider»
				
				Set<String> providerAPs = providerHandler.doSwitch(element);
				if (providerAPs != null){
					atomicPropositions.addAll(providerAPs);
				}
			«ENDIF»			
			«FOR attr : element.getAPAttributes»
				
				«templateGetPrimitveAttribut(attr, "atomicPropositions")»
			«ENDFOR»			
			«IF !forType && element.userDefinedTypeAttributes.size > 0»
				
				Set<String> visitedIds = new HashSet<>();
			«ENDIF»			
			«FOR attr : element.userDefinedTypeAttributes»
				
				«templateGetUserDefinedTypeAttribute(attr, "atomicPropositions")»
			«ENDFOR»
			
			atomicPropositions.remove("");
			
			return atomicPropositions;
		}
	'''

	def templateGetEdgeLabels(ModelElement element, boolean provider, boolean forType) '''
		private Set<String> getEdgeLabels(«element.fqBeanName» element«IF forType», Set<String> visitedIds«ENDIF»){
			Set<String> edgeLabels = new HashSet<>();			
			«IF provider»
				
				Set<String> providerLabels = providerHandler.doSwitch(element);
				if (providerLabels != null){
					edgeLabels.addAll(providerLabels);
				}
			«ENDIF»						
			«FOR attr : element.edgeLabelAttributes»
				
				«templateGetPrimitveAttribut(attr, "edgeLabels")»
			«ENDFOR»			
			«IF !forType && element.userDefinedTypeAttributes.size > 0»
				
				Set<String> visitedIds = new HashSet<>();
			«ENDIF»			
			«FOR attr : element.userDefinedTypeAttributes»
				
				«templateGetUserDefinedTypeAttribute(attr, "edgeLabels")»
			«ENDFOR»
			
			edgeLabels.remove("");
			
			return edgeLabels;
		}
	'''

	def templateGetPrimitveAttribut(Attribute attr, String fieldName) '''	
		«IF attr.list»
			for(Object prop : element.«getGetter(attr.name)»){
				if (prop != null){
					«fieldName».add(prop.toString());
				}
			}				
		«ELSE»
			if (element.«getGetter(attr.name)» != null){
				«fieldName».add(element.«getGetter(attr.name)».toString());
			}
		«ENDIF»		
	'''

	def templateGetUserDefinedTypeAttribute(Attribute attr, String fieldName) '''	
		«IF attr.list»
			for («(attr.type as UserDefinedType).fqBeanName» n : element.«attr.name.getter»){
				if (visitedIds.add(n.getId())) {
					«fieldName».addAll(get«fieldName.toFirstUpper»(n, visitedIds));
				}
			}
		«ELSE»
			if (element.«attr.name.getter» != null && visitedIds.add(element.«attr.name.getter».getId())) {
				«fieldName».addAll(get«fieldName.toFirstUpper»(element.«attr.name.getter», visitedIds));
			}
		«ENDIF»		
	'''

	def getNodes() {
		model.nodes.filter[!hasValidExcludeOption && !isIsAbstract].toList
	}

	def getSupportNodes() {
		model.nodes.filter[supportNode].toList
	}

	def getEdges() {
		model.edges.filter[!hasValidExcludeOption && !isIsAbstract].toList
	}

	def boolean hasValidExcludeOption(ModelElement element) {
		if (element !== null) {
			if(element.hasAnnotationValue(ANNOTATION_MC, "include")) return false
			if(element.hasAnnotationValue(ANNOTATION_MC, "exclude")) return true

			return element.superType.hasValidExcludeOption
		}
		return model.hasAnnotationValue(ANNOTATION_DEFAULT, "exclude")
	}

	def getAPAttributes(ModelElement element) {
		element.getAttributesWithAnnotation(ANNOTATION_ATOMICPROPOSITION)
	}

	def getEdgeLabelAttributes(ModelElement element) {
		element.getAttributesWithAnnotation(ANNOTATION_EDGELABEL)
	}

	def getGetter(String attr) {
		"get" + attr.substring(0, 1).toUpperCase + attr.substring(1) + "()"
	}
}

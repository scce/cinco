/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.validation

import de.jabc.cinco.meta.core.mgl.validation.MGLValidator
import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator
import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult
import de.jabc.cinco.meta.plugin.modelchecking.provider.ModelCheckingProposalProvider
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import de.jabc.cinco.meta.plugin.modelchecking.util.ModelCheckingExtension
import de.jabc.cinco.meta.plugin.modelchecking.util.PreferencesExtension
import java.util.Arrays
import mgl.Annotation
import mgl.Attribute
import mgl.EDataTypeType
import mgl.Edge
import mgl.GraphModel
import mgl.ModelElement
import mgl.Node
import mgl.UserDefinedType
import mgl.impl.ComplexAttributeImpl
import mgl.impl.PrimitiveAttributeImpl
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import de.jabc.cinco.meta.core.utils.MGLUtil

class ModelCheckingValidator implements IMetaPluginValidator{
	
	extension ModelCheckingExtension = new ModelCheckingExtension
	extension PreferencesExtension = new PreferencesExtension
	
	var EObject checkObject = null
	
	override checkOnEdit(EObject eObject){
		checkObject = eObject
		var ValidationResult<String, EStructuralFeature> result 
		
		//Annotation
		if (eObject instanceof Annotation){
			result = eObject.checkAnnotationWithoutModelcheck
			if (result !== null){
					return result
			}
			
			result = eObject.checkHasValidNumberOfArguments
			if (result !== null){
					return result
			}
			
			result = eObject.checkHasValidValues
			if (result !== null){
					return result
			}
			
			result = eObject.checkAnnotationOnValidElement
			if (result !== null){
					return result
			}	
		}
		
		// Attribute
		if (eObject instanceof Attribute){
			
			//Formulas Attribute
			if (hasAnnotation(eObject, ANNOTATION_FORMULAS)){
				
				//Errors
				result = eObject.checkFormulasAttributeIsList
				
				if (result !== null){
					return result
				}
				
				result = eObject.checkFormulasIsGraphModelAttribute
				
				if (result !== null){
					return result
				}
				
				result = eObject.checkFormulasAttributeCorrectType
				
				if (result !== null){
					return result
				}
				
				result = eObject.checkFormulasAttributeReadOnly
				
				if (result !== null){
					return result
				}
				
				//Warnings
				result = eObject.checkFormulasAttributeHidden
				if (result !== null){
					return result
				}	
				
				result = eObject.checkFormulasHasBooleanAttribute
				if(result !== null){
					return result
				}
				
				result = eObject.checkIsFirstFormulasAttribut
				
				if (result !== null){
					return result
				}	
					
			}
		
		}
		return null
	}
	
	//Annotation Checks
	def checkAnnotationWithoutModelcheck(Annotation annotation){
		if (annotation.modelCheckingAnnotation){
			var ModelElement modelElement
			if (annotation.parent instanceof Attribute){
				modelElement = (annotation.parent as Attribute).modelElement
			}
			else if(annotation.parent instanceof ModelElement){
				modelElement = annotation.parent as ModelElement
			}			
			if (!modelElement?.graphModel.annotations.map[name].contains(ANNOTATION_MODELCHECKING)){
				return newError("To use this annotation, the graphmodel needs to be annotated with @" + ANNOTATION_MODELCHECKING + ".")
			}
		}	
	}
	
	def checkProviderClassExists(Annotation annotation){
		val mglValidator = new MGLValidator
		
		if(annotation.value.size == 1){ //only one parameter 
			val parameter = annotation.value.head
			if(parameter != ""){
				val correctFile = mglValidator.findClass(parameter)  //find the corresponding java class file
				if(correctFile !== null){
					if(correctFile.exists ){ //checks if class exists 
						val packageExport = correctFile.packageFragment.elementName
						
						for(project : ResourcesPlugin.workspace.root.projects){
							val package = mglValidator.findPackage(parameter);
							if(package === null){
								newError("Package not found.")
							}else{
								if(project.name.equals(package.elementName)){
									var folder = project.getFolder("META-INF")
									var manifest = folder.getFile("MANIFEST.MF")
									if(manifest.exists){
										val project1 = project;
										val isExported = mglValidator.findExportedPackage(project1, packageExport);
											if(!isExported){
										    	newWarning("Corresponding package is not exported.")
											}
										}
									}
								}
							}
							
					}else{ 
						newError("Java Class does not exists.")
					}
				}
				else{
					newError("Java Class does not exists.")
				}
			}
			else{
				newError("Java Class cannot be an empty String.")
			}
		}	
	}

	
	def checkAnnotationOnValidElement(Annotation annotation){
		switch(annotation.name){
			case ANNOTATION_ATOMICPROPOSITION:{
				var element = (annotation.parent as Attribute).modelElement
				if (!(element instanceof Node) && !(element instanceof UserDefinedType)){
					return newError("Atomic Propositions are only allowed in Nodes/Containers and UserDefinedTypes")
				}
				if (!((annotation.parent as Attribute) instanceof PrimitiveAttributeImpl)){
					return newError("@" + ANNOTATION_ATOMICPROPOSITION + " is only allowed on primitive attributes like EString, EInt, ...")
				}
			}
			case ANNOTATION_EDGELABEL:{
				var element = (annotation.parent as Attribute).modelElement
				if (!(element instanceof Edge) && !(element instanceof UserDefinedType)){
					return newError("Edge Labels are only allowed in Edges and UserDefinedTypes")
				}
				if (!((annotation.parent as Attribute) instanceof PrimitiveAttributeImpl)){
					return newError("@" + ANNOTATION_EDGELABEL + " is only allowed on primitive attributes like EString, EInt, ...")
				}
			}
		}
	}
	
	def checkHasValidNumberOfArguments(Annotation annotation){
		switch(annotation.name){
			case ANNOTATION_MODELCHECKING:{
				if (annotation.value.size > 1){
					return newError("Invalid number of arguments.")
				}
			} 
			case ANNOTATION_STARTNODE, case ANNOTATION_SUPPORTNODE, case ANNOTATION_ATOMICPROPOSITION, case ANNOTATION_EDGELABEL, case ANNOTATION_FORMULAS:{
				if (annotation.value.size > 0){
					return newError("Invalid number of arguments.")
				}
			}
			case  ANNOTATION_FULFILLMENT, case ANNOTATION_DEFAULT, case ANNOTATION_MC: {
				if (annotation.value.size != 1){
					return newError("Invalid number of arguments.")
				}
			}
			case ANNOTATION_PREFERENCES: {
				if (annotation.value.size == 0){
					return newError("Invalid number of arguments.")
				}
			}
		}		
	}
	
	def checkHasValidValues(Annotation annotation){
		if (annotation.name == ANNOTATION_PREFERENCES){
			return validatePreferenceAnnotation(annotation)
		}
		if (annotation.name == ANNOTATION_MODELCHECKING){
			return checkProviderClassExists(annotation)
		}
		if (annotation.modelCheckingAnnotation){
			val acceptedStrings = (new ModelCheckingProposalProvider)
				.getAcceptedStrings(annotation)			
			val unknown = annotation.value.filter[
				!acceptedStrings.map[replace("\"","")].contains(it)
			]
			if (!unknown.empty){
				if (unknown.size == 1 && unknown.get(0) !== null){
					return newError(Arrays.toString(unknown) + " is an unknown values. Only use " + Arrays.toString(acceptedStrings)+ ".")
				}else{
					return newError(Arrays.toString(unknown) + " are unknown values. Only use " + Arrays.toString(acceptedStrings)+ ".")
				}
			}
		}
	}
	
	def checkValidNumberOfAnnotations(Annotation annotation){
		if (annotation.modelCheckingAnnotation){
			val parent = annotation.parent
			if (parent.annotations.filter[name == annotation.name].size > 1){
				return newError("Only one @" + annotation.name + " annotation is allowed.")
			}	
		}		
	}
	
	//Preferences validation
	def validatePreferenceAnnotation(Annotation annotation){		
		for (it: annotation.value){
			if (!contains(PREFERENCES_OPERATOR)){
				return newError("Use the operator \"" + PREFERENCES_OPERATOR +"\" to assign default preferences.")
			}
			val currentKey = key
			val currentValue = value
			
			if (!preferenceKeys.contains(currentKey)){
				return newError(currentKey + " is unknown.")
			}
			
			if (currentKey == "highlightColor"){
				val result = currentKey.validateColor(currentValue)
				if (result !== null){
					return result
				} 
			}
			
			if (!isValidValue(currentKey, currentValue)){
				return newError(currentValue + " isn't a valid value for " + currentKey + ".")
			}
		}
	}
	
	def validateColor(String key, String value){		
		try {
			val rgb = new ModelCheckingRuntimeExtension().parseRGB(value)
			if (rgb === null){
				return newError(value + " isn't a valid value for " + key + ".")
			}
		} catch (Exception exception) {
			return newError(value + " isn't a valid value for " + key + ".")
		}
		null
	}
	
	//Formulas Attribute
	def checkFormulasAttributeIsList(Attribute attr){
		if (attr.lowerBound != 0 || attr.upperBound != -1){
			return newError("The mcFormulas attribute needs to be a list with lowerBound = 0 and infinite upperBound")
		}
		null
	}
	
	def checkFormulasAttributeCorrectType(Attribute attr){
	
		if (attr instanceof ComplexAttributeImpl && (attr as ComplexAttributeImpl).type instanceof UserDefinedType){
			
			val attrType = (attr as ComplexAttributeImpl).type as UserDefinedType	
			
			val hasThreeStringAttributes = attrType.attributes.filter[
												lowerBound == 0 && upperBound == 1 	
												&& it instanceof PrimitiveAttributeImpl
												&& (it as PrimitiveAttributeImpl).type == EDataTypeType.ESTRING
								 			].size>2
			if (!hasThreeStringAttributes){
				return newError(
					"The type of the @mcFormulas attribute needs to have three attributes of type EString and optional one attribute of type EBoolean. "+
					"E.g. use the following UserDefinedType:\n\n" +
					"type Formula{\n"+
					"\tattr EString as expression\n"+
					"\tattr EString as description\n"+
					"\tattr EString as varName\n"+
					"\tattr EBoolean as toCheck\n"+
					"}"
				)	
			} 	
		}		
	}
	
	def checkIsFirstFormulasAttribut(Attribute attr){
		if (attr.eContainer instanceof GraphModel){
			var model = attr.eContainer as GraphModel
			var index = 0
			var formulasFound = false
			while (!formulasFound && index < model.attributes.size){
				if(model.attributes.get(index).annotations.map[name].contains(ANNOTATION_FORMULAS)){
					formulasFound = true
				}else{
					index++
				}
			}
			
			if (formulasFound && attr.equals(model.attributes.get(index))){
				return null
			}else{
				return newWarning("There is already a mcFormulas attribute. Only the first will be used.")
			}
		}
		null
	}
	
	def checkFormulasIsGraphModelAttribute(Attribute attr){
		if (!(attr.eContainer instanceof GraphModel)){
			return newError(ANNOTATION_FORMULAS + " attribute has to be a direct GraphModel attribute.")
		}
		null
	}
	
	def checkFormulasAttributeReadOnly(Attribute attr){
		if (!attr.annotations.map[name].contains("readOnly")){
			return newError(ANNOTATION_FORMULAS + " attribute has to be annotated with @readOnly.")
		}
		null
	}
	
	def checkFormulasAttributeHidden(Attribute attr){
		if (!attr.annotations.map[name].contains("propertiesViewHidden")){
			return newWarning(ANNOTATION_FORMULAS + " attribute should be annotated with @propertiesViewHidden.")
		}
		null
	}
	
	def checkFormulasHasBooleanAttribute(Attribute attr){
		if (attr instanceof ComplexAttributeImpl && (attr as ComplexAttributeImpl).type instanceof UserDefinedType){
			val attrType = (attr as ComplexAttributeImpl).type as UserDefinedType	
			val hasBoolAttr = attrType.attributes.filter[
												lowerBound == 0 && upperBound == 1 	
												&& it instanceof PrimitiveAttributeImpl
												&& (it as PrimitiveAttributeImpl).type == EDataTypeType.EBOOLEAN
								 			].size > 0
			if (!hasBoolAttr)	{
				return newWarning(
					"To safe the check-option also add an attribute of type EBoolean."
				)
			}			
		}
	}
	
	//Util Methods
	def isModelCheckingAnnotation(Annotation annotation){
		allAnnotations.contains(annotation.name)
	}
	
	def startNodesExist(GraphModel model){
		MGLUtil.getUsableNodes(model).exists[hasAnnotation(ANNOTATION_STARTNODE)]
	}
	
	def newError(String message) {
		ValidationResult.newError(message, checkObject.eClass.getEStructuralFeature("name"))
	}
	
	def newInfo(String message) {
		ValidationResult.newInfo(message, checkObject.eClass.getEStructuralFeature("name"))
	}
	
	def newWarning(String message) {
		ValidationResult.newWarning(message, checkObject.eClass.getEStructuralFeature("name"))
	}
	
}

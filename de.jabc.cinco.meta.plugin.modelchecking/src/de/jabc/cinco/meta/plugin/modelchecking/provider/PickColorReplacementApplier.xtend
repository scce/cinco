/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.provider

import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.swt.widgets.ColorDialog
import org.eclipse.swt.widgets.Shell

class PickColorReplacementApplier extends ReplacementTextApplier{
	val String key
	val String operator
	val String proposal
	
	new(String proposal, String key, String operator) {
		this.key = key
		this.operator = operator
		this.proposal = proposal
	}
	
	override getActualReplacementString(ConfigurableCompletionProposal proposal) {
		if (proposal.displayString == this.proposal){
			val cDialog = new ColorDialog(new Shell)
			val rgb = cDialog.open
			
			val r = String.valueOf(rgb.red)
			val g = String.valueOf(rgb.green)
			val b = String.valueOf(rgb.blue)
			
			return '''"«key» «operator» («r»,«g»,«b»)"'''
		}
		return proposal.displayString	
	}
}

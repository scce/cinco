/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.util

import mgl.GraphModel
import mgl.impl.ComplexAttributeImpl
import mgl.UserDefinedType
import mgl.impl.PrimitiveAttributeImpl
import mgl.EDataTypeType
import mgl.Annotatable
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FulfillmentConstraint
import mgl.ModelElement
import de.jabc.cinco.meta.core.utils.xapi.GraphModelExtension
import mgl.Annotation
import de.jabc.cinco.meta.core.utils.MGLUtil
import mgl.MGLModel
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class ModelCheckingExtension {
	
	extension GraphModelExtension = new GraphModelExtension 
	
	public val String ANNOTATION_MODELCHECKING = "modelchecking"
	public val String ANNOTATION_DEFAULT = "mcDefault"
	public val String ANNOTATION_FULFILLMENT = "mcFulfillmentConstraint"
	public val String ANNOTATION_STARTNODE = "mcStartNode"
	public val String ANNOTATION_SUPPORTNODE = "mcSupportNode"
	public val String ANNOTATION_ATOMICPROPOSITION = "mcAtomicProposition"
	public val String ANNOTATION_EDGELABEL = "mcEdgeLabel"
	public val String ANNOTATION_FORMULAS = "mcFormulas"
	public val String ANNOTATION_MC = "mc"
	public val String ANNOTATION_PREFERENCES = "mcPreferences"
	
	public val allAnnotations = #[
		ANNOTATION_ATOMICPROPOSITION,
		ANNOTATION_DEFAULT,
		ANNOTATION_EDGELABEL,
		ANNOTATION_FORMULAS,
		ANNOTATION_FULFILLMENT,
		ANNOTATION_MC,
		ANNOTATION_MODELCHECKING,
		ANNOTATION_PREFERENCES,
		ANNOTATION_STARTNODE,
		ANNOTATION_SUPPORTNODE
	]
	
	//nameMethods
	def toOnlyFirstUpper(String text){
		text?.toLowerCase.toFirstUpper
	}
	
	//Provider Methods
	def getProviderPath(GraphModel model){
		model.annotations.filter[
			name == ANNOTATION_MODELCHECKING
		].head?.value.head
	}
	
	def providerExists(GraphModel model){
		getProviderPath(model) !== null
	}
	
	//Formula Methods
	def getFormulaAttribute(GraphModel model){
		model.attributes.filter[hasAnnotation(ANNOTATION_FORMULAS)].head
	}
	
	def getFormulasAttributeName(GraphModel model){
		return model.getFormulaAttribute?.name.toFirstUpper
	}
	
	def getFormulasType(GraphModel model){
		val formulaAttribute = model.formulaAttribute
		if(formulaAttribute !== null && formulaAttribute instanceof ComplexAttributeImpl) 
		(formulaAttribute as ComplexAttributeImpl).type as UserDefinedType
	}
	
	def getFormulasTypeName(GraphModel model){
		model.formulasType?.name
	}
	
	def getFormulaStringAttributes (GraphModel model){
		model.formulasType?.attributes.filter[
			lowerBound == 0 && upperBound == 1 	
			&& it instanceof PrimitiveAttributeImpl
			&& (it as PrimitiveAttributeImpl).type == EDataTypeType.ESTRING
		]
	}
	
	def getExpressionAttributeName (GraphModel model){
		model.formulaStringAttributes?.head?.name
	}
	
	def getDescriptionAttributeName(GraphModel model){
		model.formulaStringAttributes?.get(1)?.name
	}
	
	def getVarAttributeName(GraphModel model){
		model.formulaStringAttributes?.get(2)?.name
	}
	
	def getCheckAttribute(GraphModel model){
		model.formulasType?.attributes.filter[
			lowerBound == 0 && upperBound == 1
			&& it instanceof PrimitiveAttributeImpl
			&& (it as PrimitiveAttributeImpl).type == EDataTypeType.EBOOLEAN
		].head
	}
	
	def getCheckAttributeName (GraphModel model){
		model.checkAttribute?.name
	}
	
	def checkAttributeExists(GraphModel model){
		model.checkAttribute !== null
	}
	
	def formulasExist(GraphModel model){
		model.getFormulaAttribute !== null
	}

	def getStartNodes(GraphModel model){
		MGLUtil.getUsableNodes(model).filter[hasAnnotation(ANNOTATION_STARTNODE)]
	}
	
	def getFulfillmentConstraint(GraphModel model){
		switch(model.getAnnotationValues(ANNOTATION_FULFILLMENT).head){
			case "ALL_NODES" : return FulfillmentConstraint.ALL_NODES
			case "ANY_STARTNODE" : return FulfillmentConstraint.ANY_STARTNODE
			case "EACH_STARTNODE" : return FulfillmentConstraint.EACH_STARTNODE
			default: return FulfillmentConstraint.defaultValue
		}
	}
	
	def isStartNode(ModelElement element){
		element.hasInheritedAnnotation(ANNOTATION_STARTNODE)
	}
	
	def boolean isSupportNode(ModelElement element){
		element.hasInheritedAnnotation(ANNOTATION_SUPPORTNODE)
	}
	
	def hasAnnotationValue(Annotatable element, String annotation, String value){
		element.getAnnotationValues(annotation).contains(value)		
	}
	
	def boolean hasInheritedAnnotation(ModelElement element, String annotation){
		if (element !== null){
			if (element.hasAnnotation(annotation)) return true;
			
			return element.superType.hasInheritedAnnotation(annotation)
		}
		false
	}
	
	def getAttributesWithAnnotation(ModelElement element, String annotation){
		element.allAttributes.filter[hasAnnotation(annotation)].toList
	}
	
	def getUserDefinedTypeAttributes(ModelElement element){
		element.allAttributes.filter[type instanceof UserDefinedType]
	}
	
	def getAnnotationValues(Annotatable element, String annotation){
		element.annotations.filter[
			name == annotation
		].flatMap[value].toList
	}
	
	def hasAnnotation(Annotatable model, String annotationName){
		_graphModelExtension.hasAnnotation(model, annotationName)
	}
	
	def getAnnotatedModelElement(Annotation annotation){
		_graphModelExtension.getAnnotatedModelElement(annotation)
	}
	
	def getGraphModel(ModelElement element){
		_graphModelExtension.getGraphModel(element)
	}	
	
	def getFileName(MGLModel model){
		var generatorUtils = GeneratorUtils.instance
		generatorUtils.getFileName(model)
	}
}

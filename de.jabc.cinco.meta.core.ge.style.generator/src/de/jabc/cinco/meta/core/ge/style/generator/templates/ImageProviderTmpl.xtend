/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import java.io.File
import java.net.MalformedURLException
import java.net.URL
import java.util.Hashtable
import java.util.List
import java.util.Map.Entry
import mgl.Annotation
import mgl.GraphModel
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.graphiti.ui.platform.AbstractImageProvider
import org.eclipse.graphiti.ui.platform.IImageProvider
import org.osgi.framework.Bundle
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator

class ImageProviderTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
/**
 * Generates the {@link IImageProvider} code
 *
 * @param gm The processed {@link GraphModel} 
 */	
	def generateImageProvider(MGLModel mgl)'''
		package «mgl.packageName»;


		public class «mgl.fuName»ImageProvider extends «AbstractImageProvider.name» 
			implements «IImageProvider.name» {
		
			private «Hashtable.name»<«String.name», «String.name»> images;
			
			/**
			 * Sets an ImageProvider
			*/
			public «mgl.fuName»ImageProvider() {
				«mgl.fuName»GraphitiUtils.getInstance().setImageProvider(this);
			}
		
			/**
			 * Adds an image to the editor
			 * @param id : Id of an image
			 * @param path : Path of the image
			 */
			public void addImage(«String.name» id, «String.name» path) {
				if (images == null) {
					images = new «Hashtable.name»<«String.name», «String.name»>();
				}
				images.put(id, path);
			}
			
			/**
			 * Returns the id of an image.
			 * @param path : Path is the path of an image
			 * @return Returns the id of an image.
			*/
			public «String.name» getImageId(«String.name» path) {
				for («Entry.entryName»<«String.name», «String.name»> e : images.entrySet()){
					if (e.getValue().equals(path))
						return e.getKey();
				}
				try {
					«WorkspaceExtension.name» workspaceExtension = new «WorkspaceExtension.name»();
					«IWorkspaceRoot.name» root = workspaceExtension.getWorkspaceRoot();
					«List.name»<«IFile.name»> files = workspaceExtension.getFiles(root, f -> f.getFullPath().toString().contains(path));
					if (files.size() == 1) {
						«IFile.name» f = files.get(0);
						«URL.name» url = f.getLocationURI().toURL();
						if (images.get(path) == null) {
							addImage(path, url.toString());
							addImageFilePath(path, url.toString());
						}
					}  else {
						java.io.File f = new «File.name»(path);
						if (f.exists()) {
							addImage(path, f.toURI().toURL().toString());
						}
					}
				} catch («MalformedURLException.name» e) {
					e.printStackTrace();
				}
				return path;
			}
			
		    /**
		     * Adds available images if the 'ImageFilePath' is is null
		     * If the HashTable 'images' is null a new one will be created
		    */
			@Override
			protected void addAvailableImages() {
				if (images == null) {
					images = new «Hashtable.name»<«String.name», «String.name»>();
				}
				for («Entry.entryName»<«String.name», «String.name»> e : images.entrySet()) {
					if (getImageFilePath(e.getKey()) == null)
						addImageFilePath(e.getKey(), e.getValue());
				}
			}
			
			/**
			 * Each image is logged in by adding the images and creating the related path.
			*/
			public void initImages() {
				«Bundle.name» b = «Platform.name».getBundle("«ProjectCreator.getProjectSymbolicName(ProjectCreator.getProject(mgl.eResource()))».editor.graphiti");
				«File.name» file;
				«URL.name» url = null;
				
				//Search for all used images in graphmodel and register them in the image provider
				«FOR gm : mgl.graphModels»
					«FOR entry : MGLUtil::getAllImages(gm).entrySet»
						url =  «FileLocator.name».find(b, new «Path.name»("«entry.key»"), null);
						addImage("«entry.key»", url.toString());		
					«ENDFOR»
				«ENDFOR»
				b = «Platform.name».getBundle("de.jabc.cinco.meta.core.ge.style.generator.runtime");
				url =  «FileLocator.name».find(b, new «Path.name»("/icons/_Connection.png"), null);
				addImage("_Connection.png", url.toString());
			
				addAvailableImages();
			}
		}

	'''
	/**
	 * Auxiliary method to get all nodes with the annotation "icon"
	 * @param gm : The Graphmodel
	 * @return Returns a list of all found nodes with the annotation "icon"
	 */
	def static EList<ModelElement> getIconNodes(GraphModel gm) {
		var EList<ModelElement> foundNodes = new BasicEList<ModelElement>();
		var List<Node> nodes = MGLUtil.nodes(gm).toList;
		for (node : nodes) {
			var EList<Annotation> annots = node.annotations;
			for (annot : annots) {
				if (annot.name.equals("icon")) {
					foundNodes.add(node);
				}
			}
		}
		if (!gm.iconPath.nullOrEmpty)
			foundNodes.add(gm);
		
		return foundNodes;
	}

}

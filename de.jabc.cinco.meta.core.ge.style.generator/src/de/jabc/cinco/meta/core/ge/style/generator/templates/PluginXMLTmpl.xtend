/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import java.util.ArrayList
import java.util.List
import mgl.MGLModel

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class PluginXMLTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance

	MGLModel mgl
	String mglName
	String pkgName
	String nsUri

	def generatePluginXML(MGLModel mgl){
		this.mgl = mgl
		mglName = mgl.fileName
		pkgName = mgl.packageName.toString
		this.nsUri = mgl.nsURI
		return extensions
	}
	
	def List<CharSequence> extensions() {
		var List<CharSequence> exts = new ArrayList
		exts.addAll(
			diagramTypeProviders, 
			diagramTypes,
			editors,
			imageProvider,
			wizards,
			contentTypes,
			navigatorContent,
			referenceRegistry,
			contentParser,
			factoryOverride,
			resourceContributor
		);
		
		return exts
	}
	
	def getCode() '''
	«diagramTypeProviders»
	«diagramTypes»
	«editors»
	«imageProvider»
	«wizards»
	«contentTypes»
	«navigatorContent»
	«referenceRegistry»
	«perspectives»
	'''
	
	def diagramTypeProviders() {
		var result = '''
			<extension
					point="org.eclipse.graphiti.ui.diagramTypeProviders">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += diagramTypeProviders(gm.packageName.toString, gm.name)
		}
		return result + '''</extension>'''
	}
	
	def diagramTypeProviders(String pkgName, String gmName) '''
		<diagramTypeProvider
			class="«pkgName».«gmName»DiagramTypeProvider"
			description="This is the generated editor for the «gmName»"
			id="«pkgName».«gmName»DiagramTypeProvider"
			name=".«gmName» Diagram Editor">
			<diagramType
				id="«pkgName».«gmName»DiagramType">
			</diagramType>
			<imageProvider
				id="«mgl.packageName».«mglName»ImageProvider">
			</imageProvider>
		</diagramTypeProvider>
	'''
	
	def diagramTypes() {
		var result = '''
		<extension 
				point="org.eclipse.graphiti.ui.diagramTypes">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += diagramTypes(gm.packageName.toString, gm.name)
		}
		return result + '''</extension>''' 
	}
	
	def diagramTypes(String pkgName, String gmName) '''
	
		<!--@CincoGen «gmName»-->
			<diagramType
				description="This is the generated diagram Type for «gmName»"
				id="«pkgName».«gmName»DiagramType"
				name="«gmName» Graphiti Diagram Type"
				type="«gmName»">
			</diagramType>
	'''
	
	def imageProvider() {
		imageProvider(pkgName, mglName)
	}
	
	def imageProvider(String pkgName, String gmName)'''
		<extension
			point="org.eclipse.graphiti.ui.imageProviders">
			<!--@CincoGen «gmName»-->
			<imageProvider
				class="«pkgName».«gmName»ImageProvider"
				id="«pkgName».«gmName»ImageProvider">
			</imageProvider>
		</extension>
	'''
	
	def wizards() {
		var result = '''
		<extension
				point="org.eclipse.ui.newWizards">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += wizard(
					gm.wizardClass ?: '''«gm.packageName.toString».wizard.«gm.name»DiagramWizard''',
					gm.packageName.toString, gm.name, gm.iconPath,
					gm.wizardLabel ?: '''New «gm.name»'''
				)
		}
		return result + '''</extension>'''
	}
	
	def wizard(String className, String pkgName, String gmName, String icon, String name) '''
		<!--@CincoGen «gmName»-->
		<wizard
			category="de.jabc.cinco.meta.core.wizards.category.cinco"
			class="«className»"
			«IF !icon.nullOrEmpty»
			icon="«icon»"
			«ENDIF»
			id="«pkgName».wizard.«gmName.toLowerCase»"
			name="«name»">
		</wizard>
	'''
	
	def editors() {
		var result = '''
			<extension
				point="org.eclipse.ui.editors">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += editors(gm.packageName.toString, gm.name, gm.iconPath)
		}
		return result + '''</extension>'''
	}
	
	def editors(String pkgName, String gmName, String icon)'''
		<!--@CincoGen «gmName»-->
		<editor
			class="«pkgName».«gmName»DiagramEditor"
			contributorClass="org.eclipse.graphiti.ui.editor.DiagramEditorActionBarContributor"
			default="false"
			«IF !icon.nullOrEmpty»
			icon="«icon»"
			«ENDIF»
			id="«pkgName».«gmName»Editor"
			matchingStrategy="org.eclipse.graphiti.ui.editor.DiagramEditorMatchingStrategy"
			name="«gmName» Editor">
			<contentTypeBinding
				contentTypeId="«pkgName».«gmName»ContentType">
			</contentTypeBinding>
			<contentTypeBinding
				contentTypeId="org.eclipse.graphiti.content.diagram">
			</contentTypeBinding>
		</editor>
	'''
	
	def contentTypes() {
		var result = '''
		<extension
				point="org.eclipse.core.contenttype.contentTypes">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += contentTypes(gm.packageName.toString, gm.name, gm.fileExtension)
		}
		return result + '''</extension>'''
	}
	
	def contentTypes(String pkgName, String gmName, String fileExtension) '''
		<!--@CincoGen «gmName»-->
		<content-type
			file-extensions="«fileExtension»"
			id="«pkgName».«gmName»ContentType"
			name="«gmName» Content Type"
			priority="normal">
		</content-type>
	'''
	
	//TODO modularization: Check way to handle icons
	def perspectives() {
		perspectives(pkgName, mglName, null)
	}
	
	def perspectives(String pkgName, String gmName, String icon)'''
		<extension
			point="org.eclipse.ui.perspectives">
		<!--@CincoGen «gmName»-->
			<perspective
				class="«pkgName».«gmName»PerspectiveFactory"
				fixed="false"
				id="«pkgName».«gmName.toLowerCase»perspective"
				«IF !icon.nullOrEmpty»
				icon="«icon»"
				«ENDIF»
				name="«gmName» Perspective">
			</perspective>
		</extension>
	'''
	
	def navigatorContent() {
		var result = '''
		<extension
				point="org.eclipse.ui.navigator.navigatorContent">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += navigatorContent(gm.packageName.toString, gm.name)
		}
		return result + '''</extension>'''
	}
		
	def navigatorContent(String pkgName, String gmName)'''
		<!--@CincoGen «gmName»-->
		<commonWizard
			menuGroupId="mgl"
			type="new"
			wizardId="«pkgName».wizard.«gmName.toLowerCase»">
			<enablement></enablement>
		</commonWizard>
	'''
	
	def referenceRegistry() {
		var result = '''
		<extension
				point="de.jabc.cinco.meta.core.referenceregistry">
		'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += referenceRegistry(gm.packageName.toString, gm.name)
		}
		return result + '''</extension>'''
	}
	
	def referenceRegistry(String pkgName, String gmName) '''
		<!--@CincoGen «gmName»-->
		<FileExtensionsRegistry
			class="«pkgName».«gmName»FileExtensions">
		</FileExtensionsRegistry>
	'''
	
	def contentParser()  {
		var result = '''
		<extension
			      point="org.eclipse.emf.ecore.content_parser">
      	'''
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			result += contentParser(gm.packageName.toString, gm.name)
		}
		return result + '''</extension>'''
	}
	
	def contentParser(String pkgName, String gmName) '''
		<!--@CincoGen «gmName»-->
		<parser
		class="«pkgName».«gmName»APIParser"
			contentTypeIdentifier="«pkgName».«gmName»ContentType">
		</parser>
	'''
	
	def factoryOverride() {
		factoryOverride(pkgName, mglName)
	}
	
	def factoryOverride(String pkgName, String gmName) '''
		<extension
			point="org.eclipse.emf.ecore.factory_override">
				<!--@CincoGen «gmName»-->
		   		<factory
		        	class="«pkgName».«gmName»Factory"
		        	uri="«nsUri»">
		   		</factory>
		</extension>
	'''
	
	def resourceContributor() {
		resourceContributor(pkgName)
	}
	
	def resourceContributor(String pkgName) '''
		«FOR gm : mgl.graphModels.filter[!isAbstract]»
			<extension
			      id="«pkgName».«gm.name.toFirstUpper»ResourceContributor"
			      name="«gm.name.toFirstUpper» Resource Contributor"
			      point="de.jabc.cinco.meta.core.ui.ResourceContributor">
			<!--@CincoGen «gm.name.toFirstUpper»-->
			   <Contributor
			         class="«pkgName».«gm.name.toFirstUpper»ResourceContributor">
			   </Contributor>
			</extension>
		«ENDFOR»
	'''
	
	
}

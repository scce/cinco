/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.GraphModel
import mgl.MGLModel
import org.eclipse.graphiti.ui.internal.editor.ThumbNailView
import org.eclipse.ui.IFolderLayout
import org.eclipse.ui.IPageLayout
import org.eclipse.ui.IPerspectiveFactory

class PerspectiveTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance


	/** Generates the {@link IPerspectiveFactory} class. 
	 * 
	 * @param the processed {@link GraphModel}
	 */
	def generatePerspective(MGLModel mgl)'''
		package «mgl.packageName»;
		
		public class «mgl.fuName»PerspectiveFactory implements «IPerspectiveFactory.name» {
		
			public static final «String.name» ID_PERSPECTIVE = "«mgl.package».«mgl.fileName.toLowerCase»perspective";
		
			@Override
			public void createInitialLayout(«IPageLayout.name» layout) {
				layout.addView(«IPageLayout.name».ID_PROJECT_EXPLORER, «IPageLayout.name».LEFT, 0.25f, «IPageLayout.name».ID_EDITOR_AREA); 
				layout.addView(«ThumbNailView.name».VIEW_ID, «IPageLayout.name».BOTTOM, 0.55f, «IPageLayout.name».ID_PROJECT_EXPLORER);
				
				«IFolderLayout.name» folderLayout = layout.createFolder("«mgl.package».«mgl.fileName.toLowerCase».property", «IPageLayout.name».BOTTOM, 0.75f, «IPageLayout.name».ID_EDITOR_AREA);
				
				/** This command adds the common property view **/
				/*folderLayout.addView(«IPageLayout.name».ID_PROP_SHEET);*/
				folderLayout.addView("de.jabc.cinco.meta.core.ui.propertyview");
				folderLayout.addView(«IPageLayout.name».ID_PROBLEM_VIEW);
			}
		
		}

	'''
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.ui.properties.CincoPropertyView
import de.jabc.cinco.meta.core.ui.properties.IValuesProposalProvider
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import mgl.Edge
import mgl.GraphModel
import mgl.UserDefinedType
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.*
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class PropertyViewTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension APIUtils = new APIUtils
	extension EventApiExtension = new EventApiExtension

	/**
	 * Generates the code which registers the {@link EStructuralFeature}s for the 
	 * generic {@link CincoPropertyView}
	 * 
	 * @param gm The processed {@link GraphModel}
	 */	
	def generatePropertyView(GraphModel gm)'''
		package «gm.packageName».property.view;
		
		import java.util.function.Consumer;
		import org.eclipse.emf.ecore.EObject;
		import org.eclipse.xtext.xbase.lib.Functions.Function3;
		import org.eclipse.xtext.xbase.lib.Procedures.Procedure3;
		
		public class «gm.fuName»PropertyView implements «IValuesProposalProvider.name» {
			
			«FOR me: gm.modelElements.filter[findAnnotationPostSelect !== null]»
				«val postSelectClass = me.findAnnotationPostSelect.value.head»
				static Consumer<EObject> postSelect«me.fuName» = (bo) -> {
					if (bo instanceof «me.fqBeanName») {
						new «postSelectClass»().postSelect((«me.fqBeanName») bo);
					}
				};
			«ENDFOR»
			
			static Consumer<EObject> postSelectEvents = (bo) -> {
				«val postSelectEventEnabledElements = gm.getEventEnabledElements(EventEnum.POST_SELECT)»
				«IF !postSelectEventEnabledElements.nullOrEmpty»
					«toNonInternalElement('bo')»
					«postSelectEventEnabledElements
						.sortByInheritance
						.reverse
						.ifElseCascade(
							[ me | me.instanceofCheck('bo') ],
							[ me | '''
								«me.fqBeanName» element = («me.fqBeanName») bo;
								if (element.getRootElement() instanceof «gm.fqCName») {
									«EventEnum.POST_SELECT.getNotifyCallJava(me, 'element')»
								}
							''']
						)
					»
				«ENDIF»
			};
			
			static Function3<EObject, String, Object, String> canAttributeChangeEvents = (bo, attrName, value) -> {
				«val canAttributeChangeEventEnabledElements = gm.getEventEnabledElements(EventEnum.CAN_ATTRIBUTE_CHANGE)»
				«IF canAttributeChangeEventEnabledElements.nullOrEmpty»
					return null;
				«ELSE»
					«toNonInternalElement('bo')»
					«canAttributeChangeEventEnabledElements
						.sortByInheritance
						.reverse
						.ifElseCascade(
							[ me | me.instanceofCheck('bo') ],
							[ me | '''
								«me.fqBeanName» element = («me.fqBeanName») bo;
								«EventEnum.CAN_ATTRIBUTE_CHANGE.getNotifyCallJava('''canAttributeChange«me.fuName»EventResult''', me, 'element', 'attrName', 'value')»
								return canAttributeChange«me.fuName»EventResult;
							'''],
							['''
								return null;
							''']
						)
					»
				«ENDIF»
			};
			
			static Procedure3<EObject, String, Object> preAttributeChangeEvents = (bo, attrName, value) -> {
				«val preAttributeChangeEventEnabledElements = gm.getEventEnabledElements(EventEnum.PRE_ATTRIBUTE_CHANGE)»
				«IF !preAttributeChangeEventEnabledElements.nullOrEmpty»
					«toNonInternalElement('bo')»
					«preAttributeChangeEventEnabledElements
						.sortByInheritance
						.reverse
						.ifElseCascade(
							[ me | me.instanceofCheck('bo') ],
							[ me | '''
								«me.fqBeanName» element = («me.fqBeanName») bo;
								«EventEnum.PRE_ATTRIBUTE_CHANGE.getNotifyCallJava(me, 'element', 'attrName', 'value')»
							''']
						)
					»
				«ENDIF»
			};
			
			static Consumer<EObject> refreshPossibleValues = (bo) -> new «gm.fuName»PropertyView().refreshValues(bo);
			
			public static void initEStructuralFeatureInformation() {
				
				«val allNonHiddenGraphModelAttributes = gm.attributes.reject[isAttributeHidden]»
				«CincoPropertyView.name».init_EStructuralFeatures(
					«gm.beanPackage».internal.impl.Internal«gm.fuName»Impl.class«IF !allNonHiddenGraphModelAttributes.empty»,«ENDIF»
					«FOR attr: allNonHiddenGraphModelAttributes SEPARATOR ","»
						«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«gm.fuName»_«attr.name.toFirstUpper»()
					«ENDFOR»
				);
				
				«val usableNodes = gm.getUsableNodes(true)»
				«val usableNodesAndSuperNodes = (usableNodes + usableNodes.flatMap[allSuperNodes]).toSet»
				«val usableEdges = gm.usableEdges»
				«val usableEdgesAndSuperEdges = (usableEdges + usableEdges.map[allSuperEdges].flatten).toSet»
				«val usableUserDefiendTypes = gm.getUsableUserDefinedTypes(false, true)»
				«val usableUserDefiendTypesAndSuperUserDefiendTypes = gm.getUsableUserDefinedTypes(true, true)»
				«val usableModelElements = (usableNodes + usableEdges + usableUserDefiendTypes).toSet»
				«FOR edge: (usableNodesAndSuperNodes + usableEdgesAndSuperEdges)»
					«val allNonHiddenAttributes = edge.allAttributes(false).reject[isAttributeHidden]»
					«CincoPropertyView.name».init_EStructuralFeatures(
						«edge.beanPackage».internal.impl.Internal«edge.fuName»Impl.class«IF !allNonHiddenAttributes.empty»,«ENDIF»
						«FOR attr: allNonHiddenAttributes SEPARATOR ","»
							«edge.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«edge.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»
					);
				«ENDFOR»
				
				«FOR udType: usableUserDefiendTypesAndSuperUserDefiendTypes»
					«val allNonHiddenAttributes = udType.allAttributes(false).reject[isAttributeHidden]»
					«CincoPropertyView.name».init_EStructuralFeatures(
						«udType.beanPackage».internal.impl.Internal«udType.fuName»Impl.class«IF !allNonHiddenAttributes.empty»,«ENDIF»
						«FOR attr: allNonHiddenAttributes SEPARATOR ","»
							«udType.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«udType.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»
					);
				«ENDFOR»
		
				«CincoPropertyView.name».init_MultiLineAttributes(
					«FOR attr: gm.allModelAttributes.filter[isAttributeMultiLine] SEPARATOR ","»
						«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
					«ENDFOR»			
				);
		
				«CincoPropertyView.name».init_ReadOnlyAttributes(
					«FOR me: gm.modelElements.filter[allAttributes(false).exists[isAttributeReadOnly]] SEPARATOR ","»
						«FOR attr: me.allAttributes(false).filter[isAttributeReadOnly] SEPARATOR ","»
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«me.fuName»_«attr.name.toFirstUpper»()
						«ENDFOR»
					«ENDFOR»
				);
				
				«CincoPropertyView.name».init_FileAttributes(
					«FOR attr: gm.allModelAttributes.filter[isAttributeFile] SEPARATOR ","»
						«attr.modelElement.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
					«ENDFOR»                        
				);
				
				«IF gm.allModelAttributes.exists[isAttributeFile]»
					«FOR attr: gm.allModelAttributes.filter[isAttributeFile]»
						«CincoPropertyView.name».init_FileAttributesExtensionFilters(
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
							«attr.annotations.filter[name == "file"].flatMap[value].join(" ") [ ext | ''',"«ext»"''' ]»
						);
					«ENDFOR»
				«ENDIF»
			
				«CincoPropertyView.name».init_ColorAttributes(
					«FOR attr: gm.allModelAttributes.filter[isAttributeColor] SEPARATOR ","»
						«attr.modelElement.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»()
					«ENDFOR»                        
				);
		
				«IF gm.allModelAttributes.exists[isAttributeColor]»
					«FOR attr: gm.allModelAttributes.filter[isAttributeColor]»
						«CincoPropertyView.name».init_ColorAttributesParameter(
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.fuName»_«attr.name.toFirstUpper»(),
							"«attr.annotations.filter[name == "color"].map[value].head.head»"
						);
					«ENDFOR»
				«ENDIF»
				
				«FOR udType: usableUserDefiendTypes.filter[hasLabel]»
					«CincoPropertyView.name».init_TypeLabel(
						«gm.beanPackage».internal.impl.Internal«udType.fuName»Impl.class,
						«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«udType.fuName»_«udType.annotations.filter[name == "label"].head.value.head.toFirstUpper»()
					);
				«ENDFOR»
				
				«FOR attr: gm.allModelAttributes.filter[isGrammarAttribute]»
					«FOR subType: attr.modelElement.allSubclasses + #[attr.modelElement]»
						«CincoPropertyView.name».init_GrammarEditor(
							«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«subType.fuName»_«attr.name.toFirstUpper»(),
							«attr.annotations.filter[name == "grammar"].head.value.get(1)».getInstance().getInjector("«attr.annotations.filter[name == "grammar"].head.value.head»")
						);
					«ENDFOR»
				«ENDFOR»
				
				«CincoPropertyView.name».init_DisableCreate(
					«FOR me: usableModelElements.filter[hasAnnotationDisableCreate] SEPARATOR ","»
						// The use of non-internal EClasses is intentional.
						«val underscore = if (me.fuName == "Name") "_" /* See issue #386 https://gitlab.com/scce/cinco/-/issues/386 */»
						«me.beanPackage».«gm.model.fileName.toLowerCase.toFirstUpper»Package.eINSTANCE.get«me.fuName»«underscore»()
					«ENDFOR»
				);
				«CincoPropertyView.name».init_DisableDelete(
					«FOR me: usableModelElements.filter[hasAnnotationDisableDelete] SEPARATOR ","»
						// The use of internal EClasses is intentional.
						«me.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«me.fuName»()
					«ENDFOR»
				);
				
				«FOR me: gm.modelElements.reject[findAnnotationPostSelect === null]»
					«CincoPropertyView.name».postSelects.add(postSelect«me.fuName»);
				«ENDFOR»
				
				«CincoPropertyView.name».postSelects.add(postSelectEvents);
				«CincoPropertyView.name».canAttributeChange = canAttributeChangeEvents;
				«CincoPropertyView.name».preAttributeChange = preAttributeChangeEvents;
				
				«CincoPropertyView.name».possibleValueRefreshs.add(refreshPossibleValues);
				
				«CincoPropertyView.name».assertSelectionListener();
			}
			
			@Override
			public void refreshValues(«EObject.name» bo) {
				«FOR attr: gm.allModelAttributes.filter[isAttributePossibleValuesProvider]»
					«FOR element: attr.modelElement.allSubclasses + #{attr.modelElement}»
						if (bo instanceof «attr.modelElement.fqBeanName»)
							«CincoPropertyView.name».refreshPossibleValues(
								«gm.beanPackage».internal.InternalPackage.eINSTANCE.getInternal«attr.modelElement.name»_«attr.name.toFirstUpper»(),
								new «attr.getPossibleValuesProviderClass»().getPossibleValues((«attr.modelElement.fqBeanName») bo)
							);
					«ENDFOR»
				«ENDFOR»
			}
			
		}
	'''
	
}

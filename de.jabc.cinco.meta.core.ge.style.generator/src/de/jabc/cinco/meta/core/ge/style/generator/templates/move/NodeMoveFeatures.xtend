/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.move

import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoMoveShapeFeature
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.internal.InternalModelElementContainer
import mgl.ModelElement
import mgl.Node
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IMoveShapeContext
import style.Styles

class NodeMoveFeatures {
	
	extension APIUtils = new APIUtils
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension
	
	/**
	 * Generates the 'Move-Feature' for a given node
	 * @param n : The node
	 * @param style : The style
	 */
	def doGenerateNodeMoveFeature(Node n, Styles styles)'''
		package «n.packageNameMove»;
		
		class MoveFeature«n.fuName» extends «CincoMoveShapeFeature.name» {
			
			var «ECincoError.name» error = «ECincoError.name».OK
			
			/**
			 * Call of the Superclass
			 * @param fp: Fp is the parameter of the Superclass-Call
			*/
			new(«IFeatureProvider.name» fp) {
				super(fp)
			}
			
			/**
			 * Checks if a shape is moveable
			 * @param context : Contains the information, needed to let a feature move a shape
			 * @param apiCall : Apicall shows if the Cinco Api is used
			 * @return Returns true if a shape can be moved and false if not
			*/
			override boolean canMoveShape(«IMoveShapeContext.name» context, boolean apiCall) {
				if (apiCall) {
					val source = context.sourceContainer.businessObjectForPictogramElement
					val target = context.targetContainer.businessObjectForPictogramElement
					«IF n.isFixed(styles)»
						return false
					«ELSE»
						if (target instanceof «InternalModelElementContainer.name») {
							if (target == source) {
								return true
							}
							if (target.canContain(«n.fqBeanName»)«IF n.eventEnabled» && canMove«n.fuName»Event(context, target)«ENDIF») {
								return true
							}
						}
						if (error === «ECincoError.name».OK) {
							error = «ECincoError.name».INVALID_CONTAINER
						}
						return false
					«ENDIF»
				}
				return false
			}
			
			/**
			 * Checks if a shape is moveable by using the method 'canMoveShape(context,apiCall)'
			 * @param context : Contains the information, needed to let a feature move a shape
			 * @return Returns true if a shape can be moved and false if not
			 */
			override boolean canMoveShape(«IMoveShapeContext.name» context) {
				return canMoveShape(context, «!CincoUtil.isMoveDisabled(n)»)
			}
			
			«IF n.eventEnabled»
				def private boolean canMove«n.fuName»Event(«IMoveShapeContext.name» context, «InternalModelElementContainer.name» target) {
					val bo = context.shape.businessObjectForPictogramElement
					if (bo instanceof «n.fqInternalBeanName») {
						val element = bo.element
						if (element instanceof «n.fqBeanName») {
							«EventEnum.CAN_MOVE.getNotifyCallXtend('''canMove«n.fuName»EventResult''', n, 'element', 'target.containerElement', 'context.x', 'context.y')»
							return canMove«n.fuName»EventResult?: true
						}
					}
					return false
				}
				
			«ENDIF»
			/**
			 * Moves a Shape by removing the shape at the source and adding it at the target
			 * @param context : Contains the information, needed to let a feature move a shape
			 */
			override void moveShape(«IMoveShapeContext.name» context) {
				val o = context.shape.businessObjectForPictogramElement as «n.fqInternalBeanName»
				val target = context.targetContainer.businessObjectForPictogramElement as «InternalModelElementContainer.name»
				
				super.moveShape(context)
				
				if (o.element instanceof «n.fqCName») {
					if(!moveContainer(target, o.element as «n.fqBeanName», context)) {
						(o.element as «n.fqBeanName»)
							.moveTo(target.containerElement, context.x, context.y)
					}
				}
			}
			
			def dispatch moveContainer(«InternalModelElementContainer.name» target, «n.fqBeanName» oe, «IMoveShapeContext.name» context) {
				return false
			}
			
			«FOR pc : n.possibleContainers»
				def dispatch moveContainer(«(pc as ModelElement).fqInternalBeanName» target, «n.fqBeanName» oe, «IMoveShapeContext.name» context) {
					oe.moveTo(target.containerElement as «pc.fqBeanName», context.x, context.y)
					return true
				}
			«ENDFOR»
			
			/**
			 * Get-method for an error
			 * @return Returns an 'error' in which 'error' is  'ECincoError.OK'
			 */
			override «ECincoError.name» getError() {
				return error
			}
			
			/**
			 * Set-method for an error
			 * @param error : Error is a value of the enum: MAX_CARDINALITY, MAX_IN, MAX_OUT, INVALID_SOURCE, INVALID_TARGET, INVALID_CONTAINER, INVALID_CLONE_TARGET, OK
			 */
			def void setError(«ECincoError.name» error) {
				this.error = error
			}
			
		}
	'''
	/**
	 * Auxiliary method to check if a style of a node is fixed
	 * @param n : The node
	 * @param style : The style
	 * @return Returns true if the style of a node is fixed and false if not
	 */
	def isFixed(Node n, Styles styles)
	{
		var style = CincoUtil.getStyleForNode(n, styles);
		return style.fixed;
	}
}

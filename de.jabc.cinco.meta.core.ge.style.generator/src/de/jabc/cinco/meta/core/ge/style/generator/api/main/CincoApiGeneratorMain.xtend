/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.api.main

import de.jabc.cinco.meta.core.ge.style.generator.api.templates.CEdgeTmpl
import de.jabc.cinco.meta.core.ge.style.generator.api.templates.CGraphModelTmpl
import de.jabc.cinco.meta.core.ge.style.generator.api.templates.CNodeTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.core.utils.projects.ContentWriter
import mgl.MGLModel
import mgl.ModelElement
import org.eclipse.core.resources.IProject

class CincoApiGeneratorMain extends APIUtils {
	
	extension CNodeTmpl = new CNodeTmpl
	extension CEdgeTmpl = new CEdgeTmpl
	extension CGraphModelTmpl = new CGraphModelTmpl
	extension GeneratorUtils = GeneratorUtils.instance
	
	var MGLModel mgl
	
	new (MGLModel mglModel) {
		this.mgl = mglModel
	}
	
	def mglModel(ModelElement m){
		m.eContainer as MGLModel
	}
	
	def doGenerate(IProject project) {
		var CharSequence content = null
		
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			content = gm.doGenerateImpl
			ContentWriter::writeJavaFileInSrcGen(project, mgl.packageNameAPI, gm.fuCName.concat(".java"), content)
		}
		
		for (n : mgl.nodes) {
			content = n.doGenerateImpl
			ContentWriter::writeJavaFileInSrcGen(project, mgl.packageNameAPI, n.fuCName.concat(".java"), content)
		}
		
		for (e : mgl.edges) {
			content = e.doGenerateImpl
			ContentWriter::writeJavaFileInSrcGen(project, mgl.packageNameAPI, e.fuCName.concat(".java"), content)
		}
	}
	
}

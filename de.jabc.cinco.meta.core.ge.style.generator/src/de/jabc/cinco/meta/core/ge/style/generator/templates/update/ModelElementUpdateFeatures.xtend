/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.update

import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoLayoutFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoUpdateFeature
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import graphmodel.IdentifiableElement
import mgl.ModelElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.gef.EditPart
import org.eclipse.gef.GraphicalEditPart
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.mm.pictograms.Connection
import org.eclipse.graphiti.mm.pictograms.ContainerShape
import org.eclipse.graphiti.mm.pictograms.Diagram
import org.eclipse.graphiti.mm.pictograms.Shape
import org.eclipse.graphiti.services.Graphiti
import org.eclipse.graphiti.ui.editor.DiagramBehavior
import style.Styles

class ModelElementUpdateFeatures {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	/**
	 * Generates the Class 'UpdateFeature' for the Node n
	 * @param n : The node
	 * @param styles : Styles
	 */
	def doGenerateModelElementUpdateFeature(ModelElement me, Styles styles)'''
		package «me.packageNameUpdate»;
		
		public class UpdateFeature«me.fuName» extends «CincoUpdateFeature.name» {
			
			public UpdateFeature«me.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
		
			@Override
			protected void updateStyle(«EObject.name» bo, «Shape.name» s) {
			«IF CincoUtil::hasAppearanceProvider(me)»
				«Diagram.name» d = getDiagram();
				if (bo instanceof «IdentifiableElement.name»)
					bo = ((«IdentifiableElement.name») bo).getInternalElement_();
				«String.name» gaName = «Graphiti.name».getPeService().getPropertyValue(s.getGraphicsAlgorithm(), «CincoLayoutFeature.name».KEY_GA_NAME);
				if (gaName != null && !gaName.isEmpty() && d != null) {
					«me.MGLModel.packageName».«me.MGLModel.fuName»LayoutUtils
						.updateStyleFromAppearance(
							new «CincoUtil::getAppearanceProvider(me)»().getAppearance(
								(«me.fqBeanName»)((«me.fqInternalBeanName») bo).getElement(), gaName),s.getGraphicsAlgorithm(), d);
				}
				«Object.name» object = «Graphiti.name».getLinkService().getBusinessObjectForLinkedPictogramElement(s);
				if («me.instanceofCheck("object")») {
					object = ((«me.fqBeanName») object).getInternalElement_();
					if (s instanceof «ContainerShape.name») {
						for («Shape.name» g : ((«ContainerShape.name») s).getChildren()) {
							if (((«me.fqInternalBeanName») object).getElement().equals(g.getLink().getBusinessObjects().get(0)))
								updateStyle((«me.fqInternalBeanName») object, g);
						}
					}
				}
			«ENDIF»
			}
			
			@Override
			protected void updateStyle(«EObject.name» bo, «Connection.name» s) {
				«IF CincoUtil::hasAppearanceProvider(me)»
					«Diagram.name» d = getDiagram();
					if (bo instanceof «IdentifiableElement.name»)
						bo = ((«IdentifiableElement.name») bo).getInternalElement_();
					String gaName = «Graphiti.name».getPeService().getPropertyValue(s.getGraphicsAlgorithm(), «CincoLayoutFeature.name».KEY_GA_NAME);
					if (gaName != null && !gaName.isEmpty() && d != null) {
						«me.MGLModel.packageName».«me.MGLModel.fuName»LayoutUtils.
							updateStyleFromAppearance(
								new «CincoUtil::getAppearanceProvider(me)»().getAppearance(
									(«me.fqBeanName»)((«me.fqInternalBeanName») bo).getElement(), gaName), s.getGraphicsAlgorithm(), d);
					}
					«Object.name» object = «Graphiti.name».getLinkService().getBusinessObjectForLinkedPictogramElement(s);
					if («me.instanceofCheck("object")») {
						object = ((«me.fqBeanName») object).getInternalElement_();
						for («Shape.name» g : s.getConnectionDecorators()) {
							updateStyle((«me.fqInternalBeanName») object, g);
						}
						
					}
					
					if (!(getDiagramBehavior() instanceof «DiagramBehavior.name»))
						return;
					«DiagramBehavior.name» db = («DiagramBehavior.name») getDiagramBehavior();
					if (db == null)
						return;
					«GraphicalEditPart.name» editPart = db.getEditPartForPictogramElement(s);
					if (editPart == null)
						return;
					int selected_info = editPart.getSelected();
					editPart.setSelected(«EditPart.name».SELECTED_NONE);
					editPart.setSelected(selected_info);
				«ENDIF»
			}
		}
	'''

}

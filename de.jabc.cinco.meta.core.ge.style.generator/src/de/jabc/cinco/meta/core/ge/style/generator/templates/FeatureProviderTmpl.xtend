/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.runtime.addfeature.LibraryComponentAddFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.customfeature.GraphitiCustomFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoAddBendpointFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoAddFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoCopyFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoLayoutFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoMoveBendpointFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoMoveConnectionDecoratorFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoPasteFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoRemoveBendpointFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.provider.CincoFeatureProvider
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.MGLUtil
import graphmodel.IdentifiableElement
import graphmodel.internal.InternalGraphModel
import graphmodel.internal.InternalIdentifiableElement
import graphmodel.internal.InternalModelElement
import graphmodel.internal.InternalNode
import java.util.HashSet
import mgl.GraphModel
import org.eclipse.core.resources.IFile
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.graphiti.dt.IDiagramTypeProvider
import org.eclipse.graphiti.features.IAddBendpointFeature
import org.eclipse.graphiti.features.IAddFeature
import org.eclipse.graphiti.features.ICopyFeature
import org.eclipse.graphiti.features.ICreateConnectionFeature
import org.eclipse.graphiti.features.ICreateFeature
import org.eclipse.graphiti.features.IDeleteFeature
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.ILayoutFeature
import org.eclipse.graphiti.features.IMoveBendpointFeature
import org.eclipse.graphiti.features.IMoveConnectionDecoratorFeature
import org.eclipse.graphiti.features.IMoveShapeFeature
import org.eclipse.graphiti.features.IPasteFeature
import org.eclipse.graphiti.features.IReconnectionFeature
import org.eclipse.graphiti.features.IRemoveBendpointFeature
import org.eclipse.graphiti.features.IRemoveFeature
import org.eclipse.graphiti.features.IResizeShapeFeature
import org.eclipse.graphiti.features.IUpdateFeature
import org.eclipse.graphiti.features.context.IAddBendpointContext
import org.eclipse.graphiti.features.context.IAddContext
import org.eclipse.graphiti.features.context.ICopyContext
import org.eclipse.graphiti.features.context.ICustomContext
import org.eclipse.graphiti.features.context.IDeleteContext
import org.eclipse.graphiti.features.context.ILayoutContext
import org.eclipse.graphiti.features.context.IMoveBendpointContext
import org.eclipse.graphiti.features.context.IMoveConnectionDecoratorContext
import org.eclipse.graphiti.features.context.IMoveShapeContext
import org.eclipse.graphiti.features.context.IPasteContext
import org.eclipse.graphiti.features.context.IReconnectionContext
import org.eclipse.graphiti.features.context.IRemoveBendpointContext
import org.eclipse.graphiti.features.context.IRemoveContext
import org.eclipse.graphiti.features.context.IResizeShapeContext
import org.eclipse.graphiti.features.context.IUpdateContext
import org.eclipse.graphiti.features.context.impl.AddContext
import org.eclipse.graphiti.features.custom.ICustomFeature
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.ui.features.DefaultFeatureProvider

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class FeatureProviderTmpl extends APIUtils {
		
	extension GeneratorUtils = GeneratorUtils.instance
		
	/**
	 * Generates the {@link IFeatureProvider} code for the Graphmodel
	 * 
	 * @param gm The processed {@link GraphModel}
	 */
	def generateFeatureProvider(GraphModel gm) {
		val allEdges = gm.getUsableEdges.toSet
		val allNodes = gm.getUsableNodes(true)
		val allModelElements = new HashSet()
		allModelElements.addAll(allEdges)
		allModelElements.addAll(allNodes)
		
		return '''
			package «gm.packageName»;
			
			import «gm.packageNameCreate».*;
			import «gm.packageNameAdd».*;
			
			public class «gm.fuName»FeatureProvider extends «DefaultFeatureProvider.name» implements «CincoFeatureProvider.name»{
				
				new («IDiagramTypeProvider.name» dtp) {
					super(dtp);
				}
				
				override «IAddFeature.name»[] getAllLibComponentAddFeatures() {
					return #[
						«FOR pn : allNodes.filter[isPrime && !it.isCreateDisabled] SEPARATOR ","»
							«pn.addFeaturePrimeCode»
						«ENDFOR»
					];
				}
				
				override «IAddFeature.name» getAddFeature(«IAddContext.name» context) {
					var o =  context.getNewObject();
					if (o instanceof «IFile.name») {
						o = getGraphModel(o as «IFile.name»);
						if (context instanceof «AddContext.name»)
							(context as «AddContext.name»).setNewObject(o);
					}
					if (o instanceof «EObject.name») {
						var bo = o as «EObject.name»;
						var element = bo;
						if (bo instanceof «InternalIdentifiableElement.name»)
							element = (bo as «InternalIdentifiableElement.name»).getElement();
										 
						var sameResource = true;
						if (bo.eResource() !== null) {
							sameResource = bo.eResource().equals(getDiagramTypeProvider().getDiagram().eResource());
						}
						
						val foundAddFeature = dispatchedGetAddFeature(bo);
						if (sameResource && foundAddFeature !== null) 
							return foundAddFeature;
						«FOR me : allNodes.filter[!isIsAbstract]»
							«IF isPrime(me)»
								if((element.eClass().getName().equals("«me.retrievePrimeReference.primeTypeElement»")
									|| (element.eClass().getEAllSuperTypes().stream().anyMatch[_superClass | _superClass.getName().equals("«me.retrievePrimeReference.primeTypeElement»")]))
									&& element.eClass().getEPackage().getNsURI().equals("«me.retrievePrimeReference.nsURI»")
									&& !sameResource)
									return new «LibraryComponentAddFeature.name»(this);
							«ENDIF»		
						«ENDFOR»
						
						if (foundAddFeature !== null)
							return foundAddFeature;
						
						if((element.eClass().getName().equals("EObject")
							|| «CincoAddFeature.name».getAllSuperInterfaces(element.getClass()).stream().anyMatch[_superClass | _superClass.getName().equals("«EObject.name»")])
							&& !sameResource)
							return new «LibraryComponentAddFeature.name»(this);
					}
			
					return super.getAddFeature(context);
				}
				
				«FOR me : allNodes.filter[!isIsAbstract]»
					def dispatch «IAddFeature.name» dispatchedGetAddFeature(«me.fqInternalBeanName» e) {
						return new «me.packageNameAdd».AddFeature«me.fuName»(this);
					}
				«ENDFOR»
				
				«FOR ed : allEdges.filter[!isIsAbstract]»
					def dispatch «IAddFeature.name» dispatchedGetAddFeature(«ed.fqInternalBeanName» e) {
						return new «ed.packageNameAdd».AddFeature«ed.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «IAddFeature.name» dispatchedGetAddFeature(Object e) {
					return null;
				}

				def private «Object.name» getGraphModel(«IFile.name» file) {
					val fileUri = «URI.name».createPlatformResourceURI(file.getFullPath().toString(), true);
					var res = new «ResourceSetImpl.name»().getResource(fileUri, true);
					if (res !== null) {
						for («EObject.name» o : res.getContents()) {
							if (o instanceof «InternalGraphModel.name») {
								return o;
							}
						}
					}
					return null;
				}
			
				override «ICreateFeature.name»[] getCreateFeatures() {
					return #[
					«FOR me : allNodes.filter[!isIsAbstract] SEPARATOR ","»
						new «me.packageNameCreate».CreateFeature«me.fuName»(this)
					«ENDFOR»
					];
				}
			
				override «ICreateConnectionFeature.name»[] getCreateConnectionFeatures() {
					return #[
					«FOR e : allEdges.filter[!isIsAbstract] SEPARATOR ","»
						new «e.packageNameCreate».CreateFeature«e.fuName»(this)	
					«ENDFOR»
					];
				}
			
				override «IDeleteFeature.name» getDeleteFeature(«IDeleteContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
					
					if(bo !== null) {
						val foundFeature = dispatchedGetDeleteFeature(bo);
						if(foundFeature !== null) {
							return foundFeature;
						}
					}
					
					return super.getDeleteFeature(context);
				}
				
				«FOR me : allModelElements.filter[!(it instanceof GraphModel) && !isIsAbstract]»
					def dispatch «IDeleteFeature.name» dispatchedGetDeleteFeature(«me.fqInternalBeanName» me) {
						return new «me.packageNameDelete».DeleteFeature«me.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «IDeleteFeature.name» dispatchedGetDeleteFeature(Object me) {
					return null;
				}
				
				override «ILayoutFeature.name» getLayoutFeature(«ILayoutContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
					
					if (bo instanceof «InternalModelElement.name»){
						
						if (bo instanceof «InternalNode.name»)
							return new «CincoLayoutFeature.name»(this);
					
						val foundFeature = dispatchedGetLayoutFeature(bo);
						if(foundFeature !== null) {
							return foundFeature;
						}
					}
			
					return super.getLayoutFeature(context);
				}
				
				«FOR e : allEdges.filter[!isIsAbstract]»
					def dispatch «ILayoutFeature.name» dispatchedGetLayoutFeature(«e.fqInternalBeanName» e) {
					    return new «e.packageNameLayout».LayoutFeature«e.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «ILayoutFeature.name» dispatchedGetLayoutFeature(Object e) {
					return null;
				}
				
				override «IResizeShapeFeature.name» getResizeShapeFeature(«IResizeShapeContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
					
					if (bo instanceof «InternalModelElement.name»){
						val foundFeature = dispatchedGetResizeShapeFeature(bo);
						if(foundFeature !== null) {
							return foundFeature;
						}
					}
			
					return super.getResizeShapeFeature(context);
				}
				
				«FOR n : allNodes.filter[!isIsAbstract]»
					def dispatch «IResizeShapeFeature.name» dispatchedGetResizeShapeFeature(«n.fqInternalBeanName» me) {
						return new «n.packageNameResize».ResizeFeature«n.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «IResizeShapeFeature.name» dispatchedGetResizeShapeFeature(Object me) {
					return null;
				}
				
				override «IMoveShapeFeature.name» getMoveShapeFeature(«IMoveShapeContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
					
					if (bo instanceof «InternalModelElement.name»){
						val foundFeature = dispatchedGetMoveShapeFeature(bo);
						if(foundFeature !== null) {
							return foundFeature;
						}
					}
			
					return super.getMoveShapeFeature(context);
				}
				
				«FOR n : allNodes.filter[!isIsAbstract]»
					def dispatch «IMoveShapeFeature.name» dispatchedGetMoveShapeFeature(«n.fqInternalBeanName» me) {
						return new «n.packageNameMove».MoveFeature«n.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «IMoveShapeFeature.name» dispatchedGetMoveShapeFeature(Object me) {
					return null;
				}
				
				override «IUpdateFeature.name» getUpdateFeature(«IUpdateContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
					
					if(bo !== null) {
						«/* Specific update feature needed due to appearance provider... */»
						val foundFeature = dispatchedGetUpdateFeature(bo);
						if(foundFeature !== null) {
							return foundFeature;
						}
					}
					
					return super.getUpdateFeature(context);
				}
				
				«FOR me : allModelElements.filter[!(it instanceof GraphModel) && !isIsAbstract]»
					def dispatch «IUpdateFeature.name» dispatchedGetUpdateFeature(«me.fqInternalBeanName» me) {
						return new «me.packageNameUpdate».UpdateFeature«me.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «IUpdateFeature.name» dispatchedGetUpdateFeature(Object me) {
					return null;
				}
			
			
				override «IReconnectionFeature.name» getReconnectionFeature(«IReconnectionContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getConnection());
					
					if(bo !== null) {
						val foundFeature = dispatchedGetReconnectionFeature(bo);
						if(foundFeature !== null) {
							return foundFeature;
						}
					}
					
					return super.getReconnectionFeature(context);
				}
				
				«FOR e : allEdges.filter[!isIsAbstract]»
					def dispatch «IReconnectionFeature.name» dispatchedGetReconnectionFeature(«e.fqInternalBeanName» me) {
						return new «e.packageNameReconnect».ReconnectFeature«e.fuName»(this);
					}
				«ENDFOR»
				
				def dispatch «IReconnectionFeature.name» dispatchedGetReconnectionFeature(Object me) {
					return null;
				}
					
				override «ICustomFeature.name»[] getCustomFeatures(«ICustomContext.name» context) {
					var bo = getBusinessObjectForPictogramElement(context.getPictogramElements().get(0));
					
					if (bo instanceof «InternalGraphModel.name») {
						var ime = bo as «InternalGraphModel.name»;
						if («gm.internalInstanceofCheck("ime")») {
							return #[
								«FOR annotValue : MGLUtil.getAllAnnotation("contextMenuAction", gm) SEPARATOR ","»
								new «GraphitiCustomFeature.name»(this,new «annotValue»())
								«ENDFOR»
							];
						}
					}
					
					if (bo instanceof «InternalModelElement.name») {
						var ime = bo as «InternalModelElement.name»;
						return dispatchedGetCustomFeatures(bo);
					}
					
					return #[];
				}
				
				«FOR me : allModelElements.filter[it instanceof GraphModel === false]»
					def dispatch «ICustomFeature.name»[] dispatchedGetCustomFeatures(«me.fqInternalBeanName» me) {
						return #[
							«FOR annotValue : MGLUtil.getAllAnnotation("contextMenuAction", me) SEPARATOR ","»
							new «GraphitiCustomFeature.name»(this,new «annotValue»())
							«ENDFOR»
						];
					}
				«ENDFOR»
				
				def dispatch «ICustomFeature.name»[] dispatchedGetCustomFeatures(Object me) {
					return #[];
				}
				
				override «IMoveBendpointFeature.name» getMoveBendpointFeature(«IMoveBendpointContext.name» context) {
					return new «CincoMoveBendpointFeature.name»(this);
				}
				
				override «IAddBendpointFeature.name» getAddBendpointFeature(«IAddBendpointContext.name» context) {
					return new «CincoAddBendpointFeature.name»(this);
				}
				
				override «IRemoveBendpointFeature.name» getRemoveBendpointFeature(«IRemoveBendpointContext.name» context) {
					return new «CincoRemoveBendpointFeature.name»(this);
				}
				
				override «IRemoveFeature.name» getRemoveFeature(«IRemoveContext.name» context) {
					return null;
				}
				
				override «IMoveConnectionDecoratorFeature.name» getMoveConnectionDecoratorFeature(«IMoveConnectionDecoratorContext.name» context) {
					return new «CincoMoveConnectionDecoratorFeature.name»(this);
				}
				
				override «ICopyFeature.name» getCopyFeature(«ICopyContext.name» context) {
					return new «CincoCopyFeature.name»(this);
				}
					
				override «IPasteFeature.name» getPasteFeature(«IPasteContext.name» context) {
					return new «CincoPasteFeature.name»(this);
				}
				
				override «Object.name» getBusinessObjectForPictogramElement(«PictogramElement.name» pictogramElement) {
					var bo = super.getBusinessObjectForPictogramElement(pictogramElement);
					if (bo instanceof «IdentifiableElement.name») {
						bo = (bo as «IdentifiableElement.name»).getInternalElement_();
					}
					return bo;
				}
			}
		'''
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.layout

import de.jabc.cinco.meta.core.ge.style.generator.runtime.utils.CincoLayoutUtils
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.Edge
import org.eclipse.graphiti.datatypes.IDimension
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.ILayoutContext
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature
import org.eclipse.graphiti.mm.algorithms.Text
import org.eclipse.graphiti.mm.pictograms.Connection
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import style.Styles

class EdgeLayoutFeatures extends APIUtils {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	/**
	 * Generates the Class 'LayoutFeature' for the Edge e
	 * @param e : The edge
	 * @param styles : Styles
	 */
	def doGenerateEdgeLayoutFeature(Edge e,Styles styles)'''
		package «e.packageNameLayout»;
		
		public class LayoutFeature«e.fuName» extends «AbstractLayoutFeature.name»{
			
			public LayoutFeature«e.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
			
			@Override
			public boolean canLayout(«ILayoutContext.name» context) {
				Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
				if (bo instanceof «e.fqInternalBeanName»)
					return true;
				return false;
			}
			
			@Override
			public boolean layout(«ILayoutContext.name» context) {
				«PictogramElement.name» pe = context.getPictogramElement();
				Object bo = getBusinessObjectForPictogramElement(pe);
				if (bo instanceof «e.fqInternalBeanName») {
					if (pe instanceof «Connection.name») {
						«Connection.name» conn = («Connection.name») pe;
						for («ConnectionDecorator.name» cd : conn.getConnectionDecorators()) {
							if (cd.getGraphicsAlgorithm() instanceof «Text.name») {
								«Text.name» t = («Text.name») cd.getGraphicsAlgorithm();
							    «IDimension.name» dim = «CincoLayoutUtils.name».getTextDimension(t);
							    «Graphiti.name».getGaService().setSize(t, dim.getWidth(), dim.getHeight());
							 }
						}
					}
				}
				return true;
			}
		}
	'''
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.GraphModel
import org.eclipse.core.resources.IFile
import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider
import org.eclipse.graphiti.dt.IDiagramTypeProvider
import org.eclipse.graphiti.tb.IToolBehaviorProvider
import org.eclipse.ui.IWorkbench
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.WorkbenchException
import productDefinition.CincoProduct

class DiagramTypeProviderTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	/**
	 * Generates the template for the {@link IDiagramTypeProvider}
	 * 
	 * @param gm The Graphmodel for information retrieval
	 */
	def generateDiagramTypeProvider(GraphModel gm, CincoProduct cp, IFile cpdFile)'''
		package «gm.packageName»;
			
		public class «gm.fuName»DiagramTypeProvider extends «AbstractDiagramTypeProvider.name»{
			
			private «IToolBehaviorProvider.name»[] tbProviders;
			
			public «gm.fuName»DiagramTypeProvider() {
				super();
				setFeatureProvider(new «gm.fuName»FeatureProvider(this));
				«MGLUtil::getMglModel(gm).packageName».«MGLUtil::getMglModel(gm).fuName»GraphitiUtils.getInstance().loadImages();
				«MGLUtil::getMglModel(gm).packageName».«MGLUtil::getMglModel(gm).fuName»GraphitiUtils.getInstance().setDTP(this);
				«gm.fqPropertyView».initEStructuralFeatureInformation();
			}
		
			@Override
			public «IToolBehaviorProvider.name»[] getAvailableToolBehaviorProviders() {
				if (tbProviders == null) {
					tbProviders = 
						new «IToolBehaviorProvider.name»[] {new «gm.fuName»ToolBehaviorProvider(this)};
				}
				return tbProviders;
			}
			
		}
	'''
	
	def getPerspectiveID(CincoProduct cp, GraphModel gm, IFile cpdFile) {
		if (!cp.defaultPerspective.nullOrEmpty)
			'''"«cp.defaultPerspective»"'''
		else '''«cpdFile.project.name».perspective.«cp.name»Perspective.ID_PERSPECTIVE'''
	}
	
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.GraphModel
import de.jabc.cinco.meta.core.ge.style.generator.runtime.editor.LazyDiagram

class DiagramTmpl {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	def generateDiagram(GraphModel gm) '''	
		package «gm.packageName»;
		
		public class «gm.fuName»Diagram extends «LazyDiagram.name» {
			
			public «gm.fuName»Diagram() {
				super("«gm.fuName»", "«gm.dtpId»");
			}
		}
	'''
}

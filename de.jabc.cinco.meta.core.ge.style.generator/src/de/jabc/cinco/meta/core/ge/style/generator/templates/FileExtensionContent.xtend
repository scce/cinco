/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import java.util.List
import java.util.Arrays
import de.jabc.cinco.meta.core.referenceregistry.implementing.IFileExtensionSupplier
import mgl.GraphModel
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;

class FileExtensionContent {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	var gm = null as GraphModel
	var exts = null as List<String>
	
	new (GraphModel graphmodel, List<String> fileExtensions) {
		exts = fileExtensions
		gm = graphmodel
	}
	
	def generateJavaClassContents(GraphModel gm) '''
		package «gm.packageName»;
		
		public class «gm.fuName.toFirstUpper»FileExtensions implements «IFileExtensionSupplier.name» {
			
			@Override
			public «List.name»<«String.name»> getKnownFileExtensions() {
				return «Arrays.name».asList(new «String.name»[] {«FOR e : exts SEPARATOR ','» "«e»"«ENDFOR »});
			}
		}
	'''

	def generatePluginExtensionContents() '''
		<extension
			point="de.jabc.cinco.meta.core.referenceregistry">
			<!--@CincoGen «gm.fuName.toFirstUpper»-->
			<FileExtensionsRegistry
				class="«gm.packageName».«gm.fuName.toFirstUpper»FileExtensions">
			</FileExtensionsRegistry>
		</extension>
	'''
	
}

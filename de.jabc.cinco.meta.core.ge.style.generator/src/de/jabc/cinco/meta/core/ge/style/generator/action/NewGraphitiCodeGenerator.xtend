/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.action

import de.jabc.cinco.meta.core.ge.style.generator.api.main.CincoApiGeneratorMain
import de.jabc.cinco.meta.core.ge.style.generator.main.GraphitiGeneratorMain
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.PathValidator
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import java.io.File
import java.io.FileInputStream
import java.net.URL
import java.util.HashMap
import java.util.Set
import mgl.GraphModel
import mgl.MGLModel
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.ecore.EObject
import style.Image

import static de.jabc.cinco.meta.core.utils.PathValidator.getURLForString

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.getStyles
import static extension de.jabc.cinco.meta.core.utils.projects.ProjectCreator.*
import java.util.HashSet

class NewGraphitiCodeGenerator extends AbstractHandler {
	
	extension WorkspaceExtension = new WorkspaceExtension
	extension FileExtension = new FileExtension
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension
	
	IProject project = null
	
	HashSet<String> folders

	override Object execute(ExecutionEvent event) throws ExecutionException {
		val cpdFile = MGLSelectionListener.INSTANCE.selectedCPDFile
		if (cpdFile === null) 
			throw new RuntimeException("No current cpd file in MGLSelectionListener...")
			
		val name_editorProject = '''«ProjectCreator.getProjectSymbolicName(ProjectCreator.getProject(allMGLs.get(0).eResource()))».editor.graphiti'''
		workspaceRoot.getProject(name_editorProject)?.delete(true, true, null)
		val bundles = (cpdFile.project.reqBundles + allMGLs.flatMap[graphModels].flatMap[additionalBundles]).toSet
		project = createDefaultPluginProject(name_editorProject, bundles, null)
		project.addAdditionalNature(null, "org.eclipse.xtext.ui.shared.xtextNature")
		
		for(mglModel : allMGLs) {
			if(!mglModel.graphModels.nullOrEmpty) {
				folders = newHashSet
				copyImages(mglModel)
				new GraphitiGeneratorMain(mglModel, cpdFile, mglModel.styles,folders)
				=> [
					
					mglModel.expPackages.forEach[project.exportPackage(it)]
					doGenerate(project)
				]
				new CincoApiGeneratorMain(mglModel).doGenerate(project)
			}
		}
		
		//Touch the manifest file to resolve errors where exported packages could not be used properly
		sanitizeManifestFile(name_editorProject)
		
		return null
	}
	
	def sanitizeManifestFile(String projectName) {
		workspaceRoot.getProject(projectName).getFile("META-INF/MANIFEST.MF").touch(null)
	}

	def private Set<String> getReqBundles(IProject project) {
		(#[  "org.eclipse.emf.transaction",
			 "org.eclipse.graphiti",
			 "org.eclipse.graphiti.mm",
			 "org.eclipse.graphiti.ui",
			 "org.eclipse.core.resources",
			 "org.eclipse.ui",
			 "org.eclipse.ui.ide",
			 "org.eclipse.ui.navigator",
			 "org.eclipse.ui.views.properties.tabbed",
			 "org.eclipse.gef",
			 "org.eclipse.xtext.ui",
			 "org.eclipse.xtext.xbase.lib",
			 "de.jabc.cinco.meta.core.ge.style.model",
			 "de.jabc.cinco.meta.core.ge.style.generator",
			 "de.jabc.cinco.meta.core.referenceregistry",
			 "de.jabc.cinco.meta.core.ui",
			 "de.jabc.cinco.meta.util",
			 "de.jabc.cinco.meta.runtime",
			 "de.jabc.cinco.meta.core.utils",
			 "de.jabc.cinco.meta.core.capi",
			 "de.jabc.cinco.meta.core.wizards",
			 "javax.el",
			 "com.sun.el",
			 "de.jabc.cinco.meta.core.ge.style.generator.runtime",
			 eventCorePluginID,
			 eventApiPluginID
		  ].map[Platform.getBundle(it)?.symbolicName]
		   .filterNull + #[project.name]
		).toSet
	}

	def private Set<String> additionalBundles(GraphModel gm) {
		gm.allModelAttributes.
		map[annotations].flatten.filter[name == "grammar"].
		map[value.get(1)]. //Get second parameter which should be the fully qualified name of the grammar activator
		map[substring(0,it.lastIndexOf("."))]. //Trim the activator's class name
		map[substring(0,it.lastIndexOf("."))]. //Trim the "internal"
		toSet 
	}

	def private getExpPackages(MGLModel mgl) {
		#[packageName(mgl).toString]
	}
	
	def private copyImages(MGLModel mgl) {
		val HashMap<String, URL> images = newHashMap
		
		for (gm : mgl.graphModels) {
			val String iconPath = gm.iconPath
			if (!iconPath.nullOrEmpty) {
				images.put(iconPath, getURLForString(gm, iconPath))
			}
		}
		
		for (node : mgl.nodes) {
			var annots = node.annotations;
			for (annot : annots) {
				if (annot.name.equals("icon")) {
					val iconPath = annot.value.get(0)
					if (!iconPath.nullOrEmpty) {
						images.put(iconPath, getURLForString(mgl, iconPath))
					}
				}
			}
		}
		
		var styles = CincoUtil::getStyles(mgl)
		for (var TreeIterator<EObject> ti = styles.eResource().getAllContents(); ti.hasNext();) {
			var EObject o = ti.next()
			if (o instanceof Image) {
				val Image img = (o as Image)
				val String path = img.getPath()
				if (PathValidator::isRelativePath(path)) {
					val url = PathValidator::getURLForString(img, path)
					images.put(path, url)
				}
			}
		}
		
		for (it : images.entrySet) {
			val source = new File(value.toURI)
			exportFolder(key)
			project.createFile(key, new FileInputStream(source))
		}
	}
	
	def exportFolder(String fileName){
		val index = fileName.lastIndexOf("/")
			if (index > -1) {
				val folderPath = fileName.substring(0, index)
				folders += folderPath
			}
	}
	
}

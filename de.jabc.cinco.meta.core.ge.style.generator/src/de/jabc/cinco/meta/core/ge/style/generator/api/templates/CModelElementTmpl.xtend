/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.api.templates

import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoRemoveFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.provider.CincoFeatureProvider
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import graphmodel.internal.InternalModelElement
import mgl.ModelElement
import org.eclipse.graphiti.dt.IDiagramTypeProvider
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.IUpdateFeature
import org.eclipse.graphiti.features.context.impl.RemoveContext
import org.eclipse.graphiti.features.context.impl.UpdateContext
import org.eclipse.graphiti.mm.pictograms.Diagram
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.platform.IDiagramBehavior
import org.eclipse.graphiti.ui.editor.DiagramBehavior
import org.eclipse.graphiti.ui.services.GraphitiUi

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*
import mgl.GraphModel

class CModelElementTmpl extends APIUtils {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	def getUpdateContent(ModelElement me) '''
		public void update() {
			if (getInternalElement_() == null || getInternalElement_().getRootElement() == null)
				return;
			«IFeatureProvider.name» fp = getFeatureProvider();
			«Diagram.name» diagram = getDiagram();
			if (fp != null && diagram != null) try {
				«PictogramElement.name» pe = getPictogramElement();
				if (pe != null) {
					«UpdateContext.name» uc = new «UpdateContext.name»(getPictogramElement());
					«IUpdateFeature.name» uf = fp.getUpdateFeature(uc);
					if (fp instanceof «CincoFeatureProvider.name») {
						((«CincoFeatureProvider.name») fp).executeFeature(uf, uc);
					}
				}
			} catch («NullPointerException.name» e) {
				e.printStackTrace();
				return;
			}
		}
	'''
	
	def getDeleteContent(ModelElement me) '''
		«IF !me.isIsAbstract»
			@Override
			public void delete(){
				if (getInternalElement_() == null) return; // Might be deleted by a hook
				«IFeatureProvider.name» fp = getFeatureProvider();
				«InternalModelElement.name» internal = getInternalElement_();
				super.delete();
				«RemoveContext.name» rc = new «RemoveContext.name»(this.getPictogramElement());
				«CincoRemoveFeature.name» rf = new «CincoRemoveFeature.name»(fp);
				if (rf.canRemove(rc)) {
					if (fp instanceof «CincoFeatureProvider.name»)
						((«CincoFeatureProvider.name») fp).executeFeature(rf,rc);
				}
			}
		«ENDIF»
	'''
	
	def getHighlightContent(ModelElement me) '''
		@Override
		public void highlight() {
			«IDiagramBehavior.name» idb = «me.mglModel.packageName».«me.mglModel.fuName»GraphitiUtils.getInstance().getDTP().getDiagramBehavior();
			if (idb instanceof «DiagramBehavior.name») {
				«DiagramBehavior.name» db = («DiagramBehavior.name») idb;
				db.setPictogramElementForSelection(getPictogramElement());
				db.selectBufferedPictogramElements();
			}
		}
	'''
	
	def doGenerateImpl(ModelElement me, GraphModel gm)'''
		package «gm.mglModel.packageNameAPI»;
		
		public «IF me.isIsAbstract»abstract «ENDIF»class «me.fuCName» extends «me.fqBeanImplName» 
			«IF !me.allSuperTypes.empty» implements «FOR st: me.allSuperTypes SEPARATOR ","» «st.fqBeanName» «ENDFOR» «ENDIF»{
			
			private «PictogramElement.name» pe;
			
			«me.constructor»
			
			public void setPictogramElement(«me.pictogramElementReturnType» pe) {
				this.pe = pe;
			}
			
			
			private «IFeatureProvider.name» getFeatureProvider() {
				«Diagram.name» diagram = getDiagram();
				if (diagram != null)
					return «GraphitiUi.name».getExtensionManager().createFeatureProvider(diagram);
				
				«IDiagramTypeProvider.name» dtp = «GraphitiUi.name».getExtensionManager().createDiagramTypeProvider("«me.dtpId»");
				return dtp.getFeatureProvider();
			}
			
		}
	'''
	
} 

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import graphmodel.IdentifiableElement
import graphmodel.internal.InternalGraphModel
import mgl.GraphModel
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.Resource.Factory
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl
import org.eclipse.graphiti.mm.pictograms.ContainerShape
import org.eclipse.graphiti.mm.pictograms.Diagram
import org.eclipse.graphiti.mm.pictograms.Shape

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class GraphitiResourceFactory {
	
	extension APIUtils = new APIUtils
	extension GeneratorUtils = GeneratorUtils.instance
	
	def generateResourceFactory(GraphModel gm)'''
		package «gm.packageName»
		
		import org.eclipse.emf.ecore.resource.Resource.Factory
		import org.eclipse.graphiti.mm.pictograms.PictogramElement
		
		class «gm.fuName»APIParser implements «Factory.name» {
			new() {
			}
		
			override «Resource.name» createResource(«URI.name» uri) {
				return new «gm.fuName»ApiResouce(uri)
			} 
		}
		
		class «gm.fuName»ApiResouce extends «XMIResourceImpl.name» {
			
			new() {
				super()
			}
		
			new(«URI.name» uri) {
				super(uri)
			}
		
		
			override protected getEObjectByID(«String.name» id) {
				val obj = super.getEObjectByID(id)
				switch (obj) {
					«IdentifiableElement.name»: obj.createAndUpdateGraphitiApiElement
				}
				obj
			}
			
			dispatch def createAndUpdateGraphitiApiElement(«IdentifiableElement.name» it) {
				System.out.println("Dispatching failed for createAndUpdateGraphitiApiElement")
				return it
			}
			
			«FOR me : gm.modelElements.filter[!isIsAbstract]»
				dispatch def createAndUpdateGraphitiApiElement(«me.fqCName» it) {
					val pe = getLinkedPictogramElement
					val ie = getInternalElement_()
					var cElement =	if (it instanceof «me.fqCName»)
						it as «me.fqCName»
					else new «me.fqCName»
					cElement.pictogramElement = pe as «me.pictogramElementReturnType»
					ie.element = cElement
					if (!ie.eAdapters.exists[it instanceof «me.packageNameEContentAdapter».«me.fuName»EContentAdapter])
						ie.eAdapters.add(new «me.packageNameEContentAdapter».«me.fuName»EContentAdapter)
					return it
				}
			«ENDFOR»
			
			def getLinkedPictogramElement(«IdentifiableElement.name» it) {
				val conts = getContents
				val d = conts.get(0) as «Diagram.name»;
				if (it instanceof «InternalGraphModel.name»)
					return d;
				d.fetchLinkedElement(it)
			}
			
			def fetchLinkedElement(«Diagram.name» d, «IdentifiableElement.name» me) {
				var pe = d.pictogramLinks?.filter[businessObjects.contains(me)].head?.pictogramElement
				if (pe === null) 
					pe = d.getPes.filter[link?.businessObjects?.contains(me)].head
				if (pe === null)
					pe = d.connections.filter[link?.businessObjects?.contains(me)].head
				pe
			}
			
			private def Iterable<«Shape.name»> getPes(«ContainerShape.name» cs) {
				return cs.children + cs.children.filter(«ContainerShape.name»).map[getPes].flatten
			}
		}
	'''
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.reconnect

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CNode
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlighter
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.ReconnectRegistry
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.Node
import graphmodel.internal.InternalEdge
import graphmodel.internal.InternalNode
import mgl.Edge
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.IUpdateFeature
import org.eclipse.graphiti.features.context.IReconnectionContext
import org.eclipse.graphiti.features.context.impl.ReconnectionContext
import org.eclipse.graphiti.features.context.impl.UpdateContext
import org.eclipse.graphiti.features.impl.DefaultReconnectionFeature
import org.eclipse.graphiti.mm.pictograms.Anchor

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.*

class EdgeReconnectFeatures {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension

	def doGenerateEdgeReconnectFeature(Edge e) '''
		package «e.packageNameReconnect»;
		
		public class ReconnectFeature«e.fuName» extends «DefaultReconnectionFeature.name» {
			
			private «ECincoError.name» error = «ECincoError.name».OK;
			
			private «InternalEdge.name» internalEdge = null;
			private «InternalNode.name» internalNode = null;
			«val isEventEnabled = e.eventEnabled»
			«IF isEventEnabled»
				
				private «e.fqBeanName» edge = null;
				private «Node.name» oldSource = null;
				private «Node.name» oldTarget = null;
				private «Node.name» newSource = null;
				private «Node.name» newTarget = null;
			«ENDIF»
			
			private boolean isReconnectSource = false;
			private boolean isReconnectTarget = false;
			
			public ReconnectFeature«e.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
			
			private boolean setContext(«IReconnectionContext.name» context) {
				Object boEdge = getBusinessObjectForPictogramElement(context.getConnection());
				Object boNode = getBusinessObjectForPictogramElement(context.getTargetPictogramElement());
				if (boEdge instanceof «InternalEdge.name» && boNode instanceof «InternalNode.name») {
					internalEdge = («InternalEdge.name») boEdge;
					internalNode = («InternalNode.name») boNode;
					«IF isEventEnabled»
						edge = («e.fqBeanName») internalEdge.getElement();
						oldSource = edge.getSourceElement();
						oldTarget = edge.getTargetElement();
					«ENDIF»
					if («ReconnectionContext.name».RECONNECT_SOURCE.equalsIgnoreCase(context.getReconnectType())) {
						«IF isEventEnabled»
							newSource = («Node.name») internalNode.getElement();
							newTarget = oldTarget;
						«ENDIF»
						isReconnectSource = true;
					}
					else if («ReconnectionContext.name».RECONNECT_TARGET.equalsIgnoreCase(context.getReconnectType())) {
						«IF isEventEnabled»
							newSource = oldSource;
							newTarget = («Node.name») internalNode.getElement();
						«ENDIF»
						isReconnectTarget = true;
					}
					return true;
				}
				return false;
			}
			«val highlightReconnectionEnabled = !CincoUtil.isHighlightReconnectionDisabled(e.graphModel)»
			«IF highlightReconnectionEnabled»
				
				@Override
				public void canceledReconnect(«IReconnectionContext.name» context) {
					«Highlighter.name».INSTANCE.get().onReconnectionCancel(«ReconnectRegistry.name».INSTANCE.remove(context.getConnection()));
					super.canceledReconnect(context);
				}
			«ENDIF»
			
			@Override
			public void preReconnect(«IReconnectionContext.name» context) {
				«IF highlightReconnectionEnabled»
					«Highlighter.name».INSTANCE.get().onReconnectionEnd(«ReconnectRegistry.name».INSTANCE.remove(context.getConnection()));
				«ENDIF»
				«IF isEventEnabled»
					if (setContext(context)) {
						if (isReconnectSource || isReconnectTarget) {
							«EventEnum.PRE_RECONNECT.getNotifyCallJava(e, 'edge', 'newSource', 'newTarget')»
						}
					}
				«ELSE»
					setContext(context);
				«ENDIF»
				super.preReconnect(context);
			}
		
			public boolean canReconnect(«IReconnectionContext.name» context, boolean apiCall) {
				«IF e.hasAnnotationDisableReconnect»
					// Can never reconnect because of @disable(reconnect) annotation
					return false;
				«ELSE»
					if (apiCall) {
						«IF ! CincoUtil.isHighlightReconnectionDisabled(e.graphModel)»
							if (!«ReconnectRegistry.name».INSTANCE.containsKey(context.getConnection())) {
								«ReconnectRegistry.name».INSTANCE.put(context.getConnection(), "");
								«ReconnectRegistry.name».INSTANCE.put(
									context.getConnection(),
									«Highlighter.name».INSTANCE.get().onReconnectionStart(this, context)
								);
							}
						«ENDIF»
						«Anchor.name» newAnchor = getNewAnchor(context);
						«Anchor.name» oldAnchor = context.getOldAnchor();
						if (newAnchor == null) {
							return false;
						}
						if (newAnchor.equals(oldAnchor)) {
							return true;
						}
						if (!setContext(context)) {
							return false;
						}
						if (isReconnectTarget) {
							return internalNode.canEnd(«e.fqBeanName».class)«IF e.eventEnabled» && canReconnect«e.fuName»Event()«ENDIF»;
						}
						if (isReconnectSource) {
							return internalNode.canStart(«e.fqBeanName».class)«IF e.eventEnabled» && canReconnect«e.fuName»Event()«ENDIF»;
						}
						return false;
					}
					return false;
				«ENDIF»
			}
			
			@Override
			public boolean canReconnect(«IReconnectionContext.name» context) {
				return canReconnect(context, true);
			}
			«IF !e.hasAnnotationDisableReconnect && e.eventEnabled»
				
				private boolean canReconnect«e.fuName»Event() {
					«EventEnum.CAN_RECONNECT.getNotifyCallJava('''canReconnect«e.fuName»EventResult''', e, 'edge', 'newSource', 'newTarget')»
					return canReconnect«e.fuName»EventResult == null || canReconnect«e.fuName»EventResult;
				}
			«ENDIF»
			
			@Override
			public void postReconnect(«IReconnectionContext.name» context) {
				
				if (getNewAnchor(context).equals(context.getOldAnchor()))
					return;
					
				if (isReconnectSource) {
					internalEdge.set_sourceElement(internalNode); 
				}
				else if (isReconnectTarget) {
					internalEdge.set_targetElement(internalNode);
				}
				«IF isEventEnabled»
					
					if (isReconnectSource || isReconnectTarget) {
						«EventEnum.POST_RECONNECT.getNotifyCallJava(e, 'edge', 'oldSource', 'oldTarget')»
					}
				«ENDIF»
				
				«UpdateContext.name» uc = new «UpdateContext.name»(context.getConnection());
				«IUpdateFeature.name» uf = getFeatureProvider().getUpdateFeature(uc);
				if (uf != null && uf.canUpdate(uc))
					uf.update(uc);
				
			}
			
			@Override
			protected «Anchor.name» getNewAnchor(«IReconnectionContext.name» context) {
				if(context.getNewAnchor() == null) {
					Object innerBo = org.eclipse.graphiti.services.Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(context.getTargetPictogramElement());
					if(innerBo instanceof «CNode.name») {
						return ((«CNode.name») innerBo).getAnchor();
					}
				}
				return context.getNewAnchor(); 
			}
			
			public «ECincoError.name» getError() {
				return error;
			}
			
			public void setError(«ECincoError.name» error) {
				this.error = error;
			}
		}
	'''
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.expressionlanguage

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import javax.el.ELContext
import javax.el.VariableMapper
import javax.el.FunctionMapper
import javax.el.CompositeELResolver
import javax.el.ELResolver
import javax.el.BeanELResolver

class ContextTmp {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	/**
	 * Generates the Class 'ExpressionLanguageContext' for the graphmodel gm
	 * @param gm : GraphModel
	 */
	def generateContext(mgl.GraphModel gm)'''
		package «gm.packageNameExpression»;
		
		public class «gm.fuName»ExpressionLanguageContext extends «ELContext.name» {
		
			private «Object.name» o;
		
			public «gm.fuName»ExpressionLanguageContext(«Object.name» o) {
				this.o = o;
			}
		
			@Override
			public «VariableMapper.name» getVariableMapper() {
				return null;
			}
		
			@Override
			public «FunctionMapper.name» getFunctionMapper() {
				return null;
			}
		
			@Override
			public «ELResolver.name» getELResolver() {
				«CompositeELResolver.name» compELRes = new «CompositeELResolver.name»();
				compELRes.add(new «BeanELResolver.name»(true));
				compELRes.add(new «gm.fuName»ExpressionLanguageResolver(o));
				return compELRes;
			}
		}
	'''
}

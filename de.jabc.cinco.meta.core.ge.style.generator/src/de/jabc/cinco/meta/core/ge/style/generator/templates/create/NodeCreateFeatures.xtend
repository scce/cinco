/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.create

import de.jabc.cinco.meta.core.ge.style.generator.runtime.createfeature.CincoCreateFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.ModelElement
import graphmodel.ModelElementContainer
import graphmodel.internal.InternalModelElementContainer
import mgl.Node
import org.eclipse.emf.ecore.EObject
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.ICreateContext
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import style.Styles

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.hasAnnotationDisableCreate

class NodeCreateFeatures extends APIUtils{
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension
	
	/** 
	 * Generates the 'Create-Feature' for a given node
	 * @param n : The node
	 * @param styles : The style
	*/	
	def doGenerateNodeCreateFeature(Node n, Styles styles) '''
		package «n.packageNameCreate»;
		
		public class CreateFeature«n.fuName» extends  «CincoCreateFeature.name»<«ModelElement.name»>{
			
			/**
			 * Call of the Superclass
			 * @param fp : Fp is the parameter of the Superclass-Call
			*/
			public CreateFeature«n.fuName»(«IFeatureProvider.name» fp) {
				super(fp, "«n.fuName»", "Create «n.fuName»");
			}

			«IF !n.iconNodeValue.nullOrEmpty»	
				/**
				 * @return Returns the image id
				*/
				@Override
				public String getCreateImageId() {
					return "«getIconNodeValue(n)»";
				}
			«ENDIF»
		
			/**
			 * Checks if a context can be created
			 * @param context : Contains the information, needed to let a feature create a pictogram element
			 * @param apiCall : ApiCall shows if the Cinco Api is used
			 * @return Returns true if the context can be created and false if not
			*/
			public boolean canCreate(«ICreateContext.name» context, boolean apiCall) {
				if (apiCall) {
					«Object.name» target = «Graphiti.name».getLinkService().getBusinessObjectForLinkedPictogramElement(context.getTargetContainer());
					if (target instanceof «ModelElementContainer.name») {
						target = ((«ModelElementContainer.name») target).getInternalContainerElement();
					}
					if (target instanceof «InternalModelElementContainer.name») {
						«InternalModelElementContainer.name» internalTarget = («InternalModelElementContainer.name») target;
						if (internalTarget.canContain(«n.fqBeanName».class)) {
							«IF n.eventEnabled»
								return canCreate«n.fuName»Event(context, internalTarget);
							«ELSE»
								return true;
							«ENDIF»
						}
						else {
							if (getError().equals(«ECincoError.name».OK)) {
								setError(«ECincoError.name».MAX_CARDINALITY);
							}
						}
					}
					return false;
				}
				return false;
			}
			«IF n.eventEnabled»
				
				private boolean canCreate«n.fuName»Event(«ICreateContext.name» context, «InternalModelElementContainer.name» internalContainer) {
					«ModelElementContainer.name» container = («ModelElementContainer.name») internalContainer.getElement();
					«EventEnum.CAN_CREATE_NODE.getNotifyCallJava('''canCreate«n.fuName»EventResult''', n, '''«n.fqBeanName».class''', 'container', 'context.getX()', 'context.getY()', 'context.getWidth()', 'context.getHeight()')»
					return canCreate«n.fuName»EventResult == null || canCreate«n.fuName»EventResult;
				}
			«ENDIF»
			
			@Override
			public boolean hasDoneChanges() {
				return getModelElement() != null;
			}
			
			/**
			 * Creates a pictogram element with the given 'context'
			 * @param context : Contains the information, needed to let a feature create a pictogram element
			 * @return Returns a list with the created pictogram elements and its graphical representation
		    */
			public «Object.name»[] create(«ICreateContext.name» context) {
				«PictogramElement.name» target = context.getTargetContainer();
				«InternalModelElementContainer.name» container;
				«EObject.name» targetBO = («EObject.name») getBusinessObjectForPictogramElement(target);
				if (targetBO instanceof «ModelElementContainer.name»)
					container = ((«ModelElementContainer.name») targetBO).getInternalContainerElement();
				else container = («InternalModelElementContainer.name») targetBO;
				«IF n.isPrime»
					String _libraryComponentUID = («String.name») context.getProperty("libraryComponentUID");
					«n.fqBeanName» «n.flName» = 
						(«n.fqCName») «MGLUtil::getMglModel(n).packageName».«MGLUtil::getMglModel(n).fuName»Factory.eINSTANCE.create«n.fuName»(_libraryComponentUID, container, context.getX(), context.getY(), context.getWidth(), context.getHeight());
				«ELSE»
					«n.fqBeanName» «n.flName» = 
						(«n.fqCName») «MGLUtil::getMglModel(n).packageName».«MGLUtil::getMglModel(n).fuName»Factory.eINSTANCE.create«n.fuName»(container, context.getX(), context.getY(), context.getWidth(), context.getHeight());
				«ENDIF»
				if («n.flName» == null) return null;
				setModelElement(«n.flName»);
				return new «Object.name»[] {«n.flName», ((«n.fqCName») «n.flName»).getPictogramElement()};
			}
			
			/**
			 * Checks if a context can be created by using the method 'canCreate(context,apiCall)'
			 * @param context : Contains the information, needed to let a feature create a pictogram element
			 * @return Returns true if the context can be created and false if not
		    */
			@Override
			public boolean canCreate(«ICreateContext.name» context) {
				return canCreate(context, «!n.hasAnnotationDisableCreate»);
			}
			
			/**
			 * Get-method for an error
			 * @return Returns an 'error' in which 'error' is  'ECincoError.OK'
			*/
			public «ECincoError.name» getError() {
				return error;
			}

		    /**
			 * Set-method for an error
			 * @param error : Error is a value of the enum: MAX_CARDINALITY, MAX_IN, MAX_OUT, INVALID_SOURCE, INVALID_TARGET, INVALID_CONTAINER, INVALID_CLONE_TARGET, OK
			*/
			public void setError(«ECincoError.name» error) {
				this.error = error;
			}
		}
	'''
}

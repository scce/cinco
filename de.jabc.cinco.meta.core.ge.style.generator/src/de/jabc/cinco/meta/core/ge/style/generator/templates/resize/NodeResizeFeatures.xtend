/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.resize

import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoAbstractResizeFeature
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.Direction
import graphmodel.ModelElement
import mgl.Node
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IResizeShapeContext
import style.Styles

class NodeResizeFeatures {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension
	
	/**
	 * Generates the 'Resize-Feature' for a node
	 * @param n : The node
	 * @param styles : The style
	 */
	def doGenerateNodeResizeFeature(Node n, Styles styles)'''
		package «n.packageNameResize»;
		
		public class ResizeFeature«n.fuName» extends «CincoAbstractResizeFeature.name» {
			
			private «EventApiExtension.name» «EventApiExtension.simpleName.toFirstLower» = new «EventApiExtension.name»();
			private «n.fqInternalBeanName» bo;
			
			/**
			 * Call of the Superclass
			 * @param fp: Fp is the parameter of the Superclass-Call
			 */
			public ResizeFeature«n.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
			
			/**
			 * Checks if a shape can be resized
			 * @param context : Contains the information, needed to let a feature resize a shape
			 * @param apiCall : ApiCall shows if the Cinco Api is used
			 * @return Returns true if the shape can be resized and false if not
			 */
			public boolean canResizeShape(«IResizeShapeContext.name» context, boolean apiCall) {
				if (apiCall || super.isApiCall()) {
					«Object.name» bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
					if (bo instanceof «n.fqInternalBeanName») {
						«IF n.eventEnabled»
							«n.fqInternalBeanName» ime = («n.fqInternalBeanName») bo;
							return canResize«n.fuName»Event(ime, context);
						«ELSE»
							return true;
						«ENDIF»
					}
				}
				return false;
			}
			
			/**
			 * Checks if a shape can be resized by using the method 'canResizeShape(context,apiCall)'
			 * @param context : Contains the information, needed to let a feature create a shape
			 * @return Returns if the shape can be resized and false if not
			 */
			@Override
			public boolean canResizeShape(«IResizeShapeContext.name» context) {
				return canResizeShape(context, «!CincoUtil.isResizeDisabled(n)»);
			}
			
			«IF n.eventEnabled»
				private boolean canResize«n.fuName»Event(«n.fqInternalBeanName» ime, «IResizeShapeContext.name» context) {
					if (context.getWidth() == -1 && context.getHeight() == -1 && context.getX() == -1 && context.getY() == -1) {
						return true;
					}
					«ModelElement.name» me = ime.getElement();
					if (me instanceof «n.fqBeanName») {
						«n.fqBeanName» element = («n.fqBeanName») me;
«««						«Direction.name» direction = «EventApiExtension.simpleName.toFirstLower».getResizeDirection(element.getWidth(), element.getHeight(), element.getX(), element.getY(), context.getWidth(), context.getHeight(), context.getX(), context.getY());
						«Direction.name» direction = «EventApiExtension.simpleName.toFirstLower».getResizeDirection(context);
						«EventEnum.CAN_RESIZE.getNotifyCallJava('''canResize«n.fuName»EventResult''', n, 'element', 'context.getWidth()', 'context.getHeight()', 'context.getX()', 'context.getY()', 'direction')»
						return canResize«n.fuName»EventResult == null || canResize«n.fuName»EventResult;
					}
					return false;
				}
				
			«ENDIF»
			/**
			 * Resizes a shape
			 * @param context : Contains the information, needed to let a feature resize a shape
			 */
			@Override
			public void resizeShape(final «IResizeShapeContext.name» context) {
				bo = («n.fqInternalBeanName») getBusinessObjectForPictogramElement(context.getPictogramElement());
				«graphmodel.Node.name» node = («graphmodel.Node.name») bo.getElement();
				node.resize(context.getWidth(), context.getHeight(), context.getX(), context.getY());
			}
			
		}
	'''
}

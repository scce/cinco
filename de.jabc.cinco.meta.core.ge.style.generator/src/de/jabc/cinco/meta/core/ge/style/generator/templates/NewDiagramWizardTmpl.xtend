/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import java.lang.reflect.InvocationTargetException
import mgl.GraphModel
import org.eclipse.core.resources.IContainer
import org.eclipse.core.resources.IResource
import org.eclipse.core.resources.IWorkspaceRoot
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.Path
import org.eclipse.emf.ecore.EPackage
import org.eclipse.jface.operation.IRunnableWithProgress
import org.eclipse.jface.viewers.IStructuredSelection
import org.eclipse.jface.wizard.IWizardPage
import org.eclipse.jface.wizard.Wizard
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.INewWizard
import org.eclipse.ui.IWorkbench
import org.eclipse.ui.WorkbenchException

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils

class NewDiagramWizardTmpl extends APIUtils {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
/**
 * Generates the {@link Wizard} class code for the {@link GraphModel}.
 * 
 * @param gm The processed {@link GraphModel}
 */	
	def generateNewDiagramWizard(GraphModel gm)'''
		package «gm.packageName».wizard;
		
		public class «gm.fuName»DiagramWizard extends «Wizard.name» implements «INewWizard.name» {
		
			private «IWizardPage.name» page;
		
			private «IStructuredSelection.name» ssel;
			
			public «gm.fuName»DiagramWizard() {
			}
		
			@Override
			public void addPages() {
				page = new «gm.packageName».wizard.«gm.fuName»DiagramWizardPage("new«gm.fuName»");
				addPage(page);
				
				super.addPages();
			}
			
			@Override
			public void init(«IWorkbench.name» workbench, «IStructuredSelection.name» selection) {
				ssel = selection;
			}
		
			@Override
			public boolean performFinish() {
				if (page instanceof «gm.packageName».wizard.«gm.fuName»DiagramWizardPage) {
					«gm.packageName».wizard.«gm.fuName»DiagramWizardPage p = («gm.packageName».wizard.«gm.fuName»DiagramWizardPage) page;
					final «String.name» dir = p.getDirectory();
					final «String.name» fileName = p.getFileName();
					«IRunnableWithProgress.name» operation = new «IRunnableWithProgress.name»() {
						
						@Override
						public void run(«IProgressMonitor.name» monitor) throws «InvocationTargetException.name»,
								«InterruptedException.name» {
							createDiagram(dir, fileName);
						}
					};
					
					try {
						getContainer().run(false, false, operation);
					} catch («InvocationTargetException.name» | «InterruptedException.name» e) {
						e.printStackTrace();
						return false;
					}/* catch («WorkbenchException.name» e) {
						e.printStackTrace();
						return false;
					}*/
				}
				return true;
			}
		
			
			private void createDiagram(«String.name» dir, «String.name» fName) {
				«IWorkspaceRoot.name» root = «ResourcesPlugin.name».getWorkspace().getRoot();
				«IResource.name» containerResource = root.getContainerForLocation(new «Path.name»(dir));
				if (containerResource instanceof «IContainer.name») {
					«gm.mglModel.packageName».«gm.mglModel.fuName»Factory eFactory = 
						(«gm.mglModel.packageName».«gm.mglModel.fuName»Factory) «EPackage.name».Registry.INSTANCE.getEFactory("«gm.mglModel.nsURI»");
					«gm.fqCName» «gm.flName» = («gm.fqCName») eFactory.create«gm.fuName»(containerResource.getFullPath().toString(), fName);
						new «WorkbenchExtension.name»().openEditor(«gm.flName»);
				}
			}
			
			public «IStructuredSelection.name» getSelection() {
				return this.ssel;
			}
		
			@Override
			public void createPageControls(«Composite.name» pageContainer) {
				super.createPageControls(pageContainer);
			}
			
			@Override
			public boolean canFinish() {
				return page.isPageComplete(); 
			}
				
		}
	'''
	
}

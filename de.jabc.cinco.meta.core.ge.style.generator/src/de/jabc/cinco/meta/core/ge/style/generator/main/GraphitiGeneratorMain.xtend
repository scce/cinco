/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.main

import de.jabc.cinco.meta.core.ge.style.generator.templates.DiagramEditorTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.DiagramTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.DiagramTypeProviderTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.EmfFactoryTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.FeatureProviderTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.FileExtensionContent
import de.jabc.cinco.meta.core.ge.style.generator.templates.GraphitiResourceFactory
import de.jabc.cinco.meta.core.ge.style.generator.templates.GraphitiUtilsTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.ImageProviderTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.LayoutFeatureTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.NewDiagramWizardPageTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.NewDiagramWizardTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.PluginXMLTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.PropertyViewTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.ResourceContributorTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.ToolBehaviorProviderTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.add.EdgeAddFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.add.NodeAddFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.create.EdgeCreateFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.create.NodeCreateFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.delete.ModelElementDeleteFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.expressionlanguage.ContextTmp
import de.jabc.cinco.meta.core.ge.style.generator.templates.expressionlanguage.ResolverTmp
import de.jabc.cinco.meta.core.ge.style.generator.templates.layout.EdgeLayoutFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.layout.NodeLayoutFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.move.NodeMoveFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.reconnect.EdgeReconnectFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.resize.NodeResizeFeatures
import de.jabc.cinco.meta.core.ge.style.generator.templates.update.ModelElementUpdateFeatures
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.core.utils.projects.ContentWriter
import java.util.ArrayList
import java.util.List
import mgl.Edge
import mgl.MGLModel
import mgl.Node
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.NullProgressMonitor
import productDefinition.CincoProduct
import style.Styles
import de.jabc.cinco.meta.core.utils.BuildProperties
import org.eclipse.jdt.core.IJavaProject
import java.util.Arrays
import org.eclipse.jdt.core.JavaCore
import java.util.HashSet

class GraphitiGeneratorMain { 
	
	extension DiagramTmpl = new DiagramTmpl
	extension DiagramEditorTmpl = new DiagramEditorTmpl
	extension ResourceContributorTmpl = new ResourceContributorTmpl
	extension DiagramTypeProviderTmpl = new DiagramTypeProviderTmpl
	extension FeatureProviderTmpl = new FeatureProviderTmpl
	extension ImageProviderTmpl = new ImageProviderTmpl
	extension NewDiagramWizardTmpl = new NewDiagramWizardTmpl
	extension NewDiagramWizardPageTmpl = new NewDiagramWizardPageTmpl
	extension PluginXMLTmpl = new PluginXMLTmpl
	extension PropertyViewTmpl = new PropertyViewTmpl
	extension ToolBehaviorProviderTmpl = new ToolBehaviorProviderTmpl
	extension GraphitiUtilsTmpl = new GraphitiUtilsTmpl
	extension LayoutFeatureTmpl = new LayoutFeatureTmpl
	extension ContextTmp = new ContextTmp
	extension ResolverTmp = new ResolverTmp
	extension NodeAddFeatures = new NodeAddFeatures
	extension NodeCreateFeatures = new NodeCreateFeatures
	extension ModelElementDeleteFeatures = new ModelElementDeleteFeatures
	extension EdgeAddFeatures = new EdgeAddFeatures
	extension EdgeCreateFeatures = new EdgeCreateFeatures
	extension NodeLayoutFeatures = new NodeLayoutFeatures
	extension NodeResizeFeatures = new NodeResizeFeatures
	extension NodeMoveFeatures = new NodeMoveFeatures
	extension ModelElementUpdateFeatures = new ModelElementUpdateFeatures
	extension EdgeLayoutFeatures = new EdgeLayoutFeatures
	extension EdgeReconnectFeatures = new EdgeReconnectFeatures
	extension EmfFactoryTmpl = new EmfFactoryTmpl
	extension GraphitiResourceFactory = new GraphitiResourceFactory
	extension GeneratorUtils = GeneratorUtils.instance
	
	var MGLModel mgl
	var IFile cpdFile
	var Styles styles
	val CincoProduct cincoProduct
	var List<CharSequence> pluginXMLContent = new ArrayList<CharSequence>
	
	val HashSet<String> folders
	
	new (MGLModel mgl, IFile cpdFile, Styles s,HashSet<String> folders) {
		this.mgl = mgl
		this.cpdFile = cpdFile
		this.cincoProduct = CincoUtil::getCPD(cpdFile) as CincoProduct
		styles = s
		this.folders = folders
	}
	
	def doGenerate(IProject project) {
		var content = mgl.generateFactory
		ContentWriter::writeJavaFileInSrcGen(project, mgl.packageName, mgl.fileName.toFirstUpper.concat("Factory.java"), content)
		content = mgl.generateImageProvider
		ContentWriter::writeJavaFileInSrcGen(project, mgl.packageName, mgl.fileName.toFirstUpper.concat("ImageProvider.java"), content)
		content = mgl.generateLayoutFeature(styles)
		ContentWriter::writeJavaFileInSrcGen(project, mgl.packageName, mgl.fileName.toFirstUpper.concat("LayoutUtils.java"), content)
		content = mgl.generateGraphitiUtils
		ContentWriter::writeJavaFileInSrcGen(project, mgl.packageName, mgl.fileName.toFirstUpper.concat("GraphitiUtils.java"), content)
		
		for (gm : mgl.graphModels.filter[!isAbstract]) {
			content = gm.generateDiagramTypeProvider(cincoProduct, cpdFile)
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName, gm.name.toFirstUpper.concat("DiagramTypeProvider.java"), content)
			content = gm.generateFeatureProvider
			ContentWriter::writeFile(project, "src-gen", gm.packageName, gm.name.toFirstUpper.concat("FeatureProvider.xtend"), content)
			content = gm.generateToolBehaviorProvider
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName, gm.name.toFirstUpper.concat("ToolBehaviorProvider.java"), content)
			content = gm.generateNewDiagramWizard
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName.toString.concat(".wizard"), gm.name.toFirstUpper.concat("DiagramWizard.java"), content)
			content = gm.generateNewDiagramWizardPage
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName.toString.concat(".wizard"), gm.name.toFirstUpper.concat("DiagramWizardPage.java"), content)
			content = gm.generateDiagram
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName, gm.name.toFirstUpper.concat("Diagram.java"), content)
			content = gm.generateDiagramEditor
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName, gm.name.toFirstUpper.concat("DiagramEditor.java"), content)
			content = gm.generateResourceContributor
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName, gm.name.toFirstUpper.concat("ResourceContributor.java"), content)
			content = gm.generatePropertyView
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageName.toString.concat(".property.view"), gm.name.toFirstUpper.concat("PropertyView.java"), content)
			content = gm.generateResolver
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageNameExpression, gm.name.toFirstUpper.concat("ExpressionLanguageResolver.java"), content)
			content = gm.generateContext
			ContentWriter::writeJavaFileInSrcGen(project, gm.packageNameExpression, gm.name.toFirstUpper.concat("ExpressionLanguageContext.java"), content)
			content = gm.generateResourceFactory
			ContentWriter::writeFile(project, "src-gen", gm.packageName, gm.name.toFirstUpper.concat("ResourceFactory.xtend"), content)
			
			var usedExtensions = CincoUtil.getUsedExtensions(gm);
		    var fileExtensionClassContent = new FileExtensionContent(gm, usedExtensions).generateJavaClassContents(gm);
		    ContentWriter::writeJavaFileInSrcGen(project, gm.packageName, gm.name.toFirstUpper+ "FileExtensions" +".java",fileExtensionClassContent)
		}
		
		for (Node n : mgl.nodes.filter[!isIsAbstract]) {
			if (n.isPrime){
				content = n.doGeneratePrimeAddFeature(styles)
				ContentWriter::writeJavaFileInSrcGen(project, n.packageNameAdd, "AddFeaturePrime"+n.name.toFirstUpper+".java", content)
			}
			content = n.doGenerateNodeAddFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, n.packageNameAdd, "AddFeature"+n.name.toFirstUpper+".java", content)
			
			if (!n.isIsAbstract) {
				content = n.doGenerateNodeCreateFeature(styles) 
				ContentWriter::writeJavaFileInSrcGen(project, n.packageNameCreate, "CreateFeature"+n.name.toFirstUpper+".java", content)
			}
			
			content = n.doGenerateModelElementDeleteFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, n.packageNameDelete, "DeleteFeature"+n.name.toFirstUpper+".java", content)
			
			content = n.doGenerateNodeLayoutFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, n.packageNameLayout, "LayoutFeature"+n.name.toFirstUpper+".java", content)
			
			content = n.doGenerateNodeResizeFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, n.packageNameResize, "ResizeFeature"+n.name.toFirstUpper+".java", content)
			
			content = n.doGenerateNodeMoveFeature(styles)
			ContentWriter::writeFile(project, "src-gen", n.packageNameMove, "MoveFeature"+n.name.toFirstUpper+".xtend", content)
			
			content = n.doGenerateModelElementUpdateFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, n.packageNameUpdate, "UpdateFeature"+n.name.toFirstUpper+".java", content)
			
		}
		
		for (Edge e : mgl.edges.filter[!isIsAbstract]) {
			content = e.doGenerateEdgeAddFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, e.packageNameAdd, "AddFeature"+e.name.toFirstUpper+".java", content)

			if (!e.isIsAbstract) {
				content = e.doGenerateEdgeCreateFeature(styles)
				ContentWriter::writeJavaFileInSrcGen(project, e.packageNameCreate, "CreateFeature"+e.name.toFirstUpper+".java", content)
			}
			
			content = e.doGenerateModelElementDeleteFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, e.packageNameDelete, "DeleteFeature"+e.name.toFirstUpper+".java", content)
			
			content = e.doGenerateModelElementUpdateFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, e.packageNameUpdate, "UpdateFeature"+e.name.toFirstUpper+".java", content)
			
			content = e.doGenerateEdgeLayoutFeature(styles)
			ContentWriter::writeJavaFileInSrcGen(project, e.packageNameLayout, "LayoutFeature"+e.name.toFirstUpper+".java", content)
			
			content = e.doGenerateEdgeReconnectFeature
			ContentWriter::writeJavaFileInSrcGen(project, e.packageNameReconnect, "ReconnectFeature"+e.fuName+".java", content)
			
		}
		
		pluginXMLContent = mgl.generatePluginXML
		ContentWriter::writePluginXML(project, pluginXMLContent, '''<!--@CincoGen «mgl.fuName»-->''')
		val monitor = new NullProgressMonitor();
		val bp = BuildProperties.loadBuildProperties(project,monitor);
		bp.appendBinIncludes("plugin.xml")
		bp.appendSource("xtend-gen/")
		folders.forEach[
			bp.appendBinIncludes(it+"/")
		]
		bp.store(project,monitor)
		CincoUtil.refreshProject(monitor, project)
	}
	
}

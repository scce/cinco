/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.util

import com.sun.el.ExpressionFactoryImpl
import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CNode
import de.jabc.cinco.meta.core.ge.style.generator.runtime.expressionlanguage.ExpressionLanguageContext
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoLayoutFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoResizeFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.utils.CincoLayoutUtils
import de.jabc.cinco.meta.core.ge.style.generator.templates.LayoutFeatureTmpl
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import java.util.LinkedList
import java.util.regex.Pattern
import javax.el.ELException
import mgl.Edge
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.graphiti.features.context.impl.ResizeShapeContext
import org.eclipse.graphiti.mm.GraphicsAlgorithmContainer
import org.eclipse.graphiti.mm.algorithms.styles.Style
import org.eclipse.graphiti.mm.pictograms.Diagram
import org.eclipse.graphiti.mm.pictograms.Shape
import org.eclipse.graphiti.services.Graphiti
import org.eclipse.graphiti.services.IGaService
import org.eclipse.graphiti.services.IPeService
import style.AbsolutPosition
import style.AbstractShape
import style.Alignment
import style.Appearance
import style.BooleanEnum
import style.ConnectionDecorator
import style.ContainerShape
import style.Ellipse
import style.Font
import style.GraphicsAlgorithm
import style.Image
import style.LineStyle
import style.MultiText
import style.NodeStyle
import style.Polygon
import style.Polyline
import style.PredefinedDecorator
import style.Rectangle
import style.RoundedRectangle
import style.StyleFactory
import style.Text
import mgl.GraphicalModelElement

class StyleUtil extends APIUtils {

	extension GeneratorUtils = GeneratorUtils.instance

	static var num = 0;
	static int index = 0;
	static Node node;
	static MGLModel mglModel;

	def getAlgorithmCode(AbstractShape aShape, String containerShapeName, Node n) {
		node = n;
		index = 0;
		mglModel = MGLUtil::getMglModel(n);
		'''
			«var currentPeName = getShapeName(aShape)»
			«var currentGaName = getGAName(aShape)»
			«polyVarDeclaration()»				
			
			«aShape.creator(currentPeName.toString, containerShapeName)»
			
			«IF aShape.shapeIsAnchorShape»
				«anchor(currentPeName.toString)»
			«ENDIF»
			
			«aShape.getCode(currentGaName, currentPeName)» 
			
			«aShape.setSizeFromContext(currentGaName)»
			
			«aShape.appearanceCode(currentGaName)»
			
			«aShape.setPosition(currentGaName)»
			
			«aShape.recursiveCall(currentPeName.toString)»
			
			linkAllShapes(«currentPeName», bo);
			layoutPictogramElement(«currentPeName»);
			
			((«CNode.name») bo.getElement()).setPictogramElement(«currentPeName»);
			
			bo.setX(«currentPeName».getGraphicsAlgorithm().getX());
			bo.setY(«currentPeName».getGraphicsAlgorithm().getY());
			
			if (context.getWidth() != -1 && context.getHeight() != -1)  {
			
				«ResizeShapeContext.name» rc = new «ResizeShapeContext.name»(«currentPeName»);
				rc.setWidth(context.getWidth());
				rc.setHeight(context.getHeight());
				rc.setX(context.getX());
				rc.setY(context.getY());
				«CincoResizeFeature.name».resize(rc);
			} else if (parentIsDiagram)  {
				«ResizeShapeContext.name» rc = new «ResizeShapeContext.name»(«currentPeName»);
				rc.setWidth(width);
				rc.setHeight(height);
				rc.setX(context.getX() + minX);
				rc.setY(context.getY() + minY);
			
				«CincoResizeFeature.name».resize(rc);
			}
			
			layoutPictogramElement(«currentPeName»);
			
			bo.setWidth(«currentPeName».getGraphicsAlgorithm().getWidth());
			bo.setHeight(«currentPeName».getGraphicsAlgorithm().getHeight());
			
			return «currentPeName»;
		'''
	}

	def CharSequence getCode(AbstractShape absShape, String containerShapeName) '''
		«var currentPeName = getShapeName(absShape)»
		«var currentGaName = getGAName(absShape)»
		«absShape.creator(currentPeName.toString, containerShapeName)»
		
		«IF absShape.shapeIsAnchorShape»
			«anchor(currentPeName.toString)»
		«ENDIF»
		
		«absShape.getCode(currentGaName, currentPeName)» 
		«absShape.setSize(currentGaName)»
		
		«absShape.setPosition(currentGaName)»
		«absShape.appearanceCode(currentGaName)»

		«absShape.recursiveCall(currentPeName.toString)»
	'''
	
	def CharSequence anchor(String containerShapeName) '''
			peService.createChopboxAnchor(«containerShapeName»);
	'''

	def appearanceCode(AbstractShape shape, CharSequence currentGaName) {
		'''
		peService.setPropertyValue(«currentGaName», "«CincoLayoutFeature.KEY_GA_NAME»", "«currentGaName»");
		'''+
		if (shape.referencedAppearance !== null) '''«mglModel.packageName».«mglModel.fileName»LayoutUtils.set«shape.referencedAppearance.name»Style(«currentGaName», getDiagram());'''
		else if (shape.inlineAppearance !== null)  ''' «mglModel.packageName».«mglModel.fileName»LayoutUtils.«LayoutFeatureTmpl.shapeMap.get(shape)»(«currentGaName», getDiagram());'''
		else '''«mglModel.packageName».«mglModel.fileName»LayoutUtils.set_«mglModel.fileName»DefaultAppearanceStyle(«currentGaName», getDiagram());'''
	}

	def appearanceCode(PredefinedDecorator shape, CharSequence currentGaName) {
		if (shape.referencedAppearance !== null) '''«mglModel.packageName».«mglModel.fileName»LayoutUtils.set«shape.referencedAppearance.name»Style(«currentGaName», getDiagram());'''
		else if (shape.inlineAppearance !== null)  ''' «mglModel.packageName».«mglModel.fileName»LayoutUtils.«LayoutFeatureTmpl.shapeMap.get(shape)»(«currentGaName», getDiagram());'''
		else '''«mglModel.packageName».«mglModel.fileName»LayoutUtils.set_«mglModel.fileName»DefaultAppearanceStyle(«currentGaName», getDiagram());'''
	}

	/**
	 * This method writes the {@link org.eclipse.graphiti.mm.pictograms.ContainerShape} initialization code.
	 *	
	 * @param cs The {@link ContainerShape} for which the Graphiti code should be generated
	 * @param peName The ContainerShape's name
	 * @param containerName The parent's name
	 */
	def dispatch creator(ContainerShape cs, String peName, String containerName) '''
		«org.eclipse.graphiti.mm.pictograms.ContainerShape.name» «peName» = peService.createContainerShape(«containerName», «containerIsDiagramOrMovable(
			cs as AbstractShape) || cs.isAnchorShape»);
	'''
	
	/**
	 * This method writes the {@link Shape} initialization code.
	 * 
	 * @param s The {@link style.Shape} for which the Graphiti code should be generated
	 * @param peName The ContainerShape's name
	 * @param containerName The parent's name
	 */
	def dispatch creator(style.Shape s, String peName, String containerName) '''
		«Shape.name» «peName» = peService.createShape(«containerName», «containerIsDiagramOrMovable(s as AbstractShape) || s.isAnchorShape»);
	'''

	def dispatch creator(Image s, String peName, String containerName) '''
		«Shape.name» «peName» = peService.createContainerShape(«containerName», «containerIsDiagramOrMovable(s as AbstractShape) || s.isAnchorShape»);
	'''

	def dispatch getCode(Ellipse e, CharSequence currentGaName, CharSequence currentPeName) '''
		«org.eclipse.graphiti.mm.algorithms.Ellipse.name» «currentGaName» = gaService.createPlainEllipse(«currentPeName»);
	'''

	def dispatch getCode(Rectangle r, CharSequence currentGaName, CharSequence currentPeName) '''
		«org.eclipse.graphiti.mm.algorithms.Rectangle.name» «currentGaName» = gaService.createPlainRectangle(«currentPeName»);
	'''

	def dispatch getCode(RoundedRectangle rr, CharSequence currentGaName, CharSequence currentPeName) '''
		«org.eclipse.graphiti.mm.algorithms.RoundedRectangle.name» «currentGaName» = gaService.createPlainRoundedRectangle(«currentPeName», «rr.
			cornerWidth», «rr.cornerHeight»);
	'''
	/**Thus
	 * Generates a polygon 
	 * @param p : The polygon that will be generated
	 * @param currentGaName : The 'Impl' of a shape e.g. 'polygonImpl25'
	 * @param currentPeName : The 'Shape' of a 'Impl' of a shape e.g. 'polygonImplSHAPE25
	 */
	def dispatch getCode(Polygon p, CharSequence currentGaName, CharSequence currentPeName)'''
		points = new «int»[] {«p.points.map[x +"," + y].join(",")»};
				
		xs = new «int»[] {«p.points.map[x].join(",")»};
		ys = new «int»[] {«p.points.map[y].join(",")»};
		
		minX = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().min(xs);
		maxX = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().max(xs);
		
		minY = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().min(ys);
		maxY = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().max(ys);
		
		
		«mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().transform(points, -minX, -minY);
		width = maxX - minX;
		height = maxY - minY;	
		
		parentIsDiagram = («currentPeName».getContainer() instanceof «Diagram.name»);
			
		«org.eclipse.graphiti.mm.algorithms.Polygon.name» «currentGaName» = gaService.createPolygon(«currentPeName», points);
		
		
		«mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().transform(points, -minX, -minY);
		
		gaService.setSize(«currentGaName», width, height);
		
		pointsString = "";
		for (int i : points) {
			pointsString += String.valueOf(i)+",";
		} //«mglModel.packageName».«mglModel.fuName»LayoutUtils.KEY_INITIAL_POINTS, pointsString);
		peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_INITIAL_POINTS, pointsString);
		
		if(!(«currentPeName».getContainer() instanceof «Diagram.name») && «p.parentContainerShape instanceof ContainerShape») {
			peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_INITIAL_PARENT_SIZE, "" + «currentPeName».getContainer().getGraphicsAlgorithm().getWidth() + "," + «currentPeName».getContainer().getGraphicsAlgorithm().getHeight());
		} else peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_INITIAL_PARENT_SIZE, "" + «currentPeName».getGraphicsAlgorithm().getWidth() + "," + «currentPeName».getGraphicsAlgorithm().getHeight()); 
		
			«mglModel.packageName».«mglModel.fuName»LayoutUtils.set_«mglModel.fuName»DefaultAppearanceStyle(«currentGaName», getDiagram());
			gaService.setLocation(«currentGaName», 0, 0);
			peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_HORIZONTAL, «CincoLayoutUtils.typeName».KEY_HORIZONTAL_UNDEFINED);
			peService.setPropertyValue(«currentGaName», «CincoLayoutUtils.typeName».KEY_VERTICAL,«CincoLayoutUtils.typeName».KEY_VERTICAL_UNDEFINED);
		
	'''
	/**
	 * Generates the variable declaration that is need for the getCode-Method for polygon and polyline
	 */	
	def polyVarDeclaration() '''
		boolean parentIsDiagram = false;
		int width = 0;
		int height = 0;
		int minX = 0;
		int minY = 0; 
		int maxX = 0;
		int maxY = 0;
		int[] points;  
		int[]xs;
		int[] ys;
		String pointsString = "";
	'''
	/**
	 * Generates a polyline 
	 * @param p : The polyline that will be generated
	 * @param currentGaName : The 'Impl' of a shape e.g. 'polylineImpl'
	 * @param currentPeName : The 'Shape' of a 'Impl' of a shape e.g. 'polylineImplSHAPE'
	 */
	def dispatch getCode(Polyline p, CharSequence currentGaName, CharSequence currentPeName) '''

		points = new «int»[] {«p.points.map[ x+","+y].join(",")»};
				
		xs = new «int»[] {«p.points.map[x].join(",")»};
		ys = new «int»[] {«p.points.map[y].join(",")»};
		
		minX = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().min(xs);
		maxX = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().max(xs);
		
		minY = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().min(ys);
		maxY = «mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().max(ys);
		
		
		width = maxX - minX;
		height = maxY - minY;	
		
		«org.eclipse.graphiti.mm.algorithms.Polyline.name» «currentGaName» = gaService.createPlainPolyline(«currentPeName»);
		
		parentIsDiagram = («currentPeName».getContainer() instanceof «Diagram.name»);
		
		if (parentIsDiagram || minX < 0 || minY < 0)
			«mglModel.packageName».«mglModel.fuName»GraphitiUtils.getInstance().transform(points, -minX, -minY);
		
		pointsString = "";
		for (int i : points) {
			pointsString += String.valueOf(i)+",";
		}
		
		if (!parentIsDiagram) {
			«currentGaName» = gaService.createPolyline(«currentPeName», points);
			peService.setPropertyValue(«currentGaName», «CincoLayoutUtils.typeName».KEY_INITIAL_PARENT_SIZE, "" + «currentPeName».getContainer().getGraphicsAlgorithm().getWidth() + "," + «currentPeName».getContainer().getGraphicsAlgorithm().getHeight()); 
		} else {
			«currentGaName» = gaService.createPolyline(«currentPeName», new int[] {0,0,10,10,20,0,});
			peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_INITIAL_PARENT_SIZE, "" + width + "," + height); 
		}
		
		peService.setPropertyValue(«currentGaName», «CincoLayoutUtils.typeName».KEY_INITIAL_POINTS, pointsString);
		«mglModel.packageName».«mglModel.fuName»LayoutUtils.set_«mglModel.fuName»DefaultAppearanceStyle(«currentGaName», getDiagram());
		gaService.setLocation(«currentGaName», 0, 0);
		peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_HORIZONTAL,«CincoLayoutUtils.typeName».KEY_HORIZONTAL_UNDEFINED);
		peService.setPropertyValue(«currentGaName»,«CincoLayoutUtils.typeName».KEY_VERTICAL, «CincoLayoutUtils.typeName».KEY_VERTICAL_UNDEFINED);
		
		
	'''

	def dispatch getCode(Text t, CharSequence currentGaName, CharSequence currentPeName) '''
		«org.eclipse.graphiti.mm.algorithms.Text.name» «currentGaName» = gaService.createPlainText(«currentPeName»);
		
		«««Hier muss der Code generiert werden, der den anzuzeigenden Wert aus der zugehörigen @style annotation ausliest
		
		«ExpressionFactoryImpl.name» factory = new com.sun.el.ExpressionFactoryImpl();
		«LinkedList.name» <«Shape.name»>linkingList = new «LinkedList.name» <«Shape.name»>();
		linkingList.add(«currentPeName.toString»);
		«ClassLoader.name» contextClassLoader = «Thread.name».currentThread().getContextClassLoader();
		try {
			«currentGaName.toString».setFilled(false);
			«Thread.name».currentThread().setContextClassLoader(AddFeature«node.name».class.getClassLoader());
		
			«ExpressionLanguageContext.name» elContext = 
				new «ExpressionLanguageContext.name»(bo);
			
			«String.name» _expression = "«node.text.stringEscape»";
			«Object.name» _values[] = new «Object.name»[_expression.split(";").length];
			for (int i=0; i < _values.length; i++)
				_values[i] = "";
				
			for (int i=0; i < _expression.split(";").length;i++) {
				_values[i] = factory.createValueExpression(elContext, _expression.split(";")[i], «Object.name».class).getValue(elContext);
			}
			
			peService.setPropertyValue(«currentGaName.toString», «mglModel.packageName».«mglModel.fuName»GraphitiUtils.KEY_FORMAT_STRING, "«t.value.stringEscape»");

			peService.setPropertyValue(«currentGaName.toString», "Params", "«node.text.stringEscape»");
			«currentGaName.toString».setValue(String.format("«t.value.stringEscape»", _values));
		} catch (java.util.IllegalFormatException ife) {
			«currentGaName.toString».setValue("STRING FORMAT ERROR");
		} catch («ELException.name» ele) {
			if (ele.getCause() instanceof «NullPointerException.name»)
				«currentGaName».setValue("null");
		} finally {
			«Thread.name».currentThread().setContextClassLoader(contextClassLoader);
		}
		
	'''

	def dispatch getCode(MultiText t, CharSequence currentGaName, CharSequence currentPeName) '''
		«org.eclipse.graphiti.mm.algorithms.MultiText.name» «currentGaName» = gaService.createPlainMultiText(«currentPeName»);
		
		«ExpressionFactoryImpl.name» factory = new com.sun.el.ExpressionFactoryImpl();
		«LinkedList.name» <«Shape.name»>linkingList = new «LinkedList.name» <«Shape.name»>();
		linkingList.add(«currentPeName.toString»);
		«ClassLoader.name» contextClassLoader = «Thread.name».currentThread().getContextClassLoader();
		try {
			«currentGaName.toString».setFilled(false);
			«Thread.name».currentThread().setContextClassLoader(AddFeature«node.name».class.getClassLoader());
		
			«ExpressionLanguageContext.name» elContext = 
				new «ExpressionLanguageContext.name»(bo);
				
			«Object.name» tmp0Value = factory.createValueExpression(elContext, "«node.text.stringEscape»", «Object.name».class).getValue(elContext);
		
			peService.setPropertyValue(«currentGaName.toString», «mglModel.packageName».«mglModel.fuName»GraphitiUtils.KEY_FORMAT_STRING, "«t.value.stringEscape»");

			peService.setPropertyValue(«currentGaName.toString», "Params", "«node.text.stringEscape»");
			if (tmp0Value != null)
				«currentGaName.toString».setValue(String.format("«t.value.stringEscape»", ((«String.name») tmp0Value).split(";")));
			else «currentGaName.toString».setValue("");
		} catch (java.util.IllegalFormatException ife) {
			«currentGaName.toString».setValue("STRING FORMAT ERROR");
		} catch («ELException.name» ele) {
			if (ele.getCause() instanceof «NullPointerException.name»)
				«currentGaName».setValue("null");
		} finally {
			«Thread.name».currentThread().setContextClassLoader(contextClassLoader);
		}
	'''

	def dispatch getCode(Image p, CharSequence currentGaName, CharSequence currentPeName) '''
		«org.eclipse.graphiti.mm.algorithms.Image.name» «currentGaName» = gaService.createImage(«currentPeName», "«p.path»");
		«currentGaName».setStretchH(true);
		«currentGaName».setStretchV(true);
	'''

	def setSizeFromContext(AbstractShape a, CharSequence gaName) '''
		//Setting the size independent of the one given in the context. 
		//Thus, it is possible to resize inner shapes correctly
		«IF a instanceof Polygon»
			//If the graphicsAlgorithm is a polygon, set the computed size. (The condition is checked in the generator)
			gaService.setSize(«gaName», width, height);
		«ELSE»
			«IF a.size !== null»
				gaService.setSize(«gaName», «a.size?.width», «a.size?.height»);
			«ELSE»
				gaService.setSize(«gaName», 100, 50);
			«ENDIF»
		«ENDIF»
		«setSizeProperties(a, gaName)»
	'''
	
	def setSize(AbstractShape a, CharSequence gaName) '''
		gaService.setSize(«gaName», «a.size?.width», «a.size?.height»);
		«setSizeProperties(a, gaName)»
	'''
	
	def setSizeProperties(AbstractShape a,  CharSequence gaName) '''
		«IF a.size?.widthFixed»
			peService.setPropertyValue(«gaName», "«CincoResizeFeature.FIXED_WIDTH»", "«CincoResizeFeature.FIXED»");
		«ENDIF»
		«IF a.size?.heightFixed»
			peService.setPropertyValue(«gaName», "«CincoResizeFeature.FIXED_HEIGHT»", "«CincoResizeFeature.FIXED»");
		«ENDIF»
	'''

	def static setPosition(AbstractShape aShape, CharSequence gaName) '''
		«IF aShape.eContainer instanceof NodeStyle»
			«var a = aShape.position as AbsolutPosition»
			if (context.getWidth() < 0 && context.getHeight() < 0) {
				gaService.setLocation(«gaName», context.getX() + «a?.XPos», context.getY() + «a?.YPos»);
			}
			else {
				gaService.setLocation(«gaName», context.getX(), context.getY());
			}
		«ELSE»
			«var a = aShape.position»
			«IF a instanceof AbsolutPosition»
				gaService.setLocation(«gaName», «a?.XPos», «a?.YPos»);
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_HORIZONTAL»", "«CincoLayoutFeature.KEY_HORIZONTAL_UNDEFINED»");
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_VERTICAL»", "«CincoLayoutFeature.KEY_VERTICAL_UNDEFINED»");
			«ELSEIF a instanceof Alignment»
				«var Alignment alignment = a as Alignment»
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_HORIZONTAL»", "«alignment.getHorizontalValue»");
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_VERTICAL»", "«alignment.getVerticalValue»");
				
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_MARGIN_HORIZONTAL»", "«alignment?.XMargin»");
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_MARGIN_VERTICAL»", "«alignment?.YMargin»");
			«ELSEIF a === null»
				gaService.setLocation(«gaName», 0, 0);
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_HORIZONTAL»", "«CincoLayoutFeature.KEY_HORIZONTAL_UNDEFINED»");
				peService.setPropertyValue(«gaName», "«CincoLayoutFeature.KEY_VERTICAL»", "«CincoLayoutFeature.KEY_VERTICAL_UNDEFINED»");
			«ENDIF»
		«ENDIF»
	'''

	def static setAppearanceCode(AbstractShape abs, Appearance app, CharSequence gaName) '''
		{
			«Style.name» s = gaService.createPlainStyle(getDiagram(), "«if(app.name.isNullOrEmpty) "tmpStyle" else app.name»");
			«IF app.background !== null»
				s.setBackground(gaService.manageColor(getDiagram(), «app.background.r», «app.background.g», «app.background.b»));
			«ENDIF»
			«IF app.foreground !== null»
				s.setForeground(gaService.manageColor(getDiagram(), «app.foreground.r», «app.foreground.g», «app.foreground.b»));
			«ENDIF»
			«IF app.font !== null»
				s.setFont(gaService.manageFont(getDiagram(), «app.font.fontName», «app.font.size», «app.font.isIsItalic», «app.font.
			isIsBold»));
			«ENDIF»
			«IF !app.lineStyle.equals(LineStyle.UNSPECIFIED)»
				s.setLineStyle(«org.eclipse.graphiti.mm.algorithms.styles.LineStyle.name».«app.lineStyle»);
			«ENDIF»
			«IF app.angle != -1.0»
				s.setRotation(«app.angle»);
			«ENDIF»
			«IF !app.filled.equals(BooleanEnum.UNDEF)»
				s.setFilled(«app.filled»);
			«ENDIF»
			s.setLineVisible(«!app.lineInVisible»);
			s.setLineWidth(«app.lineWidth»);
			s.setTransparency(«app.transparency»);
			«gaName».setStyle(s);
		}
	'''

	def call(ConnectionDecorator cd, Edge e, int cdIndex) {
		if (cd.predefinedDecorator !== null) return cd.predefinedDecorator.cdCall(e, cdIndex)
		if (cd.decoratorShape !== null) return cd.decoratorShape.cdShapeCall(e)
	}

	
	def cdCall(PredefinedDecorator pd, Edge e, int cdIndex) '''
		«val dummyVal = "cdDummy" + cdIndex»
		org.eclipse.graphiti.mm.algorithms.Polyline «dummyVal» = gaService.createPolyline(null);
		«pd.appearanceCode(dummyVal)»
		de.jabc.cinco.meta.core.ge.style.generator.runtime.utils.CincoLayoutUtils.create«pd.shape.getName»(cd, «dummyVal».getLineWidth());
		«pd.appearanceCode("cd.getGraphicsAlgorithm()")»
	'''
	
	def cdShapeCall(GraphicsAlgorithm ga, Edge e) {
		if (ga instanceof Text || ga instanceof MultiText)
		'''createShape«ga.hashName»(cd, («e.fqInternalBeanName») «e.flName», "«ga.value»", "«e.text»");'''
		else 
		'''createShape«ga.hashName»(cd, («e.fqInternalBeanName») «e.flName», «ga.size?.width», «ga.size?.height»);'''
	}
	
	def code(ConnectionDecorator cd, Edge e) {
		if (cd.predefinedDecorator !== null) return cd.predefinedDecorator.cdCode(e)
		if (cd.decoratorShape !== null) return cd.decoratorShape.cdShapeCode(e)
	}
	
	def cdShapeCode(GraphicsAlgorithm ga, Edge e) '''
		«IF (ga instanceof Text || ga instanceof MultiText)»
		private void createShape«ga.hashName»(«GraphicsAlgorithmContainer.name» gaContainer, «e.fqInternalBeanName» «e.flName», «String.name» textValue, «String.name» attrValue) {
		«ELSE»
		private void createShape«ga.hashName»(«GraphicsAlgorithmContainer.name» gaContainer, «e.fqInternalBeanName» «e.flName», int width, int height) {
		«ENDIF»
			«IGaService.name» gaService = «Graphiti.name».getGaService();
			«IPeService.name» peService = «Graphiti.name».getPeService();
			
			«ga.cdCode(e)»
			«IF ga instanceof Polyline || ga instanceof Polygon»
			«CincoLayoutUtils.name».mirrorXAxis((«org.eclipse.graphiti.mm.pictograms.ConnectionDecorator.name») gaContainer,«ga.simpleName.toLowerCase»);
			«ENDIF»			
			«IF ga.size !== null»
			gaService.setSize(«ga.simpleName.toLowerCase», width, height);
			«ELSE»
			gaService.setSize(«ga.simpleName.toLowerCase», 100, 25);
			«ENDIF»
			«appearanceCode(ga as AbstractShape,ga.simpleName.toLowerCase)»
		}
	'''
	
	def dispatch cdCode(Text ga, Edge e) '''
		«var currentGaName = "text"»
		«org.eclipse.graphiti.mm.algorithms.Text.name» «currentGaName» = gaService.createPlainText(gaContainer);
				
		«««Hier muss der Code generiert werden, der den anzuzeigenden Wert aus der zugehörigen @style annotation ausliest
		«ExpressionFactoryImpl.name» factory = new com.sun.el.ExpressionFactoryImpl();
		«LinkedList.name» <«Shape.name»>linkingList = new «LinkedList.name» <«Shape.name»>();
		«ClassLoader.name» contextClassLoader = «Thread.name».currentThread().getContextClassLoader();
		try {
			«currentGaName.toString».setFilled(false);
			«Thread.name».currentThread().setContextClassLoader(AddFeature«e.name».class.getClassLoader());
		
			«ExpressionLanguageContext.name» elContext = 
				new «ExpressionLanguageContext.name»(«e.flName»);
			
			«String.name» _expression = "«getText(e)»";
			«Object.name» _values[] = new «Object.name»[_expression.split(";").length];
			for (int i=0; i < _values.length; i++)
				_values[i] = "";
				
			for (int i=0; i < _expression.split(";").length;i++) {
				_values[i] = factory.createValueExpression(elContext, _expression.split(";")[i], «Object.name».class).getValue(elContext);
			}
			
		
			peService.setPropertyValue(«currentGaName.toString», «mglModel.packageName».«mglModel.fuName»GraphitiUtils.KEY_FORMAT_STRING, "«ga.value.stringEscape»");

			peService.setPropertyValue(«currentGaName.toString», "Params", "«e.text.stringEscape»");
			«currentGaName.toString».setValue(String.format("«ga.value.stringEscape»", _values));
		} catch (java.util.IllegalFormatException ife) {
			«currentGaName.toString».setValue("STRING FORMAT ERROR");
		} catch («ELException.name» ele) {
			if (ele.getCause() instanceof «NullPointerException.name»)
				«currentGaName».setValue("null");
		} finally {
			«Thread.name».currentThread().setContextClassLoader(contextClassLoader);
		}
	'''
	
	def dispatch cdCode(MultiText ga, Edge e) '''
		«MultiText.name» multitext = gaService.createMultiText(gaContainer);
		multitext.setFilled(false);
		«e.graphModel.packageName».expression.«e.graphModel.fuName»ExpressionLanguageContext elContext = new «e.graphModel.packageName».expression.«e.graphModel.fuName»ExpressionLanguageContext(«e.flName»);
		Object tmpValue = factory.createValueExpression(elContext, attrValue, «Object.name».class).getValue(elContext);
		
		peService.setPropertyValue(multitext, «mglModel.packageName».«mglModel.fuName»GraphitiUtils.KEY_FORMAT_STRING, textValue);
		multitext.setValue(String.format(textValue , tmpValue));
	'''
	
	def dispatch cdCode(Ellipse ga, Edge e) {
		ga.getCode("ellipse", "gaContainer")	
	}
	
	def dispatch cdCode(Rectangle ga, Edge e) {
		ga.getCode("rectangle", "gaContainer")
	} 
	
	def dispatch cdCode(RoundedRectangle ga, Edge e) {
		ga.getCode("roundedRectangle", "gaContainer")
	}
	
	def dispatch cdCode(Polyline p, Edge e) '''
		«org.eclipse.graphiti.mm.algorithms.Polyline.name» polyline = gaService.createPolyline(gaContainer, new int[] {«p.points.map[x +","+y].join(",")»});
	'''
	
	def dispatch cdCode(Polygon p, Edge e) '''
		«org.eclipse.graphiti.mm.algorithms.Polygon.name» polygon = gaService.createPolygon(gaContainer, new int[] {«p.points.map[x +","+y].join(",")»});
	'''
	def dispatch cdCode(Image ga, Edge e) {
		ga.getCode("image", "gaContainer")
	}
	
	
	def static setAppearance(AbstractShape aShape, CharSequence gaName) {
		var app = if (aShape.referencedAppearance === null) aShape.inlineAppearance else aShape.referencedAppearance
		var Appearance newApp = StyleFactory.eINSTANCE.createAppearance
		app.resolveParents(newApp)
		return setAppearanceCode(aShape, newApp, gaName)
	}

	def static void resolveParents(Appearance app, Appearance newApp) {
		if (app === null)
			return
		else
			app.parent.resolveParents(newApp)
		if (app.angle != -1.0)
			newApp.angle = app.angle
		if (app.lineWidth != -1)
			newApp.lineWidth = app.lineWidth
		if (app.lineInVisible !== null)
			newApp.lineInVisible = app.lineInVisible
		if (app.transparency != -1.0)
			newApp.transparency = app.transparency
		if (!app.filled.equals(BooleanEnum.UNDEF))
			newApp.filled = app.filled
		if (!app.imagePath.isNullOrEmpty)
			newApp.imagePath = app.imagePath
		if (!app.lineStyle.equals(LineStyle.UNSPECIFIED))
			newApp.lineStyle = app.lineStyle
		if (app.background !== null)
			newApp.background = EcoreUtil.copy(app.background)
		if (app.foreground !== null)
			newApp.foreground = EcoreUtil.copy(app.foreground)
		if (app.font !== null)
			newApp.font = EcoreUtil.copy(app.font)
	}

	static def containerIsDiagramOrMovable(AbstractShape abs) {
		return (abs.parentContainerShape === null)
	}
	
	static def shapeIsAnchorShape(AbstractShape shape) {
		return shape.isAnchorShape || (shape.parentContainerShape === null && !anyChildIsAnchorShape(shape))
	}
	
	static def boolean anyChildIsAnchorShape(AbstractShape shape) {
		if(shape instanceof ContainerShape) {
			return shape.children.exists[_anyChildIsAnchorShape]
		} else {
			return false
		}
	}
	
	static private def boolean _anyChildIsAnchorShape(AbstractShape shape) {
		if(shape instanceof ContainerShape) {
			return shape.isAnchorShape || shape.children.exists[_anyChildIsAnchorShape]
		} else {
			return shape.isAnchorShape
		}
	}

	static def getShapeName(AbstractShape aShape) {
		if(aShape.name.isNullOrEmpty) aShape.genSName() else aShape.name + "Shape";
	}

	static def getGAName(AbstractShape aShape) {
		if(aShape.name.isNullOrEmpty) aShape.genGName() else aShape.name;
	}

	def static getHorizontalValue(Alignment a) {
		switch a.horizontal {
			case LEFT: CincoLayoutFeature.KEY_HORIZONTAL_LEFT
			case CENTER: CincoLayoutFeature.KEY_HORIZONTAL_CENTER
			case RIGHT: CincoLayoutFeature.KEY_HORIZONTAL_RIGHT
			case UNDEFINED: CincoLayoutFeature.KEY_HORIZONTAL_UNDEFINED
		}
	}

	def static getVerticalValue(Alignment a) {
		switch a.vertical {
			case TOP: CincoLayoutFeature.KEY_VERTICAL_TOP
			case MIDDLE: CincoLayoutFeature.KEY_VERTICAL_MIDDLE
			case BOTTOM: CincoLayoutFeature.KEY_VERTICAL_BOTTOM
			case UNDEFINED: CincoLayoutFeature.KEY_VERTICAL_UNDEFINED
		}
	}

	def static genSName(AbstractShape aShape) '''«aShape.class.simpleName.toFirstLower»SHAPE«num»'''

	def static genGName(AbstractShape aShape) '''«aShape.class.simpleName.toFirstLower»«num++»'''

	def recursiveCall(AbstractShape aShape, String containerShapeName) {
		if (aShape instanceof ContainerShape) {
			aShape.children.map['''
				{
					«getCode(containerShapeName)»
				}
			'''].join("\n")
		}
	}
	
	
	def getText(GraphicalModelElement gme) {
		return gme.getText(gme.styleParameters)
	} 
	
	/**
	 * Auxiliary method to get the style parameters of a {@link GraphicalModelElement}
	 * Sanitizes expressions to contain the actual referenced values
	 * @param gme the {@link GraphicalModelElement} for which the style parameters should be returned
	 * @return Returns the string with the text of a node
	 */
	private def getText(GraphicalModelElement gme, EList<String> vals) {
		val attrPattern = "\\$\\{(.*)\\}"
		if (gme instanceof Node && (gme as Node).isPrime) {
			val expPattner = Pattern.compile(attrPattern)
			vals.subList(0,vals.size).map[str| if(str.isNullOrEmpty) " " else str]
			.map[
				val m = expPattner.matcher(it)
				if (m.matches) {
					val primeString = MGLUtil::refactorIfPrimeAttribute(gme as Node,m.group(1))
					var i = 0
					val chars = primeString.toCharArray
					while (i != -1) {
						i = primeString.indexOf(".",i+1)
						if (i != -1)
							chars.set(i+1, Character.toLowerCase(chars.get(i+1)))					
					}
					new String(chars)
				} else it
			].join(";")	
			
		} else {
			vals.subList(0,vals.size)
				.map[str| if(str.isNullOrEmpty) " " else str]
				.map[str| 
					var i = 0
					val chars = str.toCharArray
					while (i != -1) {
						i = str.indexOf(".",i+1)
						if (i != -1)
							chars.set(i+1, Character.toLowerCase(chars.get(i+1)))					
					}
					val newStr = new String(chars)
					val m1 = Pattern.compile(attrPattern).matcher(newStr)
					if (m1.matches) {
						return m1.group.replace(m1.group(1), m1.group(1).toFirstLower)
					} else {
						return str
					}
				]
				.join(";")
		}
	}
	
	def size(GraphicsAlgorithm ga) {
		switch (ga) {
			AbstractShape: ga.size
		}
	}

	def value(GraphicsAlgorithm ga) {
		switch (ga) {
			Text: ga.value
			MultiText: ga.value
		}
	}

	def hashName(GraphicsAlgorithm ga) {
		ga.class.simpleName.replaceFirst("Impl", "")+ga.hashCode
	}
	
	def simpleName(GraphicsAlgorithm ga) {
		ga.class.simpleName.replaceFirst("Impl", "")
	}
	
	def getGaName(ConnectionDecorator cd) {
		if (!cd.name.isNullOrEmpty) return cd.name
		else {
			if (cd.predefinedDecorator !== null) return cd.predefinedDecorator.shape.toString+index++
			if (cd.decoratorShape !== null) return "Shape"+index++
		} 
	}
	
	def getFName(Font f) {
		if (f === null) "Arial" else f.fontName
	}
	
	def getFontSize(Font f) {
		if (f === null) 8 else f.size
	}
	
	def isBold(Font f) {
		if (f === null) false else f.isIsBold
	}
	
	def isItalic(Font f) {
		if (f === null) false else f.isIsItalic
	}
}

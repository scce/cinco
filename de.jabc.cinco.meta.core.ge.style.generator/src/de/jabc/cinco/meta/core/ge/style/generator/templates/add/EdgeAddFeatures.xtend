/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.add

import com.sun.el.ExpressionFactoryImpl
import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CEdge
import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoAddFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.utils.CincoLayoutUtils
import de.jabc.cinco.meta.core.ge.style.generator.templates.LayoutFeatureTmpl
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.StyleUtil
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import graphmodel.internal.InternalFactory
import graphmodel.internal.InternalIdentifiableElement
import graphmodel.internal._Decoration
import graphmodel.internal._Point
import mgl.Edge
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.IUpdateFeature
import org.eclipse.graphiti.features.context.IAddConnectionContext
import org.eclipse.graphiti.features.context.IAddContext
import org.eclipse.graphiti.features.context.impl.UpdateContext
import org.eclipse.graphiti.mm.algorithms.Polyline
import org.eclipse.graphiti.mm.algorithms.styles.Point
import org.eclipse.graphiti.mm.pictograms.Connection
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import org.eclipse.graphiti.services.IGaService
import org.eclipse.graphiti.services.IPeService
import style.EdgeStyle
import style.Styles

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.*

class EdgeAddFeatures extends APIUtils {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension StyleUtil = new StyleUtil
	
	/**
	 * Generates the Class 'AddFeature' for the Edge e
	 * @param e : The edge
	 * @param styles : Styles
	 */
	def doGenerateEdgeAddFeature(Edge e, Styles styles) '''
		package «e.packageNameAdd»;
		
		public class AddFeature«e.fuName» extends «CincoAddFeature.name» {
			
			private static «ExpressionFactoryImpl.name» factory = new «ExpressionFactoryImpl.name»();
			«««TODO modularization: Check need of elContext
			//private static  «e.packageName».expression.«e.fuName»ExpressionLanguageContext elContext = null;
		
		  	public AddFeature«e.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
		
			/**
			 * Adds a pictogram element to a container shape
			 * @param context : Contains Information needed to let a feature add a pictogram element
			 * @return Returns the connection
			 */
			public «PictogramElement.name» add(«IAddContext.name» context) {
				«IAddConnectionContext.name» addConContext = («IAddConnectionContext.name») context;
				«e.fqInternalBeanName» «e.flName» = («e.fqInternalBeanName») context.getNewObject();
				«IPeService.name» peService = «Graphiti.name».getPeService();
		       
				«Connection.name» connection = peService.createFreeFormConnection(getDiagram());
				connection.setStart(addConContext.getSourceAnchor());
				connection.setEnd(addConContext.getTargetAnchor());
				
				«IGaService.name» gaService = «Graphiti.name».getGaService();
				«Polyline.name» polyline = gaService.createPolyline(connection);
				«e.getStyleForEdge(styles).appearanceCodeEdge("polyline", e)»
				«Graphiti.name».getPeService().setPropertyValue(
					polyline, «CincoLayoutUtils.name».KEY_GA_NAME, "connection");
				
				«Object.name» sourceBo = addConContext.getSourceAnchor().getParent().getLink().getBusinessObjects().get(0);
				«Object.name» targetBo = addConContext.getTargetAnchor().getParent().getLink().getBusinessObjects().get(0);
				
				«ClassLoader.name» contextClassLoader;
		
				boolean insert = true;
				for(«_Point.name» point :  «e.flName».getBendpoints()){
					«Point.name» p = gaService.createPoint(point.getX(), point.getY());
					for(«Point.name» bend: ((«FreeFormConnection.name») connection).getBendpoints()){
						if(bend.getX() == point.getX() && bend.getY() == point.getY())
							insert = false;
					}
					if(insert)
						((«FreeFormConnection.name») connection).getBendpoints().add(p);
				}
				
				// create link and wire it
				link(connection, «e.flName»);
				«ConnectionDecorator.name» cd;
				«_Decoration.name» _d;// = «InternalFactory.name».eINSTANCE.create_Decoration();
				«_Point.name» _p;// = «InternalFactory.name».eINSTANCE.create_Point();
				«Polyline.name» dummy;
				«FOR d : e.getStyleForEdge(styles).decorator»
					_d = «InternalFactory.name».eINSTANCE.create_Decoration();
					_p = «InternalFactory.name».eINSTANCE.create_Point();
					cd = peService.createConnectionDecorator(connection, «d.movable»,«d.location», true);
					«val cdIndex = CincoUtil.getStyleForEdge(e, styles).decorator.indexOf(d)»
					«d.call(e, cdIndex)»
					«Graphiti.name».getPeService().setPropertyValue(
						cd.getGraphicsAlgorithm(), «CincoLayoutUtils.name».KEY_GA_NAME, "«d.gaName»");
					«IF d.decoratorShape !== null»
						_d.setNameHint(cd.getGraphicsAlgorithm().getClass().getSimpleName().substring(0,cd.getGraphicsAlgorithm().getClass().getSimpleName().lastIndexOf("Impl")));
						_p.setX(cd.getGraphicsAlgorithm().getX());
						_p.setY(cd.getGraphicsAlgorithm().getY());
					«ENDIF»
					_d.setLocation(«d.location»);
					_d.setLocationShift(_p);
					
					peService.setPropertyValue(cd, "cdIndex", "«cdIndex»");
					link(cd, «e.flName»);
					if («e.flName».getDecorators().size() <= «CincoUtil.getStyleForEdge(e, styles).decorator.indexOf(d)»)
						«e.flName».getDecorators().add(«CincoUtil.getStyleForEdge(e, styles).decorator.indexOf(d)», _d);
				«ENDFOR»
				
				((«CEdge.name») «e.flName».getElement()).setPictogramElement(connection);
				
				// sync bendpoint changes between diagram and model
				applyBendpointSynchronizer(connection);
		
				«UpdateContext.name» uc = new «UpdateContext.name»(connection);
				«IUpdateFeature.name» uf = getFeatureProvider().getUpdateFeature(uc);
				if (hook && uf.canUpdate(uc))
					«e.flName».getElement().updateGraphModel();
				return connection;
			}
		
			/**
			 * Checks if the context can be added
			 * @param context : Contains Information needed to let a feature add a pictogram element
			 * @return Returns true if the context can be added or false
			 */
			public boolean canAdd(«IAddContext.name» context) {
				if (context instanceof «IAddConnectionContext.name» && 
					context.getNewObject() instanceof «e.fqInternalBeanName») {
					return true;
				}
				return false;
			}
			
			«FOR d : CincoUtil.getStyleForEdge(e, styles).decorator.filter[decoratorShape !== null]»
				«d.code(e)»
			«ENDFOR»
			
			@Override
			protected void link(«PictogramElement.name» pe, «Object.name» bo) {
				if (bo instanceof «InternalIdentifiableElement.name») {
					bo = ((«InternalIdentifiableElement.name»)bo).getElement();
				}
				super.link(pe, bo);
			}
		}
	'''
	
	/**
	 * Returns the right setStyle-Methode for the Edge e
	 * @param shape : AbstractShape
	 * @param currentGaName : String
	 * @param e : The edge
	 */
	def appearanceCodeEdge(EdgeStyle shape, String currentGaName, Edge e) {
		if (shape.referencedAppearance !== null) '''«MGLUtil::getMglModel(e).packageName».«MGLUtil::getMglModel(e).fileName»LayoutUtils.set«shape.referencedAppearance.name»Style(«currentGaName», getDiagram());'''
		else if (shape.inlineAppearance !== null)  '''«MGLUtil::getMglModel(e).packageName».«MGLUtil::getMglModel(e).fileName»LayoutUtils.«LayoutFeatureTmpl.shapeMap.get(shape)»(«currentGaName», getDiagram());'''
		else '''«MGLUtil::getMglModel(e).packageName».«MGLUtil::getMglModel(e).fileName»LayoutUtils.set_«MGLUtil::getMglModel(e).fileName»DefaultAppearanceStyle(«currentGaName», getDiagram());'''
	}
	
}

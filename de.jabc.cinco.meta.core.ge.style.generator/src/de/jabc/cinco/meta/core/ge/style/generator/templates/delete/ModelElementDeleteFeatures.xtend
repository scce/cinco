/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.delete

import de.jabc.cinco.meta.core.ge.style.generator.runtime.features.CincoDeleteFeature
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.IdentifiableElement
import graphmodel.internal.InternalGraphModel
import mgl.ModelElement
import org.eclipse.emf.ecore.EObject
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IDeleteContext
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import style.Styles

class ModelElementDeleteFeatures {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension
	
	/**
	 * Generates the Class 'DeleteFeature' for the graphmodel gm
 	 * @param n : Node
 	 * @param styles : Styles
 	 */
	def doGenerateModelElementDeleteFeature(ModelElement me, Styles styles)'''
		package «me.packageNameDelete»;
		
		public class DeleteFeature«me.fuName» extends «CincoDeleteFeature.name» {

			public DeleteFeature«me.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
			
			/**
			 * Checks if the node can be deleted
			 * @param context : DeleteContext
			 * @param apiCall : Checks if true
			 * @return Return true if it can be deleted
			 */
			public boolean canDelete(«IDeleteContext.name» context, boolean apiCall) {
				if (apiCall) {
					«PictogramElement.name» pe = context.getPictogramElement();
					«EObject.name» bo = «Graphiti.name».getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
					if (bo instanceof «IdentifiableElement.name»)
						bo = ((«IdentifiableElement.name») bo).getInternalElement_();
					if (bo instanceof «me.fqInternalBeanName») {
						«IF me.eventEnabled»
							return canDelete«me.fuName»Event((«me.fqInternalBeanName») bo);
						«ELSE»
							return true;
						«ENDIF»
					}
					return super.canDelete(context);
				}
				return false;
			}
			
			@Override
			public boolean canDelete(«IDeleteContext.name» context) {
				return canDelete(context, «!CincoUtil.isDeleteDisabled(me)»);
			}
			
			«IF me.eventEnabled»
				private boolean canDelete«me.fuName»Event(«me.fqInternalBeanName» ime) {
					«graphmodel.ModelElement.name» me = ime.getElement();
					if (me instanceof «me.fqBeanName») {
						«me.fqBeanName» element = («me.fqBeanName») me;
						«EventEnum.CAN_DELETE.getNotifyCallJava('''canDelete«me.fuName»EventResult''', me, 'element')»
						return canDelete«me.fuName»EventResult == null || canDelete«me.fuName»EventResult;
					}
					return false;
				}
				
			«ENDIF»
			@Override
			public void delete(«IDeleteContext.name» context) {
				«Object.name» bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
				if (bo instanceof «me.fqInternalBeanName») {
					«me.fqInternalBeanName» elm = («me.fqInternalBeanName») bo;
					«InternalGraphModel.name» intModel = elm.getRootElement();
					((«me.fqBeanName») elm.getElement()).delete();
					intModel.getElement().updateModelElements();
				}
			}
		
			@Override
			protected boolean getUserDecision(«IDeleteContext.name» context) {
				return true;
			}
		
		}
	''' 
	
}

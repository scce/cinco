/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.create

import de.jabc.cinco.meta.core.ge.style.generator.runtime.createfeature.CincoCreateEdgeFeature
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlighter
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.ModelElement
import graphmodel.Node
import graphmodel.internal.InternalNode
import mgl.Edge
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.ICreateConnectionContext
import org.eclipse.graphiti.mm.pictograms.Anchor
import org.eclipse.graphiti.mm.pictograms.Connection
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import style.Styles

import static extension de.jabc.cinco.meta.core.utils.CincoUtil.hasAnnotationDisableCreate

class EdgeCreateFeatures extends APIUtils {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension EventApiExtension = new EventApiExtension
	
	/**
	 * Generates the 'Create-Feature' for a given edge 
	 * @param e : The edge
	 * @param styles : The style
	 */
	def doGenerateEdgeCreateFeature(Edge e, Styles styles) '''
		package «e.packageNameCreate»;
		
		public class CreateFeature«e.fuName» extends «CincoCreateEdgeFeature.name»<«ModelElement.name»> {
			
			private «ICreateConnectionContext.name» context;
			
			private boolean doneChanges = false;
			
			/**
			 * Call of the Superclass
			 * @param fp : Fp is the parameter of the Superclass-Call
			*/
			public CreateFeature«e.fuName»(«IFeatureProvider.name» fp) {
				super(fp, "«e.fuName»", "Create a new edge: «e.fuName»");
			}
			
			/**
		     * Checks if a context can be created
			 * @param context : Contains the information, needed to let a feature create a connection
			 * @param apiCall : ApiCall shows if the Cinco Api is used
			 * @return Returns true if the context can be created and false if not
			*/
			public boolean canCreate(«ICreateConnectionContext.name» context, boolean apiCall) {
				if (apiCall) {
					«InternalNode.name» source = null;
					Object businessObjectSource = getBusinessObject(context.getSourcePictogramElement());
					if(businessObjectSource instanceof graphmodel.internal.InternalNode) {
						source = (graphmodel.internal.InternalNode) businessObjectSource;
					} else {
						source = (graphmodel.internal.InternalNode) getBusinessObject(context.getSourceAnchor());
					}
					«InternalNode.name» target = null;
					Object businessObjectTarget = getBusinessObject(context.getTargetPictogramElement());
					if(businessObjectTarget instanceof graphmodel.internal.InternalNode) {
						target = (graphmodel.internal.InternalNode) businessObjectTarget;
					} else {
						target = (graphmodel.internal.InternalNode) getBusinessObject(context.getTargetAnchor());
					}
					
					boolean srcOK = false;
					boolean trgOK = false;
					if (source != null && target != null) {
						srcOK = source.canStart(«e.fqBeanName».class);
						trgOK = target.canEnd(«e.fqBeanName».class);
					}
					if (! (srcOK && trgOK) && getError().equals(«ECincoError.name».OK))
						setError(«ECincoError.name».MAX_IN);
					«IF e.eventEnabled»
						return srcOK && trgOK && canCreate«e.fuName»Event(source, target);
					«ELSE»
						return srcOK && trgOK;
					«ENDIF»
				}
				return false;
			}
			«IF e.eventEnabled»
				
				private boolean canCreate«e.fuName»Event(«InternalNode.name» source, «InternalNode.name» target) {
					«EventEnum.CAN_CREATE_EDGE.getNotifyCallJava('''canCreate«e.fuName»EventResult''', e, '''«e.fqBeanName».class''', '''(«Node.name») source.getElement()''', '''(«Node.name») target.getElement()''')»
					return canCreate«e.fuName»EventResult == null || canCreate«e.fuName»EventResult;
				}
			«ENDIF»
			
			/**
			 * Checks if a context can be created by using the method 'canCreate(context,apiCall)'
			 * @param context : Contains the information, needed to let a feature create a connection
			 * @return Returns true if the context can be created and false if not
			*/
			public boolean canCreate(«ICreateConnectionContext.name» context) {
				return canCreate(context, «!e.hasAnnotationDisableCreate»);
			}
		
			/**
			 * Creates a connection between a source and a target and returns it
			 * @param context : Contains the information, needed to let a feature create a connection
			 * @return Returns the new connection between source and target
			*/
			@Override
			public «Connection.name» create(«ICreateConnectionContext.name» context) {
				«Connection.name» connection = null;
				«InternalNode.name» source = null;
				Object businessObjectSource = getBusinessObject(context.getSourcePictogramElement());
				if(businessObjectSource instanceof graphmodel.internal.InternalNode) {
					source = (graphmodel.internal.InternalNode) businessObjectSource;
				} else {
					source = (graphmodel.internal.InternalNode) getBusinessObject(context.getSourceAnchor());
				}
				«InternalNode.name» target = null;
				Object businessObjectTarget = getBusinessObject(context.getTargetPictogramElement());
				if(businessObjectTarget instanceof graphmodel.internal.InternalNode) {
					target = (graphmodel.internal.InternalNode) businessObjectTarget;
				} else {
					target = (graphmodel.internal.InternalNode) getBusinessObject(context.getTargetAnchor());
				}
				
				if (source != null && target != null) {
					
					«e.fqBeanName» «e.flName» = 
						(«e.fqCName») 
						«MGLUtil::getMglModel(e).packageName».«MGLUtil::getMglModel(e).fuName»Factory.eINSTANCE.create«e.fuName»(source, target);
					
					if («e.flName» == null) {
						doneChanges = false;
						return null;
					}
					
					connection = ((«e.fqCName») «e.flName»).getPictogramElement();
				}
				doneChanges = true;
				return connection;
			}	
		
			/**
			 * Checks if a connection can start at the source with the given edge
			 * @param context : Contains the information, needed to let a feature create a connection
			 * @return Returns true if the connection can start to create and false if not
			*/
			@Override
			public boolean canStartConnection(«ICreateConnectionContext.name» context) {
				this.context = context;
				«Object.name» source = getBusinessObject(context.getSourcePictogramElement());
				if (source instanceof «InternalNode.name») {	
					if (! ((«InternalNode.name») source).canStart(«e.fqBeanName».class)) {
						if (getError().equals(«ECincoError.name».OK))
							setError(«ECincoError.name».MAX_OUT);
					}
					else return true;
				}
				return false;
			}
		
			/**
			 * Returns the business object of the pictogram element 'anchor'
			 * @param anchor : Anchor is a representation of the model object 'Anchor'.
			 * @return Returns the business object of the pictogram element 'anchor'  or null if 'anchor' is null
			*/
			private «Object.name» getBusinessObject(«Anchor.name» anchor) {
				if (anchor != null) {
					«Object.name» bo = getBusinessObjectForPictogramElement(anchor.getParent());
					return bo;
				}
				return null;
			}
			
			private «Object.name» getBusinessObject(«PictogramElement.name» pe) {
				if (pe != null) {
					«Object.name» bo = getBusinessObjectForPictogramElement(pe);
					return bo;
				}
				return null;
			}

			/**
			 * Get-method for an error
			 * @return Returns an 'error' in which 'error' is  'ECincoError.OK'
			*/
			public «ECincoError.name» getError() {
				return error;
			}
			
			/**
			 * Set-method for an error
			 * @param error : Error is a value of the enum: MAX_CARDINALITY, MAX_IN, MAX_OUT, INVALID_SOURCE, INVALID_TARGET, INVALID_CONTAINER, INVALID_CLONE_TARGET, OK
			*/
			public void setError(«ECincoError.name» error) {
				this.error = error;
			}
			
			@Override
			public boolean hasDoneChanges() {
				return doneChanges;
			}
			
			«IF ! CincoUtil.isHighlightReconnectionDisabled(e)»
				private «String.name» highlightContextKey;
				
				@Override
				public void startConnecting() {
					super.startConnecting();
					highlightContextKey = «Highlighter.name».INSTANCE.get().onConnectionStart(this, context);
				}
				
				@Override
				public void canceledAttaching(«ICreateConnectionContext.name» context) {
					super.canceledAttaching(context);
					if (highlightContextKey != null) {
						«Highlighter.name».INSTANCE.get().onConnectionCancel(highlightContextKey);
						highlightContextKey = null;
					}
					doneChanges = false;
				}
				
				@Override
				public void endConnecting() {
					super.endConnecting();
					if (highlightContextKey != null) {
						«Highlighter.name».INSTANCE.get().onConnectionEnd(highlightContextKey);
						highlightContextKey = null;
					}
				}
			«ELSE»
				@Override
				public void canceledAttaching(«ICreateConnectionContext.name» context) {
					super.canceledAttaching(context);
					doneChanges = false;
				}
			«ENDIF»
		}
	'''
}

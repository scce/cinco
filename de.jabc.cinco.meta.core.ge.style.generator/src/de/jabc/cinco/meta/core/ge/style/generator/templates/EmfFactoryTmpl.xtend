/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CGraphModel
import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CModelElement
import de.jabc.cinco.meta.core.ge.style.generator.runtime.api.CNode
import de.jabc.cinco.meta.core.ge.style.generator.runtime.provider.CincoFeatureProvider
import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import graphmodel.ModelElementContainer
import graphmodel.Node
import graphmodel.internal.InternalContainer
import graphmodel.internal.InternalEdge
import graphmodel.internal.InternalFactory
import graphmodel.internal.InternalGraphModel
import graphmodel.internal.InternalModelElement
import graphmodel.internal.InternalModelElementContainer
import graphmodel.internal.InternalNode
import graphmodel.internal._Point
import mgl.Edge
import mgl.MGLModel
import mgl.Type
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.graphiti.dt.IDiagramTypeProvider
import org.eclipse.graphiti.features.IAddFeature
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.impl.AddConnectionContext
import org.eclipse.graphiti.features.context.impl.AddContext
import org.eclipse.graphiti.mm.pictograms.Anchor
import org.eclipse.graphiti.mm.pictograms.Diagram
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import org.eclipse.graphiti.ui.services.GraphitiUi

class EmfFactoryTmpl {
	
	extension APIUtils = new APIUtils()
	extension GeneratorUtils = GeneratorUtils.instance
	
	def generateFactory(MGLModel mgl) '''
		package «mgl.packageName»;

		public class «mgl.fuName»Factory extends «mgl.fqFactoryName» {
			
			public static «mgl.fuName»Factory eINSTANCE = new «mgl.fuName»Factory();
			
			@Override
			public «EObject.name» create(«EClass.name» eClass) {
				«FOR me : MGLUtil.modelElements(mgl).filter[!isIsAbstract]»
					if (eClass.getName().equals("«me.name»"))
						return create«me.fuName»();
				«ENDFOR»
				return super.create(eClass);
			}
			
			«modelElementsPart(mgl)»
			
			«FOR gm : mgl.graphModels.filter[!isAbstract]»
				@Override
				public «gm.fqBeanName» create«gm.fuName»() {
					«gm.fqInternalBeanName» ime = («gm.fqInternalBeanName») super.create«gm.fuName»().getInternalElement_();
					«gm.fqCName(mgl)» me = new «gm.fqCName(mgl)»();
					«EcoreUtil.name».setID(ime, me.getId()+"_INTERNAL");
					ime.setElement(me);
					return me;
				}
				
				public «gm.fqBeanName» create«gm.fuName»(«String.name» path, «String.name» fileName) {
					«IPath.name» filePath = new «Path.name»(path).append(fileName).addFileExtension("«gm.fileExtension»");
					
					«EventEnum.PRE_CREATE_GRAPH_MODEL.getNotifyCallJava(gm, '''«gm.fqBeanName».class''', 'fileName', 'filePath')»
					
					«URI.name» uri = «URI.name».createPlatformResourceURI(filePath.toOSString() ,true);
					«Resource.name» res = new «ResourceSetImpl.name»().createResource(uri);
					«String.name» fNameWithExt = (fileName.contains(".")) ? fileName : fileName.concat(".«gm.fileExtension»");
					«String.name» dName = fNameWithExt.split("\\.")[0];
					
					«gm.fqCName» model = («gm.fqCName») create«gm.fuName»();
					«Diagram.name» diagram = «Graphiti.name».getPeService().createDiagram("«gm.fuName»", dName, true);
					
					res.getContents().add(diagram);
					res.getContents().add(model.getInternalElement_());
					model.setPictogramElement(diagram);
					
					«IDiagramTypeProvider.name» dtp = «GraphitiUi.name».getExtensionManager().createDiagramTypeProvider(diagram, "«gm.dtpId»");
					model.setFeatureProvider(dtp.getFeatureProvider());
					dtp.getFeatureProvider().link(diagram, model.getInternalElement_());
					
					«IF MGLUtil::hasPostCreateHook(gm)»
					postCreates(model);
					«ENDIF»
					
					try {
						res.save(null);
					} catch (java.io.IOException e) {
						e.printStackTrace();
					}
					return model;
				}
			«ENDFOR»
			
			private «PictogramElement.name» addNode(«InternalModelElementContainer.name» parent, «InternalNode.name» node) {
				«AddContext.name» ac = new «AddContext.name»();
				ac.setNewObject(node);
				ac.setLocation(node.getX(), node.getY());
				ac.setSize(node.getWidth(), node.getHeight());
				ac.setTargetContainer(((«CModelElement.name») parent.getElement()).getPictogramElement());
				
				«IFeatureProvider.name» fp =
								(parent instanceof «InternalGraphModel.name»)
								? ((«CGraphModel.name») parent.getElement()).getFeatureProvider()
								: ((«CGraphModel.name») ((«InternalModelElement.name») parent).getRootElement().getElement()).getFeatureProvider();
				«IAddFeature.name» af = fp.getAddFeature(ac);
				if (fp instanceof «CincoFeatureProvider.name») {
					if (af.canAdd(ac)) {
						return af.add(ac);
					}
				}
				return null;
			}
			
			private «PictogramElement.name» addEdge(«InternalNode.name» source, «InternalNode.name» target, «InternalEdge.name» edge) {
				«Anchor.name» sAnchor = ((«CNode.name») source.getElement()).getAnchor();
				«Anchor.name» tAnchor = ((«CNode.name») target.getElement()).getAnchor();
				
				«AddConnectionContext.name» acc = new «AddConnectionContext.name»(sAnchor, tAnchor);
				acc.setNewObject(edge);
				
				«IFeatureProvider.name» fp = ((«CGraphModel.name») source.getRootElement().getElement()).getFeatureProvider();
				«IAddFeature.name» af = fp.getAddFeature(acc);
				if (fp instanceof «CincoFeatureProvider.name») {
					if (af.canAdd(acc)) {
						return af.add(acc);
					}
				}
				return null;
			}
			
		}
	'''
	
	def modelElementsPart(MGLModel mgl) {
		val modelElements = MGLUtil.modelElements(mgl, false).filter[!isIsAbstract].filter(Type).toSet
		'''
			«FOR me : modelElements»
				@Override
				public «me.fqBeanName» create«me.fuName»() {
					«me.fqInternalBeanName» ime = («me.fqInternalBeanName») super.create«me.fuName»().getInternalElement_();
					«me.fqCName(mgl)» me = new «me.fqCName(mgl)»();
					«EcoreUtil.name».setID(ime, me.getId()+"_INTERNAL");
					ime.setElement(me);
					return me;
				}
				
				«IF me instanceof mgl.Node»
					«IF me.prime»
						public «me.fqBeanName» create«me.fuName»(«String.name» libraryComponentUID, «InternalModelElementContainer.name» container, int x, int y, int width, int height) {
					«ELSE»
						public «me.fqBeanName» create«me.fuName»(«InternalModelElementContainer.name» container, int x, int y, int width, int height) {
					«ENDIF»
						«EventEnum.PRE_CREATE_NODE.getNotifyCallJava(me, '''«me.fqBeanName».class''', '''(«ModelElementContainer.name») container.getElement()''', 'x', 'y', 'width', 'height')»
						
						«me.fqInternalBeanName» ime = («me.fqInternalBeanName») create«me.fuName»().getInternalElement_();
						container.getModelElements().add(ime);
						ime.setX(x);
						ime.setY(y);
						ime.setWidth(width);
						ime.setHeight(height);
						«IF me.prime»
						ime.setLibraryComponentUID(libraryComponentUID);
						«ENDIF»
						addNode(container, ime);
						ime.getElement().update();
						«IF MGLUtil::hasPostCreateHook(me)»
							«me.MGLModel.packageName».«me.MGLModel.fuName»Factory.eINSTANCE.postCreates((«me.fqBeanName») ime.getElement());
						«ENDIF»
						return («me.fqBeanName») ime.getElement();
					}
				«ELSEIF me instanceof Edge»
					@Override
					public «me.fqBeanName» create«me.fuName»(«InternalNode.name» source, «InternalNode.name» target) {
						«EventEnum.PRE_CREATE_EDGE.getNotifyCallJava(me, '''«me.fqBeanName».class''', '''(«Node.name») source.getElement()''', '''(«Node.name») target.getElement()''')»						

						«me.fqInternalBeanName» ime = («me.fqInternalBeanName») create«me.fuName»().getInternalElement_();
						ime.set_sourceElement(source);
						ime.set_targetElement(target);
						
						if(source.equals(target)){
							int x = 0;
							int y = 0;
							
							if(source.getContainer() instanceof «InternalContainer.name»){
								«InternalContainer.name» con = («InternalContainer.name») source.getContainer();
								x = con.getX() + source.getX();
								y = con.getY() + source.getY();
							}
							else{
								x = source.getX();
								y = source.getY();
							}
							«_Point.name» p1 = «InternalFactory.name».eINSTANCE.create_Point();
							«_Point.name» p2 = «InternalFactory.name».eINSTANCE.create_Point();
							p1.setX(x - 30);
							p1.setY(y + 40);
							p2.setX(x - 30);
							p2.setY(y - 20);
							ime.getBendpoints().add(p1);
							ime.getBendpoints().add(p2);
						}
						
						source.getRootElement().getModelElements().add(ime);
						addEdge(source, target, ime);
						ime.getElement().update();
						«IF MGLUtil::hasPostCreateHook(me)»
							«me.MGLModel.packageName».«me.MGLModel.fuName»Factory.eINSTANCE.postCreates((«me.fqBeanName») ime.getElement());
						«ENDIF»
						return («me.fqBeanName») ime.getElement();
					}
				«ENDIF»
			«ENDFOR»
		'''
	}
	
}

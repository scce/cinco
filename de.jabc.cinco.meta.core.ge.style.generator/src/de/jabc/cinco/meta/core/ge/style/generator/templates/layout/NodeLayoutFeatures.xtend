/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates.layout

import de.jabc.cinco.meta.core.ge.style.generator.runtime.utils.CincoLayoutUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.Node
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.ILayoutContext
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature
import org.eclipse.graphiti.mm.pictograms.ContainerShape
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.mm.pictograms.Shape
import style.Styles

class NodeLayoutFeatures {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	/**
	 * Generates the Class 'LayoutFeature' for the Node n
	 * @param n : The node
	 * @param styles : Styles
	 */
	def doGenerateNodeLayoutFeature(Node n,Styles styles)'''
		package «n.packageNameLayout»;
		
		public class LayoutFeature«n.fuName» extends «AbstractLayoutFeature.name»{
					
			public LayoutFeature«n.fuName»(«IFeatureProvider.name» fp) {
				super(fp);
			}
			
			@Override
			public boolean canLayout(«ILayoutContext.name» context) {
				Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
				if (bo instanceof «n.fqBeanName»)
					return true;
				return false;
			}
			
			@Override
			public boolean layout(«ILayoutContext.name» context) {
				«PictogramElement.name» pe = context.getPictogramElement();
				if (pe instanceof «ContainerShape.name») {
					layout((«ContainerShape.name») pe);
					return true;
				}
				return false;
			}
			
			/** 
			 * Checks if the node was layouted
			 * @param cs : The containershape
			 * @return Returns true, if update process was successfull
			 */
			private boolean layout(«ContainerShape.name» cs) {
				for («Shape.name» child : cs.getChildren()) {
					«CincoLayoutUtils.name».layout(cs.getGraphicsAlgorithm(), child.getGraphicsAlgorithm());
					if (child instanceof «ContainerShape.name») {
						layout((«ContainerShape.name») child);
					}
				}
				return true;
			}
		}
	'''
	
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.templates

import de.jabc.cinco.meta.core.ge.style.generator.templates.util.APIUtils
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import mgl.GraphModel
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class GraphitiCustomFeatureTmpl extends APIUtils {
	
	extension GeneratorUtils = GeneratorUtils.instance
	
	def generateCustomFeature(GraphModel gm) '''
		package «gm.packageName»;
		
		import de.jabc.cinco.meta.runtime.action.CincoCustomAction;
		import graphmodel.IdentifiableElement;
		import graphmodel.internal.InternalModelElement;
		import org.eclipse.graphiti.features.IFeatureProvider;
		import org.eclipse.graphiti.features.context.ICustomContext;
		import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
		import org.eclipse.graphiti.features.custom.ICustomFeature;
		import org.eclipse.graphiti.services.Graphiti;
		import org.eclipse.graphiti.mm.pictograms.PictogramElement;
		
		class «gm.fuName»GraphitiCustomFeature<T extends IdentifiableElement> extends AbstractCustomFeature implements ICustomFeature{
			
			private CincoCustomAction<T> delegate;
			
			public «gm.fuName»GraphitiCustomFeature(IFeatureProvider fp) {
				super(fp);
			}
			
			public «gm.fuName»GraphitiCustomFeature(IFeatureProvider fp, CincoCustomAction<T> cca) {
				super(fp);
				this.delegate = cca;
			}
			
			@Override
			public String getDescription() {
				return delegate.getName();
			}
			
			@Override
			public String getName() {
				return delegate.getName();
			}
			
			@Override
			public boolean canExecute(ICustomContext context) {
				PictogramElement pe = context.getPictogramElements()[0];
				T bo = (T) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
				«FOR me : gm.modelElements»
					if («me.instanceofCheck("bo")») {
						«me.fqCName» «me.flCName» = new «me.fqCName»((«me.fqBeanName»)((«me.fqInternalBeanName») bo).getElement(), pe);
						return delegate.canExecute((T) «me.flCName»);
					}
				«ENDFOR»
				else {
					throw new RuntimeException("Error in canExecute with element: " + bo);
				}
			}
			
			@Override
			public void execute(ICustomContext context) {
				PictogramElement pe = context.getPictogramElements()[0];
				T bo = (T) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
				«FOR me : gm.modelElements»
					if («me.instanceofCheck("bo")») {
						«me.fqCName» «me.flCName» = new «me.fqCName»((«me.fqBeanName»)((«me.fqInternalBeanName») bo).getElement(),pe);
						delegate.execute((T) «me.flCName»);
					}
				«ENDFOR»
				else {
					throw new RuntimeException("Error in canExecute with element: " + bo);
				}
			}
			
			
		}
	'''
	
	
}

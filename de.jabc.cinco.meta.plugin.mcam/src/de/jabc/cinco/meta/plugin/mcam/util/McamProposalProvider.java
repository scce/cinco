/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.mcam.util;

import java.util.ArrayList;
import java.util.List;

import mgl.Annotation;

import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal.IReplacementTextApplier;

import de.jabc.cinco.meta.core.pluginregistry.proposalprovider.IMetaPluginAcceptor;

public class McamProposalProvider implements IMetaPluginAcceptor {

	public McamProposalProvider() {
	}

	@Override
	public List<String> getAcceptedStrings(Annotation annotation) {
		ArrayList<String> list = new ArrayList<>();
		if ("mcam_changemodule".equals(annotation.getName())) {
			list.add("Choose ChangeModule ...");
		}
		return list;
	}

	@Override
	public IReplacementTextApplier getTextApplier(Annotation annotation) {
		if ("mcam_changemodule".equals(annotation.getName())) {
			return new ChooseClassTextApplier(annotation);
		}
		return null;
	}

}

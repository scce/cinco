/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.mcam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;

import de.jabc.cinco.meta.core.utils.MGLUtil;
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import freemarker.template.TemplateException;
import mgl.Annotation;
import mgl.Attribute;
import mgl.ComplexAttribute;
import mgl.Edge;
import mgl.Enumeration;
import mgl.GraphModel;
import mgl.GraphicalElementContainment;
import mgl.IncomingEdgeElementConnection;
import mgl.MGLModel;
import mgl.ModelElement;
import mgl.Node;
import mgl.NodeContainer;
import mgl.OutgoingEdgeElementConnection;
import mgl.PrimitiveAttribute;
import mgl.Type;

public class McamImplementationGenerator {

	public static String mcamPackageSuffix = "mcam";

	public static String adapterPackageSuffix = "adapter";
	public static String cliPackageSuffix = "cli";
	public static String modulesPackageSuffix = "modules";
	public static String changeModulesPackageSuffix = "changes";
	public static String checkModulesPackageSuffix = "checks";

	private String mglName = null;
	private String mglModelId = null;
	private GraphModel gModel = null;
	private String graphModelName = null;

	private IProject mcamProject = null;
	private String mcamProjectBasePackage = null;
	private String graphModelProjectName = null;
	private String graphModelPackage = null;

	private ArrayList<String> changeModuleClasses = new ArrayList<>();
	private ArrayList<String> checkModuleClasses = new ArrayList<>();
	private String customMergeStrategy = "";

	public Map<String, Object> data = new HashMap<>();

	private boolean generateMerge = false;
	private boolean generateCheck = false;

	private ArrayList<HashMap<String, Object>> modelLabels = new ArrayList<>();
	private HashMap<ModelElement, ArrayList<Attribute>> entityAttributes = new HashMap<>();
	private HashMap<String, ArrayList<String>> fqTypeNames = new HashMap<>();
	private HashMap<String, ArrayList<String>> typeNames = new HashMap<>();
	private HashMap<ModelElement, ModelElement> extendsMap = new HashMap<>();
	
	private GeneratorUtils gu;

	public McamImplementationGenerator(MGLModel mglModel, GraphModel gModel, IProject project, String graphModelPackage,
			String graphModelProjectName) {
		super();
		gu = GeneratorUtils.getInstance();
		this.mglName = gu.getFileName(mglModel);
		this.gModel = gModel;
		this.graphModelName = gModel.getName();
		this.graphModelPackage = graphModelPackage;
		this.graphModelProjectName = graphModelProjectName;
		this.mglModelId = mglModel.getPackage();

		this.mcamProject = project;
		this.mcamProjectBasePackage = graphModelPackage + "." + mcamPackageSuffix;

		parseAnnotations();

		initEntityMaps();

		data.put("MGLModelName", this.mglName);
		data.put("MGLModelID",this.mglModelId);
		data.put("GraphModelName", this.graphModelName);
		data.put("GraphModelProjectName", this.graphModelProjectName);
		data.put("GraphModelExtension", this.gModel.getFileExtension());
		data.put("GraphModelPackage", this.graphModelPackage);

		data.put("AdapterPackage", this.mcamProjectBasePackage + "." + adapterPackageSuffix);
		data.put("ChangeModulePackage",
				this.mcamProjectBasePackage + "." + modulesPackageSuffix + "." + changeModulesPackageSuffix);
		data.put("CheckModulePackage",
				this.mcamProjectBasePackage + "." + modulesPackageSuffix + "." + checkModulesPackageSuffix);
		data.put("CliPackage", this.mcamProjectBasePackage + "." + cliPackageSuffix);

		data.put("ChangeModules", this.changeModuleClasses);
		data.put("CheckModules", this.checkModuleClasses);

		data.put("CustomMergeStrategy", this.customMergeStrategy);
		data.put("MergeStrategyClass", this.graphModelName + "MergeStrategy");

		data.put("ModelLabels", this.modelLabels);

		data.put("ContainerTypes", this.typeNames.get("Container"));
		data.put("FqContainerTypes", this.fqTypeNames.get("Container"));
		data.put("NodeTypes", this.typeNames.get("Node"));
		data.put("FqNodeTypes", this.fqTypeNames.get("Node"));
		data.put("EdgeTypes", this.typeNames.get("Edge"));
		data.put("FqEdgeTypes", this.fqTypeNames.get("Edge"));
	}

	public IProject getProject() {
		return mcamProject;
	}

	public String getMcamProjectBasePackage() {
		return mcamProjectBasePackage;
	}

	public boolean doGenerateMerge() {
		return generateMerge;
	}

	public boolean doGenerateCheck() {
		return generateCheck;
	}

	private void initEntityMaps() {
		entityAttributes.put(gModel, new ArrayList<Attribute>());
		typeNames.put("GraphModel", new ArrayList<String>());
		typeNames.get("GraphModel").add(gModel.getName());
		for (Attribute attribute : gModel.getAttributes()) {
			if (gModel.getExtends() != null && !gModel.getExtends().isIsAbstract())
				extendsMap.put(gModel, gModel.getExtends());
			entityAttributes.get(gModel).add(attribute);
		}

		typeNames.put("Container", new ArrayList<String>());
		fqTypeNames.put("Container", new ArrayList<String>());
		typeNames.put("Node", new ArrayList<String>());
		fqTypeNames.put("Node", new ArrayList<String>());
		typeNames.put("Edge", new ArrayList<String>());
		fqTypeNames.put("Edge", new ArrayList<String>());
		for (ModelElement me : MGLUtil.modelElements(gModel)) {
			if(me instanceof NodeContainer) {
				NodeContainer container = (NodeContainer) me;
				if (container.getExtends() != null)
					extendsMap.put(container, container.getExtends());
				typeNames.get("Container").add(container.getName());
				fqTypeNames.get("Container").add(MGLUtil.getFqn(container));
				entityAttributes.put(container, new ArrayList<Attribute>());
				for (Attribute attribute : container.getAttributes()) {
					entityAttributes.get(container).add(attribute);
				}
			} else if(me instanceof Node) {
				if (((Node) me).getExtends() != null)
					extendsMap.put(me, ((Node) me).getExtends());
				typeNames.get("Node").add(me.getName());
				fqTypeNames.get("Node").add(MGLUtil.getFqn(me));
				entityAttributes.put(me, new ArrayList<Attribute>());
				for (Attribute attribute : me.getAttributes()) {
					entityAttributes.get(me).add(attribute);
				}
			} else if(me instanceof Edge) {
				if (((Edge) me).getExtends() != null)
					extendsMap.put(me, ((Edge) me).getExtends());
				typeNames.get("Edge").add(me.getName());
				fqTypeNames.get("Edge").add(MGLUtil.getFqn(me));
				entityAttributes.put(me, new ArrayList<Attribute>());
				for (Attribute attribute : me.getAttributes()) {
					entityAttributes.get(me).add(attribute);
				}
			} 
			
		}
	}

	public String generate() {
		try {

			for (ModelElement element : entityAttributes.keySet()) {

				addLabelEntry(element);

				if (element.isIsAbstract())
					continue;

				if (generateMerge) {
					data.put("ModelElementType", "GraphModel");
					if (!(element instanceof GraphModel)) {

						if (element instanceof Edge) {
							data.put("ModelElementType", "Edge");
							generateAddEdgeChangeModule((Edge) element);
							generateDeleteEdgeChangeModule((Edge) element);
							generateSourceTargetChangeModule((Edge) element);
						}
						if (element instanceof Node) {
							data.put("ModelElementType", "Node");
							generateAddElementChangeModule(element);
							generateDeleteElementChangeModule(element);
							generateMoveResizeElementChangeModule(element);
						}
						if (element instanceof NodeContainer) {
							data.put("ModelElementType", "Container");
							generateAddElementChangeModule(element);
							generateDeleteElementChangeModule(element);
							generateMoveResizeElementChangeModule(element);
						}

					}

					ModelElement elementToGen = element;
					while (elementToGen != null) {
						for (Attribute attribute : entityAttributes.get(elementToGen)) {
							generateAttributeChangeModule(element, attribute);
						}
						elementToGen = extendsMap.get(elementToGen);
					}
				}
			}

			if (generateCheck) {
				generateCincoCheckModule(gModel);
				generateContainmentCheckModule(gModel);
				generateIncomingCheckModule(gModel);
				generateOutgoingCheckModule(gModel);
			}

			generateEntityId();
			generateModelAdapter();
			generateCliExecution();

			return "default";
		} catch (IOException | TemplateException e) {
			e.printStackTrace();
		}
		return "error";
	}

	private void parseAnnotations() {
		for (Annotation annotation : gModel.getAnnotations()) {
			if ("mcam".equals(annotation.getName())) {
				List<String> values = annotation.getValue();
				if (values.size() == 0 || values.contains("check"))
					generateCheck = true;
				if (values.size() == 0 || values.contains("merge"))
					generateMerge = true;
			}
			if ("mcam_changemodule".equals(annotation.getName()))
				changeModuleClasses.add(annotation.getValue().get(0));
			if ("mcam_checkmodule".equals(annotation.getName()))
				checkModuleClasses.add(annotation.getValue().get(0));
			if ("mcam_mergestrategy".equals(annotation.getName()))
				customMergeStrategy = annotation.getValue().get(0);
		}
	}

	private void addLabelEntry(ModelElement element) {
		ModelElement elementToLabel = element;
		boolean labelFound = false;
		while (element != null) {
			for (Attribute attribute : element.getAttributes()) {
				for (Annotation annotation : attribute.getAnnotations()) {
					if ("mcam_label".equals(annotation.getName())) {
						HashMap<String, Object> labelEntry = new HashMap<>();
						labelEntry.put("type", elementToLabel.getName());
						labelEntry.put("attribute", attribute.getName());
						labelEntry.put("isModelElement", (getModelElementType(attribute) != null));
						modelLabels.add(labelEntry);
						labelFound = true;
					}
				}
			}
			if (labelFound)
				break;
			element = extendsMap.get(element);
		}
	}
	
	private String getNameForAttribute(Attribute attribute) {
		if (attribute instanceof PrimitiveAttribute) {
			return ((PrimitiveAttribute) attribute).getType().getName();
		}
		if (attribute instanceof ComplexAttribute) {
			return ((ComplexAttribute) attribute).getType().getName();
		}
		return null;
	}

	private ModelElement getModelElementType(Attribute attribute) {
		// FIXME: Added cast during api overhaul
		for (ModelElement element : entityAttributes.keySet()) {
			if(getNameForAttribute(attribute).equals(element.getName()))
				return element;

		}
		return null;
	}

	private Type getEnumType(Attribute attribute) {
		// FIXME: Added cast during api overhaul
		for (Type type : MGLUtil.types(gModel)) {
			if (type instanceof Enumeration) {
				if (getNameForAttribute(attribute).equals(type.getName()))
					return type;
			}
		}

		return null;
	}

	private Set<ModelElement> getPossibleContainer(ModelElement element) {
		HashSet<ModelElement> possibleContainer = new HashSet<>();

		for (Node n : MGLUtil.getUsableNodes(gModel)) {
			if (!(n instanceof NodeContainer))
				continue;
			NodeContainer container = (NodeContainer) n;
			for (GraphicalElementContainment gec : container.getContainableElements()) {
				if (gec.eCrossReferences().size() > 0) {
					for (EObject eObj : gec.eCrossReferences()) {
						if (element.getName().equals(((ModelElement) eObj).getName())) {
							possibleContainer.add(container);
						}
					}
				} else {
					possibleContainer.add(container);
				}
			}
		}
		return possibleContainer;
	}

	private Set<ModelElement> getPossibleEdgeTargets(ModelElement element) {
		HashSet<ModelElement> possibleSources = new HashSet<>();

		for (Node node : MGLUtil.getUsableNodes(gModel)) {
			for (IncomingEdgeElementConnection incEdge : node.getIncomingEdgeConnections()) {
				if (incEdge.eCrossReferences().size() > 0) {
					for (EObject eObj : incEdge.eCrossReferences()) {
						if (element.getName().equals(((ModelElement) eObj).getName())) {
							possibleSources.add(node);
						}
					}
				} else {
					possibleSources.add(node);
				}

			}
		}

		for (Node n : MGLUtil.getUsableNodes(gModel)) {
			if (!(n instanceof NodeContainer))
				continue;
			NodeContainer container = (NodeContainer) n;
			for (IncomingEdgeElementConnection incEdge : container.getIncomingEdgeConnections()) {
				if (incEdge.eCrossReferences().size() > 0) {
					for (EObject eObj : incEdge.eCrossReferences()) {
						if (element.getName().equals(((ModelElement) eObj).getName())) {
							possibleSources.add(container);
						}
					}
				} else {
					possibleSources.add(container);
				}

			}
		}

		return possibleSources;
	}

	private Set<ModelElement> getPossibleEdgeSources(ModelElement element) {
		HashSet<ModelElement> possibleTargets = new HashSet<>();

		for (Node node : MGLUtil.getUsableNodes(gModel)) {
			for (OutgoingEdgeElementConnection outEdge : node.getOutgoingEdgeConnections()) {
				if (outEdge.eCrossReferences().size() > 0) {
					for (EObject eObj : outEdge.eCrossReferences()) {
						if (element.getName().equals(((ModelElement) eObj).getName())) {
							possibleTargets.add(node);
						}
					}
				} else {
					possibleTargets.add(node);
				}

			}
		}

		for (Node n : MGLUtil.getUsableNodes(gModel)) {
			if (!(n instanceof NodeContainer))
				continue;
			NodeContainer container = (NodeContainer) n;
			for (OutgoingEdgeElementConnection outEdge : container.getOutgoingEdgeConnections()) {
				if (outEdge.eCrossReferences().size() > 0) {
					for (EObject eObj : outEdge.eCrossReferences()) {
						if (element.getName().equals(((ModelElement) eObj).getName())) {
							possibleTargets.add(container);
						}
					}
				} else {
					possibleTargets.add(container);
				}

			}
		}

		return possibleTargets;
	}

	private void generateEntityId() throws IOException, TemplateException {
		TemplateGenerator templateGen = new TemplateGenerator("templates/adapter/EntityId.tpl", mcamProject);
		templateGen.setFilename(graphModelName + "Id.java");
		templateGen.setPkg((String) data.get("AdapterPackage"));
		templateGen.setData(data);
		templateGen.generateFile();
	}

	private void generateModelAdapter() throws IOException, TemplateException {
		TemplateGenerator templateGen = new TemplateGenerator("templates/adapter/ModelAdapter.tpl", mcamProject);
		templateGen.setFilename(graphModelName + "Adapter.java");
		templateGen.setPkg((String) data.get("AdapterPackage"));
		templateGen.setData(data);
		templateGen.generateFile();
	}

	private void generateCliExecution() throws IOException, TemplateException {
		TemplateGenerator templateGen = new TemplateGenerator("templates/cli/CliMain.tpl", mcamProject);
		templateGen.setFilename("CliMain.java");
		templateGen.setPkg((String) data.get("CliPackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		templateGen = new TemplateGenerator("templates/cli/CliExecution.tpl", mcamProject);
		templateGen.setFilename("CliExecution.java");
		templateGen.setPkg((String) data.get("CliPackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		templateGen = new TemplateGenerator("templates/cli/FrameworkExecution.tpl", mcamProject);
		templateGen.setFilename(graphModelName + "Execution.java");
		templateGen.setPkg((String) data.get("CliPackage"));
		templateGen.setData(data);
		templateGen.generateFile();
	}

	private void generateAttributeChangeModule(ModelElement element, Attribute attribute)
			throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "Attribute" + attribute.getName().substring(0, 1).toUpperCase()
				+ attribute.getName().substring(1) + "Change");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		data.put("AttributeName", attribute.getName());

		if (getModelElementType(attribute) != null) {
			data.put("AttributeCategory", "ModelElement");
			data.put("AttributeType",
					data.get("MGLModelID") +  "."  + data.get("MGLModelName").toString().toLowerCase()+ "." + ((ComplexAttribute) attribute).getType().getName());
		} else if (getEnumType(attribute) != null) {
			data.put("AttributeCategory", "Enum");
			data.put("AttributeType", data.get("GraphModelPackage") + "." + data.get("GraphModelName").toString().toLowerCase() + "." + getEnumType(attribute).getName());
		} else {
			data.put("AttributeCategory", "Normal");
			
			String attributeType;
			if(attribute instanceof PrimitiveAttribute){
				attributeType = EcorePackage.eINSTANCE.getEClassifier(((PrimitiveAttribute) attribute).getType().getName()).getInstanceClass().getName();
			}else if(attribute instanceof ComplexAttribute){
				attributeType = ((ComplexAttribute) attribute).getType().getName();
			}else{
				throw new RuntimeException(String.format("Attribute %s is neither PrimitiveAttibute nor ComplextAttribute", attribute.getName()));
			}
			
			Integer upperBound = attribute.getUpperBound();
			if (upperBound != null && upperBound != 1) {
				attributeType = "EList<" + attributeType + ">";
			}
			
			data.put("AttributeType", attributeType);
		}

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/AttributeChangeModule.tpl",
				mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateAddElementChangeModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "AddChange");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		data.put("PossibleContainer", getPossibleContainer(element));

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/AddElementModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateDeleteElementChangeModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "DeleteChange");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		data.put("PossibleContainer", getPossibleContainer(element));

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/DeleteElementModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateDeleteEdgeChangeModule(Edge element) throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "DeleteChange");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		Set<ModelElement> possibleEdgeSources = getPossibleEdgeSources(element);
		data.put("PossibleEdgeSources", possibleEdgeSources);
		Set<String> fqPossibleEdgeSources = new HashSet<String>();
		for (ModelElement me : possibleEdgeSources) {
			fqPossibleEdgeSources.add(MGLUtil.getFqn(me));
		}
		data.put("FqPossibleEdgeSources", fqPossibleEdgeSources);
		Set<ModelElement> possibleEdgeTargets = getPossibleEdgeTargets(element);
		data.put("PossibleEdgeTargets", possibleEdgeTargets);
		Set<String> fqPossibleEdgeTargets = new HashSet<String>();
		for (ModelElement me : possibleEdgeTargets) {
			fqPossibleEdgeTargets.add(MGLUtil.getFqn(me));
		}
		data.put("FqPossibleEdgeTargets", fqPossibleEdgeTargets);

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/DeleteEdgeModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateAddEdgeChangeModule(Edge element) throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "AddChange");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		Set<ModelElement> possibleEdgeSources = getPossibleEdgeSources(element);
		data.put("PossibleEdgeSources", possibleEdgeSources);
		Set<String> fqPossibleEdgeSources = new HashSet<String>();
		for (ModelElement me : possibleEdgeSources) {
			fqPossibleEdgeSources.add(MGLUtil.getFqn(me));
		}
		data.put("FqPossibleEdgeSources", fqPossibleEdgeSources);
		Set<ModelElement> possibleEdgeTargets = getPossibleEdgeTargets(element);
		data.put("PossibleEdgeTargets", possibleEdgeTargets);
		Set<String> fqPossibleEdgeTargets = new HashSet<String>();
		for (ModelElement me : possibleEdgeTargets) {
			fqPossibleEdgeTargets.add(MGLUtil.getFqn(me));
		}
		data.put("FqPossibleEdgeTargets", fqPossibleEdgeTargets);

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/AddEdgeModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateSourceTargetChangeModule(Edge element) throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "SourceTargetChange");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		Set<ModelElement> possibleEdgeSources = getPossibleEdgeSources(element);
		data.put("PossibleEdgeSources", possibleEdgeSources);
		Set<String> fqPossibleEdgeSources = new HashSet<String>();
		for (ModelElement me : possibleEdgeSources) {
			fqPossibleEdgeSources.add(MGLUtil.getFqn(me));
		}
		data.put("FqPossibleEdgeSources", fqPossibleEdgeSources);
		Set<ModelElement> possibleEdgeTargets = getPossibleEdgeTargets(element);
		data.put("PossibleEdgeTargets", possibleEdgeTargets);
		Set<String> fqPossibleEdgeTargets = new HashSet<String>();
		for (ModelElement me : possibleEdgeTargets) {
			fqPossibleEdgeTargets.add(MGLUtil.getFqn(me));
		}
		data.put("FqPossibleEdgeTargets", fqPossibleEdgeTargets);

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/SourceTargetChangeModule.tpl",
				mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateMoveResizeElementChangeModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", element.getName() + "MoveResizeChange");
		data.put("FqModelElementName", MGLUtil.getFqn(element));
		data.put("ModelElementName", element.getName());
		data.put("PossibleContainer", getPossibleContainer(element));

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/MoveResizeElementModule.tpl",
				mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".java");
		templateGen.setPkg((String) data.get("ChangeModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		changeModuleClasses.add((String) data.get("ChangeModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateCincoCheckModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", data.get("GraphModelName") + "Check");

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/CincoCheckModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".xtend");
		templateGen.setPkg((String) data.get("CheckModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();
	}

	private void generateContainmentCheckModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", data.get("GraphModelName") + "ContainmentCheck");

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/ContainmentCheckModule.tpl",
				mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".xtend");
		templateGen.setPkg((String) data.get("CheckModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		checkModuleClasses.add((String) data.get("CheckModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateIncomingCheckModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", data.get("GraphModelName") + "IncomingCheck");

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/IncomingCheckModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".xtend");
		templateGen.setPkg((String) data.get("CheckModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		checkModuleClasses.add((String) data.get("CheckModulePackage") + "." + (String) data.get("ClassName"));
	}

	private void generateOutgoingCheckModule(ModelElement element) throws IOException, TemplateException {
		data.put("ClassName", data.get("GraphModelName") + "OutgoingCheck");

		TemplateGenerator templateGen = new TemplateGenerator("templates/modules/OutgoingCheckModule.tpl", mcamProject);
		templateGen.setFilename((String) data.get("ClassName") + ".xtend");
		templateGen.setPkg((String) data.get("CheckModulePackage"));
		templateGen.setData(data);
		templateGen.generateFile();

		checkModuleClasses.add((String) data.get("CheckModulePackage") + "." + (String) data.get("ClassName"));
	}

}

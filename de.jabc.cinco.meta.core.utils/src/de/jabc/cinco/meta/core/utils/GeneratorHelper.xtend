/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils

import java.io.IOException
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.emf.codegen.ecore.generator.Generator
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter
import org.eclipse.emf.codegen.ecore.genmodel.util.GenModelUtil
import org.eclipse.emf.common.util.BasicMonitor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl

class GeneratorHelper {
	
	def static void generateGenModelCode(IProject project, String modelName) throws IOException {
		var IFile genModelFile = project.getFile('''src-gen/model/«modelName».genmodel''')
		if(!genModelFile.exists()) throw new IOException('''The file: «modelName».genmodel does not exist''');
		var ResourceSetImpl resourceSet = new ResourceSetImpl()
		var Resource res = resourceSet.getResource(
			URI.createPlatformResourceURI(genModelFile.getFullPath().toOSString(), true), true)
		res.load(null)
		for (EObject o : res.getContents()) {
			if (o instanceof GenModel) {
				var GenModel genModel = (o as GenModel)
				for (GenPackage gp : genModel.getUsedGenPackages()) {
					
					if (gp.getGenModel !== null && !gp.getGenModel().equals(genModel)) {
						genModel.getUsedGenPackages().add(gp)
					}
				}
				generateGenModelCode(genModel)
			}
		}
	}

	/** 
	 * convenience method for {@link #generateGenModelCode(IProject, String)} that extracts
	 * the project and model name from the provided MGL file
	 * @param mglModelFile
	 * @throws IOException
	 */
	def static void generateGenModelCode(IFile mglModelFile) throws IOException {
		var String modelName = if((mglModelFile.getName().endsWith(".mgl"))) mglModelFile.getName().split("\\.").
				get(0) else mglModelFile.getName()
		var IProject project = mglModelFile.getProject()
		generateGenModelCode(project, modelName)
	}

	/** 
	 * Generates Model Code from previously created/loaded Genmodel
	 * @param genModel
	 */
	def static void generateGenModelCode(GenModel genModel) {
		generateCode(genModel,GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE)
	}
	
	/** 
	 * Generates Edit Code from previously created/loaded Genmodel
	 * @param genModel
	 */
	def static void generateEditCode(GenModel genModel){
		generateCode(genModel,GenBaseGeneratorAdapter.EDIT_PROJECT_TYPE)
	}
	
	/** 
	 * Generates Editor Code from previously created/loaded Genmodel
	 * @param genModel
	 */
	def static void generateEditorCode(GenModel genModel){
		generateCode(genModel,GenBaseGeneratorAdapter.EDITOR_PROJECT_TYPE)
	}
	
	/** 
	 * Generates Tests Code from previously created/loaded Genmodel
	 * @param genModel
	 */
	def static void generateTestCode(GenModel genModel){
		generateCode(genModel,GenBaseGeneratorAdapter.TESTS_PROJECT_TYPE)
	}
	
	private def static void generateCode(GenModel genModel,String modelType){
		genModel.setCanGenerate(true)
		var Generator generator = GenModelUtil.createGenerator(genModel)
		generator.generate(genModel, modelType, new BasicMonitor())
	}
}

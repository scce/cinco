/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.messages;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class CincoMessageHandler {
	
	private static Display getDisplay() {
		var display = Display.getCurrent();
		return display != null ? display : Display.getDefault();
	}
	
	public static void showMessage(String message, String title) {
		Display display = getDisplay();
		if (display != null) {
			display.syncExec(() -> MessageDialog.openInformation(display.getActiveShell(), title, message));	
		}
		else {
			System.out.println(message);
		}
		
	}
	
	public static void showErrorMessage(String message, String title) {
		Display display = getDisplay();
		if (display != null) {
			display.syncExec(() -> MessageDialog.openError(display.getActiveShell(), title, message));
		}
		else {
			System.err.println(message);
		}
	}
	
	public static boolean showQuestion(String question, String title, boolean defaultValue) {
		Display display = getDisplay();
		if (display != null) {
			return MessageDialog.openQuestion(display.getActiveShell(), title, question);
		}
		else {
			return defaultValue;
		}
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.registry;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author Steve Bosselmann
 */
public interface IRegistry<K,T> {
	
	T lookup(K key);
	
	<U extends T> Map<K, U> lookupByType(Class<U> type);
	
	<U extends T> Map<K, U> lookupByType(Class<U> type, boolean includeSubTypes);
	
	List<K> keys();
	
	List<T> values();
}

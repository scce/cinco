/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.registry;

import java.util.Map;
import java.util.function.Function;

/**
 * 
 * @author Steve Bosselmann
 */
public class NonEmptyIdentityRegistry<K,V> extends IdentityRegistry<K,V> {

	// generated
	private static final long serialVersionUID = -2956005899058856244L;
	
	private Function<K,V> valueSupplier;
	
	public NonEmptyIdentityRegistry(Function<K,V> valueSupplier) {
		super();
		this.valueSupplier = valueSupplier;
	}
	
	protected NonEmptyIdentityRegistry(Function<K,V> valueSupplier, Map<K, V> map) {
		super(map);
		this.valueSupplier = valueSupplier;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V get(Object key) {
		V val = super.get(key);
		if (val != null)
			return val;
		val = valueSupplier.apply((K) key);
		put((K)key, val);
		return val;
	}
	
	public Function<K,V> getValueSupplier() {
		return valueSupplier;
	}
	
	public void setValueSupplier(Function<K,V> valueSupplier) {
		this.valueSupplier = valueSupplier;
	}
}

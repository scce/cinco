/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.job;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.runtime.SubMonitor;

public class ConcurrentWorkload extends Workload {

	private ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	private int tasksDone;
	private int quotaLeft;
	private Map<Task,Future<?>> results = new HashMap<>();
	private boolean done;
	private SubMonitor monitor;
	
	public ConcurrentWorkload(CompoundJob job, int percent) {
		super(job, percent);
	}
	
	@Override
	public void perform(SubMonitor monitor) {
		this.monitor = monitor;
		quotaLeft = quota;
		tasks = buildTasks();
		if (tasks.isEmpty()) {
			monitor.newChild(quota).subTask("");
		} else {
			tasks.forEach(this::submit);
			pool.shutdown();
			startMonitorUpdates();
			try {
				pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {}
			if (quotaLeft > 0) {
				monitor.newChild(quotaLeft);
				monitor.newChild(0);
			}
			done = true;
		}
	}
	
	private void submit(Task task) {
		results.put(task, pool.submit(task.runnable));
	}
	
	@Override
	protected void taskDone(Task task) {
		tasksDone++;
		currentTask = task;
		displayName = task.name;
		tickMonitor();
		super.taskDone(task);
	}
	
	protected void startMonitorUpdates() {
		new ReiteratingThread(500,200) {
			protected void work() {
				if (done || canceled) {
					quit();
				}
			}
			protected void tick() {
				updateMonitor();
			}
		}.start();
	}
	
	protected void updateMonitor() {
		if (monitor.isCanceled() || canceled) {
			if (!canceled) cancel();
			monitor.subTask("Job canceled, awaiting termination...");
		} else {
			String label = "Completed " + tasksDone + "/" + tasks.size() + ".";
			if (displayName != null)
				label += " Recent: " + displayName;
			monitor.subTask(label);
		}
	}
	
	protected void tickMonitor() {
		if (!monitor.isCanceled() && !canceled) {
			tick += (double) quota / tasks.size();
			monitor.newChild((int) tick);
			quotaLeft -= (int) tick;
			tick -= (int) tick;
		}
	}
	
	@Override
	protected void cancel() {
		pool.shutdownNow();
		super.cancel();
	}
	
	@Override
	public void requestCancel() {
		pool.shutdownNow();
		super.requestCancel();
	}
	
	public ConcurrentWorkload setMaxThreads(int max) {
		if (max < 0) {
			System.err.println("WARN: Value for max number of threads ignored: " + max);
			return this;
		}
		int nThreads = Runtime.getRuntime().availableProcessors();
		if (max > 0) {
			nThreads = Math.min(max, nThreads);
		}
		pool = Executors.newFixedThreadPool(nThreads);
		return this;
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.registry;

import java.util.UUID;
import java.util.function.Supplier;

/**
 * 
 * @author Steve Bosselmann
 */
public class InstanceRegistry<V> extends KeygenRegistry<String,V> {

	// generated
	private static final long serialVersionUID = 2098354111588724338L;
	
	private Supplier<V> supplier;
	private V instance;
	
	public InstanceRegistry() {
		super(v -> UUID.randomUUID().toString());
	}
	
	public InstanceRegistry(Supplier<V> instanceSupplier) {
		this();
		this.supplier = instanceSupplier;
	}
	
	public boolean canCreate() {
		return supplier != null;
	}
	
	public V create() {
		return supplier.get();
	}
	
	public V get() {
		if (instance == null && canCreate()) {
			instance = create();
			add(instance);
		}
		return instance;
	}
	
	public InstanceRegistry<V> set(V instance) {
		this.instance = instance;
		return this;
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.generator;

public enum ReservedKeyWords {
	// Java
	Abstract ("abstract"),
	Assert ("assert"),
	Boolean ("boolean"),
	Break ("break"),
	Byte ("byte"),
	Case ("case"),
	Catch ("catch"),
	Char ("char"),
	Class ("class"),
	Const ("const"),
	Continue ("continue"),
	Create ("create"),
	Default ("default"),
	Do ("do"),
	Double ("double"),
	Else ("else"),
	Enum ("enum"),
	Exports ("exports"),
	Extends ("extends"),
	False ("false"),
	Final ("final"),
	Finally ("finally"),
	Float ("float"),
	For ("for"),
	Goto ("goto"),
	If ("if"),
	Implements ("implements"),
	Import ("import"),
	Instanceof ("instance"),
	Int ("int"),
	Interface ("interface"),
	Long ("long"),
	Module ("module"),
	Native ("native"),
	New ("new"),
	Package ("package"),
	Private ("private"),
	Protected ("protected"),
	Public ("public"),
	Requires ("requires"),
	Return ("return"),
	Short ("short"),
	Static ("static"),
	Strictfp ("strictfp"),
	Super ("super"),
	Switch ("switch"),
	Synchronized ("synchronized"),
	This ("this"),
	Throw ("throw"),
	Throws ("throws"),
	Transient ("transient"),
	True ("true"),
	Try ("try"),
	Underscore ("_"),
	Var ("var"),
	Void ("void"),
	Volatile ("volatile"),
	While ("while"),
	
	//Xtend
	Annotation ("annotation"),
	Def ("def"),
	Dispatch ("dispatch"),
	Extension ("extension"),
	Override ("override"),
	Val ("val");
	
	private String value;

	private ReservedKeyWords(String keyWord) {
		value = keyWord;
	}

	public String getKeyword() {
		return value;
	}	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.xtext;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal;
import org.eclipse.xtext.ui.editor.contentassist.ReplacementTextApplier;

public class ChooseFontStyle extends ReplacementTextApplier{

	private String prefix;
	
	public ChooseFontStyle() {
		prefix = "";
	}
	
	public ChooseFontStyle(String prefix) {
		this.prefix = prefix;
	}
	
	@Override
	public String getActualReplacementString(
			ConfigurableCompletionProposal proposal) {
		
		FontDialog fd = new FontDialog(new Shell());
		FontData fData = fd.open();
		String style = "";
		switch (fData.getStyle()) {
			case SWT.BOLD:
				style = "BOLD,";
				break;
			case SWT.ITALIC:
				style = "ITALIC,";
				break;
			case (SWT.BOLD | SWT.ITALIC):
				style = "BOLD, ITALIC,";
				break;
			default:
				break;
		}
		
		String retVal = prefix+"\""+fData.getName() + "\"," +style+ fData.getHeight() +")";
		return retVal;
	}
	
}

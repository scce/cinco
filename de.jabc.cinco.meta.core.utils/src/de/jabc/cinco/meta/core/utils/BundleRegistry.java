/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils;

import java.util.HashSet;
import java.util.Set;

public class BundleRegistry {
	public static BundleRegistry INSTANCE = new BundleRegistry();
	private Set<String> pluginIDs;
	private Set<String> fragmentIDs;
	private Set<String> modules;
	private BundleRegistry(){
		pluginIDs = new HashSet<>();
		fragmentIDs = new HashSet<>();
		modules = new HashSet<>();
	}	
	
	public static BundleRegistry resetRegistry(){
		INSTANCE = new BundleRegistry();
		return INSTANCE;
	}
	
	public Set<String> getPluginIDs(){
		return pluginIDs;
	}
	

	public Set<String> getFragmentIDs(){
		return fragmentIDs;
	}
	
	public Set<String> getModules(){
		return modules;
	}
	
	public boolean addBundle(String bundleID,boolean isFragment){
		return addBundle(bundleID,isFragment,false);
	}
	
	public boolean addBundle(String bundleID, boolean isFragment, boolean isModule){
		if(isModule)
			this.modules.add(bundleID);
		if(!isFragment)
			return pluginIDs.add(bundleID);
		else
			return fragmentIDs.add(bundleID);
	}
	
	public boolean removeFragment(String fragmentID){
		return fragmentIDs.remove(fragmentID);
	}
	
	public boolean removePlugin(String pluginID){
		return pluginIDs.remove(pluginID);
	}
}
 
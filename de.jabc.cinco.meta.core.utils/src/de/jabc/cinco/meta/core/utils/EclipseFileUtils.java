/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils;

import java.io.File;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.util.StringInputStream;
import org.osgi.framework.Bundle;

public class EclipseFileUtils {

	/**
	 * Creates the folders on disks that are represented by the <code>folde</code>.
	 * If any of the parent folders of <code>folder</code> does not exists it will be created as well.
	 * Resources that already exists on the disk result in a message being printed in the standard output.
	 * 
	 * @param folder	the {@link IFolder} to create on disk
	 * @throws CoreException	if {@link IFolder#create(boolean, boolean, org.eclipse.core.runtime.IProgressMonitor)} throws an exception
	 * 							for another reason than that the resource to create already exists on disk
	 */
	public static void mkdirs(IFolder folder) throws CoreException {
	    if (!folder.exists()) {
		    if (folder.getParent() instanceof IFolder) {
		    	mkdirs((IFolder) folder.getParent());
		    }
		    try {
		    	folder.create(true, true, null);
	        }
		    catch (CoreException coreException) {
		    	// TODO: This is ugly. Needs to be beautified soon[tm]
		    	if (!coreException.getMessage().startsWith("A resource already exists on disk")) {
		    		throw coreException;
		    	}
		    	else System.out.println(coreException);
		    }
	    }
	}
	
	/**
	 * Writes the provided <code>contents</code> to the provided <code>file</code>.
	 * If the file does not exist yet it is created during this method.
	 * 
	 * @param file	the {@link IFile} that represents the file to which the <code>contents</code> should be written
	 * @param contents	the {@link CharSequence} that should be written to <code>file</code>
	 * @throws	RuntimeException	rethrows {@link CoreException} of {@link IFile#create(java.io.InputStream, boolean, org.eclipse.core.runtime.IProgressMonitor)}
	 * 								and {@link IFile#setContents(java.io.InputStream, boolean, boolean, org.eclipse.core.runtime.IProgressMonitor)}
	 */
	public static void writeToFile(IFile file, CharSequence contents) {
		try {
			if(!file.exists()) {
				file.create(new StringInputStream(contents.toString()), true, null);
			}
			else {
				file.setContents(new StringInputStream(contents.toString()), true, true, null);
			}
		}
		catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Returns the {@link File} that contains the provided <code>model</code>.
	 * If the file does not exist or could not be found a {@link RuntimeException} is thrown.
	 * 
	 * @param model	the {@link EObject} of which the {@link File} should be returned
	 * @return	the {@link File} of the provided <code>model</code>
	 * @throws	RuntimeException	if the file of the <code>model</code> does not exist or could not be found
	 */
	public static File getFileForModel(EObject model) {
		// get file for model (eObject)
		org.eclipse.emf.common.util.URI resolvedFile = CommonPlugin.resolve(EcoreUtil.getURI(model));
		IFile iFile = ResourcesPlugin.getWorkspace().getRoot()
				.getFile(new Path(resolvedFile.toFileString()));

		File file = iFile.getFullPath().toFile();
		if (file == null)
			throw new RuntimeException("Could not find file for " + model);
		if (!file.exists())
			throw new RuntimeException("File does not exist for " + model);

		return file;
	}
	
	/**
	 * Helper method for code generators that require static files given in some bundle (probably the one
	 * where the code generator is implemented).
	 * 
	 * If pathInBundle is a file, it will be copied to targetDirectory. If it is a directory, all its contents 
	 * will be copied to targetDirectory.
	 * 
	 * @param bundleId
	 * @param pathInBundle
	 * @param targetDirectory
	 */
	public static void copyFromBundleToDirectory(String bundleId, String pathInBundle, IFolder targetDirectory) {
		Bundle b = Platform.getBundle(bundleId);
		URL directoryURL = b.getEntry(pathInBundle);
		if (directoryURL == null) {
			throw new RuntimeException(String.format("path '%s' not found in bundle '%s'", pathInBundle, bundleId));
		}
		try {
			// solution based on http://stackoverflow.com/a/23953081
			URL fileURL = FileLocator.toFileURL(directoryURL);
			java.net.URI resolvedURI = new java.net.URI(fileURL.getProtocol(), fileURL.getPath(), null);
		    File sourceFile = new File(resolvedURI);
		    File targetDir = targetDirectory.getRawLocation().makeAbsolute().toFile(); 
		    if (sourceFile.isDirectory()) {
		    	FileUtils.copyDirectoryToDirectory(sourceFile, targetDir);
		    }
		    else {
		    	FileUtils.copyFileToDirectory(sourceFile, targetDir);
		    }
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Helper method for code generators that require static files given in some bundle (probably the one
	 * where the code generator is implemented).
	 * 
	 * @param bundleId
	 * @param pathInBundle
	 * @param targetFile
	 */
	public static void copyFromBundleToFile(String bundleId, String pathInBundle, IFile targetFile) {
		Bundle b = Platform.getBundle(bundleId);
		URL directoryURL = b.getEntry(pathInBundle);
		if (directoryURL == null) {
			throw new RuntimeException(String.format("path '%s' not found in bundle '%s'", pathInBundle, bundleId));
		}
		try {
			// solution based on http://stackoverflow.com/a/23953081
			URL fileURL = FileLocator.toFileURL(directoryURL);
			java.net.URI resolvedURI = new java.net.URI(fileURL.getProtocol(), fileURL.getPath(), null);
		    File source = new File(resolvedURI);
		    File target = targetFile.getRawLocation().makeAbsolute().toFile(); 
		    FileUtils.copyFile(source, target);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils.xtext;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

public class PerformAppearanceWizard {
	
	private String classPath = "";
	
	public PerformAppearanceWizard(EObject eObject) {
		execute(eObject);
	}

	private void execute(EObject eObject) {
		URI uri = eObject.eResource().getURI();
		String[] segments = uri.segments();
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject[] projects = root.getProjects();		
		String path = search(segments,projects);
		IJavaProject project = JavaCore.create(root.getProject(path));
		AppearanceWizard wizard = new AppearanceWizard(project);
		createPath(wizard,path);
		
	}

	private String search(String[] segments, IProject[] projects) {
		
			for(int i = 0; i< segments.length; i++)
			{
				for(int j = 0; j<projects.length; j++)
				{
					String seg = segments[i];
					String[] pro = projects[j].toString().split("/");
					if(seg.equals(pro[1])) return seg;
				}
			}
			return "";
		}
	private void createPath(AppearanceWizard wizard, String path)
	{
		classPath =  wizard.getPath();
	}

	public String getPath() {
		return classPath;
	}

}

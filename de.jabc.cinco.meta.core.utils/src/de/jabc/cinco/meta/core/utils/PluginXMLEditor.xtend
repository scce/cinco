/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils

import javax.xml.parsers.DocumentBuilderFactory
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.xtext.util.StringInputStream
import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.Element
import javax.xml.transform.stream.StreamResult
import java.io.ByteArrayOutputStream
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import java.nio.charset.StandardCharsets

class PluginXMLEditor {
	
	
	static def openPluginXMLDocument(IProject project){
		
		val pluginXMLFile = project.getFile("plugin.xml");	
		pluginXMLFile.openXMLDocument
		
		
	}
	
	 static def Document openXMLDocument(IFile pluginXMLFile){
		if(pluginXMLFile.exists){
			val pluginXMLStream = pluginXMLFile.getContents();
			val factory = DocumentBuilderFactory.newInstance();
			val builder = factory.newDocumentBuilder();
			builder.parse(pluginXMLStream)
		}else{
			pluginXMLFile.createEmptyPluginXML(new NullProgressMonitor())	
		}
		
	}
	
	
	private static def Document createEmptyPluginXML(IFile pluginXMLFile,IProgressMonitor monitor){
		pluginXMLFile.setContents(new StringInputStream(pluginXMLBody),true,true,monitor)
		openXMLDocument(pluginXMLFile)
	}
	
	 static def getPluginXMLBody() {'''
		<?xml version="1.0" encoding="UTF-8"?>
		<?eclipse version="3.0"?>
		<plugin>
		</plugin>
		'''.toString
	}
	
	static def addExtension(Document pluginXML, String name){
		val root = pluginXML.documentElement// pluginXML.firstChild
		try {
			val element = pluginXML.createElement("extension")
			val point = pluginXML.createAttribute("point")
			point.nodeValue = name
			element.attributeNode = point
			root.appendChild(element)
			element
			 
		}catch(Exception e){
			throw new RuntimeException("Could Not add Extension",e)
		}
		
	}
	
	static def addAttribute(Element element, String name, String value){
		val attribute = element.ownerDocument.createAttribute(name)
		attribute.value = value
		element.attributeNode = attribute
	}
	
	static def addSubElement(Element element,String name){
		val subElement = element.ownerDocument.createElement(name)
		element.appendChild(subElement)
		subElement
	}
	
	static def save(Document pluginXML,IProject project,IProgressMonitor monitor){
		val result = new StreamResult(new ByteArrayOutputStream)
		TransformerFactory.newInstance.newTransformer.transform(new DOMSource(pluginXML),result)
		val pluginXMLFile = project.getFile("plugin.xml")
		val stream = new StringInputStream((result.outputStream as ByteArrayOutputStream).toString(StandardCharsets.UTF_8.name))
		if(pluginXMLFile.exists){
			pluginXMLFile.setContents(
				stream,
					true,
					true,
					monitor
			)
		}else{
			pluginXMLFile.create(stream,true,monitor)
		}
		
	}
	
	
}

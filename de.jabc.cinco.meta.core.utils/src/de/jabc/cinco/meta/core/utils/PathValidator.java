/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils;

import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import de.jabc.cinco.meta.util.xapi.ResourceExtension;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

public class PathValidator {

	/**
	 * Returns whether the provided <code>path</code> is relative or not.
	 * <br/>
	 * A path is considered relative if it uses no scheme (e.g.: "file:", "platform:" or "plugin:")
	 * 
	 * @param path	the {@link String} to check whether it represents a relative path
	 * @return	<code>true</code> if the provided <code>path</code> is relative, <code>false</code>
	 * 			otherwise
	 */
	public static boolean isRelativePath(String path) {
		URI uri = URI.createURI(path, true);
		return uri.isRelative();
	}
	
	/**
	 * Checks whether the provided <code>path</code> leads to a resource, file, or folder.
	 * <br/>
	 * If the <code>path</code> is valid an empty {@link String} is returned, if not an error
	 * message is returned.
	 * <br/>
	 * The provided <code>object</code> is used as a helping {@link EObject} that helps to retrieve
	 * the resource of the provided <code>path</code> if it cannot be found right away.
	 * In this case a lookup in <code>object</code>'s project will be made.
	 * 
	 * @param object	a helping {@link EObject} to retrieve a project in which the resource
	 * 					referenced by <code>path</code> should be found in
	 * @param path		a {@link String} to check whether it points to a valid resource, file,
	 * 					or folder
	 * @return a {@link String} that is empty if the <code>path</code> is valid, a error message if
	 * 			<code>path</code> is invalid
	 */
	public static String checkPath(EObject object, String path) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		Resource res = object.eResource();
		URI uri = URI.createURI(path, true);
		
		if (uri.isPlatformResource()) {
			return checkPlatformResourceURI(uri, root);
		}
		
		else if (uri.isPlatformPlugin()) {
			return checkPlatformPluginURI(uri);
		}
		
		else {
			String fileExists = checkFileExists(getFile(path, root, res));
			if(!fileExists.isEmpty()) {
				//check for folder
				String folderExists = checkFolderExists(getFolder(path, root, res));
				if(!folderExists.isEmpty()) {
					return fileExists+folderExists;
				}
				return folderExists;
			}
			return fileExists;
		}
	}
	
	/**
	 * Returns an {@link URI} for the provided <code>path</code> and returns it if a resource, file
	 * or folder exists at the <code>path</code>.
	 * <br/>
	 * The provided <code>object</code> is used as a helping {@link EObject} that helps to retrieve
	 * the resource of the provided <code>path</code> if it cannot be found right away.
	 * In this case a lookup in <code>object</code>'s project will be made.
	 * 
	 * @param object	a helping {@link EObject} to retrieve a project in which the resource
	 * 					referenced by <code>path</code> should be found in
	 * @param path	a {@link String} that points to a file, resource or folder for which an 
	 * 				{@link URI} should be returned
	 * @return the {@link URI} of the file, resource or folder present at the provided
	 * 			<code>path</code>, returns <code>null</code> if the <code>path</code> is invalid or
	 * 			if no file, resource or folder is present under the provided <code>path</code>
	 * @see #getURLForString(EObject, String)
	 */
	public static URI getURIForString(EObject object, String path) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		Resource res = object.eResource();
		URI uri = URI.createURI(path, true);
		String retval = null;
		
		if (uri.isPlatformResource()) {
			retval = checkPlatformResourceURI(uri, root);
			if (retval == null || retval.isEmpty())
				return uri;
		}
		
		if (uri.isPlatformPlugin()) {
			retval = checkPlatformPluginURI(uri);
			if (retval == null || retval.isEmpty())
				return uri;
		}
		
		else {
			IResource resource = getFile(path, root, res);
			if(!resource.exists()) {
				//try folder
				resource = getFolder(path, root, res);
			}
			if (resource.exists())
				return getURI(resource);
		}
		return null;
	}

	/**
	 * Returns an {@link URL} for the provided <code>path</code> and returns it if a resource, file
	 * or folder exists at the <code>path</code>.
	 * <br/>
	 * The provided <code>object</code> is used as a helping {@link EObject} that helps to retrieve
	 * the resource of the provided <code>path</code> if it cannot be found right away.
	 * In this case a lookup in <code>object</code>'s project will be made.
	 * 
	 * @param object	a helping {@link EObject} to retrieve a project in which the resource
	 * 					referenced by <code>path</code> should be found in
	 * @param path	a {@link String} that points to a file, resource or folder for which an 
	 * 				{@link URL} should be returned
	 * @return the {@link URL} of the file, resource or folder present at the provided
	 * 			<code>path</code>, returns <code>null</code> if the <code>path</code> is invalid or
	 * 			if no file, resource or folder is present under the provided <code>path</code>
	 * @see #getURIForString(EObject, String)
	 */
	public static URL getURLForString(EObject object, String path) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		Resource res = object.eResource();
		URI uri = URI.createURI(path, true);
		String retval = null;
		
		try {
			if (uri.isPlatformResource()) {
				retval = checkPlatformResourceURI(uri, root);
				if (retval == null || retval.isEmpty()){
					IResource member = root.findMember(uri.toPlatformString(true));
						return member.getLocationURI().toURL();
				}
			}
			
			else if (uri.isPlatformPlugin()) {
				retval = checkPlatformPluginURI(uri);
				if (retval == null || retval.isEmpty()) {
					IResource resource = root.findMember(uri.toPlatformString(true));
					return resource.getLocationURI().toURL();
				}
			}
			
			else {
				retval = checkRelativePath(path, root, res);
				if (retval == null || retval.isEmpty()) {
					return pathToURL(res.getURI(), path, root, res);
				}
					
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns the id of the bundle the <code>uri</code>'s target is located in.
	 * 
	 * @param uri the {@link URI} for which the bundle ID should be returned
	 * @return	a {@link String} that represents the ID of the bundle {@link URI}'s target is
	 * 			located in
	 * @throws RuntimeException if the provided </code>uri</code> is not pointing to a platform
	 * 							resource, file or folder
	 */
	public static String getBundleID(URI uri) {
		URI trimmed = null;
		if (uri.isPlatformPlugin() || uri.isPlatformResource()) {
			trimmed = URI.createURI(uri.toPlatformString(true));
		}
		if (uri.isRelative()) {
			trimmed = uri;
		}
		if (uri.isPlatform()){
			trimmed = uri.deresolve(URI.createURI("platform:/"));
		}
		if (trimmed != null)
			return trimmed.segment(0);
		throw new RuntimeException("The uri: \"" + uri +"\" could not be recognized");
	}
	
	private static String checkPlatformResourceURI(URI uri, IWorkspaceRoot root) {
		IResource res = root.findMember(uri.toPlatformString(true));
		if (res == null || !(res instanceof IFile) ) {
			return "The specified file does not exists.";
		}
		return "";
	}
	
	private static String checkPlatformPluginURI(URI uri) {
		try {
			URL find = FileLocator.find(new URL(uri.toString()));
			URL fileUrl = FileLocator.toFileURL(find);
			File file = new File(fileUrl.getFile());
			if (!file.exists())
				return "The specified plugin files does not exist";
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	private static String checkRelativePath(String path, IWorkspaceRoot root, Resource res) {
        if (path != null && path.isEmpty())
                return "No path specified";
        IProject p;
        IFile file;
        URI uri = res.getURI();
		if (uri.isFile()) {
			IPath fromOSString = Path.fromOSString(uri.toFileString());
        	IResource member = root.getFileForLocation(fromOSString);
        	if (member != null) {
        		p = member.getProject();
        		file = p.getFile(path);
        		if (!file.exists()) {
        			return "The specified file: \""+path+"\" does not exists.";
        		}
        	}
        		
        } else {
	        file = handlePlatformUri(path, uri, root);
			if (!file.exists()) {
				return "The specified file: \""+path+"\" does not exists.";
			}
        }
		return "";
	}
	
	private static String checkFileExists(IFile file) {
        if (!file.exists()) {
			return "The specified file: \""+file.getFullPath()+"\" does not exist.";
		}
		return "";
	}
	
	private static String checkFolderExists(IFolder folder) {
        if (!folder.exists()) {
			return "The specified folder: \""+folder.getFullPath()+"\" does not exist.";
		}
		return "";
	}
	
	private static URI getURI(IResource file) {
        if (!file.exists()) 
			return null;
		return URI.createPlatformResourceURI(file.getFullPath().toPortableString(), true);
	}
	
	private static URL pathToURL(URI resURI, String path, IWorkspaceRoot root, Resource res) {
        if (path != null && path.isEmpty())
        	return null;
        IFile file = null;
        URI uri = res.getURI();
		if (uri.isFile()) {
        	file = handleFileUri(path, uri, root);
        } else {
        	file = handlePlatformUri(path, uri, root);
		if (!file.exists()) 
			return null;
        }
		try {
			return file.getLocationURI().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static IFile handlePlatformUri(String path, URI uri, IWorkspaceRoot root) {
		IProject p;
		IFile file;
		p = root.getFile(new Path(uri.toPlatformString(true))).getProject();
		file = p.getFile(path);
		return file;
	}

	private static IFile handleFileUri(String path, URI uri, IWorkspaceRoot root) {
		IProject p;
		IFile file = null;
		IPath fromOSString = Path.fromOSString(uri.toFileString());
		IResource member = root.getFileForLocation(fromOSString);
		if (member != null) {
			p = member.getProject();
			file = p.getFile(path);
			if (!file.exists()) {
				return null;
			}
		}
		return file;
	}
	
	private static IFile getFile(String path, IWorkspaceRoot root, Resource res) {
        if (path == null || path.isEmpty())
        	return null;
        IFile resFile = null;
        if (res.getURI().isPlatform()) {
        	resFile = root.getFile(new Path(res.getURI().toPlatformString(true)));
        } else {
        	resFile = root.getFileForLocation(Path.fromOSString(res.getURI().path()));
        }
        URI uri = URI.createURI(path);
        if (uri.isPlatform())
        	return root.getFile(new Path(uri.toPlatformString(true)));
        else
        	return resFile.getProject().getFile(path);
	}
	
	private static IFolder getFolder(String path, IWorkspaceRoot root, Resource res) {
        if (path == null || path.isEmpty())
        	return null;
        IFile resFile = null;
        if (res.getURI().isPlatform()) {
        	resFile = root.getFile(new Path(res.getURI().toPlatformString(true)));
        } else {
        	resFile = root.getFileForLocation(Path.fromOSString(res.getURI().path()));
        }
        URI uri = URI.createURI(path);
        if (uri.isPlatform())
        	return root.getFolder(new Path(uri.toPlatformString(true)));
        else
        	return resFile.getProject().getFolder(path);
	}
	
	/**
	 * Returns whether the provided <code>eObj</code> and the target of <code>path</code> are
	 * located in the same project.
	 * 
	 * @param eObj	the {@link EObject} that should be checked whether it is located in the same
	 * 				project as the target of <code>path</code>
	 * @param path	a {@link String} that represents a path to a resource, file or folder that
	 * 				should be checked whether it is located in the same project as<code>eObj</code>
	 * @return	<code>true</code> if the provided <code>eObj</code> and the target of
	 * 			<code>path</code> are located in the same project, <code>false</code> otherwise
	 */
	public static boolean checkSameProjects(EObject eObj, String path) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		Resource res = eObj.eResource();
		ResourceExtension reEx = new ResourceExtension();
		
		IFile importedFile = getFile(path, root, res);
		IProject importedProject = importedFile.getProject();
		if(importedProject == null || !importedProject.equals(reEx.getFile(res).getProject())){
			return false;
		}
		
		return true;
	}
	/**
	 * Returns a path relative to a given project
	 * @param path
	 * @param project
	 * @return Path String relative to given project
	 */
	public static String getRelativePath(String path, IProject project) {
		var projectSymbolicName = ProjectCreator
			.getProjectSymbolicName(project)
			.replace(".", "\\.");
		// Group 1 of "platform:/resource/com.example.graph/model/Graph.mgl"
		// or "/com.example.graph/model/Graph.mgl"
		// is "model/Graph.mgl"
		var regex = "\\/" + projectSymbolicName + "\\/(.+\\.mgl)$";
		var pattern = Pattern.compile(regex);
		var matcher = pattern.matcher(path);
		if (matcher.find()) {
			return matcher.group(1);
		}
		// Group 1 of "platform:/resource/com.example.graph/model/Graph.mgl"
		// is "/com.example.graph/model/Graph.mgl"
		regex = "^platform:\\/resource(\\/[a-zA-Z0-9_\\-\\.]+\\/.+\\.mgl)$";
		pattern = Pattern.compile(regex);
		matcher = pattern.matcher(path);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return path;
	}
}

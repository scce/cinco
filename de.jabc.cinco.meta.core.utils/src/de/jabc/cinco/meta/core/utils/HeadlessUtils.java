/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.mwe2.launch.runtime.Mwe2Launcher;

import de.jabc.cinco.meta.util.xapi.FileExtension;
import de.jabc.cinco.meta.util.xapi.ResourceExtension;
import de.jabc.cinco.meta.util.xapi.WorkspaceExtension;
import productDefinition.CincoProduct;

@SuppressWarnings("restriction")
public class HeadlessUtils {
	
	public static IProject importProjectFromString(String importPath,IProgressMonitor monitor) throws CoreException{
		File baseDirectory = new File(importPath);
		IProjectDescription description = new WorkspaceExtension().getWorkspace().loadProjectDescription(
				new Path(baseDirectory.getAbsolutePath() + "/.project"));
		
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(description.getName());
		if(!project.exists())
			project.create(description, monitor);
		if(!project.isOpen())
			project.open(monitor);

		return project;
	}
	
	public static IProject importProjectFromString(String workspacePath,String projectName,IProgressMonitor monitor) throws CoreException{
		String projectPath = String.format("%s/%s", workspacePath);
		return importProjectFromString(projectPath,monitor);
	}
	
	public static Iterable<IProject> importAllProjects(Iterable<String> importPaths,IProgressMonitor monitor){
			ArrayList<IProject> projects = new ArrayList<>();
			importPaths.forEach(path -> {
				try {
					projects.add(importProjectFromString(path,monitor));
				}catch(Exception e) {
					throw new RuntimeException(String.format("Could not load Project: '%s'",path));
				}
			});
			
			return projects;
	}
	
	public static Iterable<IProject> importAllProjects(String workspacePath,Iterable<String> importPaths,IProgressMonitor monitor){
		ArrayList<IProject> projects = new ArrayList<>();
		final ArrayList<String> paths = new ArrayList<>();
		importPaths.forEach(path ->{
			paths.add(String.format("%s/%s", workspacePath,path));
		});
				
		return importAllProjects(paths,monitor);
}
	
	public static CincoProduct  loadCincoProduct(IProject project,String cpdPath) {
		System.out.println(cpdPath);
		IFile cpdFile = getCPDFile(project,cpdPath);
		FileExtension fe = new FileExtension();
		ResourceExtension re = new ResourceExtension();
		return re.getContent(fe.getResource(cpdFile), CincoProduct.class);
	}
	
	public static CincoProduct loadCincoProduct(String importPath, String cpdPath, IProgressMonitor monitor) throws CoreException {
		IProject project = importProjectFromString(importPath, monitor);
		return loadCincoProduct(project,cpdPath);
	}
	
	public static IFile getCPDFile(IProject project, String cpdPath){
		return project.getFile(cpdPath);
	}
	
	public static IFile getCPDFile(String importPath, String cpdPath, IProgressMonitor monitor) throws CoreException {
		IProject project = importProjectFromString(importPath, monitor);
		return getCPDFile(project,cpdPath);
	}
	
	public static IFile getCPDFile(String workspacePath, String projectName, String cpdPath, IProgressMonitor monitor) throws CoreException {
		String importPath = String.format("%s/%s",workspacePath,projectName);
		IProject project = importProjectFromString(importPath, monitor);
		return getCPDFile(project,cpdPath);
	}

	
}




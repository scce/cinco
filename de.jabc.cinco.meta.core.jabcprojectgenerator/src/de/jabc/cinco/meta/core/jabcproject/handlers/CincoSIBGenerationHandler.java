/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.jabcproject.handlers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.jar.Manifest;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.jabc.cinco.meta.core.jabcproject.Activator;
import de.jabc.cinco.meta.core.jabcproject.TransEM4SIBGenerator;
import mgl.MGLModel;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class CincoSIBGenerationHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public CincoSIBGenerationHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IFile file = Activator.getDefault().getSelectionListener().getCurrentMGLFile();
		
		if (file != null && file.getFileExtension().equals("mgl")) {
			try {
				IPath modelPath = file.getLocation();

				prepareProject(file.getProject());
				ResourceSet resourceSet = new ResourceSetImpl();
				Resource mglResource = resourceSet.createResource(URI
						.createFileURI(modelPath.toFile().getAbsolutePath()));
				mglResource.load(null);

				MGLModel mglModel = (MGLModel) mglResource.getContents()
						.get(0);
				new TransEM4SIBGenerator(file.getProject(),
						mglModel).run(new NullProgressMonitor());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void prepareProject(IProject project) {
		Manifest mani = new Manifest();
		try {
			mani.read(new FileInputStream(project.getLocation()
					.append("META-INF/MANIFEST.MF").toFile()));
			String oldValue = mani.getMainAttributes().getValue(
					"Require-Bundle");
			if (!oldValue.contains("de.jabc.cinco.meta.libraries")) {
				oldValue = oldValue.concat(",de.jabc.cinco.meta.libraries");
				mani.getMainAttributes().putValue("Require-Bundle", oldValue);
			}
			if (!oldValue.contains("org.eclipse.xtend.typesystem.emf")) {
				oldValue = oldValue.concat(",org.eclipse.xtend.typesystem.emf");
				mani.getMainAttributes().putValue("Require-Bundle", oldValue);
			}
			if (!oldValue.contains("org.eclipse.emf.ecore.xmi")) {
				oldValue = oldValue.concat(",org.eclipse.emf.ecore.xmi");
				mani.getMainAttributes().putValue("Require-Bundle", oldValue);
			}
			if(!oldValue.contains("de.jabc.cinco.meta.core.sibgenerator")){
				oldValue.concat("de.jabc.cinco.meta.core.sibgenerator");
				mani.getMainAttributes().putValue("Require-Bundle", oldValue);
			}
			mani.write(new FileOutputStream(project.getLocation()
					.append("META-INF/MANIFEST.MF").toFile()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

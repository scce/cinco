#!/bin/bash
set -e
if [ $# -lt 3 ];
then
	echo "Illegal number of Arguments"
	echo "Usage: ./cinco_headless <workspaceDIR> <projectName> <cpdFileName>[<additionalProjectName>(:<additionalProjectName>)*]"
	exit 1
fi
ADDITIONAL_PROJECTS=""
if [ $# -eq 4 ];
then
	ADDITIONAL_PROJECTS="--otherProjects $4"
fi
echo "Generation Step 1"
./cinco -data $1 -application de.jabc.cinco.meta.headless.application --importProject $2 --cpd model/$3 $ADDIIONAL_PROJECTS
cd $1
echo "Maven Package Step 1"
mvn package || true
cd -
echo "Generation Step 2"
./cinco -data $1 -application de.jabc.cinco.meta.headless.application --importProject $2 --cpd model/$3 $ADDITIONAL_PROJECTS
cd $1
echo "Maven Package Step 2"
mvn package
EXIT_CODE=$?
cd -
exit $EXIT_CODE

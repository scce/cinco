/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.api;

import java.util.ArrayList;

import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.ChopboxAnchor;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.graphiti.mm.pictograms.Shape;

import graphmodel.Node;

public interface CNode extends CModelElement, Node{
	
	public default Anchor getAnchor() {
		PictogramElement pe = getPictogramElement();
		if (pe instanceof AnchorContainer) {
			AnchorContainer ac = ((AnchorContainer) pe);
			if (((AnchorContainer) pe).getAnchors().isEmpty()) {
				if(pe instanceof ContainerShape) {
					ArrayList<PictogramElement> childrenToCheckForAnchors = new ArrayList<PictogramElement>();
					childrenToCheckForAnchors.addAll(((ContainerShape) pe).getChildren());
					for(int i = 0; i < childrenToCheckForAnchors.size(); i++) {
						PictogramElement peChild = childrenToCheckForAnchors.get(i);
						if (!((AnchorContainer) peChild).getAnchors().isEmpty()) {
							return ((AnchorContainer) peChild).getAnchors().get(0);
						} else {
							if(peChild instanceof ContainerShape) {
								for(Shape child : ((ContainerShape) peChild).getChildren()) {
									childrenToCheckForAnchors.add(child);
								}
							}
						}
					}
				}
				
				ChopboxAnchor anchor = PictogramsFactory.eINSTANCE.createChopboxAnchor();
				anchor.setActive(false);
				anchor.setParent(ac);
			}
			return ((AnchorContainer) pe).getAnchors().get(0);
		} else throw new RuntimeException(String.format("Could not retrieve anchors from PictogramElement %s", pe));
	};
}

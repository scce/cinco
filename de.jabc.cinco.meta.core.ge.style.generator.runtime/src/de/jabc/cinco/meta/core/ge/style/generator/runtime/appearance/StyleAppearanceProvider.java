/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance;

import mgl.ModelElement;
import style.Appearance;
import style.ContainerShape;
import style.EdgeStyle;
import style.NodeStyle;
import style.Shape;

public interface StyleAppearanceProvider<T> {
	
	final String CONNECTION = "connection";
	
	/**
	 * 
	 * @param modelElement The underlying {@link ModelElement} for the graphical representation in the editor 
	 * @param styleElementName The name of the {@link ContainerShape} or {@link Shape} of the corresponding 
	 * {@link NodeStyle} {@link EdgeStyle} respectively
	 * 
	 * @return The appearance for the graphical element named {@link styleElementName} depending 
	 * on the state of the underlying {@link ModelElement}
	 */
	public Appearance getAppearance(T modelElement, String styleElementName);
}

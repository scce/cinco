/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.features

import graphmodel.internal.InternalEdge
import graphmodel.internal.InternalFactory
import graphmodel.internal._Point
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IMoveBendpointContext
import org.eclipse.graphiti.features.impl.DefaultMoveBendpointFeature
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection

class CincoMoveBendpointFeature extends DefaultMoveBendpointFeature {
	new(IFeatureProvider fp) {
		super(fp)
	}

	override boolean canMoveBendpoint(IMoveBendpointContext context) {
		return super.canMoveBendpoint(context)
	}

	override boolean moveBendpoint(IMoveBendpointContext context) {
		var boolean retval = super.moveBendpoint(context)
		var FreeFormConnection connection = context.getConnection()
		var InternalEdge edge = (getBusinessObjectForPictogramElement(connection) as InternalEdge)
		var _Point p = InternalFactory.eINSTANCE.create_Point() => [x = context.x; y = context.y]
		edge.getBendpoints().set(context.getBendpointIndex(), p)
		return retval
	}
}

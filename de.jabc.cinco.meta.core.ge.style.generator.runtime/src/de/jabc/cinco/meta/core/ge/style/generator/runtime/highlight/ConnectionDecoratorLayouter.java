/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LayoutListener;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.internal.figures.GFPolylineConnection;

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension;

public class ConnectionDecoratorLayouter extends LayoutListener.Stub {

	public static ConnectionDecoratorLayouter applyTo(PictogramElement pe) {
		DiagramBehavior db = new WorkbenchExtension().getDiagramBehavior(pe);
		GraphicalEditPart ep = db.getEditPartForPictogramElement(pe);
		figure = ep.getFigure();
		ConnectionDecoratorLayouter layouter = new ConnectionDecoratorLayouter(figure);
		figure.addLayoutListener(layouter);
		return layouter;
	}
	
	
	private static IFigure figure;
	private boolean unregisterAfterNextLayout = false;
	
	public ConnectionDecoratorLayouter(IFigure figure) {
		ConnectionDecoratorLayouter.figure = figure;
	}
	
	
	@Override
	public boolean layout(IFigure fig) {
		if (fig instanceof GFPolylineConnection) {
			GFPolylineConnection con = (GFPolylineConnection) fig;
			con.getAllDecorations().forEach(dec -> {
				dec.setForegroundColor(fig.getForegroundColor());
				dec.setBackgroundColor(fig.getBackgroundColor());
			});
			if (unregisterAfterNextLayout) {
				figure.removeLayoutListener(this);
			}
		}
		return false;
	}

	public void setUnregisterAfterNextLayout(boolean flag) {
		this.unregisterAfterNextLayout = flag;
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor

import java.util.function.Supplier
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.graphiti.ui.editor.DiagramEditorInput
import de.jabc.cinco.meta.core.ui.editor.PageAwareEditorInput
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.ui.IEditorInput

class PageAwareDiagramEditorInput extends DiagramEditorInput implements PageAwareEditorInput {
	
	Supplier<Resource> innerStateSupplier

	new (IEditorInput input) {
		this(new WorkbenchExtension().getURI(input))
	}

	new (URI diagramUri) {
		super(diagramUri, null)
	}

	override getInnerStateSupplier() {
		innerStateSupplier
	}

	override setInnerStateSupplier(Supplier<Resource> supplier) {
		innerStateSupplier = supplier
	}
}

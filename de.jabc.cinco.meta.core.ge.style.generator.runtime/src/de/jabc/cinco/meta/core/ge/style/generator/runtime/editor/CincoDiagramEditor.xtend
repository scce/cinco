/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor

import de.jabc.cinco.meta.core.ui.editor.PageAwareEditor
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.gef.dnd.TemplateTransferDragSourceListener
import org.eclipse.gef.ui.palette.PaletteViewer
import org.eclipse.graphiti.ui.editor.DiagramEditor
import org.eclipse.graphiti.ui.editor.IDiagramEditorInput
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorPart

abstract class CincoDiagramEditor extends DiagramEditor implements PageAwareEditor {
	
	protected extension ResourceExtension = new ResourceExtension
	protected extension WorkbenchExtension = new WorkbenchExtension
	
	CincoDiagramEditorActionBarContributor actionBarContributor
	PageAwareDiagramBehavior diagramBehavior
	
	def void preSave() {
		// Intentionally left blank
	}
	
	def void postSave() {
		// Intentionally left blank
	}
	
	override PageAwareDiagramBehavior createDiagramBehavior() {
		diagramBehavior = new PageAwareDiagramBehavior(this)
	}
	
	def onPaletteViewerCreated(PaletteViewer pViewer) {
		pViewer.addDragSourceListener(new TemplateTransferDragSourceListener(pViewer))
	}
	
	def PaletteViewer getPaletteViewer() {
		(paletteViewerProvider as CincoPaletteViewerProvider).paletteViewer
	}
	
	override getPageName() {
		"Diagram"
	}
	
	override mapEditorInput(IEditorInput input) {
		new PageAwareDiagramEditorInput(input.URI);
	}
	
	override getActionBarContributor() {
		actionBarContributor
		?: (actionBarContributor = new CincoDiagramEditorActionBarContributor)
	}
	
	override Resource getInnerState() {
		diagramBehavior.innerState
	}
	
	override handlePageActivated(IEditorPart prevEditor) {
		diagramBehavior.handlePageActivated(prevEditor);
	}

	override handlePageDeactivated(IEditorPart nextEditor) {
		diagramBehavior.handlePageDeactivated(nextEditor);
	}
	
	override handleInnerStateChanged() {
		diagramBehavior.handleInnerStateChanged();
		switch it:diagramTypeProvider.diagram {
			LazyDiagram: setDiagramBehavior(diagramBehavior)
		}
	}

	override handleNewInnerState() {
		switch input:editorInput {
			IDiagramEditorInput: diagramBehavior.updateBehavior.createEditingDomain(input)
		}
	}

	override handleSaved() {
		diagramBehavior.handleSaved();
	}
}

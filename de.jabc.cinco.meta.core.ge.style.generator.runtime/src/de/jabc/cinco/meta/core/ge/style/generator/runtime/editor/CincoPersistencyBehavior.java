/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor;

import org.eclipse.emf.common.util.URI;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.ui.editor.DefaultPersistencyBehavior;

public class CincoPersistencyBehavior extends DefaultPersistencyBehavior {

	public CincoPersistencyBehavior(PageAwareDiagramBehavior diagramBehavior) {
		super(diagramBehavior);
	}
	
	protected PageAwareDiagramBehavior getDiagramBehavior() {
		return (PageAwareDiagramBehavior) diagramBehavior;
	}
	
	protected void flushCommandStack() {
		diagramBehavior.getEditingDomain().getCommandStack().flush();
		clearDirtyState();
	}
	
	protected void clearDirtyState() {
		savedCommand = diagramBehavior.getEditingDomain().getCommandStack().getUndoCommand();
		diagramBehavior.getDiagramContainer().updateDirtyState();
	}
	
	protected Diagram getDiagram() {
		try {
			return (Diagram) getDiagramBehavior().getInnerState().getContents().get(0);
		} catch(NullPointerException | IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	@Override
	public Diagram loadDiagram(URI uri) {
		Diagram diagram = getDiagram();
		if (diagram != null) {
			diagram.eResource().setTrackingModification(true);
			return diagram;
		}
		else return super.loadDiagram(uri);
	}
	
}

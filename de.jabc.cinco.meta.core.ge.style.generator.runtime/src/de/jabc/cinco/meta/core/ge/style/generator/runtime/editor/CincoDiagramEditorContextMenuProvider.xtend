/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor

import org.eclipse.gef.EditPartViewer
import org.eclipse.gef.ui.actions.ActionRegistry
import org.eclipse.gef.ui.actions.GEFActionConstants
import org.eclipse.graphiti.ui.editor.DiagramEditorContextMenuProvider
import org.eclipse.graphiti.ui.platform.IConfigurationProvider
import org.eclipse.jface.action.IMenuManager
import org.eclipse.jface.action.MenuManager

import static de.jabc.cinco.meta.core.ge.style.generator.runtime.layout.EdgeLayout.*

class CincoDiagramEditorContextMenuProvider extends DiagramEditorContextMenuProvider {
	
	ActionRegistry actionRegistry
	
	new(EditPartViewer viewer, ActionRegistry registry, IConfigurationProvider configurationProvider) {
		super(viewer, registry, configurationProvider)
		this.actionRegistry = registry
	}
	
	override protected addDefaultMenuGroupRest(IMenuManager manager) {
		manager.addLayoutSubMenu(GEFActionConstants.GROUP_REST)
		super.addDefaultMenuGroupRest(manager)
	}
	
	def addLayoutSubMenu(IMenuManager manager, String group) {
		val menu = new MenuManager("Layout");
		
		// add edge layout actions
		#[ C_LEFT,C_TOP,C_RIGHT,C_BOTTOM,
		   Z_HORIZONTAL,Z_VERTICAL,
		   TO_LEFT,TO_TOP,TO_RIGHT,TO_BOTTOM
		].map[id].forEach[id|
			val action = actionRegistry.getAction(id)
			if (action?.isEnabled)
				menu.add(action)
		]
		
		if (!menu.isEmpty())
			manager.appendToGroup(group, menu)
	}
}

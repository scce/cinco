/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.createfeature;


import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.CincoContainerCardinalityException;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.CincoInvalidContainerException;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError;
import graphmodel.ModelElement;

public abstract class CincoCreateFeature<T extends ModelElement> extends AbstractCreateFeature{

	T newModelElement;
	boolean apiCall;
	protected ECincoError error = ECincoError.OK;
	
	public CincoCreateFeature(IFeatureProvider fp, String name, String description) {
		super(fp, name, description);
	}

	public T getModelElement()
	{
		return newModelElement;
	}
	
	public void setModelElement(T model)
	{
		newModelElement = model;
	}
	
	public abstract boolean canCreate(ICreateContext context, boolean apiCall);
	
	public void throwException(ICreateContext context) {
		Object target = context.getTargetContainer().getLink().getBusinessObjects().get(0);
		switch (error) {
		case INVALID_CONTAINER:
			throw new CincoInvalidContainerException(
					String.format(
							"Invalid Container: Cannot create %s in container %s", getName(), target));
		case MAX_CARDINALITY:
			throw new CincoContainerCardinalityException(
					String.format(
							"Cardinality exceeded: Cannot create %s in container %s. It can't contain more elements of type %s ", 
							getName(), target,getName()));
		case INVALID_SOURCE:

		case INVALID_TARGET:

		case MAX_IN:

		case MAX_OUT:

		default:
			break;
		}
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.createfeature;


import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.CincoEdgeCardinalityInException;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.CincoInvalidSourceException;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.CincoInvalidTargetException;
import de.jabc.cinco.meta.core.ge.style.generator.runtime.errorhandling.ECincoError;
import graphmodel.ModelElement;

public abstract class CincoCreateEdgeFeature<T extends ModelElement> extends AbstractCreateConnectionFeature{

	public static int index = 0;
	
	T newModelElement;
	protected ECincoError error = ECincoError.OK;
	
	public CincoCreateEdgeFeature(IFeatureProvider fp, String name, String description) {
		super(fp, name, description);
	}

	public T getModelElement()
	{
		return newModelElement;
	}
	
	public void setModelElement(T model)
	{
		newModelElement = model;
	}
	
	public abstract boolean canCreate(ICreateConnectionContext context, boolean apiCall);
	
	public void throwException(ICreateConnectionContext context) {
		canCreate(context);
		Object source = context.getSourcePictogramElement().getLink().getBusinessObjects().get(0);
		Object target = context.getTargetPictogramElement().getLink().getBusinessObjects().get(0);
		
		switch (error) {
		case INVALID_SOURCE:
			throw new CincoInvalidSourceException(String.format("Invalid Source: Cannot create %s with %s as source node", getName(), source));
		case INVALID_TARGET:
			throw new CincoInvalidTargetException(String.format("Invalid Target: Cannot create %s with %s as target node", getName(), target));
		case MAX_IN:
			throw new CincoEdgeCardinalityInException(String.format("Incoimg edge cardinality exception: Node %s can not have more incoming %s-edges", getName(), target));
		case MAX_OUT:
			throw new CincoEdgeCardinalityInException(String.format("Outgoing edge cardinality exception: Node %s can not have more outgoing %s-edges", getName(), source));
		default:
			break;
		}
	} 
	
}

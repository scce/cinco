/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.CreateConnectionRequest;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.IFeatureAndContext;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.tb.ContextButtonEntry;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.internal.command.CreateConnectionCommand;
import org.eclipse.graphiti.ui.internal.editor.GFDragConnectionTool;

public class CincoDragConnectionTool extends GFDragConnectionTool {

	private DiagramBehavior diagramBehavior;
	
	public CincoDragConnectionTool(DiagramBehavior diagramBehavior, ContextButtonEntry contextButtonEntry) {
		super(diagramBehavior, contextButtonEntry);
		this.diagramBehavior = diagramBehavior;
	}
	
	private long nextContinue = System.currentTimeMillis();

	@Override
	public void continueConnection(EditPart targetEditPart, EditPart targetTargetEditPart) {
		if (System.currentTimeMillis() < nextContinue) {
			handleDrag();
			return;
		}
		setConnectionSource(targetEditPart);
		lockTargetEditPart(targetEditPart);

		CreateConnectionRequest createConnectionRequest = ((CreateConnectionRequest) getTargetRequest());
		createConnectionRequest.setSourceEditPart(targetEditPart);
		createConnectionRequest.setTargetEditPart(targetTargetEditPart);
		Command command = getCommand();
		if (command != null) {
			setCurrentCommand(command);
			if (stateTransition(STATE_INITIAL, STATE_CONNECTION_STARTED)) {
				for (IFeatureAndContext ifac : getCreateConnectionFeaturesAndContext()) {
					ICreateConnectionFeature ccf = (ICreateConnectionFeature) ifac.getFeature();
					ccf.startConnecting();
					ccf.attachedToSource((ICreateConnectionContext) ifac.getContext());
				}
			}
		}

		setViewer(diagramBehavior.getDiagramContainer().getGraphicalViewer());
		handleDrag();
		unlockTargetEditPart();
		nextContinue = System.currentTimeMillis() + 50;
	}
	
	private Iterable<IFeatureAndContext> getCreateConnectionFeaturesAndContext() {
		//System.out.println("["+getClass().getSimpleName()+"] getCreateConnectionFeaturesAndContext: " + getTargetRequest());
		if (getTargetRequest() instanceof CreateConnectionRequest) {
			List<IFeatureAndContext> ret = new ArrayList<IFeatureAndContext>();
			CreateConnectionRequest r = (CreateConnectionRequest) getTargetRequest();
			//System.out.println("["+getClass().getSimpleName()+"]  > startCommand: " + r.getStartCommand());
			if (r.getStartCommand() instanceof CreateConnectionCommand) {
				CreateConnectionCommand cmd = (CreateConnectionCommand) r.getStartCommand();
				for (IFeatureAndContext ifac : cmd.getFeaturesAndContexts()) {
					if (ifac.getFeature() instanceof ICreateConnectionFeature) {
						ret.add(ifac);
					}
				}
			}
			return ret;
		}
		return Collections.emptyList();
	}
}

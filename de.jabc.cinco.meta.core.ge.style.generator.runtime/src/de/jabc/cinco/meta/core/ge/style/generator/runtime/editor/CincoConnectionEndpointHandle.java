/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor;

import org.eclipse.draw2d.ConnectionLocator;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.RequestConstants;
import org.eclipse.gef.tools.ConnectionEndpointTracker;
import org.eclipse.graphiti.ui.internal.policy.GFConnectionEndpointTracker;
import org.eclipse.graphiti.ui.internal.util.draw2d.GFConnectionEndpointHandle;
import org.eclipse.swt.events.MouseEvent;

class CincoConnectionEndpointHandle extends GFConnectionEndpointHandle {

	public CincoConnectionEndpointHandle(ConnectionEditPart owner, int endPoint) {
		super(owner, endPoint);
	}
	
	@Override
	protected DragTracker createDragTracker() {
		if (isFixed())
			return null;
		ConnectionEndpointTracker tracker = 
			new GFConnectionEndpointTracker((ConnectionEditPart) getOwner()) {
			
			@Override
			public void mouseDrag(MouseEvent me, EditPartViewer viewer) {
				if (!isViewerImportant(viewer))
					return;
				setViewer(viewer);
				boolean wasDragging = movedPastThreshold();
				getCurrentInput().setInput(me);
				handleDrag();
				if (movedPastThreshold()) {
					// At this point wasDragging might be true while NOT being in
					// state DRAG_IN_PROGRESS although the original implementation
					// assumes the latter. Fix: call handleDragStarted
					if (!wasDragging
							|| !isInState(STATE_DRAG_IN_PROGRESS | STATE_ACCESSIBLE_DRAG_IN_PROGRESS)) {
						handleDragStarted();
					}	
					handleDragInProgress();
				}
			}
			
		};
		if (getEndPoint() == ConnectionLocator.SOURCE) {
			tracker.setCommandName(RequestConstants.REQ_RECONNECT_SOURCE);
		} else {
			tracker.setCommandName(RequestConstants.REQ_RECONNECT_TARGET);
		}
		tracker.setDefaultCursor(getCursor());
		return tracker;
	}
}

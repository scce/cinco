/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.features

import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.IRemoveBendpointContext
import org.eclipse.graphiti.features.impl.DefaultRemoveBendpointFeature
import graphmodel.internal.InternalEdge

class CincoRemoveBendpointFeature extends DefaultRemoveBendpointFeature {
	new(IFeatureProvider fp) {
		super(fp)
	}

	override boolean canRemoveBendpoint(IRemoveBendpointContext context) {
		return super.canRemoveBendpoint(context)
	}

	override void removeBendpoint(IRemoveBendpointContext context) {
		super.removeBendpoint(context)
		var InternalEdge edge = (getBusinessObjectForPictogramElement(context.getConnection()) as InternalEdge)
		edge.getBendpoints().remove(context.getBendpointIndex())
	}
}

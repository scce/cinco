/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.customfeature

import de.jabc.cinco.meta.runtime.action.CincoCustomAction
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.IdentifiableElement
import graphmodel.internal.InternalIdentifiableElement
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.context.ICustomContext
import org.eclipse.graphiti.features.custom.AbstractCustomFeature
import org.eclipse.graphiti.features.custom.ICustomFeature
import org.eclipse.graphiti.services.Graphiti

class GraphitiCustomFeature<T extends IdentifiableElement> extends AbstractCustomFeature implements ICustomFeature{
	
	extension WorkbenchExtension = new WorkbenchExtension
	
	CincoCustomAction<T> delegate;
	
	new(IFeatureProvider fp) {
		super(fp)
	}
	
	new(IFeatureProvider fp, CincoCustomAction<T> cca) {
		super(fp)
		this.delegate = cca
	}
	
	override getDescription() {
		delegate.name
	}
	
	override getName() {
		delegate.name
	}
	
	override hasDoneChanges() {
		delegate.hasDoneChanges
	}
	
	override canExecute(ICustomContext context) {
		val pe = context.pictogramElements.get(0);
		val bo = Graphiti.linkService.getBusinessObjectForLinkedPictogramElement(pe)
		switch bo {
			InternalIdentifiableElement : delegate.canExecute(bo.element as T)
			IdentifiableElement : delegate.canExecute(bo as T)
			default : throw new RuntimeException("Error in canExecute with element: " + bo) 
		}
	}
	
	override execute(ICustomContext context) {
		val pe = context.pictogramElements.get(0);
		val bo = Graphiti.linkService.getBusinessObjectForLinkedPictogramElement(pe)
		bo.transact[
			switch bo {
				InternalIdentifiableElement : delegate.execute(bo.element as T)
				IdentifiableElement : delegate.execute(bo as T)
				default : throw new RuntimeException("Error in canExecute with element: " + bo) 
			}
		]
	}
	
	
}

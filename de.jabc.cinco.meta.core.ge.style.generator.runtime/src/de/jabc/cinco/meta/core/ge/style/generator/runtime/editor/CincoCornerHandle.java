/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor;

import org.eclipse.gef.DragTracker;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.tools.DragEditPartsTracker;
import org.eclipse.graphiti.tb.IShapeSelectionInfo;
import org.eclipse.graphiti.ui.internal.config.IConfigurationProviderInternal;
import org.eclipse.graphiti.ui.internal.util.draw2d.GFCornerHandle;

class CincoCornerHandle extends GFCornerHandle {

	private int resizeDirection;
	private boolean movable;

	public CincoCornerHandle(GraphicalEditPart owner, IConfigurationProviderInternal configurationProvider,
			int location, int supportedResizeDirections, boolean movable, IShapeSelectionInfo shapeSelectionInfo) {
		super(owner, configurationProvider, location, supportedResizeDirections, movable, shapeSelectionInfo);
		this.resizeDirection = supportedResizeDirections & location;
		this.movable = movable;
	}
	
	@Override
	protected DragTracker createDragTracker() {
		if (resizeDirection != 0) { // if is resizable
			return new CincoResizeTracker(getOwner(), resizeDirection);
		} else if (movable) {
			DragEditPartsTracker tracker = new CincoDragEditPartsTracker(getOwner());
			tracker.setDefaultCursor(getCursor());
			return tracker;
		} else {
			return null;
		}
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.impl.CreateContext;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.createfeature.CincoCreateFeature;

public class CincoAddFeaturePrime extends CincoAddFeature {

	protected CincoCreateFeature<?> createFeature;
	
	public CincoAddFeaturePrime(IFeatureProvider fp) {
		super(fp);
	}

	public void throwException(IAddContext ac) {
		CreateContext cc = new CreateContext();
		cc.setHeight(ac.getHeight());
		cc.setWidth(ac.getWidth());
		cc.setX(ac.getX());
		cc.setY(ac.getY());
		cc.setTargetContainer(ac.getTargetContainer());
		createFeature.canCreate(cc);
		createFeature.throwException(cc);
	}
}

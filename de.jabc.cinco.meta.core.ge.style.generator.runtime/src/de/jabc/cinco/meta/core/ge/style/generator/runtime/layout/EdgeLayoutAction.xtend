/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.layout

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.Edge
import graphmodel.internal.InternalEdge
import org.eclipse.gef.EditPart
import org.eclipse.gef.GraphicalEditPart
import org.eclipse.gef.Request
import org.eclipse.gef.ui.actions.SelectionAction
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.ui.IWorkbenchPart

import static extension org.eclipse.gef.tools.ToolUtilities.*

class EdgeLayoutAction extends SelectionAction {
	
	extension val WorkbenchExtension = new WorkbenchExtension
	
	final EdgeLayout layout
	Iterable<InternalEdge> selectedEdges
	
	new(IWorkbenchPart part, EdgeLayout layout) {
		super(part)
		this.layout = layout
	}
	
	override getId() {
		layout.id
	}
	
	override getText() {
		layout.text
	}
	
	override getImageDescriptor() {
		layout.imageDescriptor
	}
	
	override getDisabledImageDescriptor() {
		layout.imageDescriptor
	}
	
	override calculateEnabled() {
		val selection = getSelection(null)
			.map[model]
			.filter(PictogramElement)
			.map[businessObject]
		val onlyEdges = !selection.exists[!(it instanceof InternalEdge)]
		val enabled = onlyEdges && !selection.isEmpty
		selectedEdges = if (enabled) selection.filter(InternalEdge) else #[]
		return enabled
	}
	
	override run() {
		selectedEdges.head?.transact("Edge layouting") [
			selectedEdges.forEach[ layout.apply(it.element as Edge) ]
		]
	}
	
	def Iterable<EditPart> getSelection(Request request) {
		val allSelected = selectedObjects
		if (allSelected.isEmpty || !(allSelected.head instanceof GraphicalEditPart))
			return #[]
//		val primary = allSelected.last
		val selected = allSelected.selectionWithoutDependants
			.filter(EditPart)
//		if (selected.size < 2 || !selected.contains(primary))
//			return #[]
		return selected
	}
	
}

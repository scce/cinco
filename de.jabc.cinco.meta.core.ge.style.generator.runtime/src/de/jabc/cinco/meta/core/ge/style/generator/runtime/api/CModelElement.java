/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.api;

import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

public interface CModelElement {

	public <T extends PictogramElement> T getPictogramElement();
	public <T extends PictogramElement> void setPictogramElement(T pe);
	
	public Diagram getDiagram();
	
	void delete();
	
	default void addLinksToDiagram(PictogramElement pe) {
		getDiagram().getPictogramLinks().add(pe.getLink());
		if (pe instanceof org.eclipse.graphiti.mm.pictograms.ContainerShape) {
			((org.eclipse.graphiti.mm.pictograms.ContainerShape) pe).getChildren().forEach(c -> addLinksToDiagram(c));
		}
		if (pe instanceof org.eclipse.graphiti.mm.pictograms.Connection) {
			((org.eclipse.graphiti.mm.pictograms.Connection) pe).getConnectionDecorators().forEach(cd -> addLinksToDiagram(cd));
		}
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.generator.runtime.editor;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.ui.editor.DiagramBehavior;
import org.eclipse.graphiti.ui.internal.config.ConfigurationProvider;
import org.eclipse.graphiti.ui.internal.policy.IEditPolicyFactory;

/**
 * @author Steve Bosselmann
 */
public class CincoConfigurationProvider extends ConfigurationProvider {

	IEditPolicyFactory editPolicyFactory;
	
	public CincoConfigurationProvider(DiagramBehavior diagramBehavior, IDiagramTypeProvider diagramTypeProvider) {
		super(diagramBehavior, diagramTypeProvider);
	}
	
	@Override
	public IEditPolicyFactory getEditPolicyFactory() {
		if (editPolicyFactory == null && !isDisposed()) {
			editPolicyFactory = new CincoEditPolicyFactory(this);
		}
		return editPolicyFactory;
	}
	
	@Override
	public void dispose() {
		editPolicyFactory = null;
		super.dispose();
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.gratext;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult;
import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator;
import productDefinition.Annotation;

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.newError;

public class CPDValidator implements IMetaPluginValidator {

	@Override
	public ValidationResult<String, EStructuralFeature> checkAll(EObject eObject) {
		if (eObject instanceof Annotation) {
			Annotation a = (Annotation) eObject;
			if (a.getName().equals("disableGratext")
					|| a.getName().equals("sortGratext")) {
				if (!a.getValue().isEmpty()) {
					return newError("No arguments allowed.",
							eObject.eClass().getEStructuralFeature("value"));
				}
			}
		}
		return null;
	}

}

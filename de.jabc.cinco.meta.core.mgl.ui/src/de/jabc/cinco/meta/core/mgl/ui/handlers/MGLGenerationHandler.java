/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2;
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess2.IFileCallback;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.OutputConfiguration;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.inject.Inject;
import com.google.inject.Provider;

import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class MGLGenerationHandler extends AbstractHandler {
    @Inject
    private IGenerator generator;
 
    @Inject
    private Provider<EclipseResourceFileSystemAccess2> fileAccessProvider;
     
    @Inject
    IResourceDescriptions resourceDescriptions;
     
    @Inject
    IResourceSetProvider resourceSetProvider;

	/**
	 * The constructor.
	 */
	public MGLGenerationHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException{
		try {
			callGenerator();
		} catch (Exception e) {
			throw new ExecutionException("Exception in MGL 2 Ecore Transformation", e);
		}
		
		return null;
	}

	private void callGenerator() {
		IFile file = MGLSelectionListener.INSTANCE.getCurrentMGLFile();
		ResourceSet rSet = resourceSetProvider.get(file.getProject());
		
		Resource res = rSet.createResource(URI.createPlatformResourceURI(file.getFullPath().toOSString(), true));
		
		try {
			res.load(null);
			EclipseResourceFileSystemAccess2 access = fileAccessProvider.get();
			access.setProject(file.getProject());
			access.setMonitor(null);
			OutputConfiguration defaultOutput = new OutputConfiguration("DEFAULT_OUTPUT");
		    defaultOutput.setOutputDirectory("./src-gen");
		    defaultOutput.setCreateOutputDirectory(true);
		    defaultOutput.setOverrideExistingResources(true);
		    defaultOutput.setCleanUpDerivedResources(true);
		    defaultOutput.setSetDerivedProperty(true);
		    defaultOutput.setCanClearOutputDirectory(true);
		    
			access.getOutputConfigurations().put("DEFAULT_OUTPUT", defaultOutput);
			access.setPostProcessor(new IFileCallback() {
				
				@Override
				public boolean beforeFileDeletion(IFile file) {
					return false;
				}
				
				@Override
				public void afterFileUpdate(IFile file) {}
				
				@Override
				public void afterFileCreation(IFile file) {}
			});
			
			generator.doGenerate(res, access);

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		try {
			file.getProject().refreshLocal(0, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

}

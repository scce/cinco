# Changelog
All notable changes to this project will be documented in this file.

The format after release 1.1 is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Removed
* Removed `manhattan` connection type from edge styles (this connection type had no effect prior)

### Changed
* Overhaul of the CPD and MGL meta plugin API (issue [#315](https://gitlab.com/scce/cinco/-/issues/315))
  * Renamed `de.jabc.cinco.meta.core.pluginregistry.IMetaPlugin` to `IMGLMetaPlugin`
  * Renamed `de.jabc.cinco.meta.plugin.CincoPlugin` to `CincoMetaPlugin`
  * Changed signature of `ICPDMetaPlugin`'s and `IMGLMetaPlugin`'s `execute...MetaPlugin(...)` to be similar
  * Added ability to change execution order of CPD/MGL meta plugins via the `get...MetaPluginPriority()` method
  * CPD/MGL meta plugins will be activated only once per generation process (instead of once per GraphModel)
  * A MGL meta plugin will be activated, if its there is at least one corresponding annotation at any model element in any MGL
* Overhaul of the `de.jabc.cinco.meta.core.ui.handlers.CincoProductGenerationHandler`
  * Refactoring and simplification
  * Removed duplicate generation processes
  * Added more granular time measurement for the generation process
* Addition of several utilities (used in `CincoProductGenerationHandler`):
  * `de.jabc.cinco.meta.core.ui.utils.DirectedGraph` for creating and evaluating directed graphs, e.g. for dependency analysis
  * `de.jabc.cinco.meta.core.ui.utils.Stopwatch` for runtime measurements
  * `de.jabc.cinco.meta.core.ui.utils.Table` for pretty printing tables to the console
* Added new event types to the [Event API](https://gitlab.com/scce/cinco/-/wikis/Additional-Meta-Plugins/Event-API-(Event-Meta-Plugin)#supported-events)
  * canAttributeChange (for single-valued attributes)
  * preAttributeChange (for single-valued attributes)
  * canCreate (closes issue [#281](https://gitlab.com/scce/cinco/-/issues/281))
  * canDelete
  * canDoubleClick
  * postDoubleClick (for GraphModels)
  * canMove
  * canReconnect
  * canResize
  * canSelect
* The Event API Extension (`de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension`) has new methods for parsing attribute values, e. g.:
  * `toColor(String colorString)` creates a `Color` from a String value
  * `getModel(String code, Class<M> modelClass, String fileExtension, Class<S> standaloneSetupClass)` parses a String to the AST of a Xtext grammar
  * `getErrors(String code, String fileExtension, Class<S> standaloneSetupClass)` returns a list of validation errors
  * Convinience methods for reading `Date` objects, like: `getYear(Date date)`, `getDayOfWeek(Date date)`, `isWeekday(Date date)`, ...
* Refactored the Cinco Properties View
  * `@color` attributes show the currently selected color in the color picker button
  * `@grammar` attributes use the "Text" font, configured in the Eclipse settings
  * Single-valued attributes will be validated depending on their type
  * Single-valued attributes can show a customizable error message via the canAttributeChange event
* Added ability to write MGL validation methods (`de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator`), that run
  * on edit (`IMetaPluginValidator.checkOnEdit(EObject)`),
  * on save (`IMetaPluginValidator.checkOnSave(EObject)`),
  * or only on request (`IMetaPluginValidator.checkOnRequest(EObject)`).
* Added ability to set macOS (icns) and Windows (ico) icons in addition to the Linux icon (xpm) via the CPD (`macOSIcon`, `windowsIcon`)
* Added ability to set the 256x256 branding icon in addition to the 16x16, 32x32, 48x48, 64x64, 128x128 icons via the CPD (`image256`)

### Fixed
* MGLUtils `*withOrigins` caused unwanted side effects
* Fixed issue where "Sub-MGLs" wouldn't be re-generated, if their "Super-MGL" changed (issue [#346](https://gitlab.com/scce/cinco/-/issues/346))
* Fixed issue where the use of Java keywords as ModelElement names could cause issues (issue [#360](https://gitlab.com/scce/cinco/-/issues/360))
* Fixed issue where `edge.addBendpoint(x, y)` would cause an IllegalStateException, if it is not wrapped in an transaction (issue [#361](https://gitlab.com/scce/cinco/-/issues/361))
* Fixed issue where you cannot select "none" for a ModelElement attribute in the "Cinco Properties View" ([issue #104](https://gitlab.com/scce/cinco/-/issues/104))
* Fixed issue where unused model elements were not considered when computing most common types (e.g. of getRootElement()) of super types (issue [#392](https://gitlab.com/scce/cinco/-/issues/392))

## [2.0] - 2021-03-15

### Added
* Enable ID calculation for the Reference Registry via extension point (#236)
* MGL modularity/Cross MGL inheritance
* Scoped wildcards for containements and connection relationships
* Anchors can be customized using the 'anchor' keyword for shapes in the MSL
* Cinco Event System (#262)
  * EventHub: A centralized event handling interface
  * Event API: Evolution of Cinco hooks
  * More info in the [Wiki](https://gitlab.com/scce/cinco/-/wikis/Additional-Meta-Plugins/Event-API-(Event-Meta-Plugin))
* Startup meta plugin (#225): Lets Cinco users run code at Cinco product startup
* Style property within graphical model elements as a replacement for the removed style annotation (#266)

### Changed
* Rewrite MGL Ecore generator (priorly known as MGLAlternateGenerator)
* Removed `CincoPostResizeHook.postResize(T modelElement, int deltaWidth, int deltaHeight)` and
  `postResize(T modelElement, int direction, int deltaWidth, int deltaHeight)` (#235)
  * Use `postResize(T modelElement, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction)` instead
* Added new `graphmodel.Node.resize(int width, int height, int x, int y)` method
* Changed Java Version. *Java 11* is now required
* Changed Eclipse Version. CINCO is now based on *Eclipse 2020-06*.
* Removed the style annotation and replaced it with 

### Fixed
* `@postResize` did not trigger (#265)
* `@preSave` did not trigger
* Prime references in prime references (#259)
* "No model changes! Generate anyways?" dialog now displays the message correctly

### Removed
* Style annotation (has been replaced by the new style property) (#266)

## [0.7] - 2016-10-17
- Technology:
  - Java 8 is now supported/required
  - Removed support for 32 bit systems (sorry, ancients ;))
  - New "GraText" textual serialization format for all models

- General Changes/Addition:
  - new CINCO Properties view: can display hierarchic attributes
  - Generated perspective per Cinco product
  - Generated "new Project" wizard per product
  - Generated "new model" wizard per MGL
  - Model and project validation with MCaM framework
  - Example generator now produces README.txt explaining selected features

- MGL Changes:
  - New Hooks: postAttributeValueChange, preDelete, postMove, postResize, postSelect (all use C-prefixed transformation API)
  - @disable annotation with comma-separated list of: create, move, resize, reconnect, select, delete
  - @readOnly annotation for attributes
  - @file annotation on EString attribute produces file selector in property view
  - set-based constraints for containableElements (e.g. {A,B}[1,5])
  - passed parameters to @style annotation can now be arbitrary EL Expressions

- Advanced Prime References
  - directly import MGLs to use for prime references
  - import with "as" to provide namespace
  - namespace notation for prime references ( "::" for MGLs, "." for Ecore, "this::" for the same MGL)
  - Containers can be prime references
  - Support for multiple prime referencing nodes for same type (then produces GUI to select type)

- Graphical Editor
  - support for polygon resizing
  - image URLs can be set at runtime via appearance provider

- Style Changes
  - type keyword in edgeStyle: manhattan or freeform
  - Removed "relativeTo" keyword; positions now relative to the parent shape
  - Margins (positive and negative) for relative position
  - Named decorators in edge styles to reference in appearance provider
  - Shapes can have fixed size (i.e. not scaling when node is resized)


## [0.6] - 2015-05-13
- new Cinco Product Definition (CPD) file format that enables branding of CPs
  and more easy merging of multiple MGLs into a single CP
- "Generate Cinco Product" action moved to context menu of CPD file
- support for the simplified export of a stand-alone CP
- new annotation for post-create hooks (@postCreate) that is invoked after a
  node/edge/model is created (example generated by wizard)
- A transformation API is now generated for every Cinco product. It allows one
  to very easily implement model changes (e.g. in custom actions or post-create
  hooks) that are automatically reflected in both the visual representation as
  well as the underlying model (example generated by wizard)
- every model element is now uniquely identifiably, as an ID generated
  automatically
- multiple text shapes can now be used for node layouts
- initial version of the spreadsheet meta plug-in that allows for the
  definition of calculating semantics using Excel or similar tools
- improved expressiveness of cardinality constraints for containers
- new annotation (@palette) to sort the nodes and containers in the palette
  into custom groups (example generated by wizard)

## [0.5] - 2014-09-25
- advanced copy&paste feature that fully preserves layout
- enhanced cinco wizard that allows for the initialization of various example
  features (containers, prime references, icons, appearance provider, code
  generator, custom actions)
- cinco custom actions (formerly custom features) now possible for context
  menu and doubleclick
- unified file referencing (e.g. for style file, icons): bundle-relative path
  or platform URI
- unified Java class referencing (e.g. for custom action or generatable annotations):
  bundleName + FQ class name (bundle may be omitted when referring to "this bundle")
- added support for generating jABC4 Service Graphs for Cinco Product models.
  This enables code generators, interpreters, and model transformations to be modeled
  within jABC4.
- extended MGL/Style validators and content assist (more errors; more help)
- @icon annotation (node, container, edge) for the create tools in the tool palette
- lots polishing, small features, and bugfixing

## [0.4] - 2014-08-13
- possible source and target nodes of edges is now defined at the node's end
  with keywords incomingEdges and outgoingEdges, allowing cardinalities (the
  keywords sourceNode and targetNode have been removed)
- basic copy&paste features are now generated (not yet preserving some minor
  graphical properties, such as edge bend points and decorator locations)
- added possibility to define appearances "inline"
- appearances can now be defined for edgeStyle and edge decorators
- adapted annotation syntax to be more like Java: @annotationName(param1, param2, ...)
- renamed @Style annotation to @style
- "decorators {}"-syntax for edges changed to multiple "decorator {}"-blocks
- the .style editor now validates the parameters of the @style annotation
- new @multiLine annotation for attributes
- added predefined edge decorator shapes (ARROW, CIRCLE, DIAMOND, TRIANGLE)
- define CustomFeatures on nodes and containers with @customFeature annotation
- other node types can now be used as attributes
- error/success dialog after "Generate Cinco Product"
- again, many other small features and fixes

## [0.3] - 2014-04-16
- containers can now contain other containers (thus containableNodes is renamed
  to containableElements)
- containableElements keyword can also be used for the root model
- Selecting edge decorators now shows the correct property view and highlights
  the corresponding edge
– Several methods are generated into the model code, creating a more
  domain-specific API (e.g. typed getters for successors, predecessors, contained
  elements, ...)
- Many other small features and fixes

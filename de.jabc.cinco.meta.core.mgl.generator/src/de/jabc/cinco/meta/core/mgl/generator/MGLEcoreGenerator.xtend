/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator

import de.jabc.cinco.meta.core.mgl.generator.elements.ElementEClasses
import de.jabc.cinco.meta.core.mgl.generator.extensions.NodeMethodsGeneratorExtensions
import de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventApiExtension
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import de.jabc.cinco.meta.util.xapi.CollectionExtension
import graphmodel.GraphmodelPackage
import graphmodel.ModelElementContainer
import graphmodel.internal.InternalNode
import graphmodel.internal.InternalPackage
import java.io.IOException
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.Map
import java.util.Set
import mgl.Attribute
import mgl.ComplexAttribute
import mgl.ContainingElement
import mgl.EDataTypeType
import mgl.Edge
import mgl.Enumeration
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.PrimitiveAttribute
import mgl.ReferencedEClass
import mgl.ReferencedModelElement
import mgl.UserDefinedType
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EEnumLiteral
import org.eclipse.emf.ecore.EModelElement
import org.eclipse.emf.ecore.EOperation
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import productDefinition.CincoProduct

import static extension de.jabc.cinco.meta.core.mgl.generator.extensions.EcoreExtensions.*
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class MGLEcoreGenerator {

	extension CollectionExtension = new CollectionExtension
	extension GeneratorUtils = GeneratorUtils.instance
	extension NodeMethodsGeneratorExtensions = new NodeMethodsGeneratorExtensions
	extension EventApiExtension = new EventApiExtension

	public val allElementEClasses = new HashSet<ElementEClasses>()
	val eEnumMap = new HashMap<Enumeration, EEnum>()

	/**
	 * Create an {@link EPackage} that fully provides representations of the provided <code>mdlModels</code> needed for the resulting CINCO product.
	 *
	 * The generated {@link EPackage} contains every needed {@link EClass} representation of every {@link ModelElement} and also every needed
	 * {@link EAttribute}, {@link EOperation} and {@link EEnum}.
	 *
	 *
	 * This method also checks whether the provided <code>importedExternalMGLs</code> have been generated properly and whether these generated resources are accessible.
	 *
	 * @param mglModels				an {@link Iterable} that contains every {@link MGLModel} for which representations should be included in the resulting {@link EPackage}
	 * @param importedExternalMGLs	a {@link Map} that matches all imported external {@link MGLModels} to their generated {@link Resource}
	 * @param packageMGLModelMap	a {@link Map} that matches all {@link MGLModel MGLModels} to their {@link EPackage} representation. These {@link EPackage EPackages} are sub-packages
	 * 								of the returned {@link EPackage}. The provided <code>packageMGLModelMap</code> is used in {@link de.jabc.cinco.meta.core.mgl.generator.MGLGenerator MGLGenerator}
	 * 								for the creation of the GenModel
	 * @param cp					the {@link CincoProduct} for which this Ecore model should be created. It provides some meta-data used in the returned {@link EPackage}
	 * @return	a {@link EPackage} that represents all provided <code>mglModels</code> as needed for the CINCO product
	 */
	def EPackage generateEcoreModels(Iterable<MGLModel> mglModels, Map<MGLModel, Resource> importedExternalMGLs, Map<EModelElement, MGLModel> packageMGLModelMap, CincoProduct cp) {
		checkExternalMGLsHaveBeenGenerated(importedExternalMGLs)

		val rootPackage = createEPackage(cp.name, cp.name, "info.scce.cinco.product")

		// Create subpackages for every MGLModel and populate them
		for(mglModel : mglModels) {
			val mglEPackage = createEPackage(mglModel.fileName, mglModel.fileName, mglModel.nsURI)

			// TODO get rid of this map
			packageMGLModelMap.put(mglEPackage, mglModel)

			rootPackage.ESubpackages += mglEPackage

			// Create subpackages internal and views
			val mglInternalEPackage = createEPackage("internal", mglModel.fileName + "-internal", mglModel.nsURI + "/internal")
			mglEPackage.ESubpackages += mglInternalEPackage
			val mglViewsEPackage = createEPackage("views", mglModel.fileName + "-views", mglModel.nsURI + "/views")
			mglEPackage.ESubpackages += mglViewsEPackage

			// Create EClasses for every model element and add them to the associated EPackages
			mglModel.modelElements.map[new ElementEClasses(it)].forEach[
				allElementEClasses.add(it)
				mglEPackage.EClassifiers += it.mainEClass
				mglInternalEPackage.EClassifiers += it.internalEClass
				mglViewsEPackage.EClassifiers += it.mainView
			]
			mglModel.types.filter(UserDefinedType).map[new ElementEClasses(it)].forEach[
				allElementEClasses.add(it)
				mglEPackage.EClassifiers += it.mainEClass
				mglInternalEPackage.EClassifiers += it.internalEClass
				mglViewsEPackage.EClassifiers += it.mainView
			]

			// Create EClasses for every enumeration and add them to the associated EPackages
			mglModel.types.filter(Enumeration).forEach[
				val eEnum = it.createEnumeration
				eEnumMap.put(it, eEnum)
				mglEPackage.EClassifiers += eEnum
			]
		}

		addAttributesToInternalEClasses(allElementEClasses)

		// Populate EClasses depending of their associated model element types
		populateAll(allElementEClasses)
		populateModelElementEClasses(allElementEClasses)
		populateContainingElementEClasses(allElementEClasses)
		populateGraphModelEClasses(allElementEClasses)
		populateNodeEClasses(allElementEClasses)
		populateEdgeEClasses(allElementEClasses)

		// Add inheritances
		setSuperTypes(allElementEClasses)

		// Add references to views and populate the views
		referenceAndPopulateMainView(allElementEClasses)

		return rootPackage
	}

	/**
	 * Checks whether the Ecore model has been fully generated for each provided {@link MGLModel}.
	 *
	 * The ecore model is considered fully generated if for every model element an {@link EClass}, an internal {@link EClass}
	 * and a view {@link EClass} is present in the provided Ecore {@link Resource}.
	 *
	 * @param	importedExternalMGLs	a {@link Map} that contains all imported external {@link MGLModel MGLModels} and their
	 * 									associated Ecore {@link Resource}
	 * @throws	RuntimeException	a {@link RuntimeException} is thrown if any provided Ecore {@link Resource} does not contain
	 * 								an {@link EClass}, an internal {@link EClass} and a view {@link EClass} for every associated
	 * 								{@link MGLModel}
	 */
	private def void checkExternalMGLsHaveBeenGenerated(Map<MGLModel, Resource> importedExternalMGLs) {
		for(externalMGLEntry : importedExternalMGLs.entrySet) {
			val mglEcore = externalMGLEntry.value
			mglEcore.load(null)
			val ecoreEClasses = mglEcore.allContents.toList.filter(EClass)
			externalMGLEntry.key.modelElements.forEach[
				val eClass = ecoreEClasses.findFirst[eObj | eObj.name == it.name]
				val internalEClass = ecoreEClasses.findFirst[eObj | eObj.name == ("Internal" + it.name)]
				val mainView = ecoreEClasses.findFirst[eObj | eObj.name == (it.name + "View")]
				if(eClass === null || internalEClass === null || mainView === null) {
					throw new RuntimeException("The ecore representation of " + it.name + " could not be found.")
				}
			]
		}
	}

	/**
	 * Creates and returns a new {@link EPackage} with the provided <code>name</code>, <code>prefix</code>, and <code>nsURI</code>.
	 *
	 * @param	name	a {@link String} that represents the name of the new {@link EPackage}
	 * @param	prefix	a {@link String} that represents the prefix of the new {@link EPackage}
	 * @param	nsURI	a {@link String} that represents the nsURI of the new {@link EPackage}
	 * @return	an {@link EPackage} with the provided <code>name</code>, namespace <code>prefix</code>, and <code>nsURI</code>
	 */
	private def EPackage createEPackage(String name, String prefix, String nsURI) {
		var ePackage = EcoreFactory.eINSTANCE.createEPackage
		ePackage.name = name.toLowerCase
		ePackage.nsPrefix = prefix.toLowerCase
		ePackage.nsURI = nsURI
		return ePackage
	}

	/**
	 * Creates and populates an EEnum that represents the provided <code>enumeration</code>.
	 * All names and literals are set to match the <code>enumeration</code>'s values.
	 *
	 * @param	enumeration	an {@link Enumeration} for which an EEnum representation should be created and populates
	 * @return	the {@link EEnum} that represents the provided <code>enumeration</code>
	 */
	private def EEnum createEnumeration(Enumeration enumeration){
		val eEnum = EcoreFactory.eINSTANCE.createEEnum
		eEnum.name = enumeration.name
		val literals = new ArrayList<EEnumLiteral>
		enumeration.literals.forEach[literal, index |
			var eLiteral = EcoreFactory.eINSTANCE.createEEnumLiteral
			eLiteral.literal = literal
			eLiteral.name = literal
			eLiteral.value = index
			literals += eLiteral
		]
		eEnum.ELiterals += literals
		return eEnum
	}

	/**
	 * Creates {@link EAttribute EAttributes} in the {@link EClass} of every provided {@link ElementEClasses} of <code>elementEClasses</code>.
	 *
	 * All of the {@link ElementEClasses}' {@link ModelElement}'s {@link Attribute Attributes} are considered.
	 * Bounds, names, types and default values will also be set.
	 *
	 * @param	elementEClasses	a {@link Set} which contains all {@link ElementEClasses} which should be populated with {@link EAttribute} representations
	 */
	private def void addAttributesToInternalEClasses(Set<ElementEClasses> elementEClasses) {
		for (eec: elementEClasses) {
			for (attribute: eec.modelElement.allAttributes(false)) {
				switch attribute {
					ComplexAttribute case attribute.type instanceof Enumeration: {
						eec
							.internalEClass
							.createEAttribute(
								attribute.name,
								eEnumMap.get(attribute.type),
								attribute.lowerBound,
								attribute.upperBound
							)
							.setDefaultValue(attribute.defaultValue)
					}
					ComplexAttribute: {
						eec
							.internalEClass
							.createReference(
								attribute.name,
								elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, attribute.type)].mainEClass,
								attribute.lowerBound,
								attribute.upperBound,
								attribute.type instanceof UserDefinedType,
								null
							)
					}
					PrimitiveAttribute: {
						eec
							.internalEClass
							.createEAttribute(
								attribute.name,
								attribute.type.getEDataType,
								attribute.lowerBound,
								attribute.upperBound
							)
							.setDefaultValue(attribute.defaultValue)
					}
				}
			}
		}
	}

	/**
	 * Populates <strong>every</strong> {@link EClass EClasses} of the provided <code>elementEClasses</code> with the following properties:
	 *
	 * <ul>
	 * 	<li>Getter, setter, adder and remover for every attribute</li>
	 * 	<li><code>getInternal*</code> methods</li>
	 * 	<li><code>isExactly*</code> methods</li>
	 * </ul>
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} that should be populated
	 */
	private def void populateAll(Set<ElementEClasses> elementEClasses) {
		elementEClasses.forEach[
			// Create getter, setter, and optionally adder and remover for every non-conflicting attribute
			it.modelElement.nonConflictingAttributes.forEach[attribute |
				it.mainEClass.createGetter(it, attribute)
				it.mainEClass.createSetter(it, attribute)
				if (attribute.upperBound != 1) {
					it.mainEClass.createAdder(it, attribute)
					it.mainEClass.createRemove(it, attribute)
				}
			]

			// Create additional methods for every model element EClass
			it.mainEClass.createEOperation(
				'''getInternal«it.modelElement.name»''',
				it.internalEClass, 0 ,1 ,
				'''return («it.modelElement.fqInternalBeanName») getInternalElement_();'''
			)
			// TODO What's the point of this if it always returns true?
			it.mainEClass.createEOperation(
				'''isExactly«it.modelElement.name»''',
				EcorePackage.eINSTANCE.EBoolean, 0, 1,
				"return true;"
			)
		]
	}

	/**
	 * Populates every {@link EClass EClasses} representing {@link ModelElement ModelElements} of the provided
	 * <code>elementEClasses</code> with the following properties:
	 *
	 * <ul>
	 * 	<li><code>preDelete</code> methods</li>
	 * 	<li><code>postDelete</code> methods</li>
	 * 	<li><code>postSave</code> methods</li>
	 * 	<li><code>getRootElement</code> methods</li>
	 * </ul>
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} that should be populated
	 */
	private def void populateModelElementEClasses(Set<ElementEClasses> elementEClasses) {
		elementEClasses.filter[it.modelElement instanceof ModelElement && !(it.modelElement instanceof UserDefinedType)].forEach[
			val mostCommonGraphModel = modelElement.mostCommonGraphModel(elementEClasses)

			it.mainEClass.createEOperation(
				"preDelete",
				null, 1, 1,
				'''
					«val preDeleteAnnotation = it.modelElement.getAnnotation("preDelete")»
					«IF preDeleteAnnotation !== null»
						new «preDeleteAnnotation.value.get(0)»().preDelete(this);
					«ENDIF»
					«EventEnum.PRE_DELETE.getNotifyCallJava(modelElement, 'this')»
				'''
			)

			it.mainEClass.createEOperation(
				"getPostDeleteFunction",
				GraphmodelPackage.eINSTANCE.runnable,
				1, 1,
				'''
					«val postDeleteAnnotation = it.modelElement.getAnnotation("postDelete")»
					«IF postDeleteAnnotation !== null»
						de.jabc.cinco.meta.runtime.hook.CincoPostDeleteHook<? super «it.modelElement.fqBeanName»> postDeleteHook = new «postDeleteAnnotation.value.get(0)»();
						return postDeleteHook.getPostDeleteFunction(this);
					«ELSE»
						return () -> {};
					«ENDIF»
				'''
			)
			
			it.mainEClass.createEOperation(
				"getPostDeleteEvent",
				GraphmodelPackage.eINSTANCE.runnable,
				1, 1,
				'''
					«IF modelElement.isEventEnabled && EventEnum.POST_DELETE.accepts(modelElement)»
						«EventEnum.POST_DELETE.getNotifyCallJava('''postDelete«modelElement.fuName»Runnable''', modelElement, 'this')»
						if (postDelete«modelElement.fuName»Runnable == null) {
							return () -> {};
						}
						else {
							return postDelete«modelElement.fuName»Runnable;
						}
					«ELSE»
						return () -> {};
					«ENDIF»
				'''
			)
			
			it.mainEClass.createEOperation(
				"preSave",
				null, 1, 1,
				modelElement.writeMethodCallPreSave('this.getRootElement()')
			)
			
			it.mainEClass.createEOperation(
				"postSave",
				null, 1, 1,
				modelElement.writeMethodCallPostSave('this.getRootElement()')
			)

			it.mainEClass.createEOperation(
				"getRootElement",
				elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, mostCommonGraphModel)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("GraphModel"),
				0, 1,
				'''
					«IF it.modelElement instanceof GraphModel»
						return this;
					«ELSE»
						«/* there might be no internal element during the model creation phase */»
						if (this.getInternalElement_() != null && this.getInternalElement_().getRootElement() != null) {
							return («IF mostCommonGraphModel !== null»«mostCommonGraphModel.fqBeanName»«ELSE»graphmodel.GraphModel«ENDIF») this.getInternalElement_().getRootElement().getElement();
						}
						return null;
					«ENDIF»
				'''
			)
		]
	}

	/**
	 * Populates every {@link EClass EClasses} representing {@link ContainingElement ContainingElements} of the provided
	 * <code>elementEClasses</code> with the following properties:
	 *
	 * <ul>
	 * 	<li><code>getNodes</code> methods</li>
	 * 	<li><code>getContainmentConstraints</code> methods</li>
	 * 	<li><code>get*NodeName*s</code> methods</li>
	 * 	<li><code>canNew*NodeName*</code> methods</li>
	 *  <li><code>new*NodeName*</code> methods for prime-references and non-prime types</li>
	 * </ul>
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} that should be populated
	 */
	private def void populateContainingElementEClasses(Set<ElementEClasses> elementEClasses) {
		elementEClasses.filter[it.modelElement instanceof ContainingElement].forEach[
			val containingElements = MGLUtil.removeDuplicateModelElements((it.modelElement as ContainingElement).containableNodes.flatMap[cn| #[cn] + cn.allNodeSubTypes.filter(Node)]).filter(Node)
			
			it.mainEClass.createGenericListEOperation(
				"getNodes",
				GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
				0, -1,
				'''
					return org.eclipse.emf.common.util.ECollections.unmodifiableEList(getInternalContainerElement().getModelElements()
							.stream().filter(me -> me instanceof graphmodel.internal.InternalNode).map(me -> (graphmodel.Node)me.getElement()).
								collect(java.util.stream.Collectors.toList()));
				'''
			)
			it.internalEClass.createEOperation(
				"getContainmentConstraints",
				GraphmodelPackage.eINSTANCE.containmentConstraint, 0, -1,
				'''
					 org.eclipse.emf.common.util.BasicEList<ContainmentConstraint>constraints =
						new org.eclipse.emf.common.util.BasicEList<ContainmentConstraint>();
					«FOR containmentConstraint : (it.modelElement as ContainingElement).allContainmentConstraints.filter[types.size>0]»
						«containmentConstraint.containmentConstraint»
					«ENDFOR»
					return constraints;
				'''
			)
			containingElements.forEach[containableNode |
				it.mainEClass.createEOperation(
					'''get«containableNode.fuName»s''',
					elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass,
					0, -1,
					'''return getModelElements(«containableNode.fqBeanName».class);'''
				)

				if(!containableNode.isIsAbstract) {
					it.mainEClass.createEOperation(
						'''canNew«containableNode.fuName»''',
						EcorePackage.eINSTANCE.EBoolean,
						1, 1,
						'''return this.canContain(«containableNode.fqBeanName».class);'''
					)

					if(containableNode.isPrime) {
						val newPrimeNodeSimpleMethodContent = '''return new«containableNode.fuName»(«containableNode.primeName»,x,y,-1,-1);'''
						val newPrimeNodeMethodContent = '''
							if (this.canContain(«containableNode.fqBeanName».class)) {
								«containableNode.fqBeanName» node = «containableNode.fqFactoryName».eINSTANCE.create«containableNode.fuName»();
								this.getInternalContainerElement().getModelElements().add(node.getInternalElement_());
								((«containableNode.fqInternalBeanName») node.getInternalElement_())
									.setLibraryComponentUID(org.eclipse.emf.ecore.util.EcoreUtil.getID(«containableNode.primeName»));
								node.move(x, y);
								node.resize(width, height);
								«IF containableNode.hasPostCreateHook»
									«containableNode.fqFactoryName».eINSTANCE.postCreates(node);
								«ENDIF»
								return node;
							} else throw new «RuntimeException.name»(
								«String.name».format("Cannot add node %s to %s", «containableNode.fuName».class, this.getClass()));
						'''

						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1, 1,
							newPrimeNodeSimpleMethodContent,
							createEObjectParameter(containableNode.primeName, 1, 1),
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1)
						)
						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1, 1,
							newPrimeNodeSimpleMethodContent,
							createEObjectParameter(containableNode.primeName, 1, 1),
							createEStringParameter("id", 1, 1),
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1)
						)
						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1, 1,
							newPrimeNodeMethodContent,
							createEObjectParameter(containableNode.primeName, 1, 1),
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1),
							createEIntParameter("width", 1, 1),
							createEIntParameter("height", 1, 1)
						)
						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1, 1,
							newPrimeNodeMethodContent,
							createEObjectParameter(containableNode.primeName, 1, 1),
							createEStringParameter("id", 1, 1),
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1),
							createEIntParameter("width", 1, 1),
							createEIntParameter("height", 1, 1)
						)
					} else {
						val newNodeSimpleMethodContent = '''return new«containableNode.fuName»(x, y, -1, -1);'''
						val newNodeMethodContent = '''
							if (this.canContain(«containableNode.fqBeanName».class)) {
								«containableNode.fqBeanName» node = «containableNode.fqFactoryName».eINSTANCE.create«containableNode.fuName»((graphmodel.internal.InternalModelElementContainer) this.getInternalElement_());
								this.getInternalContainerElement().getModelElements().add(node.getInternalElement_());
								node.move(x, y);
								node.resize(width, height);
								return node;
							} else throw new «RuntimeException.name»(
								«String.name».format("Cannot add node %s to %s", «containableNode.fqBeanName».class, this.getClass()));
						'''

						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1,
							1,
							newNodeSimpleMethodContent,
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1)
						)
						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1,
							1,
							newNodeSimpleMethodContent,
							createEStringParameter("id",1,1),
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1)
						)
						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1,
							1,
							newNodeMethodContent,
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1),
							createEIntParameter("width", 1, 1),
							createEIntParameter("height", 1, 1)
						)
						it.mainEClass.createEOperation(
							'''new«containableNode.fuName»''',
							elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, containableNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node"),
							1,
							1,
							newNodeMethodContent,
							createEStringParameter("id",1,1),
							createEIntParameter("x", 1, 1),
							createEIntParameter("y", 1, 1),
							createEIntParameter("width", 1, 1),
							createEIntParameter("height", 1, 1)
						)
					}
				}
			]
		]
	}

	/**
	 * Populates every {@link EClass EClasses} representing {@link GraphModel GraphModels} of the provided
	 * <code>elementEClasses</code> with the following properties:
	 *
	 * <ul>
	 * 	<li><code>new</code> methods for the {@link GraphModel GraphModels} themselves</li>
	 * </ul>
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} that should be populated
	 */
	private def populateGraphModelEClasses(Set<ElementEClasses> elementEClasses) {
		elementEClasses.filter[it.modelElement instanceof GraphModel].forEach[
			it.mainEClass.createEOperation(
				'''new«it.modelElement.fuName»''',
				it.mainEClass, 1, 1,
				'''
					«IPath.name» filePath = new «Path.name»(path).append(fileName).addFileExtension("«it.modelElement.name.toLowerCase»");
					«URI.name» uri = «URI.name».createPlatformResourceURI(filePath.toOSString(), true);
					«IFile.name» file = «ResourcesPlugin.name».getWorkspace().getRoot().getFile(filePath);
					«Resource.name» res = new «ResourceSetImpl.name»().createResource(uri);
					«it.modelElement.fqBeanName» graph = «it.modelElement.fqFactoryName».eINSTANCE.create«it.modelElement.fuName»();

					«EcoreUtil.name».setID(graph, «EcoreUtil.name».generateUUID());

					res.getContents().add(graph.getInternalElement_());

					«IF it.modelElement.hasPostCreateHook»
						if (postCreateHook)
							«it.modelElement.fqFactoryName».eINSTANCE.postCreates((«it.modelElement.fqBeanName») graph);
					«ENDIF»
					try {
						res.save(null);
					} catch («IOException.name» e) {
						e.printStackTrace();
					}

					return graph;
				''',
				createEStringParameter("path", 1, 1),
				createEStringParameter("fileName", 1, 1),
				createEBooleanParameter("postCreateHook", 1, 1)
			)
		]
	}

	/**
	 * Populates every {@link EClass EClasses} representing {@link Node Nodes} of the provided
	 * <code>elementEClasses</code> with the following properties:
	 *
	 * <ul>
	 * 	<li><code>getOutgoing</code> methods</li>
	 *  <li><code>getIncoming</code> methods</li>
	 *  <li><code>s_moveTo</code> methods</li>
	 *  <li><code>postMove</code> methods</li>
	 *  <li><code>resize</code> methods</li>
	 *  <li><code>getOutgoing*NodeName*s</code> methods</li>
	 *  <li><code>getIncoming*NodeName*s</code> methods</li>
	 *  <li><code>getIncoming*NodeName*s</code> methods</li>
	 *  <li><code>getIncomingConstraints</code> methods</li>
	 *  <li><code>getOutgoingConstraints</code> methods</li>
	 *  <li><code>getIncoming*NodeName*s</code> methods</li>
	 *  <li><code>getPredecessors</code> methods</li>
	 *  <li><code>getSuccessors</code> methods</li>
	 * 	<li><code>get*NodeName*Predecessors</code> methods</li>
	 *  <li><code>get*NodeName*Successors</code> methods</li>
	 * 	<li><code>canNew*ConnectingEdge*</code> methods</li>
	 *  <li><code>new*ConnectingEdge*</code> methods</li>
	 * 	<li><code>canMoveTo</code> methods</li>
	 * 	<li><code>moveTo</code> methods</li>
	 * 	<li><code>s_moveTo</code> methods</li>
	 * 	<li><code>getContainer</code> methods</li>
	 * 	<li><code>get*NodeName*s</code> methods for prime-references</li>
	 * 	<li><code>getLibraryComponentUID</code> methods</li>
	 * 	<li><code>setLibraryComponentUID</code> methods</li>
	 * </ul>
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} that should be populated
	 */
	private def populateNodeEClasses(Set<ElementEClasses> elementEClasses) {
		elementEClasses.filter[it.modelElement instanceof Node].forEach[
			val outgoingEdges = MGLUtil.removeDuplicateModelElements((#[it.modelElement as Node] + (it.modelElement as Node).allNodeSubTypes).flatMap[outgoingEdgeConnections.flatMap[co| co.connectingEdges].toSet.allEdgesSuperTypesAndSubTypes]).filter(Edge)
			val incomingEdges = MGLUtil.removeDuplicateModelElements((#[it.modelElement as Node] + (it.modelElement as Node).allNodeSubTypes).flatMap[incomingEdgeConnections.flatMap[co| co.connectingEdges].toSet.allEdgesSuperTypesAndSubTypes]).filter(Edge)
			val mostCommonOutgoing = outgoingEdges.lowestMutualSuperEdge
			val mostCommonIncoming = incomingEdges.lowestMutualSuperEdge
			val mostCommonOutgoingEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, mostCommonOutgoing)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Edge")
			val mostCommonIncomingEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, mostCommonIncoming)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Edge")
			val allContainingElements = (it.modelElement as Node).allContainingElements
			val possibleContainers = (it.modelElement as Node).possibleContainers
			val possiblePredecessors = (it.modelElement as Node).possiblePredecessors.toSet
			val possibleSuccessors = (it.modelElement as Node).possibleSuccessors.toSet
			val lowestMutualPredecessorNode = (it.modelElement as Node).possiblePredecessors.lowestMutualSuperNode
			val lowestMutualSuccessorNode = (it.modelElement as Node).possibleSuccessors.lowestMutualSuperNode
			val lowestMutualPredecessorNodeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, lowestMutualPredecessorNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")
			val lowestMutualSuccessorNodeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, lowestMutualSuccessorNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")
			val outgoingConnectingEdges = (it.modelElement as Node).getOutgoingConnectingEdges(false).map[subTypes  as Iterable<Edge> + #[it]].flatten.filter[!isIsAbstract].toSet
			
			if(!outgoingEdges.nullOrEmpty) {
				it.mainEClass.createGenericListEOperation(
					"getOutgoing", mostCommonOutgoingEClass, 0, -1,
					'''
						EList<graphmodel.internal.InternalEdge> out = ((graphmodel.internal.Internal«IF it.modelElement instanceof NodeContainer»Container«ELSE»Node«ENDIF»)getInternalElement_()).getOutgoing();
						return org.eclipse.emf.common.util.ECollections.unmodifiableEList(out
							.stream().map(me -> («mostCommonOutgoing?.fqBeanName ?: "graphmodel.Edge"»)me.getElement()).
								collect(java.util.stream.Collectors.toList()));
					'''
				)
				it.mainEClass.createGenericListEOperation(
					"getSuccessors",
					lowestMutualSuccessorNodeEClass,
					'''return ((graphmodel.Node)this).getSuccessors(«lowestMutualSuccessorNode?.fqBeanName ?: "graphmodel.Node"».class);'''
				)
			}
			if(!incomingEdges.nullOrEmpty) {
				it.mainEClass.createGenericListEOperation(
					"getIncoming", mostCommonIncomingEClass, 0, -1,
					'''
						EList<graphmodel.internal.InternalEdge> in = ((graphmodel.internal.Internal«IF it.modelElement instanceof NodeContainer»Container«ELSE»Node«ENDIF»)getInternalElement_()).getIncoming();
						return org.eclipse.emf.common.util.ECollections.unmodifiableEList(in
							.stream().map(me -> («mostCommonIncoming?.fqBeanName ?: "graphmodel.Edge"»)me.getElement()).
								collect(java.util.stream.Collectors.toList()));
					'''
				)
				it.mainEClass.createGenericListEOperation(
					"getPredecessors",
					lowestMutualPredecessorNodeEClass,
					'''return ((graphmodel.Node)this).getPredecessors(«lowestMutualPredecessorNode?.fqBeanName ?: "graphmodel.Node"».class);'''
				)
			}

			/*
			 * This method is only generated to allow for the implementation of the api. Thus, to add additional behavior for move
			 * the method "s_moveTo(...)" should be overridden.
			 *
			 * ATTENTION: Overriding the "moveTo(...)" overrides the post move hook call!
			 */
			it.mainEClass.createEOperation(
				"s_moveTo",
				null, 1, 1,
				"",
				GraphmodelPackage.Literals.MODEL_ELEMENT_CONTAINER.createEParameter("container", 1, 1),
				createEIntParameter("x", 1, 1),
				createEIntParameter("y", 1, 1)
			)
			
			it.mainEClass.createEOperation(
				"preMove",
				null, 1, 1,
				it.modelElement.writeMethodCallPreMove("this", "newContainer", "newX", "newY"),
				GraphmodelPackage.eINSTANCE.modelElementContainer.createEParameter("newContainer", 1, 1),
				createEIntParameter("newX", 1, 1),
				createEIntParameter("newY", 1, 1)
			)
			
			it.mainEClass.createEOperation(
				"postMove",
				null, 1, 1,
				it.modelElement.writeMethodCallPostMove("this", "source", "target", "x", "y", "deltaX", "deltaY"),
				GraphmodelPackage.eINSTANCE.modelElementContainer.createEParameter("source", 1, 1),
				GraphmodelPackage.eINSTANCE.modelElementContainer.createEParameter("target", 1, 1),
				createEIntParameter("x", 1, 1),
				createEIntParameter("y", 1, 1),
				createEIntParameter("deltaX", 1, 1),
				createEIntParameter("deltaY", 1, 1)
			)
			
			it.mainEClass.createEOperation(
				"preResize",
				null, 1, 1,
				modelElement.writeMethodCallPreResize('this', 'newWidth', 'newHeight', 'newX', 'newY', 'direction'),
				createEIntParameter("newWidth", 1, 1),
				createEIntParameter("newHeight", 1, 1),
				createEIntParameter("newX", 1, 1),
				createEIntParameter("newY", 1, 1),
				GraphmodelPackage.eINSTANCE.direction.createEParameter("direction", 1, 1)
			)

			it.mainEClass.createEOperation(
				"postResize",
				null, 1, 1,
				modelElement.writeMethodCallPostResize('this', 'oldWidth', 'oldHeight', 'oldX', 'oldY', 'direction'),
				createEIntParameter("oldWidth", 1, 1),
				createEIntParameter("oldHeight", 1, 1),
				createEIntParameter("oldX", 1, 1),
				createEIntParameter("oldY", 1, 1),
				GraphmodelPackage.eINSTANCE.direction.createEParameter("direction", 1, 1)
			)

			outgoingEdges.forEach[edge |
				it.mainEClass.createEOperation(
					'''getOutgoing«edge.name»s''',
					elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, edge)]?.mainEClass, 0, -1,
					'''return this.getOutgoing(«edge.fqBeanName».class);'''
				)
			]
			incomingEdges.forEach[edge |
				it.mainEClass.createEOperation(
					'''getIncoming«edge.name»s''',
					elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, edge)]?.mainEClass, 0, -1,
					'''return this.getIncoming(«edge.fqBeanName».class);'''
				)
			]

			if((it.modelElement as Node).incomingEdgeConnections.size() > 0) {
				it.internalEClass.createEOperation(
					"getIncomingConstraints",
					GraphmodelPackage.eINSTANCE.getEClassifier("ConnectionConstraint"),
					0, -1,
					'''
						 «FOR pair : (it.modelElement as Node).incomingEdgeConnections.indexed»
						 	ConnectionConstraint cons«pair.key» = new ConnectionConstraint(false, «pair.value.lowerBound», «pair.value.upperBound», «pair.value.connectingEdges.edgesList»);
						 «ENDFOR»
						 org.eclipse.emf.common.util.BasicEList<ConnectionConstraint>eList = new org.eclipse.emf.common.util.BasicEList<ConnectionConstraint>();
						 eList.addAll(com.google.common.collect.Lists.newArrayList(«(it.modelElement as Node).incomingEdgeConnections.constraintVariables»));
						 eList.addAll(super.getIncomingConstraints());
						 return eList;
					'''
				)
			}
			if((it.modelElement as Node).outgoingEdgeConnections.size() > 0) {
				it.internalEClass.createEOperation(
					"getOutgoingConstraints",
					GraphmodelPackage.eINSTANCE.getEClassifier("ConnectionConstraint"),
					0, -1,
					'''
						 «FOR pair : (it.modelElement as Node).outgoingEdgeConnections.indexed»	 
						 	ConnectionConstraint cons«pair.key» = new ConnectionConstraint(true, «pair.value.lowerBound», «pair.value.upperBound», «pair.value.connectingEdges.edgesList»);
						 «ENDFOR»
						 org.eclipse.emf.common.util.BasicEList<ConnectionConstraint>eList = new org.eclipse.emf.common.util.BasicEList<ConnectionConstraint>();
						 eList.addAll(com.google.common.collect.Lists.newArrayList(«(it.modelElement as Node).outgoingEdgeConnections.constraintVariables»));
						 eList.addAll(super.getOutgoingConstraints());
						 return eList;
					'''
				)
			}
			
			possiblePredecessors.forEach[predecessorNode |
				val methodName = '''get«predecessorNode.name.toFirstUpper»Predecessors'''
				val predecessorNodeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, predecessorNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")

				it.mainEClass.createEOperation(
					methodName,
					predecessorNodeEClass,
					0, -1,
					'''return ((graphmodel.Node)this).getPredecessors(«predecessorNode.fqBeanName».class);'''
				)
				it.internalEClass.createEOperation(
					methodName,
					predecessorNodeEClass,
					0, -1,
					'''return ((graphmodel.Node)this.getElement()).getPredecessors(«predecessorNode.fqBeanName».class);'''
				)
			]
			possibleSuccessors.forEach[successorNode |
				val methodName = '''get«successorNode.name.toFirstUpper»Successors'''
				val successorNodeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, successorNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")

				it.mainEClass.createEOperation(
					methodName,
					successorNodeEClass,
					0, -1,
					'''return ((graphmodel.Node)this).getSuccessors(«successorNode.fqBeanName».class);'''
				)
				it.internalEClass.createEOperation(
					methodName,
					successorNodeEClass,
					0, -1,
					'''return ((graphmodel.Node)this.getElement()).getSuccessors(«successorNode.fqBeanName».class);'''
				)
			]

			outgoingConnectingEdges.forEach[outgoingConnectingEdge |
				val canNewMethodName = '''canNew«outgoingConnectingEdge.fuName»'''
				val newMethodName = '''new«outgoingConnectingEdge.fuName»'''
				val outgoingConnectingEdgeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, outgoingConnectingEdge)]?.mainEClass

				outgoingConnectingEdge.allPossibleTargets.forEach[outgoingConnectingEdgeTarget |
					val targetEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, outgoingConnectingEdgeTarget)]?.mainEClass

					it.mainEClass.createEOperation(
						canNewMethodName,
						EcorePackage.eINSTANCE.EBoolean, 0, 1,
						'''return this.canStart(«outgoingConnectingEdge.fqBeanName».class) && target.canEnd(«outgoingConnectingEdge.fqBeanName».class);''',
						targetEClass.createEParameter("target", 1, 1)
					)
					it.mainEClass.createEOperation(
						newMethodName,
						outgoingConnectingEdgeEClass,
						0, 1,
						'''
							if (!this.canStart(«outgoingConnectingEdge.fqBeanName».class))
								throw new «RuntimeException.name»(«String.name».format("Cannot start edge %s at node %s", «outgoingConnectingEdge.fqBeanName».class, this.getClass()));
							else if (!target.canEnd(«outgoingConnectingEdge.fqBeanName».class))
								throw new «RuntimeException.name»(«String.name».format("Cannot end edge %s at node %s", «outgoingConnectingEdge.fqBeanName».class, target.getClass()));
							else {
								«outgoingConnectingEdge.fqBeanName» edge = «outgoingConnectingEdge.fqFactoryName».eINSTANCE.create«outgoingConnectingEdge.fuName»((«InternalNode.name») this.getInternalElement_(), («InternalNode.name») target.getInternalElement_());
								edge.setSourceElement(this);
								edge.setTargetElement(target);
								target.getRootElement().getModelElements().add(edge);
								return edge;
							}
						''',
						targetEClass.createEParameter("target", 1, 1)
					)
					it.mainEClass.createEOperation(
						newMethodName,
						outgoingConnectingEdgeEClass,
						0, 1,
						'''
							if (!this.canStart(«outgoingConnectingEdge.fqBeanName».class))
								throw new «RuntimeException.name»(«String.name».format("Cannot start edge %s at node %s", «outgoingConnectingEdge.fuName».class, this.getClass()));
							else if (!target.canEnd(«outgoingConnectingEdge.fqBeanName».class))
								throw new «RuntimeException.name»(«String.name».format("Cannot end edge %s at node %s", «outgoingConnectingEdge.fuName».class, target.getClass()));
							else {
								«outgoingConnectingEdge.fqBeanName» edge = «outgoingConnectingEdge.fqFactoryName».eINSTANCE.create«outgoingConnectingEdge.fuName»(id, («InternalNode.name») this.getInternalElement_(), («InternalNode.name») target.getInternalElement_());
								edge.setSourceElement(this);
								edge.setTargetElement(target);
								return edge;
							}
						''',
						targetEClass.createEParameter("target", 1, 1),
						createEStringParameter("id",1,1)
					)
				]
			]

			possibleContainers.forEach[possibleContainer |
				val possibleContainerEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, possibleContainer as ModelElement)]?.mainEClass

				it.mainEClass.createEOperation(
					"canMoveTo",
					EcorePackage.eINSTANCE.EBoolean,
					1, 1,
					'''return target.canContain(«it.modelElement.fqBeanName».class);''',
					elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, possibleContainer as ModelElement)]?.mainEClass.createEParameter("target", 1, 1),
					createEIntParameter("x", 1, 1),
					createEIntParameter("y", 1, 1)
				)
				it.mainEClass.createEOperation(
					"moveTo",
					null,
					1, 1,
					'''
						transact("Move to", () -> {
							«ModelElementContainer.name» sourceContainer = this.getContainer();
							«InternalNode.name» ime = («InternalNode.name») this.getInternalElement_();
							int deltaX = x - ime.getX();
							int deltaY = y - ime.getY();
							«IF it.modelElement.booleanWriteMethodCallPreMove»
								preMove(target, x, y);
							«ENDIF»
							s_moveTo(target, x, y);
							target.getInternalContainerElement().getModelElements().add(ime);
							ime.setX(x);
							ime.setY(y);
							«IF it.modelElement.booleanWriteMethodCallPostMove»
								postMove(sourceContainer, target, x, y, deltaX, deltaY);
							«ENDIF»
						});
					''',
					possibleContainerEClass.createEParameter("target", 1, 1),
					createEIntParameter("x", 1, 1),
					createEIntParameter("y", 1, 1)
				)
				/**
				 * This method is only generated to allow for the implementation of the api. Thus, to add additional behavior for move
				 * the method "s_moveTo(...)" should be overridden.
				 *
				 * ATTENTION: Overriding the "moveTo(...)" overrides the post move hook call!
				 */
				it.mainEClass.createEOperation(
					"s_moveTo",
					null, 1, 1,
					"",
					possibleContainerEClass.createEParameter(possibleContainer.name.toFirstLower,1,1),
					createEIntParameter("x", 1, 1),
					createEIntParameter("y", 1, 1)
				)
			]

			/*
			 * TODO Check why this checks whether no GraphModel can contain this node.
			 * What happens if the node can be contained both, directly in a container and a graphmodel?
			 * This check has been transfered from the old generator
			 */
			if(!allContainingElements.nullOrEmpty && allContainingElements.filter(GraphModel).size == 0){
				val lowestMutualSuperNode  = allContainingElements.filter(Node).lowestMutualSuperNode
				if(lowestMutualSuperNode !== null){
					it.mainEClass.createEOperation(
						"getContainer",
						elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, lowestMutualSuperNode)]?.mainEClass,
						0, 1,
						'''
							return («lowestMutualSuperNode.fqBeanName»)getInternalElement_().getContainer().getContainerElement();
						'''
					)
				}
			} else if(allContainingElements.size >= 1){
				if (allContainingElements.forall[c | c instanceof GraphModel]) {
					val lowestMutualSuperGraphModel  = allContainingElements.filter(GraphModel).lowestMutualSuperGraphModel
					it.mainEClass.createEOperation(
						"getContainer",
						elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, lowestMutualSuperGraphModel)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("GraphModel"),
						0, 1,
						'''
							return («IF lowestMutualSuperGraphModel !== null»«lowestMutualSuperGraphModel.fqBeanName»«ELSE»GraphModel«ENDIF»)getInternalElement_().getContainer().getContainerElement();
						'''
					)
				}
			}

			if((it.modelElement as Node).isPrime(false)) {
				var EClassifier primeEClass = switch((it.modelElement as Node).retrievePrimeReference) {
					ReferencedEClass: EcorePackage.eINSTANCE.EObject
					ReferencedModelElement: elementEClasses.findFirst[elementEClass | MGLUtil.equalModelElement(elementEClass.modelElement, ((it.modelElement as Node).retrievePrimeReference as ReferencedModelElement).type)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")
				}

				it.mainEClass.createEOperation(
					'''get«(it.modelElement as Node).primeName.toFirstUpper»''',
					primeEClass,
					0, 1,
					'''
						String uid = ((«(it.modelElement as Node).fqInternalBeanName»)getInternalElement_()).getLibraryComponentUID();
						return «IF (it.modelElement as Node).retrievePrimeReference instanceof ReferencedModelElement»(«(it.modelElement as Node).primeFqTypeName»)«ENDIF» «ReferenceRegistry.name».getInstance().getEObject(uid);
					'''
				)
				it.internalEClass.createEOperation(
					'''get«(it.modelElement as Node).primeName.toFirstUpper»''',
					primeEClass,
					0, 1,
					'''
						String uid = getLibraryComponentUID();
						return «IF (it.modelElement as Node).retrievePrimeReference instanceof ReferencedModelElement»(«(it.modelElement as Node).primeFqTypeName»)«ENDIF» «ReferenceRegistry.name».getInstance().getEObject(uid);
					'''
				)

				if(!(it.modelElement as Node).allSuperTypes.exists[it instanceof Node && (it as Node).isPrime]) {
					it.internalEClass.createEAttribute("libraryComponentUID", EcorePackage.eINSTANCE.EString, 0, 1)
					it.mainEClass.createEOperation(
						"getLibraryComponentUID",
						EcorePackage.eINSTANCE.EString,
						1, 1,
						'''return ((«(it.modelElement as Node).fqInternalBeanName») this.getInternalElement_()).getLibraryComponentUID();'''
					)
					it.mainEClass.createEOperation(
						"setLibraryComponentUID",
						null, 1, 1,
						'''((«(it.modelElement as Node).fqInternalBeanName») this.getInternalElement_()).setLibraryComponentUID(id);''',
						createEStringParameter("id",1,1))
				}
			}
		]
	}

	/**
	 * Populates every {@link EClass EClasses} representing {@link Edge Edges} of the provided
	 * <code>elementEClasses</code> with the following properties:
	 *
	 * <ul>
	 * 	<li><code>getSourceElement</code> methods</li>
	 *  <li><code>getTargetElement</code> methods</li>
	 *  <li><code>canReconnectSource</code> methods</li>
	 *  <li><code>reconnectSource</code> methods</li>
	 *  <li><code>canReconnectTarget</code> methods</li>
	 *  <li><code>reconnectTarget</code> methods</li>
	 * </ul>
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} that should be populated
	 */
	private def populateEdgeEClasses(Set<ElementEClasses> elementEClasses) {
		elementEClasses.filter[it.modelElement instanceof Edge].forEach[
			val lowestMutualSourceNode = ((it.modelElement as Edge).subTypes.map[it as Edge] + (it.modelElement as Edge).iterable).map[allPossibleSources].cleanupConnectingNodes.lowestMutualSuperNode
			val lowestMutualTargetNode = ((it.modelElement as Edge).subTypes.map[it as Edge] + (it.modelElement as Edge).iterable).map[allPossibleTargets].cleanupConnectingNodes.lowestMutualSuperNode
			val lowestMutualSourceNodeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, lowestMutualSourceNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")
			val lowestMutualTargetNodeEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, lowestMutualTargetNode)]?.mainEClass ?: GraphmodelPackage.eINSTANCE.getEClassifier("Node")
			val possibleSources = (it.modelElement as Edge).possibleSources
			val possibleTargets = (it.modelElement as Edge).possibleTargets

			it.mainEClass.createEOperation(
				"getSourceElement",
				lowestMutualSourceNodeEClass,
				0, 1,
				'''return(«IF lowestMutualSourceNode !== null»«lowestMutualSourceNode.fqBeanName»«ELSE»graphmodel.Node«ENDIF»)super.getSourceElement();'''
			)
			it.mainEClass.createEOperation(
				"getTargetElement",
				lowestMutualTargetNodeEClass,
				0, 1,
				'''return(«IF lowestMutualTargetNode !== null»«lowestMutualTargetNode.fqBeanName»«ELSE»graphmodel.Node«ENDIF»)super.getTargetElement();'''
			)

			possibleSources.forEach[possibleSource |
				val sourceEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, possibleSource)]?.mainEClass

				it.mainEClass.createEOperation(
					"canReconnectSource",
					EcorePackage.eINSTANCE.EBoolean,
					1, 1,
					'''return source.canStart(this.getClass());''',
					createEParameter(sourceEClass, "source", 1, 1)
				)

				it.mainEClass.createEOperation(
					"reconnectSource",
					null, 1, 1,
					'''this.setSourceElement(source);''',
					createEParameter(sourceEClass,"source",1,1))
			]

			possibleTargets.forEach[possibleTarget |
				val targetEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, possibleTarget)]?.mainEClass

				it.mainEClass.createEOperation(
					"canReconnectTarget",
					EcorePackage.eINSTANCE.EBoolean,
					1, 1,
					'''return target.canEnd(this.getClass());''',
					createEParameter(targetEClass, "target", 1, 1)
				)

				it.mainEClass.createEOperation(
					"reconnectTarget",
					null, 1, 1,
					'''this.setTargetElement(target);''',
					createEParameter(targetEClass,"target",1,1)
				)
			]
		]
	}
	
	def cleanupConnectingNodes(Iterable<Set<Node>> sets){
		
		sets.flattenUnless([d|d.nullOrEmpty])
	}

	/**
	 * Sets the super types of the {@link EClass EClasses} of the provided <code>elementEClasses</code>.
	 *
	 * If the contained {@link ModelElement} extends another {@link ModelElement} the {@link EClass} representation of this parent {@link ModelElement}
	 * will be used as its super type.
	 * Otherwise the MGL type ({@link UserDefinedType}, {@link Node}, {@link Edge}, {@link GraphModel}) will be set as the parent type.
	 *
	 * The super type of the internal classes are also set with this method.
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} for which the super types should be set
	 */
	private def void setSuperTypes(Set<ElementEClasses> elementEClasses) {
		elementEClasses.forEach[
			var ModelElement parentModelElement
			switch(it) {
				case it.modelElement instanceof UserDefinedType:
					parentModelElement = it.modelElement.extend
				case it.modelElement instanceof Node:
					parentModelElement = it.modelElement.extend
				case it.modelElement instanceof Edge:
					parentModelElement = it.modelElement.extend
				case it.modelElement instanceof GraphModel:
					parentModelElement = it.modelElement.extend
			}
			if(parentModelElement !== null) {
				val finalParentModelElement = parentModelElement
				val parentElementEClass = elementEClasses.findFirst[MGLUtil.equalModelElement(modelElement, finalParentModelElement)]
				it.mainEClass.ESuperTypes += parentElementEClass.mainEClass
				it.internalEClass.ESuperTypes += parentElementEClass.internalEClass
				it.mainView.ESuperTypes += parentElementEClass.mainView
				
				//Also add the Container super type to containers that extend node to ensure proper behaviour
				if(it.modelElement instanceof ContainingElement && !(it.modelElement.extend instanceof ContainingElement)) {
					it.mainEClass.ESuperTypes += GraphmodelPackage.eINSTANCE.getEClassifier("Container") as EClass
					it.internalEClass.ESuperTypes += InternalPackage.eINSTANCE.getEClassifier("InternalContainer") as EClass
				}
			} else {
				switch(it) {
					case it.modelElement instanceof GraphModel: {
							it.mainEClass.ESuperTypes += GraphmodelPackage.eINSTANCE.getEClassifier("GraphModel") as EClass
							it.internalEClass.ESuperTypes += InternalPackage.eINSTANCE.getEClassifier("InternalGraphModel") as EClass
						}
					case it.modelElement instanceof Node:
						if(it.modelElement instanceof ContainingElement) {
							it.mainEClass.ESuperTypes += GraphmodelPackage.eINSTANCE.getEClassifier("Container") as EClass
							it.internalEClass.ESuperTypes += InternalPackage.eINSTANCE.getEClassifier("InternalContainer") as EClass
						} else {
							it.mainEClass.ESuperTypes += GraphmodelPackage.eINSTANCE.getEClassifier("Node") as EClass
							it.internalEClass.ESuperTypes += InternalPackage.eINSTANCE.getEClassifier("InternalNode") as EClass
						}
					case it.modelElement instanceof Edge: {
						it.mainEClass.ESuperTypes += GraphmodelPackage.eINSTANCE.getEClassifier("Edge") as EClass
						it.internalEClass.ESuperTypes += InternalPackage.eINSTANCE.getEClassifier("InternalEdge") as EClass
					}
					case it.modelElement instanceof UserDefinedType: {
						it.mainEClass.ESuperTypes += GraphmodelPackage.eINSTANCE.type
						it.internalEClass.ESuperTypes += InternalPackage.eINSTANCE.internalType
					}
				}
			}
		]
	}

	/**
	 * Creates references to the main view and populates this view with getter, setter, adder, and remove methods for every attribute.
	 * Also creates a <code>get*modelElementName*View</code> method.
	 *
	 * @param	elementEClasses	a {@link Set} that contains all {@link ElementEClasses} for which references of the main view should be created and
	 * 							which's main view should be populated with the above mentioned methods
	 */
	private def void referenceAndPopulateMainView(Set<ElementEClasses> elementEClasses) {
		elementEClasses.forEach[
			if (!it.mainView.isAbstract) {
				it.mainEClass.createEOperation(
					'''get«it.modelElement.name»View''',
					it.mainView,
					0, 1,
					'''
						«it.mainView.name» «it.mainView.name.toFirstLower» = «(it.modelElement.eContainer as MGLModel).package».«(it.modelElement.eContainer as MGLModel).ePackageName».views.ViewsFactory.eINSTANCE.create«it.mainView.name»();
						«it.mainView.name.toFirstLower».setInternal«it.mainEClass.name»((«(it.modelElement.eContainer as MGLModel).package».«(it.modelElement.eContainer as MGLModel).ePackageName».internal.Internal«it.mainEClass.name»)getInternalElement_());
						return «it.mainView.name.toFirstLower»;
					'''
				)
			}

			it.mainView.createReference(
				it.internalEClass.name.toFirstLower,
				it.internalEClass,
				0, 1,
				false,
				null
			)

			it.modelElement.allAttributes.forEach [attribute |
				if (!(attribute instanceof ComplexAttribute && (attribute as ComplexAttribute).override)) {
					val feature = it.internalEClass.EStructuralFeatures.findFirst[name == attribute.name]
					if (feature !== null) {
						it.mainView.createGetter(it, attribute)
						it.mainView.createSetter(it, attribute)
						if(attribute.upperBound != 1 ){
							it.mainView.createAdder(it, attribute)
							it.mainView.createRemove(it, attribute)
						}
					}
				}
			]
		]
	}

	/**
	 * Utility method to create getter methods for the provided <code>eClass</code>.
	 *
	 * @param	eClass			the {@link EClass} which gets the newly created getter method
	 * @param	elementEClasses	the {@link ElementEClasses} which holds the {@link ModelElement} and the associated {@link EClass EClasses} for which
	 * 							the getter method should be created
	 * @param	attr			the {@link Attribute} which should be accessible with the new getter method
	 */
	private def EOperation createGetter(EClass eClass, ElementEClasses elementEClasses, Attribute attr) {
		var EClassifier returnType
		if(attr instanceof PrimitiveAttribute) {
			returnType = attr.type.EDataType
		} else if((attr as ComplexAttribute).type instanceof Enumeration) {
			returnType = eEnumMap.get((attr as ComplexAttribute).type)
		} else {
			returnType = allElementEClasses.findFirst[MGLUtil.equalModelElement(it.modelElement, (attr as ComplexAttribute).type)].mainEClass
		}
		eClass.createEOperation(
			'''«IF (attr instanceof PrimitiveAttribute && (attr as PrimitiveAttribute).type == EDataTypeType.EBOOLEAN)»is«ELSE»get«ENDIF»«attr.name.toFirstUpper»''',
			returnType,
			attr.lowerBound,
			attr.upperBound,
			'''return get«elementEClasses.internalEClass.name»().«IF (attr instanceof PrimitiveAttribute && (attr as PrimitiveAttribute).type == EDataTypeType.EBOOLEAN)»is«ELSE»get«ENDIF»«attr.name.toFirstUpper»();'''
		)
	}

	/**
	 * Utility method to create setter methods for the provided <code>eClass</code>.
	 *
	 * @param	eClass			the {@link EClass} which gets the newly created setter method
	 * @param	elementEClasses	the {@link ElementEClasses} which holds the {@link ModelElement} and the associated {@link EClass EClasses} for which
	 * 							the setter method should be created
	 * @param	attr			the {@link Attribute} which should be set-able with the new setter method
	 */
	private def EOperation createSetter(EClass modelElementClass, ElementEClasses elementEClasses, Attribute attr) {
		val parameter = EcoreFactory.eINSTANCE.createEParameter
		parameter.name = "_arg"
		parameter.lowerBound = 0
		parameter.upperBound = attr.upperBound
		if(attr instanceof PrimitiveAttribute) {
			parameter.EType = attr.type.EDataType
		} else if((attr as ComplexAttribute).type instanceof Enumeration) {
			parameter.EType = eEnumMap.get((attr as ComplexAttribute).type)
		} else {
			parameter.EType = allElementEClasses.findFirst[MGLUtil.equalModelElement(it.modelElement, (attr as ComplexAttribute).type)].mainEClass
		}

		modelElementClass.createEOperation(
			'''set«attr.name.toFirstUpper»''',
			null, 0, 1,
			'''
				get«elementEClasses.internalEClass.name»().getElement().transact("Set «attr.name.toFirstUpper»", () -> {
					«IF attr.upperBound == 1»
						get«elementEClasses.internalEClass.name»().set«attr.name.toFirstUpper»(_arg);
					«ELSE»
						get«elementEClasses.internalEClass.name»().get«attr.name.toFirstUpper»().clear();
						get«elementEClasses.internalEClass.name»().get«attr.name.toFirstUpper»().addAll(_arg);
					«ENDIF»
				});
			''',
			parameter
		)
	}

	/**
	 * Utility method to create adder methods for the provided <code>eClass</code>.
	 *
	 * @param	eClass			the {@link EClass} which gets the newly created adder method
	 * @param	elementEClasses	the {@link ElementEClasses} which holds the {@link ModelElement} and the associated {@link EClass EClasses} for which
	 * 							the adder method should be created
	 * @param	attr			the {@link Attribute} which should be add-ible with the new adder method
	 */
	private def EOperation createAdder(EClass modelElementClass, ElementEClasses elementEClasses, Attribute attr) {
		val parameter = EcoreFactory.eINSTANCE.createEParameter
		parameter.name = "_arg"
		parameter.lowerBound = 0
		parameter.upperBound = 1
		if(attr instanceof PrimitiveAttribute) {
			parameter.EType = attr.type.EDataType
		} else if((attr as ComplexAttribute).type instanceof Enumeration) {
			parameter.EType = eEnumMap.get((attr as ComplexAttribute).type)
		} else {
			parameter.EType = allElementEClasses.findFirst[MGLUtil.equalModelElement(it.modelElement, (attr as ComplexAttribute).type)].mainEClass
		}

		modelElementClass.createEOperation(
			'''add«attr.name.toFirstUpper»''', null, 0, 1,
			'''
				get«elementEClasses.internalEClass.name»().getElement().transact("Set «attr.name.toFirstUpper»", () -> {
					get«elementEClasses.internalEClass.name»().get«attr.name.toFirstUpper»().add(_arg);
				});
			''',
			parameter
		)
	}

	/**
	 * Utility method to create remove methods for the provided <code>eClass</code>.
	 *
	 * @param	eClass			the {@link EClass} which gets the newly created remove method
	 * @param	elementEClasses	the {@link ElementEClasses} which holds the {@link ModelElement} and the associated {@link EClass EClasses} for which
	 * 							the remove method should be created
	 * @param	attr			the {@link Attribute} which should be removable with the new remove method
	 */
	private def EOperation createRemove(EClass modelElementClass, ElementEClasses elementEClasses, Attribute attr){
		val parameter = EcoreFactory.eINSTANCE.createEParameter
		parameter.name = attr.name.toFirstLower
		parameter.upperBound = 1
		parameter.lowerBound = 0
		if(attr instanceof PrimitiveAttribute) {
			parameter.EType = attr.type.EDataType
		} else if((attr as ComplexAttribute).type instanceof Enumeration) {
			parameter.EType = eEnumMap.get((attr as ComplexAttribute).type)
		} else {
			parameter.EType = allElementEClasses.findFirst[MGLUtil.equalModelElement(it.modelElement, (attr as ComplexAttribute).type)].mainEClass
		}

		modelElementClass.createEOperation(
		'''remove«attr.name.toFirstUpper»''', null, 0, 1,
		'''
			get«elementEClasses.internalEClass.name»().getElement().transact("Set «attr.name.toFirstUpper»", () -> {
				get«elementEClasses.internalEClass.name»().get«attr.name.toFirstUpper»().remove(«attr.name»);
			});
		''',
		parameter)
	}

	/**
	 * Utility method to retrieve an actual {@link EDataType} of the provided <code>type</code>.
	 *
	 * @param	type	the {@link EDataTypeType} for which the actual {@link EDataType} should be returned
	 */
	private def EDataType getEDataType(EDataTypeType type) {
		EcorePackage.eINSTANCE.getEClassifier(type.literal) as EDataType
	}

	/**
	 * Returns all containing elements of the provided <code>element</code>.
	 * This includes all super and sub types of all containing elements of the <code>element</code>.
	 *
	 * @param	element	the {@link GraphicalModelElement} for which all containing elements should be retrieved
	 * @return	an {@link Iterable} that contains all {@link ContainingElement ContainingElements} the provided <code>element</code> can be contained in.
	 * 			This includes every sub and super types of the possible containers
	 */
	private def Iterable<ContainingElement> allContainingElements(GraphicalModelElement element){
		if(element !== null){
			val containingElements = new HashSet<ContainingElement>
			var graphModels = allMGLs.flatMap[graphModels]
			for (gm : graphModels) {
				val containable = gm.allContainmentConstraints
				if (containable.exists[(types.exists[equalModelElement(element)] || types.empty) && upperBound !== 0] || gm.containableElements.empty) {
					containingElements += gm
				}
			}

			containingElements += element.containingElements
			containingElements += element.allSuperTypes.flatMap[(it as GraphicalModelElement).containingElements]
			containingElements += element.allSubTypes.flatMap[(it as GraphicalModelElement).containingElements]
			return containingElements
		} else #[]
	}

	/**
	 * Returns all containing elements of the provided <code>element</code>.
	 * This only includes containments declared directly through defined containment constraints.
	 *
	 * @param	element	the {@link GraphicalModelElement} for which the containing elements should be retrieved
	 * @return	an {@link Iterable} that contains the {@link ContainingElement ContainingElements} the provided <code>element</code> can be contained in
	 */
	private def containingElements(GraphicalModelElement element){
		return allMGLs.flatMap[graphModels].flatMap[getContainingElements.filter[allContainmentConstraints.exists[types.exists[equalModelElement(element)] && upperBound !== 0]]]
	}

}

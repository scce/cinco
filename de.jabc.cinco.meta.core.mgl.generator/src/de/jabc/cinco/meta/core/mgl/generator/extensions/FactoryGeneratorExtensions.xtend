/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator.extensions

import de.jabc.cinco.meta.core.mgl.generator.elements.ElementEClasses
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import java.io.IOException
import mgl.Edge
import mgl.GraphModel
import mgl.GraphicalModelElement
import mgl.MGLModel
import mgl.Node
import mgl.Type
import mgl.UserDefinedType
import org.eclipse.core.runtime.Path
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class FactoryGeneratorExtensions {
	static extension GeneratorUtils = GeneratorUtils.instance
	
	static def createFactory(MGLModel model, Iterable<ElementEClasses> elmClasses) {
		return createFactory(model, elmClasses, null)
	}
	
	// FIXME: Switching over GraphModel / ModelElement should not be necessary. Introduce a common super class InternalIdentifiableElement instead?
	static def createFactory(MGLModel model, Iterable<ElementEClasses> elmClasses, Iterable<MGLModel> referencedMGLs)'''
		package «model.package».factory
		
		import static extension org.eclipse.emf.ecore.util.EcoreUtil.*
		
		import «model.package».«model.fileName.toLowerCase».«model.fileName.toLowerCase.toFirstUpper»Package
		import «model.package».«model.fileName.toLowerCase».impl.«model.fileName.toLowerCase.toFirstUpper»FactoryImpl
		import «model.package».«model.fileName.toLowerCase».internal.InternalFactory
		import «model.package».«model.fileName.toLowerCase».internal.InternalPackage
		
		import «model.package».«model.fileName.toLowerCase».adapter.*
		
		import graphmodel.internal.InternalModelElement
		import graphmodel.internal.InternalModelElementContainer
		import graphmodel.internal.InternalGraphModel
		import graphmodel.internal.InternalContainer
		import graphmodel.internal.InternalNode
		import graphmodel.internal.InternalEdge
		import graphmodel.internal.InternalType
		import graphmodel.internal.InternalIdentifiableElement
		import graphmodel.ModelElement
		import graphmodel.IdentifiableElement
		import graphmodel.GraphModel
		import graphmodel.Type
		
		import org.eclipse.emf.ecore.EClass
		import org.eclipse.emf.ecore.EPackage
		import org.eclipse.emf.ecore.plugin.EcorePlugin
		
		class «model.fileName»Factory extends «model.factoryName»Impl {
			
«««			Can't call this method as extension...
			final extension InternalFactory = InternalFactory.eINSTANCE
			public static «model.fileName»Factory eINSTANCE = «model.fileName»Factory.init
			
			extension «WorkbenchExtension.name» = new «WorkbenchExtension.name»
			
			static def «model.fileName»Factory init() {
				try {
					val fct = EPackage::Registry.INSTANCE.getEFactory(«model.fileName.toLowerCase.toFirstUpper»Package.eNS_URI) as «model.fileName»Factory
					if (fct !== null)
						return fct as «model.fileName»Factory
				}
				catch (Exception exception) {
					EcorePlugin.INSTANCE.log(exception);
				}
				new «model.fileName»Factory
			}
			
			«elmClasses.specificCreateMethods»
			
			private def <T extends IdentifiableElement> setInternal(T elm, InternalIdentifiableElement internal) {
				elm => [
					if (id.isNullOrEmpty)
						ID = generateUUID
					switch elm {
						GraphModel: elm.setInternalElement_(internal as InternalGraphModel)
						ModelElement: elm.setInternalElement_(internal as InternalModelElement)
						Type: elm.setInternalElement_(internal as InternalType)
					}
				]
			}
			
			«FOR graphmodel : model.graphModels.filter[!isAbstract]»
				/**
				* This method creates a new «graphmodel.fuName» object with an underlying «Resource.name». Thus you can 
				* simply call the «graphmodel.fuName»'s save method to save your changes.
				*/
				def «graphmodel.fqBeanName» create«graphmodel.fuName»(«String.name» path, «String.name» fileName) {
					var filePath = new «Path.name»(path).append(fileName).addFileExtension("«graphmodel.name.toLowerCase»");
					var uri = «URI.name».createPlatformResourceURI(filePath.toOSString(), true);
					var res = new «ResourceSetImpl.name»().createResource(uri);
					var graph = «graphmodel.fqFactoryName».eINSTANCE.create«graphmodel.fuName»();
					
					«EcoreUtil.name».setID(graph, «EcoreUtil.name».generateUUID());

					res.getContents().add(graph.getInternalElement_());
					
					«IF graphmodel.hasPostCreateHook»
						postCreates(graph);
					«ENDIF»
					try {
						res.save(null);
					} catch («IOException.name» e) {
						e.printStackTrace();
					}

					return graph;
				}
			«ENDFOR»
			
			«getPostCreateHooks(model)»
			
		}
	'''
	
	static def factoryName(MGLModel model){
		'''«model.fileName.toLowerCase.toFirstUpper»Factory'''
	}
	
	static def specificCreateMethods(Iterable<ElementEClasses> elementEClasses) {
		elementEClasses
			.map[modelElement]
			.filter[
				switch it {
					GraphModel:      true
					Node:            true
					Edge:            true
					UserDefinedType: true
					default:         false
				}
			]
			.reject[isAbstract]
			.map[specificCreateMethod]
			.join
	}
	
	static def specificCreateMethodsImported(MGLModel mgl) {
		mgl
			.modelElements
			.filter[
				switch it {
					Node:    true
					Edge:    true
					default: false
				}
			]
			.filter(GraphicalModelElement)
			.reject[isAbstract]
			.map[specificCreateMethod(mgl)]
			.join
	}
	
	dispatch static def specificCreateMethod(GraphModel it)'''
		/**
		 * This method creates an «name» with the given id.
		 *
		 * @param ID: The id for the new element
		 * @param ime: The internal model element {@link graphmodel.internal.InternalModelElement}
		 * @param parent: The parent element of the newly created element. Needed if a post create hook accesses the parent
		 * element of the created element
		 * @param hook: Indicates, if the post create hook should be executed
		 */
		def create«name»(String ID, InternalModelElement ime, InternalModelElementContainer parent, boolean hook){
			super.create«name» => [ 
				setID(ID)
				internal = ime ?: createInternal«name» => [
					setID(ID + "_INTERNAL")
					«IF !(it instanceof Type)»
						container = parent
					«ENDIF»
					eAdapters.add(new «mglModel.package».adapter.«name»EContentAdapter)
				]
				«postCreateHook»
			]
			
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(String ID){
			create«name»(ID,null,null,false)
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook will be triggered.
		 */
		def create«name»(InternalModelElementContainer parent){
			create«name»(generateUUID,null,parent,true)
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook will be triggered.
		 */
		def create«name»(String ID, InternalModelElementContainer parent){
			create«name»(ID,null,parent,true)
		}

		def create«name»(String ID, InternalModelElement ime, InternalModelElementContainer parent){
			create«name»(ID,ime,parent,true)
		}

		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(InternalModelElement ime) {
			create«name»(generateUUID,ime,null,false)
		}
		
		override create«name»() {
			create«name»(generateUUID)
		}
		'''
		
	dispatch static def specificCreateMethod(Node it) {
		specificCreateMethod(it, null)
	}
	
	dispatch static def specificCreateMethod(Node it, MGLModel originMgl)'''
		/**
		 * This method creates an «name» with the given id.
		 *
		 * @param ID: The id for the new element
		 * @param ime: The internal model element {@link graphmodel.internal.InternalModelElement}
		 * @param parent: The parent element of the newly created element. Needed if a post create hook accesses the parent
		 * element of the created element
		 * @param hook: Indicates, if the post create hook should be executed
		 */
		def create«name»(String ID, InternalModelElement ime, InternalModelElementContainer parent, boolean hook){
			«IF originMgl === null»
			super.create«name» => [
			«ELSE»
			«originMgl.fileName.toLowerCase»Factory.create«name» => [
			«ENDIF» 
				setID(ID)
				internal = ime ?: «IF originMgl !== null»«originMgl.fileName.toLowerCase»InternalFactory.«ENDIF»createInternal«name» => [
					setID(ID + "_INTERNAL")
					«IF !(it instanceof Type)»
						container = parent
					«ENDIF»
					container = parent
					eAdapters.add(new «mglModel.package».adapter.«name»EContentAdapter)
				]
				«postCreateHook»
			]
			
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(String ID){
			create«name»(ID,null,null,false)
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook will be triggered.
		 */
		def create«name»(InternalModelElementContainer parent){
			create«name»(generateUUID,null,parent,true)
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook will be triggered.
		 */
		def create«name»(String ID, InternalModelElementContainer parent){
			create«name»(ID,null,parent,true)
		}

		def create«name»(String ID, InternalModelElement ime, InternalModelElementContainer parent){
			create«name»(ID,ime,parent,true)
		}

		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(InternalModelElement ime) {
			create«name»(generateUUID,ime,null,false)
		}
		
		«IF originMgl !== null»def«ELSE»override«ENDIF» create«name»() {
			create«name»(generateUUID)
		}
	'''
		
	dispatch static def specificCreateMethod(Edge it) {
		specificCreateMethod(it, null)
	}

	dispatch static def specificCreateMethod(Edge it, MGLModel originMgl)'''
		def create«name»(String ID, InternalModelElement ime, InternalNode source, InternalNode target, boolean hook) {
			«IF originMgl === null»
			super.create«name» => [
			«ELSE»
			«originMgl.fileName.toLowerCase»Factory.create«name» => [
			«ENDIF» 
				setID(ID)
				internal = ime ?: «IF originMgl !== null»«originMgl.fileName.toLowerCase»InternalFactory.«ENDIF»createInternal«name» => [
					(it as InternalEdge).set_sourceElement(source)
					(it as InternalEdge).set_targetElement(target)
					container = source?.rootElement ?: target?.rootElement
					setID(ID + "_INTERNAL")
					eAdapters.add(new «mglModel.package».adapter.«name»EContentAdapter)
				]
				«postCreateHook»
			]
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook will be triggered.
		 */
		def create«name»(String ID, InternalNode source, InternalNode target){
			create«name»(ID,null,source,target,true)
		}
		
		/**
		 * This method creates an «name» with generated id. Post create hook will be triggered.
		 */
		def create«name»(InternalNode source, InternalNode target){
			create«name»(generateUUID,null,source,target,true)
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(String ID){
			create«name»(ID,null,null,null,false)
		}
		
		/**
		 * This method creates an «name» with a generated id. Post create hook won't be triggered.
		 */
		«IF originMgl !== null»def«ELSE»override«ENDIF» create«name»() {
			create«name»(generateUUID)
		}
	'''
	
	dispatch static def specificCreateMethod(UserDefinedType it)'''
		/**
		 * This method creates an «name» with the given id.
		 *
		 * @param ID: The id for the new element
		 * @param ime: The internal model element {@link graphmodel.internal.InternalModelElement}
		 * @param parent: The parent element of the newly created element. Needed if a post create hook accesses the parent
		 * element of the created element
		 * @param hook: Indicates, if the post create hook should be executed
		 */
		def create«name»(String ID, InternalModelElement ime, boolean hook){
			super.create«name» => [ 
				setID(ID)
				internal = ime ?: createInternal«name» => [
					setID(ID + "_INTERNAL")
					eAdapters.add(new «mglModel.package».adapter.«name»EContentAdapter)
				]
				«postCreateHook»
			]
			
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(String ID){
			create«name»(ID,null,false)
		}
		
		/**
		 * This method creates an «name» with the given id. Post create hook won't be triggered.
		 */
		def create«name»(InternalModelElement ime) {
			create«name»(generateUUID,ime,false)
		}
		
		override create«name»() {
			create«name»(generateUUID)
		}
	'''
}

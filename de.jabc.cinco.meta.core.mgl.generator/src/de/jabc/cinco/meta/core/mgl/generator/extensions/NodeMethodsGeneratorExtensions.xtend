/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator.extensions

import de.jabc.cinco.meta.core.mgl.generator.elements.ElementEClasses
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.util.xapi.CollectionExtension
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashSet
import java.util.List
import java.util.Set
import mgl.ContainingElement
import mgl.Edge
import mgl.EdgeElementConnection
import mgl.GraphModel
import mgl.GraphicalElementContainment
import mgl.IncomingEdgeElementConnection
import mgl.MGLModel
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.OutgoingEdgeElementConnection
import mgl.UserDefinedType
import mgl.impl.MglFactoryImpl

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*
import org.eclipse.emf.ecore.util.EcoreUtil

class NodeMethodsGeneratorExtensions {
	
	extension GeneratorUtils = GeneratorUtils.instance
	extension CollectionExtension = new CollectionExtension

	var possiblePredecessorsMap = new HashMap<Node,Iterable<Node>>
	var possibleSuccessorsMap = new HashMap<Node,Iterable<Node>>

	def containmentConstraint(
		GraphicalElementContainment gec) '''
		constraints.add(new ContainmentConstraint(«gec.lowerBound»,«gec.upperBound»,«containedClasses(gec)»));
	'''
	
	protected def String containedClasses(GraphicalElementContainment gec) {
		if(gec.types.size>0){
			gec.types.map[fqBeanName+".class"].join(",")
		}else{
			(gec.containingElement.eContainer as GraphModel).mglModel.nodes.map[fqBeanName+".class"].join(",")
		}
	}

	def allNodesSuperTypesAndSubTypes(Node node){
		val result = #{node} + #{node}.allNodeSubTypes + node.allSuperNodes
		result.forEach(resultNode | EcoreUtil.resolveAll(resultNode))
		return result.toSet
	}
	
	/**
	 * 	returns the most specific GraphModel type that can be rootElement for given model element
	 */	
	def GraphModel mostCommonGraphModel(ModelElement element, Iterable<ElementEClasses> elmClasses){
		val graphModelCandidates = elmClasses.filter[modelElement instanceof GraphModel]
		val matchingGraphModels = new LinkedHashSet<GraphModel>()
		val allUnusedSubTypeCandidates = elmClasses.map[modelElement].filter[allSuperTypes.exists[c | equalModelElement(c, element)]].toSet
		switch element {
			case element instanceof Node: {
				graphModelCandidates.forEach[
					(getUsableNodes((it.modelElement as GraphModel), true).map[e| 
						e.subTypes().filter(Node) + #[e]
					].flattenDiscardNull).forEach[n |
						if(n.equalNodes(element as Node) || 
							n.allSuperTypes.exists[st | 
								(st as Node).equalNodes(element as Node)
							])
						{
							matchingGraphModels.add((it.modelElement as GraphModel))
						}
						allUnusedSubTypeCandidates.removeIf[c | !equalModelElement(c, n)]
					]
				]
			}
			case element instanceof Edge:
				graphModelCandidates.forEach[
					(it.modelElement as GraphModel).usableEdges.map[e| 
						e.subTypes().filter(Edge) + #[e]
					].flattenUnless[e|
						e.nullOrEmpty
					].forEach[e |
						if(e.equalEdges(element as Edge) || e.allSuperTypes.exists[st | (st as Edge).equalEdges(element as Edge)]) {
							matchingGraphModels.add((it.modelElement as GraphModel))
						}
						allUnusedSubTypeCandidates.removeIf[c | !equalModelElement(c, e)]
					]
				]
				
			case element instanceof UserDefinedType:
				graphModelCandidates.forEach[
					(it.modelElement as GraphModel).types.filter(UserDefinedType).map[e| 
						e.subTypes().filter(UserDefinedType) + #[e]
					].flattenUnless[e|
						e.nullOrEmpty
					].forEach[userDefinedType |
						if(userDefinedType.equalUserDefinedTypes(element as UserDefinedType)) {
							matchingGraphModels.add((it.modelElement as GraphModel))
						}
						allUnusedSubTypeCandidates.removeIf[c | !equalModelElement(c, userDefinedType)]
					]
				]	
			case element instanceof GraphModel:
				return element as GraphModel
		}
		if(!allUnusedSubTypeCandidates.empty) {
			return null
		} else if(matchingGraphModels.size === 1) {
			return matchingGraphModels.get(0)
		} else if(matchingGraphModels.size > 1) {
			return matchingGraphModels.findFirst[mgm | matchingGraphModels.forall[mgm2 | MGLUtil.isSubType(mgm, mgm2)]]
		} else {
			return null
		}
	}
	
	def String edgesList(Iterable<Edge> edges) {
		edges.map[edge|GeneratorUtils.instance.fqBeanName(edge) + ".class"].join(",")
	}

	def constraintVariables(Iterable<? extends EdgeElementConnection> connections) {
		val vars = new ArrayList<String>
		connections.forEach[c, i|vars += "cons" + i]
		vars.join(",")
	}

	def possibleSuccessors(Node it) {
		var posSuc = possibleSuccessorsMap.get(it)
		if(posSuc === null){
			posSuc = (#[it] + it.allNodeSubTypes).flatMap[outgoingEdgeConnections].flatMap[
				connectingEdges.toSet.allEdgesSuperTypesAndSubTypes.flatMap[edge |
					edge.edgeElementConnections.filter(IncomingEdgeElementConnection).map[connectedElement]
				]
			].map[it as Node].filterNull
			possibleSuccessorsMap.put(it,posSuc)	
		}
		
		return posSuc
	}
	
	def Set<Edge> allEdgesSuperTypesAndSubTypes(Set<Edge> edges){
		(edges + edges.allEdgeSuperTypes + edges.allEdgeSubTypes).toSet
	}
	
	def allEdgeSuperTypes(Iterable<Edge> edges){
		edges.map[allSuperTypes.map[i| i as Edge]].flatten.toSet		
	}		
	
	def allOtherEdges(Iterable<Edge> edges){
		if(!edges.nullOrEmpty)
			(edges.head.eContainer as MGLModel).edges.drop[edge| edges.contains(edge)]
		else
			#[]
	}
	
	def Iterable<? extends Edge> allEdgeSubTypes(Iterable<Edge> it){
		allOtherEdges.map[edge|edge.allSuperTypes.map[i| i as Edge]].filter[edges|edges.exists[edge| it.contains(edge)]].flatten
	}
	
	def possiblePredecessors(Node it) {
		var posPre=	possiblePredecessorsMap.get(it)
		
		if(posPre === null){
			posPre = (#[it] + it.allNodeSubTypes).flatMap[incomingEdgeConnections].flatMap[
				connectingEdges.toSet.allEdgesSuperTypesAndSubTypes.toSet.flatMap[edge |
					allMGLs.flatMap[nodes.filter[outgoingEdgeConnections.exists[connectingEdges.exists[connectingEdge | MGLUtil.equalEdges(connectingEdge, edge)]]]]
				]
			].map[it as Node].filterNull
			possiblePredecessorsMap.put(it,posPre)
		}
		posPre
	}
	
	/**
	 * Collects all containment constraints of a given ContainingElement including all inherited
	 * containment constraints.
	 * Will Fail if ContainignElement is not instance of NodeContainer or GraphModel.
	 * @param ce - ContainingElement (May be GraphModel or NodeContainer)
	 * @return Iterable containing references to containment reference defined in ce and inherited from other
	 * ContainingElements
	 * */
	def getAllContainmentConstraints(ContainingElement ce){
		ce.containableElements + ce.allSuperTypes.filter(ContainingElement).map[containableElements].flatten
	}
	
	/**
	 * Returns List of all super types of a ContainingElement
	 * List contains of GraphModel if ce is instance of GraphModel
	 * or NodeContainer if ce is instance of NodeContainer.
	 * Fails otherwise.
	 * May return empty list.
	 * @param ce - ContainingElement (May be GraphModel or NodeContainer)
	 * @return ArrayList containing super types of ce
	 */
	def allSuperTypes(ContainingElement ce){
		val superTypes = new ArrayList<ModelElement>
		var sType = ce.extend

		while(sType!==null){
			superTypes.add(sType)
			if(sType instanceof GraphModel) {
				sType = sType.extends
			} else {
				sType = sType.extend
			}
		}
		superTypes
	}

	/**
	 * Returns Inherited Type of a ContainingElement. This  may be a GraphModel if ce is a GraphModel
	 * or NodeContainer if ce is instance of NodeContainer
	 * Fails otherwise.
	 * May return @null.
	 * @param ce: ContainingElement
	 * @returns ContainingElement
	 */
	def extend(ContainingElement ce){
		switch(ce){
			case ce instanceof GraphModel: return ((ce as GraphModel).extends)
			case ce instanceof NodeContainer: return ((ce as NodeContainer).extends)
			default : throw new IllegalArgumentException(String.format("Can not match Type: %s", ce))
		}
	}

}

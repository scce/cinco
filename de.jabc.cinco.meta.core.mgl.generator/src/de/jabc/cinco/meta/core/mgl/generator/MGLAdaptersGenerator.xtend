/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator

import mgl.MGLModel
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.core.utils.projects.ContentWriter
import de.jabc.cinco.meta.core.mgl.generator.extensions.AdapterGeneratorExtension
import mgl.UserDefinedType

class MGLAdaptersGenerator {
	
	extension AdapterGeneratorExtension = new AdapterGeneratorExtension
	
	def generateAdapters(MGLModel model) {
		val project = ProjectCreator.getProject(model.eResource)
		val packageName = model.package + ".adapter"
		ProjectCreator.exportPackage(project,packageName)
		
		for(n: model.graphModels.filter[!isAbstract]){
			val adapterContent = n.generateAdapter
			val fileName = n.name + "EContentAdapter.xtend"
			ContentWriter::writeFile(project, "src-gen", packageName, fileName, adapterContent.toString)
		}
		for (n : model.nodes + model.edges) {
			val fileName = n.name + "EContentAdapter.xtend"
			val adapterContent = n.generateAdapter
			ContentWriter::writeFile(project, "src-gen", packageName, fileName, adapterContent.toString)
		}
		for (t : model.types.filter(UserDefinedType)) {
			val fileName = t.name + "EContentAdapter.xtend"
			val adapterContent = t.generateAdapter
			ContentWriter::writeFile(project, "src-gen", packageName, fileName, adapterContent.toString)
		}
	}
	
}

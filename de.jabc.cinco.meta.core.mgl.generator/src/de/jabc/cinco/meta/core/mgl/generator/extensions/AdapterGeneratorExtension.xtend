/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator.extensions

import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.event.api.util.EventEnum
import de.jabc.cinco.meta.runtime.contentadapter.CincoEContentAdapter
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import graphmodel.internal.InternalGraphModel
import mgl.GraphModel
import mgl.ModelElement
import mgl.UserDefinedType
import org.eclipse.emf.common.notify.Notification
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.util.EContentAdapter

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.mglModel
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.postAttributeValueChange

class AdapterGeneratorExtension {
	
	extension GeneratorUtils = GeneratorUtils.getInstance
	
	def generateAdapter(ModelElement it) '''
		package «mglModel.package».adapter
		
		class «name»EContentAdapter extends «EContentAdapter.name» implements «CincoEContentAdapter.name»{
		
			override notifyChanged(«Notification.name» notification) {
				super.notifyChanged(notification)
				val o = notification.notifier
				val feature = notification.feature
				if (o instanceof «fqInternalBeanName») {
					if (o.eContainer == null && !(o instanceof «InternalGraphModel.name»)) return;
					switch feature {
						«EStructuralFeature.name» case feature.isRelevant: {
							«postAttributeValueChange("o")»
							«EventEnum.POST_ATTRIBUTE_CHANGE.getNotifyCallXtend(it, '''o.element as «fqBeanName»''', 'feature.name', 'notification.oldValue')»
							«IF !(it instanceof GraphModel)»
								//o.element.update
								o.element?.rootElement?.updateModelElements
							«ENDIF»
						}
					}
				}
			}
			
			private def isRelevant(«EStructuralFeature.name» ftr) {
				ftr.eDeliver 
					&& «fqInternalPackageName».eINSTANCE.EClassifiers.contains(ftr?.eContainer)
					&& ftr.name != "libraryComponentUID"
			}
		}
	'''
	
	def generateAdapter(UserDefinedType it) '''
		package «mglModel.package».adapter
		
		class «name»EContentAdapter extends «EContentAdapter.name» {
		
			extension «GraphModelExtension.name» = new «GraphModelExtension.name»

			override notifyChanged(«Notification.name» notification) {
				super.notifyChanged(notification)
				
				val o = notification.notifier
				val feature = notification.feature
				if (o instanceof «fqInternalBeanName») {
					if (o.eContainer == null) return;
						switch feature {
							«EStructuralFeature.name» case feature.isRelevant: {
								«postAttributeValueChange("o")»
								o.element?.rootElement?.updateModelElements
							}
						}
				}
			}
			
			private def isRelevant(«EStructuralFeature.name» ftr) {
				ftr.eDeliver && «fqInternalPackageName».eINSTANCE.EClassifiers.contains(ftr?.eContainer)
			}
		
		}
	'''
	

}

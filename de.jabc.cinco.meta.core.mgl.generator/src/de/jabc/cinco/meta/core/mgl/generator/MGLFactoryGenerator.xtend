/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator

import mgl.MGLModel
import de.jabc.cinco.meta.core.mgl.generator.elements.ElementEClasses
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.util.xapi.WorkspaceExtension
import org.eclipse.core.runtime.Path

import static extension de.jabc.cinco.meta.core.mgl.generator.extensions.FactoryGeneratorExtensions.*
import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.util.xapi.FileExtension

class MGLFactoryGenerator {
	
	extension FileExtension = new FileExtension
	extension GeneratorUtils = GeneratorUtils.instance
	
	def generateFactory(MGLModel model, Iterable<ElementEClasses> elmClasses) {
		val factoryContent = model.createFactory(elmClasses, getAllImportedMGLs(model, true, false))
		
		val path = "/src-gen/" + model.package.replaceAll("\\.", "/") + "/factory"
		val name = model.fileName + "Factory.xtend"
		val project = ProjectCreator.getProject(model.eResource)
		val packageName = model.package
		ProjectCreator.exportPackage(project, packageName+".factory")
		val fullPath = path + "/" + name
		val file = project.getFile(fullPath)
		if (!file.exists) {
			val we = new WorkspaceExtension()
			we.createFolders(project, new Path(path))
			we.create(file)
		}
		file.writeContent(factoryContent.toString)
	}
	
}

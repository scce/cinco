/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator.elements;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EcoreFactory;

import mgl.ModelElement;

public class ElementEClasses {
	private EClass mainEClass = null;
	private EClass internalEClass = null;
	private EClass mainView = null;
	private ModelElement modelElement;
	
	public ElementEClasses(ModelElement modelElement) {
		this.modelElement = modelElement;
		this.mainEClass = EcoreFactory.eINSTANCE.createEClass();
		this.mainEClass.setName(modelElement.getName());
		this.mainEClass.setAbstract(modelElement.isIsAbstract());
		this.internalEClass = EcoreFactory.eINSTANCE.createEClass();
		this.internalEClass.setName("Internal" + modelElement.getName());
		this.internalEClass.setAbstract(modelElement.isIsAbstract());
		this.mainView = EcoreFactory.eINSTANCE.createEClass();
		this.mainView.setName(modelElement.getName() + "View");
		this.mainView.setAbstract(modelElement.isIsAbstract());
	}

	public EClass getMainEClass() {
		return mainEClass;
	}

	public void setMainEClass(EClass mainEClass) {
		this.mainEClass = mainEClass;
	}

	public EClass getInternalEClass() {
		return internalEClass;
	}

	public void setInternalEClass(EClass internalEClass) {
		this.internalEClass = internalEClass;
	}

	public EClass getMainView() {
		return mainView;
	}

	public void setMainView(EClass mainView) {
		this.mainView = mainView;
	}

	public ModelElement getModelElement() {
	  return this.modelElement;
	}
	
	public void setModelElement(ModelElement modelElement){
		this.modelElement = modelElement;
	}
	
}

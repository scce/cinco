/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator.runtime;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import graphmodel.GraphModel;

public interface IGenerator<T extends GraphModel> {
	
	/**
	 * @param graphModel Graph model for which code is to be generated.
	 * @param outlet     Path to given outlet. When generate is called,
	 *                   existence of path is  guaranteed.
	 * @throws RuntimeException
	 */ 
	public void generate(T graphModel, IPath outlet, IProgressMonitor monitor);

	public default void collectTasks(T graphModel, IPath outlet, CompoundJob job) {
		job
			.consume(100)
			.task("Generating...", () -> generate(graphModel, outlet, new NullProgressMonitor()));
	}
	
}

/**
 * -
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator.runtime;

import java.nio.file.Path;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.runtime.xapi.NIOExtension;
import graphmodel.GraphModel;

public interface IGenerator2<T extends GraphModel> extends IGenerator<T> {
	
	@Override
	public default void generate(T graphModel, IPath outlet, IProgressMonitor monitor) {
		var nio = new NIOExtension();
		this.generate(graphModel, nio.toNIOPath(outlet), monitor);
	}
	
	@Override
	public default void collectTasks(T graphModel, IPath outlet, CompoundJob job) {
		var nio = new NIOExtension();
		this.collectTasks(graphModel, nio.toNIOPath(outlet), job);
	}
	
	
	
	/**
	 * @param graphModel Graph model for which code is to be generated.
	 * @param outlet     Path to given outlet. When generate is called,
	 *                   existence of path is  guaranteed.
	 * @throws RuntimeException
	 */
	public void generate(T graphModel, Path outlet, IProgressMonitor monitor);
	
	public default void collectTasks(T graphModel, Path outlet, CompoundJob job) {
		job
			.consume(100)
			.task("Generating...", () -> this.generate(graphModel, outlet, new NullProgressMonitor()));
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator.runtime.handler;

import static de.jabc.cinco.meta.core.utils.job.JobFactory.job;

import graphmodel.GraphModel;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.plugin.generator.runtime.registry.GeneratorDiscription;
import de.jabc.cinco.meta.plugin.generator.runtime.registry.GraphModelGeneratorRegistry;
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension;

/**
 * Handler implementation for the code generation command.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class GraphModelCodeGenerationHandler extends AbstractHandler {
	
	private static WorkbenchExtension eapi = new WorkbenchExtension();
	
	private IEditorPart activeEditor;
	private GraphModel graphModel;
	private GeneratorDiscription<GraphModel> generatorDescription;
	private String fileName;
	private IProject project;
	private IPath outlet;
	
	public GraphModelCodeGenerationHandler() { }
	
	/**
	 * the command has been executed, so extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		CompoundJob job = job("Code Generation");
		
		job.consume(100, "Initializing...")
		   .task("Retrieve active editor", () -> retrieveActiveEditor(event))
		   .task("Retrieve graph model", this::retrieveGraphModel)
		   .task("Retrieve generator", this::retrieveGenerator)
		   .cancelIf(this::isGeneratorMissing, "Failed to retrieve generator.\n\n"
					+ "Either this type of model is not associated with a generator "
					+ "or something went seriously wrong. In the latter case, try to "
					+ "restart the application.")
		   .task("Initialize outlet", this::initOutlet);
		job.schedule();
		
		try {
			job.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		IStatus result = job.getResult();
		if (Status.OK != result.getSeverity()) {
			return null;
		}
		
		job = job("Code Generation")
				.label("Generating for model " + fileName + " ...");
		
		generatorDescription.getGenerator().collectTasks(graphModel, outlet, job);
		
		job.consume(5, "Refreshing workspace...")
		   .task("Refresh workspace", this::refreshProject)
		   .onFinishedShowMessage("Code generation successful.")
		   .schedule();
		
		return null;
	}
	
	private void retrieveActiveEditor(ExecutionEvent event) {
		try {
			IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
			activeEditor = window.getActivePage().getActiveEditor();
		} catch (Exception e) {
			throw new RuntimeException("Failed to retrieve active editor.", e);
		}
	}
	
	@SuppressWarnings("restriction")
	private void retrieveGraphModel() {
		try {
			fileName = eapi.getResource(activeEditor).getURI().lastSegment();
			graphModel = eapi.getGraphModel(activeEditor);
		} catch (Exception e) {
			throw new RuntimeException("Failed to retrieve graph model for editor: " + activeEditor, e);
		}
	}
	
	private void retrieveGenerator() {
		try {
			System.err.println("### model.class: " + graphModel.getClass());
			List<GeneratorDiscription<GraphModel>> generatorDescriptions =
					GraphModelGeneratorRegistry.INSTANCE.getAllGenerators(graphModel.getClass());
			if (generatorDescriptions != null && !generatorDescriptions.isEmpty()) 
				generatorDescription = generatorDescriptions.get(0);
		} catch(Exception e) {
			throw new RuntimeException("Failed to retrieve generator.", e);
		}
	}
	
	private boolean isGeneratorMissing() {
		return generatorDescription == null
				|| generatorDescription.getGenerator() == null;
	}
	
	@SuppressWarnings("restriction")
	private void initOutlet() {
		try {
			project = eapi.getProject(activeEditor);
			if (project == null || !project.isOpen()) {
				throw new RuntimeException("The project is closed or does not exist: " + project);
			}
			outlet = project.getLocation().append(generatorDescription.getOutlet());
			if (!outlet.toFile().exists()) {
				outlet.toFile().mkdirs();
				project.refreshLocal(IProject.DEPTH_INFINITE, null);
			} else if (!outlet.toFile().isDirectory()) {
				throw new RuntimeException("Outlet exists, but is no directory: " + outlet);
			}
		} catch(RuntimeException e) {
			throw e;
		} catch(Exception e) {
			throw new RuntimeException("Unexpected exception. Failed to initialize the outlet: " + outlet, e);
		}
	}
	
	private void refreshProject() {
		try {
			project.refreshLocal(IProject.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator.runtime.registry;

import graphmodel.GraphModel;
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;


public class GeneratorDiscription<T extends GraphModel> {
	
	private IGenerator<T> generator =null;
	private	String outlet = null;
	
	public GeneratorDiscription(){}
	
	public GeneratorDiscription(IGenerator<T> generator, String outlet){
		this.setGenerator(generator);
		this.setOutlet(outlet);
	}

	public IGenerator<T> getGenerator() {
		return generator;
	}

	public void setGenerator(IGenerator<T> generator) {
		this.generator = generator;
	}

	public String getOutlet() {
		return outlet;
	}

	public void setOutlet(String outlet) {
		this.outlet = outlet;
	}

}

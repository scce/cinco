/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator.runtime.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.ClassUtils;

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;
import graphmodel.GraphModel;

public class GraphModelGeneratorRegistry<T extends GraphModel> {
	HashMap<String,List<GeneratorDiscription<T>>> generators;
	
	public static GraphModelGeneratorRegistry<GraphModel> INSTANCE = new GraphModelGeneratorRegistry<GraphModel>();
	
	private GraphModelGeneratorRegistry(){
		this.generators = new HashMap<String, List<GeneratorDiscription<T>>>();
	}
	
	public void addGenerator(String graphModelClassName, IGenerator<T> generator, String outlet){
		List<GeneratorDiscription<T>> _generators = this.generators.get(graphModelClassName);
		if(_generators==null){
			_generators = new ArrayList<GeneratorDiscription<T>>();
		}
		
		_generators.add(new GeneratorDiscription<T>(generator, outlet));
		
		this.generators.put(graphModelClassName,_generators);
	}
	
	public GeneratorDiscription<T> getGeneratorAt(String graphModelClassName,int i){
		return this.generators.get(graphModelClassName).get(i);
	}
	
	public List<GeneratorDiscription<T>> getAllGenerators(String graphModelClassName){
		return this.generators.get(graphModelClassName);
	}
	
	public List<GeneratorDiscription<T>> getAllGenerators(Class<?> graphModelClass){
		List<GeneratorDiscription<T>> list = this.generators.get(graphModelClass.getName().replace("Impl", "").replace(".impl", ""));
		if (list != null && !list.isEmpty())
			return list;
		for (Object ntrfc : ClassUtils.getAllInterfaces(graphModelClass)) {
			if (ntrfc instanceof Class) {
				list = getAllGenerators((Class<?>) ntrfc);
				if (list != null && !list.isEmpty())
					return list;
			}
		}
		return list;
	}
}

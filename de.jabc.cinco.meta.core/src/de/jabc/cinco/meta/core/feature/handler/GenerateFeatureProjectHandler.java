/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.feature.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.resource.IResourceDescriptions;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;
import org.eclipse.xtext.util.StringInputStream;

import com.google.inject.Inject;

import de.jabc.cinco.meta.core.pluginregistry.PluginRegistry;
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.core.utils.BundleRegistry;
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class GenerateFeatureProjectHandler extends AbstractHandler {

	@Inject
	IResourceDescriptions resourceDescriptions;

	@Inject
	IResourceSetProvider resourceSetProvider;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			IFile file = MGLSelectionListener.INSTANCE.getSelectedCPDFile();
			if(file!=null){
				ResourceSet rSet = new ResourceSetImpl();
				generateFeature(file,rSet);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	private void generateFeature(IFile file, ResourceSet rSet) {
		Resource res = rSet.createResource(URI
				.createPlatformResourceURI(file.getFullPath()
						.toPortableString(), true));
		try {
			res.load(null);
			CincoProduct cpd = (CincoProduct) res.getContents()
					.get(0);
			String packageName = ProjectCreator.getProjectSymbolicName(ProjectCreator.getProject(cpd.eResource())); 
			String projectName = packageName + ".feature";
			ArrayList<String> srcFolders = new ArrayList<>();
			srcFolders.add("src");
			ArrayList<IProject> referencedProjects = new ArrayList<>();
			HashSet<String> requiredBundles = new HashSet<>();
			IResource iRes = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(cpd.eResource().getURI().toPlatformString(true)));
			String symbolicName;
			if (iRes != null)
				symbolicName = ProjectCreator.getProjectSymbolicName(iRes.getProject());
			else symbolicName = file.getProject().getName();
			requiredBundles.add(symbolicName);
			ArrayList<String> exportedPackages = new ArrayList<>();
			ArrayList<String> additionalNatures = new ArrayList<>();
			additionalNatures.add("org.eclipse.pde.FeatureNature");

			IProject featureProject = ProjectCreator.createProject(
				projectName, srcFolders, referencedProjects,
				requiredBundles, exportedPackages,
				additionalNatures, null,false);
			
			// Generating feature.xml
			try{
				HashMap<String, Set<String>> annotationToMGLPackageMap = PluginRegistry.getInstance().getMGLDependentPlugins();
				HashMap<String, Set<String>> annotationToPackageMap = PluginRegistry.getInstance().getUsedPlugins();
				HashMap<String, Set<String>> annotationToFragmentMap = PluginRegistry.getInstance().getUsedFragments();
				HashMap<String, Set<String>> annotationToMGLFragmentMap = PluginRegistry.getInstance().getMGLDependentFragments();
				
				Set<String> otherPluginIDs = new HashSet<>();
				otherPluginIDs.addAll(BundleRegistry.INSTANCE.getPluginIDs());
				otherPluginIDs.addAll(customPluginIDs());
				Set<String> otherFragmentIDs = BundleRegistry.INSTANCE.getFragmentIDs();
				GenerateFeatureXML.generate(packageName,annotationToMGLPackageMap,annotationToPackageMap,annotationToFragmentMap,annotationToMGLFragmentMap, otherPluginIDs,otherFragmentIDs,featureProject);
			}catch(Exception e){
				throw new Exception("Could not create feature.xml",e);
			}
			
			// Generating pom.xml
			try {
				mavenizeFeatureProject(featureProject, packageName);
			}catch(Exception e) {
				throw new Exception("Could not create pom.xml for feature project",e);
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	private Set<String> customPluginIDs() {
		HashSet<String> ids = new HashSet<>();
		ids.add("org.apache.felix.scr");
		return ids;
	}

	private void mavenizeFeatureProject(IProject featureProject, String packageName) throws CoreException {
		NullProgressMonitor monitor = new NullProgressMonitor();
		StringInputStream content = new StringInputStream(FeaturePOMTemplate.content(packageName).toString());
		IFile productPOMFile = featureProject.getFile("pom.xml");
		if(productPOMFile.exists())
			productPOMFile.setContents(content,false,false,monitor);
		else
			productPOMFile.create(content,false,monitor);
		
	}
}


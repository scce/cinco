/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.feature.handler

import java.util.HashMap
import java.util.Set
import org.eclipse.core.resources.IProject
import org.eclipse.xtext.util.StringInputStream
import java.util.HashSet
import mgl.MGLModel
import productDefinition.CincoProduct
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener
import de.jabc.cinco.meta.util.xapi.FileExtension;
import de.jabc.cinco.meta.core.utils.PathValidator;
import java.util.ArrayList
import java.util.List

class GenerateFeatureXML {
	def static void generate(String packageName,
			HashMap<String, Set<String>> annotationToMGLPackageMap, HashMap<String, Set<String>> annotationToPackageMap,
			HashMap<String, Set<String>> annotationToFragmentMap,
			HashMap<String, Set<String>> annotationToMGLFragmentMap,
			Set<String> otherPluginIDs, Set<String> otherFragmentIDs, IProject featureProject) {
				
		val plugins = otherPluginIDs
		val fragments = otherFragmentIDs
		 
		allMGLs.forEach[model| 
			plugins.addAll(collectPlugins(model,annotationToPackageMap,annotationToMGLPackageMap,packageName))
			fragments.addAll(collectFragments(model,annotationToFragmentMap,annotationToMGLFragmentMap,packageName))
		]
		val featureXML = generateXML(plugins,fragments,packageName)
			
		val xmlFile = featureProject.getFile("feature.xml")
		if(xmlFile.exists){
			xmlFile.setContents(new StringInputStream(featureXML.toString),true,true,null);
		}else{
			xmlFile.create(new StringInputStream(featureXML.toString),true,null)
		}
	} 
	
	def static generateXML(Iterable<String> plugins, Iterable<String> fragments,String packageName) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<feature id="«packageName».feature"
			label="«packageName» Feature "
			version="1.0.0.qualifier">
		<includes
				id="de.jabc.cinco.meta.feature"
				version="0.0.0"/>
			«FOR pl: plugins»
			«IF pl!='''«packageName».feature'''.toString»
			<plugin
				id="«pl»"
				download-size="0"
				install-size="0"
				version="0.0.0"
				unpack="false"/>
			«ENDIF»
			«ENDFOR»
			«FOR frg: fragments»
				<plugin
					id="«frg»"
					download-size="0"
					install-size="0"
					version="0.0.0"
					fragment="true"
					unpack="false"/>
			«ENDFOR»
		</feature>
	'''
	
	def static collectPlugins(MGLModel model, HashMap<String, Set<String>> annotationToPackageMap, HashMap<String, Set<String>> annotationToMGLPackageMap,String packageName){
		model.annotations.map [anno|
				var pkgs = annotationToPackageMap.get(anno.name);
				if(pkgs===null){
					pkgs=new HashSet<String>();
				} 
				val mglPkgs = if(annotationToMGLPackageMap.get(anno.name)!==null){annotationToMGLPackageMap.get(anno.name).map[mglDepPkg|String.format("%s.%s", packageName, mglDepPkg)]}else{ new HashSet<String>()};
				pkgs+mglPkgs
			].flatten
	}
	
	def static collectFragments(MGLModel model,HashMap<String, Set<String>> annotationToFragmentMap,
			HashMap<String, Set<String>> annotationToMGLFragmentMap,String packageName){
				
		model.annotations.map [anno|
			var frgm = annotationToFragmentMap.get(anno.name);
			if(frgm===null){
				frgm=new HashSet<String>
			}
			val mglFrgm = if(annotationToMGLFragmentMap.get(anno.name)!==null){annotationToMGLFragmentMap.get(anno.name).map [mglDepFrg| String.format("%s.%s", packageName, mglDepFrg)]}else{new HashSet<String>()};
			frgm+mglFrgm
		].flatten
	}
	
	def static allMGLs(){
		val cpdFile = MGLSelectionListener.INSTANCE.selectedCPDFile
		val cpd = MGLSelectionListener.INSTANCE.selectedCPD
		val cpdProject = cpdFile.project
		cpd.mgls.map[descriptor|
			val fileExtension = new FileExtension
			val path          = PathValidator.getRelativePath(descriptor.getMglPath(),cpdProject);
			val file          = cpdProject.getFile(path);
			fileExtension.getContent(file, MGLModel);
		]
	}
	
}

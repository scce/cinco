/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 - 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.feature.handler

class FeaturePOMTemplate {
	def static content(String mglProjectID)'''
	<!-- GENERATED POM NO CHANGES REQUIRED BELOW THIS LINE -->
	<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	  <modelVersion>4.0.0</modelVersion>
	  <artifactId>«mglProjectID».feature</artifactId>
	  <parent>
	        <groupId>«mglProjectID»</groupId>
		<artifactId>«mglProjectID».parent</artifactId>
	  	<version>1.0.0-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	  </parent>
	  <packaging>eclipse-feature</packaging>
	</project> 
	'''
}

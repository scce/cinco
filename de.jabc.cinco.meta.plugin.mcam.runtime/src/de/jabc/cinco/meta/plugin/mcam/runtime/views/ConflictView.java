/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.mcam.runtime.views;

import java.io.File;
import java.util.List;

import info.scce.mcam.framework.processes.MergeInformation.MergeType;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;

import de.jabc.cinco.meta.plugin.mcam.runtime.views.pages.ConflictViewPage;
import de.jabc.cinco.meta.plugin.mcam.runtime.views.provider.ConflictViewTreeProvider.ViewType;
import de.jabc.cinco.meta.plugin.mcam.runtime.views.utils.EclipseUtils;

@SuppressWarnings("rawtypes")
public class ConflictView extends McamView<ConflictViewPage> {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "de.jabc.cinco.meta.plugin.mcam.runtime.views.ConflictView";

	private IAction showByIdViewAction;
	
	private IAction sortByMergeTypeAction;
	private IAction sortByNameAction;
	
	/*
	 * Filtering
	 */
	private IAction filterNothingAction;
	
	private IAction filterOnlyConflictedAction;
	private IAction filterOnlyAddedAction;
	private IAction filterOnlyChangedAction;
	private IAction filterOnlyDeletedAction;

	@Override
	protected void fillLocalPullDown(IMenuManager manager) {
		IMenuManager viewSubmenu = new MenuManager("Views");
		manager.add(viewSubmenu);
		viewSubmenu.add(showByIdViewAction);

		IMenuManager filterSubmenu = new MenuManager("Filters");
		manager.add(filterSubmenu);
		filterSubmenu.add(filterNothingAction);
		filterSubmenu.add(filterOnlyAddedAction);
		filterSubmenu.add(filterOnlyChangedAction);
		filterSubmenu.add(filterOnlyConflictedAction);
		filterSubmenu.add(filterOnlyDeletedAction);
		
		IMenuManager sortSubmenu = new MenuManager("Sort By");
		manager.add(sortSubmenu);
		sortSubmenu.add(sortByNameAction);
		sortSubmenu.add(sortByMergeTypeAction);
	}

	@Override
	protected void fillContextMenu(IMenuManager manager) {
		manager.add(openModelAction);
	}

	@Override
	protected void fillLocalToolBar(IToolBarManager manager) {
		manager.add(expandAllAction);
		manager.add(collapseAllAction);
		manager.add(new Separator());
	}

	@Override
	protected void makeActions() {
		super.makeActions();
		
		/*
		 * ------------------------------------------ */
		showByIdViewAction = new Action() {
			public void run() {
				activePage.getDataProvider().setActiveView(ViewType.BY_ID);
				activePage.getTreeViewer().setLabelProvider(
						activePage.getDefaultLabelProvider());

				EclipseUtils.runBusy(new Runnable() {
					public void run() {
						activePage.reload();
						reloadViewState();
					}
				});
			}
		};
		showByIdViewAction.setText("by entity");
		showByIdViewAction.setToolTipText("show results by entity");
		showByIdViewAction.setChecked(false);

		/*
		 * ------------------------------------------ 
		 */
		sortByMergeTypeAction = new Action() {
			public void run() {
				updateSorter(sortByMergeTypeAction);
			}
		};
		sortByMergeTypeAction.setText("sort by merge type");
		sortByMergeTypeAction.setToolTipText("sort ids by merge type");
		sortByMergeTypeAction.setEnabled(true);
		sortByMergeTypeAction.setChecked(false);
		
		sortByNameAction = new Action() {
			public void run() {
				updateSorter(sortByNameAction);
			}
		};
		sortByNameAction.setText("sort by name");
		sortByNameAction.setToolTipText("sort ids by name alphabetical");
		sortByNameAction.setEnabled(true);
		sortByNameAction.setChecked(false);
		
		

		/*
		 * ------------------------------------------ */
		filterNothingAction = new Action() {
			public void run() {
				updateFilter(filterNothingAction);
			}
		};
		filterNothingAction.setText("show all");
		filterNothingAction.setToolTipText("show all ids");
		filterNothingAction.setEnabled(true);
		filterNothingAction.setChecked(false);
		
		filterOnlyAddedAction = new Action() {
			public void run() {
				updateFilter(filterOnlyAddedAction);
			}
		};
		filterOnlyAddedAction.setText("show added");
		filterOnlyAddedAction.setToolTipText("show added ids");
		filterOnlyAddedAction.setEnabled(true);
		filterOnlyAddedAction.setChecked(false);
		
		filterOnlyChangedAction = new Action() {
			public void run() {
				updateFilter(filterOnlyChangedAction);
			}
		};
		filterOnlyChangedAction.setText("show changed");
		filterOnlyChangedAction.setToolTipText("show changed ids");
		filterOnlyChangedAction.setEnabled(true);
		filterOnlyChangedAction.setChecked(false);
		
		filterOnlyConflictedAction = new Action() {
			public void run() {
				updateFilter(filterOnlyConflictedAction);
			}
		};
		filterOnlyConflictedAction.setText("show conflicted");
		filterOnlyConflictedAction.setToolTipText("show conflicted ids");
		filterOnlyConflictedAction.setEnabled(true);
		filterOnlyConflictedAction.setChecked(false);
		
		filterOnlyDeletedAction = new Action() {
			public void run() {
				updateFilter(filterOnlyDeletedAction);
			}
		};
		filterOnlyDeletedAction.setText("show deleted");
		filterOnlyDeletedAction.setToolTipText("show deleted ids");
		filterOnlyDeletedAction.setEnabled(true);
		filterOnlyDeletedAction.setChecked(false);
		
	}

	@Override
	protected void reloadViewState() {
		showByIdViewAction.setChecked(false);

		switch (activePage.getDataProvider().getActiveView()) {
		case BY_ID:
			showByIdViewAction.setChecked(true);
			break;

		default:
			break;
		}
		
		restoreActiveFilter(activePage.getActiveFilter());
		restoreActiveSorter(activePage.getActiveSort());

		activePage.getTreeViewer().refresh();
	}

	protected void updateSorter(IAction action) {
		sortByNameAction.setChecked(false);
		sortByMergeTypeAction.setChecked(false);
		if(action == sortByNameAction) {
			sortByNameAction.setChecked(true);
			activePage.setActiveSort(1);
			activePage.getTreeViewer().setSorter(activePage.getNameSorter());
		}
		if(action == sortByMergeTypeAction) {
			sortByMergeTypeAction.setChecked(true);
			activePage.setActiveSort(0);
			activePage.getTreeViewer().setSorter(activePage.getTypeSorter());
		}
	}
	
	private void restoreActiveSorter(int sorter) {
		switch (sorter) {
			case 1:
				sortByNameAction.run();
				break;
			case 0:
			default:
				sortByMergeTypeAction.run();
				break;
		}
	}
	

	protected void updateFilter(IAction action) {
		filterNothingAction.setChecked(false);
		
		filterOnlyAddedAction.setChecked(false);
		activePage.getTreeViewer().removeFilter(activePage.getConflictViewTypeFilter(MergeType.ADDED));
		
		filterOnlyChangedAction.setChecked(false);
		activePage.getTreeViewer().removeFilter(activePage.getConflictViewTypeFilter(MergeType.CHANGED));
		
		filterOnlyDeletedAction.setChecked(false);
		activePage.getTreeViewer().removeFilter(activePage.getConflictViewTypeFilter(MergeType.DELETED));
		
		filterOnlyConflictedAction.setChecked(false);
		activePage.getTreeViewer().removeFilter(activePage.getConflictViewTypeFilter(MergeType.CONFLICTED));
		
		if(action == filterNothingAction) {
			filterNothingAction.setChecked(true);
			activePage.setActiveFilter(0);
		}
		
		if(action == filterOnlyAddedAction) {
			activePage.getTreeViewer().addFilter(activePage.getConflictViewTypeFilter(MergeType.ADDED));
			filterOnlyAddedAction.setChecked(true);
			activePage.setActiveFilter(1);
		}
		if(action == filterOnlyChangedAction) {
			activePage.getTreeViewer().addFilter(activePage.getConflictViewTypeFilter(MergeType.CHANGED));
			filterOnlyChangedAction.setChecked(true);
			activePage.setActiveFilter(2);
		}
		if(action == filterOnlyDeletedAction) {
			activePage.getTreeViewer().addFilter(activePage.getConflictViewTypeFilter(MergeType.DELETED));
			filterOnlyDeletedAction.setChecked(true);
			activePage.setActiveFilter(3);
		}
		if(action == filterOnlyConflictedAction) {
			activePage.getTreeViewer().addFilter(activePage.getConflictViewTypeFilter(MergeType.CONFLICTED));
			filterOnlyConflictedAction.setChecked(true);
			activePage.setActiveFilter(4);
		}
	}

	private void restoreActiveFilter(int filter) {
		switch (filter) {
			case 1:
				filterOnlyAddedAction.run();
				break;
			case 2:
				filterOnlyChangedAction.run();
				break;
			case 3:
				filterOnlyDeletedAction.run();
				break;
			case 4:
				filterOnlyConflictedAction.run();
				break;
			case 0:
			default:
				filterNothingAction.run();
				break;
		}
	}

	@Override
	public ConflictViewPage<?, ?, ?> createPage(String pageId, IEditorPart editor) {
		List<PageFactory> pfs = getPageFactories();
		for (PageFactory pf : pfs) {
			ConflictViewPage<?, ?, ?> page = pf.createConflictViewPage(pageId, editor);
			if (page != null)
				return page;
		}
		return null;
	}

	@Override
	public String getPageId(IResource res) {
		if (res instanceof IFile == false || res == null)
			return null;

		IFile file = (IFile) res;
		String path = file.getRawLocation().toOSString();
		File origFile = new File(path);
		return origFile.getAbsolutePath();
	}

	@Override
	protected void initView(Composite parent) {
	}

}

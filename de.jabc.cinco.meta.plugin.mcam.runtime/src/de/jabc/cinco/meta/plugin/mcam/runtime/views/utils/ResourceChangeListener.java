/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.mcam.runtime.views.utils;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.swt.widgets.Display;

import de.jabc.cinco.meta.plugin.mcam.runtime.views.McamView;
import de.jabc.cinco.meta.plugin.mcam.runtime.views.pages.McamPage;

public class ResourceChangeListener implements IResourceChangeListener {

	public enum ListenerEvents {
		ADDED, REMOVED, CONTENT_CHANGE
	}

	private final McamView<?> view;

	private Set<ListenerEvents> actualEvents;

	@SuppressWarnings("rawtypes")
	public ResourceChangeListener(McamView view) {
		super();
		this.view = view;
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		if (delta == null)
			return;

		IFile iFile = getIFileFromDelta(delta);
		if (iFile == null)
			return;

		// Check if event is relevant for file extension
		getActualListenerEvents(event);
		
		Set<ListenerEvents> retainSet = new HashSet<ListenerEvents>();
		retainSet.add(ListenerEvents.CONTENT_CHANGE);
		retainSet.retainAll(actualEvents);
		
		if (retainSet.size() < 1)
			return;

		// Execute refresh
		String pageId = view.getPageId(iFile);
		if (pageId != null) {
			Object obj = view.getPageMap().get(pageId);
			if (obj instanceof McamPage) {
				
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						((McamPage) obj).reload();
						view.refreshView();
					}
				});
			}
		}
	}

	private void getActualListenerEvents(IResourceChangeEvent event) {
		actualEvents = new HashSet<ListenerEvents>();

		switch (event.getType()) {
		case IResourceChangeEvent.PRE_CLOSE:
			break;
		case IResourceChangeEvent.PRE_DELETE:
			break;
		case IResourceChangeEvent.POST_CHANGE:
			try {
				event.getDelta().accept(new DeltaPrinter());
			} catch (CoreException e) {
				e.printStackTrace();
			}
			break;
		case IResourceChangeEvent.PRE_BUILD:
			try {
				event.getDelta().accept(new DeltaPrinter());
			} catch (CoreException e) {
				e.printStackTrace();
			}
			break;
		case IResourceChangeEvent.POST_BUILD:
			try {
				event.getDelta().accept(new DeltaPrinter());
			} catch (CoreException e) {
				e.printStackTrace();
			}
			break;
		}
	}

	private IFile getIFileFromDelta(IResourceDelta delta) {
		for (IResourceDelta child : delta.getAffectedChildren()) {
			IResource res = child.getResource();
			if (res instanceof IFile) {
				IFile file = (IFile) res;
				return file;
			}
			return getIFileFromDelta(child);
		}
		return null;
	}

	public class DeltaPrinter implements IResourceDeltaVisitor {
		public boolean visit(IResourceDelta delta) {
			switch (delta.getKind()) {
			case IResourceDelta.ADDED:
				actualEvents.add(ListenerEvents.ADDED);
				break;
			case IResourceDelta.REMOVED:
				actualEvents.add(ListenerEvents.REMOVED);
				break;
			case IResourceDelta.CHANGED:
				int flags = delta.getFlags();
				if ((flags & IResourceDelta.CONTENT) != 0) {
					actualEvents.add(ListenerEvents.CONTENT_CHANGE);
				}
				break;
			}
			return true; // visit the children
		}
	}

}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.mcam.runtime.views.provider;

import de.jabc.cinco.meta.plugin.mcam.runtime.views.nodes.TreeNode;

public abstract class TreeProvider {

	private boolean updateNeeded = true;

	public boolean isUpdateNeeded() {
		return updateNeeded;
	}

	public void flagDirty() {
		updateNeeded = true;
	}

	public void flagClean() {
		updateNeeded = true;
	}

	public void load(Object rootObject) {
		synchronized (this) {
			if (updateNeeded) {
				loadData(rootObject);
				updateNeeded = false;
			}
		}
	}

	abstract protected void loadData(Object rootObject);

	public TreeNode getTree() {
		synchronized (this) {
			return getTreeRoot();
		}
	}

	abstract protected TreeNode getTreeRoot();

	protected TreeNode findExistingNode(TreeNode node, TreeNode parentNode) {
		TreeNode existingNode = parentNode.find(node.getId());

		if (existingNode != null)
			return existingNode;

		parentNode.getChildren().add(node);
		node.setParent(parentNode);
		return node;
	}
}

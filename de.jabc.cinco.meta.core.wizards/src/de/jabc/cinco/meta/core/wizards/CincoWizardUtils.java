/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.wizards;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.JavaConventions;

public class CincoWizardUtils {
	
	public static String validateModelName(String modelName) {
		if (!modelName.isEmpty()) {
			IStatus nameStatus = JavaConventions.validateIdentifier(modelName, "1.7", "1.7");
			if (nameStatus.getCode() != IStatus.OK) {
				return "Model Name: " + nameStatus.getMessage();
			}
			else if (!Character.isUpperCase(modelName.charAt(0))) {
				return "Model Name: must start with capital letter";
			}
		}
		else {
			return "Model Name: must not be empty";
		}
		return null;
	}

	public static String validatePackageName(String packageName) {
		if (!packageName.isEmpty()) {
			IStatus nameStatus = JavaConventions.validatePackageName(packageName, "1.7", "1.7");
			if (nameStatus.getCode() != IStatus.OK) {
				return "Package Name: " + nameStatus.getMessage();
			}
			
		} else {
			return "Package Name: must not be empty";
		}
		return null;
	}

	public static String validateProjectName(String projectName) {
		if (projectName.isEmpty())
			return "Enter project name";
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot()
				.getProjects();
		for (IProject p : projects) {
			if (p.getName().equals(projectName))
				return "Project: " + projectName + " already exists";
		}
		if (projectName.matches(".*[:/\\\\\"&<>\\?#,;].*")) {
			return "The project name contains illegal characters (:/\"&<>?#,;)";
		}
		if(!projectName.toLowerCase().equals(projectName)) {
			return "Project/Package Name: must not contain upper case letters";
		}	

		return null;
	}

}

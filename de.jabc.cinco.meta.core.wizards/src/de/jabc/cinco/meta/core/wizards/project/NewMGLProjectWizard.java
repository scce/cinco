/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.wizards.project;

import java.util.EnumSet;
import java.util.Set;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWizard;

public class NewMGLProjectWizard extends Wizard implements IWorkbenchWizard{
	MainPage mainPage = new MainPage("mainPage");
	CreateExampleProjectPage examplesPage = new CreateExampleProjectPage("examplesPage");
	CreateNewProjectPage newProjectPage = new CreateNewProjectPage("newProjectPage");

	@Override
	public void addPages() {
		addPage(mainPage);
		addPage(examplesPage);
		addPage(newProjectPage);
		super.addPages();
	}
	
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		if (page == mainPage) {
			if (mainPage.isCreateExample()) {
				return examplesPage;
			}
			else {
				return newProjectPage;
			}
		}
		else {
			return null;
		}
	}
	
	@Override
	public boolean canFinish() {
		if (mainPage.isCreateExample()) {
			return examplesPage.isPageComplete();
		}
		else {
			return newProjectPage.isPageComplete();
		}
	}
	
	@Override
	public boolean performFinish() {
		String projectName;
		String packageName;
		String mglModelName;
		Set<ExampleFeature> features = EnumSet.noneOf(ExampleFeature.class);
		
		if (mainPage.isCreateExample()) {
			projectName = examplesPage.getProjectName();
			packageName = examplesPage.getPackageName();
			mglModelName = examplesPage.getModelName();
			features.addAll(examplesPage.getSelectedFeatures());
		}
		else {
			projectName = newProjectPage.getProjectName();
			packageName = newProjectPage.getPackageName();
			mglModelName = newProjectPage.getModelName();
		}
		
		CincoProductProjectCreator projectCreator = new CincoProductProjectCreator(
				projectName, packageName, mglModelName, mglModelName + "Tool", mainPage.isCreateExample(), features);
		
		projectCreator.create();
		
		return true;
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("New Cinco Product Project");
	}
	
}

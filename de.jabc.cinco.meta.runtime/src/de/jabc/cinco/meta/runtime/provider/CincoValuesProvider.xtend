/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.provider

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.IdentifiableElement
import graphmodel.ModelElement
import java.util.Map

/** 
 * This class returns a map with possible "display" strings for an attribute of type <A>
 * @author kopetzki
 * @param<E> The {@link ModelElement} type which contains the attribute
 * @param<A> The {@link ModelElement}'s attribute type
 */
abstract class CincoValuesProvider<E extends IdentifiableElement, A extends Object> extends CincoRuntimeBaseClass {
	
	/** 
	 * @param modelElement The {@link ModelElement} containing the attribute
	 * @return The mapping from mgl.Attribute to its display value i.e. maps from attribute type to {@link String}
	 */
	def Map<A, String> getPossibleValues(E modelElement)

}

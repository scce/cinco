/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.hook

import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.graphiti.features.IFeatureProvider
import org.eclipse.graphiti.features.IUpdateFeature
import org.eclipse.graphiti.features.context.impl.UpdateContext
import org.eclipse.graphiti.mm.pictograms.Diagram
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import org.eclipse.graphiti.services.Graphiti
import org.eclipse.graphiti.ui.services.GraphitiUi

abstract class CincoPostCreateHook<T extends EObject> extends CincoRuntimeBaseClass {
	
	Diagram diagram

	def abstract void postCreate(T object)

	def void postCreateAndUpdate(T object, Diagram d) {
		this.diagram = d
		object.transact[
			postCreate(object)
			update(object)
		]
	}

	def private void update(T object) {
		var List<PictogramElement> linkedPictogramElements = Graphiti.getLinkService().
			getPictogramElements(getDiagram(), object)
		// if the element got deleted in the postCreateHook, linked elements will be empty
		if (linkedPictogramElements.isEmpty()) {
			return;
		}
		var UpdateContext uc = new UpdateContext((linkedPictogramElements.get(0) as PictogramElement))
		var IFeatureProvider provider = GraphitiUi.getExtensionManager().createFeatureProvider(getDiagram())
		var IUpdateFeature uf = provider.getUpdateFeature(uc)
		if(uf !== null && uf.canUpdate(uc)) uf.update(uc)
	}

	def Diagram getDiagram() {
		return diagram
	}

	def void setDiagram(Diagram diagram) {
		this.diagram = diagram
	}
}

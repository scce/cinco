/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.hook

import org.eclipse.emf.ecore.EObject
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.Direction

abstract class CincoPostResizeHook<T extends EObject> extends CincoRuntimeBaseClass {
	
	/**
	 * This method is called after resizing a graphical element on the canvas.
	 * @param modelElement The modelElement which is linked to the resized graphical representation.
	 * @param oldWidth The width before the resize operation.
	 * @param oldHeight The height before the resize operation.
	 * @param oldX The x-coordinate before the resize operation.
	 * @param oldY The y-coordinate before the resize operation.
	 * @param direction The direction of the resize operation.
	 */
	def void postResize(T modelElement, int oldWidth, int oldHeight, int oldX, int oldY, Direction direction)
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.layout;

import graphmodel.ModelElementContainer;
import graphmodel.Node;

/**
 * Exception to be thrown whenever a @Node that is supposed to be put into
 * a @Container cannot be put there.
 */
public class NodeCannotBeMovedToContainerException extends Exception {

	/** generated */
	private static final long serialVersionUID = -2979430956616063736L;
	
	private Node child;
	private ModelElementContainer parent;

	public NodeCannotBeMovedToContainerException(Node node, ModelElementContainer ModelElementContainer) {
		super();
		assign(node, ModelElementContainer);
	}

	public NodeCannotBeMovedToContainerException(String error, Node node, ModelElementContainer ModelElementContainer) {
		super(error);
		assign(node, ModelElementContainer);
	}

	public NodeCannotBeMovedToContainerException(Throwable cause, Node node,
			ModelElementContainer ModelElementContainer) {
		super(cause);
		assign(node, ModelElementContainer);
	}

	public NodeCannotBeMovedToContainerException(String error, Throwable cause, Node node,
			ModelElementContainer ModelElementContainer) {
		super(error, cause);
		assign(node, ModelElementContainer);
	}

	private void assign(Node node, ModelElementContainer ModelElementContainer) {
		child = node;
		parent = ModelElementContainer;
	}

	public Node getChild() {
		return child;
	}

	public ModelElementContainer getParent() {
		return parent;
	}
}

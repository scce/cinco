/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.layout;

import java.util.Map;

public enum LayoutConfiguration {

	DISABLE_RESIZE (0),
	
	MIN_WIDTH (120),
	MAX_WIDTH (-1),
	FIXED_WIDTH (-1),
	DISABLE_RESIZE_WIDTH (0),
	SHRINK_TO_CHILDREN_WIDTH (1),
	
	MIN_HEIGHT (24),
	MAX_HEIGHT (-1),
	FIXED_HEIGHT (-1),
	DISABLE_RESIZE_HEIGHT (0),
	SHRINK_TO_CHILDREN_HEIGHT (1),
	
	PADDING_X (5),
	PADDING_Y (5),
	PADDING_LEFT (5),
	PADDING_RIGHT (5),
	PADDING_TOP (5),
	PADDING_BOTTOM (5);
	
	public int defaultValue = 0;
	
	private LayoutConfiguration(int defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public int from(Map<LayoutConfiguration,?> config) {
		Object value = config.get(this);
		if (value == null) {
			return this.defaultValue;
		}
		return
			(value instanceof Integer)
			? (Integer) value
			: (value instanceof Boolean)
				? ((Boolean) value)
					? 1
					: 0
				: Integer.parseInt(value.toString());
	}
}

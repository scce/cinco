/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.xapi

import graphmodel.GraphModel
import graphmodel.internal.InternalGraphModel
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.transaction.RecordingCommand
import org.eclipse.emf.transaction.TransactionalEditingDomain
import org.eclipse.emf.transaction.util.TransactionUtil
import org.eclipse.graphiti.mm.pictograms.Diagram

/**
 * Resource-specific extension methods.
 * 
 * @author Steve Bosselmann
 */
class ResourceExtension extends de.jabc.cinco.meta.util.xapi.ResourceExtension {
	
	/**
	 * Retrieves the contained diagram.
	 * It is assumed that a diagram exists and is placed at the default location
	 * (i.e. the first content object of the resource).
	 * However, if the content object at this index is not a diagram all content
	 * objects are searched through for diagrams and the first occurrence is
	 * returned, if existent.
	 * 
	 * Convenience method for {@code getContent(Diagram.class, 0)}.
	 * 
	 * @return The diagram, or {@code null} if not existent.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def Diagram getDiagram(Resource resource) {
		getContent(resource, Diagram, 0)
	}
	
	/**
	 * Retrieves the contained graph model.
	 * It is assumed that a graph model exists and is placed at the default location
	 * (i.e. the second content object of the resource).
	 * However, if the content object at this index is not a graph model all content
	 * objects are searched through for graph models and the first occurrence is
	 * returned, if existent.
	 * 
	 * Convenience method for {@code getContent(GraphModel.class, 1)}.
	 * 
	 * @return The graph model, or {@code null} if not existent.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def getGraphModel(Resource resource) {
		getContent(resource, InternalGraphModel, 1)?.element
	}
	
	/**
	 * Returns {@code true} if the resource contains a graph model,
	 * {@code false} otherwise.
	 */
	def containsGraphModel(Resource resource) {
		resource.graphModel !== null
	}
	
	/**
	 * Retrieves the contained graph model of the specified type.
	 * It is assumed that a graph model of the specified type exists and is placed
	 * at the default location (i.e. the second content object of the resource).
	 * However, if the content object at this index is not a graph model of the
	 * specified type all content objects are searched through for graph models
	 * of that type and the first occurrence is returned, if existent.
	 * 
	 * Convenience method for {@code getContent(<ModelClass>, 1)}.
	 * 
	 * @return The diagram, or {@code null} if the resource does not contain any
	 *   graph model of the specified type.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def <T extends GraphModel> getGraphModel(Resource resource, Class<T> modelClass) {
		val model = resource.getGraphModel
		if (model?.eClass?.name == modelClass.simpleName) {
			return model as T
		}
	}
	
	/**
	 * Returns {@code true} if the resource contains a graph model of the specified
	 * type, {@code false} otherwise.
	 */
	def <T extends GraphModel> containsGraphModel(Resource resource, Class<T> modelClass) {
		resource.getGraphModel(modelClass) !== null
	}
	
	/**
	 * Convenient method to wrap a modification of a {@link Resource} in a
	 * {@link RecordingCommand}.
	 * Retrieves a {@link TransactionalEditingDomain} for the specified object
	 * via {@link TransactionUtil#getEditingDomain(EObject)}. If none is found,
	 * a new one is created.
	 * 
	 * @param resource to be modified.
	 * @param runnable that performs the actual modification.
	 */
	def transact(Resource resource, Runnable runnable) {
		transact(resource, null, runnable)
	}
	
	/**
	 * Convenient method to wrap a modification of a {@link Resource} in a
	 * {@link RecordingCommand}.
	 * Retrieves a {@link TransactionalEditingDomain} for the specified object
	 * via {@link TransactionUtil#getEditingDomain(EObject)}. If none is found,
	 * a new one is created.
	 * 
	 * @param resource to be modified.
	 * @param label of the command.
	 * @param runnable that performs the actual modification.
	 */
	def transact(Resource resource, String label, Runnable runnable) {
		val domain = resource.editingDomain
		domain.commandStack.execute(new RecordingCommand(domain) {
			override protected doExecute() {
				try { runnable.run } catch(IllegalStateException e) {
					e.printStackTrace
				}
			}
		} => [
			setLabel(label)
		])
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.xapi

import graphmodel.GraphModel
import graphmodel.internal.InternalGraphModel
import java.util.NoSuchElementException
import org.eclipse.core.resources.IFile
import org.eclipse.graphiti.mm.pictograms.Diagram

/**
 * File-specific extension methods.
 * 
 * @author Steve Bosselmann
 */
class FileExtension extends de.jabc.cinco.meta.util.xapi.FileExtension {
	
	/**
	 * Creates the resource for this file and retrieves the contained diagram.
	 * It is assumed that a diagram exists and is placed at the default location
	 * (i.e. the first content object of the resource).
	 * However, if the content object at this index is not a diagram all content
	 * objects are searched through for diagrams and the first occurrence is
	 * returned, if existent.
	 * 
	 * Convenience method for {@code getContent(Diagram.class, 0)}.
	 * 
	 * @return The diagram, or {@code null} if not existent.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def getDiagram(IFile file) {
		getContent(file, Diagram, 0)
	}
	
	/**
	 * Creates the resource for this file and retrieves the contained graph model.
	 * It is assumed that a graph model exists and is placed at the default location
	 * (i.e. the second content object of the resource).
	 * However, if the content object at this index is not a graph model all content
	 * objects are searched through for graph models and the first occurrence is
	 * returned, if existent.
	 * 
	 * Convenience method for {@code getContent(GraphModel.class, 1)}.
	 * 
	 * @return The graph model, or {@code null} if not existent.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def getGraphModel(IFile file) {
		getContent(file, InternalGraphModel, 1).element
	}
	
	/**
	 * Creates the resource for this file and retrieves the contained graph model
	 * of the specified type.
	 * It is assumed that a graph model of the specified type exists and is placed
	 * at the default location (i.e. the second content object of the resource).
	 * However, if the content object at this index is not a graph model of the
	 * specified type all content objects are searched through for graph models
	 * of that type and the first occurrence is returned, if existent.
	 * 
	 * Convenience method for {@code getContent(<ModelClass>, 1)}.
	 * 
	 * @return The diagram, or {@code null} if the resource does not contain any
	 *   graph model of the specified type.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def <T extends GraphModel> getGraphModel(IFile file, Class<T> modelClass) {
		val model = file.getGraphModel
		if (model.eClass.name == modelClass.simpleName) {
			return model as T
		} else throw new NoSuchElementException(
			"No model of type " + modelClass + " found in " + file)
	}
	
	/**
     * Returns the time that the file denoted by this abstract pathname was
     * last modified.
     * <p>
     * <b>Note:</b><br>
     * While the unit of time of the return value is milliseconds, the
     * granularity of the value depends on the underlying file system and may
     * be larger.  For example, some file systems use time stamps in units of
     * seconds.
     *
     * @return  A <code>long</code> value representing the time the file was
     *          last modified, measured in milliseconds since the epoch
     *          (00:00:00 GMT, January 1, 1970), or <code>0L</code> if the
     *          file is <code>null</code>, does not exist or if an I/O error
     *          occurs. The value may be negative indicating the number of
     *          milliseconds before the epoch.
     */
	def long lastModified(IFile file) {
		try {
			val ioFile = file?.rawLocation?.makeAbsolute?.toFile
			if (ioFile !== null) {
				return ioFile.lastModified
			}
		}
		catch (Exception e) {
			e.printStackTrace
		}
		return 0L
	}
	
}

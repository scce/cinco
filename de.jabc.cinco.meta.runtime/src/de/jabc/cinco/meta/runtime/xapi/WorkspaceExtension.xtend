/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.runtime.xapi

import graphmodel.GraphModel
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IContainer

/**
 * Workspace-specific extension methods.
 * 
 * @author Steve Bosselmann
 */
class WorkspaceExtension extends de.jabc.cinco.meta.util.xapi.WorkspaceExtension {
	
	/**
	 * Retrieves all files in the container, creates the resource for each of them
	 * and retrieves the contained graph model.
	 * It is assumed that a graph model exists and is placed at the default location
	 * (i.e. the second content object of the resource).
	 * However, if the content object at this index is not a graph model all content
	 * objects are searched through for graph models and the first occurrence is
	 * returned, if existent.
	 * 
	 * Only recurses through accessible sub-containers (e.g. open projects).
	 * 
	 * @throws NoSuchElementException if the resource does not contain any graph model.
	 * @throws RuntimeException if accessing the resource failed.
	 * @deprecated use {@link de.jabc.cinco.meta.core.referenceregistry.ReferenceRegistry#lookup()} instead 
	 * or filter by {@link de.jabc.cinco.meta.util.xapi.WorkspaceExtension#getWorkspaceRoot()}: <br>
	 * 1. <strong>recommended</strong>: ReferenceRegistry.instance.lookup(GraphModel) <br>
	 * 2. <strong>file-based fallback</strong>: (new WorkspaceExtension).getWorkspaceRoot.files.map[getGraphModel].filterNull
	 */
	@Deprecated
	def getGraphModels(IContainer container) {
		extension val FileExtension = new FileExtension
		container.files.map[
			try {
				getGraphModel
			} catch (Exception e) {}
		].filterNull
	}
	
	/**
	 * Creates the resource for the file and retrieves the contained graph model
	 * of the specified type.
	 * It is assumed that a graph model of the specified type exists and is placed
	 * at the default location (i.e. the second content object of the resource).
	 * However, if the content object at this index is not a graph model of the
	 * specified type all content objects are searched through for graph models
	 * of that type and the first occurrence is returned, if existent.
	 * 
	 * @throws NoSuchElementException if the resource does not contain any graph
	 *   model of the specified type.
	 * @throws RuntimeException if accessing the resource failed.
	 */
	def <T extends GraphModel> getGraphModel(IFile file, Class<T> modelClass) {
		extension val FileExtension = new FileExtension
		file.getGraphModel(modelClass)
	}
}

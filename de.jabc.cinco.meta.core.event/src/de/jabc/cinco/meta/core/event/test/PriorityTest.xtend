/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.event.test

import de.jabc.cinco.meta.core.event.hub.Priority
import java.util.List
import java.util.concurrent.ThreadLocalRandom
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.*

/**
 * Unit test (JUnit 5) for {@link Priority}.
 * @author Fabian Storek
 */
class PriorityTest {

	val static RANGE = 100 / 3
	
	@Test
	def void testOrder() {
		
		val List<Priority> list = newArrayList
		
		list.addRandom
		list.add(new Priority(Integer.MAX_VALUE))
		list.addRandom
		list.add(new Priority(Integer.MIN_VALUE))
		list.addRandom
		
		val sortedList = list.sort
		
		for (i: 0 ..< sortedList.size - 1) {
			
			val current = sortedList.get(i)
			val next    = sortedList.get(i + 1)
			
			assertTrue(
				(current.major < next.major) ||
				(current.major == next.major && current.minor <= next.minor)
			)
			
		}
		
	}
	
	def private void addRandom(List<Priority> list) {
		for (i: 0 ..< RANGE) {
			val random = ThreadLocalRandom.current.nextInt(-RANGE, RANGE)
			list.add(new Priority(random))
		}
	}
		
}

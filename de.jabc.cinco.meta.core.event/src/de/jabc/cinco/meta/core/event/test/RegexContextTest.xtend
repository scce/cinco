/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.event.test

import de.jabc.cinco.meta.core.event.hub.impl.RegexContext
import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.*

/**
 * Unit test (JUnit 5) for {@link RegexContext}.
 * @author Fabian Storek
 */
class RegexContextTest {
	
	@Test
	def void testMatches() {
		
		val hello_world      = new RegexContext('hello.*world')
		val helloWorld       = new RegexContext('helloworld')
		val helloDisneyWorld = new RegexContext('hello disney-world')
		
		assertTrue (hello_world.matches(helloWorld))
		assertTrue (hello_world.matches(helloDisneyWorld))
		assertFalse(helloWorld.matches(helloDisneyWorld))

		assertTrue (helloWorld.matches(hello_world))
		assertTrue (helloDisneyWorld.matches(hello_world))
		assertFalse(helloDisneyWorld.matches(helloWorld))
		
	}
	
}

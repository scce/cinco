/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.event.hub

/**
 * An interface for context definitions.
 * <p>
 * Use the {@link Context#matches(Context) matches(Context)} method (or its
 * syntactic sugar {@linkplain Context#operator_spaceship(Context) <=>}) for a
 * semantic comparison.
 * <p>
 * {@link CompositeContext} is an implementation for this interface.
 * <p>
 * <b>Implementing this interface:</b>
 * <p>
 * <ul>
 * <li>{@link Context#matches(Context) matches(Context)}</li>
 * <ul>
 * <li>Returns a boolean weather or not this Context matches another Context.</li>
 * <li>Contexts, that are considered {@linkplain Object#equals(Object) equal}, must match.</li>
 * <li>The {@link Context#ANY ANY} Context must match every other Context.</li>
 * <li>This relation must be reflexive and symmetric.</li>
 * </ul>
 * </ul>
 * 
 * @author Fabian Storek
 */
interface Context {
	
	/**
	 * The <i>any</i> Context matches every Context.
	 */
	val Context ANY = new Context {
		
		override matches(Context other) {
			true
		}
		
		override toString() {
			'Context.ANY'
		}
		
	}
	
	/**
	 * Semantic comparison to another Context.
	 * <p>
	 * <b>Implementing this method:</b>
	 * <p>
	 * <ul>
	 * <li>Returns a boolean weather or not this Context matches another Context.</li>
	 * <li>Contexts, that are considered {@linkplain Object#equals(Object) equal}, must match.</li>
	 * <li>The {@link Context#ANY ANY} Context must match every other Context.</li>
	 * <li>This relation must be reflexive and symmetric.</li>
	 * </ul>
	 * 
	 * @param other The <i>other</i> Context this Context will be compared to.
	 * @return <ul>
	 *         <li>{@code true}, if this Context matches the other Context.</li>
	 *         <li>{@code true}, if this and the other Context are equal or identical.</li>
	 *         <li>{@code true}, if this or the other Context is the {@link Context#ANY ANY} Context.</li>
	 *         <li>{@code false}, if this Context does not match the other Context.</li>
	 *         </ul>
	 */
	def boolean matches(Context other)
	
}

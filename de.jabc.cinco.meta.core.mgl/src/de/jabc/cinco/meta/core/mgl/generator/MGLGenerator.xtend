/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator

import de.jabc.cinco.meta.core.pluginregistry.PluginRegistry
import de.jabc.cinco.meta.core.utils.GeneratorHelper
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.URIHandler
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Set
import mgl.Annotation
import mgl.MGLModel
import mgl.UserDefinedType
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.Path
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EModelElement
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.XMIResource
import productDefinition.CincoProduct

import static extension de.jabc.cinco.meta.core.utils.MGLUtil.*

class MGLGenerator {
	
	val HashMap<EModelElement, MGLModel> packageMGLModelMap = newHashMap
	
	synchronized def doGenerateEcoreModels(List<MGLModel> allMGLModels, Set<MGLModel> generateMGLModels, Map<MGLModel, Pair<Resource, Resource>> importedExternalMGLs, CincoProduct cp){
		
		val preparedModels = prepareMglModels(generateMGLModels);
		GeneratorUtils.instance.allMGLs = preparedModels;
		val mglEcoreGenerator = new MGLEcoreGenerator
		
		val newImportedExternalMGLs = new HashMap<MGLModel, Resource>()
		importedExternalMGLs.entrySet.forEach[iem | newImportedExternalMGLs.put(iem.key, iem.value.key)]
		val rootEPackage = mglEcoreGenerator.generateEcoreModels(preparedModels, newImportedExternalMGLs, packageMGLModelMap, cp)
		
		generateMGLModels.forEach[
			val mglModelElement = (MGLUtil.modelElements(it) + it.types.filter[type | type instanceof UserDefinedType]).toList
			new MGLFactoryGenerator().generateFactory(it, mglEcoreGenerator.allElementEClasses.filter[elementEClass | mglModelElement.contains(elementEClass.modelElement)])
			new MGLAdaptersGenerator().generateAdapters(it)
		]
		
		// Generate GenModels and model code for generated Ecore files
		saveEcoreModel(rootEPackage, cp)
		val externalGenmodels = importedExternalMGLs.entrySet.map[value.value].toList
		new GenModelDescription(rootEPackage, cp, externalGenmodels).generateGenModelCode
		
		// Activate MGL meta plugins
		generateMGLMetaPlugins(allMGLModels, generateMGLModels.toList, cp, rootEPackage)
	}
	
	def retrievePersistedImportedExternalMGLEClasses(Set<MGLModel> importedExternalMGLs) {
		val resourceSet = new ResourceSetImpl()
		val uri = URI.createURI("test")
		val resource = resourceSet.createResource(uri)
		resource.load(null)
	}
	
	def generateGenModelCode(GenModelDescription it) {
		genModel = generateGenModel(
			model,
			ecorePath,
			EPackage,projectName,
			projectID, projectPath,
			it.externalGenmodels.flatMap[allContents.toIterable.filter(typeof(GenPackage))].toList
		)
		saveGenModel(genModel, model)
		GeneratorHelper.generateGenModelCode(genModel)
	}
	
	def saveEcoreModel(EPackage ePackage, CincoProduct cp) {
		EPackage.Registry.INSTANCE.put(ePackage.nsURI, ePackage)
		var outPath = ProjectCreator.getProject(cp.eResource).fullPath.append(
			"/src-gen/model/" +  cp.name + ".ecore")
		var uri = URI::createURI(outPath.toString)
		val modelResource = cp.eResource
		var ePackageResourceSet = modelResource.resourceSet
		if( ePackageResourceSet === null){
			ePackageResourceSet = new ResourceSetImpl()
		}
		var ePackageResource = ePackageResourceSet.createResource(uri)
		ePackageResource.contents += ePackage
		var optionMap = new HashMap<String, Object>
		optionMap.put(XMIResource.OPTION_URI_HANDLER, new URIHandler(ePackage))
		ePackageResource.save(optionMap)
	}
	
	def saveGenModel(GenModel genModel, CincoProduct model) {
		var outPath = ProjectCreator.getProject(model.eResource).fullPath.append(
			"/src-gen/model/" + model.name + ".genmodel")
		var uri = URI::createURI(outPath.toString)
		var genModelResource = model.eResource.resourceSet.createResource(uri)
		genModelResource.contents.add(genModel)
		genModelResource.save(null)
	}
	
	/**
	 * Collects the MGL meta plugins executes them, if any MGL or their
	 * imported/referenced MGLs contain a suitable annotation.
	 */
	def private static void generateMGLMetaPlugins(List<MGLModel> allMGLs, List<MGLModel> generatedMGLs, CincoProduct cpd, EPackage rootEPackage) {
		val mglMetaPluginGenerators = PluginRegistry.instance.pluginGenerators
		if (mglMetaPluginGenerators === null || mglMetaPluginGenerators.empty) {
			return
		}
		val allAnnotations = allMGLs.flatMap[eAllContents.filter(Annotation).toList]
		val project = new ResourceExtension().getProject(cpd.eResource)
		val ePackages = generatedMGLs.toInvertedMap [ mgl |
			rootEPackage.ESubpackages.findFirst[nsURI == mgl.nsURI]
		]
		mglMetaPluginGenerators
			.entrySet
			.sortWith[ l, r |
				val priorityDiff = -(l.value.comparePriorityTo(r.value)) // Highest priority first
				if (priorityDiff == 0) {
					return l.key.compareToIgnoreCase(r.key) // Lexicographical order
				}
				else {
					return priorityDiff
				}
			]
			.forEach [ entry |
				val mglMetaPluginName = entry.key
				val mglMetaPlugin = entry.value
				val relatedAnnotations = allAnnotations.filter[annotation | mglMetaPluginName == annotation.name].toList
				if (!relatedAnnotations.nullOrEmpty) {
					System.out.println(
						"Executing MGL meta plugin: " + mglMetaPluginName +
						" (Priority: " + mglMetaPlugin.getMGLMetaPluginPriority + ")"
					);
					mglMetaPlugin.executeMGLMetaPlugin(relatedAnnotations, generatedMGLs, allMGLs, cpd, project, ePackages)
				}
			]
	}
	
	def generateGenModel(CincoProduct model, String ecorePath, EPackage ePackage, String projectName, String projectID, IPath projectPath, List<GenPackage> externalGenPackages){
		val genModel = GenModelCreator::createGenModel(new Path(ecorePath), ePackage, projectName, projectID,
			projectPath)
		val genResource = ePackage.eResource.resourceSet.createResource(
			URI.createURI("platform:/plugin/de.jabc.cinco.meta.core.mgl.model/model/GraphModel.genmodel"))
		genResource.load(null)
		val graphModelGenModel = (genResource.contents.get(0) as GenModel)
		genModel.usedGenPackages += graphModelGenModel.genPackages
		genModel.usedGenPackages.addAll(externalGenPackages)
		genModel.operationReflection = true
		for (genPackage : genModel.genPackages) {
			genPackage.basePackage = "info.scce.cinco.product"
			for(nestedGenPackage : genPackage.nestedGenPackages) {
				nestedGenPackage.basePackage = packageMGLModelMap.get(nestedGenPackage.ecoreModelElement).package
			}
		}
		return genModel
	}
	
}



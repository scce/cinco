/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.generator

import org.eclipse.core.internal.runtime.InternalPlatform
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.core.runtime.Path
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.pde.core.project.IBundleProjectService
import productDefinition.CincoProduct
import java.util.List
import de.jabc.cinco.meta.util.xapi.ResourceExtension
import org.eclipse.jdt.internal.core.ProjectEntry

class GenModelDescription{	
	String ecorePath
	Resource resource
	URI resourceUri
	String filePath
	String projectName
	Path projectPath
	CincoProduct model
	EPackage ePackage
	String projectID
	GenModel genModel
	List<Resource> externalGenmodels

	new(EPackage ePackage, CincoProduct cp, List<Resource> externalGenmodels){
		this.model = cp
		ecorePath = model.name + ".ecore".toFirstUpper
		this.ePackage = ePackage
		resource = model.eResource
		resourceUri = resource.URI
		filePath = resourceUri.toPlatformString(true)
		val iFile = ResourcesPlugin.workspace.root.getFile(new Path(filePath))
		projectName = iFile.project.name
		this.externalGenmodels = externalGenmodels
		
		projectPath = new Path(projectName)
		
		//var bc = InternalPlatform::getDefault().getBundleContext();
		//println(bc)
		//println(IBundleProjectService.name)
		//var ref = bc.getServiceReference(IBundleProjectService.name);
		//println(ref)
		//var service = bc.getService(ref) as IBundleProjectService
		//var bpd = service.getDescription(iFile.project);
		//val re = new ResourceExtension()
		//val project = re.project(res)
		projectID = projectName
		//bpd.symbolicName
		//bc.ungetService(ref);
	}
	
	def getEcorePath(){ecorePath}
	def getResource(){resource}
	def getResourceUri(){resourceUri}
	def getFilePath(){filePath}
	def getProjectName(){projectName}
	def getProjectPath(){projectPath}
	def getModel(){model}
	def getEPackage(){ePackage}
	def getProjectID(){projectID}
	def getExternalGenmodels(){externalGenmodels}
	
	def setGenModel(GenModel gm){
		this.genModel = gm
	}
	def getGenModel(){genModel}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
/*
 * generated by Xtext
 */
package de.jabc.cinco.meta.core.mgl.scoping

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.MGLUtil
import java.net.URL
import java.util.ArrayList
import mgl.ComplexAttribute
import mgl.ContainingElement
import mgl.Edge
import mgl.EdgeElementConnection
import mgl.GraphModel
import mgl.GraphicalElementContainment
import mgl.Import
import mgl.MGLModel
import mgl.MglPackage
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.PrimitiveAttribute
import mgl.ReferencedAttribute
import mgl.ReferencedEClass
import mgl.ReferencedModelElement
import mgl.ReferencedType
import mgl.Type
import mgl.UserDefinedType
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.Scopes
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 *
 */
class MGLScopeProvider extends AbstractDeclarativeScopeProvider {
	
	override IScope getScope(EObject eobj, EReference ref){
		if (eobj instanceof ReferencedAttribute<?> && (ref === MglPackage.eINSTANCE.referencedEClass_Type || ref === MglPackage.eINSTANCE.referencedModelElement_Type)) {
			return scope_ReferencedAttribute_feature((eobj as ReferencedAttribute<?>), ref)
		}
		else if (eobj instanceof ReferencedType) {
			return scope_ReferencedType_type(eobj, ref)
		}
		else if (eobj instanceof ComplexAttribute) {
			return scope_ComplexAttribute_type(eobj, ref)
		}
		else if (eobj instanceof ModelElement && ref === MglPackage.eINSTANCE.defaultValueOverride_Attribute) {
			return scope_ModelElement_defaultValueOverrides_attribute(eobj as ModelElement, ref)
		}
		return super.getScope(eobj, ref)?: IScope.NULLSCOPE
	}
	
	def IScope scope_ReferencedAttribute_feature(ReferencedAttribute<?> attr, EReference ref){
		var scope = IScope.NULLSCOPE
		
		if(attr.referencedType instanceof ReferencedEClass)
			scope = Scopes.scopeFor((attr.referencedType as ReferencedEClass)?.type.EAllStructuralFeatures)
		else
			scope = Scopes.scopeFor((attr.referencedType as ReferencedModelElement).type.attributes)
			
		if(scope === null){
			return IScope.NULLSCOPE	
		}else{
			return scope	
		}
		
	}
	
	def IScope scope_ReferencedEClass_type(ReferencedEClass refType,EReference ref){
		var scope = IScope.NULLSCOPE
			var res = null as Resource
				try{
					res = CincoUtil::getResource(refType.imprt.importURI, refType.eResource)
				}catch(Exception e){
					return null;
				}
			if(res!==null){
				scope = Scopes.scopeFor(res.allContents.toList.filter[d| d instanceof EClass])
			}
		
		if(scope === null){
			return IScope.NULLSCOPE	
		}else{
			return scope	
		}
		
	}
	
	def IScope scope_ReferencedModelElement_type(ReferencedModelElement refType,EReference ref){
 		var scope = IScope.NULLSCOPE
			if(refType.local){
				var me = refType.eContainer as ModelElement
				var mglModel = MGLUtil::mglModel(me)
				var types = new ArrayList<Type>()
				types += mglModel.types.unmodifiableView + mglModel.nodes.unmodifiableView + mglModel.edges.unmodifiableView
				types += mglModel.graphModels
				
				scope = Scopes.scopeFor(types)
			}else{
				var res = null as Resource
				try{
					res = CincoUtil::getResource(refType.imprt.importURI, refType.eResource)
				}catch(Exception e){
					return scope
				}
				
				if(res!==null){

					scope = Scopes.scopeFor(res.allContents.toList.filter[d| d instanceof ModelElement])
						
				}
			}
		if(scope === null){
			return IScope.NULLSCOPE	
		}else{
			return scope	
		}
		
	}
	
	def IScope scope_GraphModel_extends(GraphModel graphModel, EReference ref){
 		var scope = IScope.NULLSCOPE
			if(graphModel.imprt === null){
				var mglModel = graphModel.eContainer as MGLModel
				var graphModels = new ArrayList<GraphModel>()
				graphModels += mglModel.graphModels
				
				scope = Scopes.scopeFor(graphModels)
			}else{
				var res = null as Resource
				try{
					res = CincoUtil::getResource(graphModel.imprt.importURI, graphModel.eResource)
				}catch(Exception e){
					return null;
				}
				
				if(res!==null){
					scope = Scopes.scopeFor(res.allContents.toList.filter[d| d instanceof GraphModel])	
				}
			}
		if(scope === null){
			return IScope.NULLSCOPE	
		}else{
			return scope	
		}
	}
	
	def IScope scope_Node_extends(Node node, EReference ref){
		if(node instanceof NodeContainer) {
			return scope_NodeContainer_extends(node, ref)
		} else {
			var scope = IScope.NULLSCOPE
			if(node.imprt === null){
				var mglModel = node.eContainer as MGLModel
				var nodes = new ArrayList<Node>()
				nodes += mglModel.nodes.filter[!(it instanceof NodeContainer)]
				
				scope = Scopes.scopeFor(nodes)
			}else{
				var res = null as Resource
				try{
					res = CincoUtil::getResource(node.imprt.importURI, node.eResource)
				}catch(Exception e){
					return null;
				}
				
				if(res!==null){
					scope = Scopes.scopeFor(res.allContents.toList.filter[it instanceof Node && !(it instanceof NodeContainer)])
				}
			}
			if(scope === null){
				return IScope.NULLSCOPE	
			}else{
				return scope	
			}
		}
	}
	
	def IScope scope_NodeContainer_extends(NodeContainer nodeContainer, EReference ref){
 		var scope = IScope.NULLSCOPE
			if(nodeContainer.imprt === null){
				var mglModel = nodeContainer.eContainer as MGLModel
				var nodes = new ArrayList<Node>()
				nodes += mglModel.nodes
				
				scope = Scopes.scopeFor(nodes)
			}else{
				var res = null as Resource
				try{
					res = CincoUtil::getResource(nodeContainer.imprt.importURI, nodeContainer.eResource)
				}catch(Exception e){
					return null;
				}
				
				if(res!==null){
					scope = Scopes.scopeFor(res.allContents.toList.filter[d| d instanceof Node])	
				}
			}
		if(scope === null){
			return IScope.NULLSCOPE	
		}else{
			return scope	
		}
	}
	
	def IScope scope_Edge_extends(Edge edge, EReference ref){
 		var scope = IScope.NULLSCOPE
			if(edge.imprt === null){
				var mglModel = edge.eContainer as MGLModel
				var edges = new ArrayList<Edge>()
				edges += mglModel.edges
				
				scope = Scopes.scopeFor(edges)
			}else{
				var res = null as Resource
				try{
					res = CincoUtil::getResource(edge.imprt.importURI, edge.eResource)
				}catch(Exception e){
					return null;
				}
				
				if(res!==null){
					scope = Scopes.scopeFor(res.allContents.toList.filter[d| d instanceof Edge])	
				}
			}
		if(scope === null){
			return IScope.NULLSCOPE	
		}else{
			return scope	
		}
	}
	
	def IScope scope_ModelElement_imprt(ModelElement modelElement,EReference ref){
		var mglModel = modelElement.eContainer as MGLModel
		var imports = new ArrayList<Import>()
		imports += mglModel.imports
		
		return Scopes.scopeFor(imports) ?: IScope.NULLSCOPE
	}
	
	def IScope scope_GraphicalElementContainment_types(GraphicalElementContainment graphicalElementContainment, EReference ref) {
		val mglModel = graphicalElementContainment.MGLModel
		val referencedImport = graphicalElementContainment.referencedImport
		if(referencedImport === null) {
			return Scopes.scopeFor(mglModel.nodes)
		} else {
			var res = null as Resource
			try {
				res = CincoUtil::getResource(referencedImport.importURI, referencedImport.eResource)
			} catch(Exception e) {
				return null;
			}
			
			if(res !== null) {
				return Scopes.scopeFor(res.allContents.toList.filter(Node))
			}
		}
		return IScope.NULLSCOPE
	}
	
	def MGLModel getMGLModel(GraphicalElementContainment graphicalElementContainment) {
		return graphicalElementContainment.eContainer.eContainer as MGLModel
	}

	def dispatch GraphModel getGraphModel(ModelElement element){
		switch(element){
			GraphModel: return element
			default: return element.eContainer as GraphModel
		}
	}
	
	def dispatch getGraphModel(ContainingElement ce){
		switch(ce){
			GraphModel: return ce
			default: return ce.eContainer as GraphModel
		}
	}
	
	def IScope scope_ReferencedType_type(ReferencedType refType,EReference ref){
		var scope = IScope.NULLSCOPE
		if(refType instanceof ReferencedModelElement && ref==MglPackage.eINSTANCE.referencedModelElement_Type){
			scope =  scope_ReferencedModelElement_type(refType as ReferencedModelElement,ref)
			}else if(refType instanceof ReferencedEClass && ref == MglPackage.eINSTANCE.referencedEClass_Type){
			scope =  scope_ReferencedEClass_type(refType as ReferencedEClass,ref)
		}else{
			scope = super.getScope(refType,ref)
			 
		}
		if(scope === null){
			return IScope.NULLSCOPE
		}else{
			return scope
		}
	}
	
	def IScope scope_ComplexAttribute_type(ComplexAttribute it, EReference ref){
		val mglModel = MGLUtil::mglModel(it.modelElement)
		Scopes.scopeFor(mglModel.nodes+mglModel.types+mglModel.edges+mglModel.graphModels)
	}
	
	def IScope scope_EdgeElementConnection_connectingEdges(EdgeElementConnection eec, EReference ref){
		val mglModel = MGLUtil::mglModel(eec)
		val referencedImport = eec.referencedImport
		if(referencedImport === null) {
			return Scopes.scopeFor(mglModel.edges)
		} else {
			var res = null as Resource
			try {
				res = CincoUtil::getResource(referencedImport.importURI, referencedImport.eResource)
			} catch(Exception e) {
				return null;
			}
			
			if(res !== null) {
				return Scopes.scopeFor(res.allContents.toList.filter(Edge))
			}
		}
		return IScope.NULLSCOPE
	}
	
	def loadResource(String uri){
		try{
			var url = new URL(uri);
			return url.openConnection.inputStream
		
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	def IScope scope_ModelElement_defaultValueOverrides_attribute(ModelElement me, EReference ref) {
		val list = newLinkedList
		var currentElement = me.extends
		while (currentElement !== null) {
			list += currentElement.attributes.filter(PrimitiveAttribute)
			currentElement = currentElement.extends
		}
		return Scopes.scopeFor(list)?: IScope.NULLSCOPE
	}
	
	private def getExtends(ModelElement element) {
		switch element {
			Node:            element.extends
			Edge:            element.extends
			UserDefinedType: element.extends
			GraphModel:      element.extends
		}
	}
	
}

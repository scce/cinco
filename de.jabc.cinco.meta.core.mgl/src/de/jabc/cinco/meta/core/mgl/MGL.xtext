/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
grammar de.jabc.cinco.meta.core.mgl.MGL with org.eclipse.xtext.common.Terminals

import "http://www.jabc.de/cinco/meta/core/mgl"
import "http://www.eclipse.org/emf/2002/Ecore" as ecore



// Root element

MGLModel returns MGLModel:
	(imports+=Import)*
	(annotations+=Annotation)*
	'id' package=QName
	'stylePath' stylePath=STRING
	(
		(graphModels+=GraphModel)
		| (nodes+=Node)
		| (edges+=Edge)
		| (nodes+=NodeContainer)
		| (types+=Type)
	)*
;



// Model elements

GraphModel returns GraphModel:
	(annotations+=Annotation)*
	(isAbstract?='abstract' )?
	'graphModel' name=EString ('extends' (imprt=[Import|QName] '::')? extends=[GraphModel|EString])?
	('{'(
		('iconPath' iconPath=EString)?
		& ('diagramExtension' fileExtension=EString)?
		& ('containableElements' '(' ((containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard) (',' (containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard))*) ')' )?
		& (attributes+=Attribute | defaultValueOverrides+=DefaultValueOverride)*
	)'}')?
;

GraphicalModelElement:
	Edge
	| Node
	| NodeContainer
;

Edge returns Edge:
	{Edge}
	(annotations+=Annotation)*
	(isAbstract?='abstract')?
	'edge' name=EString ('extends' (imprt=[Import|QName] '::')? extends=[Edge|EString])?
	('{'(
		('style' usedStyle=ID ('(' styleParameters+=EString (',' styleParameters+=EString)* ')')? )?
		& (attributes+=Attribute | defaultValueOverrides+=DefaultValueOverride)*
	)'}')?
;

Node returns Node:
	{Node}
	(annotations+=Annotation)*
	(isAbstract?='abstract')?
	'node' name=EString ('extends' (imprt=[Import|QName]'::')? extends=[Node|EString])?
	('{'(
		('style' usedStyle=ID ('(' styleParameters+=EString (',' styleParameters+=EString)* ')')? )?
		& ('incomingEdges' '(' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard) (',' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard))* ')')?
		& ('outgoingEdges' '(' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard) (',' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard))* ')')?
		& (primeReference=(ReferencedEClass|ReferencedModelElement))?
		& (attributes+=Attribute | defaultValueOverrides+=DefaultValueOverride)*
	)'}')?
;

NodeContainer returns NodeContainer:
	{NodeContainer}
	(annotations+=Annotation)*
	(isAbstract?='abstract')?
	'container' name=EString ('extends' (imprt=[Import|QName] '::')? extends=[NodeContainer|EString])?
	('{'(
		('style' usedStyle=ID ('(' styleParameters+=EString (',' styleParameters+=EString)* ')')? )?
		& ('incomingEdges' '(' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard) (',' (incomingEdgeConnections+=IncomingEdgeElementConnection | incomingWildcards+=Wildcard))* ')')?
		& ('outgoingEdges' '(' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard) (',' (outgoingEdgeConnections+=OutgoingEdgeElementConnection | outgoingWildcards+=Wildcard))* ')')?
		& ('containableElements' '(' ((containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard) (',' (containableElements+=GraphicalElementContainment | containmentWildcards+=Wildcard))*) ')')?
		& (primeReference=(ReferencedEClass|ReferencedModelElement))?
		& (attributes+=Attribute | defaultValueOverrides+=DefaultValueOverride)*
	)'}')?
;

Type:
	Enum
	| UserDefinedType
;

Enum returns Enumeration:
	{Enumeration}
	(annotations+=Annotation)*
	'enum' name=EString
	'{'
		(literals+=EString)+
	'}'
;

UserDefinedType returns UserDefinedType:
	{UserDefinedType}
	(annotations+=Annotation)*
	(isAbstract?='abstract')?
	'type' name=EString ('extends' (imprt=[Import|QName] '::')? extends=[UserDefinedType|EString])?
	('{'(
		(attributes+=Attribute | defaultValueOverrides+=DefaultValueOverride)*
	)'}')?
;



// Attributes

Attribute returns Attribute:
	PrimitiveAttribute
	| ComplexAttribute
;

PrimitiveAttribute returns PrimitiveAttribute:
	(annotations+=Annotation)*
	(notChangeable?='final')? (unique?='unique')? 'attr' type=EDataTypeType 'as' name=EString ('[' lowerBound=EInt (',' upperBound=BoundValue)? ']')? (':'? '=' defaultValue=EString)?
;

ComplexAttribute returns ComplexAttribute:
	(annotations+=Annotation)*
	(notChangeable?='final')? (unique?='unique')? (override?='override')? 'attr' type=[Type|EString] 'as' name=EString ('[' lowerBound=EInt (',' upperBound=BoundValue)? ']')? (':'? '=' defaultValue=EString)?
;

DefaultValueOverride returns DefaultValueOverride:
	'override' attribute=[PrimitiveAttribute] (':'? '=' defaultValue=EString)?
;



// Prime references

ReferencedType returns ReferencedType:
	ReferencedEClass
	| ReferencedModelElement
;

ReferencedEClass returns ReferencedEClass:
	(annotations+=Annotation)*
	'prime' imprt=[Import|ID] '.' type=[ecore::EClass|ID] 'as' name=EString
	(copiedAttributes+=ReferencedEStructuralFeature)*
;

ReferencedModelElement returns ReferencedModelElement:
	(annotations+=Annotation)*
	'prime' (local?='this' | imprt=[Import|QName]) '::' type=[ModelElement|QName] 'as' name=EString
	(copiedAttributes+=ReferencedMGLAttribute)*
;

ReferencedAttribute returns ReferencedAttribute:
	ReferencedMGLAttribute
	| ReferencedEStructuralFeature
;

ReferencedEStructuralFeature returns ReferencedEStructuralFeature:
	(parameter?='primeparam' | 'primeattr') feature=[ecore::EStructuralFeature|QName] 'as' name=EString
;

ReferencedMGLAttribute returns ReferencedMGLAttribute:
	'primeattr' feature=[Attribute|QName] 'as' name=EString
;



// Connections & containment

IncomingEdgeElementConnection returns IncomingEdgeElementConnection:
	{IncomingEdgeElementConnection}
	(
		(referencedImport=[Import] '::')? connectingEdges+=[Edge|QName]
		| (referencedImport=[Import] '::')? '{' connectingEdges+=[Edge|QName] (',' connectingEdges+=[Edge|QName])* '}'
	)
	('[' lowerBound=EInt ',' upperBound=BoundValue ']')?
;

OutgoingEdgeElementConnection returns OutgoingEdgeElementConnection:
	{OutgoingEdgeElementConnection}
	(
		(referencedImport=[Import] '::')? connectingEdges+=[Edge|QName]
		|(referencedImport=[Import]'::')? '{' connectingEdges+=[Edge|QName] (',' connectingEdges+=[Edge|QName])* '}'
	)
	('[' lowerBound=EInt ',' upperBound=BoundValue ']')?
;

GraphicalElementContainment returns GraphicalElementContainment:
	{GraphicalElementContainment}
	(
		((referencedImport=[Import] '::')? types+=[Node])
		| ((referencedImport=[Import] '::')? '{' types+=[Node] (',' types+=[Node])* '}')
	)
	('[' lowerBound=EInt ',' upperBound=BoundValue ']')?
;



// Other

Import returns Import:
	{Import}
	(stealth?='stealth')? 'import' (external?='external')? importURI=STRING 'as' name=ID
;

Annotation:
	{Annotation}
	'@' name=EString ('(' value+=EString (',' value+=EString)* ')')?
;

Wildcard returns Wildcard:
	(selfWildcard?='*' | (referencedImport=[Import] '::' '*'))
	('[' lowerBound=EInt ',' upperBound=BoundValue ']')?
;



// Terminals & enums

QName:
	(ID|ANY_OTHER)+ (=> '.' (ID|ANY_OTHER)+)*
;

EString returns ecore::EString:
	STRING
	| ID
;
	
EInt returns ecore::EInt:
	'-'? INT
;

BoundValue returns ecore::EInt:
	'*'
	| EInt
;

enum EDataTypeType returns EDataTypeType:
	EString 
	| EBoolean
	| EInt
	| ELong
	| EFloat
	| EDouble
	| EDate
//	| EChar
//	| EByte
//	| EShort
//	| EBigInteger
//	| EBigDecimal
;



// Unused

/*

Include returns Import:
	{Import}
	'include' importURI=STRING 'as' name=ID
;

Information returns Information:
	ModelCheckInformation
	| DataFlowInformation
;

ModelCheckInformation returns ModelCheckInformation:
	{ModelCheckInformation}
	'ModelCheckInformation'
	'{'
		('extends' extends=[ModelCheckInformation|EString])?
	'}'
;

DataFlowInformation returns DataFlowInformation:
	{DataFlowInformation}
	'DataFlowInformation'
	'{'
		('extends' extends=[DataFlowInformation|EString])?
	'}'
;

OrContainmentConstraint returns OrConstraint:
	negative?='not' '<' constraints+=GraphicalElementContainment (',' constraints+=GraphicalElementContainment)* '>'
;

URI:
	EString
;

Map:
	'EMap' '<' QName ',' QName '>'
;

EdgeDirection returns EdgeDirection:
	'Undirected'
	| 'TargetDirected'
	| 'SourceDirected'
	| 'Bidirected'
;

*/

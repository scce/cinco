/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.scoping;

import java.util.LinkedHashSet;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider;

import de.jabc.cinco.meta.core.utils.PathValidator;
import mgl.Import;
import mgl.MGLModel;

public class MGLImportUriGlobalScopeProvider extends ImportUriGlobalScopeProvider {

	@Override
	protected LinkedHashSet<URI> getImportedUris(Resource resource) {
		LinkedHashSet<URI> uris = super.getImportedUris(resource);
		
		for (EObject o : resource.getContents()) {
			if (o instanceof MGLModel) {
				MGLModel gm = (MGLModel) o;
				for (Import i : gm.getImports()) {
					try {
					String retVal = PathValidator.checkPath(gm, i.getImportURI());
					if (retVal != null && !retVal.isEmpty())
						continue;
					URI uri = PathValidator.getURIForString(gm, i.getImportURI());
					uris.add(uri);
					}catch(Exception e) {
						
					}
				}
			}
		}
		return uris;
	}
	
}

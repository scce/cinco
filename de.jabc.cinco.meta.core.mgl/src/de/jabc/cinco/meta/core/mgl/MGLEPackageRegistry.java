/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EPackage;

public class MGLEPackageRegistry {
	public static MGLEPackageRegistry INSTANCE = new MGLEPackageRegistry();
	
	private Set<EPackage> mglEPackages;
	
	private MGLEPackageRegistry(){
		this.mglEPackages = new HashSet<>();
	}
	
	public Set<EPackage> getMGLEPackages(){
		return Collections.unmodifiableSet(this.mglEPackages);
		
	}
	
	public void addMGLEPackage(EPackage ePkg){
		this.mglEPackages.add(ePkg);
	}

	public static void resetRegistry() {
		INSTANCE = new MGLEPackageRegistry();
		
	}
}

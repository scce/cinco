/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.mgl.validation

import de.jabc.cinco.meta.core.utils.CincoUtil
import de.jabc.cinco.meta.core.utils.InheritanceUtil
import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.PathValidator
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.runtime.xapi.FileExtension
import de.jabc.cinco.meta.runtime.xapi.WorkspaceExtension
import de.jabc.cinco.meta.util.DirectedGraph
import java.io.File
import java.util.HashSet
import java.util.List
import java.util.Set
import java.util.jar.Manifest
import mgl.Annotation
import mgl.Attribute
import mgl.BoundedConstraint
import mgl.ComplexAttribute
import mgl.ContainingElement
import mgl.DefaultValueOverride
import mgl.Edge
import mgl.GraphModel
import mgl.GraphicalElementContainment
import mgl.GraphicalModelElement
import mgl.Import
import mgl.MGLModel
import mgl.MglPackage
import mgl.ModelElement
import mgl.Node
import mgl.NodeContainer
import mgl.PrimitiveAttribute
import mgl.ReferencedEClass
import mgl.ReferencedType
import mgl.Type
import mgl.UserDefinedType
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.ResourcesPlugin
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.jdt.core.IJavaProject
import org.eclipse.jdt.core.IType
import org.eclipse.jdt.core.JavaCore
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.validation.Check
import productDefinition.CincoProduct

class MGLValidator extends AbstractMGLValidator {
	
	extension FileExtension = new FileExtension
	extension InheritanceUtil = new InheritanceUtil
	extension WorkspaceExtension = new WorkspaceExtension
	
	public static val String NOT_EXPORTED = "package is not exported"
		
	@Check
	def checkPackageNameExists(MGLModel model) {
		if (model.package.nullOrEmpty || model.package == "\"\"") {
			error('Package name must be present.',MglPackage.Literals::MGL_MODEL__PACKAGE)
		}
	}
	
	@Check
	def checkNamedElementNameStartsWithCapital(ModelElement namedElement) {
		if (!Character::isUpperCase(namedElement.name.charAt(0))) {
			error('Name must start with a capital', MglPackage.Literals::TYPE__NAME)
		}
	}
	
	@Check
	def checkNamedElementNameNotUnique(ModelElement namedElement) {
		for (e: namedElement.eContainer.eAllContents.toIterable.filter(typeof(ModelElement))) {
			if (e.name == namedElement.name && e != namedElement)
				error('Name must be unique', MglPackage.Literals::TYPE__NAME) 
		}
	}
	
	@Check
	def checkNodeIncomingConnections(GraphicalModelElement elem) {
		if (elem.incomingEdgeConnections.length < 2) {
			return;
		}
		
		for (connection: elem.incomingEdgeConnections) {
			if(connection.connectingEdges === null) {
				error("Incoming Edges cannot have a don't care and other edges.", MglPackage.Literals::GRAPHICAL_MODEL_ELEMENT__INCOMING_EDGE_CONNECTIONS)
			}
		}
	}
	
	@Check
	def checkNodeOutgoingConnections(GraphicalModelElement elem) {
		if(elem.outgoingEdgeConnections.length < 2) {
			return;
		}
		
		for (connection: elem.outgoingEdgeConnections) {
			if (connection.connectingEdges === null) {
				error("Incoming Edges cannot have a don't care and other edges.", MglPackage.Literals::GRAPHICAL_MODEL_ELEMENT__OUTGOING_EDGE_CONNECTIONS)
			}
		}
	}
	
	@Check
	def checkIncomingEdgeConnectionsUnique(GraphicalModelElement elem) {
		val set = <List<Edge>> newHashSet()
		for (connection: elem.incomingEdgeConnections) {
			if (connection.connectingEdges !== null && !set.add(connection.connectingEdges)) {
				error("Given Edges should be unique", MglPackage.Literals::GRAPHICAL_MODEL_ELEMENT__INCOMING_EDGE_CONNECTIONS)
			}
		}
	}
	
	@Check
	def checkOutgoingEdgeConnectionsUnique(GraphicalModelElement elem) {
		val set = <List<Edge>> newHashSet()
		for (connection: elem.outgoingEdgeConnections) {
			if (connection.connectingEdges !== null && !set.add(connection.connectingEdges)) {
				error("Given Edges should be unique", MglPackage.Literals::GRAPHICAL_MODEL_ELEMENT__OUTGOING_EDGE_CONNECTIONS)
			}	
		}
	}
	
	@Check
	def checkUpperBound(Attribute attribute){
		if (attribute.upperBound == 0 || attribute.upperBound < -1) {
			error("Upper Bound of attribute " + attribute.name + " must be -1 or bigger than 0", MglPackage.Literals::ATTRIBUTE__UPPER_BOUND)
		}
		
		if (attribute.upperBound < attribute.lowerBound && attribute.upperBound != -1) {
			error("Upper Bound of attribute " + attribute.name + " can not be lower than Lower Bound", MglPackage.Literals::ATTRIBUTE__UPPER_BOUND)
		}
	}
	
	@Check
	def checkLowerBound(Attribute attribute){
		if(attribute.lowerBound<0)
			error("Lower Bound of attribute "+attribute.name+" cannot be lower than 0.",MglPackage.Literals::ATTRIBUTE__LOWER_BOUND)
		if(attribute.lowerBound>attribute.upperBound&&attribute.upperBound!=-1)
			error("Lower Bound of attribute "+attribute.name+" cannot be larger than Upper Bound.",MglPackage.Literals::ATTRIBUTE__LOWER_BOUND)
	}
	
	@Check
	def checkReservedWordsInAttributes(Attribute attr){
		 if(attr.name.toUpperCase=="ID")
			error("Attribute Name cannot be "+attr.name+".",MglPackage.Literals::ATTRIBUTE__NAME)
	}

	@Check
	def checkFeatureNameUnique(Attribute attr) {
		for (a: attr.modelElement.attributes) {
			if (a!= attr && a.name.equalsIgnoreCase(attr.name)) {
				error("Attribute Names must be unique", MglPackage.Literals::ATTRIBUTE__NAME)
			}
		}
		val container = attr.modelElement
		if(container instanceof Node){
			if(container.primeReference !== null){
				val name = container.primeReference.name
				if(attr.name.equalsIgnoreCase(name)){
					error("Attribute Names must be different from Prime Reference Names", MglPackage.Literals::ATTRIBUTE__NAME)
				}
			}
		}
		
		var element = attr.modelElement		
			
		var superType = element.extends
		while (superType !== null && element.checkMGLInheritance.nullOrEmpty) {
			for (a : superType.attributes) {
				if(a.name.equalsIgnoreCase(attr.name)) {
					if(!(attr instanceof ComplexAttribute) || !(attr as ComplexAttribute).override) {
						error("Attribute Names must be unique", MglPackage.Literals::ATTRIBUTE__NAME)
					}
				}
			}
			superType = superType.extends
		}
	}
	
	@Check
	def checkIsOverridenTypeSuperType(ComplexAttribute attr) {
		var e = attr.parent.extends
		while(e !== null ){
			e.attributes.filter(ComplexAttribute).forEach[at|
				if (at.name == attr.name && !attr.type.isSubType(at.type)) {
					error("Overriding attribute types must be subtype of overridden attribute types.", MglPackage.Literals::COMPLEX_ATTRIBUTE__TYPE)	
				}
			]
			
			e.attributes.filter(PrimitiveAttribute).forEach[at|
				if (at.name == attr.name) {
					error("Only Complex Attributes can be overridden.", MglPackage.Literals.COMPLEX_ATTRIBUTE__TYPE)
				}
			]
			
			e = e.extends
		}
	}
	
	def static boolean isSubType(Type subtype, Type superType){
		if (subtype instanceof ModelElement && superType instanceof ModelElement) {
			var e = (subtype as ModelElement).extends
			while (e !== null) {
				if(e == superType) {
					return true
				} else {
					e = e.extends
				}
			}
		}
		return false
	}
	
	private static def ModelElement parent(ComplexAttribute attribute) {
		attribute.eContainer as ModelElement
	}
	
	private static def <T extends ModelElement> getExtends(ModelElement element) {
		switch(element){
			Node: element.extends
			Edge: element.extends
			UserDefinedType: element.extends
			GraphModel: element.extends
		}
	}
	
	@Check
	def checkCanResolveEClass(ReferencedEClass ref) {
		var eclass = ref.type
		
		if (eclass.eIsProxy) {
			try {
				var EObject obj
				ref.type = EcoreUtil2::resolve(eclass, obj) as EClass
			} catch(Exception e){
				error("Cannot resolve EClass: " + eclass, MglPackage.Literals::REFERENCED_ECLASS__TYPE)
			}
			
		}
	}
	
	@Check
	def checkNodeInheritsFromNonAbstractPrimeReferenceNode(Node node) {
		var currentNode = node
		val noCircles = node.checkMGLInheritance.nullOrEmpty
		while (currentNode.extends !== null && noCircles){
			currentNode = currentNode.extends
			if (!currentNode.isIsAbstract && currentNode instanceof ReferencedType) {
				error("Node " + node.name + " inherits from non abstract prime node " + currentNode.name, MglPackage.Literals::NODE__EXTENDS)
			}
		}
	}
	
	@Check
	def checkNodeInheritsFromNonAbstractNodeWithPrimeReferenceAttribute(Node node) {
		val parentNode = node.extends
		if(node.primeReference !== null && parentNode !== null && MGLUtil.isPrime(parentNode)){
			error('''Nodes extending prime nodes may not override the prime reference'''.toString,MglPackage.Literals::NODE__PRIME_REFERENCE)
		}
	}
	
	
	@Check
	def checkGraphicalModelElementUsesStyleAttribute(GraphicalModelElement graphicalModelElement) {
		if (!graphicalModelElement.isIsAbstract && (graphicalModelElement.usedStyle === null || graphicalModelElement.usedStyle.isEmpty)) {
			error("Non-abstract Graphical Model Elements have to reference a style.", MglPackage.Literals::TYPE__NAME)
		}
	}
	
	@Check
	def checkAbstractGraphicalModelElementHasUselessStyleAttributes(GraphicalModelElement graphicalModelElement) {
		if (graphicalModelElement.isIsAbstract && graphicalModelElement.usedStyle !== null) {
			warning("Referencing styles has no effect on abstract elements", MglPackage.Literals::TYPE__NAME)
		}
		
		if (graphicalModelElement.isIsAbstract && graphicalModelElement.annotations.exists[x | x.name.equals("icon")]) {
			warning("@icon annotation has no effect on abstract elements", MglPackage.Literals::TYPE__NAME)
		}
		
		if (graphicalModelElement.isIsAbstract && graphicalModelElement.annotations.exists[x | x.name.equals("palette")]) {
			warning("@palette annotation has no effect on abstract elements", MglPackage.Literals::TYPE__NAME)
		}
	}
	
	@Check
	def checkGraphModelContainableElements(ContainingElement model) {
		if(model.containableElements.size > 1) {
			for(containment:model.containableElements) {
				if(containment.types === null) {
					error("Dont't care type must not be accompanied by other containable elements.", MglPackage.Literals::CONTAINING_ELEMENT__CONTAINABLE_ELEMENTS);
				}
			}
		}
		
		if(model.containableElements.size == 1) {
			if(model.containableElements.get(0).types === null) {
				if(model.containableElements.get(0).upperBound == 0) {
					warning("Container element cannot contain any model elements by this definition.", MglPackage.Literals::CONTAINING_ELEMENT__CONTAINABLE_ELEMENTS)
				}
			}
		}
	}
	
	@Check
	def checkBoundedConstraintCardinality(BoundedConstraint bc) {
		var lower = bc.lowerBound
		var upper = bc.upperBound
		
		if (lower < 0) {
			error("Containment lower bound must not be lower 0.", MglPackage.Literals::BOUNDED_CONSTRAINT__LOWER_BOUND)
		}
		
		if (lower > upper && upper != -1) {
			error("Containment lower bound must not be bigger than upper bound.", MglPackage.Literals::BOUNDED_CONSTRAINT__LOWER_BOUND)
		}
		
		if (upper < -1) {
			error("Containment upper bound must not be lower -1", MglPackage.Literals::BOUNDED_CONSTRAINT__UPPER_BOUND)
		}
	}
	
	@Check
	def checkDiagramExtensionisNotEmpty(GraphModel m) {
		if(!m.isIsAbstract && m.fileExtension.nullOrEmpty) {
			error("Non-abstract graph models require a file extension.",MglPackage.Literals::GRAPH_MODEL__FILE_EXTENSION)
		}
	}
	
	@Check
	def checkGraphModelIconPath(GraphModel gm) {
		if (!gm.iconPath.nullOrEmpty) {
			val retVal = PathValidator.checkPath(gm, gm.iconPath) as String
 			if (!retVal.empty) {
 				error(retVal, MglPackage.Literals.GRAPH_MODEL__ICON_PATH, "The specified path: \"" + gm.iconPath +"\" does not exist")
			}
 		}
	}
	
	@Check
	def checkImportUris(Import imp) {
		try{
			val retVal = PathValidator.checkPath(imp, imp.importURI)
		if (!retVal.nullOrEmpty)
			error(retVal, MglPackage.Literals.IMPORT__IMPORT_URI, "Could not load resource")
		}catch(Exception e){
			error("Could not load resource", MglPackage.Literals.IMPORT__IMPORT_URI, "Could not load resource")
		}
		
	}
	
	@Check
	def checkExternalMGLIsStealth(Import imp){
		if(!PathValidator.isRelativePath(imp.importURI)){
			if(!PathValidator.checkSameProjects(imp,imp.importURI)&& imp.importURI.mglImport && !imp.isStealth && !imp.isExternal){
				error("MGLs imported from foreign Projects must be imported stealthy or be marked as an external import",MglPackage.Literals.IMPORT__IMPORT_URI);
			}
		} 
			
	}
	
	def isMglImport(String importURI){
		importURI.endsWith(".mgl")
	}
	
	@Check
	def checkMGLInheritanceCircles(ModelElement me) {
			var retvalList = me.checkMGLInheritance
			if (!retvalList.nullOrEmpty) {
				if (me instanceof Node) {
					error("Cycle in inheritance caused by: " + retvalList, MglPackage.Literals.NODE__EXTENDS)
				}
				
				if (me instanceof Edge) {
					error("Cycle in inheritance caused by: " + retvalList, MglPackage.Literals.EDGE__EXTENDS)
				}
				
				if (me instanceof UserDefinedType) {
					error("Cycle in inheritance caused by: " + retvalList, MglPackage.Literals.USER_DEFINED_TYPE__EXTENDS)
				}
				if (me instanceof GraphModel){
					error("Cycle in inheritance caused by: " + retvalList, MglPackage.Literals.GRAPH_MODEL__EXTENDS)
				}
			}
	}
	
	@Check
	def checkNodeInheritsFromNode(Node node) {
		if (!(node instanceof NodeContainer) && node.extends !== null && (node.extends instanceof NodeContainer)) {
			error("Inheriting from Containers is not possible for Nodes.", MglPackage.Literals.NODE__EXTENDS)
		}
	}
	
	@Check
	def checkAttributeNameUnequalContainablePlurals(Attribute attribute) {
		val attrEContainer = attribute.eContainer;
		if(attrEContainer instanceof ContainingElement
			&& (
				(attrEContainer as ContainingElement).getContainableElements.filter[upperBound !== 0].flatMap[types].exists[
					name.toLowerCase() + "s" == attribute.name.toLowerCase()
				]
				|| ((attrEContainer as ContainingElement).containmentWildcards.exists[upperBound !== 0]
					&& (attrEContainer.eContainer as MGLModel).nodes.exists[
						name.toLowerCase() + "s" == attribute.name.toLowerCase()
					]
				)
			)
		) {
			error("Attribute names must not be equal to plurals of containable elements", MglPackage.Literals.ATTRIBUTE__NAME)
		}
	}
	
	@Check
	def checkHasFinalDefaultValue(Attribute attr){
		if (attr.notChangeable) {
			if (attr.defaultValue === null || attr.defaultValue == "") {
				error("Final Attribute must have a default value", MglPackage.Literals.ATTRIBUTE__NOT_CHANGEABLE)
			}
		}
	}
	
	@Check
	def checkIsPackageNameValidJavaPackageName(MGLModel it) {
		var splitPN = package.split("\\.")
		for(part:splitPN){
			var ca = part.toCharArray
			var i=0;
			while(i<ca.length){
				if(i==0) {
					if(!Character.isJavaIdentifierStart(ca.get(i))) {
						error("Character "+ca.get(i)+" is no valid Java identifier start.",MglPackage.Literals.MGL_MODEL__PACKAGE);
					}
				}
						
				if(!Character.isJavaIdentifierPart(ca.get(i))) {
					error("Character "+ca.get(i)+" is no valid Java identifier part.",MglPackage.Literals.MGL_MODEL__PACKAGE);
				}
							
				i = i+1
			}
		}
	}
	
	@Check
	def checkReferencedNodeHasNameAttribute(ComplexAttribute attribute) {
		val modelElement = attribute.modelElement as ModelElement
		val graphModel = MGLUtil::mglModel(modelElement)
		
		val refNodes = graphModel.nodes.filter[Node n | n.name.equals(attribute.type)]
		val refEdges = graphModel.edges.filter[Edge e | e.name.equals(attribute.type)]		
		
		if ((!refNodes.nullOrEmpty || !refEdges.nullOrEmpty)) {
			if (!nodesContainsName(refNodes) && !edgesContainsName(refEdges)) {
				error("Add a String attribute \"name\" to the NodeType(s): " + refNodes.map[name], MglPackage.Literals.COMPLEX_ATTRIBUTE__TYPE)
			}
		}
	}
	
	private def edgesContainsName(Iterable<Edge> edges) {
		val parentEdges = <Edge> newArrayList()
		for (Edge e : edges) {
			var currentParent = e
			while (currentParent !== null) {
				parentEdges.add(currentParent)
				currentParent = currentParent.extends
			}
		}
		
		val remainingParents = parentEdges.filter[Edge p |  p.attributes.map[name].contains("name")]
		return !remainingParents.isEmpty 
	}
	
	private def nodesContainsName(Iterable<Node> nodes) {
		val parentNodes = <Node> newArrayList() 
		for (Node n : nodes) {
			var currentParent = n
			while (currentParent !== null) {
				parentNodes.add(currentParent)
				currentParent = currentParent.extends
			}
		}
		
		val remainingParents = parentNodes.filter[Node p | p.attributes.map[name].contains("name")]
		return !remainingParents.isEmpty 
	}
	
	@Check 
	def checkContainableElementIsIndependent(GraphicalElementContainment e) {
			var superType = getContainingSuperType(e.containingElement)
			
			while (superType!=e.containingElement && superType!==null && (superType instanceof ContainingElement)) {
				if (superType.containableElements.exists[y | y.types.exists[x | e.types.contains(x)]]) {
					error("Containment must be independent from inherited containments", MglPackage.Literals.GRAPHICAL_ELEMENT_CONTAINMENT__TYPES)
				}
				superType = getContainingSuperType(superType)
			}
		
		
	}
	
	private def dispatch ContainingElement getContainingSuperType(ContainingElement modelElement){
		switch(modelElement){
			GraphModel: (modelElement.extends) as GraphModel
			NodeContainer: (modelElement.extends) as NodeContainer
		} 
	}
	
	private def dispatch ContainingElement getContainingSuperType(Node modelElement) {
		return null
	}
	
	@Check
	def checkMultipleAnnotation(Annotation annot) {
		val elementAnnotations = annot.parent.annotations
		if (elementAnnotations.filter[name == annot.name].size > 1 && !annot.isMultipleAllowed)
			error('''Multiple annotations of type: «annot.name»''', MglPackage.Literals.ANNOTATION__NAME)
	}
	
	def isMultipleAllowed(Annotation annotation) {
		#["mcam_checkmodule", "contextMenuAction", "postDelete", "postCreate"].contains(annotation.name)
	}
	
	@Check
	def checkCustomActionAnnotation(Annotation annotation){
		if (isCustomAction(annotation.name)) {
			if (annotation.value.nullOrEmpty || annotation.value.size != 1) {
				error("CustomAction needs exactly one Java Class as a Parameter", MglPackage.Literals.ANNOTATION__VALUE)
			} else {
				val parameter = annotation.value.get(0)
				if (parameter.empty) {
					error("Java Class cannot be an empty String", MglPackage.Literals.ANNOTATION__VALUE)
				} else {
					checkIfJavaClassExistsAndIsAccessible(parameter)
				}
			}
		}
	}
	
	private def isCustomAction(String name) {
		switch (name) {
			case "contextMenuAction",
			case "doubleClickAction",
			case "postSelect",
			case "postCreate",
			case "postMove",
			case "postResize",
			case "preDelete",
			case "postDelete",
			case "postSave",
			case "postAttributeChange",
			case "possibleValuesProvider": {
				return true
			}
			default: {
				return false
			}
		}
	}
	
	private def checkIfJavaClassExistsAndIsAccessible(String fqClassName) {
		val correctFile = findClass(fqClassName)
		if (correctFile === null || !correctFile.exists) {
			error("Java Class does not exists", MglPackage.Literals.ANNOTATION__VALUE)
		} else {
			val package = findPackage(fqClassName)
			if (package === null) {
				error("Package not found", MglPackage.Literals.ANNOTATION__VALUE)
			} else {
				checkIfPackageIsExported(correctFile, package)
			}
		}
	}
	
	def findClass(String parameter) {
		var IType javaClass = null
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for( project : projects) {
			var jproject = JavaCore.create(project) as IJavaProject
			if (jproject.exists) {
				try {
					javaClass = jproject.findType(parameter)
					if (javaClass !== null) {
						return javaClass 
					}
				} catch (Exception e) {
					// nothing to do here (?)
				}
			}
		}
		return javaClass
	}
	
	def findPackage(String parameter) { 
		var IType javaClass = null
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for (project : projects) {
			var jproject = JavaCore.create(project) as IJavaProject
			if (jproject.exists) {
				try {
					javaClass = jproject.findType(parameter)
					if (javaClass !== null) {
						return jproject
					}
				} catch (Exception e) {
					// nothing to do here (?)
				}
			}
		}
		
		return null
	}
	
	private def checkIfPackageIsExported(IType correctFile, IJavaProject package_) {
		val packageExport = correctFile.packageFragment.elementName
		val root = ResourcesPlugin.workspace.root
		val projects = root.projects
		for (project : projects) {
			if (project.name == package_.elementName) {
				var folder = project.getFolder("META-INF")
				var manifest = folder.getFile("MANIFEST.MF")
				if (manifest.exists) {
					val isExported = findExportedPackage(project, packageExport)
					if (!isExported) {
				    	warning("Corresponding package is not exported", MglPackage.Literals.ANNOTATION__VALUE, NOT_EXPORTED)
					}
				}
			}
		}
	}
	
	def findExportedPackage(IProject project, String packageName) {
		val iManiFile= project.getFolder("META-INF").getFile("MANIFEST.MF")
		
		CincoUtil.refreshFiles(null, iManiFile)
		val manifest = new Manifest(iManiFile.getContents())
			
		var value = manifest.getMainAttributes().getValue("Export-Package")
		if (value === null) {
			value = ""
		} 
		
		return value.contains(packageName)
	}
	
	@Check
	def checkColorAnnotation(Annotation a) { 
		if (a.name == "color") {
			if (a.value.size == 1) { //one parameter allowed
				if (a.parent instanceof PrimitiveAttribute) { //only correct Type (EString)
					val attr = a.parent as PrimitiveAttribute
					if (attr.type.getName != "EString") {
						error("Attribute type has to be EString", MglPackage.Literals.ANNOTATION__NAME)
					}
					if (a.value.get(0) != "") {
						if(a.value.get(0) == "rgb") {
							if(!attr.defaultValue.empty || attr.defaultValue.empty != "") { //correct default values
								val defaultValue = attr.defaultValue
								var result = defaultValue.split(",")
								if(result.size != 3) {
									error("default value doesn't have a RGB-scheme", MglPackage.Literals.ANNOTATION__NAME)
								} else {
									checkRGBDefaultValues(result)
								}
							}
						} else if (a.value.get(0) == "hex") { //check default values
							if (!attr.defaultValue.empty || attr.defaultValue != "") {
								val defaultValue = attr.defaultValue
								if (defaultValue.length == 7) {
									if (defaultValue.startsWith("#")) {
										for (var i = 1; i < defaultValue.length ; i++) {
											val value = defaultValue.charAt(i)
											if (value < '0' || value > '9' ) {
												if (!checkLetter(value)) {
													error("hex values are between 0 and 9 or A and F",  MglPackage.Literals.ANNOTATION__NAME)
												}
											}
										}
									} else {
										error("default value of hex must start with '#'",  MglPackage.Literals.ANNOTATION__NAME)
									}
								} else {
									error("default value does not have a hex-scheme",  MglPackage.Literals.ANNOTATION__NAME)
								}
							}
						} else if (a.value.get(0) == "rgba") { //check default values
							if(!(attr.defaultValue.empty) || attr.defaultValue.empty != "") { 
								val defaultValue = attr.defaultValue
								var result = defaultValue.split(",")
								if (result.size != 4){
									error("default value doesn't have a RGBA-scheme", MglPackage.Literals.ANNOTATION__NAME)
								} else {
									checkRGBDefaultValues(result)
									var a_string = result.get(3)
									try {
										var alpha = Integer.parseInt(a_string)
										if (alpha < 0 || alpha > 255) {
											error("alpha-value has to be bigger or equal than 0 and lower or equal than 255 ", MglPackage.Literals.ANNOTATION__NAME )
										}
									} catch(Exception e){
										error("Please enter only numbers as default value", MglPackage.Literals.ANNOTATION__NAME)
									}
								}
							}	
						} else {
							error("color Annotation needs one parameter like rgb, rgba or hex", MglPackage.Literals.ANNOTATION__VALUE)
						}
					}
				} else {
					error("Attribute has to be a PrimitiveAttribute ", MglPackage.Literals.ANNOTATION__NAME)
				}
			} else {
				error("color Annotation needs exactly one parameter like rgb, hex or rgba", MglPackage.Literals.ANNOTATION__VALUE)
			}
		}
	}

	private def checkRGBDefaultValues(String[] result) {
		val r_string = result.get(0)
		val g_string = result.get(1)
		val b_string = result.get(2)
		
		try {
			val r = Integer.parseInt(r_string)	
			val g = Integer.parseInt(g_string)
			val b = Integer.parseInt(b_string)
							
			if (r < 0 || r > 255) {
				error("r-value has to be bigger or equal than 0 and lower or equal than 255", MglPackage.Literals.ANNOTATION__NAME)
			}
			
			if (g < 0 || g > 255) {
				error("g-value has to be bigger or equal than 0 and lower or equal than 255",  MglPackage.Literals.ANNOTATION__NAME)
			}
			
			if (b < 0 || b > 255) {
				error("b-value has to be bigger or equal than 0 and lower or equal than 255",  MglPackage.Literals.ANNOTATION__NAME)
			}					
		} catch(Exception e) {
			error("Please enter only numbers as default value", MglPackage.Literals.ANNOTATION__NAME)
		}
	}
	
	private def checkLetter(char c) {
		switch (c.toString) {
			case 'A',
			case 'B',
			case 'C',
			case 'D',
			case 'E',
			case 'F': {
				return true
			}
			default: {
				return false
			}
		}
	}
	
	@Check
	def checkFileAnnotations(Annotation a) {
		if (a.name == "file" && a.parent instanceof PrimitiveAttribute) { //only correct Type (EString)
			val attr = a.parent as PrimitiveAttribute
			if (attr.type.getName != "EString") {
				error("Type has to be EString",MglPackage.Literals.ANNOTATION__NAME)
			}
			
			if (!attr.defaultValue.empty || attr.defaultValue.empty != "") { //only correct default values
				val defaultValue = attr.defaultValue
				try {
					val file = new File(defaultValue);
					if (!file.exists) {
						error("Wrong Path: File doesn't exists", MglPackage.Literals.ANNOTATION__NAME)
					}
				} catch (Exception e){
					error("Wrong Path: File doesn't exists", MglPackage.Literals.ANNOTATION__NAME)
				}
			}
		}
		
	}
	
	@Check
	def checkImportCycleExists(Import imprt) {
		if(!imprt.isStealth) {
			val originalMGLModel = imprt.eContainer as MGLModel
			val importedMGLModel = CincoUtil.getImportedMGLModel(imprt)
			if(importedMGLModel !== null) {
				val importsToCheck = importedMGLModel.imports.filter[!isStealth].toList
				val alreadyVisitedMGLModel = newLinkedList(originalMGLModel, importedMGLModel)
				for(var i = 0; i < importsToCheck.size; i++) {
					val currentImportedMGL = CincoUtil.getImportedMGLModel(importsToCheck.get(i))
					if(currentImportedMGL !== null) {
						if(alreadyVisitedMGLModel.exists[MGLUtil.equalMGLModels(it, currentImportedMGL)]) {
							error("Cyclic imports detected at " + GeneratorUtils.instance.getFileName(currentImportedMGL) + ".mgl", MglPackage.Literals.IMPORT__IMPORT_URI)
							return
						} else {
							alreadyVisitedMGLModel.add(currentImportedMGL)
							importsToCheck.addAll(currentImportedMGL.imports.filter[!isStealth].toList)
						}
					}
				}
			}
		}
		// No cycle found
		return
	}
	
	@Check
	def checkExternalImportsPointToMGLs(Import imprt) {
		if(imprt.isExternal && !imprt.importURI.endsWith(".mgl")) {
			error("Imports of external resources may only import MGLs", MglPackage.Literals.IMPORT__IMPORT_URI);
		}
	}
	
	@Check
	def checkModelElementNameForNameClashes(ModelElement me) {
		val mgl = MGLUtil.getMglModel(me)
		val imports = mgl.imports.filter[!isStealth]
		imports.forEach[imp |
			MGLUtil.modelElements(CincoUtil.getImportedMGLModel(imp), true).forEach[
				if(!MGLUtil.equalModelElement(it, me) && it.name == me.name) {
					error("This name already exists in the imported MGL \"" + imp.name + "\"", MglPackage.Literals.TYPE__NAME);
				}
			]
		]
	}
	
	@Check
	def checkImportForNameClashes(Import imprt) {
		if(!imprt.isStealth) {
			val mgl = imprt.eContainer as MGLModel
			val imports = mgl.imports.filter[!isStealth]
			val importedMGL = CincoUtil.getImportedMGLModel(imprt)
			imports.forEach[imp |
				if(imprt !== imp) {
					MGLUtil.modelElements(CincoUtil.getImportedMGLModel(imp), true).forEach[importedMe |
						MGLUtil.modelElements(importedMGL).forEach[
							if(it.name == importedMe.name) {
								error("The model element name \"" + it.name + "\" leads to a name clash, as " +
									"it exists in the imported MGLs \"" + imp.name + "\" and \"" + imprt.name + "\".",
									MglPackage.Literals.IMPORT__NAME);
							}
						]
					]
				}
			]
		}
	}
	
	@Check
	def checkDefaultValueOverrideScope(DefaultValueOverride dvo) {
		var me = dvo.eContainer as ModelElement
		val meType = switch (me) {
			GraphModel:      "Graph model"
			NodeContainer:   "Container"
			Node:            "Node"
			Edge:            "Edge"
			UserDefinedType: "Type"
			default:         me.class.simpleName
		}
		if (me.extends === null) {
			// Cannot override anything because model element does not extend anything
			error(
				'''«meType» «me.name» does not extend other type.''',
				dvo,
				MglPackage.Literals.DEFAULT_VALUE_OVERRIDE__MODEL_ELEMENT
			)
			return
		}
		if (dvo.attribute === null) {
			// Allow the default error message from Xtext if the reference to
			// an attribute could not be found
			return
		}
		me = me.extends
		while (me !== null) {
			// Check if super types has attribute with matching name
			if (me.attributes.exists[name == dvo.attribute.name]) {
				return
			}
			else {
				me = me.extends
			}
		}
		error(
			'''Couldn't resolve reference to PrimitiveAttribute '«dvo.attribute.name»'.''',
			dvo,
			MglPackage.Literals.DEFAULT_VALUE_OVERRIDE__ATTRIBUTE
		)
	}
	
	@Check
	def checkDefaultValueOverrideDuplicates(ModelElement element) {
		val dvos = element.defaultValueOverrides.sortBy[attribute.name]
		var lastDvo = dvos.head
		for (i: 1 ..< dvos.size) {
			val currentDvo = dvos.get(i)
			if (currentDvo.attribute === lastDvo.attribute) {
				error(
					'''Duplicate override of default value''',
					lastDvo,
					MglPackage.Literals.DEFAULT_VALUE_OVERRIDE__ATTRIBUTE
				)
				error(
					'''Duplicate override of default value''',
					currentDvo,
					MglPackage.Literals.DEFAULT_VALUE_OVERRIDE__ATTRIBUTE
				)
			}
			lastDvo = currentDvo
		}
	}
	
	@Check(NORMAL) // Check on save / build
	def void checkMglUsage(MGLModel mgl) {
		val mglFile = mgl.file
		val mglReltivePath = mglFile.projectRelativePath.toString
		val mglIsUsed = mglFile
			.project
			.findFiles("cpd")
			.map [ getContent(CincoProduct) ]
			.flatMap [ mgls ]
			.map [ mglPath ]
			.contains(mglReltivePath)
		if (!mglIsUsed) {
			warning(
				'''This MGL unused. Add this MGL as "«mglReltivePath»" to a CPD.''',
				mgl,
				MglPackage.Literals.MGL_MODEL__PACKAGE
			)
		}
	}
	
	@Check(NORMAL) // Check on save / build
	def void checkGlobalModelElementUsage(MGLModel mgl) {
		
		val extension Cache = Cache.getInstance(mgl)
		
		try {
		
			for (node: mgl.nodes) {
				if (node.isAbstract && !node.hasSubType) {
					// Node is abstract and has no sub-type
					warning(
						'''The abstract «IF node instanceof NodeContainer»container«ELSE»node«ENDIF» "«node.name»" is unused. It is never extended.''',
						node,
						MglPackage.Literals.TYPE__NAME
					)
				}
				else if (!node.isAbstract && !node.isContained) {
					// Node is never contained
					warning(
						'''The «IF node instanceof NodeContainer»container«ELSE»node«ENDIF» "«node.name»" is unused. It cannot be contained by any graph model or container.''',
						node,
						MglPackage.Literals.TYPE__NAME
					)
				}
			}
			
			for (edge: mgl.edges) {
				if (edge.isAbstract && !edge.hasSubType) {
					// Edge is abstract and has no sub-type
					warning(
						'''The abstract edge "«edge.name»" is unused. It is never extended.''',
						edge,
						MglPackage.Literals.TYPE__NAME
					)
				}
				else if (!edge.isAbstract) {
					val isNotIncoming = !edge.isConnectedIncoming
					var isNotOutgoing = !edge.isConnectedOutgoing
					if (isNotIncoming && isNotOutgoing) {
						// Edge is never incoming nor outgoing
						warning(
							'''The edge "«edge.name»" is unused. It never serves as an incoming or outgoing edge.''',
							edge,
							MglPackage.Literals.TYPE__NAME
						)
					}
					else if (isNotIncoming) {
						// Edge is never incoming
						warning(
							'''The edge "«edge.name»" is unused. It never serves as an incoming edge.''',
							edge,
							MglPackage.Literals.TYPE__NAME
						)
					}
					else if (isNotOutgoing) {
						// Edge is never outgoing
						warning(
							'''The edge "«edge.name»" is unused. It never serves as an outgoing edge.''',
							edge,
							MglPackage.Literals.TYPE__NAME
						)
					}
				}
			}
				
		}
		catch (Exception e) {
			e.printStackTrace
		}
		
	}
	
	/**
	 * Cache object for {@link MGLValidator#checkGlobalModelElementUsage(MGLModel)
	 * checkGlobalModelElementUsage(MGLModel)}. Saves time when validating projects
	 * with many MGLs.
	 */
	private static class Cache {
		
		extension FileExtension = new FileExtension
		extension WorkspaceExtension = new WorkspaceExtension
		
		static var Cache instance
		
		var long timeStamp
		
		var List<MGLModel> mgls
		var List<GraphModel> graphModels
		var List<Node> nodesAndContainers
		var List<Node> nodes
		var List<NodeContainer> containers
		var List<Edge> edges
		
		var DirectedGraph<ModelElement> inheritanceGraph
		var Set<Node> isContained
		var Set<Edge> isConnectedIncoming
		var Set<Edge> isConnectedOutgoing
		
		static def getInstance(MGLModel mgl) {
			if (instance === null || instance.isOutdated(mgl)) {
				instance = new Cache(mgl)
			}
			return instance
		}
		
		new (MGLModel mgl) {
			
			this.timeStamp = System.currentTimeMillis
			
			try {
				
				// Find and prepare all MGLs related to `mgl` 
				val mglFile = mgl.file
				val mglReltivePath = mglFile.projectRelativePath.toString
				val project = mglFile.project
				this.mgls = project
					.findFiles("cpd")                       // CPD files
					.map [ getContent(CincoProduct) ]       // CPD models
					.map [ cpd | cpd.mgls.map [ mglPath ] ] // List of MGL paths for each CPD
					.filter [ contains(mglReltivePath) ]    // List of MGL paths for each relevant CPD
					.map [ paths |                          // List of MGL models for each relevant CPD
						paths.map [ path |
							project.getFile(path).getContent(MGLModel)
						]
					]
					.map [ mgls |                           // Prepare MGL models
						MGLUtil.prepareMglModels(mgls.toSet)
					]
					.flatten                                // Merge to a flat list of MGLs
					.toList
				
				// Get elements
				this.graphModels        = mgls.flatMap[ m | m.graphModels ].toList
				this.nodesAndContainers = mgls.flatMap[ m | m.nodes ].toList
				this.nodes              = nodesAndContainers.reject(NodeContainer).toList
				this.containers         = nodesAndContainers.filter(NodeContainer).toList
				this.edges              = mgls.flatMap[ m | m.edges ].toList
				
				// Build inheritance graph
				this.inheritanceGraph = new DirectedGraph<ModelElement> [ l, r | modelElementEquals(l, r) ]
				for (type: graphModels + nodes + containers + edges) {
					val n = inheritanceGraph.addNode(type)
					val superType = type.extends
					if (superType !== null) {
						n.addParent(superType)
					}
				}
				
				// Build containment set
				this.isContained = new HashSet<Node>
				for (container: graphModels + containers) {
					val mustNotContain = container
						.containableElements
						.filter[upperBound == 0]
						.flatMap[types]
						.flatMap[subTypes]
					val canContain = container
						.containableElements
						.reject[upperBound == 0]
						.flatMap[types]
						.flatMap[subTypes]
						.filter(Node)
						.reject[mustNotContain.contains(it)]
					isContained.addAll(canContain)
				}
				
				// Build connectedness sets
				this.isConnectedIncoming = new HashSet<Edge>
				this.isConnectedOutgoing = new HashSet<Edge>
				for (node: nodesAndContainers) {
					val mustNotConnectIncoming = node
						.incomingEdgeConnections
						.filter[upperBound == 0]
						.flatMap[connectingEdges]
						.flatMap[subTypes]
					val canConnectIncoming = node
						.incomingEdgeConnections
						.reject[upperBound == 0]
						.flatMap[connectingEdges]
						.flatMap[subTypes]
						.filter(Edge)
						.reject[mustNotConnectIncoming.contains(it)]
					isConnectedIncoming.addAll(canConnectIncoming)
					
					val mustNotConnectOutgoing = node
						.outgoingEdgeConnections
						.filter[upperBound == 0]
						.flatMap[connectingEdges]
						.flatMap[subTypes]
					val canConnectOutgoing = node
						.outgoingEdgeConnections
						.reject[upperBound == 0]
						.flatMap[connectingEdges]
						.flatMap[subTypes]
						.filter(Edge)
						.reject[mustNotConnectOutgoing.contains(it)]
					isConnectedOutgoing.addAll(canConnectOutgoing)
				}
				
			}
			catch (Exception e) {
				e.printStackTrace
				instance = null
			}
			
		}
		
		static def modelElementEquals(ModelElement l, ModelElement r) {
			l.name == r.name &&
			(l.eContainer as MGLModel).package == (l.eContainer as MGLModel).package
		}
		
		def getSubTypes(ModelElement me) {
			inheritanceGraph.getNode(me).descendantsAndSelf.map[content]
		}
		
		def hasSubType(ModelElement me) {
			!inheritanceGraph.getNode(me).children.empty
		}
		
		def isContained(Node node) {
			isContained.exists [ other | node.modelElementEquals(other) ]
		}
		
		def isConnectedIncoming(Edge edge) {
			isConnectedIncoming.exists [ other | edge.modelElementEquals(other) ]
		}
		
		def isConnectedOutgoing(Edge edge) {
			isConnectedOutgoing.exists [ other | edge.modelElementEquals(other) ]
		}
		
		def isOutdated(MGLModel mgl) {
			val project = mgl.file.project
			val cpds = project.findFiles("cpd")
			val mgls = project.findFiles("mgl")
			return (cpds + mgls).exists [ lastModified > timeStamp ]
		}
		
	}
	
}

package de.jabc.cinco.meta.plugin.ocl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EPackage;

import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin;
import mgl.Annotation;
import mgl.MGLModel;
import productDefinition.CincoProduct;

public class OCLMetaPlugin implements IMGLMetaPlugin {
	
	@Override
	public void executeMGLMetaPlugin(List<Annotation> mglAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject,
	                                 Map<MGLModel, EPackage> ePackages) {
		for (var mglModel: allMGLs) {
			var ePackage = ePackages.get(mglModel);
			for (var graphModel: mglModel.getGraphModels()) {
				try {
					new AddOCLConstraints(graphModel, ePackage).addOCLConstraints();
					OCLPluginProjectCreater.createPlugin(mglModel);
				}
				catch (IOException | CoreException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void executeMGLMetaPlugin(List<Annotation> mglAnnotations, List<MGLModel> generatedMGLs,
			List<MGLModel> allMGLs, CincoProduct cpd, IProject mainProject) {
		// Intentionally left blank
	}
	
}

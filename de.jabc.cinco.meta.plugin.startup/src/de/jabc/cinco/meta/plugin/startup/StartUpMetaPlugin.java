/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.startup;

import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.addAttribute;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.addExtension;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.addSubElement;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.openPluginXMLDocument;
import static de.jabc.cinco.meta.core.utils.PluginXMLEditor.save;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin;
import mgl.MGLModel;
import productDefinition.Annotation;
import productDefinition.CincoProduct;

public class StartUpMetaPlugin implements ICPDMetaPlugin {

	public StartUpMetaPlugin() {
		// Intentionally left blank
	}
	
	@Override
	public void executeCPDMetaPlugin(List<Annotation> cpdAnnotations,
	                                 List<MGLModel> generatedMGLs,
	                                 List<MGLModel> allMGLs,
	                                 CincoProduct cpd,
	                                 IProject mainProject) {
		String projectName = mainProject.getName();
		for (var annotation: cpdAnnotations) {
			if (annotation.getName().equals("startup")) {
				Document pluginXML = openPluginXMLDocument(mainProject);
				for (String value: annotation.getValue()) {
					Element extension = addExtension(pluginXML, "de.jabc.cinco.meta.runtime.startup");
					Element startupClass = addSubElement(extension, "startupclass");
					addAttribute(startupClass, "cincoProductID", projectName);
					addAttribute(startupClass, "class", value);
				}
				save(pluginXML, mainProject, new NullProgressMonitor());
			}
		}
	}
	
}

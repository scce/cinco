/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.startup;

import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult;
import de.jabc.cinco.meta.plugin.validation.SimpleValidator;
import mgl.Annotation;

public class Validator extends SimpleValidator {
		Pattern pattern;
	public Validator() {
		String regex = "[a-zA-Z$][A-Za-z0-9_$]*(\\.[a-zA-Z$][A-Za-z0-9_$]*)*";
		
		pattern = Pattern.compile(regex);
	}

	@Override
	public String getProjectAnnotation() {
		
		return "startup";
	}

	@Override
	public ValidationResult<String, EStructuralFeature> checkProjectAnnotation(Annotation annot) {
		if(annot.getValue().size()<1){
			return newError("'@startup' Annotation must have at least 1 value'");
		}else {
			for(String value: annot.getValue()) {
				if(!pattern.matcher(value).matches()) {
					return newError(String.format("'%s' is not a valid fully qualified Class Name", value));
				}
			}
			
		}
		
		return super.checkProjectAnnotation(annot);
	}

}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.primeviewer.validation

import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator
import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult
import de.jabc.cinco.meta.core.utils.xapi.GraphModelExtension
import mgl.Annotation
import mgl.MGLModel
import mgl.ReferencedEClass
import mgl.ReferencedModelElement
import mgl.ReferencedType
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.*

class PrimeViewerValidator implements IMetaPluginValidator {
	
	extension GraphModelExtension = new GraphModelExtension

	override checkOnEdit(EObject obj) {
		if (obj instanceof Annotation) {
			switch obj.name {
				case "primeviewer":     obj.checkPrimeviewer
				case "pvLabel":         obj.checkPvLabel
				case "pvFileExtension": obj.checkPvFileExtension
			}
		}
	}
	
	def ValidationResult<String, EStructuralFeature> checkPrimeviewer(Annotation annotation) {
		if (annotation.value !== null && annotation.value.size != 0) {
			return newError(
				"The @primeviewer annotation does not expect any parameters.",
				annotation.eClass.getEStructuralFeature("value")
			)
		}
		if (!annotation.pvFileExtensionAnnotationExists) {
			return newWarning(
				'''
					This MGL does not contain any @pvFileExtension(...) annotations.
					The Prime Viewer meta plugin will have no effect.
					Add @pvFileExtension(...) to a prime reference to display it in the Prime Viewer.
				'''
				.toString.trim,
				annotation.eClass.getEStructuralFeature("value")
			)
		}
	}

	def ValidationResult<String, EStructuralFeature> checkPvLabel(Annotation annotation) {
		if (annotation.value === null || annotation.value.size != 1) {
			return newError(
				"Provide exactly one valid attribute as label, e.g. @pvLabel(name)",
				annotation.eClass.getEStructuralFeature("name")
			)
		}
		if (annotation.parent instanceof ReferencedType) {
			val attrName = annotation.value.head
			val refType = annotation.parent as ReferencedType
			val valid = switch it:refType {
				ReferencedEClass:       type.getEStructuralFeature(attrName) !== null
				ReferencedModelElement: type.allAttributes.map[name].exists[it == attrName]
			}
			if (!valid) {
				return newError(
					'''«attrName» is not a valid attribute of the referenced type «refType.name»''',
					annotation.eClass.getEStructuralFeature("value")
				)
			}
		}
		if (!annotation.primeviewerAnnotationExists) {
			return newWarning(
				"Missing @primeviewer annotation. To activate the Prime Viewer meta plugin, add @primeviewer to the graph model.",
				annotation.eClass.getEStructuralFeature("name")
			)
		}
		if (!annotation.parent.hasAnnotation("pvFileExtension")) {
			return newWarning(
				'''
					The prime reference "«(annotation.parent as ReferencedType).name»" has no @pvFileExtension(...) annotation.
					To display it in the Prime Viewer, you must add @pvFileExtension(...) to the prime reference.
				'''
				.toString.trim,
				annotation.eClass.getEStructuralFeature("name")
			)
		}
	}

	def ValidationResult<String, EStructuralFeature> checkPvFileExtension(Annotation annotation) {
		if (annotation.value === null || annotation.value.size != 1) {
			return newError(
				'Provide exactly one file extension, e.g. @pvFileExtension("value")',
				annotation.eClass.getEStructuralFeature("name")
			)
		}
		if (!annotation.primeviewerAnnotationExists) {
			return newWarning(
				"Missing @primeviewer annotation. To activate the Prime Viewer meta plugin, add @primeviewer to the graph model.",
				annotation.eClass.getEStructuralFeature("name")
			)
		}
	}
	
	private def MGLModel getMglModel(Annotation annotation) {
		var EObject parent = annotation.parent
		while (parent !== null && !(parent instanceof MGLModel)) {
			parent = parent.eContainer
		}
		return parent as MGLModel
	}
	
	private def boolean pvFileExtensionAnnotationExists(Annotation annotation) {
		annotation
			.mglModel
			.nodes
			.map[primeReference]
			.filterNull
			.exists[hasAnnotation("pvFileExtension")]
	}
	
	private def boolean primeviewerAnnotationExists(Annotation annotation) {
		annotation
			.mglModel
			.graphModels
			.exists[hasAnnotation("primeviewer")]
	}
	
}

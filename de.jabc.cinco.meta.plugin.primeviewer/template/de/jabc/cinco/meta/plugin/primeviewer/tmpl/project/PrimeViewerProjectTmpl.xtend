package de.jabc.cinco.meta.plugin.primeviewer.tmpl.project

import de.jabc.cinco.meta.core.event.util.EventCoreExtension
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils
import de.jabc.cinco.meta.plugin.primeviewer.tmpl.file.ActivatorTmpl
import de.jabc.cinco.meta.plugin.primeviewer.tmpl.file.ContentProviderTmpl
import de.jabc.cinco.meta.plugin.primeviewer.tmpl.file.LabelProviderTmpl
import de.jabc.cinco.meta.plugin.primeviewer.tmpl.file.PluginXmlTmpl
import de.jabc.cinco.meta.plugin.primeviewer.tmpl.file.ProviderHelperTmpl
import de.jabc.cinco.meta.plugin.template.ProjectTemplate

class PrimeViewerProjectTmpl extends ProjectTemplate {
	
	extension EventCoreExtension = new EventCoreExtension
	extension GeneratorUtils = GeneratorUtils.instance
	
	override _projectName() {
		'''«mainProject.name».primeviewer'''
	}
	
	override _projectDescription() {
		project [
			folder("src") [
				pkg(basePackage) [
					file(ActivatorTmpl)
				]
				forEachOf(primeNodes) [ node |
					pkg('''«basePackage».«node.primeTypePackagePrefix»''') [
						file(new ContentProviderTmpl(node))
						file(new LabelProviderTmpl(node))
						file(new ProviderHelperTmpl(node))
					]
				]
			]
			file(new PluginXmlTmpl(basePackage))
			activator = '''«basePackage».Activator'''
			lazyActivation = true
			requiredBundles = #[
				mainProject.name,
				eventCorePluginID,
				"org.eclipse.core.resources",
				"org.eclipse.core.runtime",
				"org.eclipse.emf.common",
				"org.eclipse.emf.ecore",
				"org.eclipse.ui",
				"org.eclipse.ui.navigator",
				"org.eclipse.xtend.lib"
			]
			binIncludes = #[
				"plugin.xml"
			]
		]
	}
	
	def primeNodes() {
		allMGLs
			.flatMap[nodes]
			.filter[primeReference !== null && primeReference.hasAnnotation("pvFileExtension")]
	}
	
}

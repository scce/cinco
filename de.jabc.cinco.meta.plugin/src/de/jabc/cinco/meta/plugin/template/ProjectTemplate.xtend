/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.template

import de.jabc.cinco.meta.plugin.CincoMetaContext
import de.jabc.cinco.meta.plugin.dsl.ProjectDescription
import de.jabc.cinco.meta.plugin.dsl.ProjectDescriptionLanguage

abstract class ProjectTemplate extends CincoMetaContext {
	
	extension protected ProjectDescriptionLanguage = new ProjectDescriptionLanguage
	
	var String projectName
	var ProjectDescription projectDescription
	
	abstract def protected ProjectDescription _projectDescription()
	abstract def protected String _projectName()
	
	def void init() {
		// Override in sub classes
	}
	
	def createProject() {
		init
		getProjectDescription()?.withContext(this).create()
	}
	
	def getProjectDescription() {
		if (projectDescription === null) {
			projectDescription = _projectDescription()
		}
		return projectDescription
	}
	
	def protected getProjectDescription(Class<? extends ProjectTemplate> templateClass) {
		templateClass.newInstanceWithContext(this).getProjectDescription()
	}
	
	def getProjectName() {
		if (projectName === null) {
			projectName = _projectName()
		}
		return projectName
	}
	
	def protected getProjectName(Class<? extends ProjectTemplate> templateClass) {
		templateClass.newInstanceWithContext(this).getProjectName()
	}
	
	def getBasePackage() {
		getProjectName()
	}
	
	def protected getBasePackage(Class<? extends ProjectTemplate> templateClass) {
		templateClass.newInstanceWithContext(this).getBasePackage()
	}
	
	def subPackage(String suffix) {
		getBasePackage() + suffix.withDotPrefix
	}
	
	def private withDotPrefix(String name) {
		switch it: name {
			case nullOrEmpty:     ''
			case startsWith('.'): name
			default:              '.' + name
		}
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.template

import de.jabc.cinco.meta.plugin.CincoMetaContext
import de.jabc.cinco.meta.plugin.dsl.FileContainerDescription
import de.jabc.cinco.meta.plugin.dsl.FileDescription
import de.jabc.cinco.meta.plugin.dsl.FolderDescription
import de.jabc.cinco.meta.plugin.dsl.PackageDescription
import de.jabc.cinco.meta.plugin.dsl.ProjectDescription
import org.eclipse.xtend.lib.annotations.Accessors

abstract class FileTemplate extends CincoMetaContext {
	
	@Accessors FileContainerDescription<?> parent
	@Accessors ProjectDescription project
	
	new() {/* empty constructor for instantiation via reflection */}
	
	def CharSequence getContent() {
		init
		template
	}
	
	def void init() {/* override in sub classes */}
	
	abstract def String getTargetFileName()
	
	protected def getTargetFileName(Class<? extends FileTemplate> tmplClass) {
		tmplClass.newInstanceWithContext(this).targetFileName
	}
	
	def CharSequence template()
	
	def FileDescription getFileDescription() {
		new FileDescription(class)
	}
	
	def getBasePackage() {
		switch it:project {
			ProjectDescription: name
			default: package
		}
	}
	
	def getPackage() {
		switch it:parent {
			PackageDescription: name
			FolderDescription: hierarchy.filter(FolderDescription).toList.reverse.join('.')
			default: ''
		}
	}
}

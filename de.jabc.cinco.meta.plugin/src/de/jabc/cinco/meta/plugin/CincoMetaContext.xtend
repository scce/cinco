/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin

import de.jabc.cinco.meta.core.utils.MGLUtil
import de.jabc.cinco.meta.core.utils.xapi.GraphModelExtension
import de.jabc.cinco.meta.util.xapi.CodingExtension
import de.jabc.cinco.meta.util.xapi.CollectionExtension
import de.jabc.cinco.meta.util.xapi.WorkspaceExtension
import java.util.List
import mgl.GraphModel
import mgl.MGLModel
import org.eclipse.core.resources.IProject
import org.eclipse.xtend.lib.annotations.Accessors
import productDefinition.CincoProduct

// Protected getters prevent confusion between 'this' and 'it' in ProjectDescriptionLanguage
@Accessors(PROTECTED_GETTER, PUBLIC_SETTER)
abstract class CincoMetaContext {
	
	// Extensions for use in the sub-classes
	protected extension CodingExtension     = new CodingExtension
	protected extension CollectionExtension = new CollectionExtension
	protected extension GraphModelExtension = new GraphModelExtension
	protected extension MGLUtil             = new MGLUtil
	protected extension WorkspaceExtension  = new WorkspaceExtension
	
	/** A reference to the relevant MGLModel. */
	var MGLModel model
	
	/** A reference to the relevant GraphModel. */
	var GraphModel graphModel
	
	/** A list of all related annotations from all MGLs */
	var List<mgl.Annotation> mglAnnotations
	
	/** A list of all related annotations from the CPD */
	var List<productDefinition.Annotation> cpdAnnotations
	
	/** A list of newly generated MGLs */
	var List<MGLModel> generatedMGLs
	
	/** A list of all MGLs used in this Cinco product */
	var List<MGLModel> allMGLs
	
	/** The Cinco product descriptor */
	var CincoProduct cpd
	
	/** The main Cinco product project, that contains the CPD file */
	var IProject mainProject
	
	/**
	 * Default constructor used for instantiation via reflection.
	 */
	new () {
		// Intentionally left blank
	}
	
	/**
	 * Set the attributes of this {@code CincoMetaContext} to the attributes of
	 * {@code otherContext}.
	 */
	def void setContext(CincoMetaContext otherContext) {
		if (otherContext !== null) {
			this.model          = otherContext.model?:          this.model
			this.graphModel     = otherContext.graphModel?:     this.graphModel
			this.mglAnnotations = otherContext.mglAnnotations?: this.mglAnnotations
			this.cpdAnnotations = otherContext.cpdAnnotations?: this.cpdAnnotations
			this.generatedMGLs  = otherContext.generatedMGLs?:  this.generatedMGLs
			this.allMGLs        = otherContext.allMGLs?:        this.allMGLs
			this.cpd            = otherContext.cpd?:            this.cpd
			this.mainProject    = otherContext.mainProject?:    this.mainProject
		}
	}
	
	/**
	 * Set the attributes of this {@code CincoMetaContext} to the attributes of
	 * {@code otherContext}, if the attribute is of this context is not yet set
	 * ({@code this.attr == null}).
	 */
	def void setContextIfNotAlreadySet(CincoMetaContext otherContext) {
		if (otherContext !== null) {
			this.model          = this.model?:          otherContext.model
			this.graphModel     = this.graphModel?:     otherContext.graphModel
			this.mglAnnotations = this.mglAnnotations?: otherContext.mglAnnotations
			this.cpdAnnotations = this.cpdAnnotations?: otherContext.cpdAnnotations
			this.generatedMGLs  = this.generatedMGLs?:  otherContext.generatedMGLs
			this.allMGLs        = this.allMGLs?:        otherContext.allMGLs
			this.cpd            = this.cpd?:            otherContext.cpd
			this.mainProject    = this.mainProject?:    otherContext.mainProject
		}
	}
	
	/**
	 * Set the attributes of {@code context} to the attributes of {@code otherContext}
	 * and return {@code context}.
	 */
	def static <T extends CincoMetaContext> T withContext(T context, CincoMetaContext otherContext) {
		if (context !== null) {
			context.setContext(otherContext)
		}
		return context
	}
	
	/**
	 * Create a new instance of {@code contextClass} (using the default constructor),
	 * set its attributes to the attributes of {@code otherContext}
	 * and return the new instance.
	 */
	def static <T extends CincoMetaContext> T newInstanceWithContext(Class<T> contextClass, CincoMetaContext otherContext) {
		val context = contextClass
			.constructors
			.findFirst[parameterCount == 0]
			.newInstance as T
		context.setContext(otherContext)
		return context
	}
	
}

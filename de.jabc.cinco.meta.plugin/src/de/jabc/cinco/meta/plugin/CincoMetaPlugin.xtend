/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin

import de.jabc.cinco.meta.core.utils.BundleRegistry
import de.jabc.cinco.meta.core.pluginregistry.ICPDMetaPlugin
import de.jabc.cinco.meta.core.pluginregistry.IMGLMetaPlugin
import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import java.util.List
import mgl.MGLModel
import org.eclipse.core.resources.IProject
import productDefinition.CincoProduct

abstract class CincoMetaPlugin extends CincoMetaContext implements IMGLMetaPlugin, ICPDMetaPlugin {
	
	abstract def void executeCincoMetaPlugin()
	
	override executeMGLMetaPlugin(List<mgl.Annotation> mglAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		try {
			this.mglAnnotations = mglAnnotations
			this.generatedMGLs  = generatedMGLs
			this.allMGLs        = allMGLs
			this.cpd            = cpd
			this.mainProject    = mainProject
			executeCincoMetaPlugin
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
	override executeCPDMetaPlugin(List<productDefinition.Annotation> cpdAnnotations,
	                              List<MGLModel> generatedMGLs,
	                              List<MGLModel> allMGLs,
	                              CincoProduct cpd,
	                              IProject mainProject) {
		try {
			this.cpdAnnotations = cpdAnnotations
			this.generatedMGLs  = generatedMGLs
			this.allMGLs        = allMGLs
			this.cpd            = cpd
			this.mainProject    = mainProject
			executeCincoMetaPlugin
		}
		catch (Exception e) {
			e.printStackTrace
		}
	}
	
	def IProject create(ProjectTemplate projectTemplate) {
		projectTemplate.setContextIfNotAlreadySet(this)
		val project = projectTemplate.createProject()
		if (project !== null) {
			BundleRegistry.INSTANCE.addBundle(project.name, false,true)
		}
		return project
	}
	
	def <T extends ProjectTemplate> IProject create(Class<T> projectTemplateClass) {
		val projectTemplate = projectTemplateClass
			.constructors
			.findFirst[parameterCount == 0]
			.newInstance as T
		return create(projectTemplate)
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.dsl

import de.jabc.cinco.meta.plugin.template.FileTemplate
import java.util.function.Consumer
import org.eclipse.core.resources.IFile
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.core.runtime.Path

class FileDescription extends ProjectResourceDescription<IFile> {
	@Accessors CharSequence content
	@Accessors Class<? extends FileTemplate> templateClass
	@Accessors Consumer<IFile> postProcessing
	@Accessors FileTemplate template
	@Accessors boolean overwrite = true
	
	new(String name) { super(name) }
	
	new(Class<? extends FileTemplate> templateClass) {
		this(templateClass.simpleName, templateClass)
	}
	
	new(FileTemplate template) {
		this(template.class)
		this.template = template
	}
	
	new(String name, Class<? extends FileTemplate> templateClass) {
		super(name)
		this.templateClass = templateClass
	}
	
	new(String name, FileTemplate template) {
		this(name, template.class)
		this.template = template
	}
	
	new(String name, CharSequence content) {
		super(name)
		this.content = content
	}
	
	override IFile create() {
		val content = getContent
		if (content !== null)
			return createFile(name, content)
		
		var template = getTemplate
		if (template === null)
			template = this.template = templateClass.createNewInstance
		
		if (template !== null) template => [
			it.cpd = this.cpd
			it.model = this.model
			it.graphModel = this.graphModel
			it.parent = this.parent
			it.project = this.project
			createFile(targetFileName, it.content)
		]
		
		if (IResource === null)
			warn("Nothing to create: content is null and no template provided")
		return IResource
	}
	
	protected def createFile(String fileName, CharSequence content) {
		val targetFile = parent.IResource.getFile(new Path(fileName))
		if (overwrite || targetFile === null || !targetFile.exists) {
			val file = parent.IResource.createFile(fileName, content)
			IResource = file
			postProcessing?.accept(file)
		}
		return IResource
	}
	
	private def <T> T createNewInstance(Class<T> clazz, Object ... parameters) {
		clazz
			?.constructors
			?.findFirst [ constructor | constructor.parameterCount == parameters.size ]
			?.newInstance(parameters) as T
	}
}

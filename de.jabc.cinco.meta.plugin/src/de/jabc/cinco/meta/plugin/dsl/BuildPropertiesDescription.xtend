/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.dsl

import java.util.Set
import org.eclipse.xtend.lib.annotations.Accessors

class BuildPropertiesDescription extends FileDescription {
	
	@Accessors val Set<String> binIncludes = newHashSet
	@Accessors val Set<String> additionalBundles = newHashSet
	
	new() {
		super("build.properties")
	}
	
	override getContent() '''
		source.. = «project.sourceFolders.map[name + '/'].join(', ')»
		bin.includes = ., META-INF/«binIncludes.map[''', «it»'''].join»
		«"additional.bundles".withValues(additionalBundles)»
	'''
	
	private def withValues(String name, Iterable<String> names) {
		if (!names.nullOrEmpty)
			names.join('''«name» = ''', ", ", "", [it])
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.dsl

import java.io.FileNotFoundException
import org.eclipse.core.resources.IFile
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class FileCopyDescription extends FileDescription {
	
	var String targetFileName
	var String sourcePath
	var BasePath relativeToBasePath
	
	new(String name) {
		super(name)
	}
	
	new(String targetFileName, String sourcePath, BasePath relativeToBasePath) {
		super(targetFileName)
		this.targetFileName = targetFileName
		this.sourcePath = sourcePath
		this.relativeToBasePath = relativeToBasePath
	}
	
	override IFile create() {
		val sourceBasePath = switch (relativeToBasePath) {
			case WORKSPACE_ROOT: workspaceRoot
			case PROJECT:        parent.IResource.project
			case FOLDER:         parent.IResource
		}
		val sourceFile = sourceBasePath.getFile(sourcePath.toPath)
		if (sourceFile === null || !sourceFile.exists) {
			throw new FileNotFoundException('''File "«sourceBasePath.fullPath.append(sourcePath)»" does not exist''')
		}
		val targetFile = parent.IResource.getFile(name.toPath)
		if (overwrite || targetFile === null || !targetFile.exists) {
			IResource = parent.IResource.createFile(name, sourceFile.contents)
			postProcessing?.accept(IResource)
		}
		return IResource
	}
	
}

enum BasePath {
	WORKSPACE_ROOT,
	PROJECT,
	FOLDER
}
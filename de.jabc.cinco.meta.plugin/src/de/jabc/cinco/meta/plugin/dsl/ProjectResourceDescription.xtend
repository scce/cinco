/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.dsl

import de.jabc.cinco.meta.plugin.CincoMetaContext
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.NullProgressMonitor

abstract class ProjectResourceDescription<T extends IResource> extends CincoMetaContext {
	@Accessors String name
	@Accessors String symbolicName
	@Accessors ProjectDescription project
	@Accessors FileContainerDescription<?> parent
	@Accessors(PUBLIC_GETTER, PROTECTED_SETTER) T iResource
	
	new(String name) {
		setName(name)
	}
	
	protected def T create()
	
	def T create(FileContainerDescription<?> parent) {
		this.withParent(parent).create
	}
	
	def void setName(String name) {
		this.name = name
		if (name !== null) {
			var it = name.replaceAll("[^a-zA-Z0-9_\\-\\.]", "_")
			if (endsWith("."))
				it = substring(0, lastIndexOf('.')) + "_"
			if (startsWith(".")) 
				it = "_" + substring(1);
			symbolicName = it
		}
	}
	
	def withParent(FileContainerDescription<?> parent) {
		withContext(parent)
		setParent(parent)
		if (parent instanceof ProjectDescription) {
			setProject(parent as ProjectDescription)
		}
		else {
			setProject(parent.getProject)
		}
		return this
	}
	
	protected def getMonitor() {
		new NullProgressMonitor
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.dsl

import java.util.List
import org.eclipse.core.resources.IFolder
import org.eclipse.core.runtime.Path
import org.eclipse.xtend.lib.annotations.Accessors

class FolderDescription extends FileContainerDescription<IFolder> {
	@Accessors boolean isSourceFolder = true
	@Accessors List<PackageDescription> packages = newArrayList
	
	new(String name) { super(name) }
	
	new(FileContainerDescription<?> parent, String name) {
		this(name)
		this.parent = parent
		if (parent instanceof ProjectDescription) {
			this.project = parent
		}
	}
	
	override create() {
		IResource = (parent ?: project)?.IResource.createFolder(name)
		createFiles
		createFolders
		createPackages
		return IResource
	}
	
	def createPackages() {
		packages.forEach[ pkgDesc |
			if (pkgDesc.isDeleteIfExistent) {
				IResource.getFolder(new Path(pkgDesc.name.replace(".", "/"))) => [ folder |
					if (folder?.exists) folder.delete(true, monitor)
				]
			}
			pkgDesc.withParent(this).create
		]
	}
	
	def setIsSourceFolder(boolean flag) {
		this => [isSourceFolder = flag]
	}
}

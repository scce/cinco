/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.dsl

import de.jabc.cinco.meta.plugin.template.FileTemplate
import de.jabc.cinco.meta.plugin.template.ProjectTemplate
import java.util.Map
import java.util.function.Consumer
import org.eclipse.core.resources.IFile

class ProjectDescriptionLanguage {
	
	// Project Types
	public val JAVA    = ProjectType.JAVA
	public val PLUGIN  = ProjectType.PLUGIN
	public val FEATURE = ProjectType.FEATURE
	
	// Base Paths
	public val WORKSPACE_ROOT = BasePath.WORKSPACE_ROOT
	public val PROJECT        = BasePath.PROJECT
	public val FOLDER         = BasePath.FOLDER
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, CharSequence contents) {
		file(container, fileName, true, contents, null)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, boolean overwrite, CharSequence contents) {
		file(container, fileName, overwrite, contents, null)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, CharSequence contents, (IFile) => void postProcessing) {
		file(container, fileName, true, contents, postProcessing)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, boolean overwrite, CharSequence contents, (IFile) => void postProcessing) {
		container => [ 
			add(new FileDescription(fileName) => [
				it.overwrite = overwrite
				it.content = contents
				it.postProcessing = postProcessing.nullSaveConvesion
			])
		]
	}
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, () => CharSequence template) {
		file(container, fileName, true, template, null)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, () => CharSequence template, (IFile) => void postProcessing) {
		file(container, fileName, true, template, postProcessing)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, String fileName, boolean overwrite, () => CharSequence template, (IFile) => void postProcessing) {
		val _template = template // Rename parameter to avoid name collision
		container => [ 
			add(new FileDescription(fileName) => [
				it.overwrite = overwrite
				it.template = new FileTemplate() {
					override getTargetFileName() { fileName        }
					override template()          { _template.apply }
				}
				it.postProcessing = postProcessing.nullSaveConvesion
			])
		]
	}
	
	def <T extends FileContainerDescription<?>> file(T container, Class<? extends FileTemplate> template) {
		file(container, template, true)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, Class<? extends FileTemplate> template, boolean overwrite) {
		file(container, template, overwrite, null)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, Class<? extends FileTemplate> template, (IFile)=>void postProcessing) {
		file(container, template, true, postProcessing)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, Class<? extends FileTemplate> template, boolean overwrite, (IFile)=>void postProcessing) {
		container => [ 
			add(new FileDescription(template) => [
				it.overwrite = overwrite
				it.postProcessing = postProcessing.nullSaveConvesion
			])
		]
	}
	
	def <T extends FileContainerDescription<?>> file(T container, FileTemplate template) {
		file(container, template, true)
	}
	
	def <T extends FileContainerDescription<?>> file(T container, FileTemplate template, boolean overwrite) {
		container => [ add(new FileDescription(template) => [it.overwrite = overwrite]) ]
	}
	
	def <T extends FileContainerDescription<?>> file(T container, Pair<CharSequence, Class<? extends FileTemplate>> template) {
		container => [ add(new FileDescription(template.value?.toString, template.key)) ]
	}
	
	def <T extends FileContainerDescription<?>> setFiles(T container, Class<? extends FileTemplate>[] templates) {
		container => [ templates.map[new FileDescription(it)].forEach[container.add(it)] ]
	}
	
	def <T extends FileContainerDescription<?>> setFiles(T container, Iterable<FileTemplate> templates) {
		container => [ templates.map[new FileDescription(it)].forEach[container.add(it)] ]
	}
	
	def <T extends FileContainerDescription<?>> setFiles(T container, Pair<CharSequence, Class<? extends FileTemplate>>[] templates) {
		container => [ templates.map[new FileDescription(value?.toString, key)].forEach[container.add(it)] ]
	}
	
	def <T extends FileContainerDescription<?>> filesFromBundle(T container, Pair<String,String> bundleSource) {
		container => [
			filesFromBundles.add(bundleSource)
		]
	}
	
	def <T extends FileContainerDescription<?>> filesFromProject(T container, Pair<String,String> bundleSource) {
		container => [
			filesFromProjects.add(bundleSource)
		]
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath) {
		copyFile(container, targetFileName, sourcePath, WORKSPACE_ROOT, true, null)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, boolean overwrite) {
		copyFile(container, targetFileName, sourcePath, WORKSPACE_ROOT, overwrite, null)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, (IFile) => void postProcessing) {
		copyFile(container, targetFileName, sourcePath, WORKSPACE_ROOT, true, postProcessing)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, boolean overwrite, (IFile) => void postProcessing) {
		copyFile(container, targetFileName, sourcePath, WORKSPACE_ROOT, true, postProcessing)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, BasePath relativeToBasePath) {
		copyFile(container, targetFileName, sourcePath, relativeToBasePath, true, null)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, BasePath relativeToBasePath, boolean overwrite) {
		copyFile(container, targetFileName, sourcePath, relativeToBasePath, overwrite, null)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, BasePath relativeToBasePath, (IFile) => void postProcessing) {
		copyFile(container, targetFileName, sourcePath, relativeToBasePath, true, postProcessing)
	}
	
	def <T extends FileContainerDescription<?>> copyFile(T container, String targetFileName, String sourcePath, BasePath relativeToBasePath, boolean overwrite, (IFile) => void postProcessing) {
		container => [
			add(new FileCopyDescription(targetFileName, sourcePath, relativeToBasePath) => [
				it.overwrite = overwrite
				it.postProcessing = postProcessing.nullSaveConvesion
			])
		]
	}
	
	def <T extends FileContainerDescription<?>> folder(T container, String name, (FolderDescription)=>FolderDescription struct) {
		container => [
			add(new FolderDescription(container, name) => [ 
				struct.apply(it)
			])
		]
	}
	
	def pkg(FolderDescription folder, (PackageDescription) => PackageDescription struct) {
		pkg(folder, folder.project.template.projectName, struct)
	}
	
	def pkg(FolderDescription folder, String name, (PackageDescription) => PackageDescription struct) {
		folder => [ 
			packages.add(new PackageDescription(name) => [
				struct.apply(it)
			])
		]
	}
	
	def <T extends ProjectResourceDescription<?>,X> forEachOf(T container, Iterable<X> iterable, (X) => T struct) {
		container => [ 
			iterable.forEach[x | struct.apply(x)]
		]
	}
	
	def <T extends ProjectResourceDescription<?>, K, V> forEachOf(T container, Map<K, V> map, (K, V) => T struct) {
		container => [ 
			map.forEach[key, value | struct.apply(key, value)]
		]
	}
	
	/**
	 * Language construct to define a new project and its inherent structure.
	 * <p>Example usage:
	 * <p><pre>
	 *   project ("fully.qualified.project.name") [ ... ]
	 * </pre></p>
	 * </p>
	 * 
	 * @param tmpl  The project template that is used to retrieve the name of the project
	 *   via getProjectName, typically 'model.package + projectSuffix'.
	 * @param struct  The nested language block that defines the inherent project structure.
	 */
	def project(ProjectTemplate tmpl, (ProjectDescription) => ProjectDescription struct) {
		struct.apply(new ProjectDescription(tmpl))
	}
	
	def project(ProjectTemplate tmpl, String name, (ProjectDescription) => ProjectDescription struct) {
		struct.apply(new ProjectDescription(tmpl, name))
	}
	
	/**
	 * Language construct to define a new project and its inherent structure.
	 * <p>Example usage:
	 * <p><pre>
	 *   project ("fully.qualified.project.name") [ ... ]
	 * </pre></p>
	 * </p>
	 * 
	 * @param name  The name of the project. Typically, only a suffix is provided that
	 *   is used by the generator to create a fully qualified project name following the
	 *   convention 'model.package + projectSuffix'. To do so, use the method 'suffix'
	 *   instead of providing the full name. As an example, for a Cinco product project
	 *   named {@code info.scce.cinco.product.somegraph} the following declaration 
	 *   <p>{@code project (suffix("plugin.name")) [ ... ]}</p>
	 *   would lead to the name {@code info.scce.cinco.product.somegraph.plugin.name}
	 *   for the generated project.
	 * @param struct  The nested language block that defines the inherent project structure.
	 */
	def project(String name, (ProjectDescription) => ProjectDescription struct) {
		struct.apply(new ProjectDescription(name))
	}
	
	def setBinIncludes(ProjectDescription projDesc, String[] names) {
		projDesc => [ buildProperties.binIncludes => [clear addAll(names)] ]
	}

	def setAdditionalBundles(ProjectDescription projDesc, String[] names) {
		projDesc => [ buildProperties.additionalBundles => [clear addAll(names)] ]
	}
	
	def setExportedPackages(ProjectDescription projDesc, String[] names) {
		projDesc => [ manifest.exportedPackages => [clear addAll(names)] ]
	}
	
	def setImportedPackages(ProjectDescription projDesc, String[] names) {
		projDesc => [ manifest.importedPackages => [clear addAll(names)] ]
	}

	def setRequiredBundles(ProjectDescription projDesc, String[] names) {
		projDesc => [ manifest.requiredBundles => [clear addAll(names)] ]
	}
	
	def setActivator(ProjectDescription projDesc, CharSequence activatorFqn) {
		projDesc => [ manifest.activator = activatorFqn?.toString ]
	}
	
	def setLazyActivation(ProjectDescription projDesc, boolean flag) {
		projDesc => [ manifest.lazyActivation = flag ]
	}
	
	/**
	 * Null check is needed, because Xtend converts {@code (T) => void} to
	 * {@code Consumer<T>} in a non-null-safe way
	 */
	private def <T> Consumer<T> nullSaveConvesion((T) => void procedure) {
		if (procedure === null) {
			return null
		}
		else {
			return procedure
		}
	}
	
}

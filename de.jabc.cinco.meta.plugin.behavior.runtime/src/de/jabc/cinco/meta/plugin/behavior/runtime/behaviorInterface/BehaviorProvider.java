/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.behavior.runtime.behaviorInterface;

import graphmodel.Container;
import graphmodel.Node;

/**
 *  Interface that specifies how a behavior provider for the MetaPlugin 
 *  behaviorModification works. Classes which are passed to the plugin
 *  with the annotation behavior have to implement this Interface.
 *  The implementing class should specify a behavior for a container, that is triggered,
 *  whenever a node enters or leaves that container, including when it is created in that container
 *  but excluding when it is deleted there.  
 */
public interface BehaviorProvider{
	/**
	 * Method which is called whenever a node leaves the specified container.
	 * @param leftNode node that left source
	 * @param source the specified container
	 */
	public void hasLeft(Node leftNode, Container source);
	/**
	 * Method which is called whenever a node enters the specified container.
	 * @param enteredNode node that entered target
	 * @param target the specified container
	 */
	public void hasEntered(Node enteredNode, Container target);
	
}

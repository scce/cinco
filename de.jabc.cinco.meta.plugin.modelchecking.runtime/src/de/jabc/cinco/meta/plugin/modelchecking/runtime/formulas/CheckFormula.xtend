/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas

import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.Result
import graphmodel.Node
import java.util.List
import java.util.Map.Entry

class CheckFormula {
	String expression
	String fullExpression
	String description
	String varName
	boolean toCheck
	Result result
	Set<Node> satisfyingNodes
	List<Entry<String,Set<Node>>> subToSatList
	List<Entry<String, Result>> subToResultList
	String errorMessage
	final String id

	new(String id) {
		this.id = id
		result = Result.NOT_CHECKED
		errorMessage = ""
		satisfyingNodes = newHashSet
		subToSatList = newLinkedList
		subToResultList = newLinkedList
		expression = ""
		fullExpression = ""
		description = ""
		varName = ""
		toCheck = true
	}

	new(String id, String expression, String varName) {
		this(id)
		setExpression(expression)
		this.varName = varName
	}

	def getErrorMessage() {
		errorMessage
	}

	def setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage
	}

	def resetCheckResult() {
		result = Result.NOT_CHECKED
		satisfyingNodes.clear
		subToSatList = newLinkedList
		subToResultList = newLinkedList
	}

	def getExpression() {
		expression
	}

	def setExpression(String expression) {
		this.result = Result.NOT_CHECKED
		this.expression = expression
	}
	
	def getFullExpression(){
		fullExpression
	}
	
	def setFullExpression(String expression){
		this.fullExpression = expression
	}

	def getDescription() {
		description
	}

	def setDescription(String description) {
		this.description = description
	}
	
	def getVarName() {
		varName
	}
	
	def setVarName(String name){
		varName = name
	}
	
	def hasVarName(){
		varName !== null && varName != ""
	}

	def isToCheck() {
		toCheck
	}

	def setToCheck(boolean toCheck) {
		this.toCheck = toCheck
	}

	def getResult() {
		result
	}

	def setResult(Result result) {
		this.result = result
	}

	def hasSatisfyingNodes() {
		!satisfyingNodes.isEmpty()
	}

	def getSatisfyingNodes() {
		satisfyingNodes
	}
	
	def getSubToSatList(){
		subToSatList
	}
	
	def setSubToSatList(List<Entry<String, Set<Node>>> list){
		if (list === null){
			subToSatList = newLinkedList
		}else{
			subToSatList = list
		}
	}
	
	def hasSubToSatList(){
		!subToSatList.isEmpty
	}
	
	def getSubToResultList(){
		subToResultList
	}
	
	def setSubToResultList(List<Entry<String, Result>> list){
		if (list === null){
			subToResultList = newLinkedList
		}else{
			subToResultList = list
		}
		
	}
	
	def hasSubToResultList(){
		!subToResultList.isEmpty
	}

	def setSatisfyingNodes(Set<Node> satisfyingNodes) {
		if (satisfyingNodes !== null) {
			this.satisfyingNodes = satisfyingNodes
		} else {
			resetCheckResult
		}
	}

	def getId() {
		id
	}
}

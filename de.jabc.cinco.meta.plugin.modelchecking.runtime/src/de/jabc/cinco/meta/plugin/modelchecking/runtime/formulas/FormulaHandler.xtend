/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas

import graphmodel.GraphModel
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import java.util.List
import graphmodel.Type
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaFactory
import org.eclipse.emf.common.util.EList
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import java.util.HashSet
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler.FormulaException
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler.FormulaException.ErrorCode

abstract class FormulaHandler<M extends GraphModel, T extends Type> {

	extension WorkbenchExtension = new WorkbenchExtension
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	val FormulaFactory<M, T> formulaFactory;
	List<CheckFormula> formulas;
	volatile boolean editing;

	new(FormulaFactory<M, T> formulaFactory) {
		this.formulaFactory = formulaFactory
		reloadFormulas();
		editing = false
	}

	def getModel() {
		activeGraphModel as M
	}

	def reloadFormulas() {
		val newFormulas = formulaFactory.createFormulas(model)
		if (formulas !== null) {
			newFormulas.forEach [
				val formula = getFormulaById(it.id)
				if (formula !== null) {
					it.result = formula.result
					it.errorMessage = formula.errorMessage
					it.satisfyingNodes = formula.satisfyingNodes
					it.varName = formula.varName
				}
			]
		}
		this.formulas = newFormulas
		reloadFullExpressions
	}

	def void reloadFullExpressions() {
		formulas.forEach [
			fullExpression = this.getFullExpression(it)
		]
	}

	def getFormulas() {
		formulas
	}

	def hasToCheck() {
		return !formulas.filter[isToCheck].isEmpty
	}

	def resultsPresent() {
		formulas.exists[result.valid]
	}

	def getFormulaById(String id) {
		formulas.findFirst[getId == id]
	}

	def getFormulaByExpression(String exp) {
		formulas.findFirst[expression.equals(exp)]
	}

	def get(int index) {
		formulas.get(index)
	}

	def getIndex(CheckFormula formula) {
		formulas.indexOf(formula)
	}

	def getIndexById(String id) {
		formulas.indexOf(id.formulaById)
	}

	def size() {
		formulas.size
	}

	def contains(String exp) {
		formulas.map[getExpression].contains(exp)
	}

	def contains(CheckFormula formula) {
		contains(formula.getExpression)
	}

	def add(String exp) {
		val varName = getFreeDefaultVarName(exp)
		val newFormula = formulaFactory.createGraphModelFormula(exp, varName)
		val newCheckFormula = formulaFactory.createFormula(newFormula)
		newCheckFormula.validateExpression(exp)

		formulas.add(newCheckFormula)
		reloadFullExpressions
		edit[addFormulaToModel(newFormula)]
	}

	def remove(CheckFormula formula) {
		formulas.remove(formula)
		reloadFullExpressions
		edit[removeFormulaFromModel(getFormulaFromModel(formula.id))]
	}

	def validateExpression(CheckFormula formula, String exp) {
		if (exp === null || exp == "") {
			throw new FormulaException(ErrorCode.INVALID)
		} else if (formulas.filter[!equals(formula)].map[expression].toSet.contains(exp)) {
			throw new FormulaException(ErrorCode.EXISTS)
		} else if (exp.hasCircularDependencies(formula.varName)) {
			throw new FormulaException(ErrorCode.CYCLE)
		}
	}

	def setExpression(CheckFormula formula, String exp) {
		formula.validateExpression(exp)
		formula.expression = exp
		reloadFullExpressions
		edit[setExpression(getFormulaFromModel(formula.id), exp)]
	}

	def setDescription(CheckFormula formula, String description) {
		formula.description = description
		edit[setDescription(getFormulaFromModel(formula.id), description)]
	}

	def setVarName(CheckFormula formula, String varName, boolean refactor) {
		if (refactor) {
			formulas.forEach [f |
				val newExpression = f.expression.replace(formula.varName, varName)
				f.expression = newExpression
				edit[setExpression(getFormulaFromModel(f.id), newExpression)]
			]
		}

		formula.varName = varName
		reloadFullExpressions
		edit[setVarName(getFormulaFromModel(formula.id), varName)]
	}

	def setToCheck(CheckFormula formula, boolean toCheck) {
		formula.toCheck = toCheck
		edit[setToCheck(getFormulaFromModel(formula.getId), toCheck)]
	}

	def getFormulaFromModel(String id) {
		formulasFromModel.findFirst[it.id == id]
	}

	def edit(Runnable runnable) {
		editing = true
		runnable.run
		editing = false
	}

	def isEditing() {
		editing
	}

	def void addFormulaToModel(T formula)

	def EList<T> getFormulasFromModel()

	def void removeFormulaFromModel(T formula)

	def void setExpression(T formula, String exp)

	def void setDescription(T formula, String description)

	def void setVarName(T formula, String varName)

	def void setToCheck(T formula, boolean toCheck)

	// variable methods
	def getFreeDefaultVarName(String exp) {
		var i = 0
		while (formulas.map[varName].contains(i.toString) || exp.hasCircularDependencies(i.toString)) {
			i++
		}
		return i.toString
	}

	def hasCircularDependencies(String exp, String varName) {
		var flatted = exp
		val fullVarName = varName.fullVarName
		while (flatted.containsVarNames || exp.contains(fullVarName)) {
			if (flatted.contains(fullVarName)) {
				return true
			} else {
				flatted = flatted.flattenExpression
			}
		}
		return false
	}

	def flattenExpression(String exp) {
		var result = exp
		var next = result.findFirstVarName

		var pos = 0

		while (next !== null) {
			val posVarName = result.substring(pos).indexOf(next)
			val nextVarName = next

			val replaceExp = formulas.findFirst[fullVarName == nextVarName].expression

			val head = result.substring(0, pos + posVarName)
			val tail = result.substring(head.length + next.length, result.length)

			result = head + replaceExp + tail

			pos = head.length + replaceExp.length

			next = result.substring(pos).findFirstVarName
		}

		result
	}

	def findFirstVarName(String exp) {
		var pos = -1
		var String name = null
		for (varName : varNames) {
			val fullVarName = varName.fullVarName
			val index = exp.indexOf(fullVarName)
			if (index >= 0 && (pos < 0 || index < pos)) {
				pos = index
				name = fullVarName
			}
		}
		return name
	}

	def getFullExpression(CheckFormula formula) {
		var result = formula.expression
		while (result.containsVarNames) {
			result = result.flattenExpression
		}
		return result
	}

	def getVarNames() {
		new HashSet<String> => [ set |
			formulas.forEach[if (hasVarName) {set.add(varName)}]
		]
	}
	
	def getFullVarNames() {
		val prefix = varNamePrefix
		varNames.map[prefix + it].toSet
	}

	def canRefactor(CheckFormula formula, String varName) {
		validateVarName(formula.expression, varName)
		formulas.filter[!equals(formula)].map[expression].exists[it.contains(formula.varName.fullVarName)]
	}

	def containsVarNames(String exp) {
		return fullVarNames.exists[exp.contains(it)]
	}

	def validateVarName(String exp, String varName) {
		if (!varName.matches("\\w*") ||
			varName.length == 0) {
			throw new FormulaException(ErrorCode.INVALID)
		} else if (formulas.map[it.varName].contains(varName)) {
			throw new FormulaException(ErrorCode.EXISTS)
		} else if (exp.hasCircularDependencies(varName)) {
			throw new FormulaException(ErrorCode.CYCLE)
		}
	}

	def getVarNamePrefix() {
		activeAdapter?.getSettings("varNamePrefix")
	}
	
	def getFullVarName(String varName){
		varNamePrefix + varName
	}
	
	def getFullVarName(CheckFormula formula){
		formula.varName.fullVarName
	}

	static class FormulaException extends Exception {
		enum ErrorCode {
			INVALID,
			EXISTS,
			CYCLE
		}

		val ErrorCode errorCode

		new(ErrorCode code) {
			errorCode = code
		}

		def getErrorCode() {
			errorCode
		}
	}

}

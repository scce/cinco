/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.SWT
import org.eclipse.jface.layout.TableColumnLayout
import org.eclipse.jface.viewers.TableViewer
import org.eclipse.swt.layout.GridData
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler
import org.eclipse.jface.viewers.ColumnLabelProvider
import org.eclipse.jface.viewers.TableViewerColumn
import org.eclipse.jface.viewers.ColumnWeightData
import org.eclipse.swt.widgets.Table
import org.eclipse.swt.widgets.Display
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.Result
import org.eclipse.jface.viewers.StyledCellLabelProvider
import org.eclipse.jface.viewers.ViewerCell
import de.jabc.cinco.meta.plugin.modelchecking.runtime.support.VarNameEditingSupport
import de.jabc.cinco.meta.plugin.modelchecking.runtime.support.FormulaEditingSupport
import de.jabc.cinco.meta.plugin.modelchecking.runtime.support.DescriptionEditingSupport
import de.jabc.cinco.meta.plugin.modelchecking.runtime.support.CheckEditingSupport

class TableViewerComposite extends Composite{
	
	val tableColumnLayout = new TableColumnLayout
	
	val ModelCheckingViewPart view
	var FormulaHandler<?,?> handler

	var TableViewerColumn colVarName
	var TableViewerColumn colFormula
	var TableViewerColumn colResult
	var TableViewerColumn colCheck
	
	var VarNameEditingSupport varNameEditingSupport
	var FormulaEditingSupport formulaEditingSupport
	var DescriptionEditingSupport descriptionEditingSupport
	var CheckEditingSupport checkEditingSupport
	
	new (Composite parent, int style, ModelCheckingViewPart view){
		super(parent, style)
		
		this.view = view

		layout = tableColumnLayout		 	
		layoutData = new GridData(SWT.FILL,SWT.FILL,true,true) 
	}
	
	def getPreferences(){
		view.preferences
	}
	

	def getViewer(){
		view.viewer
	}
	
	def setFormulaHandler(FormulaHandler<?,?> handler){
		this.handler = handler
		varNameEditingSupport.formulaHandler = handler
		formulaEditingSupport.formulaHandler = handler
		descriptionEditingSupport.formulaHandler = handler
		checkEditingSupport.formulaHandler = handler
	}
	
	def showFormulas(){
		colFormula.editingSupport = formulaEditingSupport
	}
	
	def showDescriptions(){
		colFormula.editingSupport = descriptionEditingSupport
	}

	def create() {
		viewer.table.layoutData = new GridData(SWT.FILL,SWT.FILL,true,true)  
		val table = viewer.table
		 
		tableColumnLayout.createColumns(table)
		
		table.headerVisible = true
		table.linesVisible = true
	}
	
	private def createColumns(TableColumnLayout layout, Table table) {
		createColVarName
		createColFormula
		createColResult
		createColCheck		
		
		layout.setColumnData(table.getColumn(0), new ColumnWeightData(10,5,true))
		layout.setColumnData(table.getColumn(1), new ColumnWeightData(70,100,true))  
		layout.setColumnData(table.getColumn(2), new ColumnWeightData(10,5,true)) 
		layout.setColumnData(table.getColumn(3), new ColumnWeightData(10,5,true)) 
	}
	
	private def createColVarName(){
		colVarName = viewer.createTableViewerColumn("var", 0, SWT.LEFT, SWT.LEFT)
		
		colVarName.labelProvider = new ColumnLabelProvider {
			
			override getText(Object element) {
				view.handler.varNamePrefix + (element as CheckFormula).varName
			}
			
			override getForeground(Object element) {
				return display.getSystemColor(SWT.COLOR_BLACK) 
			}
			
		}
		varNameEditingSupport = new VarNameEditingSupport(view)
		colVarName.editingSupport = varNameEditingSupport
	}
	
	private def createColFormula(){
		colFormula = viewer.createTableViewerColumn("formula", 1, SWT.LEFT, SWT.LEFT) 
		
		colFormula.labelProvider = new StyledCellLabelProvider{
			
			override update(ViewerCell cell) {
				cell.styleRanges = newArrayList
				val formula = cell.element as CheckFormula
				if (preferences.showingDescription) {
					var descriptionText = formula.description 
					if (descriptionText == "") {
						cell.text = '''Description for [«formula.expression»]'''
					}else{
						cell.text = descriptionText
					}
					
				}else{
					cell.text = formula.expression
					
					val provider = new StyleRangeProvider					
				
					if (preferences.inReverseMode){
						cell.styleRanges = provider.getStyleRanges(formula, handler.fullVarNames, view.reverseCheckState, preferences.highlightUnSatSub)
					}else{
						cell.styleRanges = provider.getStyleRanges(formula, handler.fullVarNames, preferences.highlightUnSatSub)
					}
				}
				
				super.update(cell)				
			}
		}
		
		formulaEditingSupport = new FormulaEditingSupport(view) 
		descriptionEditingSupport = new DescriptionEditingSupport(view) 
		colFormula.editingSupport = formulaEditingSupport
	}
	
	private def createColResult(){
		colResult = viewer.createTableViewerColumn("result", 2, SWT.CENTER, SWT.RIGHT) 
		colResult.labelProvider = new ColumnLabelProvider(){
			override getText(Object element) {
				var formula = (element as CheckFormula) 
				
				if (formula.result == Result.ERROR)	{
					return '''error'''
				}
				
				if (formula.result == Result.NOT_CHECKED){
					return '''-'''
				}
				
				var result = "" 
				
				if (!preferences.inReverseMode && !view.resultUpToDate) {
					result = '''*''' 
				}
				
				if (preferences.inReverseMode){
					
					if (view.reverseCheckState.empty || view.reverseCheckState.containsInvalidNodes){
						return '''-'''
					}
					
					
					if (view.reverseCheckState.satisfiedBySelection(formula)){
						return '''true«result»'''
					}else{
						return '''false«result»'''
					}
				}
				
				switch (formula.result) {
					case TRUE:{
						return '''true«result»''' 
					}
					case FALSE:{
						return '''false«result»''' 
					}
					default: {
					}
				}
			}
			
			override getBackground(Object element) {
				var display = Display.getCurrent() 
				var formula = (element as CheckFormula) 
				
				if (formula.result == Result.ERROR){
					return display.getSystemColor(SWT.COLOR_YELLOW) 
				}
				if (formula.result == Result.NOT_CHECKED){
					return display.getSystemColor(SWT.COLOR_WHITE) 
				}
				
				if (preferences.inReverseMode){
					if (view.reverseCheckState.empty || view.reverseCheckState.containsInvalidNodes){
						return display.getSystemColor(SWT.COLOR_WHITE) 
					}
					
					if (view.reverseCheckState.satisfiedBySelection(formula)){
						return display.getSystemColor(SWT.COLOR_GREEN) 
					}else{
						return display.getSystemColor(SWT.COLOR_RED) 
					}
				}
				
				
				switch (formula.result) {
					case TRUE:{
						return display.getSystemColor(SWT.COLOR_GREEN) 
					}
					case FALSE:{
						return display.getSystemColor(SWT.COLOR_RED) 
					}
					default: {
					}
				}
			}
			
			override getForeground(Object element) {
				return display.getSystemColor(SWT.COLOR_BLACK) 
			}
		} 
	}
	
	private def createColCheck(){
		colCheck = viewer.createTableViewerColumn("check", 3, SWT.CENTER, SWT.RIGHT) 
		colCheck.labelProvider = new ColumnLabelProvider(){
			override getText(Object element) {
				if (((element as CheckFormula)).toCheck) {
					return "X" 
				} else {
					return "" 
				}
			}
			
			override getForeground(Object element) {
				return display.getSystemColor(SWT.COLOR_BLACK) 
			}
		}
		
		checkEditingSupport = new CheckEditingSupport(view) 
		colCheck.editingSupport = checkEditingSupport
	}
	
	private def createTableViewerColumn(TableViewer viewer, String title, int colNumber, int alignment, int style) {
		val viewerColumn = new TableViewerColumn(viewer,style) 
		val column = viewerColumn.column
		column.text = title
		column.resizable = true
		column.moveable = true
		column.alignment = alignment
		return viewerColumn 
	}
	
}

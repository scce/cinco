/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import graphmodel.Node
import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.Result
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel

class ReverseCheckState {
	
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	
	var Set<Node> selection
	var CheckableModel<?,?> checkableModel
	var containsInvalid = false
	
	def isEmpty(){
		selection.empty
	}
	
	def getSubToResultList(CheckFormula formula){
		val list = newLinkedList
		if (!empty && !containsInvalid){
			for (entry : formula.getSubToSatList){
				var result = Result.FALSE
				
				if (entry.value.containsAll(selection)){
					result = Result.TRUE
				}
				
				list.add(entry.key -> result)
			}
		}
		list
	}
	
	def satisfiedBySelection(CheckFormula formula){
		formula.satisfyingNodes.containsAll(selection)
	}
	
	def <N,E> setCheckableModel(CheckableModel<N,E> model){
		this.checkableModel = model
	}
	
	def updateSelection(){
		selection = activeModel.allNodesAndContainers.filter[selectedElementIds.contains(it.id)].toSet
		updateInvalid
	}
	
	def containsInvalidNodes(){
		containsInvalid
	}
	
	private def <N,E> updateInvalid(){
		containsInvalid = selection.exists[
			val checkableModel = checkableModel as CheckableModel<N,E>
			!checkableModel.nodes.map[checkableModel.getId(it)].toSet.contains(it.id)
		]
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.model

import java.util.Set

abstract class AbstractBasicCheckableModel<N extends AbstractBasicCheckableNode<E>, E extends AbstractBasicCheckableEdge<N>> implements CheckableModel<N,E> {
		
	def N createNode(String id, boolean isStartNode, Set<String> atomicPropositions)
	def E createEdge(N source, N target, Set<String> labels)
	
	val Set<N> nodes
	val Set<E> edges
	
	new(){
		nodes = newHashSet
		edges = newHashSet
	}
	
	override getId(N node) {
		node.id
	}
	
	override isStartNode(N node) {
		node.isStartNode
	}
	
	override addNewNode(String id, boolean isStartNode, Set<String> atomicPropositions) {
		val checkableNode = createNode(id, isStartNode, atomicPropositions)	
		nodes.add(checkableNode)
	}
	
	override addNewEdge(N source, N target, Set<String> labels) {
		val checkableEdge = createEdge(source, target, labels)
		source.addOutgoing(checkableEdge)
		target.addIncoming(checkableEdge)
		edges.add(checkableEdge)
	}
	
	override getNodes() {
		nodes
	}
	
	override getEdges() {
		edges
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.gear

import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode
import de.metaframe.gear.AssemblyLine
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.SubToSatProvider
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.AbstractSubToSatModelChecker

class GearModelChecker extends AbstractSubToSatModelChecker<GEARModel> implements SubToSatProvider<GEARModel, BasicCheckableNode, BasicCheckableEdge> {
	override getSatisfyingNodes(GEARModel model, String expression) throws Exception {
		val line = new AssemblyLine<BasicCheckableNode, BasicCheckableEdge>
		line.addDefaultMacros
		line.modelCheck(model, expression)
	}

	override getSubToSatMap(GEARModel model, String expression) throws Exception {
		val line = new AssemblyLine<BasicCheckableNode, BasicCheckableEdge>
		line.addDefaultMacros
		line.compilationEnabled = false
		line.modelCheckAll(model, expression)
	}

	override getName() {
		"GEAR"
	}

	override createCheckableModel() {
		new GEARModel()
	}
}

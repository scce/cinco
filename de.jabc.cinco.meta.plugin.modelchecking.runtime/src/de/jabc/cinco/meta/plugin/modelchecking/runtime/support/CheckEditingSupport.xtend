/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.support

import java.util.Arrays
import org.eclipse.jface.viewers.CellEditor
import org.eclipse.jface.viewers.CheckboxCellEditor
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula

class CheckEditingSupport extends ModelCheckingEditingSupport{
	
	val CellEditor editor

	new(de.jabc.cinco.meta.plugin.modelchecking.runtime.views.ModelCheckingViewPart view) {
		super(view)
		editor = new CheckboxCellEditor(view.viewer.table)
	}

	override protected getCellEditor(Object element) {
		editor
	}

	override protected getValue(Object element) {
		(element as CheckFormula).toCheck
	}

	override protected setValue(Object element, Object value) {
		if (handler !== null) {
			var newValue = (value as Boolean)
			var formula = (element as CheckFormula)
			if(formula.toCheck != newValue){
				handler.setToCheck(formula, newValue)
				view.refreshAll
				if (newValue) {
					view.autoCheck(Arrays.asList(formula))
				}
			}
		}
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight

import org.eclipse.graphiti.util.IColorConstant
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.animation.HighlightAnimation
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.ColorProvider
import org.eclipse.graphiti.mm.pictograms.PictogramElement
import java.util.Set
import graphmodel.ModelElement
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.graphiti.util.ColorConstant
import org.eclipse.swt.graphics.RGB
import java.util.Collection
import java.util.function.Consumer
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlight

class HighlightDescription {
	extension WorkbenchExtension = new WorkbenchExtension
	
	var IColorConstant foregroundColor
	var IColorConstant backgroundColor
	var Consumer<Highlight> startFunction
	
	val Set<PictogramElement> elements
	
	new(){
		this(IColorConstant.BLACK, ColorProvider.INSTANCE.get.next)
	}
	
	new(IColorConstant fg, IColorConstant bg){
		this(fg, bg, null)
	}
	
	new(IColorConstant fg, IColorConstant bg, HighlightAnimation animation){
		foregroundColor = fg
		backgroundColor = bg
		this.startFunction = [on()]
		elements = newHashSet
	}
	
	def getForegroundColor(){
		foregroundColor
	}
	
	def setForegroundColor(IColorConstant color) {
		foregroundColor = color
	}

	def setForegroundColor(int red, int green, int blue) {
		setForegroundColor(new ColorConstant(red, green, blue))
	}

	def setForegroundColor(String hexRGBString) {
		setForegroundColor(new ColorConstant(hexRGBString))
	}
	
	def setForegroundColor(RGB rgb){
		new ColorConstant(rgb.red, rgb.green, rgb.blue).setForegroundColor
	}
	
	def setBackgroundColor(IColorConstant color) {
		backgroundColor = color
	}

	def setBackgroundColor(int red, int green, int blue) {
		setBackgroundColor(new ColorConstant(red, green, blue))
	}

	def setBackgroundColor(String hexRGBString) {
		setBackgroundColor(new ColorConstant(hexRGBString))
	}
	
	def setBackgroundColor(RGB rgb){
		new ColorConstant(rgb.red, rgb.green, rgb.blue).setBackgroundColor
	}
	
	def getBackgroundColor(){
		backgroundColor
	}
	
	def getStartFunction(){
		startFunction
	}
	
	def setStartFunction(Consumer<Highlight> consumer){
		this.startFunction = consumer
	}
	
	def void add(ModelElement element){
		element?.pictogramElement?.add
	}
	
	def void addAllModelElements(Collection<ModelElement> elements){
		elements.forEach[add]
	}
	
	def void remove(ModelElement element){
		element?.pictogramElement?.remove
	}
	
	def void add(PictogramElement element){
		if (element !== null){
			elements.add(element)
		}
	}
	
	def void addAll(Collection<PictogramElement> elements){
		elements.forEach[add]
	}
	
	def void remove(PictogramElement element){
		if (elements.contains(element)){
			elements.remove(element)
		}
	}
	
	def contains(PictogramElement element){
		elements.contains(element)
	}
	
	def getElements(){
		elements
	}
	
	def getNewHighlight(){
		new Highlight => [hl |
			elements.forEach[hl.add(it)]
			hl.backgroundColor = backgroundColor
			hl.foregroundColor = foregroundColor
		]
	}
}

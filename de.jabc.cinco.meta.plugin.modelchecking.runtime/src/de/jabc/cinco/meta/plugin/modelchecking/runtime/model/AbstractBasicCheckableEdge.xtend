/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.model

import java.util.Set

abstract class AbstractBasicCheckableEdge<N> {
	protected val N source
	protected val N target
	protected val Set<String> labels
	
	new (N source, N target, Set<String> labels){
		this.labels = newHashSet
		this.source = source
		this.target = target
		
		addLabels(labels)
	}
	
	def addLabels(String labels){
		if (labels !== null){
			for (label : labels.replaceAll(" ","").split(",")) {
				this.labels.add(label)
			}
		}
	}
	
	def addLabels(Set<String> labels){
		if (labels !== null){
			for (label : labels){
				addLabels(label)
			}
		}
	}
	
	def getSource() {source}
	def getTarget() {target}
	def getLabels() {labels}
		
}

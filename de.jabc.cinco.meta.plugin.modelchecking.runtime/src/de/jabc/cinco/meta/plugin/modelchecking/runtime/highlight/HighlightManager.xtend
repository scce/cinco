/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight

import java.util.Set
import de.jabc.cinco.meta.core.ge.style.generator.runtime.highlight.Highlight
import java.util.Map
import org.eclipse.swt.graphics.RGB
import graphmodel.Node
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.graphiti.util.IColorConstant
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension

class HighlightManager {
	
	extension WorkbenchExtension = new WorkbenchExtension
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	
	Set<HighlightDescription> currentHighlightDescriptions = newHashSet
	Set<Highlight> currentHighlights = newHashSet
	
	def setNewHighlightDescriptions(Map<RGB, Set<Node>> rgbMap) {

		val allNodes = activeGraphModel.allNodesAndContainers
		val intendedHighlightDescriptions = newHashSet

		for (entry : rgbMap.entrySet){
			val hd = new HighlightDescription => [
				foregroundColor = IColorConstant.BLACK
				backgroundColor = entry.key
				entry.value
					.filterNull
					.filter[allNodes.contains(it)]
					.forEach[node|
						add(node)
					]
			]
			intendedHighlightDescriptions.add(hd)
		}
		currentHighlightDescriptions = activeAdapter.getHighlightDescriptions(activeGraphModel, intendedHighlightDescriptions)		
	}

	def clearHighlight() {
		highlightOff
		currentHighlights = newHashSet
		currentHighlightDescriptions = newHashSet
	}

	def void highlightOn() {
		highlightOff
		currentHighlights = newHashSet
		currentHighlightDescriptions.forEach[
			val highlight = newHighlight
			currentHighlights.add(highlight)
			startFunction.accept(highlight)
		]
	}

	def void highlightOff() {
		currentHighlights.forEach[
			off
		]
	}
}

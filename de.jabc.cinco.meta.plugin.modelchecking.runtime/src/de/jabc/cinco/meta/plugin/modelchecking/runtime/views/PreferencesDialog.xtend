/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.ModelChecker
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import java.util.List
import org.eclipse.jface.dialogs.Dialog
import org.eclipse.jface.dialogs.IDialogConstants
import org.eclipse.swt.SWT
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.graphics.Color
import org.eclipse.swt.graphics.GC
import org.eclipse.swt.graphics.Image
import org.eclipse.swt.graphics.RGB
import org.eclipse.swt.layout.GridData
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Combo
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Display
import org.eclipse.swt.widgets.Label
import org.eclipse.swt.widgets.Shell
import org.eclipse.swt.widgets.ColorDialog

class PreferencesDialog extends Dialog{
	public static val int APPLY = 0;
	public static val int APPLY_FOR_ALL = 2;
	public static val int CANCEL = 1; 
	List<ModelChecker<?,?,?>> modelCheckers
	PreferenceManager manager
	
	boolean formulasExist
	
	Button buttonHighlightUnSatSub
	
	Composite container
	
	new(Shell parentShell, List<ModelChecker<?,?,?>> modelCheckers, PreferenceManager manager, boolean formulasExist) {
		super(parentShell)
		this.manager = manager
		this.modelCheckers = modelCheckers
		this.formulasExist = formulasExist
		blockOnOpen = true
		
		manager.makeTempChanges
	}
	
	override protected createDialogArea(Composite parent) {
		container = super.createDialogArea(parent) as Composite
		
		createCheckerDropDown(container)
		
		if (formulasExist){
			val buttonAutoChecks = new Button(container, SWT.CHECK);
	        buttonAutoChecks.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false,
	                false));
	        buttonAutoChecks.setText("Enable auto check");
	        buttonAutoChecks.addSelectionListener(new SelectionListener {
										
				override widgetDefaultSelected(SelectionEvent e) {
					
				}
				
				override widgetSelected(SelectionEvent e) {
					manager.toggleAutoCheck
					refreshButtons
				}
	            						
	        });
	        
	        buttonAutoChecks.selection = manager.withAutoCheck
	        buttonAutoChecks.enabled = !manager.inReverseMode
	        
	        buttonHighlightUnSatSub = new Button(container, SWT.CHECK);
	        buttonHighlightUnSatSub.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false,
	                false));
	        buttonHighlightUnSatSub.addSelectionListener(new SelectionListener {
										
				override widgetDefaultSelected(SelectionEvent e) {
					
				}
				
				override widgetSelected(SelectionEvent e) {
					manager.toggleHighlightUnSatSub
					refreshButtons
				}
	            						
	        });
		}
        
		container.createNodeHighlightPreferences
		
        return container;
	}
	
	def createCheckerDropDown(Composite parent){
		val labelChecker = new Label(parent, SWT.NONE)
		labelChecker.text = "Select Model Checker:"
		
		val comboCheckers = new Combo(parent,SWT.DROP_DOWN.bitwiseOr(SWT.READ_ONLY))
		
		comboCheckers.addSelectionListener(new SelectionListener{
			
			override widgetDefaultSelected(SelectionEvent e) {
				
			}
			
			override widgetSelected(SelectionEvent e) {
				val newChecker = modelCheckers.get(comboCheckers.selectionIndex)
				manager.modelChecker = newChecker
				refreshButtons
			}
			
		})
		
		for (checker : modelCheckers){
			comboCheckers.add(checker.name)
		}
		comboCheckers.enabled = !manager.inReverseMode
		comboCheckers.select(modelCheckers.indexOf(manager.modelChecker))
	}
	
	def createNodeHighlightPreferences(Composite parent){
		val nodeHighlightComposite = new Composite(parent, SWT.NONE) => [
			layoutData = new GridData(SWT.FILL,SWT.NONE,true,false) => [
				widthHint = 20
			]
			layout = new GridLayout(3,false) => [
				marginWidth = 0
				marginHeight = 0
			]
		]
		
		val buttonShowHighlight = new Button(nodeHighlightComposite, SWT.CHECK);
        buttonShowHighlight.setLayoutData(
        	new GridData(SWT.BEGINNING, SWT.CENTER, false, false)
        )
        buttonShowHighlight.setText("Show Highlight,");
        buttonShowHighlight.addSelectionListener(new SelectionListener {
									
			override widgetDefaultSelected(SelectionEvent e) {
				
			}
			
			override widgetSelected(SelectionEvent e) {
				manager.toggleShowingHighlight
				refreshButtons
			}
            						
        });
        
        new Label(nodeHighlightComposite, SWT.NONE) => [
        	layoutData = new GridData(SWT.LEFT, SWT.CENTER, false, false)
        	text = "default color: "
        ]
        
        new Button(nodeHighlightComposite, SWT.FLAT) => [
        	layoutData = new GridData(SWT.LEFT, SWT.CENTER, false, false) => [
        	]
        	
        	
        	image = display.getColorImage(manager.highlightColor)
        	addSelectionListener(new SelectionListener{
										
				override widgetDefaultSelected(SelectionEvent e) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}
				
				override widgetSelected(SelectionEvent e) {
					val cDialog = new ColorDialog(shell)
					val currentColor = manager.highlightColor
					cDialog.RGB = new RGB(currentColor.red, currentColor.green, currentColor.blue)
					val rgb = cDialog.open
					if (rgb !== null){						
						manager.setHighlightColor(rgb)
					}
					image = display.getColorImage(manager.highlightColor)	
					refreshButtons
				}
        		
        	})
        	
        ]
        
        buttonShowHighlight.selection = manager.showingHighlight
	}
	
	override protected createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Apply", true)
		createButton(parent, IDialogConstants.YES_TO_ALL_ID, "Apply to all models", false).addSelectionListener(new SelectionListener{
			
			override widgetDefaultSelected(SelectionEvent e) {
				throw new UnsupportedOperationException("TODO: auto-generated method stub")
			}
			
			override widgetSelected(SelectionEvent e) {
				returnCode =  IDialogConstants.YES_TO_ALL_ID
				close
			}
			
		})
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false)
		refreshButtons
	}
	
	override protected configureShell(Shell newShell) {
		super.configureShell(newShell)
		var title = "Model Checking View - Preferences"
		if (manager.inReverseMode){
			title += " (Reverse Mode)"
		}
		newShell.text = title
	}
	
	private def refreshButtons(){
		val ext = new ModelCheckingRuntimeExtension
		getButton(IDialogConstants.OK_ID).enabled = manager.settingsChanged
		
		if (formulasExist){
			val supportsSubToSat = ext.supportsSubToSat(manager.modelChecker)
			buttonHighlightUnSatSub.selection = manager.highlightUnSatSub
			buttonHighlightUnSatSub.enabled = supportsSubToSat
			
			if (supportsSubToSat){
				buttonHighlightUnSatSub.text = '''Highlight unsatisfied sub formulas'''
			}else{
				buttonHighlightUnSatSub.text = '''Highlight unsatisfied sub formulas (not supported)'''
			}
		}
		
		container.layout()
	}
	
	def getColorImage(Display display, RGB rgb){
		val size = 15
		val image = new Image(display, size, size)
		val gc = new GC(image)
		gc.background = new Color(display, rgb)
		gc.fillRectangle(0,0,size,size)
		gc.dispose
		return image
	}
	
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.model

import java.util.Set

abstract class AbstractBasicCheckableNode<E> {
	protected val String id
	protected var boolean isStartNode
	protected val Set<E> incoming
	protected val Set<E> outgoing
	protected val Set<String> atomicPropositions
	
	new (String id, boolean isStartNode, Set<String> atomicPropositions){
		this.atomicPropositions = newHashSet
		this.incoming = newHashSet
		this.outgoing = newHashSet
		this.id = id
		this.isStartNode = isStartNode
		addAtomicPropositions(atomicPropositions)
	}
	
	def addAtomicPropositions(String atomicPropositions){
		if (atomicPropositions !== null){
			for (ap : atomicPropositions.replaceAll(" ","").split(",")){
				this.atomicPropositions.add(ap)
			}
		}
	}
	
	def addAtomicPropositions(Set<String> atomicPropositions){
		if (atomicPropositions !== null){
			for (ap : atomicPropositions){
				addAtomicPropositions(ap)
			}
		}
	}
	
	def getId() {id}
	def isStartNode() {isStartNode}
	def getAtomicPropositions() {atomicPropositions}
	def getIncoming() {incoming}
	def getOutgoing() {outgoing}
	
	def setIsStartNode(boolean isStartNode){
		this.isStartNode = isStartNode
	}
	
	def addIncoming(E edge){
		this.incoming.add(edge)
	}
	
	def addOutgoing(E edge){
		this.outgoing.add(edge)
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.support

import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler.FormulaException
import de.jabc.cinco.meta.plugin.modelchecking.runtime.views.ModelCheckingViewPart
import org.eclipse.jface.viewers.CellEditor
import org.eclipse.jface.viewers.TextCellEditor

class VarNameEditingSupport extends ModelCheckingEditingSupport {

	final CellEditor editor

	new(ModelCheckingViewPart view) {
		super(view)
		this.editor = new TextCellEditor(view.viewer.table)
	}

	override protected getCellEditor(Object element) {
		editor
	}

	override protected getValue(Object element) {
		(element as CheckFormula).varName
	}

	override protected setValue(Object element, Object value) {
		val newVarName = (value as String).trim
		val formula = (element as CheckFormula)
		if (handler !== null && newVarName != formula.varName) {
			try {
				var refactor = false

				if (handler.canRefactor(formula, newVarName)) {
					refactor = showQuestionDialog("Refactor?",
						"Would you like to refactor the variable name? Otherwise it will only be renamed.")
				}

				handler.setVarName(formula, newVarName, refactor)
			} catch (FormulaException e) {
				var message = ""
				switch e.errorCode {
					case INVALID:
						message = "Variable names may only contain the characters A-Z, a-z and 0-9."
					case EXISTS:
						message = "Variable name already exists."
					case CYCLE:
						message = "Variable name leads to circular dependencies."
				}
				showErrorDialog("Invalid expression", message)
			} finally {
				view.refreshAll
				view.autoCheckAllFormulas
			}
		}
	}
}

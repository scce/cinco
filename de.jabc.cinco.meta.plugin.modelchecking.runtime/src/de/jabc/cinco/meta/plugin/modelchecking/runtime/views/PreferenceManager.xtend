/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.ModelChecker
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import graphmodel.GraphModel
import java.util.List
import java.util.function.Consumer
import org.eclipse.core.runtime.preferences.InstanceScope
import org.eclipse.swt.graphics.RGB
import org.osgi.service.prefs.BackingStoreException
import org.osgi.service.prefs.Preferences

class PreferenceManager {

	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension

	val Preferences prefs
	var Preferences currentPreferences
	var boolean makeTempPreferences
	val List<ModelChecker<?, ?, ?>> modelCheckers

	val String DEFAULT_CHECKER = "GEAR"

	val String PREF_MODELS = "modelPreferences"
	val String PREF_TEMP = "tempPreferences"
	val String PREF_REVERSE = "reversePreferences"

	val String KEY_SELECTION = "selectionModel"
	val String KEY_DESCRIPTION = "showDescription"
	val String KEY_HIGHLIGHT_NODES = "highlightNodes"
	val String KEY_HIGHLIGHT_UNSATSUB = "highlightSubFormulas"
	val String KEY_HIGHLIGHTCOLOR_NODE = "highlightColor"
	val String KEY_AUTOCHECK = "autoCheck"
	val String KEY_HAD_AUTOCHECK = "hadAutoCheck"
	val String KEY_MODELCHECKER = "modelChecker"

	new(List<ModelChecker<?, ?, ?>> modelCheckers) {
		prefs = InstanceScope.INSTANCE.getNode("de.jabc.cinco.meta.plugin.modelchecking.runtime")

		clear
		this.makeTempPreferences = false
		this.modelCheckers = modelCheckers
	}

	private def clear() {
		if (prefs.nodeExists(PREF_REVERSE)) {
			reversePrefs.edit[removeNode]
		}

		if (prefs.nodeExists(PREF_TEMP)) {
			tempPrefs.edit[removeNode]
		}
	}

	def void loadPreferences(GraphModel model) {
		val exists = model.containsPreferencesForModel
		val modelNode = prefs.node(PREF_MODELS).node(model.id)
		currentPreferences = modelNode

		if (!exists) {
			currentPreferences.init(model)
		}
	}

	def containsPreferencesForModel(GraphModel model) {
		prefs.node(PREF_MODELS).nodeExists(model.id)
	}

	def isShowingDescription() {
		currentPreferences?.getBoolPreferences(KEY_DESCRIPTION)
	}

	def isShowingHighlight() {
		currentPreferences?.getBoolPreferences(KEY_HIGHLIGHT_NODES)
	}

	def isWithSelection() {
		currentPreferences?.getBoolPreferences(KEY_SELECTION)
	}

	def isWithAutoCheck() {
		currentPreferences?.getBoolPreferences(KEY_AUTOCHECK)
	}

	def hadAutoCheckEnabled() {
		currentPreferences?.getBoolPreferences(KEY_HAD_AUTOCHECK)
	}

	def isHighlightUnSatSub() {
		currentPreferences?.getBoolPreferences(KEY_HIGHLIGHT_UNSATSUB)
	}

	def isInReverseMode() {
		if (currentPreferences !== null){
			reversePrefs.getBoolPreferences(currentPreferences.name)
		}		
	}

	def getModelChecker() {
		val name = currentPreferences.get(KEY_MODELCHECKER, activeModel.defaultChecker.name)
		modelCheckers.findFirst[it.name == name]
	}
	
	def getHighlightColor(){
		val r = currentPreferences.node(KEY_HIGHLIGHTCOLOR_NODE).getInt("R", activeAdapter?.highlightColor?.red)
		val g = currentPreferences.node(KEY_HIGHLIGHTCOLOR_NODE).getInt("G", activeAdapter?.highlightColor?.green)
		val b = currentPreferences.node(KEY_HIGHLIGHTCOLOR_NODE).getInt("B", activeAdapter?.highlightColor?.blue)
		return new RGB(r, g, b)
	}
	
	def setHighlightColor(RGB rgb){
		currentPreferences.highlightColor = rgb
	}

	def toggleHighlightUnSatSub() {
		currentPreferences.toggle(KEY_HIGHLIGHT_UNSATSUB)
	}

	def toggleShowingDescription() {
		currentPreferences.toggle(KEY_DESCRIPTION)
	}

	def toggleShowingHighlight() {
		currentPreferences.toggle(KEY_HIGHLIGHT_NODES)
	}

	def toggleWithSelection() {
		currentPreferences.toggle(KEY_SELECTION)
	}

	def toggleAutoCheck() {
		currentPreferences.toggle(KEY_AUTOCHECK)
		hadAutoCheckEnabled = currentPreferences.getBoolPreferences(KEY_AUTOCHECK)
	}

	def setHadAutoCheckEnabled(boolean enabled) {
		currentPreferences.edit[putBoolean(KEY_HAD_AUTOCHECK, enabled)]
	}

	def setInReverseMode(boolean inReverseMode) {
		prefs.edit [
			if (inReverseMode) {
				currentPreferences.putBoolean(KEY_AUTOCHECK, false)
			}
			reversePrefs.putBoolean(currentPreferences.name, inReverseMode)
		]
	}

	def setModelChecker(ModelChecker<?, ?, ?> checker) {
		currentPreferences.edit [
			it.modelChecker = checker

			if (makeTempPreferences && checker.supportsSubToSat && tempPrefs?.modelChecker.supportsSubToSat) {
				currentPreferences.putBoolean(KEY_HIGHLIGHT_UNSATSUB,
					tempPrefs?.getBoolPreferences(KEY_HIGHLIGHT_UNSATSUB))
			}
		]
	}

	def makeTempChanges() {
		if (!makeTempPreferences) {
			makeTempPreferences = true
			currentPreferences.copyAllPreferencesTo(tempPrefs)
		} else {
			println("Already making temporary changes. Apply or discard first.")
		}
	}

	def apply() {
		if (makeTempPreferences) {
			makeTempPreferences = false
			tempPrefs.edit[removeNode]
		} else {
			println("No temporary changes found.")
		}
	}

	def applyToAll(boolean full) {
		makeTempPreferences = false
		tempPrefs.edit[removeNode]

		for (modelName : prefs.node(PREF_MODELS).childrenNames) {
			val pref = prefs.node(PREF_MODELS).node(modelName)
			currentPreferences.copyTo(pref, full)
		}
	}

	def discard() {
		if (makeTempPreferences) {
			makeTempPreferences = false
			tempPrefs.edit [
				copyAllPreferencesTo(currentPreferences)
				removeNode
			]
		} else {
			println("No temporary changes found.")
		}
	}

	def changesAffectChecks() {
		if (makeTempPreferences) {
			currentPreferences.modelChecker != tempPrefs.get(KEY_MODELCHECKER, DEFAULT_CHECKER) ||
				(currentPreferences.getBoolPreferences(KEY_AUTOCHECK) && !tempPrefs.getBoolPreferences(KEY_AUTOCHECK))
		} else {
			println("No temporary changes found.")
			false
		}

	}

	def settingsChanged() {
		if (makeTempPreferences && currentPreferences !== null) {
			return !currentPreferences.isEqualTo(tempPrefs)
		}
		false
	}

//	Preferences Util methods
	private def getBoolPreferences(Preferences pref, String key) {
		pref?.getBoolean(key, activeAdapter?.getSettings(key)?.toBool)
	}

	private def void toggle(Preferences pref, String key) {
		pref.edit[putBoolean(key, !pref.getBoolPreferences(key))]
	}

	private def init(Preferences pref, GraphModel model) {
		pref.edit [
			it.modelChecker = model.defaultChecker
			val autoCheck = model.adapter.getSettings(KEY_AUTOCHECK).toBool
			putBoolean(KEY_AUTOCHECK, autoCheck)
			putBoolean(KEY_HAD_AUTOCHECK, autoCheck)
			putBoolean(KEY_HIGHLIGHT_NODES, model.adapter.getSettings(KEY_HIGHLIGHT_NODES).toBool)
			putBoolean(KEY_DESCRIPTION, model.adapter.getSettings(KEY_DESCRIPTION).toBool)
			putBoolean(KEY_SELECTION, model.adapter.getSettings(KEY_SELECTION).toBool)

			val highlightUnSatSub = modelChecker.supportsSubToSat &&
				model.adapter.getSettings(KEY_HIGHLIGHT_UNSATSUB).toBool
			putBoolean(KEY_HIGHLIGHT_UNSATSUB, highlightUnSatSub)
		]
		pref.highlightColor = model.adapter.highlightColor
	}
	
	private def setHighlightColor(Preferences pref, RGB rgb){
		pref.node(KEY_HIGHLIGHTCOLOR_NODE).edit[
			putInt("R", rgb.red)
			putInt("G", rgb.green)
			putInt("B", rgb.blue)
		]
	}

	private def copyTo(Preferences pref, Preferences other, boolean all) {
		other.edit [
			putBoolean(KEY_HIGHLIGHT_NODES, pref.getBoolean(KEY_HIGHLIGHT_NODES, true))
			put(KEY_MODELCHECKER, pref.get(KEY_MODELCHECKER, DEFAULT_CHECKER))

			if (all) {
				putBoolean(KEY_HIGHLIGHT_UNSATSUB, pref.getBoolean(KEY_HIGHLIGHT_UNSATSUB, false))
				putBoolean(KEY_AUTOCHECK, pref.getBoolean(KEY_AUTOCHECK, true))
				putBoolean(KEY_HAD_AUTOCHECK, pref.getBoolean(KEY_HAD_AUTOCHECK, true))
			}
		]
		pref.copyColorValues(other)
	}

	private def copyAllPreferencesTo(Preferences pref, Preferences other) {
		other.edit [
			putBoolean(KEY_SELECTION, pref.getBoolPreferences(KEY_SELECTION))
			putBoolean(KEY_SELECTION, pref.getBoolPreferences(KEY_SELECTION))
			putBoolean(KEY_HIGHLIGHT_NODES, pref.getBoolPreferences(KEY_HIGHLIGHT_NODES))
			putBoolean(KEY_HIGHLIGHT_UNSATSUB, pref.getBoolPreferences(KEY_HIGHLIGHT_UNSATSUB))
			putBoolean(KEY_AUTOCHECK, pref.getBoolPreferences(KEY_AUTOCHECK))
			putBoolean(KEY_HAD_AUTOCHECK, pref.getBoolPreferences(KEY_HAD_AUTOCHECK))
			put(KEY_MODELCHECKER, pref.get(KEY_MODELCHECKER, DEFAULT_CHECKER))
			
		]
		
		pref.copyColorValues(other)
	}

	private def copyColorValues(Preferences pref, Preferences other){
		val colorPrefs = pref.node(KEY_HIGHLIGHTCOLOR_NODE)
		other.node(KEY_HIGHLIGHTCOLOR_NODE).edit[
			putInt("R", colorPrefs.getInt("R", activeAdapter?.highlightColor?.red))
			putInt("G", colorPrefs.getInt("G", activeAdapter?.highlightColor?.green))
			putInt("B", colorPrefs.getInt("B", activeAdapter?.highlightColor?.blue))
		]
	}
	
	private def isEqualTo(Preferences pref, Preferences other) {
		other.getBoolPreferences(KEY_HIGHLIGHT_NODES) == pref.getBoolPreferences(KEY_HIGHLIGHT_NODES) &&
			other.getBoolPreferences(KEY_HIGHLIGHT_UNSATSUB) == pref.getBoolPreferences(KEY_HIGHLIGHT_UNSATSUB) &&
			other.getBoolPreferences(KEY_AUTOCHECK) == pref.getBoolPreferences(KEY_AUTOCHECK) &&
			other.modelChecker == pref.modelChecker &&
			pref.haveEqualColorValues(other)			
	}
	
	private def haveEqualColorValues(Preferences pref, Preferences other){
		val prefColor = pref.node(KEY_HIGHLIGHTCOLOR_NODE)
		val otherColor = other.node(KEY_HIGHLIGHTCOLOR_NODE)
		otherColor.getInt("R", activeAdapter?.highlightColor?.red) ==
			prefColor.getInt("R", activeAdapter?.highlightColor?.red) &&
		otherColor.getInt("G", activeAdapter?.highlightColor?.green) ==
			prefColor.getInt("G", activeAdapter?.highlightColor?.green) &&
		otherColor.getInt("B", activeAdapter?.highlightColor?.blue) ==
			prefColor.getInt("B", activeAdapter?.highlightColor?.blue)
	}

	private def setModelChecker(Preferences pref, ModelChecker<?, ?, ?> checker) {
		pref.edit [
			put(KEY_MODELCHECKER, checker.name)
			val highlightUnSatSub = checker.supportsSubToSat && highlightUnSatSub
			putBoolean(KEY_HIGHLIGHT_UNSATSUB, highlightUnSatSub)
		]
	}

	private def getModelChecker(Preferences pref) {
		modelCheckers.findFirst[name == pref.get(KEY_MODELCHECKER, DEFAULT_CHECKER)]
	}

	private def save() {
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}

	private def getTempPrefs() {
		prefs.node(PREF_TEMP)
	}

	private def getReversePrefs() {
		prefs.node(PREF_REVERSE)
	}

	private def edit(Preferences pref, Consumer<Preferences> consumer) {
		consumer.accept(pref)
		save
	}

	// Util methods
	private def toBool(String bool) {
		Boolean.parseBoolean(bool)
	}

	private def getDefaultChecker(GraphModel model) {
		if (model.adapter === null){
			return modelCheckers.findFirst[name == DEFAULT_CHECKER]
		}
		
		var defaultChecker = modelCheckers.findFirst [
			name == model.adapter.getSettings("defaultModelChecker")
		]

		if (defaultChecker === null) {
			defaultChecker = modelCheckers.findFirst[name == DEFAULT_CHECKER]
		}

		return defaultChecker
	}
}

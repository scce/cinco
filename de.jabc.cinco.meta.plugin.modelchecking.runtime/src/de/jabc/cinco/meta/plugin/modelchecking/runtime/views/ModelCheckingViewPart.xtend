/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import org.eclipse.ui.part.ViewPart
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.layout.GridLayout
import org.eclipse.swt.layout.GridData
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import org.eclipse.ui.IPartListener2
import org.eclipse.ui.ISelectionListener
import org.eclipse.swt.SWT
import org.eclipse.swt.custom.StyledText
import org.eclipse.swt.events.TraverseEvent
import org.eclipse.swt.widgets.Button
import org.eclipse.swt.widgets.Label
import org.eclipse.jface.viewers.TableViewer
import org.eclipse.swt.events.ModifyEvent
import org.eclipse.swt.events.FocusListener
import org.eclipse.swt.events.FocusEvent
import org.eclipse.jface.viewers.StructuredSelection
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler
import org.eclipse.swt.events.SelectionListener
import org.eclipse.swt.events.SelectionEvent
import org.eclipse.jface.viewers.ArrayContentProvider
import org.eclipse.jface.viewers.DoubleClickEvent
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import org.eclipse.jface.viewers.SelectionChangedEvent
import java.util.Set
import graphmodel.Node
import java.util.List
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import java.util.Arrays
import org.eclipse.jface.action.Action
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.ModelCheckingAdapter
import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightManager

abstract class ModelCheckingViewPart extends ViewPart implements IPartListener2, ISelectionListener{
	protected extension WorkbenchExtension = new WorkbenchExtension
	protected extension ModelCheckingRuntimeExtension =new ModelCheckingRuntimeExtension
	
	protected enum State {
		IDLE, FORMULAS_CHECK, AUTO_CHECK, AUTO_CHECK_ALL
	}
	
	protected Action actionToggleShowingDescription
	protected Action actionOpenSettingsDialog
	protected Action actionToggleWithSelection
	
	protected Composite composite
	protected Composite compositeFormulary
	protected TableViewerComposite compositeTableViewer
	protected Composite compositeFormularyButtons
	protected Composite bottomComposite
	protected Composite compositeTextFieldCheck
	
	protected TableViewer viewer
	
	protected StyledText textFieldFormulas
	
	protected Button buttonCheck
	protected Button buttonDeleteFormula
	protected Button buttonAddFormula
	protected Button buttonCheckFormulas
	protected Button buttonReverseMode
	protected Button buttonShowDescription
	protected Button buttonWithSelection
	
	protected Label labelStatus
	protected Label labelLastCheck
	
	protected HighlightManager highlightManager
	
	//Abstract methods
	def void checkTextFieldFormula(String text)
	def void autoCheck(List<CheckFormula> formulas)
	def void autoCheckAllFormulas()
	def void checkAllFormulas()

	def Set<Node> getSatisfyingOfFormulas(List<CheckFormula> formulas)
	
	def ModelCheckingAdapter getActiveAdapter()
	def FormulaHandler<?,?> getHandler()
	def PreferenceManager getPreferences()
	def ReverseCheckState getReverseCheckState()
	def TopicalityHelper getTopicalityHelper()
	def State getState()
	
	def void deactivateReverseMode()
	
	def boolean resultUpToDate()
	
	//Create View
	override setFocus() {
		textFieldFormulas.setFocus
	}
	
	override dispose() {
		activePage?.removePartListener(this)
		activePage?.removeSelectionListener(this)
		if (topicalityHelper !== null) topicalityHelper.cleanup 
		super.dispose
	}
	
	override createPartControl(Composite parent) {
		composite = new Composite(parent,SWT.NONE) 
		var gridLayout = new GridLayout(1,false) 
		composite.layout = gridLayout
		val gridData = new GridData(SWT.FILL,SWT.FILL,true,true)
		composite.shell.setMinimumSize(200,200)
		composite.layoutData = gridData
		composite.createTextFieldCheckComposite				
		composite.createFormularyComposite
		composite.createBottomComposite
		activePage.addPartListener(this)
		activePage.addSelectionListener(this)
		composite.pack
		
		site.shell.setMinimumSize(600,330)
	}
	
	private def createTextFieldCheckComposite(Composite parent) {
		compositeTextFieldCheck = new Composite(parent,SWT.NONE) 
		var searchBarGridLayout = new GridLayout(2,false) 
		var searchBarGridData = new GridData(SWT.FILL,SWT.NONE,true,false) 
		compositeTextFieldCheck.layout = searchBarGridLayout 
		compositeTextFieldCheck.layoutData = searchBarGridData 
		
		// SearchField for Formula
		textFieldFormulas = new StyledText(compositeTextFieldCheck,SWT.SINGLE.bitwiseOr(SWT.BORDER)) 
		textFieldFormulas.layoutData = new GridData(SWT.FILL,SWT.NONE,true,false)
		
		textFieldFormulas.addTraverseListener([TraverseEvent e|if (buttonCheck.isEnabled && e.detail === SWT.TRAVERSE_RETURN) {
			checkTextFieldFormula(textFieldFormulas.text) 
		}]) 
		
		textFieldFormulas.addModifyListener([ ModifyEvent e |
			refreshButtons
			if (handler !== null){
				textFieldFormulas.styleRanges = new StyleRangeProvider().getVarNameStyleRanges(textFieldFormulas.text,
				handler.fullVarNames)
			}

		])
		textFieldFormulas.addFocusListener(new FocusListener(){
			override focusLost(FocusEvent e) {}
			
			override focusGained(FocusEvent e) {
				if (activeAdapter !== null){
					viewer.selection = new StructuredSelection
				}				
			}
		})
		 
		// Check Button to start check
		buttonCheck = createButton(compositeTextFieldCheck, SWT.PUSH, "check", true, false)
		buttonCheck.layoutData = new GridData(SWT.RIGHT,SWT.NONE,false,false) 
		buttonCheck.addSelectionListener(new SelectionListener(){
			override widgetSelected(SelectionEvent event) {
				checkTextFieldFormula(textFieldFormulas.text) 
			}
			
			override widgetDefaultSelected(SelectionEvent e) {}
		}) 
		compositeTextFieldCheck.layout()
	}
	
	private def createFormularyComposite(Composite parent){
		compositeFormulary = new Composite(parent, SWT.NONE)
		compositeFormulary.layout = new GridLayout(1,false)
		compositeFormulary.layoutData = new GridData(SWT.FILL, SWT.FILL, true, true)
		
		val labelFormulas = new Label(compositeFormulary,SWT.LEFT) 
		labelFormulas.text = "Formulas:" 
		labelFormulas.layoutData = new GridData(SWT.LEFT,SWT.NONE,true,false)
		
		compositeFormulary.createTableViewerComposite
		compositeFormulary.createFormularyButtonsComposite
		compositeFormulary.layout()
	}
	
	private def createTableViewerComposite(Composite parent) {
		compositeTableViewer = new TableViewerComposite(parent, SWT.NONE, this)
		
		viewer = new TableViewer(compositeTableViewer,SWT.MULTI.bitwiseOr(SWT.H_SCROLL).bitwiseOr(SWT.V_SCROLL).bitwiseOr(SWT.FULL_SELECTION).bitwiseOr(SWT.BORDER)){
			override refresh() {
				if (handler !== null) {
					this.input = handler.formulas
				}
				selection = new StructuredSelection
				super.refresh
			}
		} 
		
		viewer.contentProvider = new ArrayContentProvider
		viewer.addDoubleClickListener([DoubleClickEvent event|
			var selection=(event.selection as StructuredSelection) 
			var formula=(selection.firstElement as CheckFormula) 
			textFieldFormulas.text = formula.expression
		]) 
		
		
		viewer.addSelectionChangedListener([SelectionChangedEvent event|
			var suffix = "" 
			if (viewer.structuredSelection.size > 0) {
				var selectedFormulas = newArrayList
				
				for (formula : (viewer.structuredSelection as StructuredSelection).toList) {
					selectedFormulas.add((formula as CheckFormula)) 
				}
				
				var satisfyingAllFormulas = selectedFormulas.satisfyingOfFormulas
				
				newHighlight = satisfyingAllFormulas
			
				switch satisfyingAllFormulas {
					case satisfyingAllFormulas.size == 1: suffix="1 satisfying node." 
					case satisfyingAllFormulas.size > 1:
						suffix='''«satisfyingAllFormulas.size» satisfying nodes.'''
				}
			}else{
				highlightManager.clearHighlight
			}
			refreshButtons
			refreshStatusText("", suffix) 
		])
		
		compositeTableViewer.create
		
	}
	
	private def createFormularyButtonsComposite(Composite parent) {
		compositeFormularyButtons = new Composite(parent,SWT.NONE) 
		var buttonGridLayout = new GridLayout(5,false) 
		compositeFormularyButtons.layout = buttonGridLayout 
		compositeFormularyButtons.layoutData = new GridData(SWT.FILL,SWT.FILL,true,false) 
		
		buttonAddFormula = createButton(compositeFormularyButtons,SWT.PUSH, "add", false, false)
		buttonAddFormula.layoutData = new GridData(SWT.LEFT,SWT.NONE,false,false) 
		buttonAddFormula.addSelectionListener(new SelectionListener(){
			override widgetSelected(SelectionEvent e) {
				var formula = textFieldFormulas.text 
				handler.add(formula) 
				refreshAll
				autoCheck(Arrays.asList(handler.getFormulaByExpression(formula))) 
			}
			override widgetDefaultSelected(SelectionEvent e) {}
		}) 			
		
		buttonDeleteFormula = createButton(compositeFormularyButtons, SWT.PUSH, "delete", false, false)
		buttonDeleteFormula.setLayoutData(new GridData(SWT.LEFT,SWT.NONE,false,false)) 
		buttonDeleteFormula.addSelectionListener(new SelectionListener(){
			override widgetSelected(SelectionEvent e) {
				val selection = viewer.structuredSelection.toList
				
				activeGraphModel.transact("Delete Formulas",[
					selection.forEach[
						handler.remove(it as CheckFormula)
					]
				])
				refreshAll
			}
			override widgetDefaultSelected(SelectionEvent e) {}
		}) 
		
		
		buttonCheckFormulas = createButton(compositeFormularyButtons, SWT.PUSH, "check formulas", true, false)
		buttonCheckFormulas.setLayoutData(new GridData(SWT.LEFT,SWT.NONE,false,false)) 
		buttonCheckFormulas.addSelectionListener(new SelectionListener(){
			override widgetSelected(SelectionEvent e) {
				checkAllFormulas 
			}
			
			override widgetDefaultSelected(SelectionEvent e) {}
		}) 
			
			
		labelLastCheck = new Label(compositeFormularyButtons,SWT.LEFT) 
		labelLastCheck.text = '''Last Check: not checked...'''
		
		buttonShowDescription = createButton(compositeFormularyButtons,SWT.CHECK, "Description", true, false)
		buttonShowDescription.setLayoutData(new GridData(SWT.RIGHT,SWT.NONE,true,false)) 
		buttonShowDescription.addSelectionListener(new SelectionListener(){
			override widgetSelected(SelectionEvent e) {
				
				actionToggleShowingDescription.run
			}
			
			override widgetDefaultSelected(SelectionEvent e) {}
		}) 
		
		compositeFormularyButtons.layout()
	}
	
	private def createBottomComposite(Composite parent){
		bottomComposite = new Composite(parent, SWT.NONE)
		bottomComposite.layout = new GridLayout(3,false)
		bottomComposite.layoutData = new GridData(SWT.FILL, SWT.NONE, true, false)
		
		labelStatus = new Label(bottomComposite,SWT.WRAP) 
		labelStatus.layoutData = new GridData(SWT.BEGINNING,SWT.TOP,true,false) 
		labelStatus.text = "Status: " 
		
		buttonWithSelection = createButton(bottomComposite, SWT.CHECK, "Selection Mode", true, false)
		buttonWithSelection.layoutData = new GridData(SWT.RIGHT,SWT.NONE,false,false)
		buttonWithSelection.addSelectionListener(new SelectionListener{
			override widgetSelected(SelectionEvent e) {
				actionToggleWithSelection.run
			}
			
			override widgetDefaultSelected(SelectionEvent e) {}
		}) 	
		
		buttonReverseMode = createButton(bottomComposite, SWT.CHECK, "Reverse Mode", true, true)
		buttonReverseMode.layoutData = new GridData(SWT.RIGHT,SWT.NONE,false,false)
		buttonReverseMode.addSelectionListener(new SelectionListener(){
			override widgetSelected(SelectionEvent e) {
				
				if (preferences.inReverseMode){
					deactivateReverseMode
				}else{
					preferences.inReverseMode = true
					reverseCheckState.checkableModel = topicalityHelper.activeCheckableModel
					selectionChanged(null,null)
					
				}
				refreshTableViewer
				refreshButtons			
			}
			
			override widgetDefaultSelected(SelectionEvent e) {}
		})
		
		//bottomComposite.createModeSelectionComposite
		bottomComposite.layout()
	}
	
	private def createButton(Composite parent, int style, String text, boolean enabled, boolean selection){
		var button = new Button(parent, style)
		button.text = text
		button.enabled = enabled
		button.selection = selection
		button
	}
	
	//Util methods
	protected def hide(){
		compositeTextFieldCheck.visible = false
		compositeFormulary.visible = false
		bottomComposite.visible = false
				
		viewSite.actionBars.toolBarManager => [
			items.forEach[visible = false]
			update(true)
		]
	}
	
	protected def layout(){
		compositeFormulary.layout()
		bottomComposite.layout()
		composite.layout() 
		compositeFormularyButtons.layout()
	}
	
	def refreshAll() {
		if (activeAdapter === null) return;
		refreshTableViewer
		refreshButtons
		refreshStatusText
		refreshHighlight
		layout
	}
	
	protected def refreshStatusText() {
		if (activeAdapter === null) return;
		refreshStatusText("", "") 
	}
	
	protected def refreshStatusText(String prefix, String suffix) {
		if (activeAdapter === null) return;
		var selection = (viewer.structuredSelection as StructuredSelection) 
		var statusText = StatusTextProvider.getStatusText(prefix, suffix, resultUpToDate, selection) 
		labelStatus.text = statusText
		bottomComposite.layout();
	}
	
	protected def refreshHighlight(){
		if (activeAdapter === null) return;
		if (preferences.showingHighlight){
			highlightManager.highlightOn
		}else{
			highlightManager.highlightOff
		}
	}
	
	protected def refreshTableViewer() {
		if (activeAdapter === null || handler === null) return;
		viewer.refresh 
	}
	
	protected def void refreshButtons() {
		if (activeAdapter === null) return;
		buttonShowDescription.selection = showingDescription
		buttonWithSelection.selection = withSelection

		buttonReverseMode.visible = handler!==null
		(buttonReverseMode.layoutData as GridData).exclude = handler===null
		
		buttonReverseMode.selection = inReverseMode
		
		if (handler !== null) {
			if (state != State.IDLE) {
				buttonAddFormula.enabled = false
				buttonDeleteFormula.enabled = false
				buttonCheckFormulas.enabled = false 
				buttonReverseMode.enabled = false
			} else {
				buttonAddFormula.enabled = textFieldFormulas.text.trim != "" 
					&& !handler.contains(textFieldFormulas.text)
				
				buttonDeleteFormula.enabled = !viewer.selection.empty
				
				buttonCheckFormulas.enabled = !handler.formulas.empty && handler.hasToCheck
				
				val resultsPresent = handler?.resultsPresent
				buttonReverseMode.enabled = resultsPresent
				if (!resultsPresent && inReverseMode){
						deactivateReverseMode
				}
			}
		}
		
		buttonCheck.enabled = !inReverseMode
		buttonCheckFormulas.enabled = !inReverseMode
		buttonWithSelection.enabled = !inReverseMode
	}
	
	def void setNewHighlight(Set<Node> nodes) {
		highlightManager.newHighlightDescriptions = #{preferences.highlightColor -> nodes}
		refreshHighlight
	}
	
	protected def isShowingDescription(){
		preferences.showingDescription
	}
	
	protected def isWithSelection(){
		preferences.withSelection
	}
	
	protected def isInReverseMode(){
		preferences.inReverseMode
	}
	
	protected def isWithAutoCheck() {
		handler !== null && preferences.withAutoCheck
	}
	
	def getViewer(){
		viewer
	}
	
}

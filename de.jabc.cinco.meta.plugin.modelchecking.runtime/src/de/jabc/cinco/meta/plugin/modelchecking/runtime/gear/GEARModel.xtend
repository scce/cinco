/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.gear

import de.metaframe.gear.model.Model

class GEARModel extends de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableModel implements Model<de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode, de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge> {

	override getAtomicPropositions(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode node) {
		node.atomicPropositions
	}

	override getEdgeLabels(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge edge) {
		edge.labels
	}

	override getEdgeType(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge edge) {
		Model$EdgeType.MUST
	}

	override getIdentifier(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode node) {
		node.id
	}

	override getIncomingEdges(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode node) {
		node.incoming
	}

	override getOutgoingEdges(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableNode node) {
		node.outgoing
	}

	override getInitialNodes() {
		nodes.filter[isStartNode].toSet
	}

	override getSource(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge edge) {
		edge.source
	}

	override getTarget(de.jabc.cinco.meta.plugin.modelchecking.runtime.model.BasicCheckableEdge edge) {
		edge.target
	}
}

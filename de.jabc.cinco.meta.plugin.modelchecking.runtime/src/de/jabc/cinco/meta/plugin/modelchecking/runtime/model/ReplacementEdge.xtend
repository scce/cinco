/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.model

import graphmodel.Node
import java.util.Set

class ReplacementEdge {
	Node source
	Node target

	Set<String> labels = newHashSet

	new(Node source, Node target, Set<String> labels) {
		this.source = source
		this.target = target
		if (labels !== null) {
			this.labels.addAll(labels)
		}
	}
	
	def getSource(){
		source
	}
	
	def getTarget(){
		target
	}
	
	def setSource(Node source){
		this.source = source
	}
	
	def setTarget(Node target){
		this.target = target
	}
	
	def getLabels(){
		labels
	}
	
	def setLabels(Set<String> labels){
		this.labels = labels
	}
	
	def isValidEdge(){
		source !== null && target !== null
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.core

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import java.util.List
import static de.jabc.cinco.meta.core.utils.job.JobFactory.job
import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension

import graphmodel.GraphModel
import graphmodel.Node
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.ModelChecker
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.SubToSatProvider

class CheckExecution {
	
	extension WorkbenchExtension = new WorkbenchExtension
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	
	var Set<CheckExecutionListener> listeners = newHashSet
	
	new(){}
	
	def <N,E> executeFormulasCheck(ModelCheckingAdapter adapter, ModelChecker<CheckableModel<N,E>,N,E> checker, GraphModel model, List<CheckFormula> formulas, boolean withSelection){
		if (checker === null) throw new RuntimeException("No ModelChecker set.")
		val checkableModel = adapter?.getCheckableModel(checker, withSelection)
		if (adapter === null || formulas.size == 0){
			notifyCheckFinished(formulas)
			return
		}

		job("Check Formulas")
			.consumeConcurrent(100)
			.taskForEach(formulas,[checker.checkAndInform(model, checkableModel, it)],[getJobName])
			.onDone[notifyCheckFinished(formulas)]
			.schedule
	}
	
	private def <N,E> getCheckableModel(ModelCheckingAdapter adapter, ModelChecker<CheckableModel<N,E>,N,E> checker, boolean withSelection){
		val checkableModel = checker.createCheckableModel
		adapter.buildCheckableModel(checkableModel, withSelection)
		checkableModel
	}
 	
 	def <N,E> getSatisfyingNodes(ModelChecker<CheckableModel<N,E>,N,E> checker, GraphModel model, CheckableModel<N,E> checkableModel, String expression) throws Exception{	
		val satisfyingNodeIds = checker.getSatisfyingNodes(checkableModel, expression).map[checkableModel.getId(it)].toSet
		model.allNodesAndContainers.filter[satisfyingNodeIds.contains(it.id)].toSet		
	}
	
	def <N,E> getSubToSatMap(SubToSatProvider<CheckableModel<N,E>,N,E> checker, GraphModel model, CheckableModel<N,E> checkableModel, String expression) throws Exception{	
		checker.getSubToSatMap(checkableModel, expression)?.mapValues[	
			val ids = map[checkableModel.getId(it)].toSet
			model.allNodesAndContainers.filter[ids.contains(it.id)].toSet
		]
	}
	
	def <N,E> checkAndInform(ModelChecker<CheckableModel<N,E>,N,E> checker, GraphModel model, CheckableModel<N,E> checkableModel, CheckFormula formula){
		if (formula.toCheck){
			try{
				var Set<Node> satisfyingNodes
				
				if (checker.supportsSubToSat){
					val map = getSubToSatMap(checker as SubToSatProvider<CheckableModel<N,E>,N,E>, model, checkableModel, formula.fullExpression)
					
					satisfyingNodes = map?.get(formula.fullExpression)

					formula.subToSatList = map?.entrySet?.sortBy[-key.length]
					formula.subToResultList = map?.mapValues[getFormulaCheckResult(model, checkableModel, it)]?.entrySet?.sortBy[-key.length]
				}				
				
				if (satisfyingNodes === null){
					satisfyingNodes = checker.getSatisfyingNodes(model, checkableModel, formula.fullExpression)
				}
				
				formula.satisfyingNodes = satisfyingNodes
				
				formula.result = model.getFormulaCheckResult(checkableModel, satisfyingNodes)
				
			}catch(Exception e){
				formula.resetCheckResult
				formula.result = Result.ERROR
				formula.errorMessage = e.message
			}
		}else{
			formula.resetCheckResult
		}
		notifyNewResult(formula) 			
	}
	
	def getFormulaCheckResult(GraphModel model, CheckableModel<?,?> checkableModel, Set<Node> satisfying){
		if (model.fulfills(checkableModel,satisfying)){
			return Result.TRUE
		}
		Result.FALSE
	}
	
	def fulfills(GraphModel model, CheckableModel<?,?> checkableModel, Set<Node> satisfying){
		val adapter = model.adapter
		if (adapter === null) throw new RuntimeException("No suitable adapter found.")
		model.adapter.fulfills(model, checkableModel,satisfying)
	}
	
	def getJobName(CheckFormula formel){
		if (formel.description.equals("")){
			return "Checking " + formel.expression
		}
		else {
			return "Checking " + formel.description
		}
	}	
	
	//Listener methods
	interface CheckExecutionListener {
		def void onNewResult(CheckFormula formula)
		def void onFinished(List<CheckFormula> formulas)
	}
	
	def addCheckExecutionListener(CheckExecutionListener listener){
		listeners.add(listener)
	}
	
	def removeCheckExecutionListener(CheckExecutionListener listener){
		listeners.remove(listener)
	}
	
	def notifyNewResult(CheckFormula formula){
		async[listeners.forEach[
			onNewResult(formula)
		]]
	}
	
	def notifyCheckFinished(List<CheckFormula> formulas){
		async[listeners.forEach[onFinished(formulas)]]
	}	
}

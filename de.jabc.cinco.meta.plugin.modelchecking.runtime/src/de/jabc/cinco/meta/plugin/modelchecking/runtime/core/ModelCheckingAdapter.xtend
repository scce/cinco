/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.core

import java.util.Set
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import graphmodel.GraphModel
import graphmodel.Node
import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightDescription

interface ModelCheckingAdapter {
	def boolean canHandle(GraphModel model)

	def void buildCheckableModel(GraphModel model, CheckableModel<?, ?> checkableModel, boolean withSelection)

	def FormulaHandler<?, ?> getFormulaHandler()

	def Class<?> getFormulasTypeClass()
	
	def String getSettings(String key)

	def boolean fulfills(GraphModel model, CheckableModel<?, ?> checkableModel, Set<Node> satisfyingNodes)
	
	def Set<HighlightDescription> getHighlightDescriptions(GraphModel model, Set<HighlightDescription> intendedHighlightDescriptions)

}

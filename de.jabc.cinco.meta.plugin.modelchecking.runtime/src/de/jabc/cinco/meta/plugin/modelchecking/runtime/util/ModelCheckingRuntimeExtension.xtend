/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.util

import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.ModelCheckingAdapter
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.ModelChecker
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.SubToSatProvider
import de.jabc.cinco.meta.runtime.xapi.GraphModelExtension
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import graphmodel.GraphModel
import graphmodel.Node
import graphmodel.internal.impl.InternalModelElementImpl
import java.util.Set
import java.util.concurrent.atomic.AtomicReference
import java.util.function.Supplier
import org.eclipse.core.runtime.Platform
import org.eclipse.swt.graphics.RGB

class ModelCheckingRuntimeExtension {
	extension WorkbenchExtension = new WorkbenchExtension
	extension GraphModelExtension = new GraphModelExtension

	static final String ADAPTER_EXTENSION_ID = "de.jabc.cinco.meta.plugin.modelchecking.runtime.adapter"
	static final String CHECKER_EXTENSION_ID = "de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker"

	private def getModelCheckAdapters() {
		val adapters = newArrayList
		val configElements = Platform.extensionRegistry.getConfigurationElementsFor(ADAPTER_EXTENSION_ID)
		for (element : configElements) {
			val o = element.createExecutableExtension("class")
			if (o instanceof ModelCheckingAdapter) {
				adapters.add(o)
			}
		}
		if (adapters.empty) {
			println("Adapters are empty")
		}
		adapters
	}

	def getModelCheckers() {
		val checkers = newArrayList
		val configElements = Platform.extensionRegistry.getConfigurationElementsFor(CHECKER_EXTENSION_ID)
		for (element : configElements) {
			val o = element.createExecutableExtension("class")
			if (o instanceof ModelChecker<?, ?, ?>) {
				checkers.add(o)
			}
		}
		if (checkers.empty) {
			println("Checkers are empty")
		}
		checkers
	}

	def supportsSubToSat(ModelChecker<?, ?, ?> checker) {
		checker instanceof SubToSatProvider<?, ?, ?>
	}

	def getActiveAdapter() {
		activeModel.adapter
	}

	def getAdapter(GraphModel model) {
		modelCheckAdapters?.findFirst[it.canHandle(model)]
	}

	def getActiveModel() {
		syncedGet[activeGraphModel]
	}
	
	def getAllNodesAndContainers(GraphModel model){
		model.find(Node).toSet
	}
	

	def buildCheckableModel(ModelCheckingAdapter adapter, CheckableModel<?, ?> model, boolean withSelection) {
		adapter.buildCheckableModel(syncedGet[activeGraphModel], model, withSelection)
	}

	def buildCheckableModel(CheckableModel<?, ?> model, boolean withSelection) {
		buildCheckableModel(syncedGet[activeAdapter], model, withSelection)
	}

	def fulfills(ModelCheckingAdapter adapter, CheckableModel<?, ?> model, Set<Node> satisfying) {
		adapter.fulfills(activeModel, model, satisfying)
	}

	def fulfills(CheckableModel<?, ?> model, Set<Node> satisfying) {
		activeAdapter.fulfills(model, satisfying)
	}

	def getSelectedElementIds() {
		val elements = syncedGet[activeDiagramEditor?.selectedPictogramElements]
		val ids = newArrayList
		elements?.forEach [
			val businessObject = syncedGet[it?.businessObject]
			if (businessObject instanceof InternalModelElementImpl) {
				ids.add(businessObject?.element?.id)
			}
		]
		ids
	}

	def <V> syncedGet(Supplier<V> supplier) {
		val AtomicReference<V> ref = new AtomicReference
		sync[ref.set(supplier.get)]
		ref.get
	}

	def <V> asyncGet(Supplier<V> supplier) {
		val AtomicReference<V> ref = new AtomicReference
		async[ref.set(supplier.get)]
		ref.get
	}

	def <T extends Node> findPathsToFirst(Node node, Class<T> clazz, (T)=>boolean predicate) {
		(new GraphModelExtension).findPathsToFirst(node, clazz, predicate)
	}

	def getNodeById(GraphModel model, String id) {
		model.allNodesAndContainers.findFirst[it.id == id]
	}
	
	def getNodesById(GraphModel model, Set<String> ids) {
		model.allNodesAndContainers.filter[ids.contains(it.id)].toSet
	}

	def getHighlightColor(ModelCheckingAdapter adapter) {
		parseRGB(adapter.getSettings("highlightColor"))
	}

	def parseRGB(String value) {
		if (value.startsWith("(") && value.endsWith(")")) {
			val values = value.substring(1, value.length - 1).split(",").map[trim]

			if (values.size == 3) {
				val rgbNumbers = newLinkedList

				rgbNumbers.add(Integer.parseInt(values.get(0)))
				rgbNumbers.add(Integer.parseInt(values.get(1)))
				rgbNumbers.add(Integer.parseInt(values.get(2)))

				if (!rgbNumbers.exists[it < 0 || it > 255]) {
					return new RGB(rgbNumbers.get(0), rgbNumbers.get(1), rgbNumbers.get(2))
				}
			}

		}
		return null
	}

	def log(String title, CharSequence text) {
		val length = Math.max(title.length + 8, 60)
		val headLength = ((length - title.length) / 2) as int
		var firstLine = ""

		for (var i = 0; i < headLength; i++) {
			firstLine += "="
		}

		firstLine = firstLine + title + firstLine

		var lastLine = ""

		for (var i = 0; i < firstLine.length; i++) {
			lastLine += "="
		}

		println('''
			«firstLine»
			«text»
			«lastLine»
		''')
	}

}

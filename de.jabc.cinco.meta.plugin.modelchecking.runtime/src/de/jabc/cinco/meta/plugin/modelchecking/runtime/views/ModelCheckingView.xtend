/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.CheckExecution
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.CheckExecution.CheckExecutionListener
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.ModelCheckingAdapter
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.Result
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.CheckFormula
import de.jabc.cinco.meta.plugin.modelchecking.runtime.formulas.FormulaHandler
import de.jabc.cinco.meta.plugin.modelchecking.runtime.highlight.HighlightManager
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.ModelChecker
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import de.jabc.cinco.meta.plugin.modelchecking.runtime.views.TopicalityHelper.TopicalityListener
import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.List
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.Path
import org.eclipse.core.runtime.Platform
import org.eclipse.jface.action.Action
import org.eclipse.jface.action.Separator
import org.eclipse.jface.dialogs.IDialogConstants
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.resource.ImageDescriptor
import org.eclipse.jface.viewers.ISelection
import org.eclipse.swt.graphics.Image
import org.eclipse.swt.widgets.Composite
import org.eclipse.ui.IEditorPart
import org.eclipse.ui.IWorkbenchPart
import org.eclipse.ui.IWorkbenchPartReference

class ModelCheckingView extends ModelCheckingViewPart implements TopicalityListener, CheckExecutionListener {
	extension WorkbenchExtension = new WorkbenchExtension
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension

	public static val String ID = "de.jabc.cinco.meta.plugin.modelchecking.runtime.views.ModelCheckingView"

	List<ModelChecker<?, ?, ?>> modelCheckers

	enum ViewMode {
		STANDARD,
		SELECTION_MODE,
		REVERSE_MODE
	}

	State state
	public ReverseCheckState reverseCheckState

	public boolean resultUpToDate

	public PreferenceManager preferences
	public FormulaHandler<?, ?> handler
	CheckExecution execution
	ModelCheckingAdapter activeAdapter
	TopicalityHelper topicalityHelper

	IEditorPart activeEditor

	override createPartControl(Composite parent) {

		super.createPartControl(parent)

		state = State.IDLE
		this.execution = new CheckExecution
		execution.addCheckExecutionListener(this)

		this.topicalityHelper = new TopicalityHelper
		activePage.addSelectionListener(topicalityHelper)
		topicalityHelper?.addTopicalityListener(this)
		topicalityHelper.start

		modelCheckers = getModelCheckers
		preferences = new PreferenceManager(modelCheckers)

		reverseCheckState = new ReverseCheckState

		highlightManager = new HighlightManager
		makeActions
		hookMenus
		hide
		reloadView
	}

	def reloadView() {
		topicalityHelper.pause

		var newEditor = activeDiagramEditor
		if (newEditor === null) {		// no graphmodel editor
			if(activeEditor === null) return;
			activeEditor = null
			activeAdapter = null
			hide
			return
		} else if (newEditor == activeEditor && handler !== null) { // same graphmodel editor as before
			topicalityHelper.unpause
			return
		}

		hide

		activeEditor = newEditor
		this.activeAdapter = _modelCheckingRuntimeExtension.getActiveAdapter
		handler = activeAdapter?.formulaHandler

		if(activeAdapter === null) return; // graphmodel doesn't support modelchecking

		viewSite.actionBars.toolBarManager => [
			items.forEach[visible = true]
			update(true)
		]
		textFieldFormulas.text = ""
		preferences.loadPreferences(activeGraphModel)
		setUpModelChecker
		topicalityHelper.reloadAdapters
		topicalityHelper.withSelection = withSelection
		compositeTextFieldCheck.visible = true
		bottomComposite.visible = true

		if (handler !== null) { // graphmodel supports formulas
			compositeFormulary.visible = true
			compositeTableViewer.formulaHandler = handler
			checkAllFormulas
		}

		refreshAll
	}

	private def void setUpModelChecker() {
		val checker = preferences.modelChecker as ModelChecker<CheckableModel<?, ?>, ?, ?>
		topicalityHelper.modelChecker = checker
	}

	// Checking methods
	override checkTextFieldFormula(String expression) {
		val formula = new CheckFormula(null, expression, null)
		if (handler !== null){
			formula.fullExpression = handler.getFullExpression(formula)
		}else {
			formula.fullExpression = expression
		}
		buttonCheck.enabled = false
		checkFormulas(#[formula])
	}

	override checkAllFormulas() {
		state = State.FORMULAS_CHECK
		handler.formulas.checkFormulas
	}

	private def <N, E> checkFormulas(List<CheckFormula> formulas) {
		labelStatus.text = "Status: checking..."
		refreshButtons
		bottomComposite.layout()
		composite.layout()
		val withSelection = withSelection
		new Thread [
			execution.executeFormulasCheck(activeAdapter,
				preferences.modelChecker as ModelChecker<CheckableModel<N, E>, N, E>, activeModel, formulas,
				withSelection)
		].start
	}

	override autoCheckAllFormulas() {
		if (withAutoCheck) {
			state = State.AUTO_CHECK_ALL
			handler.formulas.autoCheck
		}
	}

	override autoCheck(List<CheckFormula> formulas) {
		if (withAutoCheck) {
			formulas.checkFormulas
			if(state == State.IDLE) state = State.AUTO_CHECK
		}
	}

	override getSatisfyingOfFormulas(List<CheckFormula> formulas) {
		var satisfyingAllFormulas = newHashSet

		if (formulas.size > 0) {
			if (formulas.size == 1) {
				return formulas.head.satisfyingNodes
			}

			for (node : formulas.head.satisfyingNodes) {
				var satisfyingAll = !formulas.subList(1, formulas.size).exists [
					satisfyingNodes.map[id].forall[it != node.id]
				]

				if (satisfyingAll) {
					satisfyingAllFormulas.add(node)
				}
			}
		}
		return satisfyingAllFormulas
	}

	override deactivateReverseMode() {
		preferences.inReverseMode = false
		if (!resultUpToDate && preferences.hadAutoCheckEnabled) {
			if (MessageDialog.openQuestion(composite.shell, "Reactivate auto check?",
				"The model changed while reverse mode was active." +
					"\nAuto check will override your current check results." +
					"\nDo you want to reactivate auto check?")) {
				preferences.toggleAutoCheck
			} else {
				preferences.hadAutoCheckEnabled = false
			}
		} else if (preferences.hadAutoCheckEnabled) {
			preferences.toggleAutoCheck
		}
		checkableModelChanged(resultUpToDate)
	}

	override partActivated(IWorkbenchPartReference partRef) {
		reloadView()
	}

	override partBroughtToTop(IWorkbenchPartReference partRef) {
	}

	override partClosed(IWorkbenchPartReference partRef) {
		reloadView()
	}

	override partDeactivated(IWorkbenchPartReference partRef) {
	}

	override partOpened(IWorkbenchPartReference partRef) {
	}

	override partHidden(IWorkbenchPartReference partRef) {
	}

	override partVisible(IWorkbenchPartReference partRef) {
	}

	override partInputChanged(IWorkbenchPartReference partRef) {
		System.out.println("Part input changed");
	}

	override onNewResult(CheckFormula formula) {
		if (formula.id === null) {
			if (formula.result == Result.ERROR) {
				labelStatus.text = '''Status: Error. «formula.errorMessage»'''
			} else {
				val satisfying = formula.satisfyingNodes
				satisfying.setNewHighlight
				labelStatus.text = StatusTextProvider.getStatusText(satisfying.size, formula.result)
			}

			buttonCheck.enabled = true
			bottomComposite.layout()
			composite.layout()
		} else {
			if (activeAdapter !== null && handler !== null)
			refreshTableViewer
		}
	}

	override onFinished(List<CheckFormula> formulas) {
		val notChecked = formulas.filter[toCheck].filter[result == Result.NOT_CHECKED].toList
		if (notChecked.empty) {
			if (state == State.AUTO_CHECK_ALL || state == State.FORMULAS_CHECK) {
				topicalityHelper.refreshCheckableModel
				resultUpToDate = true
				highlightManager.clearHighlight
				var datetime = LocalDateTime.now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
				if (activeAdapter !== null){
					labelLastCheck.text = '''Last Check: «datetime»; Model checker: «preferences.modelChecker.name»'''
				}				
			}
			if (state != State.IDLE) {
				state = State.IDLE
				refreshAll
			}
		} else {
			// Workaround for startup issue
			notChecked.checkFormulas
		}

	}

	override checkableModelChanged(boolean resultUpToDate) {

		if (!isInReverseMode) {
			if (withAutoCheck && !resultUpToDate) {
				autoCheckAllFormulas
			} else {
				this.resultUpToDate = resultUpToDate
				highlightManager.clearHighlight
				refreshAll
			}
		} else {
			this.resultUpToDate = resultUpToDate
		}
	}

	override formulasChanged() {
		if (!handler.editing) {
			handler.reloadFormulas
			autoCheckAllFormulas
			highlightManager.clearHighlight
			refreshAll
		}
	}

	override selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (inReverseMode) {
			reverseCheckState.updateSelection
			refreshTableViewer
			if (reverseCheckState.containsInvalidNodes) {
				labelStatus.text = '''Selection contains nodes that were not in the checked model!'''
			} else {
				labelStatus.text = ''''''
			}
			layout
		}
	}

	override getActiveAdapter() {
		activeAdapter
	}

	override getHandler() {
		handler
	}

	override getPreferences() {
		preferences
	}

	override getReverseCheckState() {
		reverseCheckState
	}

	override getTopicalityHelper() {
		topicalityHelper
	}

	override getState() {
		state
	}

	override resultUpToDate() {
		resultUpToDate
	}

	// Create Actions
	protected def makeActions() {

		actionOpenSettingsDialog = new Action {

			override run() {

				val settingsDialog = new PreferencesDialog(composite.shell, modelCheckers, preferences,
					handler !== null)

				settingsDialog.create
				settingsDialog.open

				val result = settingsDialog.returnCode

				if (result == IDialogConstants.CANCEL_ID) {
					preferences.discard
				} else {
					val checkRequired = preferences.changesAffectChecks
					if (result == IDialogConstants.OK_ID) {
						preferences.apply
					} else if (result == IDialogConstants.YES_TO_ALL_ID) {
						preferences.applyToAll(handler !== null)
					}
					if (checkRequired) {
						autoCheckAllFormulas
					}
					refreshAll
				}
			}

		} => [
			text = "Preferences"
			toolTipText = "Preferences"

			imageDescriptor = ImageDescriptor.createFromImage(
				new Image(
					display,
					FileLocator.openStream(
						Platform.getBundle("de.jabc.cinco.meta.plugin.modelchecking.runtime"),
						new Path("icons/preferences_16dp.png"),
						true
					)
				)
			)
		]

		actionToggleShowingDescription = new Action {

			override run() {
				preferences.toggleShowingDescription
				if (preferences.showingDescription) {
					compositeTableViewer.showDescriptions
				} else {
					compositeTableViewer.showFormulas
				}
				refreshAll
			}

			override getStyle() {
				AS_CHECK_BOX
			}
		}

		actionToggleWithSelection = new Action {

			override run() {
				preferences.toggleWithSelection
				topicalityHelper.withSelection = withSelection
				refreshButtons
				topicalityHelper.selectionChanged(null, null)

			}

			override getStyle() {
				AS_CHECK_BOX
			}
		}
	}

	protected def hookMenus() {
		val bars = viewSite.actionBars

		val tbm = bars.toolBarManager
		tbm.removeAll

		tbm.add(actionOpenSettingsDialog)
		tbm.add(new Separator)
	}

}

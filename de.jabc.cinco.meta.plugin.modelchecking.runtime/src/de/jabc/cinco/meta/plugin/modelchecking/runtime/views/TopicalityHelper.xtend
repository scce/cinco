/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.modelchecking.runtime.views

import de.jabc.cinco.meta.runtime.xapi.WorkbenchExtension
import de.jabc.cinco.meta.plugin.modelchecking.runtime.util.ModelCheckingRuntimeExtension
import de.jabc.cinco.meta.core.utils.job.ReiteratingThread
import org.eclipse.emf.ecore.resource.Resource
import de.jabc.cinco.meta.plugin.modelchecking.runtime.core.ModelCheckingAdapter
import java.util.Set
import java.util.HashSet
import org.eclipse.emf.ecore.util.EContentAdapter
import java.util.concurrent.atomic.AtomicReference
import org.eclipse.emf.common.notify.Notification
import graphmodel.internal.impl.InternalGraphModelImpl
import org.eclipse.emf.ecore.EObject
import org.eclipse.ui.IWorkbenchPart
import org.eclipse.jface.viewers.ISelection
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.ISelectionListener
import de.jabc.cinco.meta.plugin.modelchecking.runtime.modelchecker.ModelChecker
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.CheckableModel
import de.jabc.cinco.meta.plugin.modelchecking.runtime.model.ModelComparator

class TopicalityHelper implements ISelectionListener{
	extension WorkbenchExtension = new WorkbenchExtension
	extension ModelCheckingRuntimeExtension = new ModelCheckingRuntimeExtension
	
	ReiteratingThread thread
	Resource activeResource
	CheckableModel<?,?> activeCheckableModel
	ModelChecker<?,?,?> checker
	ModelCheckingAdapter activeAdapter
	ModelComparator<CheckableModel<?,?>> comparator
	boolean withSelection
	Set<TopicalityListener> listeners = new HashSet
	EContentAdapter resourceEContentAdapter
	
	enum EditState{NO_CHANGE, MODEL_EDIT, NEW_MODEL}
	
	AtomicReference<TopicalityHelper.EditState> editState
	
	new (){
		editState = new AtomicReference
		editState.set(EditState.NO_CHANGE)
		withSelection = false	
		resourceEContentAdapter = new EContentAdapter() {
			
			override notifyChanged(Notification notification) {
				if (notification.formulasChanged){//} || notification.descriptionEdit){
					listeners.forEach[formulasChanged]
				}
				if (notification.newGraphModel){
					editState.set(EditState.NEW_MODEL)
				}
				editState.compareAndSet(EditState.NO_CHANGE, EditState.MODEL_EDIT)
			}
			
		}
		initThread
	}
	
	def setModelChecker(ModelChecker<CheckableModel<?,?>,?,?> checker){
		this.checker = checker
		comparator = checker.comparator as ModelComparator<CheckableModel<?,?>>
	}
	
	def isNewGraphModel(Notification notification){
		!notification.touch &&
		notification.eventType == Notification.ADD && 
		notification.newValue instanceof InternalGraphModelImpl
	}
	
	def formulasChanged(Notification notification){
		
		if (!notification.touch && notification.notifier instanceof EObject){
			val notifier = (notification.notifier as EObject)?.eContainer?.class
			val formulasTypeClass = activeAdapter?.formulasTypeClass
			if (activeAdapter === null) {println("adapter null")}
			if (formulasTypeClass === null) {println("formulas null");return false;}
			
			notifier?.isAssignableFrom(formulasTypeClass) ||
			notification.oldValue?.class?.isAssignableFrom(formulasTypeClass) ||
			notification.newValue?.class?.isAssignableFrom(formulasTypeClass)			
		}
		
	}
	
	def refreshCheckableModel(){
		if (activeAdapter !== null){
			activeCheckableModel = retrieveActiveCheckableModel
			editState.set(EditState.NO_CHANGE)
			unpause
		}else{
			pause
			activeCheckableModel = null
		}
	}
	
	
	
	def setWithSelection(boolean withSelection){
		if (this.withSelection != withSelection){
			editState.compareAndSet(EditState.NO_CHANGE, EditState.MODEL_EDIT)
		}
		this.withSelection = withSelection
	}
	
	def reloadAdapters(){
		this.activeAdapter = getActiveAdapter
		setResourceAdapter
	}
	
	
	def isEqualCheckableModel(){		
		comparator.areEqualModels(retrieveActiveCheckableModel,activeCheckableModel)
	}
	
	def retrieveActiveCheckableModel(){
		val newCheckableModel = checker?.createCheckableModel
		activeAdapter?.buildCheckableModel(newCheckableModel,withSelection)
		newCheckableModel
	}
	
	def getActiveCheckableModel(){
		activeCheckableModel
	}
	
	def initThread() {
		thread = new ReiteratingThread(2000){			
			override protected work() {
				if (activeCheckableModel === null){
					pause
				}
				if (editState.get == EditState.NO_CHANGE){
				}else {
					val state = editState.get					
					editState.set(EditState.NO_CHANGE)
					
					if (state == EditState.NEW_MODEL){
						async[listeners.forEach[formulasChanged]]
					}
					
					if (!isEqualCheckableModel){
						async[listeners.forEach[checkableModelChanged(false)]]	
					}else{
						async[listeners.forEach[checkableModelChanged(true)]]
					}					
				}
			}
			
		}
	}
	
	def start(){
		thread?.start
	}
	
	def void setResourceAdapter(){		
		activeResource?.eAdapters?.remove(resourceEContentAdapter)		
		activeResource = activeGraphModel?.eResource
		activeResource?.eAdapters?.add(resourceEContentAdapter)
	}
	
	def togglePause(){
		if (thread.paused) unpause
		else pause
	}
	
	def pause(){
		thread.pause
	}
	
	def unpause(){
		thread.unpause
	}
	
	def cleanup() {
		PlatformUI.workbench.activeWorkbenchWindow.activePage.removeSelectionListener(this)	
		activeResource?.eAdapters?.remove(resourceEContentAdapter)		
		thread?.quit
	}
	
	override selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (withSelection){
			editState.compareAndSet(EditState.NO_CHANGE, EditState.MODEL_EDIT)
		}
	}

	//Listener
	def addTopicalityListener(TopicalityListener listener){		
		if (listener !== null) listeners.add(listener) 
	}
	
	def removeTopicalityListener(TopicalityListener listener){
		listeners.remove(listener)
	}
	
	interface TopicalityListener  {
		def void checkableModelChanged(boolean resultUpToDate)
		def void formulasChanged()
	}
}

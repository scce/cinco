/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.gratext.runtime.resource

import de.jabc.cinco.meta.core.ui.editor.ResourceContributor
import de.jabc.cinco.meta.core.ui.editor.ResourceContributorRegistry
import de.jabc.cinco.meta.runtime.xapi.ResourceExtension
import graphmodel.GraphModel
import graphmodel.internal.InternalGraphModel
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.util.HashMap
import java.util.Map
import org.eclipse.emf.common.util.TreeIterator
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.InternalEObject
import org.eclipse.emf.ecore.impl.EClassImpl
import org.eclipse.emf.ecore.impl.EObjectImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.linking.lazy.LazyLinkingResource
import org.eclipse.xtext.parser.IParseResult
import org.eclipse.xtext.util.CancelIndicator

abstract class GratextResource extends LazyLinkingResource {
	
	extension val ResourceExtension = new ResourceExtension
	
	GraphModel model
	val HashMap<EObject,ResourceContributor> contributions = newHashMap
	Runnable internalStateChangedHandler
	Runnable newParseResultHandler
	boolean skipInternalStateUpdate
	
	def Transformer getTransformer(InternalGraphModel model)
	
	def Transformer getLastTransformer(InternalGraphModel model)
	
	def void removeTransformer(InternalGraphModel model)
	
	def boolean isSortGratext()
	
	def String serialize() {
		val internalModel = model?.getInternalElement_() ?: getContent(InternalGraphModel)
		(new Serializer(internalModel, internalModel.lastTransformer) => [
			sorted = isSortGratext
		]).run
	}
	
	override doSave(OutputStream outputStream, Map<?, ?> options) {
		new OutputStreamWriter(outputStream, encoding) => [
			write(serialize)
			flush
		]
	}
	
	override clearInternalState() {
		unload(getContents)
		transact[getContents.clear]
		clearErrorsAndWarnings
		setParseResult(null)
	}
	
	def onInternalStateChanged(Runnable runnable) {
		internalStateChangedHandler = runnable
	}
	
	def onNewParseResult(Runnable runnable) {
		newParseResultHandler = runnable
	}
	
	def getContent(int index) {
		getContents.get(index)
	}
	
	override getEObjectByID(String id) {
    	val eObject = intrinsicIDToEObjectMap?.get(id)
  		if (eObject !== null)
    		return eObject
		getContents.tail.toList.allProperContents.getEObjectById(id)
		?: if (getContents.size > 0) {
			getContents.head.allProperContents.getEObjectById(id)
		}
	}
	
	private def getEObjectById(TreeIterator<EObject> iterator, String id) {
		while (iterator.hasNext) {
			val eObject = iterator.next
			val eObjectId = EcoreUtil.getID(eObject)
			intrinsicIDToEObjectMap?.put(eObjectId,eObject)
			if (eObjectId == id)
	          return eObject
		}
	}
	
	def add(EObject object) {
		transact[getContents.add(object)]
	}
	
	def insert(int index, EObject object) {
		transact[getContents.add(index, object)]
	}
	
	def remove(EObject... objects) {
		transact[getContents.removeAll(objects)]
	}
	
	def clear() {
		transact[
			for (content : newArrayList(getContents))
				unload
			getContents.clear
		]
	}
	
	def unload(EObject... objects) {
		objects?.filterNull.forEach[unload]
	}
	
	def update(String newText) {
		update(0, getParseResult().getRootNode().getText().length(), newText)
	}
	
	def skipInternalStateUpdate() {
		skipInternalStateUpdate = true 
	}
	
	override updateInternalState(IParseResult oldParseResult, IParseResult newParseResult) {
		val oldRoot = oldParseResult?.rootASTElement
		if (oldRoot !== null && oldRoot != newParseResult.rootASTElement) {
			if (!skipInternalStateUpdate) {
				clear
				model = null
			}
		}
		updateInternalState(newParseResult)
	}
	
	override updateInternalState(IParseResult newParseResult) {
		parseResult = newParseResult
		val newRoot = newParseResult.rootASTElement
		if (newRoot !== null) {
			if (!skipInternalStateUpdate) {
				if (model === null) {
					insert(0, newRoot)
					newRoot.reattachModificationTracker
					clearErrorsAndWarnings
					addSyntaxErrors
					transact[
						doLinking
						transformModel
						addContributions
					]
					internalStateChangedHandler?.run
				}
			} else {
				skipInternalStateUpdate = false
			}
		}
		newParseResultHandler?.run
	}
	
	def transformModel() {
		val gratextModel = getContent(InternalGraphModel)
		val transformer = getTransformer(gratextModel)
		model = transformer.transform(gratextModel).element
		val internal = (model as EObjectImpl).eInternalContainer
		remove(gratextModel)
		add(model.getInternalElement_())
		model.setInternalElement_(internal as InternalGraphModel)
		removeTransformer(gratextModel)
	}
	
	def addContributions() {
		ResourceContributorRegistry.contributors?.forEach[contributor|
			contributor.contributeToResource(this)?.forEach[contribution|
				contributions.put(contribution, contributor)
			]
		]
	}
	
	override resolveLazyCrossReferences(CancelIndicator mon) {
		getContents
			.filter[isResolveCrossReferencesRequired]
			.forEach[resolveCrossReferences(mon ?: CancelIndicator.NullImpl)]
	}
	
	def isResolveCrossReferencesRequired(EObject obj) {
		val contributor = contributions.get(obj)
		(contributor === null) || contributor.isResolveCrossReferencesRequired(obj)
	}
	
	def void resolveCrossReferences(EObject obj, CancelIndicator monitor) {
		if (monitor.canceled) return;
		val iEobj = obj as InternalEObject
		val cls = iEobj.eClass
		val sup = cls.EAllStructuralFeatures as EClassImpl.FeatureSubsetSupplier
		sup.crossReferences?.forEach[
			if (monitor.canceled) return;
			if (isPotentialLazyCrossReference)
				doResolveLazyCrossReference(iEobj, it)
		]
		EcoreUtil.getAllContents(iEobj, true).forEachRemaining[
			resolveCrossReferences(monitor)
		]
	}
}

/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.headless.handler;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ui.commands.ICommandService;

import de.jabc.cinco.meta.core.feature.handler.GenerateFeatureProjectHandler;
import de.jabc.cinco.meta.core.ge.style.generator.action.NewGraphitiCodeGenerator;
import de.jabc.cinco.meta.core.ui.handlers.CincoProductGenerationHandler;
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.core.utils.BundleRegistry;
import de.jabc.cinco.meta.core.utils.CincoProperties;
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.plugin.gratext.GratextBuilder;
import de.jabc.cinco.meta.plugin.gratext.GratextGenerationHandler;
import de.jabc.cinco.meta.productdefinition.ui.handler.ProductProjectGenerationHandler;
import de.jabc.cinco.meta.util.xapi.FileExtension;
import productDefinition.CincoProduct;
public class HeadlessCincoProductGenerationHandler extends CincoProductGenerationHandler {

	@Override
	protected void generateGraphitiEditor() {
		System.out.println("CALLING GRAPHITI GENERATOR");
		NewGraphitiCodeGenerator graphitiGen = new NewGraphitiCodeGenerator();
		callHandler(graphitiGen);
	}
	@Override
	protected void generateCincoSIBs(IFile mglFile) {
	}
	@Override
	protected void generateProductProject() {
		System.out.println("GENERATE PRODUCT PROJECT");
		ProductProjectGenerationHandler productProjectGenerationHandler = new ProductProjectGenerationHandler();
		callHandler(productProjectGenerationHandler);
		
	}
	@Override
	protected void generateFeatureProject(IFile mglFile) {
		System.out.println("GENERATING FEATURE PROJECT");
		GenerateFeatureProjectHandler featureGenerationHandler = new GenerateFeatureProjectHandler();
		callHandler(featureGenerationHandler);
	}
	@Override
	protected void generateGratextModel(IFile mglFile) {
		System.out.println("GENERATE GRATEXT MODEL");
		MGLSelectionListener.INSTANCE.putMGLFile(mglFile);
		if (isGratextEnabled()) {
			GratextGenerationHandler handler = new GratextGenerationHandler();
			callHandler(handler);
		}
	}
	@Override	
	protected void buildGratext() {
		System.out.println("BUILDING GRATEXT MODEL");
		if (isGratextEnabled()) {
			GratextBuilder builder = new GratextBuilder();
			callHandler(builder);
		}
	}
	
	private void callHandler(AbstractHandler handler) {
		try {
			handler.execute(null);
		}catch(ExecutionException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Trying to serialize this
	 * 
	 * 
	 */
	
	private GeneratorUtils  generatorUtils = GeneratorUtils.getInstance();

	
	
	
	private boolean         pyroOnly                 = false;
	
	@Override
	synchronized public Object execute(ExecutionEvent executionEvent) throws ExecutionException {

		CincoProperties.getInstance().headless(true);
		setEvent(executionEvent);
		
		System.out.println("PREPARING");
			
		readCPDFile();
		readCincoProperties();
		deleteFolders();
		readGenerationTimestamp();
		calculateMGLSets();
		generatorUtils.clearCaches();
		forceNextGeneration();
			
		
		
		System.out.println("INITIALIZATION");
		disableAutoBuild();
		
		System.out.println("CPD PROCESSING");
		generateCPDMetaPlugins();
		calculateMGLSets();
		BundleRegistry.INSTANCE.addBundle(getCpdProject().getName(), false,true);
		generateImportedXtextLanguages();
		generateNeededGenModels();
		
		System.out.println("MGL PRE-PROCESSING");
		getGenerateMGLFiles().stream().forEach(mglFile->{
			deleteGeneratedMGLResources(mglFile.mgl);
				
		});
		generateEcoreModels();

		System.out.println("MGL PROCESSING"); 
		getGenerateMGLFiles().stream().forEach(
				mglFile -> {
					var file = mglFile.file;
					generateCincoSIBs(file);
					generateGratextModel(file);
				}
			);

		System.out.println("MGL POST-PROCESSING");
		generateGraphitiEditor();
		generateFeatureProject(null);
		prepareBuildProperties();
		generateProductProject();
		buildProject();
		System.out.println("BUILD GRATEXT");
		getGenerateMGLFiles().stream().forEach(
				mglFile -> {buildGratext();
				 mglFile.file.getName();}
			);

		System.out.println("GLOBAL PROCESSING");
		generateActivator();
		generateProjectWizard();

		restoreAutoBuild();
//		refreshWorkspace(new NullProgressMonitor());
//		buildProject();

		return null;
	}
	private void refreshWorkspace(NullProgressMonitor nullProgressMonitor) {
		try {
			getCpdProject().getWorkspace().getRoot().refreshLocal(0, nullProgressMonitor);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}

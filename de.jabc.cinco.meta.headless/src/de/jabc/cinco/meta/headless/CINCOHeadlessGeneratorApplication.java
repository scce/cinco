/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.headless;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

/**
 * The CINCOHeadlessGenerator executes the DIME Generator class from the a command line. 
 */
public class CINCOHeadlessGeneratorApplication implements IApplication {

	private String graphPath;

	private String importPath = null;
	private List<String> otherProjects = null;
	
	private final String USAGE = "Usage: cinco -data <workspacePath> -application de.jabc.cinco.meta.headless.application --importProject <projectName> --cpd <modelPath> [--otherProjects <projectName>(:<projectName>)*]?";
	@Override
	public Object start(IApplicationContext context) throws Exception {
		System.out.println("Reading Parameters");
		String[] params = getArgs(context);
		if (params[0] != null && params[1] != null) {
			importPath = params[0];
			graphPath  = params[1];
			if(params[2]!=null) {
				otherProjects = splitProjects(params[2]);
			}
			
		//} else if (params[0] != null) {
		//	graphPath  = params[0];
		} else {
			System.out.println(USAGE);
			System.out.println(String.format("Can not read parameters: %s",Arrays.toString(params)));
			return IApplication.EXIT_OK;
		}
		
		
		try {
			System.out.println("Project Root: " + importPath);
			System.out.println("CPD File: " + graphPath);
			System.out.println("Other Projects: "+otherProjects);

			Job job = new HeadlessGenerator().createGenerationJob(importPath, graphPath,otherProjects);
			System.out.println("Scheduling generation job...");
			job.schedule();
			
			synchronized (job) {
				job.join();
				
			}
			
			IStatus result = job.getResult();
			if (Status.OK_STATUS.equals(result)) {
				System.out.println("Generation job result: " + result);
				System.out.println("Success.");
				return IApplication.EXIT_OK;
			} else {
				Throwable e = result.getException();
				if (e != null) {
					e.printStackTrace();
				}
				System.out.println("Generation job result: " + result);
				System.out.println("Error.");
				return 1;
			}
		
		
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println(USAGE);
			return IApplication.EXIT_OK;
		}
		
	}
	
	private List<String> splitProjects(String projectList) {
		return Arrays.asList(projectList.split(":"));
	}

	/**
	 * Splits app arguments this way example: --importProject \<projectPath\> --cpd
	 * \<cpdPath\> => [importPath,cpdPath]
	 * 
	 * @param context
	 * @return String array of argument parameters
	 */
	public String[] getArgs(IApplicationContext context) {
		final Map<?, ?> args = context.getArguments();
		final String[] appArgs = (String[]) args.get("application.args");
		final String[] parameters = new String[3];
		if (appArgs.length >= 4) {
			int j = 0;
			for(int i=0;i<appArgs.length-1;i++) {
				if(appArgs[i].equals("--importProject")) {
					parameters[j]= appArgs[i+1];
					j++;
				}
				if(appArgs[i].equals("--cpd")) {
					parameters[j]=appArgs[i+1];
					j++;
				}
				if(appArgs[i].equals("--otherProjects")) {
					parameters[j]=appArgs[i+1];
					break;
				}
			}
		} else {
			System.err.println(Arrays.toString(appArgs));
			System.err.println(USAGE);
		}

		return parameters;
	}

	@Override
	public void stop() {
		// nothing here
	}

}

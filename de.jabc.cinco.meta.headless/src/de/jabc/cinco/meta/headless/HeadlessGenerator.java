/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.headless;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.emf.mwe2.language.Mwe2StandaloneSetup;
import org.eclipse.xtext.XtextStandaloneSetup;
import org.eclipse.xtext.resource.IResourceServiceProvider;

import de.jabc.cinco.meta.core.ge.style.StyleStandaloneSetup;
import de.jabc.cinco.meta.core.mgl.MGLStandaloneSetup;
import de.jabc.cinco.meta.core.ui.handlers.CincoProductGenerationHandler;
import de.jabc.cinco.meta.core.ui.listener.MGLSelectionListener;
import de.jabc.cinco.meta.core.utils.CincoProperties;
import de.jabc.cinco.meta.core.utils.HeadlessUtils;
import de.jabc.cinco.meta.core.utils.job.CompoundJob;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.core.utils.job.Workload;
import de.jabc.cinco.meta.headless.handler.HeadlessCincoProductGenerationHandler;
import de.jabc.cinco.meta.productdefinition.CPDStandaloneSetup;
import de.jabc.cinco.meta.util.xapi.FileExtension;
import de.jabc.cinco.meta.util.xapi.WorkspaceExtension;
import productDefinition.CincoProduct;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.xtend.core.XtendStandaloneSetup;

public class HeadlessGenerator {
	
	
	protected static CincoProduct cincoProduct = null;
	protected static IFile cpdFile = null;
	protected static HeadlessCincoProductGenerationHandler handler = null;
	protected static boolean alive = true;
	private IProgressMonitor monitor = new NullProgressMonitor();
	private String workspacePath="";
	
	public CompoundJob createGenerationJob(String mainProjectName, String cpdPath, List<String> otherProjectNames) {
		workspacePath = new WorkspaceExtension().getWorkspaceRoot().getRawLocation().toOSString();
		cpdFile = loadModel(workspacePath,mainProjectName,cpdPath,monitor);
		
		if(otherProjectNames!=null&&!otherProjectNames.isEmpty())
			HeadlessUtils.importAllProjects(workspacePath,otherProjectNames, monitor);
		
		
		
		
		CompoundJob cpgpJob = JobFactory.job("CINCO Product Generation Process");
		CompoundJob generationJob = JobFactory.job("CPGP");
		generationJob.consume(10, "Initializing...").//task("Loading Model",()->loadModel(importPath,cpdPath,monitor)).
		task("Registering Ecore Models",()->{registerEcore();}).
		task("Running Standalone Setups for Xtext Languages",setupXtextLanguagesTask()).
		task("Selecting CPD", selectCPDFileTask(cpdFile)).
		task("Beginning Generation",()->generateCincoProduct());//.

		cpgpJob.consume(10,"Opening Project").task("Opening all Projects",()->openAllProjects(ResourcesPlugin.getWorkspace()))
		.consumeConcurrent(10,"Workspace").task(()->{
			
			// Just doing something to keep the Workspace open
			while(HeadlessGenerator.alive) {
				ResourcesPlugin.getWorkspace().getRoot().isAccessible();
				if(CincoProperties.shouldShutdown()) {
					shutdown(cpgpJob);
				}
				
			}
		}).task(generationJob);
		
		
		cpgpJob.cancelOnFail(true);

		return cpgpJob;
	}

	

	private void shutdown(CompoundJob cpgpJob) {
		this.finish("Shutdown Received");
		// TODO Shutdown Workspace clean
		cpgpJob.cancel();
		
	}



	private void openAllProjects(IWorkspace workspace) {
		for(IProject project:workspace.getRoot().getProjects()) {
			try {
				project.open(monitor);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}



	@SuppressWarnings("restriction")
	private void registerEcore() {
			
			FileExtension ext = new FileExtension();
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			List<IFile> ecoreFiles = ext.findFiles(root, "ecore");
			//IResourceServiceProvider.Registry.INSTANCE.getExtensionToFactoryMap().put(key, value)
					
	}


	private void generateCincoProduct() {
		
		try {
			handler = new HeadlessCincoProductGenerationHandler();
			handler.headless();
			handler.execute(new ExecutionEvent(null,new HashMap<>(),null,null));
			finish("Generation Successful");
		} catch (Exception e) {
			finish(String.format("Exception during Generation:\n%s\n%s",e.getMessage(),Arrays.toString(e.getStackTrace())));
			e.printStackTrace();
		}
		
		
		
		
	}


	public  Runnable loadModelTask(String workspacePath,String projectName,String cpdPath, IProgressMonitor monitor) {
		return (() -> HeadlessGenerator.cpdFile = loadModel(workspacePath,projectName,cpdPath,monitor));
	}
	
	public  IFile loadModel(String workspacePath, String projectName, String cpdPath, IProgressMonitor monitor) {
		{try {
			return HeadlessUtils.getCPDFile(workspacePath,projectName, cpdPath, monitor);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			throw new RuntimeException(e);
		}}
	}
	
	private Runnable selectCPDFileTask(IFile cpdFile) {
		return (()->{selectCPDFile(cpdFile);});
	}
	
	private void selectCPDFile(IFile cpdFile) {
		MGLSelectionListener.INSTANCE.selectCPDFile(cpdFile);
	}
	
	private Runnable setupXtextLanguagesTask() {
		return ()->{
			setupXtextLanguages();
		};
	}
	
	private void setupXtextLanguages() {
		XtendStandaloneSetup.doSetup();
		XtextStandaloneSetup.doSetup();
		Mwe2StandaloneSetup.doSetup();
		MGLStandaloneSetup.doSetup();
		StyleStandaloneSetup.doSetup();
		CPDStandaloneSetup.doSetup();
		
	}
	
	public void finish(String msg) {
		System.out.println(msg);
		HeadlessGenerator.alive=false;
	}
	
	

}


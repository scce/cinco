/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.model.customfeature;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.jabc.cinco.meta.runtime.action.CincoCustomAction;
import graphmodel.IdentifiableElement;

public class CincoCustomActionFeature<T extends IdentifiableElement> extends AbstractCustomFeature {

    private CincoCustomAction<T> action;

    public CincoCustomActionFeature(IFeatureProvider fp, CincoCustomAction<T> action) {
        super(fp);
        this.action = action;
    }
    
    @Override
    public String getName() {
    	return action.getName();
    }
    
    @Override
    public boolean hasDoneChanges() {
    	return action.hasDoneChanges();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean canExecute(ICustomContext context) {
        PictogramElement[] pes = context.getPictogramElements();
        if (pes.length > 1)
            return false;
        if (pes.length == 0)
            return false;
        Object o = getBusinessObjectForPictogramElement(pes[0]);
        T modelElement = null;
        if (o instanceof IdentifiableElement) try {
            modelElement = (T) o;
            modelElement.getId(); // triggers explicit type cast
        } catch (ClassCastException e) {
            return false;
        }
        return action.canExecute(modelElement);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void execute(ICustomContext context) {
        if (canExecute(context)) {
            action.execute((T) getBusinessObjectForPictogramElement(context.getPictogramElements()[0]));
        }
    }

}





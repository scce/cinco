/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaModelStatusConstants;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.xtext.util.StringInputStream;
import org.osgi.framework.Bundle;

import de.jabc.cinco.meta.core.utils.BuildProperties;
import de.jabc.cinco.meta.core.utils.BundleRegistry;
import de.jabc.cinco.meta.core.utils.MGLUtil;
import de.jabc.cinco.meta.core.utils.generator.GeneratorUtils;
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator;
import mgl.Annotation;
import mgl.GraphModel;
import mgl.MGLModel;

public class CreateCodeGeneratorPlugin{

	private static final String GENERATOR_RUNTIME_BUNDLE_NAME = "de.jabc.cinco.meta.plugin.generator.runtime";
	private GeneratorUtils generatorUtils = GeneratorUtils.getInstance();

	public CreateCodeGeneratorPlugin() {
		// Intentionally left blank
	}

	public void execute(MGLModel mglModel) {
		createCodeGeneratorEclipseProject(mglModel);
	}
	
	public IProject createCodeGeneratorEclipseProject(MGLModel mglModel) {
		for (GraphModel graphModel : mglModel.getGraphModels()) {
			
			Annotation anno = getGeneratableAnnotation(graphModel);
			if (anno == null) {
				continue;
			}
			
			String bundleName = "";
			boolean localBundle = false;
			String implementingClassName = "";
			if (anno.getValue().size() == 3) {
				bundleName = anno.getValue().get(0);
				localBundle = bundleName.isEmpty();
				implementingClassName = anno.getValue().get(1);
			}
			else if (anno.getValue().size() == 2) {
				bundleName = "";
				localBundle = true;
				implementingClassName = anno.getValue().get(0);
			}
			
			String packageName = implementingClassName.replaceAll("\\.[^.]+$", "");
			String className   = implementingClassName.replaceAll("^(.+\\.)*", "");
			if (!implementingClassName.equals(packageName + "." + className)) {
				System.err.println(
					"Skipping '@generatable' annotation for graph model '" +
					graphModel.getName() +
					"', beacuse generator FQN is malformed: " +
					implementingClassName
				);
				continue;
			}
			
			List<String> srcFolders = new ArrayList<>();
			srcFolders.add("src");
			Set<String> requiredBundles = new HashSet<>();
			
			Path graphModelPath = new Path(graphModel.eResource().getURI().toPlatformString(true));
			IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IResource graphModelResource = workspaceRoot.findMember(graphModelPath);
			String symbolicName;
			if (graphModelResource != null) {
				symbolicName = ProjectCreator.getProjectSymbolicName(graphModelResource.getProject());
			}
			else {
				symbolicName = getPackage(graphModel);
			}
			if (localBundle) {
				bundleName = symbolicName;
			}
			
			BundleRegistry.INSTANCE.addBundle(bundleName, false, true);
			BundleRegistry.INSTANCE.addBundle(GENERATOR_RUNTIME_BUNDLE_NAME, false);
			requiredBundles.add(symbolicName);
			requiredBundles.add("org.eclipse.ui");
			requiredBundles.add("org.eclipse.core.runtime");
			requiredBundles.add("org.eclipse.core.resources");
			requiredBundles.add("org.eclipse.ui.navigator");
			requiredBundles.add("org.eclipse.emf.common");
			requiredBundles.add("org.eclipse.emf.ecore");
			requiredBundles.add("org.eclipse.graphiti.ui");
			requiredBundles.add("org.eclipse.ui.workbench");
			requiredBundles.add("de.jabc.cinco.meta.core.mgl.model");
			requiredBundles.add(GENERATOR_RUNTIME_BUNDLE_NAME);
			
			IProject project = workspaceRoot.getProject(bundleName);
			if (project != null && project.exists()) {
				exportPackage(project, bundleName, packageName, className);
			}
			else {
				try {
					String modelClassName = graphModel.getName();
					String modelPackage = getPackage(graphModel).concat(".").concat(modelClassName.toLowerCase());
					createGeneratorStubProject(project, packageName, className, modelPackage, modelClassName, graphModel);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			
			if (!localBundle) {
				requiredBundles.add(bundleName);
			}
			else {
				Bundle bundle = Platform.getBundle(GENERATOR_RUNTIME_BUNDLE_NAME);
				try {
					ProjectCreator.addRequiredBundle(project, bundle);
				}
				catch (IOException | CoreException e) {
					e.printStackTrace();
					continue;
				}
			}
			
			try {
				addGeneratorEntry(project, graphModel);
			}
			catch (CoreException e) {
				e.printStackTrace();
				continue;
			}	
		}
		return null;
	}
	
	private Annotation getGeneratableAnnotation(GraphModel graphModel) {
		for (Annotation anno : graphModel.getAnnotations()) {
			if (anno.getName().equals("generatable")) {
				return anno;
			}
		}
		return null;
	}

	private void addGeneratorEntry(IProject pr, GraphModel graphModel) throws CoreException {
		String bundleName=null;
		String implementingClassName = null;
		String outlet = null;
		ArrayList<String[]> generators  = new ArrayList<String[]>();
		for(Annotation anno: graphModel.getAnnotations()){
			if(anno.getName().equals("generatable")){
				if (anno.getValue().size() == 3) {
					bundleName = anno.getValue().get(0);
					implementingClassName = anno.getValue().get(1);
					outlet = anno.getValue().get(2);
					
				} else if (anno.getValue().size() == 2) {
					implementingClassName = anno.getValue().get(0);
					outlet = anno.getValue().get(1);
					bundleName= ProjectCreator.getProjectSymbolicName(pr);
				}
				String[] a = {bundleName,implementingClassName,outlet};
				generators.add(a);
			}
		}
		
		String extension = generateExtension(graphModel, generators);
		
		IFile plFile = pr.getFile("plugin.xml");
		
		addExtension(plFile,extension,graphModel.getName());
		
	}

	public String removeGeneratorEntries(String pluginxml,String graphmodelName){
		String regex = "<extension.*?</extension>";
		Pattern pattern = Pattern.compile(regex,Pattern.DOTALL);
		Matcher matcher = pattern.matcher(pluginxml);
		while (matcher.find()) {
			if (matcher.group().contains(String.format("<!--@MetaPlugin Generatable %s-->",graphmodelName)))
				pluginxml = pluginxml.replace(matcher.group() + "\n", "");
		}
		return pluginxml;
	}
	
	@SuppressWarnings("resource")
	private void addExtension(IFile plFile, String extension,String graphModelName) throws CoreException {
		if(plFile.exists()){
				InputStream l = plFile.getContents(true);
				String contents = new Scanner(l, "UTF-8").useDelimiter("\\A").next();
				contents = removeGeneratorEntries(contents, graphModelName);
				contents = contents.replace("</plugin>", extension + "\n" + "</plugin>");
				plFile.setContents(new StringInputStream(contents), true, true, new NullProgressMonitor());
		}else{
			String pluginXML = String.format("<plugin>%s\n</plugin>",extension);
			plFile.create(new StringInputStream(pluginXML), true,	new NullProgressMonitor());
		}
	}

	private String generateExtension(GraphModel graphModel,
		ArrayList<String[]> generators) {
		StringBuilder sb = new StringBuilder();
		sb.append("<extension\n"+
				"point=\"de.jabc.cinco.meta.plugin.generator.runtime.registry\">\n"+
				String.format("<!--@MetaPlugin Generatable %s-->\n",graphModel.getName())+
				"<graphmodel\n");
		sb.append(String.format("      class=\"%s\">\n",generateFullyQualifiedName(graphModel)));
		sb.append("</graphmodel>\n");
    
		for(String[]gen: generators){
	    	sb.append("<generator\n");
	    	if(!gen[0].equals("")){
	    		sb.append(String.format("bundle_id=\"%s\"\n",gen[0]));
	    	}else{
	    		sb.append(String.format("bundle_id=\"%s\"\n",getPackage(graphModel)));
	    	}
	        
	        sb.append(String.format("class=\"%s\"\n",gen[1]));
	        sb.append(String.format("outlet=\"%s\">\n</generator>\n",gen[2]));
		}
    
		sb.append("</extension>");
    
		return sb.toString();
	}

	private String getPackage(GraphModel graphModel) {
		return MGLUtil.mglModel(graphModel).getPackage();
	}

	private Object generateFullyQualifiedName(GraphModel graphModel) {
		String name = graphModel.getName();
		String nameLower = generatorUtils.getFileName(((MGLModel) graphModel.eContainer())).toLowerCase();
		String packageName=getPackage(graphModel);
		String fqName = String.format("%s.%s.%s",packageName,nameLower,name);
		return fqName;
	}

	@SuppressWarnings("unused")
	private IProject createGenerationHandlerProject(
			List<String> exportedPackages,
			List<String> additionalNature, String projectName,
			List<IProject> referencedProjects, List<String> srcFolders,
			Set<String> requiredBundles) throws IOException,
			FileNotFoundException, CoreException {
		if(!new Path("/"+projectName).toFile().exists()){
			
		IProgressMonitor progressMonitor = new NullProgressMonitor();
		IProject tvProject = ProjectCreator.createProject(projectName,
				srcFolders, referencedProjects, requiredBundles,
				exportedPackages, additionalNature, progressMonitor,false);
		String projectPath = tvProject.getLocation().makeAbsolute()
				.toPortableString();
		
		File maniFile = tvProject.getLocation().append("META-INF/MANIFEST.MF").toFile();
		BufferedWriter bufwr = new BufferedWriter(new FileWriter(maniFile,true));
		bufwr.append("Bundle-Activator: " +projectName+".Activator\n");
		bufwr.append("Bundle-ActivationPolicy: lazy\n");
		bufwr.flush();
		bufwr.close();
		
		IFile bpf = (IFile) tvProject.findMember("build.properties");
		BuildProperties buildProperties = BuildProperties.loadBuildProperties(bpf);
		buildProperties.appendBinIncludes("plugin.xml");
		buildProperties.store(bpf, progressMonitor);
		return tvProject;
		}
		return null;
	}

	private void exportPackage(IProject pr,String bundleName, String packageName,String className) {
			Manifest manni;
			try {
				File manniFile = pr.getFile("/META-INF/MANIFEST.MF").getLocation().makeAbsolute().toFile();
				manni = new Manifest(new FileInputStream(manniFile));
				Attributes mainAttr = manni.getMainAttributes();
				String oldValues = mainAttr.getValue("Export-Package");
				if (oldValues == null)
					oldValues = new String();
				if(!oldValues.contains(packageName)) {
					String newValues = oldValues.isEmpty()
						? packageName
						: oldValues.concat(",").concat(packageName);
					mainAttr.putValue("Export-Package", newValues);
					manni.write(new FileOutputStream(manniFile));
				}
			} catch (IOException e) {
				throw new RuntimeException("IOException while exporting Package",e);
			}
	}

	private void createGeneratorStubProject(IProject pr,String packageName,String className, String modelPackage,String modelClassName,GraphModel graphModel) throws RuntimeException{
		try{
		String graphModelProjectName = ProjectCreator.getProject(graphModel.eResource()).getName(); 
		String projectName = pr.getName();
		Set<String> requiredBundles = new HashSet<>();
		requiredBundles.add("de.jabc.cinco.meta.core.mgl.model");
		requiredBundles.add("org.eclipse.equinox.registry");
		requiredBundles.add(GENERATOR_RUNTIME_BUNDLE_NAME);
		requiredBundles.add(graphModelProjectName);
		List<IProject> referencedProjects = new ArrayList<>();
		List<String> srcFolders = new ArrayList<>();
		srcFolders.add("src");
		List<String> exportedPackages = new ArrayList<>();
		exportedPackages.add(packageName);
		List<String> additionalNature = new ArrayList<>();
		IProgressMonitor progressMonitor = new NullProgressMonitor();
		IProject tvProject = ProjectCreator.createProject(projectName,
				srcFolders, referencedProjects, requiredBundles,
				exportedPackages, additionalNature, progressMonitor,false);
		tvProject.refreshLocal(IResource.DEPTH_INFINITE, progressMonitor);
		try{
			ProjectCreator.createJavaClass(pr, packageName, className,tvProject.getFolder("/src/"),stubContents(packageName,className,modelPackage,modelClassName), progressMonitor);
		}catch(JavaModelException e){
			if(e.getJavaModelStatus().getCode() != IJavaModelStatusConstants.NAME_COLLISION)
				throw e;
		}
		
		}catch(Exception e){
			throw new RuntimeException("Exception while creating Generator Stub Project", e);
		}
	}

	private String stubContents(String packageName, String className, String modelPackage, String modelClassName) {
		String contents = "package %s;\n"+
				"\n"+
				"import org.eclipse.core.runtime.IPath;\n"+
				"import %s.%s;\n"+
				"import org.eclipse.core.runtime.IProgressMonitor;\n"+
				"import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;\n"+
				"\n"+
				"\n"+
				"public class %s implements IGenerator<%s>{\n"+
				"\tpublic void generate(%s model,IPath outlet, IProgressMonitor monitor){\n"+
				"\n"+
				"\t}\n"+
				"\n"+
				"}\n";
		return String.format(contents, packageName,modelPackage,modelClassName,className,modelClassName,modelClassName);
	}

}

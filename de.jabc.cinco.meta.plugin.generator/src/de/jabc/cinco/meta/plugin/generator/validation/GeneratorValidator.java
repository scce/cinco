/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.plugin.generator.validation;

import mgl.Annotation;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult;
import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator;

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.newError;

public class GeneratorValidator implements IMetaPluginValidator {

	public GeneratorValidator() {
	}

	@Override
	public ValidationResult<String, EStructuralFeature> checkOnEdit(EObject eObject) {
		if(eObject.eClass().getName().equals("Annotation")){
			Annotation anno = (Annotation)eObject;
			
			if(anno.getName().equals("generatable") && anno.getValue().size() < 2)
				return newError("Generatable Annotation values must consist of qualified implementing Class name and outlet folder (relative to project).",eObject.eClass().getEStructuralFeature("value"));
		}
		return null;
	}

}

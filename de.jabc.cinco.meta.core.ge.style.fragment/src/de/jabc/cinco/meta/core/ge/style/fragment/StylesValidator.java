/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.fragment;

import java.util.Collection;

import mgl.Annotation;
import mgl.Edge;
import mgl.GraphModel;
import mgl.GraphicalModelElement;
import mgl.MGLModel;
import mgl.MglPackage;
import mgl.ModelElement;
import mgl.Node;
import mgl.NodeContainer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import style.EdgeStyle;
import style.NodeStyle;
import style.Style;
import style.Styles;
import de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult;
import de.jabc.cinco.meta.core.pluginregistry.validation.IMetaPluginValidator;
import de.jabc.cinco.meta.core.utils.CincoUtil;
import de.jabc.cinco.meta.core.utils.PathValidator;

import static de.jabc.cinco.meta.core.pluginregistry.validation.ValidationResult.newError;

public class StylesValidator implements IMetaPluginValidator {

	private static enum RequiredStyleType {
		EdgeStyle,
		NodeStyle
	}
	
	public StylesValidator() {

	}

	@Override
	public ValidationResult<String, EStructuralFeature> checkOnEdit(EObject eObject) {
		ValidationResult<String, EStructuralFeature> ep = null;
		if (eObject instanceof MGLModel)
			return checkMGLModelStylePathAttribute((MGLModel) eObject);
		if (eObject instanceof Node || eObject instanceof Edge)
			return checkStyleAttribute((GraphicalModelElement) eObject);
		if ( !(eObject instanceof Annotation) )
			return null;
		Annotation annotation = (Annotation) eObject;
		ModelElement me = getModelElement((Annotation) eObject);
		if (me instanceof GraphModel && annotation.getName().equals(CincoUtil.ID_DISABLE_HIGHLIGHT)) {
			ep = checkGraphModelDisableHighlight((GraphModel) me, annotation);
		}
		if (annotation.getName().equals(CincoUtil.ID_ICON)) {
			ep = checkIcon(annotation);
		}
		if (me instanceof ModelElement && annotation.getName().equals(CincoUtil.ID_DISABLE)) {
			ep = checkDisable(me, annotation);
		}
		
		return ep;
	} 
	
	private ValidationResult<String, EStructuralFeature> checkDisable(ModelElement me,	Annotation annotation) {
		ValidationResult<String, EStructuralFeature> result = null;
		if (me instanceof Node)
			result = checkPredefinedValue(CincoUtil.DISABLE_NODE_VALUES, annotation);
		if (me instanceof Edge)
			result = checkPredefinedValue(CincoUtil.DISABLE_EDGE_VALUES, annotation);
		return result;
	}
	
	private ValidationResult<String, EStructuralFeature> checkPredefinedValue(Collection<String> values, Annotation annotation) {
		for (String s : annotation.getValue()) {
			if (!values.contains(s))
				return newError(
					"Invalid value: \"" +s+ "\". Possible values are: " + values, 
					annotation.eClass().getEStructuralFeature(MglPackage.ANNOTATION__NAME));
		}
		return null;
	}
	
	private ValidationResult<String, EStructuralFeature> checkIcon(Annotation annotation) {
		if (annotation.getValue().size() == 0)
			return newError(
					"Please specify an icon by relative or platform path", annotation.eClass()
					.getEStructuralFeature("value"));
		
		String path = annotation.getValue().get(0);
		String retval = PathValidator.checkPath(annotation, path);
		ValidationResult<String, EStructuralFeature> ep = newError(
				retval ,annotation.eClass()
				.getEStructuralFeature("value"));
		return (retval.isEmpty()) ? null : ep;
	}
	
	private ValidationResult<String, EStructuralFeature> checkStyleAttribute(GraphicalModelElement gme) {
		String styleName = gme.getUsedStyle();
		if (!gme.isIsAbstract() && (styleName == null || styleName.isEmpty())) {
			return newError(
					"Please provide a style atrribute",
					gme.eClass().getEStructuralFeature("usedStyle"));
		}
		
		Styles styles = getStyles(getMGLModel(gme));
		if (styles == null)
			return null;
		
		RequiredStyleType reqStyleType = null;
		if(gme instanceof Node) {
			reqStyleType = RequiredStyleType.NodeStyle;
		} else if(gme instanceof Edge) {
			reqStyleType = RequiredStyleType.EdgeStyle;
		}
		
		Style style = null;
		for (Style s : styles.getStyles()) {
			if (s.getName().equals(styleName) ){
				if (reqStyleType == RequiredStyleType.NodeStyle && s instanceof EdgeStyle)
					return newError(
							"Edge style assigned to a node.",
							gme.eClass().getEStructuralFeature("usedStyle"));
				if (reqStyleType == RequiredStyleType.EdgeStyle && s instanceof NodeStyle)
					return newError(
							"Node style assigned to an edge.",
							gme.eClass().getEStructuralFeature("usedStyle"));
				style = s;
				break;
			}
		}
		
		if (!(styleName == null || styleName.isEmpty()) && style == null) {
			return newError(
					"Style: " + styleName + " does not exist",
					gme.eClass().getEStructuralFeature("usedStyle"));
		}
		
		int params = style.getParameterCount();
		int providedStyleParametersCount = gme.getStyleParameters().size();
		if (params > providedStyleParametersCount)
			return newError(
					"Style: " + styleName + " contains text element with " + params + " parameter(s) but you provided: " + providedStyleParametersCount,
					gme.eClass().getEStructuralFeature("usedStyle"));
		if (params < providedStyleParametersCount) {
			return newError(
					"Style: " + styleName +" contains text element with " + params + " parameter(s) but you provided: " + providedStyleParametersCount,
					gme.eClass().getEStructuralFeature("usedStyle"));
		}
		return null;
	}
	
	private ValidationResult<String, EStructuralFeature> checkMGLModelStylePathAttribute(MGLModel mm) {
		if (mm.getStylePath() != null && mm.getStylePath().length() == 0) {
			return newError("Missing path to style file", 
					mm.eClass().getEStructuralFeature("stylePath"));
		} else {
			PathValidator.checkPath(mm, mm.getStylePath());
			Styles styles = getStyles(mm);
			if (styles == null)
				return newError("Style file " + mm.getStylePath() +" does not exist", 
						mm.eClass().getEStructuralFeature("stylePath"));
		}
		return null;
	}
	
	private ValidationResult<String, EStructuralFeature> checkGraphModelDisableHighlight(GraphModel me, Annotation annotation) {
		return checkPredefinedValue(CincoUtil.DISABLE_HIGHLIGHT_VALUES, annotation);
	}
	
	private ModelElement getModelElement(Annotation annot) {
		if (annot.getParent() instanceof ModelElement)
			return (ModelElement) annot.getParent();
		return null;
	}
	
	private MGLModel getMGLModel(ModelElement me) {
		return (MGLModel)me.eContainer();
	}
	
	private Styles getStyles(MGLModel mm) {
		String path = mm.getStylePath();
		if (path.length() > 0) {
			URI uri = URI.createURI(path, true);
			try {
				Resource res = null;
				if (uri.isPlatformResource()) {
					res = new ResourceSetImpl().getResource(uri, true);
				}
				else {
					IProject p = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(mm.eResource().getURI().toPlatformString(true))).getProject();
					IFile file = p.getFile(path);
					if (file.exists()) {
						URI fileURI = URI.createPlatformResourceURI(file.getFullPath().toOSString(), true);
						res = new ResourceSetImpl().getResource(fileURI, true);
					}
					else {
						return null;
					}
				}
				
				for (Object o : res.getContents()) {
					if (o instanceof Styles)
						return (Styles) o;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
	
}

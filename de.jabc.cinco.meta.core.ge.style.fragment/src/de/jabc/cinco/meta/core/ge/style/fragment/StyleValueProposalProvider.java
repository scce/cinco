/*-
 * #%L
 * CINCO
 * %%
 * Copyright (C) 2021 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package de.jabc.cinco.meta.core.ge.style.fragment;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mgl.Annotation;
import mgl.Edge;
import mgl.GraphModel;
import mgl.MGLModel;
import mgl.Node;
import mgl.NodeContainer;
import mgl.Type;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal.IReplacementTextApplier;

import style.EdgeStyle;
import style.NodeStyle;
import style.Style;
import style.Styles;
import de.jabc.cinco.meta.core.pluginregistry.proposalprovider.IMetaPluginAcceptor;
import de.jabc.cinco.meta.core.utils.xtext.ChooseFileTextApplier;

public class StyleValueProposalProvider implements IMetaPluginAcceptor {

	private final String STYLE_ID = "style";
	private final String ICON_ID = "icon";
	
	public StyleValueProposalProvider() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<String> getAcceptedStrings(Annotation annotation) {
		String annotName = annotation.getName();
		Styles styles;
		if (STYLE_ID.equals(annotName)) {
			if(annotation.getParent() instanceof Type){
				Type type = (Type) annotation.getParent();
				MGLModel gModel = getMGLModel(type);
				try {
					styles = getStyles(gModel);
					if (type instanceof Node) {
						return getNodeStyles(styles);
					}
					if (type instanceof Edge) {
						return getEdgeStyles(styles);
					}
					if (type instanceof NodeContainer) {
						return getNodeStyles(styles);
					}
					if (type instanceof GraphModel) {
						return Collections.emptyList();
					}
				} catch (FileNotFoundException fnfe) {
					return Collections.emptyList();
				}
			}
		}
		
		if (ICON_ID.equals(annotName)) {
			List<String> ret = new ArrayList<>();
			ret.add("Choose file...");
			return ret;
		}
		
		return Collections.emptyList();
	}
	
	MGLModel getMGLModel(Type me) {
		return (MGLModel) me.eContainer();
	}
	
	private List<String> getNodeStyles(Styles styles) {
		List<String> nodeStyles = new ArrayList<>();
		for (Style s : styles.getStyles()) {
			if (s instanceof NodeStyle) {
				nodeStyles.add(s.getName());
			}
		}
		return nodeStyles;
	}
	
	private List<String> getEdgeStyles(Styles styles) {
		List<String> edgeStyles = new ArrayList<>();
		for (Style s : styles.getStyles()) {
			if (s instanceof EdgeStyle) {
				edgeStyles.add(s.getName());
			}
		}
		return edgeStyles;
	}

	private Styles getStyles(MGLModel mglModel) throws FileNotFoundException {
		String stylePath = mglModel.getStylePath();
		if (stylePath != null && !stylePath.isEmpty()) {
			URI uri = URI.createURI(stylePath, true);
			Resource res = null;
			if (uri.isPlatformResource())
				res = new ResourceSetImpl().getResource(uri, true);
			else {
				IProject p = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(mglModel.eResource().getURI().toPlatformString(true))).getProject();
				IFile file = p.getFile(stylePath);
				URI fileURI = URI.createPlatformResourceURI(file.getFullPath().toOSString(), true);
				res = new ResourceSetImpl().getResource(fileURI, true);
			}
			if (res == null) {
				throw new FileNotFoundException(stylePath);
			}
			
			Object o = res.getContents().get(0);
			if (o instanceof Styles)
				return (Styles) o;
			else throw new FileNotFoundException(stylePath);
		}
		return null;
	}

	@Override
	public IReplacementTextApplier getTextApplier(Annotation annotation) {
		if (ICON_ID.equals(annotation.getName())) {
			return new ChooseFileTextApplier(annotation);
		}
		return null;
	}

}
